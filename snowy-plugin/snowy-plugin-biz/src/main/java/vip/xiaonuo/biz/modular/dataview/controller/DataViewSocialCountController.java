package vip.xiaonuo.biz.modular.dataview.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.dataview.param.DataViewMapVO;
import vip.xiaonuo.biz.modular.dataview.param.DataViewMonthPowerReq;
import vip.xiaonuo.biz.modular.dataview.param.DataViewSocialCountVO;
import vip.xiaonuo.biz.modular.dataview.service.DataViewSocialCountService;
import vip.xiaonuo.biz.modular.homepage.param.SkyLoginUserIdParam;
import vip.xiaonuo.biz.modular.homepage.param.StationAllHisPowerVO;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @Author wangjian
 * @Date 2024/7/8 13:21
 */
@Api(tags = "大屏社会数据统计")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class DataViewSocialCountController {

    @Resource
    private DataViewSocialCountService dataViewSocialCountService;

    @ApiOperationSupport(order = 1)
    @ApiOperation("社会数据统计")
    @PostMapping("/biz/dataView/social")
    public CommonResult<DataViewSocialCountVO> social() {
        return CommonResult.data(dataViewSocialCountService.social());
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation("月数据")
    @PostMapping("/biz/dataView/monthPower")
    public CommonResult<StationAllHisPowerVO> monthPower(@RequestBody @Valid DataViewMonthPowerReq req) throws RuntimeException{
        //日期
        String date = req.getDate();
        String province = req.getCity();
        String pattern = "\\d{4}-\\d{2}";
        if (!Pattern.matches(pattern, date)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        SkyLoginUserIdParam param = null;
        if (roleCodeList.contains(SkyRoleCodeEnum.SUPERADMIN.getValue())) {
            // 超管都可以
        } else if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param = new SkyLoginUserIdParam();
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param = new SkyLoginUserIdParam();
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param = new SkyLoginUserIdParam();
            param.setStationContactId(loginUser.getId());
        }
        return CommonResult.data(dataViewSocialCountService.monthPower(date, param, province));
    }

    @ApiOperationSupport(order = 3)
    @ApiOperation("年数据")
    @PostMapping("/biz/dataView/yearPower")
    public CommonResult<StationAllHisPowerVO> yearPower(@RequestBody @Valid DataViewMonthPowerReq req) throws RuntimeException{
        //日期
        String date = req.getDate();
        String city = req.getCity();
        String pattern = "\\d{4}";
        if (!Pattern.matches(pattern, date)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        SkyLoginUserIdParam param = null;
        if (roleCodeList.contains(SkyRoleCodeEnum.SUPERADMIN.getValue())) {
            // 超管都可以
        } else if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param = new SkyLoginUserIdParam();
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param = new SkyLoginUserIdParam();
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param = new SkyLoginUserIdParam();
            param.setStationContactId(loginUser.getId());
        }
        return CommonResult.data(dataViewSocialCountService.yearPower(date, param, city));
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation("获取登录账号下所有市")
    @PostMapping("/biz/dataView/city")
    public CommonResult<List<String>> city() {
        return CommonResult.data(dataViewSocialCountService.city());
    }

    @ApiOperationSupport(order = 5)
    @ApiOperation("地图轮播数据")
    @PostMapping("/biz/dataView/map")
    public CommonResult<List<DataViewMapVO>> map() {
        return CommonResult.data(dataViewSocialCountService.map());
    }

    /**
     * 校验日期
     *
     * @param date 日期
     * @param type 类型（1-日2-月3-年4-总）
     * @return
     */
    private boolean validateDate(String date, int type) {
        String pattern;
        switch (type) {
            case 1:
                pattern = "\\d{4}-\\d{2}-\\d{2}";
                break;
            case 2:
                pattern = "\\d{4}-\\d{2}";
                break;
            case 3:
                pattern = "\\d{4}";
                break;
            case 4:
                return true;
            default:
                // 非法类型，直接返回false
                return false;
        }
        return Pattern.matches(pattern, date);
    }
}
