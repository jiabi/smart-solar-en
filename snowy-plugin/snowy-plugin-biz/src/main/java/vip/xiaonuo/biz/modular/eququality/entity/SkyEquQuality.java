/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.eququality.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 设备质保信息实体
 *
 * @author 全佳璧
 * @date  2023/09/25 14:20
 **/
@Getter
@Setter
@TableName("sky_equ_quality")
public class SkyEquQuality {

    /** 设备SN */
    @TableId
    @ApiModelProperty(value = "设备SN", position = 1)
    private String equSn;

    /** 设备类型 */
    @ApiModelProperty(value = "设备类型", position = 2)
    private Integer equType;

    /** 设备型号 */
    @ApiModelProperty(value = "设备型号", position = 3)
    private String equModel;

    /** 发货日期 */
    @ApiModelProperty(value = "发货日期", position = 4)
    private Date startDate;

    /** 质保期限 */
    @ApiModelProperty(value = "质保期限", position = 5)
    private Date endDate;

    /** 质保时长 */
    @ApiModelProperty(value = "质保时长", position = 6)
    private Double timeLimit;

    /** 临期状态 */
    @ApiModelProperty(value = "临期状态", position = 7)
    private Integer criticalStatus;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 8)
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 9)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 10)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 11)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 12)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 13)
    private String tenantId;
}
