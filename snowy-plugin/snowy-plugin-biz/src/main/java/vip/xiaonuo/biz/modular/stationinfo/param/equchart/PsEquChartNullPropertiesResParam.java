package vip.xiaonuo.biz.modular.stationinfo.param.equchart;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/6/5 14:57
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ContentRowHeight(18)
@HeadRowHeight(20)
@ColumnWidth(24)
public class PsEquChartNullPropertiesResParam {
    /**
     * 时间轴
     */
    @ExcelProperty(value = "时间")
    private String time;
}
