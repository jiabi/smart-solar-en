package vip.xiaonuo.biz.modular.app.alarm.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.modular.alarminfo.entity.SkyAlarmInfo;
import vip.xiaonuo.biz.modular.alarminfo.mapper.SkyAlarmInfoMapper;
import vip.xiaonuo.biz.modular.alarminfo.param.SkyAlarmInfoIdParam;
import vip.xiaonuo.biz.modular.alarminfo.service.SkyAlarmInfoService;
import vip.xiaonuo.biz.modular.alarminfocurrent.entity.SkyAlarmInfoCurrent;
import vip.xiaonuo.biz.modular.alarminfocurrent.mapper.SkyAlarmInfoCurrentMapper;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoCurrentIdParam;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageParam;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageVO;
import vip.xiaonuo.biz.modular.alarminfocurrent.service.SkyAlarmInfoCurrentService;
import vip.xiaonuo.biz.modular.app.alarm.mapper.AppSkyAlarmInfoMapper;
import vip.xiaonuo.biz.modular.app.alarm.param.AppSkyAlarmHisInfoRes;
import vip.xiaonuo.biz.modular.app.alarm.param.AppSkyAlarmInfoPageVO;
import vip.xiaonuo.biz.modular.app.alarm.param.AppSkyAlarmCurrentInfoRes;
import vip.xiaonuo.biz.modular.app.alarm.service.AppSkyAlarmInfoService;
import vip.xiaonuo.biz.modular.app.inv.mapper.AppSkyInvInfoMapper;
import vip.xiaonuo.biz.modular.app.inv.param.AppPsIdAndNameParam;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoIdParam;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoVO;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.common.page.CommonPageRequest;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/3/15 15:24
 */
@Service
public class AppSkyAlarmInfoServiceImpl implements AppSkyAlarmInfoService {

    @Resource
    private AppSkyAlarmInfoMapper appSkyAlarmInfoMapper;

    @Resource
    private SkyAlarmInfoCurrentService skyAlarmInfoCurrentService;

    @Resource
    private SkyAlarmInfoService skyAlarmInfoService;

    @Resource
    private SkyAlarmInfoMapper skyAlarmInfoMapper;

    @Resource
    private SkyAlarmInfoCurrentMapper alarmInfoCurrentMapper;

    @Resource
    private AppSkyInvInfoMapper appskyInvInfoMapper;

    @Override
    public Page<AppSkyAlarmInfoPageVO> page(SkyAlarmInfoPageParam skyAlarmInfoPageParam) {
        Page<AppSkyAlarmInfoPageVO> res = new Page<>();
        List<SkyAlarmInfoPageVO> records = appSkyAlarmInfoMapper.page(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList());
        if (CollectionUtils.isEmpty(records)) {
            return new Page<>();
        }

        // 获取电站
        List<String> snList = records.stream().map(SkyAlarmInfoPageVO::getEquSn).collect(Collectors.toList());
        List<AppPsIdAndNameParam> list = appskyInvInfoMapper.getStationBySN(snList);
        Map<String, AppPsIdAndNameParam> map = null;
        if (!CollectionUtils.isEmpty(list)) {
            map = list.stream().collect(Collectors.toMap(AppPsIdAndNameParam::getSn, Function.identity()));
        }

        // 组装数据
        List<AppSkyAlarmInfoPageVO> resVOs = new ArrayList<>();
        for (SkyAlarmInfoPageVO record : records) {
             AppSkyAlarmInfoPageVO resvo = new AppSkyAlarmInfoPageVO();
            resvo.setId(record.getId());
            resvo.setName(record.getAlarmCode());
            resvo.setSn(record.getEquSn());
            resvo.setType(record.getAlarmLevel());
            if (record.getAlarmLevel()!=null && record.getAlarmLevel() == 1) {
                resvo.setFaultType("告警");
            } else if (record.getAlarmLevel()!=null && record.getAlarmLevel() == 2){
                resvo.setFaultType("故障");
            }
            resvo.setCreatTime(record.getEndTime());
            resvo.setAlarmTime(record.getBeginTime());
            resvo.setStatus(ObjectUtil.isNotEmpty(record.getEndTime()) ? "已恢复":"发生中");
            if (!CollectionUtils.isEmpty(map)) {
                if (ObjectUtil.isNotEmpty(map.get(record.getEquSn()))) {
                    AppPsIdAndNameParam param = map.get(record.getEquSn());
                    resvo.setPsName(param.getPsName());
                    resvo.setPsGuid(param.getPsId());
                    resvo.setDeviceName(param.getDeviceName());
                    resvo.setDeviceType(param.getDeviceType());
                    resvo.setZone(param.getZone());
                }
            }
            resVOs.add(resvo);
        }
        res.setTotal(appSkyAlarmInfoMapper.countNum(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList()));
        res.setRecords(resVOs);
        res.setSize(skyAlarmInfoPageParam.getSize());
        res.setCurrent(skyAlarmInfoPageParam.getCurrent());
        return res;
    }

    @Override
    public Page<AppSkyAlarmInfoPageVO> currentPage(SkyAlarmInfoPageParam skyAlarmInfoPageParam) {
        Page<AppSkyAlarmInfoPageVO> res = new Page<>();

        List<SkyAlarmInfoPageVO> records = appSkyAlarmInfoMapper.pageCurrent(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList());
        if (CollectionUtils.isEmpty(records)) {
            return new Page<>();
        }

        // 获取电站
        List<String> snList = records.stream().map(SkyAlarmInfoPageVO::getEquSn).collect(Collectors.toList());
        List<AppPsIdAndNameParam> list = appskyInvInfoMapper.getStationBySN(snList);
        Map<String, AppPsIdAndNameParam> map = null;
        if (!CollectionUtils.isEmpty(list)) {
            map = list.stream().collect(Collectors.toMap(AppPsIdAndNameParam::getSn, Function.identity()));
        }

        // 组装数据
        List<AppSkyAlarmInfoPageVO> resVOs = new ArrayList<>();
        for (SkyAlarmInfoPageVO record : records) {
            AppSkyAlarmInfoPageVO resvo = new AppSkyAlarmInfoPageVO();
            resvo.setId(record.getId());
            resvo.setName(record.getAlarmCode());
            resvo.setSn(record.getEquSn());
            resvo.setType(record.getAlarmLevel());
//            if (record.getAlarmLevel()!=null && record.getAlarmLevel() == 1) {
//                resvo.setFaultType("告警");
//            } else if (record.getAlarmLevel()!=null && record.getAlarmLevel() == 2){
//                resvo.setFaultType("故障");
//            }
            resvo.setCreatTime(record.getEndTime());
            resvo.setAlarmTime(record.getBeginTime());
            resvo.setStatus(ObjectUtil.isNotEmpty(record.getEndTime()) ? "2":"1");
            if (!CollectionUtils.isEmpty(map)) {
                if (ObjectUtil.isNotEmpty(map.get(record.getEquSn()))) {
                    AppPsIdAndNameParam param = map.get(record.getEquSn());
                    resvo.setPsName(param.getPsName());
                    resvo.setPsGuid(param.getPsId());
                    resvo.setDeviceName(param.getDeviceName());
                    resvo.setDeviceType(param.getDeviceType());
                    resvo.setZone(param.getZone());
                }
            }
            resVOs.add(resvo);
        }
        res.setTotal(appSkyAlarmInfoMapper.countCurrentNum(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList()));
        res.setRecords(resVOs);
        res.setSize(skyAlarmInfoPageParam.getSize());
        res.setCurrent(skyAlarmInfoPageParam.getCurrent());
        return res;
    }

    @Override
    public AppSkyAlarmCurrentInfoRes detail(SkyAlarmInfoCurrentIdParam skyAlarmInfoIdParam) {
        SkyAlarmInfoCurrent detail = skyAlarmInfoCurrentService.detail(skyAlarmInfoIdParam);
        if (detail == null) {
            return null;
        }
        AppSkyAlarmCurrentInfoRes res = new AppSkyAlarmCurrentInfoRes();
        res.setName(detail.getAlarmCode());
        res.setSn(detail.getEquSn());
        res.setType(detail.getAlarmLevel());
        res.setCreatTime(detail.getBeginTime());
        res.setCause(detail.getAlarmSource());
        res.setAdvice(detail.getAlarmMsg());

        if (StringUtils.isNotEmpty(detail.getEquSn())) {
            List<String> sns = new ArrayList<>();
            List<AppPsIdAndNameParam> list = appskyInvInfoMapper.getStationBySN(sns);
            Map<String, AppPsIdAndNameParam> map = list.stream().collect(Collectors.toMap(AppPsIdAndNameParam::getSn, Function.identity()));
            if (!CollectionUtils.isEmpty(map)) {
                if (ObjectUtil.isNotEmpty(map.get(detail.getEquSn()))) {
                    AppPsIdAndNameParam param = map.get(detail.getEquSn());
                    res.setPsName(param.getPsName());
                    res.setPsGuid(param.getPsId());
                }
            }
        }
        return res;
    }

    @Override
    public AppSkyAlarmHisInfoRes getHistoryAlarmInfo(SkyAlarmInfoIdParam skyAlarmInfoIdParam) {
        SkyAlarmInfo detail = skyAlarmInfoService.detail(skyAlarmInfoIdParam);
        if (detail == null) {
            return null;
        }
        AppSkyAlarmHisInfoRes res = new AppSkyAlarmHisInfoRes();
        res.setName(detail.getAlarmCode());
        res.setSn(detail.getEquSn());
        res.setType(detail.getAlarmLevel());
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        res.setCreatTime(simpleDateFormat.format(detail.getEndTime()));
        res.setAlarmTime(simpleDateFormat.format(detail.getBeginTime()));
        res.setCause(detail.getAlarmSource());
        res.setAdvice(detail.getAlarmMsg());

        if (StringUtils.isNotEmpty(detail.getEquSn())) {
            List<String> sns = new ArrayList<>();
            List<AppPsIdAndNameParam> list = appskyInvInfoMapper.getStationBySN(sns);
            Map<String, AppPsIdAndNameParam> map = list.stream().collect(Collectors.toMap(AppPsIdAndNameParam::getSn, Function.identity()));
            if (!CollectionUtils.isEmpty(map)) {
                if (ObjectUtil.isNotEmpty(map.get(detail.getEquSn()))) {
                    AppPsIdAndNameParam param = map.get(detail.getEquSn());
                    res.setPsName(param.getPsName());
                    res.setPsGuid(param.getPsId());
                }
            }
        }
        return res;
    }
}
