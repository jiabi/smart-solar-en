package vip.xiaonuo.biz.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Author wangjian
 * @Date 2023/9/25 14:23
 **/
@Component
public class StationInfoConfig {
    @Value("${smart.farm.dev.timeZone}")
    private String timeZone;

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

}
