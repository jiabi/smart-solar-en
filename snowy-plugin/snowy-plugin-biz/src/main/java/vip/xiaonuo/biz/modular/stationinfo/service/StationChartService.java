package vip.xiaonuo.biz.modular.stationinfo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoIdParam;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoIdParam;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.PsEquChartBaseInfoResVO;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.PsChartBaseInfoResVO;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.PsChartDataResVO;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.PsEquChartResVO;
import vip.xiaonuo.biz.modular.stationinfo.param.PsOverviewHisPowerProportionVO;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/2/28 18:38
 */
public interface StationChartService extends IService<StationInfo> {

    PsOverviewHisPowerProportionVO hisPowerProportion(Integer type, String date, String stationId);

    PsChartDataResVO psChart(Integer type, String date, List<String> stationId);

    PsEquChartResVO equChart(String deviceSn, String date, Integer type, List<String> paramsList);

    PsChartBaseInfoResVO psChartBaseInfo(StationInfoIdParam req);

    PsEquChartBaseInfoResVO equChartBaseInfo(SkyInvInfoIdParam req);
}
