package vip.xiaonuo.biz.modular.app.inv.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/27 19:15
 */

@Getter
@Setter
public class AppPsIdAndNameParam {
    private String psId;
    private String sn;
    private String psName;

    /** 时区 */
    private String zone;
    private Integer deviceType;
    private String deviceName;
}
