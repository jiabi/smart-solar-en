package vip.xiaonuo.biz.modular.app.station.param.chart;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/12 15:00
 */
@Getter
@Setter
public class AppPsPowerChartVO {
    /**
     * 日期集合
     */
    private List<String> dateList;

    /**
     * 发电量集合
     */
    private List<Double> power;
}
