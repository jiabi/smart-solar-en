package vip.xiaonuo.biz.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author wangjian
 * @Date 2023/9/26 09:20
 **/
public class ColumnTitleMap {
    private Map<String, String> columnTitleMap = new HashMap<String, String>();
    private ArrayList<String> titleKeyList = new ArrayList<String> ();

    public ColumnTitleMap(String datatype) {
        switch (datatype) {
            case "stationInfo":
                initStationInfoCol();
                initStationInfoTitleKeyList();
                break;
            case "eququalityInfo":
                initEququalityInfoCol();
                initEququalityInfoTitleKeyList();
                break;
            case "equipmodelInfo":
                initEquipmodelInfoCol();
                initEquipmodelInfoTitleKeyList();
                break;
            case "eququalityImportInfo":
                eququalityImportInfoCol();
                eququalityImportInfoTitleKeyList();
                break;
            case "importEququalityResult":
                importEququalityResultCol();
                importEququalityResultTitleKeyList();
            case "importEquChartResult":
                importEquChartResultCol();
                importEquChartResultTitleKeyList();
            default:
                break;
        }
    }


    /**
     * mysql表需要导出字段--显示名称对应集合
     */
    private void initStationInfoCol() {
        columnTitleMap.put("id", "电站ID");
        columnTitleMap.put("stationName", "电站名称");
        columnTitleMap.put("stationStatus", "电站状态：0.空电站；1.电站正常；2.部分离线；3.全部离线");
        columnTitleMap.put("stationAlarm", "报警状态：0.无报警；1.有报警");
        columnTitleMap.put("stationSize", "装机容量（单位：kWp）");
        columnTitleMap.put("elecEfficiency", "发电功率（单位：kWp）");
        columnTitleMap.put("stationSysRate", "系统功率比");
        columnTitleMap.put("dayPowerhours", "当日发满小时（单位：h）");
        columnTitleMap.put("updateTime", "修改时间");
        columnTitleMap.put("stationCreatetime", "建站日期");
        columnTitleMap.put("stationGridstatus", "并网状态：0.未并网；1.已并网");
        columnTitleMap.put("stationType", "电站类型：1.分布式户用；2.分布式商业；3.分布式工业；4.地面电站");
        columnTitleMap.put("stationSystem", "电站系统");
        columnTitleMap.put("stationCompany", "安装公司");
    }

    /**
     * mysql表需要导出字段集
     */
    private void initStationInfoTitleKeyList() {
        titleKeyList.add("id");
        titleKeyList.add("stationName");
        titleKeyList.add("stationStatus");
        titleKeyList.add("stationAlarm");
        titleKeyList.add("stationSize");
        titleKeyList.add("elecEfficiency");
        titleKeyList.add("stationSysRate");
        titleKeyList.add("dayPowerhours");
        titleKeyList.add("updateTime");
        titleKeyList.add("stationCreatetime");
        titleKeyList.add("stationGridstatus");
        titleKeyList.add("stationType");
        titleKeyList.add("stationSystem");
        titleKeyList.add("stationCompany");
    }

    /**
     * mysql表需要导出字段--显示名称对应集合
     */
    private void initEququalityInfoCol() {
        columnTitleMap.put("equSn", "设备SN");
        columnTitleMap.put("equType", "设备类型");
        columnTitleMap.put("modelName", "设备型号");
        columnTitleMap.put("startDate", "发货日期");
        columnTitleMap.put("endDate", "质保期限");
        columnTitleMap.put("timeLimit", "质保时长");
        columnTitleMap.put("criticalStatus", "临期状态");
        columnTitleMap.put("deleteFlag", "删除标志");
        columnTitleMap.put("createTime", "创建时间");
        columnTitleMap.put("createUser", "创建用户");
        columnTitleMap.put("updateTime", "修改时间");
        columnTitleMap.put("updateUser", "修改用户");
        columnTitleMap.put("tenantId", "租户id");
    }

    /**
     * mysql表需要导出字段集
     */
    private void initEququalityInfoTitleKeyList() {
        titleKeyList.add("equSn");
        titleKeyList.add("equType");
        titleKeyList.add("modelName");
        titleKeyList.add("startDate");
        titleKeyList.add("endDate");
        titleKeyList.add("timeLimit");
        titleKeyList.add("criticalStatus");
        titleKeyList.add("updateTime");
        titleKeyList.add("deleteFlag");
        titleKeyList.add("createTime");
        titleKeyList.add("createUser");
        titleKeyList.add("updateTime");
        titleKeyList.add("updateUser");
        titleKeyList.add("tenantId");
    }

    /**
     * mysql表需要导出字段--显示名称对应集合
     */
    private void initEquipmodelInfoCol() {
        columnTitleMap.put("id", "设备型号ID");
        columnTitleMap.put("equType", "设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪");
        columnTitleMap.put("modelName", "型号名称");
        columnTitleMap.put("companyId", "厂家ID");
        columnTitleMap.put("ratedPower", "额定功率");
        columnTitleMap.put("detailType", "设备详细类型：0.单项；1.三项；2.单向表；4.双向表；5.棒式；6.导轨");
        columnTitleMap.put("mpptCount", "MPPT路数");
        columnTitleMap.put("batVolType", "电池电压类型：1.LV-48L");
        columnTitleMap.put("batType", "电池类型：1.铅酸");
        columnTitleMap.put("batMode", "电池模式：1.电压模式");
        columnTitleMap.put("deleteFlag", "删除标志");
        columnTitleMap.put("createTime", "创建时间");
        columnTitleMap.put("createUser", "创建用户");
        columnTitleMap.put("updateTime", "修改时间");
        columnTitleMap.put("updateUser", "修改用户");
        columnTitleMap.put("tenantId", "租户id");
    }

    /**
     * mysql表需要导出字段集
     */
    private void initEquipmodelInfoTitleKeyList() {
        titleKeyList.add("id");
        titleKeyList.add("equType");
        titleKeyList.add("modelName");
        titleKeyList.add("companyId");
        titleKeyList.add("ratedPower");
        titleKeyList.add("detailType");
        titleKeyList.add("mpptCount");
        titleKeyList.add("batVolType");
        titleKeyList.add("batType");
        titleKeyList.add("batMode");
        titleKeyList.add("deleteFlag");
        titleKeyList.add("createTime");
        titleKeyList.add("createUser");
        titleKeyList.add("updateTime");
        titleKeyList.add("updateUser");
        titleKeyList.add("tenantId");
    }

    /**
     * mysql表需要导出字段--显示名称对应集合
     */
    private void eququalityImportInfoCol() {
        columnTitleMap.put("equSn", "设备SN（必填）");
        columnTitleMap.put("equType", "设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪(导入请填对应数字！)");
        columnTitleMap.put("equModel", "设备型号");
        columnTitleMap.put("startDate", "发货日期（如：2023-10-01）");
//        columnTitleMap.put("endDate", "质保期限（如：2023-10-01）");
        columnTitleMap.put("timeLimit", "质保时长（单位：年；最多一位小数）");
//        columnTitleMap.put("criticalStatus", "临期状态 0：临期；1：正常（可不填）");

    }

    /**
     * mysql表需要导出字段集
     */
    private void eququalityImportInfoTitleKeyList() {
        titleKeyList.add("equSn");
        titleKeyList.add("equType");
        titleKeyList.add("equModel");
        titleKeyList.add("startDate");
//        titleKeyList.add("endDate");
        titleKeyList.add("timeLimit");
//        titleKeyList.add("criticalStatus");
    }

    /**
     * mysql表需要导出字段--显示名称对应集合
     */
    private void importEququalityResultCol() {
        columnTitleMap.put("rowNum", "所在行号");
        columnTitleMap.put("content", "错误内容");

    }

    /**
     * mysql表需要导出字段集
     */
    private void importEquChartResultTitleKeyList() {
        titleKeyList.add("time");
    }

    /**
     * mysql表需要导出字段--显示名称对应集合
     */
    private void importEquChartResultCol() {
        columnTitleMap.put("time", "时间");
    }

    /**
     * mysql表需要导出字段集
     */
    private void importEququalityResultTitleKeyList() {
        titleKeyList.add("rowNum");
        titleKeyList.add("content");
    }

    public Map<String, String> getColumnTitleMap() {
        return columnTitleMap;
    }

    public ArrayList<String> getTitleKeyList() {
        return titleKeyList;
    }
}
