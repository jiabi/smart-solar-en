package vip.xiaonuo.biz.modular.invinfo.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/28 17:42
 */

@Getter
@Setter
public class SkySnAndEquTypeParam {
    private String sn;

    private Integer equType;

    private String modelId;
}
