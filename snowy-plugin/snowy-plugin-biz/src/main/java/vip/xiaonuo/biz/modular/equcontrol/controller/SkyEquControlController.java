package vip.xiaonuo.biz.modular.equcontrol.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.equcontrol.param.*;
import vip.xiaonuo.biz.modular.equcontrol.service.SkyEquControlService;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @Author wangjian
 * @Date 2024/5/6 14:23
 */
@Api(tags = "设备控制控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyEquControlController {

    @Resource
    private SkyEquControlService skyEquControlService;

    /**
     * 参数读取
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("参数读取")
//    @SaCheckPermission("/biz/equControl/getParam")
    @PostMapping("/biz/equControl/getParam")
    public CommonResult<SkyIotParam<SkyIotReadResParam>> getParam(@RequestBody @Valid SkyReqParam<SkyReadParamDeviceInfoParam> skyReqParam) {
        return CommonResult.data(skyEquControlService.getParam(skyReqParam));
    }

    /**
     * 远程控制
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("远程控制")
//    @SaCheckPermission("/biz/equControl/control")
    @PostMapping("/biz/equControl/control")
    public CommonResult<SkyIotParam<SkyIotEquControlDevice>> equControl(@RequestBody @Valid SkyReqParam<SkyIotEquControlDevice<IotMqttKeyElecMeterParam>> skyEquControlReqParam) {
        SkyIotParam<SkyIotEquControlDevice> res = skyEquControlService.equControl(skyEquControlReqParam);
        return CommonResult.data(res);
    }

    /**
     * 远程控制-登录密码校验
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("远程控制-登录密码校验")
//    @SaCheckPermission("/biz/equControl/checkPassword")
    @PostMapping("/biz/equControl/checkPassword")
    public CommonResult<Boolean> checkPassword(@RequestBody @Valid CheckPasswordParam model) {

        return CommonResult.data(skyEquControlService.checkPassword(model));
    }
}
