package vip.xiaonuo.biz.modular.eququality.param;

import lombok.Data;

import java.util.Date;

@Data
public class EquQualityTripartiteVO {

    private Date manufacture;

    private Double period;

    private Date endDate;
}
