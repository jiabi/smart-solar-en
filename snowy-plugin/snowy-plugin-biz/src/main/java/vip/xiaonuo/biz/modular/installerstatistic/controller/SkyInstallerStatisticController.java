/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.installerstatistic.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.installerstatistic.task.InstallerStatisticTimerTaskRunner;
import vip.xiaonuo.biz.modular.stationpower.task.StationPowerTimerTaskRunner;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.installerstatistic.entity.SkyInstallerStatistic;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticAddParam;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticEditParam;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticIdParam;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticPageParam;
import vip.xiaonuo.biz.modular.installerstatistic.service.SkyInstallerStatisticService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 安装商数据统计控制器
 *
 * @author 全佳璧
 * @date  2024/03/13 09:19
 */
@Api(tags = "安装商数据统计控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyInstallerStatisticController {

    @Resource
    private SkyInstallerStatisticService skyInstallerStatisticService;

    /**
     * 获取安装商数据统计分页
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取安装商数据统计分页")
    @SaCheckPermission("/biz/installerstatistic/page")
    @GetMapping("/biz/installerstatistic/page")
    public CommonResult<Page<SkyInstallerStatistic>> page(SkyInstallerStatisticPageParam skyInstallerStatisticPageParam) {
        return CommonResult.data(skyInstallerStatisticService.page(skyInstallerStatisticPageParam));
    }

    /**
     * 添加安装商数据统计
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加安装商数据统计")
    @CommonLog("添加安装商数据统计")
    @SaCheckPermission("/biz/installerstatistic/add")
    @PostMapping("/biz/installerstatistic/add")
    public CommonResult<String> add(@RequestBody @Valid SkyInstallerStatisticAddParam skyInstallerStatisticAddParam) {
        skyInstallerStatisticService.add(skyInstallerStatisticAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑安装商数据统计
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑安装商数据统计")
    @CommonLog("编辑安装商数据统计")
    @SaCheckPermission("/biz/installerstatistic/edit")
    @PostMapping("/biz/installerstatistic/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyInstallerStatisticEditParam skyInstallerStatisticEditParam) {
        skyInstallerStatisticService.edit(skyInstallerStatisticEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除安装商数据统计
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除安装商数据统计")
    @CommonLog("删除安装商数据统计")
    @SaCheckPermission("/biz/installerstatistic/delete")
    @PostMapping("/biz/installerstatistic/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyInstallerStatisticIdParam> skyInstallerStatisticIdParamList) {
        skyInstallerStatisticService.delete(skyInstallerStatisticIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取安装商数据统计详情
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取安装商数据统计详情")
    @SaCheckPermission("/biz/installerstatistic/detail")
    @GetMapping("/biz/installerstatistic/detail")
    public CommonResult<SkyInstallerStatistic> detail(@Valid SkyInstallerStatisticIdParam skyInstallerStatisticIdParam) {
        return CommonResult.data(skyInstallerStatisticService.detail(skyInstallerStatisticIdParam));
    }

    @Resource
    private InstallerStatisticTimerTaskRunner installerStatisticTimerTaskRunner;
    @GetMapping("/biz/test")
    public void test(){
        installerStatisticTimerTaskRunner.action();
    }


}
