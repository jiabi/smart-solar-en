package vip.xiaonuo.biz.modular.equcontrol.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/5/8 13:20
 */
@Getter
@Setter
public class SkyIotReadResParam {
    /** 设备id **/
    private String id;

    /** 1：户用并网逆变；2：户储逆变器；3：电表；4：电池；5：采集器 **/
    private Integer type;

    private String sn;

    private String paramData;

    private String config;
}
