package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description:
 * @Author: wangjian
 * @CreateTime: 2023-11-15  14:51
 */
@Getter
@Setter
public class StationInfoOverviewPowerHisVO {
    /** 发电量 */
    @ApiModelProperty(value = "发电量")
    private String stationPower;

    /** 日期 */
    @ApiModelProperty(value = "日期")
    private Double datetime;
}