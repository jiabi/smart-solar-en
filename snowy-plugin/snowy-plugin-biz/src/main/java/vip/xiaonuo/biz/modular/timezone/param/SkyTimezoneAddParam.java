/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.timezone.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 地区时区添加参数
 *
 * @author 全佳璧
 * @date  2024/03/08 16:23
 **/
@Getter
@Setter
public class SkyTimezoneAddParam {

    /** 地区编码 */
    @ApiModelProperty(value = "地区编码", position = 3)
    private Integer addrCode;

    /** 中文名称 */
    @ApiModelProperty(value = "中文名称", position = 4)
    private String addrNameCn;

    /** 英文名称 */
    @ApiModelProperty(value = "英文名称", position = 5)
    private String addrNameEn;

    /** 上一层级区域ID */
    @ApiModelProperty(value = "上一层级区域ID", position = 6)
    private String belongToLevel;

    /** 中文时区 */
    @ApiModelProperty(value = "中文时区", position = 7)
    private String timezoneCn;

    /** 英文时区 */
    @ApiModelProperty(value = "英文时区", position = 8)
    private String timezoneEn;

    /** 时区偏移 */
    @ApiModelProperty(value = "时区偏移", position = 9)
    private String timezoneTimespan;

}
