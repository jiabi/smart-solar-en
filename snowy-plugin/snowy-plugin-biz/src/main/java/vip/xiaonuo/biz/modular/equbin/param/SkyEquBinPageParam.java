/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.equbin.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 设备升级包查询参数
 *
 * @author 全佳璧
 * @date  2024/04/17 19:40
 **/
@Getter
@Setter
public class SkyEquBinPageParam {

    /** 当前页 */
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    /** 每页条数 */
    @ApiModelProperty(value = "每页条数")
    private Integer size;

    /** 设备类型：1采集器；2并网逆变器；3储能逆变器 */
    @ApiModelProperty(value = "设备类型：1采集器；2并网逆变器；3储能逆变器")
    private Integer equType;

    /** 适用型号 */
    @ApiModelProperty(value = "适用型号")
    private String equModel;

    /** 适用模块：1采集器（wstk）、2主ARM（marm）、3辅ARM（sarm）、4DSP（mdsp）、5安规（sfty） */
    @ApiModelProperty(value = "适用模块：1采集器（wstk）、2主ARM（marm）、3辅ARM（sarm）、4DSP（mdsp）、5安规（sfty）")
    private Integer equModule;

    @ApiModelProperty(value = "适用模块：采集器wifi（wstk）、主ARM（marm）、辅ARM（sarm）、DSP（mdsp）、安规（sfty）、采集器4g（gstk）,前端只能传（）中的标识")
    private String equModuleName;


    /** 升级包名 */
    @ApiModelProperty(value = "升级包名")
    private String binName;

}
