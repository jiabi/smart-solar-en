package vip.xiaonuo.biz.modular.app.station.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import vip.xiaonuo.biz.modular.app.inv.param.AppStationInfoOverviewPowerVO;
import vip.xiaonuo.biz.modular.app.station.param.AppStationInfoOverviewVO;
import vip.xiaonuo.biz.modular.app.station.param.PsIdAndPowerInfoParam;
import vip.xiaonuo.biz.modular.app.station.param.UserInfoParam;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.param.PsInstallerAndUserReqParam;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoOverviewPowerVO;

import java.util.List;
import java.util.Map;

/**
 * @Author wangjian
 * @Date 2024/3/11 14:58
 */

@Mapper
@Repository
public interface AppStationInfoMapper extends BaseMapper<StationInfo> {
    AppStationInfoOverviewVO overviewInfo(@Param("stationCompany") String stationCompany);

    List<PsIdAndPowerInfoParam> getTodayPower(@Param("idList") List<String> idList,@Param("day") String day);

    AppStationInfoOverviewPowerVO getpowerbyPsId(@Param("id") String id,@Param("date") String date);


    List<UserInfoParam> getAllInstaller(PsInstallerAndUserReqParam param);

    List<UserInfoParam> getAllInstallerByUser(PsInstallerAndUserReqParam param);

    List<UserInfoParam> getAllUser(PsInstallerAndUserReqParam param);

    List<UserInfoParam> getInstallerUser(PsInstallerAndUserReqParam param);
}
