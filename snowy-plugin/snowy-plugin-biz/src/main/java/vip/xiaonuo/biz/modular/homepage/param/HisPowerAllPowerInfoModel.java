package vip.xiaonuo.biz.modular.homepage.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/2/27 14:02
 */

@Getter
@Setter
public class HisPowerAllPowerInfoModel {

    /** 时间类型 */
    private Integer timeType;

    /** 日期 */
    private String dateTime;

    /** 发电量 */
    private String stationPower;
}
