package vip.xiaonuo.biz.modular.dataview.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/7/10 11:22
 */
@Getter
@Setter
public class DataViewSocialCountParam {
    private Double powerDaily;
}
