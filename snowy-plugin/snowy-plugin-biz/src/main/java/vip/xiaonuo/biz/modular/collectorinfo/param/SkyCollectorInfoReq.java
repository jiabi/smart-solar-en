package vip.xiaonuo.biz.modular.collectorinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/8/7 17:51
 */
@Getter
@Setter
public class SkyCollectorInfoReq {
    /** 当前页 */
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    /** 每页条数 */
    @ApiModelProperty(value = "每页条数")
    private Integer size;

    /** 采集器sn */
    @ApiModelProperty(value = "采集器sn")
    private String collectorSn;

    /** 所属电站 */
    @ApiModelProperty(value = "所属电站")
    private String stationId;

    /** 逆变器sn */
    @ApiModelProperty(value = "逆变器sn")
    private String invSn;

    /** 安装公司id */
    @ApiModelProperty(value = "安装公司id")
    private String stationCompany;

    /** 业主id */
    @ApiModelProperty(value = "业主id")
    private String stationContactId;

    /** 分销商 */
    @ApiModelProperty(value = "分销商")
    private String distributor;

    public int getLimitStart() {
        return (getCurrent() - 1) * getSize();
    }

}
