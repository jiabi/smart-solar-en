package vip.xiaonuo.biz.modular.invmonitorcurrent.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @Author wangjian
 * @Date 2023/10/16 13:54
 **/
@Getter
@Setter
public class SkyInvMonitorHistoryParam {
    /** 设备SN */
    @ApiModelProperty(value = "设备SN", required = true)
    @NotBlank(message = "设备SN不能为空")
    private String invSn;

    /** 日期 */
    @ApiModelProperty(value = "年、年-月、年-月-日")
    private String dateTime;
}
