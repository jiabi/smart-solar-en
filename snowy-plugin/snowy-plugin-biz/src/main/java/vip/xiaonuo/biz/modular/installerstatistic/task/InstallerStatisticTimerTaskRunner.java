package vip.xiaonuo.biz.modular.installerstatistic.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.core.util.DoubleUtils;
import vip.xiaonuo.biz.modular.installerstatistic.entity.SkyInstallerStatistic;
import vip.xiaonuo.biz.modular.installerstatistic.mapper.SkyInstallerStatisticMapper;
import vip.xiaonuo.biz.modular.installerstatistic.param.InstallerStatisticEquInfoParam;
import vip.xiaonuo.biz.modular.installerstatistic.param.InstallerStatisticPsInfoParam;
import vip.xiaonuo.biz.modular.installerstatistic.service.SkyInstallerStatisticService;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.service.SkyStationPowerService;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.service.SkyStationUserService;
import vip.xiaonuo.common.timer.CommonTimerTaskRunner;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/4/1 9:45
 */

@Slf4j
@Component
public class InstallerStatisticTimerTaskRunner implements CommonTimerTaskRunner {

    @Resource
    private SkyStationPowerService stationPowerService;

    @Resource
    private SkyStationUserService skyStationUserService;

    @Resource
    private SkyInstallerStatisticMapper skyInstallerStatisticMapper;

    @Resource
    private SkyInstallerStatisticService skyInstallerStatisticService;

    @Resource
    private StationInfoService stationInfoService;

    @Override
    public void action() {
        LocalDateTime beginTime = LocalDateTime.now();
        log.info("定时任务-首页统计-开始：{}", beginTime);
        List<SkyStationUser> stationUsers = skyStationUserService.list().stream().filter(a -> a.getStationId() != null ).collect(Collectors.toList());

        // 分销商数据统计
        Map<String, List<SkyStationUser>> fenxiaoshangMap = stationUsers.stream().filter(a -> a.getInstallerIdL2() != null ).collect(Collectors.groupingBy(SkyStationUser::getInstallerIdL2));
        if (!CollectionUtils.isEmpty(fenxiaoshangMap)) {
            stationStatistic(fenxiaoshangMap);
        }

        // 安装商数据统计
        Map<String, List<SkyStationUser>> installerMap = stationUsers.stream().filter(a -> a.getInstallerIdL1() != null ).collect(Collectors.groupingBy(SkyStationUser::getInstallerIdL1));
        if (!CollectionUtils.isEmpty(installerMap)) {
            stationStatistic(installerMap);
        }

        // 用户数据统计
        Map<String, List<SkyStationUser>> userMap = stationUsers.stream().filter(a -> a.getUserId() != null ).collect(Collectors.groupingBy(SkyStationUser::getUserId));
        if (!CollectionUtils.isEmpty(userMap)) {
            stationStatistic(userMap);
        }
        LocalDateTime endTime = LocalDateTime.now();
        log.info("定时任务-首页统计-结束：{}。用时：{}毫秒", endTime , Duration.between(beginTime, endTime).toMillis());
    }

    /**
     * 数据统计
     * @param installerMap
     */
    public void stationStatistic(Map<String, List<SkyStationUser>> installerMap) {
        SkyInstallerStatistic orDefault = new SkyInstallerStatistic();
        orDefault.setStationTotal(0);
        orDefault.setStationNormal(0);
        orDefault.setStationAbnormal(0);
        orDefault.setStationOffline(0);
        orDefault.setPowerDaily(0D);
        orDefault.setPowerMonth(0D);
        orDefault.setPowerYear(0D);
        orDefault.setPowerTotal(0D);
        orDefault.setCurrentPower(0D);
        orDefault.setInstalledCapacity(0D);
        orDefault.setPowerRatio(0D);
        orDefault.setEquipTotal(0);
        orDefault.setEquipAbnormal(0);
        orDefault.setSaveCoal(0.0);
        orDefault.setReduceCo2(0.0);
        orDefault.setReduceSo2(0.0);
        orDefault.setEquivalentPlanting(0.0);

        installerMap.forEach((installerId, installList) ->{
            List<String> installPsIdList = installList.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(installPsIdList)) {
                orDefault.setInstallerId(installerId);
                skyInstallerStatisticService.updateById(orDefault);
                return;
            }
            QueryWrapper<SkyStationPower> installQW = new QueryWrapper<>();
            installQW.lambda().in(SkyStationPower::getStationId,installPsIdList).in(SkyStationPower::getTimeType,1,2,3,4,29);
            List<SkyStationPower> installPsPower = stationPowerService.list(installQW);
            Map<Integer, List<SkyStationPower>> collect = installPsPower.stream().collect(Collectors.groupingBy(SkyStationPower::getTimeType));
//            Map<String, Map<Integer, List<SkyStationPower>>> collect = installPsPower.stream().collect(Collectors.groupingBy(SkyStationPower::getStationId, Collectors.groupingBy(SkyStationPower::getTimeType)));
            SkyInstallerStatistic statistic = new SkyInstallerStatistic();
            // 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；
            // 5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
            // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；
            // 13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
            // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；
            // 21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
            // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
            // 29.发电功率（实时）
            statistic.setInstallerId(installerId);
            statistic.setStationTotal(installList.size());
            // 获取当前日期
            LocalDate currentDate = LocalDate.now();
            // 使用 DateTimeFormatter 格式化为年-月格式
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
            DateTimeFormatter formatterYear = DateTimeFormatter.ofPattern("yyyy");
            String formattedDate = currentDate.format(formatter);
            String formattedYear = currentDate.format(formatterYear);
            List<SkyStationPower> stationPowerList = collect.get(1);
            if(!CollectionUtils.isEmpty(stationPowerList)){
                List<SkyStationPower> collect2 = stationPowerList.stream().filter(v -> currentDate.toString().equals(v.getDateTime())).collect(Collectors.toList());
                List<Double> collect1 = stationPowerList.stream().filter(v -> currentDate.toString().equals(v.getDateTime())).map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList());
            }
            statistic.setPowerDaily(collect.containsKey(1) ? collect.get(1).stream().filter(v ->  currentDate.toString().equals(v.getDateTime())).map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList()).stream().reduce(Double::sum).orElse(0D) : 0D );
            statistic.setPowerMonth(collect.containsKey(2) ? collect.get(2).stream().filter(v ->  formattedDate.equals(v.getDateTime())).map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList()).stream().reduce(Double::sum).orElse(0D) : 0D );
            statistic.setPowerYear(collect.containsKey(3) ? collect.get(3).stream().filter(v ->  formattedYear.equals(v.getDateTime())).map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList()).stream().reduce(Double::sum).orElse(0D) : 0D );
            statistic.setPowerTotal(collect.containsKey(4) ? collect.get(4).stream().map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList()).stream().reduce(Double::sum).orElse(0D) : 0D );
//            statistic.setCurrentPower(collect.containsKey(29) ? collect.get(29).stream().map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList()).stream().reduce(Double::sum).orElse(0D) : 0D );
            List<InstallerStatisticPsInfoParam> psInfo = skyInstallerStatisticMapper.psStatisticsByInstallerPsId(installPsIdList);
            String nowFormat = LocalDateTime.now().minusMinutes(15L).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            List<InstallerStatisticEquInfoParam> psEqu = skyInstallerStatisticMapper.psDeviceByInstallerPsId(installPsIdList, nowFormat);
            if (psInfo != null){
                statistic.setStationNormal(Math.toIntExact(psInfo.stream().filter(s -> s.getStatus() == 1).count()));
                statistic.setStationAbnormal(Math.toIntExact(psInfo.stream().filter(s -> s.getStatus() == 2).count()));
                statistic.setStationOffline(psInfo.size() - statistic.getStationNormal() - statistic.getStationAbnormal());
                statistic.setPowerTotal(psInfo.stream().mapToDouble(InstallerStatisticPsInfoParam::getStationPower).sum());
                statistic.setInstalledCapacity(psInfo.stream().mapToDouble(InstallerStatisticPsInfoParam::getStationSize).sum());
                // 节约标准煤（吨）=0.000305*累计发电量（kWh），数据来自国家发改委
                //CO₂减排量（吨）=0.000793*累计发电量（kWh），数据来自国家生态环境部
                //等效植树量（棵）=累计发电量（kWh）*0.997/18.3，即1年内需要种多少棵树才能吸收掉CO₂减排量中的所有CO₂，数据来自IPCC
                //累计收入=度电收益*累计发电量（kWh）
                //SO₂减排量（吨）=0.000003*累计发电量（kWh）
                statistic.setSaveCoal(0.000305 * statistic.getPowerTotal());
                statistic.setReduceCo2(0.000793 * statistic.getPowerTotal());
                statistic.setReduceSo2(0.000003 * statistic.getPowerTotal());
                statistic.setEquivalentPlanting(statistic.getPowerTotal() * 0.997 / 18.3);
            } else {
                statistic.setStationNormal(0);
                statistic.setStationAbnormal(0);
                statistic.setStationOffline(0);
                statistic.setPowerTotal(0D);
                statistic.setInstalledCapacity(0D);
                statistic.setSaveCoal(0D);
                statistic.setReduceCo2(0D);
                statistic.setReduceSo2(0D);
                statistic.setEquivalentPlanting(0D);
            }
            if (psEqu != null) {
                statistic.setPowerRatio(statistic.getInstalledCapacity() == 0D ? 0D : DoubleUtils.round((psEqu.stream().mapToDouble(InstallerStatisticEquInfoParam::getMonPpv).sum() / statistic.getInstalledCapacity() * 100),2));
                statistic.setEquipTotal(psEqu.size());
                statistic.setEquipAbnormal(Math.toIntExact(psEqu.stream().filter(s -> s.getMonSate() == 2).count()));
                statistic.setCurrentPower(DoubleUtils.round((psEqu.stream().mapToDouble(InstallerStatisticEquInfoParam::getMonPpv).sum()),2));
            } else {
                statistic.setPowerRatio(0D);
                statistic.setEquipTotal(0);
                statistic.setEquipAbnormal(0);
                statistic.setCurrentPower(0D);
            }
            skyInstallerStatisticService.saveOrUpdate(statistic);
        });
    }

//    /**
//     * 用户数据统计
//     * @param userMap
//     */
//    private void userStatistic(Map<String, List<SkyStationUser>> userMap) {
//        userMap.forEach((userId, userList) ->{
//            List<String> userPsIdList = userList.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
//            QueryWrapper<SkyStationPower> installQW = new QueryWrapper<>();
//            installQW.lambda().in(SkyStationPower::getStationId,userPsIdList);
//            List<SkyStationPower> installPsPower = stationPowerService.list(installQW);
//            Map<String, Map<Integer, List<SkyStationPower>>> collect = installPsPower.stream().collect(Collectors.groupingBy(SkyStationPower::getStationId, Collectors.groupingBy(SkyStationPower::getTimeType)));
//            SkyInstallerStatistic statistic = new SkyInstallerStatistic();
//            // 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；
//            // 5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
//            // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；
//            // 13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
//            // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；
//            // 21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
//            // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
//            // 29.发电功率（实时）
//            statistic.setInstallerId(userId);
//            statistic.setStationTotal(userList.size());
//            statistic.setPowerDaily(collect.containsKey(userId) ? collect.get(userId).containsKey(1) ? collect.get(userId).get(1).stream().map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList()).stream().reduce(Double::sum).orElse(0D) : 0D : 0D);
//            statistic.setPowerMonth(collect.containsKey(userId) ? collect.get(userId).containsKey(2) ? collect.get(userId).get(2).stream().map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList()).stream().reduce(Double::sum).orElse(0D) : 0D : 0D);
//            statistic.setPowerYear(collect.containsKey(userId) ? collect.get(userId).containsKey(3) ? collect.get(userId).get(3).stream().map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList()).stream().reduce(Double::sum).orElse(0D) : 0D : 0D);
//            statistic.setPowerTotal(collect.containsKey(userId) ? collect.get(userId).containsKey(4) ? collect.get(userId).get(4).stream().map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList()).stream().reduce(Double::sum).orElse(0D) : 0D : 0D);
//            statistic.setCurrentPower(collect.containsKey(userId) ? collect.get(userId).containsKey(29) ? collect.get(userId).get(29).stream().map(a -> Double.valueOf(a.getStationPower())).collect(Collectors.toList()).stream().reduce(Double::sum).orElse(0D) : 0D : 0D);
//
//            List<InstallerStatisticPsInfoParam> psInfo = skyInstallerStatisticMapper.psStatisticsByInstallerPsId(userPsIdList);
//            List<InstallerStatisticEquInfoParam> psEqu = skyInstallerStatisticMapper.psDeviceByInstallerPsId(userPsIdList);
//            if (psInfo != null){
//                statistic.setStationNormal(Math.toIntExact(psInfo.stream().filter(s -> s.getStatus() == 1).count()));
//                statistic.setStationAbnormal(Math.toIntExact(psInfo.stream().filter(s -> s.getStatus() == 2).count()));
//                statistic.setStationOffline(psInfo.size() - statistic.getStationNormal() - statistic.getStationAbnormal());
//                statistic.setPowerTotal(psInfo.stream().mapToDouble(InstallerStatisticPsInfoParam::getStationPower).sum());
//                statistic.setInstalledCapacity(psInfo.stream().mapToDouble(InstallerStatisticPsInfoParam::getStationSize).sum());
//                // 节约标准煤（吨）=0.000305*累计发电量（kWh），数据来自国家发改委
//                //CO₂减排量（吨）=0.000793*累计发电量（kWh），数据来自国家生态环境部
//                //等效植树量（棵）=累计发电量（kWh）*0.997/18.3，即1年内需要种多少棵树才能吸收掉CO₂减排量中的所有CO₂，数据来自IPCC
//                //累计收入=度电收益*累计发电量（kWh）
//                //SO₂减排量（吨）=0.000003*累计发电量（kWh）
//                statistic.setSaveCoal(0.000305 * statistic.getPowerTotal());
//                statistic.setReduceCo2(0.000793 * statistic.getPowerTotal());
//                statistic.setReduceSo2(0.000003 * statistic.getPowerTotal());
//                statistic.setEquivalentPlanting(statistic.getPowerTotal() * 0.997 / 18.3);
//            } else {
//                statistic.setStationNormal(0);
//                statistic.setStationAbnormal(0);
//                statistic.setStationOffline(0);
//                statistic.setPowerTotal(0D);
//                statistic.setInstalledCapacity(0D);
//                statistic.setSaveCoal(0D);
//                statistic.setReduceCo2(0D);
//                statistic.setReduceSo2(0D);
//                statistic.setEquivalentPlanting(0D);
//            }
//            if (psEqu != null) {
//                statistic.setPowerRatio(statistic.getInstalledCapacity() == 0 ? 0 : (psEqu.stream().mapToDouble(InstallerStatisticEquInfoParam::getMonPac).sum() / statistic.getInstalledCapacity() * 100));
//                statistic.setEquipTotal(psEqu.size());
//                statistic.setEquipAbnormal(Math.toIntExact(psEqu.stream().filter(s -> s.getMonSate() == 2).count()));
//            } else {
//                statistic.setPowerRatio(0D);
//                statistic.setEquipTotal(0);
//                statistic.setEquipAbnormal(0);
//            }
//            skyInstallerStatisticMapper.insert(statistic);
//
//        });
//    }
}
