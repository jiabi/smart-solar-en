package vip.xiaonuo.biz.core.util.easyexcel;

import cn.hutool.core.exceptions.UtilException;
import com.aliyun.oss.ServiceException;
import org.springframework.util.Assert;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.LocalDateUtils;

import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/8 10:22
 */
public class ExportUtil {
    /**
     * 校验时间，开始-结束时间必填，并且时间区间小于等于31天
     * @param startTime
     */
    public static void checkTime(LocalDateTime startTime, LocalDateTime endTime){
        Assert.notNull(startTime, "导出需要指定开始日期");
        Assert.notNull(endTime, "导出需要指定结束日期");
        //开始时间+31天需要大于结束时间
        int to = startTime.toLocalDate().plusDays(31).compareTo(endTime.toLocalDate());
        Assert.isTrue(to>=0,"时间范围一个月内");
    }
    /**
     * 编码文件名称
     */
    public static String translateFileName(String fileName) {
        String name;
        try {
            name = URLEncoder.encode(fileName + LocalDateUtils.formatLocalDateTime(LocalDateUtils.getCurrentLocalDateTime(), LocalDateUtils.DATETIME_FORMATTER1), "UTF-8");
        } catch (Exception e) {
            throw new UtilException("文件名称编码失败");
        }
        return name;
    }

    /**
     * 编码文件名称 -不加日期
     */
    public static String encodeFileName(String fileName) {
        String name;
        try {
            name = URLEncoder.encode(fileName, "UTF-8");
        } catch (Exception e) {
            throw new UtilException("文件名称编码失败");
        }
        return name;
    }

    /**
     * 校验最大,最小导入数量
     * 设置每条行号
     *
     * @param excelList
     */
    public static List<? extends EasyExcelRowBase> pubCheck(List<? extends EasyExcelRowBase> excelList, int min, int max) {
        if (excelList.size() < min) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        if (excelList.size() > max) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_LENGTH_OUT_3000_9109);
        }
        for (int i = 0; i < excelList.size(); i++) {
            excelList.get(i).setRow(i + 2);
        }
        return excelList;
    }
}
