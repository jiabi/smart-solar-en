/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.timezone.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.timezone.entity.SkyTimezone;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezoneAddParam;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezoneEditParam;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezoneIdParam;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezonePageParam;

import java.util.List;

/**
 * 地区时区Service接口
 *
 * @author 全佳璧
 * @date  2024/03/08 16:23
 **/
public interface SkyTimezoneService extends IService<SkyTimezone> {

    /**
     * 获取地区时区分页
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     */
    Page<SkyTimezone> page(SkyTimezonePageParam skyTimezonePageParam);

    /**
     * 添加地区时区
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     */
    void add(SkyTimezoneAddParam skyTimezoneAddParam);

    /**
     * 编辑地区时区
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     */
    void edit(SkyTimezoneEditParam skyTimezoneEditParam);

    /**
     * 删除地区时区
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     */
    void delete(List<SkyTimezoneIdParam> skyTimezoneIdParamList);

    /**
     * 获取地区时区详情
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     */
    SkyTimezone detail(SkyTimezoneIdParam skyTimezoneIdParam);

    /**
     * 获取地区时区详情
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     **/
    SkyTimezone queryEntity(String id);

    /**
     * 获取地区时区详情
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     **/
    String queryByAddrCode(int addrCode);

}
