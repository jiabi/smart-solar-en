package vip.xiaonuo.biz.modular.stationinfo.param;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/4 10:55
 */

@Getter
@Setter
public class PsInvStatisticsVO {

    /** 设备数量 */
    private Integer equNum;

    /** 设备正常数量 */
    private Integer equNormalNum;

    /** 设备异常数量 */
    private Integer equAbnormalNum;

    private List<PsInvStatisticsParam> psInvStatisticsParamList;

}
