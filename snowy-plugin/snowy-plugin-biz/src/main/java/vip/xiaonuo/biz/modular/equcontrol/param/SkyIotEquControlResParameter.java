package vip.xiaonuo.biz.modular.equcontrol.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/5/7 11:26
 */
@Getter
@Setter
public class SkyIotEquControlResParameter {
    private String key;
    private String value;
    /** 1-成功 0失败 **/
    private Integer status;
}
