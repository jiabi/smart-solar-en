/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.equipmodel.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.equcontrol.param.*;
import vip.xiaonuo.biz.modular.equcontrol.param.SetSTKConfigParam;
import vip.xiaonuo.biz.modular.equipmodel.param.*;
import vip.xiaonuo.biz.modular.eququality.param.SkyEquQualityExportParam;
import vip.xiaonuo.biz.modular.equrelationship.entity.SkyEquRelationship;
import vip.xiaonuo.biz.modular.equrelationship.mapper.SkyEquRelationshipMapper;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.stationequ.entity.SkyStationEqu;
import vip.xiaonuo.biz.modular.stationequ.mapper.SkyStationEquMapper;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoExportVO;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoPageVO;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.equipmodel.entity.SkyEquipModel;
import vip.xiaonuo.biz.modular.equipmodel.mapper.SkyEquipModelMapper;
import vip.xiaonuo.biz.modular.equipmodel.service.SkyEquipModelService;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 设备型号Service接口实现类
 *
 * @author 全佳璧
 * @date  2023/09/25 14:15
 **/
@Service
@Slf4j
public class SkyEquipModelServiceImpl extends ServiceImpl<SkyEquipModelMapper, SkyEquipModel> implements SkyEquipModelService {

    @Resource
    private SkyInvInfoMapper skyInvInfoMapper;
    @Resource
    private SkyEquRelationshipMapper skyEquRelationshipMapper;

    @Resource
    private SkyStationEquMapper skyStationEquMapper;

    @Resource
    private SkyEquipModelMapper skyEquipModelMapper;

    @Resource
    private IotProperties iotProperties;

    @Resource
    private RestTemplate restTemplate;
    @Override
    public Page<SkyEquipModel> page(SkyEquipModelPageParam skyEquipModelPageParam) {
        QueryWrapper<SkyEquipModel> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(skyEquipModelPageParam.getEquType())) {
            queryWrapper.lambda().eq(SkyEquipModel::getEquType, skyEquipModelPageParam.getEquType());
        }
        if(ObjectUtil.isNotEmpty(skyEquipModelPageParam.getModelName())) {
            queryWrapper.lambda().like(SkyEquipModel::getModelName, skyEquipModelPageParam.getModelName());
        }
        if(ObjectUtil.isNotEmpty(skyEquipModelPageParam.getCompanyId())) {
            queryWrapper.lambda().eq(SkyEquipModel::getCompanyId, skyEquipModelPageParam.getCompanyId());
        }
        if(ObjectUtil.isNotEmpty(skyEquipModelPageParam.getDetailType())) {
            queryWrapper.lambda().eq(SkyEquipModel::getDetailType, skyEquipModelPageParam.getDetailType());
        }
//        if(ObjectUtil.isAllNotEmpty(skyEquipModelPageParam.getSortField(), skyEquipModelPageParam.getSortOrder())) {
//            CommonSortOrderEnum.validate(skyEquipModelPageParam.getSortOrder());
//            queryWrapper.orderBy(true, skyEquipModelPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
//                    StrUtil.toUnderlineCase(skyEquipModelPageParam.getSortField()));
//        } else {
//            queryWrapper.lambda().orderByAsc(SkyEquipModel::getId);
//        }
        queryWrapper.lambda().orderByAsc(SkyEquipModel::getId);
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyEquipModelAddParam skyEquipModelAddParam) {
        SkyEquipModel skyEquipModel = BeanUtil.toBean(skyEquipModelAddParam, SkyEquipModel.class);
        skyEquipModel.setId(skyEquipModelAddParam.getModelName());
        this.save(skyEquipModel);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyEquipModelEditParam skyEquipModelEditParam) {

        if (!skyEquipModelEditParam.getId().equals(skyEquipModelEditParam.getModelName())) {
            skyStationEquMapper.update(null,new UpdateWrapper<SkyStationEqu>().lambda()
                    .set(SkyStationEqu::getModelId, skyEquipModelEditParam.getModelName())
                    .eq(SkyStationEqu::getModelId, skyEquipModelEditParam.getId()));

            skyInvInfoMapper.update(null,new UpdateWrapper<SkyInvInfo>().lambda()
                    .set(SkyInvInfo::getModelId, skyEquipModelEditParam.getModelName())
                    .eq(SkyInvInfo::getModelId, skyEquipModelEditParam.getId()));

            // sky_meter_info、sky_collector_info
        }


        SkyEquipModel skyEquipModel = this.queryEntity(skyEquipModelEditParam.getId());
        BeanUtil.copyProperties(skyEquipModelEditParam, skyEquipModel);
        this.updateById(skyEquipModel);
        skyEquipModelMapper.update(null, new UpdateWrapper<SkyEquipModel>().lambda()
                .set(SkyEquipModel::getId, skyEquipModelEditParam.getModelName()).eq(SkyEquipModel::getId, skyEquipModelEditParam.getId()));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyEquipModelIdParam> skyEquipModelIdParamList) {
        List<String> equipModelIds = CollStreamUtil.toList(skyEquipModelIdParamList, SkyEquipModelIdParam::getId);

        QueryWrapper<SkyInvInfo> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().in(SkyInvInfo::getModelId,equipModelIds);
        boolean exists = skyInvInfoMapper.exists(queryWrapper);
        if (exists) {
            throw new CommonException("存在设备绑定待删除型号，请先解绑设备！");
        }

        // 执行删除
        this.removeByIds(equipModelIds);
    }

    @Override
    public SkyEquipModel detail(SkyEquipModelIdParam skyEquipModelIdParam) {
        return this.queryEntity(skyEquipModelIdParam.getId());
    }

    @Override
    public SkyEquipModel queryEntity(String id) {
        SkyEquipModel skyEquipModel = this.getById(id);
        if(ObjectUtil.isEmpty(skyEquipModel)) {
            throw new CommonException("设备型号不存在，id值为：{}", id);
        }
        return skyEquipModel;
    }

    @Override
    public List<Map<String, Object>> exportInfoList(SkyEquipModelExportParam exportParam) {
        ArrayList<Map<String,Object>> list = new ArrayList<>();
        SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (exportParam!=null && !CollectionUtils.isEmpty(exportParam.getIds()) && exportParam.getIds().size() > 0) {
            QueryWrapper<SkyEquipModel> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().in(SkyEquipModel::getId, exportParam.getIds());
            queryWrapper.lambda().orderByDesc(SkyEquipModel::getCreateTime);
            List<SkyEquipModel> infoList = this.list(queryWrapper);
            for (SkyEquipModel info : infoList) {
                SkyEquipModelExportVO vo = new SkyEquipModelExportVO();
                BeanUtil.copyProperties(info, vo);
                if (!Objects.isNull(info.getCreateTime())) {
                    vo.setCreateTime(sdfTime.format(info.getCreateTime()));
                }
                if (!Objects.isNull(info.getUpdateTime())) {
                    vo.setUpdateTime(sdfTime.format(info.getUpdateTime()));
                }
                Map<String, Object> map = JSON.parseObject(JSON.toJSONString(vo));
                list.add(map);
            }
            return list;
        }

        QueryWrapper<SkyEquipModel> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(exportParam.getEquType())) {
            queryWrapper.lambda().eq(SkyEquipModel::getEquType, exportParam.getEquType());
        }
        if(ObjectUtil.isNotEmpty(exportParam.getModelName())) {
            queryWrapper.lambda().like(SkyEquipModel::getModelName, exportParam.getModelName());
        }
        if(ObjectUtil.isNotEmpty(exportParam.getCompanyId())) {
            queryWrapper.lambda().eq(SkyEquipModel::getCompanyId, exportParam.getCompanyId());
        }
        if(ObjectUtil.isNotEmpty(exportParam.getDetailType())) {
            queryWrapper.lambda().eq(SkyEquipModel::getDetailType, exportParam.getDetailType());
        }
        queryWrapper.lambda().orderByAsc(SkyEquipModel::getId);
        List<SkyEquipModel> infoList = this.list(queryWrapper);
        for (SkyEquipModel info : infoList) {
            SkyEquipModelExportVO vo = new SkyEquipModelExportVO();
            BeanUtil.copyProperties(info, vo);
            if (!Objects.isNull(info.getCreateTime())) {
                vo.setCreateTime(sdfTime.format(info.getCreateTime()));
            }
            if (!Objects.isNull(info.getUpdateTime())) {
                vo.setUpdateTime(sdfTime.format(info.getUpdateTime()));
            }
            Map<String, Object> map = JSON.parseObject(JSON.toJSONString(vo));
            list.add(map);
        }
        return list;
    }

    @Override
    public List<SkyEquipModel> getEquipModelList(Integer equType) {
        QueryWrapper<SkyEquipModel> eq = new QueryWrapper<>();
        if (equType != null) {
            eq.lambda().eq(SkyEquipModel::getEquType, equType);
        }
        List<SkyEquipModel> skyEquipModelList = this.list(eq);
        if(!CollectionUtils.isEmpty(skyEquipModelList)){
            skyEquipModelList.stream().forEach(skyEquipModel -> {
                skyEquipModel.setId(skyEquipModel.getModelName());
            });
        }
        return skyEquipModelList;
    }

    @Override
    public SkyIotParam<SkyIotReadResParam> collectorRed(SkyReqParam<SkyReadParamDeviceInfoParam> skyReqParam){
        List<SkyReadParamDeviceInfoParam> deviceList = skyReqParam.getDeviceList();
        if (CollectionUtils.isEmpty(deviceList)) {
            return null;
        }
        SkyIotParam<SkyReadParamDeviceInfoIotParam> iotParam = new SkyIotParam();
        List<SkyReadParamDeviceInfoIotParam> deviceList2Iot = new ArrayList<>();
        deviceList.stream().forEach(device->{
            //采集器sn
            if (ObjectUtil.isEmpty(device.getSn())) {
                throw new SkyCommonException(SkyCommonExceptionEnum.SN_COLLECTOR_NOT_NULL_9107);
            }
            SkyReadParamDeviceInfoIotParam deviceIot = new SkyReadParamDeviceInfoIotParam();
            BeanUtil.copyProperties(device,deviceIot);

            SkyEquRelationship relationship = skyEquRelationshipMapper.selectOne(new QueryWrapper<SkyEquRelationship>().lambda().eq(SkyEquRelationship::getCollectorSn, device.getSn()));
            if (ObjectUtil.isEmpty(relationship)) {
                throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
            }
            deviceIot.setSn(relationship.getEquSn());
            deviceIot.setId(device.getSn());
            List<String> paramList = new ArrayList<>(device.getParamList());
            deviceIot.setParamList(paramList);
            deviceList2Iot.add(deviceIot);
        });
        iotParam.setSendTime(LocalDateTime.now().format(LocalDateUtils.DATETIME_FORMATTER));
        iotParam.setRequestId(String.valueOf(System.currentTimeMillis()));
        iotParam.setDeviceList(deviceList2Iot);
        // 调用iot接口
        log.info("调用iot读数据-请求参数：{}" , JSON.toJSONString(iotParam));
        MultiValueMap<String,Object> requestEntity = new LinkedMultiValueMap<>();
        requestEntity.add("topic",iotProperties.getTopicPrefix() + deviceList.get(0).getSn() + iotProperties.getReadMethod());
        requestEntity.add("msg",iotParam);
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        // 设置连接超时时间为15秒
        requestFactory.setConnectTimeout(15000);
        // 设置读取超时时间为15秒
        requestFactory.setReadTimeout(15000);
        restTemplate.setRequestFactory(requestFactory);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(iotProperties.getEquControlReadUrl(), HttpMethod.POST, new HttpEntity<>(requestEntity), String.class);
        } catch (RestClientException e) {
            log.info("调用iot读数据-连接超时");
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        log.info("调用iot读数据-返回数据：{}" , response.getBody());
        return JSON.parseObject(response.getBody(), new ParameterizedTypeReference<SkyIotParam<SkyIotReadResParam>>() {}.getType());
    }

    @Override
    public SkyIotParam<SkyIotEquControlDevice> collectorControl(SkyReqParam<SkyIotEquControlDevice<SetSTKConfigParam>> skyReqParam){
        skyReqParam.setSendTime(LocalDateTime.now().format(LocalDateUtils.DATETIME_FORMATTER));
        skyReqParam.setRequestId(String.valueOf(System.currentTimeMillis()));
        // 调用iot接口
        log.info("参数设置-请求参数：{}" , JSON.toJSONString(skyReqParam));
        MultiValueMap<String,Object> requestEntity = new LinkedMultiValueMap<>();

        List<SkyIotEquControlDevice<SetSTKConfigParam>> deviceList = skyReqParam.getDeviceList();
        if (CollectionUtils.isEmpty(deviceList)) {
            return null;
        }
        SkyEquRelationship relationship = skyEquRelationshipMapper.selectOne(new QueryWrapper<SkyEquRelationship>().lambda().eq(SkyEquRelationship::getCollectorSn, deviceList.get(0).getSn()));
        if (ObjectUtil.isEmpty(relationship)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        deviceList.stream().forEach(device->{
            device.setSn(relationship.getEquSn());
            device.setId(relationship.getCollectorSn());
        });

        requestEntity.add("topic",iotProperties.getTopicPrefix() + deviceList.get(0).getId() + iotProperties.getWriteMethod());
        requestEntity.add("msg",skyReqParam);
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        // 设置连接超时时间为15秒
        requestFactory.setConnectTimeout(15000);
        // 设置读取超时时间为15秒
        requestFactory.setReadTimeout(15000);
        restTemplate.setRequestFactory(requestFactory);
        ResponseEntity<String> response = null;
        try {
            log.info("调用iot参数设置-请求参数：{}" , JSON.toJSONString(requestEntity));
            response = restTemplate.exchange(iotProperties.getEquControlWriteUrl(), HttpMethod.POST, new HttpEntity<>(requestEntity), String.class);
        } catch (RestClientException e) {
            log.info("调用iot参数设置-连接超时");
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        log.info("调用iot参数设置-返回数据：{}" , response.getBody());
        String body = response.getBody().replaceAll("null", "");
        return JSON.parseObject(body, new ParameterizedTypeReference<SkyIotParam<SkyIotEquControlDevice>>() {}.getType());
    }
}
