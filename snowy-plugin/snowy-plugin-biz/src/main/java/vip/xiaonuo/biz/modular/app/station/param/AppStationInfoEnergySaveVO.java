package vip.xiaonuo.biz.modular.app.station.param;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author: wangjian
 * @date: 2023/11/14 17:53
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AppStationInfoEnergySaveVO {

    /** 节约用煤（吨） */
    private Double saveCoal;

    /** 减少CO2排放（吨） */
    private Double reduceCo2;

    /** 减少SO2排放（吨） */
    private Double reduceSo2;

    /** 等效植树量（棵） */
    private Double equivalentPlanting;
}
