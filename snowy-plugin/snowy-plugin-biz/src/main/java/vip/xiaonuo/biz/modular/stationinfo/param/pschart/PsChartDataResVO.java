package vip.xiaonuo.biz.modular.stationinfo.param.pschart;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/4 17:05
 */

@Getter
@Setter
public class PsChartDataResVO {
    /**
     * 日期集合
     */
    private List<String> dateList;

    /**
     * 发电量集合
     */
    private List<Double> power;

    /**
     * 用电量集合
     */
    private List<Double> energy;

    /**
     * 并网电量集合
     */
    private List<Double> GridEnergy;

    /**
     * 购电量集合
     */
    private List<Double> GridPurchase;

    /**
     * 充电量集合
     */
    private List<Double> charging;

    /**
     * 放电量集合
     */
    private List<Double> discharging;

    /**
     * 日数据集合
     */
    private List<PsChartDayDataParam> dayDataList;

    /**
     * 直流发电功率集合
     */
    private List<Double> inPower;
}
