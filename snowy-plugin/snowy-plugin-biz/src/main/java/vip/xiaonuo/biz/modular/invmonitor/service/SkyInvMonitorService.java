/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invmonitor.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.invmonitor.entity.SkyInvMonitor;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorAddParam;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorEditParam;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorIdParam;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorPageParam;

import java.util.List;

/**
 * 逆变器历史数据Service接口
 *
 * @author 全佳璧
 * @date  2023/10/23 09:44
 **/
public interface SkyInvMonitorService extends IService<SkyInvMonitor> {

    /**
     * 获取逆变器历史数据分页
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     */
    Page<SkyInvMonitor> page(SkyInvMonitorPageParam skyInvMonitorPageParam);

    /**
     * 添加逆变器历史数据
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     */
    void add(SkyInvMonitorAddParam skyInvMonitorAddParam);

    /**
     * 编辑逆变器历史数据
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     */
    void edit(SkyInvMonitorEditParam skyInvMonitorEditParam);

    /**
     * 删除逆变器历史数据
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     */
    void delete(List<SkyInvMonitorIdParam> skyInvMonitorIdParamList);

    /**
     * 获取逆变器历史数据详情
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     */
    SkyInvMonitor detail(SkyInvMonitorIdParam skyInvMonitorIdParam);

    /**
     * 获取逆变器历史数据详情
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     **/
    SkyInvMonitor queryEntity(String id);

}
