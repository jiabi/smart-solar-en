/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.installerstatistic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.installerstatistic.entity.SkyInstallerStatistic;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticAddParam;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticEditParam;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticIdParam;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticPageParam;

import java.util.List;

/**
 * 安装商数据统计Service接口
 *
 * @author 全佳璧
 * @date  2024/03/13 09:19
 **/
public interface SkyInstallerStatisticService extends IService<SkyInstallerStatistic> {

    /**
     * 获取安装商数据统计分页
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    Page<SkyInstallerStatistic> page(SkyInstallerStatisticPageParam skyInstallerStatisticPageParam);

    /**
     * 添加安装商数据统计
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    void add(SkyInstallerStatisticAddParam skyInstallerStatisticAddParam);

    /**
     * 编辑安装商数据统计
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    void edit(SkyInstallerStatisticEditParam skyInstallerStatisticEditParam);

    /**
     * 删除安装商数据统计
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    void delete(List<SkyInstallerStatisticIdParam> skyInstallerStatisticIdParamList);

    /**
     * 获取安装商数据统计详情
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    SkyInstallerStatistic detail(SkyInstallerStatisticIdParam skyInstallerStatisticIdParam);

    /**
     * 获取安装商数据统计详情
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     **/
    SkyInstallerStatistic queryEntity(String id);

    void recountStatisticById(String id);

}
