package vip.xiaonuo.biz.modular.dataview.param;

import lombok.Data;

import java.util.List;

@Data
public class AreaStationProvinceCountVO {

    /**
     * 电站设备数量信息
     */
    private List<AreaStationProvinceVO> areaStationProvinces;

    /**
     * 设备总数
     */
    private Integer equTotalNumber;
}
