package vip.xiaonuo.biz.modular.eququality.param;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2023/9/28 09:01
 **/
@Getter
@Setter
public class SkyEquQualityExcelVO {
    /** 设备SN */
    @ExcelProperty(value = "设备SN")
    @ApiModelProperty(value = "设备SN", position = 1)
    private String equSn;

    /** 设备类型 */
    @ExcelProperty(value = "设备类型")
    @ApiModelProperty(value = "设备类型", position = 2)
    private Integer equType;

    /** 型号id */
    @ExcelProperty(value = "型号id")
    @ApiModelProperty(value = "型号id", position = 3)
    private String equModel;

    /** 型号名称 */
    @ExcelProperty(value = "型号名称")
    @ApiModelProperty(value = "型号名称", position = 3)
    private String modelName;

    /** 发货日期 */
    @ExcelProperty(value = "发货日期")
    @ApiModelProperty(value = "发货日期", position = 4)
    private Date startDate;

    /** 质保期限 */
    @ExcelProperty(value = "质保期限")
    @ApiModelProperty(value = "质保期限", position = 5)
    private Date endDate;

    /** 质保时长 */
    @ExcelProperty(value = "质保时长")
    @ApiModelProperty(value = "质保时长", position = 6)
    private Double timeLimit;

    /** 临期状态 */
    @ExcelProperty(value = "临期状态")
    @ApiModelProperty(value = "临期状态", position = 7)
    private Integer criticalStatus;

    /** 删除标志 */
    @ExcelProperty(value = "删除标志")
    @ApiModelProperty(value = "删除标志", position = 8)
    @ExcelIgnore
    private String deleteFlag;

    /** 创建时间 */
    @ExcelProperty(value = "创建时间")
    @ApiModelProperty(value = "创建时间", position = 9)
    @TableField(fill = FieldFill.INSERT)
    @ExcelIgnore
    private Date createTime;

    /** 创建用户 */
    @ExcelProperty(value = "创建用户")
    @ApiModelProperty(value = "创建用户", position = 10)
    @ExcelIgnore
    private String createUser;

    /** 修改时间 */
    @ExcelProperty(value = "修改时间")
    @ApiModelProperty(value = "修改时间", position = 11)
    @ExcelIgnore
    private Date updateTime;

    /** 修改用户 */
    @ExcelProperty(value = "修改用户")
    @ApiModelProperty(value = "修改用户", position = 12)
    @ExcelIgnore
    private String updateUser;

    /** 租户id */
    @ExcelProperty(value = "租户id")
    @ApiModelProperty(value = "租户id", position = 13)
    private String tenantId;
}
