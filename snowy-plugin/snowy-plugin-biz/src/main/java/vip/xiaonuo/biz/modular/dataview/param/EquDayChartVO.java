package vip.xiaonuo.biz.modular.dataview.param;

import lombok.Data;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.PsChartDayDataParam;

import java.util.List;

@Data
public class EquDayChartVO {

    /**
     * 当日发电量总数
     */
    private List<Double> powers;

    /**
     * 日期集合
     */
    private List<String> dateList;


    /**
     * 总发电量
     */
    private Double powerTotal;

}
