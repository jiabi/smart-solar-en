package vip.xiaonuo.biz.modular.collectormonitorcurrent.mapper;

import vip.xiaonuo.biz.modular.collectormonitorcurrent.entity.SkyCollectorMonitorCurrent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author wangjian
* @description 针对表【sky_collector_monitor_current(采集器实时数据)】的数据库操作Mapper
* @createDate 2023-10-25 14:30:23
* @Entity src.main.java.vip.xiaonuo.biz.modular.collectormonitorcurrent.entity.SkyCollectorMonitorCurrent
*/
public interface SkyCollectorMonitorCurrentMapper extends BaseMapper<SkyCollectorMonitorCurrent> {

}




