/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.collectorinfo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.modular.collectorinfo.param.*;
import vip.xiaonuo.biz.modular.collectorupgrade.entity.SkyCollectorUpgrade;
import vip.xiaonuo.biz.modular.collectorupgrade.param.SkyCollectorUpgradeAddParam;
import vip.xiaonuo.biz.modular.collectorupgrade.service.SkyCollectorUpgradeService;
import vip.xiaonuo.biz.modular.equrelationship.entity.SkyEquRelationship;
import vip.xiaonuo.biz.modular.equrelationship.mapper.SkyEquRelationshipMapper;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoPageVO;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;
import vip.xiaonuo.biz.modular.invmonitorcurrent.mapper.SkyInvMonitorCurrentMapper;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.collectorinfo.entity.SkyCollectorInfo;
import vip.xiaonuo.biz.modular.collectorinfo.mapper.SkyCollectorInfoMapper;
import vip.xiaonuo.biz.modular.collectorinfo.service.SkyCollectorInfoService;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

/**
 * 采集器信息Service接口实现类
 *
 * @author 全佳璧
 * @date  2023/10/23 09:46
 **/
@Service
public class SkyCollectorInfoServiceImpl extends ServiceImpl<SkyCollectorInfoMapper, SkyCollectorInfo> implements SkyCollectorInfoService {

    @Resource
    private SkyCollectorInfoMapper skyCollectorInfoMapper;

    @Resource
    private SkyCollectorUpgradeService skyCollectorUpgradeService;

    @Resource
    private SkyEquRelationshipMapper skyEquRelationshipMapper;

    @Override
    public Page<SkyCollectorInfoPageResVO> page(SkyCollectorInfoReq skyCollectorInfoPageParam) {
        Page<SkyCollectorInfoPageResVO> pageDataModel = new Page<>(skyCollectorInfoPageParam.getCurrent(), skyCollectorInfoPageParam.getSize());
        List<SkyCollectorInfoPageResVO> list = skyCollectorInfoMapper.pageList(skyCollectorInfoPageParam);

        pageDataModel.setRecords(list);
        pageDataModel.setTotal(skyCollectorInfoMapper.countNum(skyCollectorInfoPageParam));
        return pageDataModel;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyCollectorInfoAddParam skyCollectorInfoAddParam) {
        SkyCollectorInfo skyCollectorInfo = BeanUtil.toBean(skyCollectorInfoAddParam, SkyCollectorInfo.class);
        this.save(skyCollectorInfo);

        // add升级表
        String collectorSn = skyCollectorInfoAddParam.getCollectorSn();
        SkyCollectorUpgrade one = skyCollectorUpgradeService.getOne(new QueryWrapper<SkyCollectorUpgrade>().lambda()
                .eq(SkyCollectorUpgrade::getCollectSn, collectorSn));
        if (one != null) {
            return;
        }
        SkyCollectorUpgradeAddParam upgradeAddParam = new SkyCollectorUpgradeAddParam();
        upgradeAddParam.setCollectSn(collectorSn);
        // 查询逆变器(2:并网，3储能)
        SkyEquRelationship relationship = skyEquRelationshipMapper.selectOne(new QueryWrapper<SkyEquRelationship>().lambda()
                .eq(SkyEquRelationship::getCollectorSn, collectorSn)
                .in(SkyEquRelationship::getTypeId, CollectionUtils.arrayToList(new int[]{2, 3})));
        if (relationship != null) {
            upgradeAddParam.setEquSn(relationship.getEquSn());
        }
        if (ObjectUtil.isNotEmpty(skyCollectorInfoAddParam.getStationId())) {
            upgradeAddParam.setStationId(skyCollectorInfoAddParam.getStationId());
        }
        upgradeAddParam.setUpgradeStatus(0);
        skyCollectorUpgradeService.add(upgradeAddParam);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyCollectorInfoEditParam skyCollectorInfoEditParam) {
        SkyCollectorInfo skyCollectorInfo = this.queryEntity(skyCollectorInfoEditParam.getCollectorSn());
        BeanUtil.copyProperties(skyCollectorInfoEditParam, skyCollectorInfo);
        this.updateById(skyCollectorInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyCollectorInfoIdParam> skyCollectorInfoIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyCollectorInfoIdParamList, SkyCollectorInfoIdParam::getCollectorSn));
    }

    @Override
    public SkyCollectorInfo detail(SkyCollectorInfoIdParam skyCollectorInfoIdParam) {
        return this.queryEntity(skyCollectorInfoIdParam.getCollectorSn());
    }

    @Override
    public SkyCollectorInfo queryEntity(String id) {
        SkyCollectorInfo skyCollectorInfo = this.getById(id);
        if(ObjectUtil.isEmpty(skyCollectorInfo)) {
            throw new CommonException("采集器信息不存在，id值为：{}", id);
        }
        return skyCollectorInfo;
    }

}
