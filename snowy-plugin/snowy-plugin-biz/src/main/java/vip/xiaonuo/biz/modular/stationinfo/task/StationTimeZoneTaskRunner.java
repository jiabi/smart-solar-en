package vip.xiaonuo.biz.modular.stationinfo.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.stationinfo.mapper.StationInfoMapper;
import vip.xiaonuo.common.timer.CommonTimerTaskRunner;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/7/12 9:53
 */
@Slf4j
@Component
public class StationTimeZoneTaskRunner implements CommonTimerTaskRunner {

    @Resource
    private StationInfoMapper stationInfoMapper;

    @Override
    public void action() {
        List<String> ids = stationInfoMapper.getGermanyStation();
        LocalDate currentDate = LocalDate.now();
        if (!CollectionUtils.isEmpty(ids) && currentDate.isAfter(LocalDate.of(currentDate.getYear(), 3, 24))) {
            stationInfoMapper.plusOneHour(ids);
        } else if (!CollectionUtils.isEmpty(ids)) {
            stationInfoMapper.miusOneHour(ids);
        }
    }
}
