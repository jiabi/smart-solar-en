package vip.xiaonuo.biz.modular.homepage.param;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @Author wangjian
 * @Date 2024/2/27 13:22
 */

@Getter
@Setter
public class HomePageHisPowerReqParam {
    /**
     * 日 yyyy-MM-dd
     * 月 yyyy-MM
     * 年 yyyy
     * 总 不用传
     */
    private String date;

    /**
     *1日 2月 3年 4总
     */
    @NotNull(message = "类型不能为空")
    @Min(value = 1, message = "类型不能小于1")
    @Max(value = 4, message = "类型不能大于4")
    private Integer type;
}
