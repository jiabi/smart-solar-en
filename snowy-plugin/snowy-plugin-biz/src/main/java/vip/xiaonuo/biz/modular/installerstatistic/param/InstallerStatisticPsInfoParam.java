package vip.xiaonuo.biz.modular.installerstatistic.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/4/7 11:04
 */

@Getter
@Setter
public class InstallerStatisticPsInfoParam {

    private Integer status;

    private Double stationSize;

    private Double stationPower;

}
