/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.collectorupgrade.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.collectorupgrade.param.*;
import vip.xiaonuo.biz.modular.invupgrade.param.SkyInvUpgradeParam;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.collectorupgrade.entity.SkyCollectorUpgrade;
import vip.xiaonuo.biz.modular.collectorupgrade.service.SkyCollectorUpgradeService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 采集器升级控制器
 *
 * @author 全佳璧
 * @date  2024/04/17 19:51
 */
@Api(tags = "采集器升级控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyCollectorUpgradeController {

    @Resource
    private SkyCollectorUpgradeService skyCollectorUpgradeService;

    /**
     * 获取采集器升级分页
     *
     * @author 全佳璧
     * @date  2024/04/17 19:51
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取采集器升级分页")
//    @SaCheckPermission("/biz/collectorupgrade/page")
    @PostMapping("/biz/collectorupgrade/page")
    public CommonResult<Page<SkyCollectorUpgradePageVO>> page(@RequestBody SkyCollectorUpgradePageParam param) {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }
        return CommonResult.data(skyCollectorUpgradeService.page(param));
    }

    /**
     * 添加采集器升级
     *
     * @author 全佳璧
     * @date  2024/04/17 19:51
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加采集器升级")
    @CommonLog("添加采集器升级")
//    @SaCheckPermission("/biz/collectorupgrade/add")
    @PostMapping("/biz/collectorupgrade/add")
    public CommonResult<String> add(@RequestBody @Valid SkyCollectorUpgradeAddParam skyCollectorUpgradeAddParam) {
        skyCollectorUpgradeService.add(skyCollectorUpgradeAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑采集器升级
     *
     * @author 全佳璧
     * @date  2024/04/17 19:51
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑采集器升级")
    @CommonLog("编辑采集器升级")
//    @SaCheckPermission("/biz/collectorupgrade/edit")
    @PostMapping("/biz/collectorupgrade/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyCollectorUpgradeEditParam skyCollectorUpgradeEditParam) {
        skyCollectorUpgradeService.edit(skyCollectorUpgradeEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除采集器升级
     *
     * @author 全佳璧
     * @date  2024/04/17 19:51
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除采集器升级")
    @CommonLog("删除采集器升级")
//    @SaCheckPermission("/biz/collectorupgrade/delete")
    @PostMapping("/biz/collectorupgrade/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyCollectorUpgradeIdParam> skyCollectorUpgradeIdParamList) {
        skyCollectorUpgradeService.delete(skyCollectorUpgradeIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取采集器升级详情
     *
     * @author 全佳璧
     * @date  2024/04/17 19:51
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取采集器升级详情")
//    @SaCheckPermission("/biz/collectorupgrade/detail")
    @GetMapping("/biz/collectorupgrade/detail")
    public CommonResult<SkyCollectorUpgrade> detail(@Valid SkyCollectorUpgradeIdParam skyCollectorUpgradeIdParam) {
        return CommonResult.data(skyCollectorUpgradeService.detail(skyCollectorUpgradeIdParam));
    }


    /**
     * 采集器升级
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("采集器升级")
//    @SaCheckPermission("/biz/collectorupgrade/upgrade")
    @PostMapping("/biz/collectorupgrade/upgrade")
    public CommonResult<String> upgrade(@RequestBody @Valid SkyCollectorUpgradeParam skyCollectorUpgradeParam) {
        skyCollectorUpgradeService.upgrade(skyCollectorUpgradeParam);
        return CommonResult.ok();
    }

}
