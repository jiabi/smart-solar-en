/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.alarminfocurrent.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.*;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.alarminfocurrent.entity.SkyAlarmInfoCurrent;
import vip.xiaonuo.biz.modular.alarminfocurrent.service.SkyAlarmInfoCurrentService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 故障信息实时信息控制器
 *
 * @author 全佳璧
 * @date  2023/10/23 10:48
 */
@Api(tags = "故障信息实时信息控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyAlarmInfoCurrentController {

    @Resource
    private SkyAlarmInfoCurrentService skyAlarmInfoCurrentService;

    /**
     * 获取故障信息实时信息分页
     *
     * @author 全佳璧
     * @date  2023/10/23 10:48
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("设备信息-报警信息页面-获取故障信息实时信息分页")
//    @SaCheckPermission("/biz/alarminfocurrent/page")
    @GetMapping("/biz/alarminfocurrent/page")
    public CommonResult<Page<SkyAlarmInfoPageVO>> page(SkyAlarmInfoPageReqDTO skyAlarmInfoPageReqDTO) {
        SkyAlarmInfoPageParam param = new SkyAlarmInfoPageParam();
        BeanUtil.copyProperties(skyAlarmInfoPageReqDTO,param);
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }

        return CommonResult.data(skyAlarmInfoCurrentService.page(param));
    }

//    /**
//     * 告警信息目录-告警分页
//     */
//    @ApiOperationSupport(order = 6)
//    @ApiOperation("告警信息目录-告警分页")
//    @SaCheckPermission("/biz/alarminfocurrent/pageInfo")
//    @PostMapping("/biz/alarminfocurrent/pageInfo")
//    public CommonResult<Page<SkyAlarmInfoPageVO>> pageInfo(@RequestBody @Valid SkyAlarmInfoPageInfoParam skyAlarmInfoPageParam) {
//        return CommonResult.data(skyAlarmInfoCurrentService.pageInfo(skyAlarmInfoPageParam));
//    }

    /**
     * 添加故障信息实时信息
     *
     * @author 全佳璧
     * @date  2023/10/23 10:48
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加故障信息实时信息")
    @CommonLog("添加故障信息实时信息")
    @SaCheckPermission("/biz/alarminfocurrent/add")
    @PostMapping("/biz/alarminfocurrent/add")
    public CommonResult<String> add(@RequestBody @Valid SkyAlarmInfoCurrentAddParam skyAlarmInfoCurrentAddParam) {
        skyAlarmInfoCurrentService.add(skyAlarmInfoCurrentAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑故障信息实时信息
     *
     * @author 全佳璧
     * @date  2023/10/23 10:48
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑故障信息实时信息")
    @CommonLog("编辑故障信息实时信息")
    @SaCheckPermission("/biz/alarminfocurrent/edit")
    @PostMapping("/biz/alarminfocurrent/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyAlarmInfoCurrentEditParam skyAlarmInfoCurrentEditParam) {
        skyAlarmInfoCurrentService.edit(skyAlarmInfoCurrentEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除故障信息实时信息
     *
     * @author 全佳璧
     * @date  2023/10/23 10:48
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除故障信息实时信息")
    @CommonLog("删除故障信息实时信息")
    @SaCheckPermission("/biz/alarminfocurrent/delete")
    @PostMapping("/biz/alarminfocurrent/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyAlarmInfoCurrentIdParam> skyAlarmInfoCurrentIdParamList) {
        skyAlarmInfoCurrentService.delete(skyAlarmInfoCurrentIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取故障信息实时信息详情
     *
     * @author 全佳璧
     * @date  2023/10/23 10:48
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取故障信息实时信息详情")
    @SaCheckPermission("/biz/alarminfocurrent/detail")
    @GetMapping("/biz/alarminfocurrent/detail")
    public CommonResult<SkyAlarmInfoCurrent> detail(@Valid SkyAlarmInfoCurrentIdParam skyAlarmInfoCurrentIdParam) {
        return CommonResult.data(skyAlarmInfoCurrentService.detail(skyAlarmInfoCurrentIdParam));
    }

    /**
     * 获取所有告警信息描述
     *
     * @author 全佳璧
     * @date  2023/10/23 10:48
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("获取所有告警信息描述")
//    @SaCheckPermission("/biz/alarminfocurrent/alarmMsg")
    @GetMapping("/biz/alarminfocurrent/alarmMsg")
    public CommonResult<List<String>> alarmMsg() {
        return CommonResult.data(skyAlarmInfoCurrentService.alarmMsg());
    }
}
