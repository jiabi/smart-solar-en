/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.equbin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.dict.entity.BizDict;
import vip.xiaonuo.biz.modular.dict.mapper.BizDictMapper;
import vip.xiaonuo.biz.modular.dict.param.BizDictEditParam;
import vip.xiaonuo.biz.modular.equbin.param.*;
import vip.xiaonuo.biz.modular.invupgrade.entity.SkyInvUpgrade;
import vip.xiaonuo.biz.modular.invupgrade.mapper.SkyInvUpgradeMapper;
import vip.xiaonuo.biz.modular.invupgrade.param.SkyInvUpgradePageParam;
import vip.xiaonuo.biz.modular.invupgrade.param.SkyInvUpgradeParam;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.equbin.entity.SkyEquBin;
import vip.xiaonuo.biz.modular.equbin.mapper.SkyEquBinMapper;
import vip.xiaonuo.biz.modular.equbin.service.SkyEquBinService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 设备升级包Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/04/17 19:40
 **/
@Service
public class SkyEquBinServiceImpl extends ServiceImpl<SkyEquBinMapper, SkyEquBin> implements SkyEquBinService {


    @Resource
    private BizDictMapper bizDictMapper;

    @Override
    public Page<SkyEquBinPageVO> page(SkyEquBinPageParam skyEquBinPageParam) {
        QueryWrapper<SkyEquBin> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(skyEquBinPageParam.getEquType())) {
            queryWrapper.lambda().eq(SkyEquBin::getEquType, skyEquBinPageParam.getEquType());
        }
        if(ObjectUtil.isNotEmpty(skyEquBinPageParam.getEquModel())) {
            queryWrapper.lambda().eq(SkyEquBin::getEquModel, skyEquBinPageParam.getEquModel());
        }
        if(ObjectUtil.isNotEmpty(skyEquBinPageParam.getEquModule())) {
            queryWrapper.lambda().eq(SkyEquBin::getEquModule, skyEquBinPageParam.getEquModule());
        }
        if(ObjectUtil.isNotEmpty(skyEquBinPageParam.getEquModuleName())){
            queryWrapper.lambda().eq(SkyEquBin::getEquModule, translate(skyEquBinPageParam));
        }
        if(ObjectUtil.isNotEmpty(skyEquBinPageParam.getBinName())) {
            queryWrapper.lambda().like(SkyEquBin::getBinName, skyEquBinPageParam.getBinName());
        }
        queryWrapper.lambda().orderByDesc(SkyEquBin::getCreateTime);
        Page<SkyEquBin> page = this.page(CommonPageRequest.defaultPage(), queryWrapper);
        List<SkyEquBin> records = page.getRecords();

        Page<SkyEquBinPageVO> pageRes = new Page<>();
        pageRes.setCurrent(page.getCurrent());
        pageRes.setSize(page.getSize());
        pageRes.setPages(page.getPages());
        pageRes.setTotal(page.getTotal());
        List<SkyEquBinPageVO> list = new ArrayList<>();
        for (SkyEquBin equBin:records) {
            SkyEquBinPageVO resvo = new SkyEquBinPageVO();
            BeanUtil.copyProperties(equBin,resvo);
            if (ObjectUtil.isNotEmpty(equBin.getBinName())) {
                if (ObjectUtil.isNotEmpty(equBin.getEquModule()) && equBin.getEquModule() == 1) {
                    resvo.setEquModuleName("wstk");
                }
                if (ObjectUtil.isNotEmpty(equBin.getEquModule()) && equBin.getEquModule() == 2) {
                    resvo.setEquModuleName("marm");
                }
                if (ObjectUtil.isNotEmpty(equBin.getEquModule()) && equBin.getEquModule() == 3) {
                    resvo.setEquModuleName("sarm");
                }
                if (ObjectUtil.isNotEmpty(equBin.getEquModule()) && equBin.getEquModule() == 4) {
                    resvo.setEquModuleName("mdsp");
                }
                if (ObjectUtil.isNotEmpty(equBin.getEquModule()) && equBin.getEquModule() == 5) {
                    resvo.setEquModuleName("sfty");
                }
                if (ObjectUtil.isNotEmpty(equBin.getEquModule()) && equBin.getEquModule() == 6) {
                    resvo.setEquModuleName("gstk");
                }
            }
            list.add(resvo);
        }
        pageRes.setRecords(list);
        return pageRes;
    }

    public Integer translate(SkyEquBinPageParam skyEquBinPageParam){
        if ("wstk".equals(skyEquBinPageParam.getEquModuleName())) {
            return 1;
        }
        if("marm".equals(skyEquBinPageParam.getEquModuleName())){
            return 2;
        }
        if("sarm".equals(skyEquBinPageParam.getEquModuleName())){
            return 3;
        }
        if("mdsp".equals(skyEquBinPageParam.getEquModuleName())){
            return 4;
        }
        if("sfty".equals(skyEquBinPageParam.getEquModuleName())){
            return 5;
        }
        if("gstk".equals(skyEquBinPageParam.getEquModuleName())){
            return 6;
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyEquBinAddParam skyEquBinAddParam) {
        SkyEquBin one = this.getOne(new QueryWrapper<SkyEquBin>().lambda().eq(SkyEquBin::getBinName, skyEquBinAddParam.getBinName()));
        if (ObjectUtil.isNotEmpty(one)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NAME_EXIST_9124);
        }
        if ("wstk".equals(skyEquBinAddParam.getEquModuleName()) ) {
            skyEquBinAddParam.setEquModule(1);
        }
        if ("marm".equals(skyEquBinAddParam.getEquModuleName())) {
            skyEquBinAddParam.setEquModule(2);
        }
        if ("sarm".equals(skyEquBinAddParam.getEquModuleName())) {
            skyEquBinAddParam.setEquModule(3);
        }
        if ("mdsp".equals(skyEquBinAddParam.getEquModuleName())) {
            skyEquBinAddParam.setEquModule(4);
        }
        if ("sfty".equals(skyEquBinAddParam.getEquModuleName())) {
            skyEquBinAddParam.setEquModule(5);
        }
        if ("gstk".equals(skyEquBinAddParam.getEquModuleName()) ) {
            skyEquBinAddParam.setEquModule(6);
        }
        SkyEquBin skyEquBin = BeanUtil.toBean(skyEquBinAddParam, SkyEquBin.class);
        if (ObjectUtil.isNotEmpty(skyEquBinAddParam.getFileSize())) {
            skyEquBin.setBinSize(skyEquBinAddParam.getFileSize());
        } else {
            skyEquBin.setBinSize(0);
        }
        this.save(skyEquBin);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyEquBinEditParam skyEquBinEditParam) {
        SkyEquBin skyEquBin = this.queryEntity(skyEquBinEditParam.getId());
        BeanUtil.copyProperties(skyEquBinEditParam, skyEquBin);
        this.updateById(skyEquBin);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyEquBinIdParam> skyEquBinIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyEquBinIdParamList, SkyEquBinIdParam::getId));
    }

    @Override
    public SkyEquBin detail(SkyEquBinIdParam skyEquBinIdParam) {
        return this.queryEntity(skyEquBinIdParam.getId());
    }

    @Override
    public SkyEquBin queryEntity(String id) {
        SkyEquBin skyEquBin = this.getById(id);
        if(ObjectUtil.isEmpty(skyEquBin)) {
            throw new CommonException("设备升级包不存在，id值为：{}", id);
        }
        return skyEquBin;
    }


    @Override
    public List<SkyEquBinLinkageVO> selectSkyEquBin(SkyEquBinReqParam skyInvUpgradeParam) {
        if(StringUtils.isEmpty(skyInvUpgradeParam.getModelName()) && ObjectUtil.isEmpty(skyInvUpgradeParam.getEquType())){
            throw new SkyCommonException(SkyCommonExceptionEnum.MODEL_NAME_NULL);
        }
        List<SkyEquBinLinkageVO> skyEquBinLinkageVOS = new ArrayList<>();
        LambdaQueryWrapper<SkyEquBin> wrapper = Wrappers.lambdaQuery(SkyEquBin.class);
        if(StringUtils.isNotEmpty(skyInvUpgradeParam.getModelName())){
            wrapper.likeRight(SkyEquBin::getBinName,skyInvUpgradeParam.getModelName());
        } else if (ObjectUtil.isNotEmpty(skyInvUpgradeParam.getEquType()) && skyInvUpgradeParam.getEquType() == 1) {
            wrapper.eq(SkyEquBin::getEquType, skyInvUpgradeParam.getEquType());
        }
        List<SkyEquBin> skyEquBinList = this.list(wrapper);
        if(CollectionUtils.isNotEmpty(skyEquBinList)){
            skyEquBinList.stream().forEach(skyEquBin -> {
                SkyEquBinLinkageVO skyEquBinLinkageVO = new SkyEquBinLinkageVO();
                BeanUtil.copyProperties(skyEquBin,skyEquBinLinkageVO);
                String binNameSplit = skyEquBin.getBinName().split("_")[1];
                BizDict bizDict = bizDictMapper.selectOne(Wrappers.lambdaQuery(BizDict.class).eq(BizDict::getDictValue, binNameSplit));
                if(ObjectUtil.isNotEmpty(bizDict)){
                    skyEquBinLinkageVO.setBinNameSplit(bizDict.getDictLabel());
                    skyEquBinLinkageVO.setBinNameValue(bizDict.getDictValue());
                }
                if(StringUtils.isNotEmpty(binNameSplit)){
                    if (ObjectUtil.isNotEmpty(skyInvUpgradeParam.getEquType()) && skyInvUpgradeParam.getEquType() == 1) {
                        List<SkyEquBin> skyEquBins = this.list(Wrappers.lambdaQuery(SkyEquBin.class)
                                .eq(SkyEquBin::getEquType,1)
                                .like(SkyEquBin::getBinName, skyEquBin.getBinName().split("_")[1]));
                        skyEquBinLinkageVO.setChildrens(skyEquBins);
                        skyEquBinLinkageVOS.add(skyEquBinLinkageVO);
                    } else {
                        List<SkyEquBin> skyEquBins = this.list(Wrappers.lambdaQuery(SkyEquBin.class)
                                .eq(SkyEquBin::getEquModel,skyInvUpgradeParam.getModelName())
                                .like(SkyEquBin::getBinName, skyEquBin.getBinName().split("_")[1]));
                        skyEquBinLinkageVO.setChildrens(skyEquBins);
                        skyEquBinLinkageVOS.add(skyEquBinLinkageVO);
                    }
                }
            });
        }
        return new ArrayList<>(skyEquBinLinkageVOS.stream()
                .collect(Collectors.toMap(SkyEquBinLinkageVO::getBinNameSplit, vo -> vo, (existing, replacement) -> existing))
                .values());
    }

}
