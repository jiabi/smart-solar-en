package vip.xiaonuo.biz.modular.stationinfo.param.pschart;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/8 10:33
 */
@Setter
@Getter
@ContentRowHeight(18)
@HeadRowHeight(20)
@ColumnWidth(24)
public class PsChartAnalysisExcelResParam {
    /**
     * 时间轴
     */
    @ExcelProperty(value = "时间")
    private String time;

    /**
     * 当日发电量
     */
    @ExcelProperty(value = "发电量（kWh）")
    private Double dayGenerate;

    /**
     * 当日用电量
     */
    @ExcelProperty(value = "用电量（kWh）")
    private Double dayUse;

    /**
     * 当日并网量
     */
    @ExcelProperty(value = "并网量（kWh）")
    private Double dayGrid;

    /**
     * 当日购电量
     */
    @ExcelProperty(value = "购电量（kWh）")
    private Double dayBuy;

    /**
     * 当日充电量
     */
    @ExcelProperty(value = "充电量（kWh）")
    private Double dayCharge;

    /**
     * 当日放电量
     */
    @ExcelProperty(value = "放电量（kWh）")
    private Double dayDischarge;
}
