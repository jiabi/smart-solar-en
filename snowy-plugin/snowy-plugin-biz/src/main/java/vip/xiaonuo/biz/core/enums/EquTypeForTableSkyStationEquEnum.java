package vip.xiaonuo.biz.core.enums;

import lombok.Getter;

/**
 * 设备类型
 *
 * @Author wangjian
 * @Date 2024/4/9 19:35
 */

@Getter
public enum EquTypeForTableSkyStationEquEnum {
    /**
     * 采集器
     */
    COLLECTOR(1),

    /**
     * 并网逆变器
     */
    GRID_INV(2),

    /**
     * 储能逆变器
     */
    STORAGE_INV(3);

    private final Integer value;

    EquTypeForTableSkyStationEquEnum(Integer value) {
        this.value = value;
    }
}
