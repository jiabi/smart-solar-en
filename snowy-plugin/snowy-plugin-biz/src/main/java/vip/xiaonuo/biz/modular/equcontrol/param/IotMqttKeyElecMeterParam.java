package vip.xiaonuo.biz.modular.equcontrol.param;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/7/18 10:32
 */
public class IotMqttKeyElecMeterParam {
    private String key;
    private Object value;
    private String modbus;

    // 构造函数
    public IotMqttKeyElecMeterParam(@JsonProperty("key") String key,
                      @JsonProperty("value") Object value,
                      @JsonProperty("modbus") String modbus) {
        this.key = key;
        this.value = value;
        this.modbus = modbus;
    }

    // Getters 和 Setters
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getModbus() {
        return modbus;
    }

    public void setModbus(String modbus) {
        this.modbus = modbus;
    }

    // toString 方法（可选）
    @Override
    public String toString() {
        return "IotMqttKeyElecMeterParam{" +
                "key='" + key + '\'' +
                ", value=" + value +
                ", modbus='" + modbus + '\'' +
                '}';
    }
}
