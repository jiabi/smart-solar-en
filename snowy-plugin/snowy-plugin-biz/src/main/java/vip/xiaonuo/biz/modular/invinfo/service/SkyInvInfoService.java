/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invinfo.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageParam;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageVO;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.param.*;
import cn.hutool.json.JSONObject;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 逆变器信息Service接口
 *
 * @author 全佳璧
 * @date  2023/10/05 16:24
 **/
public interface SkyInvInfoService extends IService<SkyInvInfo> {

    /**
     * 获取逆变器信息分页
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    Page<SkyInvInfoPageVO> page(SkyInvInfoPageParam skyInvInfoPageParam);

    /**
     * 添加逆变器信息
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    void add(SkyInvInfoAddParam skyInvInfoAddParam);

    /**
     * 编辑逆变器信息
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    void edit(SkyInvInfoEditParam skyInvInfoEditParam);

    /**
     * 删除逆变器信息
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    void delete(List<SkyInvInfoIdParam> skyInvInfoIdParamList);

    /**
     * 获取逆变器信息详情
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    SkyInvInfoDetailVO detail(SkyInvInfoIdParam skyInvInfoIdParam);

    /**
     * 时区
     * @param zoneIdStr
     * @return
     */
    LocalDateTime getNowTimeByZoneId(String zoneIdStr);

    /**
     * 获取逆变器信息详情
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     **/
    SkyInvInfo queryEntity(String id);

    /**
     * 获取动态字段配置
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
    **/
    List<JSONObject> dynamicFieldConfigList();

    /**
     * @description: 故障分页查询
     * @author: wangjian
     * @date: 2023/10/12 13:37
     **/
    Page<SkyAlarmInfoPageVO> alarmPage(SkyAlarmInfoPageParam skyAlarmInfoPageParam);

    /**
     * 获取未绑定设备
     * @return
     */
    List<SkyInvNotBindResVO> invNotBind(SkyInvNotBindReqParam reqParam);

    void invBind(SkyInvBindPsParam reqParam);

    String timeZoneOperation();
}
