package vip.xiaonuo.biz.modular.collectorupgrade.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2024/4/22 14:12
 */

@Getter
@Setter
public class SkyCollectorUpgradePageVO {
    /** ID */
    @ApiModelProperty(value = "ID", position = 1)
    private String id;

    /** 采集器SN */
    @ApiModelProperty(value = "采集器SN", position = 2)
    private String collectSn;

    /** 设备SN */
    @ApiModelProperty(value = "设备SN", position = 3)
    private String equSn;

    /** 电站ID */
    @ApiModelProperty(value = "电站ID", position = 4)
    private String stationId;

    /** 版本号 */
    @ApiModelProperty(value = "版本号", position = 5)
    private String ver;

    /** 升级包名 */
    @ApiModelProperty(value = "升级包名", position = 6)
    private String upgradeBin;

    /** 升级状态 0:等待；1.升级中 */
    @ApiModelProperty(value = "升级状态 0:等待；1.升级中", position = 7)
    private Integer upgradeStatus;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 9)
    private Date createTime;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 11)
    private Date updateTime;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称", position = 11)
    private String stationName;

    /** 设备型号名称 */
    @ApiModelProperty(value = "设备型号名称", position = 12)
    private String modelName;

    /** 采集器类型 */
    @ApiModelProperty(value = "采集器类型")
    private String collectorType;

}
