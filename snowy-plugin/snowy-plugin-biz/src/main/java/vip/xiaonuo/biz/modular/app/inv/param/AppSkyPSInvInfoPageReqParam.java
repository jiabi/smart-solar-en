package vip.xiaonuo.biz.modular.app.inv.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @Author wangjian
 * @Date 2024/3/15 11:14
 */
@Getter
@Setter
public class AppSkyPSInvInfoPageReqParam {

    /** 当前页 */
    @ApiModelProperty(value = "当前页码")
    @NotNull(message = "pageNum not null")
    private Integer pageNum;

    /** 每页条数 */
    @ApiModelProperty(value = "每页条数")
    @NotNull(message = "pageSize not null")
    private Integer pageSize;

    /** 设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪 */
    private Integer type;

    /** 所属电站Id */
    @ApiModelProperty(value = "所属电站Id")
    private String stationId;


    public int getLimitStart() {
        return (getPageNum() - 1) * getPageSize();
    }
}
