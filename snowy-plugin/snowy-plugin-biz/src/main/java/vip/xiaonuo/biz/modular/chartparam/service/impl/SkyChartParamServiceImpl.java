/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.chartparam.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.modular.chartparam.param.*;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.chartparam.entity.SkyChartParam;
import vip.xiaonuo.biz.modular.chartparam.mapper.SkyChartParamMapper;
import vip.xiaonuo.biz.modular.chartparam.service.SkyChartParamService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 图表参数字典表Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/02/20 18:55
 **/
@Service
public class SkyChartParamServiceImpl extends ServiceImpl<SkyChartParamMapper, SkyChartParam> implements SkyChartParamService {


    @Resource
    private SkyChartParamMapper skyChartParamMapper;

    @Override
    public Page<SkyChartParam> page(SkyChartParamPageParam skyChartParamPageParam) {
        QueryWrapper<SkyChartParam> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(skyChartParamPageParam.getParamName())) {
            queryWrapper.lambda().like(SkyChartParam::getParamName, skyChartParamPageParam.getParamName());
        }
        if(ObjectUtil.isNotEmpty(skyChartParamPageParam.getType())) {
            queryWrapper.lambda().eq(SkyChartParam::getType, skyChartParamPageParam.getType());
        }
        if(ObjectUtil.isNotEmpty(skyChartParamPageParam.getParamField())) {
            queryWrapper.lambda().like(SkyChartParam::getParamField, skyChartParamPageParam.getParamField());
        }
        if(ObjectUtil.isNotEmpty(skyChartParamPageParam.getParamTable())) {
            queryWrapper.lambda().like(SkyChartParam::getParamTable, skyChartParamPageParam.getParamTable());
        }
        queryWrapper.lambda().orderByDesc(SkyChartParam::getId);
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyChartParamAddParam skyChartParamAddParam) {
        if (ObjectUtil.isNotEmpty(skyChartParamAddParam.getMemo()) && skyChartParamAddParam.getMemo().length() > 50) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9003);
        }
        skyChartParamAddParam.setIsDelete(2);
        SkyChartParam skyChartParam = BeanUtil.toBean(skyChartParamAddParam, SkyChartParam.class);
        this.save(skyChartParam);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyChartParamEditParam skyChartParamEditParam) {
        if (StringUtils.isNotEmpty(skyChartParamEditParam.getMemo()) && skyChartParamEditParam.getMemo().length() > 50) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9003);
        }
        SkyChartParam skyChartParam = this.queryEntity(skyChartParamEditParam.getId());
        BeanUtil.copyProperties(skyChartParamEditParam, skyChartParam);
        this.updateById(skyChartParam);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(SkyChartParamIdParam skyChartParamIdParam) {
        // 逻辑删除
        SkyChartParam skyChartParam = this.queryEntity(skyChartParamIdParam.getId());
        if (skyChartParam != null) {
            skyChartParamMapper.deleteById(skyChartParam);
        }
    }

    @Override
    public SkyChartParam detail(SkyChartParamIdParam skyChartParamIdParam) {
        return this.queryEntity(skyChartParamIdParam.getId());
    }

    @Override
    public SkyChartParam queryEntity(String id) {
        SkyChartParam skyChartParam = this.getById(id);
        if(ObjectUtil.isEmpty(skyChartParam)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        return skyChartParam;
    }

    @Override
    public List<ChartParamTablesResVO> getAllTables() {
        List<ChartParamTablesResVO> resDTO =  skyChartParamMapper.getTables();
        return resDTO;
    }

    @Override
    public List<ChartParamTableNameAndCommentResVO> getTablesNameAndComment(ChartParamTableNameReq req) {
        List<ChartParamTableNameAndCommentResVO> resDTO =  skyChartParamMapper.getTablesNameAndComment(req.getTableName());
        return resDTO;
    }

    @Override
    public List<SkyChartParam> getList(SkyChartParamListParam skyChartParamListParam) {
        QueryWrapper<SkyChartParam> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotEmpty(skyChartParamListParam.getEquType())) {
            Integer equType = skyChartParamListParam.getEquType();
            if (equType == 2) {
                queryWrapper.lambda().in(SkyChartParam::getType, Arrays.asList(1,2));
            }
        }
        return skyChartParamMapper.selectList(queryWrapper);
    }

}
