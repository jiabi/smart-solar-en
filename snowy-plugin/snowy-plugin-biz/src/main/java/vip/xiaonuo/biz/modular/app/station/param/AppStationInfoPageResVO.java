package vip.xiaonuo.biz.modular.app.station.param;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.modular.stationinfo.entity.Area;
import vip.xiaonuo.biz.modular.stationinfo.param.StationContactInfo;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2024/3/20 10:43
 */
@Getter
@Setter

public class AppStationInfoPageResVO {
    /** 电站ID */
    @ApiModelProperty(value = "电站ID")
    private String id;

    /** guid */
    @ApiModelProperty(value = "guid")
    private String guid;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称")
    private String name;

    /** 电站状态 */
    @ApiModelProperty(value = "电站状态 10-正常 20-故障 30-离线")
    private Integer status;

    /** 装机容量 */
    @ApiModelProperty(value = "装机容量")
    private Double installed;

    /** 当前功率 */
    @ApiModelProperty(value = "当前功率")
    private Double currentPower;

    /** 今日发电 */
    @ApiModelProperty(value = "今日发电")
    private Double energyInDay;

    /** 电站地址	 */
    @ApiModelProperty(value = "电站地址")
    private String address;

    /** 图片uri */
    @ApiModelProperty(value = "图片uri")
    private String uri;

    /** 经度 */
    @ApiModelProperty(value = "经度")
    private String areaLng;

    /** 纬度 */
    @ApiModelProperty(value = "纬度")
    private String areaLat;

}
