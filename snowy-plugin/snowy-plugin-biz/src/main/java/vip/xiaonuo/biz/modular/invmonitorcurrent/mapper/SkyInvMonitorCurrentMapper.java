/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invmonitorcurrent.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.dataview.param.EquDayChartParam;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.SkyInvInfoMonitorTimeParam;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.SkyInvInfoRankVO;
import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteParam;
import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteVO;
import vip.xiaonuo.biz.modular.stationinfo.param.*;

import java.util.List;

/**
 * 逆变器实时数据Mapper接口
 *
 * @author 全佳璧
 * @date  2023/10/12 15:36
 **/
public interface SkyInvMonitorCurrentMapper extends BaseMapper<SkyInvMonitorCurrent> {
    List<SkyInvMonitorCurrent> selectHistory(@Param("date") String date, @Param("invSn") String invSn);

    /**
     * 根据电站id查询实时发电量
     **/
    StationInfoOverviewPowerVO getPower(@Param("stationId") String stationId);

    /**
     * 根据电站id查询实时用电量
     **/
    StationInfoOverviewPowerUsedVO powerUsed(@Param("stationId") String stationId);

    /**
     * 根据电站id查询电网实时数据
     **/
    StationInfoOverviewPowerGridVO powerGrid(@Param("stationId") String stationId);

    /**
     * 根据电站id查询逆变器排名
     **/
    List<SkyInvInfoRankVO> getRank(@Param("stationId") String stationId);

    /**
     * 根据电站id查询逆变器统计
     **/
    List<PsInvStatisticsParam> invStatistics(@Param("stationId") String stationId);

    /**
     * 获取逆变器状态
     * @param sns
     * @return
     */
    List<SkyInvInfoMonitorTimeParam> getInvStatus(List<String> sns);

    /**
     * 设备日图表-每日明细
     * @param date
     * @param invSn
     * @return
     */
    @DS("slave_1")
    List<SkyInvMonitorCurrent> selectCurrentHistory(@Param("dateForm") String dateForm, @Param("invSn") String invSn , @Param("date") String date);

    @DS("slave_1")
    List<EquDayChartParam> selectDayHistory(@Param("dateForm") String dateForm, @Param("invSns") List<String> invSns , @Param("date") String date);

    void deleteBatchBySn(@Param("snList") List<String> snList);

    List<SkyInvMonitorCurrentTripartiteVO> selectInvMonitorCurrents(@Param("invSns") List<String> invSns,SkyInvMonitorCurrentTripartiteParam skyInvMonitorCurrentTripartiteParam);
}
