package vip.xiaonuo.biz.modular.invmonitorcurrent.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class SkyInvMonitorHistoryVO {
    /** ID */
    @ApiModelProperty(value = "ID")
    private String id;

    /** 逆变器SN */
    @ApiModelProperty(value = "逆变器SN")
    private String invSn;

    /** 系统时间 */
    @ApiModelProperty(value = "系统时间")
    @JsonFormat(pattern = "hh:mm")
    private Date monitorTime;

    /** MPPT直流电压1 */
    @ApiModelProperty(value = "MPPT直流电压1")
    private Double monPv1vol;

    /** MPPT直流电流1 */
    @ApiModelProperty(value = "MPPT直流电流1")
    private Double monPv1cur;

    /** R端电网电流 */
    @ApiModelProperty(value = "R端电网电流")
    private Double monRcur;

    /** R端电网电压 */
    @ApiModelProperty(value = "R端电网电压")
    private Double monRvol;

    /** R端电网功率 */
    @ApiModelProperty(value = "R端电网功率")
    private Double monRpower;

    /** S端电网电流 */
    @ApiModelProperty(value = "S端电网电流")
    private Double monScur;

    /** S端电网电压 */
    @ApiModelProperty(value = "S端电网电压")
    private Double monSvol;

    /** S端电网功率 */
    @ApiModelProperty(value = "S端电网功率")
    private Double monSpower;

    /** T端电网电流 */
    @ApiModelProperty(value = "T端电网电流")
    private Double monTcur;

    /** T端电网电压 */
    @ApiModelProperty(value = "T端电网电压")
    private Double monTvol;

    /** T端电网功率 */
    @ApiModelProperty(value = "T端电网功率")
    private Double monTpower;

    /** RS线电压 */
    @ApiModelProperty(value = "RS线电压")
    private Double monRs;

    /** ST线电压 */
    @ApiModelProperty(value = "ST线电压")
    private Double monSt;

    /** TR线电压 */
    @ApiModelProperty(value = "TR线电压")
    private Double monTr;

    /** 直流PV1-N功率 */
    @ApiModelProperty(value = "直流PV1-N功率")
    private Double PV1NPower;

    /** 电网总功率 */
    @ApiModelProperty(value = "电网总功率")
    private Double gridTotalPower;

    /** 累计用电量 */
    @ApiModelProperty(value = "累计用电量")
    private Double totalLoadEnergy;
}
