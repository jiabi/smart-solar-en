package vip.xiaonuo.biz.modular.invmonitorcurrent.provider;

import org.springframework.stereotype.Service;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.api.SkyInvMonitorCurrentApi;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.invmonitorcurrent.mapper.SkyInvMonitorCurrentMapper;
import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteParam;
import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteVO;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class SkyInvMonitorCurrentProvider implements SkyInvMonitorCurrentApi {

    @Resource
    private SkyInvMonitorCurrentMapper skyInvMonitorCurrentMapper;

    @Override
    public List<SkyInvMonitorCurrentTripartiteVO> selectInvMonitorCurrents(List<String> invSns){
        SkyInvMonitorCurrentTripartiteParam skyInvMonitorCurrentTripartiteParam = new SkyInvMonitorCurrentTripartiteParam();
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if(roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            skyInvMonitorCurrentTripartiteParam.setDistributor(loginUser.getId());
        }else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            skyInvMonitorCurrentTripartiteParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            skyInvMonitorCurrentTripartiteParam.setStationContactId(loginUser.getId());
        }
        //获取最近五分钟之内的设备数据
        String startTime = LocalDateUtils.formatLocalDateTime(LocalDateUtils.getCurrentLocalDateTime().minusMinutes(5), LocalDateUtils.LONG_DATETIME_)+":00";
        String endTime = LocalDateUtils.formatLocalDateTime(LocalDateUtils.getCurrentLocalDateTime(), LocalDateUtils.LONG_DATETIME_)+":00";
        skyInvMonitorCurrentTripartiteParam.setStartTime(startTime);
        skyInvMonitorCurrentTripartiteParam.setEndTime(endTime);
        List<SkyInvMonitorCurrentTripartiteVO> skyInvMonitorCurrentTripartiteVOS = skyInvMonitorCurrentMapper.selectInvMonitorCurrents(invSns,skyInvMonitorCurrentTripartiteParam);
        return skyInvMonitorCurrentTripartiteVOS;
    }
}
