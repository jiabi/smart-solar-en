/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.equbin.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 设备升级包实体
 *
 * @author 全佳璧
 * @date  2024/04/17 19:40
 **/
@Getter
@Setter
@TableName("sky_equ_bin")
public class SkyEquBin {

    /** 升级包ID */
    @TableId
    @ApiModelProperty(value = "升级包ID", position = 1)
    private String id;

    /** 设备类型：1采集器；2并网逆变器；3储能逆变器 */
    @ApiModelProperty(value = "设备类型：1采集器；2并网逆变器；3储能逆变器", position = 2)
    private Integer equType;

    /** 适用型号 */
    @ApiModelProperty(value = "适用型号", position = 3)
    private String equModel;

    /** 适用模块：1采集器（wstk）、2主ARM（marm）、3辅ARM（sarm）、4DSP（mdsp）、5安规（sfty） */
    @ApiModelProperty(value = "适用模块：1采集器（wstk）、2主ARM（marm）、3辅ARM（sarm）、4DSP（mdsp）、5安规（sfty）", position = 4)
    private Integer equModule;

    /** 版本号 */
    @ApiModelProperty(value = "版本号", position = 5)
    private String binVer;

    /** 升级包大小 (bit)*/
    @ApiModelProperty(value = "升级包大小", position = 6)
    private Integer binSize;

    /** 升级包名 */
    @ApiModelProperty(value = "升级包名", position = 7)
    private String binName;

    /** 下载链接 */
    @ApiModelProperty(value = "下载链接", position = 8)
    private String binUrl;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 9)
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 10)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 11)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 12)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 13)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 14)
    private String tenantId;
}
