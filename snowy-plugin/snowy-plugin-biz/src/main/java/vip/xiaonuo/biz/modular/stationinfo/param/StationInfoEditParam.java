/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationinfo.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 电站信息编辑参数
 *
 * @author 全佳璧
 * @date  2023/09/12 10:51
 **/
@Getter
@Setter
public class StationInfoEditParam {

    /** 电站ID */
    @ApiModelProperty(value = "电站ID", required = true, position = 1)
    @NotBlank(message = "id不能为空")
    private String id;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称", position = 2)
    private String stationName;

    /** 电站备注 */
    @ApiModelProperty(value = "电站备注", position = 3)
    private String stationMemo;

    /** 建站日期 */
    @ApiModelProperty(value = "建站日期", position = 4)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date stationCreatetime;

    /** 装机容量 */
    @ApiModelProperty(value = "装机容量", position = 6)
    private Double stationSize;

    /** 电站类型 */
    @ApiModelProperty(value = "电站类型", position = 7)
    private Integer stationType;

    /** 电站系统 */
    @ApiModelProperty(value = "电站系统", position = 8)
    private Integer stationSystem;

    /** 并网状态 */
    @ApiModelProperty(value = "并网状态", position = 9)
    private Integer stationGridstatus;

    /** 并网时间 */
    @ApiModelProperty(value = "并网时间", position = 10)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date stationGridtime;

    /** 电站状态 */
    @ApiModelProperty(value = "电站状态", position = 11)
    private Integer stationStatus;

    /** 报警状态 */
    @ApiModelProperty(value = "报警状态", position = 12)
    private Integer stationAlarm;

    /** 业主ID */
    @ApiModelProperty(value = "业主ID", position = 13)
    private String stationContactid;

    /** 售电单价 */
    @ApiModelProperty(value = "售电单价", position = 14)
    private Double pricePer;

    /** 补贴单价 */
    @ApiModelProperty(value = "补贴单价", position = 15)
    private Double subsidyPer;

    /** 货币种类 */
    @ApiModelProperty(value = "货币种类", position = 16)
    private String priceType;

    /** 发电收益 */
    @ApiModelProperty(value = "发电收益", position = 17)
    private Double elecIncome;

    /** 补贴收益 */
    @ApiModelProperty(value = "补贴收益", position = 18)
    private Double subsidyIncome;

    /** 累计收益 */
    @ApiModelProperty(value = "累计收益", position = 19)
    private Double totalIncome;

    /** 是否公开 */
    @ApiModelProperty(value = "是否公开", position = 20)
    private Integer stationPublic;

    /** 累计发电量 */
    @ApiModelProperty(value = "累计发电量", position = 21)
    private Double stationTotalpower;

    /** 累计运行天数 */
    @ApiModelProperty(value = "累计运行天数", position = 22)
    private Integer stationRuntime;

    /** 设备总数 */
    @ApiModelProperty(value = "设备总数", position = 23)
    private Integer equipTotal;

    /** 节约用煤（吨） */
    @ApiModelProperty(value = "节约用煤（吨）", position = 24)
    private Double saveCoal;

    /** 减少CO2排放（吨） */
    @ApiModelProperty(value = "减少CO2排放（吨）", position = 25)
    private Double reduceCo2;

    /** 减少SO2排放（吨） */
    @ApiModelProperty(value = "减少SO2排放（吨）", position = 26)
    private Double reduceSo2;

    /** 等效植树量（棵） */
    @ApiModelProperty(value = "等效植树量（棵）", position = 27)
    private Double equivalentPlanting;

    /** 安装公司 */
    @ApiModelProperty(value = "安装公司", position = 28)
    private String stationCompany;

    /** 经度 */
    @ApiModelProperty(value = "经度", position = 29)
    private String areaLng;

    /** 纬度 */
    @ApiModelProperty(value = "纬度", position = 30)
    private String areaLat;

    /** 地址 */
    @ApiModelProperty(value = "地址", position = 31)
    private String areaDetail;

    /** 时区 */
    @ApiModelProperty(value = "时区", position = 32)
    private String timeZone;

    /** 总成本 */
    @ApiModelProperty(value = "总成本", position = 35)
    private String totalCost;

    /** 日还款 */
    @ApiModelProperty(value = "日还款", position = 36)
    private Double dayRepayment;

    /** 业主姓名 */
    @ApiModelProperty(value = "业主姓名", position = 37)
    private String userName;

    /** 业主电话 */
    @ApiModelProperty(value = "业主电话", position = 38)
    private String userTel;

    /** 业主工作单位 */
    @ApiModelProperty(value = "业主工作单位", position = 39)
    private String userAddr;

    /** 洲 */
    @ApiModelProperty(value = "洲", position = 40)
    private String areaContient;

    /** 国家 */
    @ApiModelProperty(value = "国家", position = 41)
    private String areaCountry;

    /** 省 */
    @ApiModelProperty(value = "省", position = 42)
    private String areaProvice;

    /** 市 */
    @ApiModelProperty(value = "市", position = 43)
    private String areaCity;

    /** 图片url */
    @ApiModelProperty(value = "图片url", position = 44)
    private String picUrl;

    /** 设备sn集合 */
    @ApiModelProperty(value = "设备sn集合", position = 44)
    private List<String> nbSns;

    /** 业主信息 */
    @ApiModelProperty(value = "业主信息", position = 13)
    private StationContactInfo stationContact;

}
