package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description:
 * @Author: wangjian
 * @CreateTime: 2023-11-15  14:01
 */
@Getter
@Setter
public class StationInfoOverviewPowerUsedVO {
    /** 用电功率 */
    @ApiModelProperty(value = "用电功率")
    private Double loadPower;
    /** 当日用电量 */
    @ApiModelProperty(value = "当日用电量")
    private String todayEnergy;
    /** 当月用电量 */
    @ApiModelProperty(value = "当月用电量")
    private String monthEnergy;
    /** 当年用电量 */
    @ApiModelProperty(value = "当年用电量")
    private String yearEnergy;
    /** 累计用电量 */
    @ApiModelProperty(value = "累计用电量")
    private String totalEnergy;

}