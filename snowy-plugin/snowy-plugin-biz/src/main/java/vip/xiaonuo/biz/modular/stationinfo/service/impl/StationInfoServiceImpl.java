/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationinfo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.config.StationInfoConfig;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.DoubleUtils;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.alarminfocurrent.entity.SkyAlarmInfoCurrent;
import vip.xiaonuo.biz.modular.alarminfocurrent.mapper.SkyAlarmInfoCurrentMapper;
import vip.xiaonuo.biz.modular.collectorinfo.param.SkyCollectorInfoPageResVO;
import vip.xiaonuo.biz.modular.eququality.param.SkyPsIdParam;
import vip.xiaonuo.biz.modular.equrelationship.entity.SkyEquRelationship;
import vip.xiaonuo.biz.modular.equrelationship.param.SkyEquRelationshipSNParam;
import vip.xiaonuo.biz.modular.equrelationship.service.SkyEquRelationshipService;
import vip.xiaonuo.biz.modular.installerstatistic.entity.SkyInstallerStatistic;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticIdParam;
import vip.xiaonuo.biz.modular.installerstatistic.service.SkyInstallerStatisticService;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.invinfo.param.SkySnAndEquTypeParam;
import vip.xiaonuo.biz.modular.invinfo.service.SkyInvInfoService;
import vip.xiaonuo.biz.modular.invinfo.service.impl.SkyInvInfoServiceImpl;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;
import vip.xiaonuo.biz.modular.invmonitorcurrent.mapper.SkyInvMonitorCurrentMapper;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.SkyInvInfoMonitorTimeParam;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.SkyInvInfoRankVO;
import vip.xiaonuo.biz.modular.invupgrade.entity.SkyInvUpgrade;
import vip.xiaonuo.biz.modular.invupgrade.mapper.SkyInvUpgradeMapper;
import vip.xiaonuo.biz.modular.stationequ.entity.SkyStationEqu;
import vip.xiaonuo.biz.modular.stationequ.service.SkyStationEquService;
import vip.xiaonuo.biz.modular.stationinfo.entity.Area;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationPower;
import vip.xiaonuo.biz.modular.stationinfo.mapper.AreaMapper;
import vip.xiaonuo.biz.modular.stationinfo.mapper.StationPowerMapper;
import vip.xiaonuo.biz.modular.stationinfo.param.*;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.service.SkyStationPowerService;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.param.SkyStationUserAddParam;
import vip.xiaonuo.biz.modular.stationuser.service.SkyStationUserService;
import vip.xiaonuo.biz.modular.timezone.service.SkyTimezoneService;
import vip.xiaonuo.biz.modular.user.entity.BizUser;
import vip.xiaonuo.biz.modular.user.mapper.BizUserMapper;
import vip.xiaonuo.biz.modular.user.param.BizUserAddParam;
import vip.xiaonuo.biz.modular.user.service.BizUserService;
import vip.xiaonuo.biz.modular.userrelationship.entity.SkyUserRelationship;
import vip.xiaonuo.biz.modular.userrelationship.param.SkyUserRelationshipAddParam;
import vip.xiaonuo.biz.modular.userrelationship.service.SkyUserRelationshipService;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.mapper.StationInfoMapper;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import cn.hutool.json.JSONObject;
import cn.hutool.core.annotation.AnnotationUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.annotation.Resource;

import vip.xiaonuo.common.util.CommonCryptogramUtil;
import vip.xiaonuo.dbs.api.DbsApi;
import vip.xiaonuo.dev.api.DevDfcApi;
import vip.xiaonuo.sys.api.SysUserApi;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * 电站信息Service接口实现类
 *
 * @author 全佳璧
 * @date  2023/09/12 10:51
 **/
@Slf4j
@Service
public class StationInfoServiceImpl extends ServiceImpl<StationInfoMapper, StationInfo> implements StationInfoService {

    @Resource
    private DbsApi dbsApi;

    @Resource
    private DevDfcApi devDfcApi;

    @Resource
    private BizUserService bizUserService;

    @Resource
    private BizUserMapper bizUserMapper;

    @Resource
    private StationInfoMapper stationInfoMapper;

    @Resource
    private AreaMapper areaMapper;

    @Resource
    private SkyInvInfoMapper skyInvInfoMapper;

    @Resource
    private SkyInvMonitorCurrentMapper skyInvMonitorCurrentMapper;

    @Resource
    private StationPowerMapper stationPowerMapper;

    @Autowired
    private StationInfoConfig stationInfoConfig;
    @Resource
    private SkyTimezoneService timezoneService;

    @Resource
    private SkyStationEquService skyStationEquService;

    @Resource
    private SkyStationUserService skyStationUserService;

    @Resource
    private SkyUserRelationshipService skyUserRelationshipService;

    @Resource
    private SkyInstallerStatisticService skyInstallerStatisticService;

    @Resource
    private SkyInvUpgradeMapper skyInvUpgradeMapper;

    @Resource
    private SkyEquRelationshipService skyEquRelationshipService;

    @Resource
    private SysUserApi sysUserApi;

    @Resource
    private SkyStationPowerService skyStationPowerService;

    @Resource
    private SkyInvInfoServiceImpl skyInvInfoServiceImpl;

    @Resource
    private SkyInvInfoService skyInvInfoService;

    @Resource
    private SkyAlarmInfoCurrentMapper skyAlarmInfoCurrentMapper;



    @Override
    public Page<StationInfoPageModel> page(StationInfoPageParam stationInfoPageParam) {
        Page<StationInfoPageModel> pageDataModel = new Page<>(stationInfoPageParam.getCurrent(),stationInfoPageParam.getSize());
        if (Objects.isNull(stationInfoPageParam.getStationSizeBegin()) && !Objects.isNull(stationInfoPageParam.getStationSizeEnd())) {
            stationInfoPageParam.setStationSizeBegin(Double.MIN_VALUE);
        }
        if (Objects.isNull(stationInfoPageParam.getStationSizeEnd()) && !Objects.isNull(stationInfoPageParam.getStationSizeBegin())) {
            stationInfoPageParam.setStationSizeEnd(Double.MAX_VALUE);
        }
        List<StationInfoPageModel> list = stationInfoMapper.pageStationInfo(stationInfoPageParam);
        if(!CollectionUtils.isEmpty(list)){
            List<String> installerIdList = list.stream().map(StationInfoPageModel::getStationCompany).collect(Collectors.toList());
            List<BizUser> bizUsers = bizUserMapper.selectList(new QueryWrapper<BizUser>().lambda().in(BizUser::getId, installerIdList));
            Map<String, String> map = bizUsers.stream().collect(Collectors.toMap(BizUser::getId, BizUser::getAccount));
//        for (StationInfoPageModel info:list) {
//            // 修改更新时间修改为对应该时区时间
//            try {
//                this.updateTime(info);
//            } catch (ParseException e) {
//                // 更新时间时区换算错误！
//                throw new SkyCommonException(SkyCommonExceptionEnum.TIME_ZONE_ERROR_9105);
//            }
//        }
            List<String> ids = list.stream().map(StationInfoPageModel::getId).collect(Collectors.toList());

            // 电站状态 sql对比的是修改时间
            // 根据电站id获取离线数量
            Map<String, Integer> offlineMap = getOfflineNumMapByPsIds(ids);
            // 根据电站id获取设备数量
            Map<String, Integer> equNumMap = getEquNumByPsIds(ids);

            for (StationInfoPageModel model:list) {
                if (ObjectUtil.isNotEmpty(model.getStationContact()) && ObjectUtil.isNotEmpty(model.getStationContact().getUserTel())) {
                    model.getStationContact().setUserTel(CommonCryptogramUtil.doSm4CbcDecrypt(model.getStationContact().getUserTel()));
                }
                if (ObjectUtil.isNotEmpty(model.getStationCompany())) {
                    model.setStationCompanyName(map.getOrDefault(model.getStationCompany(),""));
                }
                // 电站状态
                int stationStatus = stationStatus(offlineMap, equNumMap, model.getId());
                model.setStationStatus(stationStatus);
                //最近更新时间，根据时区
                if(ObjectUtil.isNotEmpty(model.getUpdateTime()) && ObjectUtil.isNotEmpty(model.getTimeZone())){
                    model.setUpdateTime(timeZone(model.getUpdateTime(),model.getTimeZone()));
                }
            }
        }
        pageDataModel.setRecords(list);
        pageDataModel.setTotal(stationInfoMapper.countNum(stationInfoPageParam));
        return pageDataModel;
    }

    private int stationStatus(Map<String, Integer> offlineMap, Map<String, Integer> equNumMap, String stationId) {
        int status = 0;
        Integer offline = offlineMap.getOrDefault(stationId, 0);
        Integer count = equNumMap.getOrDefault(stationId,0);
        if(offline > 0 && count > 0 && count.compareTo(offline) == 0){
            //没有离线
            status = 1;
        } else if(offline >0 && count.compareTo(offline) > 0){
            //部分离线
            status = 2;
        } else if(offline ==0 && count != 0){
            // 全部离线
            status = 3;
        }else{
            //接入中
        }
        return status;
    }

    @NotNull
    private Map<String, Integer> getEquNumByPsIds(List<String> ids) {
        Map<String, Integer> equNumMap = new HashMap<>();
        List<PsIdDeviceCountParam>  equCount = skyInvInfoMapper.getEquNum(ids);
        if (!CollectionUtils.isEmpty(equCount)) {
            equNumMap = equCount.stream().collect(Collectors.toMap(PsIdDeviceCountParam::getPsId, PsIdDeviceCountParam::getNum));
        }
        return equNumMap;
    }

    /**
     * 根据电站id获取离线数量
     * @param ids
     * @return
     */
    @NotNull
    private Map<String, Integer> getOfflineNumMapByPsIds(List<String> ids) {
        Map<String, Integer> offlineMap = new HashMap<>();
        String offlineTime = LocalDateTime.now().minusMinutes(15).format(LocalDateUtils.DATETIME_FORMATTER);
        List<PsIdDeviceOfflineAndPowerParam> offlineList = stationInfoMapper.getPsDeviceStatusCountBatch(ids,offlineTime);
        if (!CollectionUtils.isEmpty(offlineList)) {
            offlineMap = offlineList.stream().collect(Collectors.toMap(PsIdDeviceOfflineAndPowerParam::getPsId, PsIdDeviceOfflineAndPowerParam::getNum));
        }
        return offlineMap;
    }

    /**
     * 根据时区计算时间
     * @param date
     * @param timeZoneId
     * @return
     */
    public Date timeZone(Date date, String timeZoneId){
        Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.of(timeZoneId);
        // 将Instant转换为ZonedDateTime
        ZonedDateTime zonedDateTime = instant.atZone(zoneId);
        // 创建一个DateTimeFormatter对象，指定日期和时间的格式
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        // 使用DateTimeFormatter将ZonedDateTime转换为String
        String formattedDateTime = zonedDateTime.format(formatter);
        LocalDateTime localDateTime = LocalDateTime.parse(formattedDateTime, formatter);
        Date from = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return from;
    }

    /**
     * @description: 更新时间修改为对应时区时间
     * @author: wangjian
     * @date: 2023/10/10 15:46
     **/
    private void updateTime(StationInfoPageModel info) throws ParseException {
        if (StringUtils.isEmpty(info.getTimeZone()) || Objects.isNull(info.getUpdateTime()) || stationInfoConfig.getTimeZone().equals(info.getTimeZone())) {
            return;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            info.setUpdateTime(
                    formTimeZone(sdfTime.format(info.getUpdateTime()), info.getTimeZone())
            );
        }

    }

    /**
     * @description: 新增时区修改
     * @author: wangjian
     * @date: 2023/10/10 15:46
     **/
    private void checkTimeZone(StationInfoAddParam stationInfoAddParam) throws ParseException {
        if (StringUtils.isEmpty(stationInfoAddParam.getTimeZone()) || stationInfoConfig.getTimeZone().equals(stationInfoAddParam.getTimeZone())) {
            return;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if (!Objects.isNull(stationInfoAddParam.getStationCreatetime())) {
                stationInfoAddParam.setStationCreatetime(
                        formTimeZone(sdf.format(stationInfoAddParam.getStationCreatetime()), stationInfoAddParam.getTimeZone())
                );
            }
            if (!Objects.isNull(stationInfoAddParam.getStationGridtime())) {
                stationInfoAddParam.setStationGridtime(
                        formTimeZone(sdf.format(stationInfoAddParam.getStationGridtime()), stationInfoAddParam.getTimeZone())
                );
            }
        }

    }


    private Date formTimeZone(String time, String timeZone) throws ParseException {
        SimpleDateFormat formatter;
        if (time.length()>10) {
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        } else {
            formatter = new SimpleDateFormat("yyyy-MM-dd");
        }
        // 设置源时区
        formatter.setTimeZone(TimeZone.getTimeZone(stationInfoConfig.getTimeZone()));
        Date sourceDate = formatter.parse(time);
        // 设置目标时区
        formatter.setTimeZone(TimeZone.getTimeZone(timeZone));
        String targetDate = formatter.format(sourceDate);
        if (time.length()>10) {
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        } else {
            formatter = new SimpleDateFormat("yyyy-MM-dd");
        }
        return formatter.parse(targetDate);
    }

    /**
     * @description: 并网状态判断
     * @author: wangjian
     * @date: 2023/10/10 15:21
     **/
    private boolean gridStatus(Date stationGridTime, String zoneId){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String gridTime = format.format(stationGridTime);
        LocalDate ld = LocalDate.parse(gridTime);
        boolean after = ld.isAfter(skyInvInfoServiceImpl.getNowTimeByZoneId(zoneId).toLocalDate());
        return after;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(StationInfoAddParam stationInfoAddParam) {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            stationInfoAddParam.setStationContactid(loginUser.getId());
        }
        // 并网状态
        if (!Objects.isNull(stationInfoAddParam.getStationGridtime())) {
            boolean after = this.gridStatus(stationInfoAddParam.getStationGridtime(),
                    StringUtils.isEmpty(stationInfoAddParam.getTimeZone()) ? stationInfoAddParam.getTimeZone() : skyInvInfoServiceImpl.timeZoneOperation());
            if (after) {
                stationInfoAddParam.setStationGridstatus(0);
            } else {
                stationInfoAddParam.setStationGridstatus(1);
            }
        }
        StationInfo stationInfo = BeanUtil.toBean(stationInfoAddParam, StationInfo.class);
        //并网时间
        if(!CollectionUtils.isEmpty(stationInfoAddParam.getNbSns())){
            stationGridDate(stationInfoAddParam.getNbSns(),stationInfoAddParam.getTimeZone(),stationInfo);
        }
        //建站日期为当前日期formatLocalDateTime
        String nowDate = LocalDateUtils.formatLocalDateTime(LocalDateTime.now(), LocalDateUtils.DATE_FORMATTER);
        try {
            Date date = formTimeZone(nowDate, stationInfoAddParam.getTimeZone());
            stationInfo.setStationCreatetime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // 系统功率比
        this.sysRate(stationInfo);
        // 业主信息
//        if (StringUtils.isNotEmpty(stationInfoAddParam.getUserTel())) {
//            String userId = this.checkOrAddUser(stationInfoAddParam);
//            stationInfo.setStationContactid(userId);
//        }

        // 时区
//        try {
//            checkTimeZone(stationInfoAddParam);
//        } catch (ParseException e) {
//            throw new SkyCommonException(SkyCommonExceptionEnum.TIME_ZONE_ERROR_9105);
//        }

        // 地区信息
        Area area = BeanUtil.copyProperties(stationInfoAddParam,Area.class);
        int i = areaMapper.insert(area);
        if (i!=1) {
            throw new SkyCommonException(SkyCommonExceptionEnum.AREA_SAVE_DEFAULT_9106);
        }

        stationInfo.setAreaId(area.getId());
        stationInfo.setUpdateTime(new Date());

//        if(StringUtils.isNotEmpty(stationInfoAddParam.getStationCompany())){
//            JSONObject userByIdWithException = sysUserApi.getUserByIdWithException(stationInfoAddParam.getStationCompany());
//            stationInfo.setStationCompany(String.valueOf(userByIdWithException.get("account")));
//        }
        if(CollectionUtils.isEmpty(stationInfoAddParam.getNbSns())){
            stationInfo.setStationStatus(0);
        }

        stationInfoMapper.insert(stationInfo);
        // 绑定设备
        if (stationInfoAddParam.getNbSns() != null && stationInfoAddParam.getNbSns().size()>0) {
            List<String> nbSns = stationInfoAddParam.getNbSns();
            // 绑定
            this.bindEqu(stationInfo.getId(),nbSns);

            // 电站设备关系表
            this.insertStationEqu(stationInfo,nbSns);

            // 升级表关联
            extracted(stationInfo, nbSns);
        }

        // 电站用户关系 sky_station_user
        stationInfo.setStationCompany(stationInfoAddParam.getStationCompany());
        this.insertStationUserRelation(stationInfo);
    }

    @Override
    public void stationGridDate(List<String> nbSns, String timeZone, StationInfo stationInfo){
        List<SkyInvMonitorCurrent> invMonitorCurrentList = skyInvMonitorCurrentMapper.selectList(Wrappers.lambdaQuery(SkyInvMonitorCurrent.class)
                .in(SkyInvMonitorCurrent::getInvSn, nbSns));
        if(!CollectionUtils.isEmpty(invMonitorCurrentList)){
            Optional<Date> earliestCreateTime = invMonitorCurrentList.stream()
                    .map(SkyInvMonitorCurrent::getMonitorTimeFormat)
                    .filter(Objects::nonNull)
                    .min(Date::compareTo);
            earliestCreateTime.ifPresent(date -> stationInfo.setStationGridtime(timeZone(date, timeZone)));
            //已并网
            stationInfo.setStationGridstatus(1);
        }
    }


    /**
     * add电站升级表关联
     * @param stationInfo
     * @param nbSns
     */
    private void extracted(StationInfo stationInfo, List<String> nbSns) {
        List<SkyInvUpgrade> skyInvUpgrades = skyInvUpgradeMapper.selectList(new QueryWrapper<SkyInvUpgrade>().lambda().in(SkyInvUpgrade::getEquSn,nbSns));
        if (!CollectionUtils.isEmpty(skyInvUpgrades)) {
            Map<String, SkyInvUpgrade> upgradeMap = skyInvUpgrades.stream().collect(Collectors.toMap(SkyInvUpgrade::getEquSn, Function.identity()));
            for (String invSn: nbSns) {
                if (upgradeMap.containsKey(invSn)) {
                    skyInvUpgradeMapper.update(null,new UpdateWrapper<SkyInvUpgrade>().lambda()
                            .set(SkyInvUpgrade::getStationId, stationInfo.getId()).eq(SkyInvUpgrade::getEquSn,invSn));
                } else {
                    SkyEquRelationshipSNParam snParam = new SkyEquRelationshipSNParam();
                    snParam.setEquSn(invSn);
                    String collectorSn = skyEquRelationshipService.getCollectorSn(snParam);
                    SkyInvUpgrade upgrade = new SkyInvUpgrade();
                    upgrade.setCollectSn(collectorSn);
                    upgrade.setEquSn(invSn);
                    upgrade.setStationId(stationInfo.getId());
                    skyInvUpgradeMapper.insert(upgrade);
                }
            }
        } else {
            for (String invSn: nbSns) {
                SkyEquRelationshipSNParam snParam = new SkyEquRelationshipSNParam();
                snParam.setEquSn(invSn);
                String collectorSn = skyEquRelationshipService.getCollectorSn(snParam);
                SkyInvUpgrade upgrade = new SkyInvUpgrade();
                upgrade.setCollectSn(collectorSn);
                upgrade.setEquSn(invSn);
                upgrade.setStationId(stationInfo.getId());
                skyInvUpgradeMapper.insert(upgrade);
            }
        }
    }

    /**
     * 电站用户关系表新增
     * @param stationInfo
     */
    public void insertStationUserRelation(StationInfo stationInfo){
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (roleCodeList.contains(SkyRoleCodeEnum.SUPERADMIN.getValue())){
            // 管理员add
            SkyStationUserAddParam stationUser = new SkyStationUserAddParam();
            stationUser.setStationId(stationInfo.getId());
            if (ObjectUtil.isEmpty(stationInfo.getStationCompany()) && ObjectUtil.isEmpty(stationInfo.getStationContactid())) {
                throw new SkyCommonException(SkyCommonExceptionEnum.INSTALLER_AND_USER_ALL_NULL_9138);
            }
            if (ObjectUtil.isNotEmpty(stationInfo.getStationContactid())) {
                // 新增电站没有填写安装商，则查询用户关系表
                SkyUserRelationship userRelationship = getFromUserRelationshipByUserIdAndLevel(stationInfo.getStationContactid(), 0);
                if (userRelationship != null && StringUtils.isNotEmpty(userRelationship.getInstallerId())) {
                    // 安装商
                    if (ObjectUtil.isEmpty(stationInfo.getStationCompany())) {
                        stationUser.setInstallerIdL1(userRelationship.getInstallerId());
                        stationInfo.setStationCompany(userRelationship.getInstallerId());
                    } else {
                        stationUser.setInstallerIdL1(stationInfo.getStationCompany());
                    }
                    // 分销商
                    SkyUserRelationship relation = getFromUserRelationshipByUserIdAndLevel(stationInfo.getStationCompany(), 1);
                    if (relation != null && StringUtils.isNotEmpty(relation.getInstallerId())) {
                        stationUser.setInstallerIdL2(userRelationship.getInstallerId());
                    }
                }
                stationUser.setUserId(stationInfo.getStationContactid());
            } else if (ObjectUtil.isNotEmpty(stationInfo.getStationCompany())) {
                stationUser.setInstallerIdL1(stationInfo.getStationCompany());
                // 查询设置分销商id
                SkyUserRelationship userRelationship = getFromUserRelationshipByUserIdAndLevel(stationInfo.getStationCompany(), 1);
                if (userRelationship != null && StringUtils.isNotEmpty(userRelationship.getUserId())) {
                    stationUser.setInstallerIdL2(userRelationship.getInstallerId());
                }
            }
            skyStationUserService.add(stationUser);
        } else if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())){
            // 分销商新增电站
            SkyStationUserAddParam stationUser = new SkyStationUserAddParam();
            stationUser.setStationId(stationInfo.getId());
            stationUser.setInstallerIdL2(loginUser.getId());
            if (ObjectUtil.isNotEmpty(stationInfo.getStationCompany())) {
                stationUser.setInstallerIdL1(stationInfo.getStationCompany());
            }
            if (ObjectUtil.isNotEmpty(stationInfo.getStationContactid())) {
                stationUser.setUserId(stationInfo.getStationContactid());
            }
            skyStationUserService.add(stationUser);
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            // 安装商新增电站
            SkyStationUserAddParam stationUser = new SkyStationUserAddParam();
            stationUser.setStationId(stationInfo.getId());
            stationUser.setInstallerIdL1(loginUser.getId());
            // 用户
            if (ObjectUtil.isNotEmpty(stationInfo.getStationContactid())) {
                stationUser.setUserId(stationInfo.getStationContactid());
            }
            // 分销商
            SkyUserRelationship userRelationship = getFromUserRelationshipByUserIdAndLevel(loginUser.getId(), 1);
            if (userRelationship != null && StringUtils.isNotEmpty(userRelationship.getUserId())) {
                stationUser.setInstallerIdL2(userRelationship.getInstallerId());
            }
            skyStationUserService.add(stationUser);
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            // 用户新增电站
            SkyStationUserAddParam stationUser = new SkyStationUserAddParam();
            stationUser.setStationId(stationInfo.getId());
            stationUser.setUserId(loginUser.getId());
            // 安装商
            if (ObjectUtil.isNotEmpty(stationInfo.getStationCompany())) {
                stationUser.setInstallerIdL1(stationInfo.getStationCompany());
                // 分销商
                SkyUserRelationship userRelationship = getFromUserRelationshipByUserIdAndLevel(stationInfo.getStationCompany(), 1);
                if (userRelationship != null && StringUtils.isNotEmpty(userRelationship.getUserId())) {
                    stationUser.setInstallerIdL2(userRelationship.getUserId());
                }
            } else {
                // 新增电站没有填写安装商，则查询用户关系表
                SkyUserRelationship userRelationship = getFromUserRelationshipByUserIdAndLevel(loginUser.getId(), 0);
                if (userRelationship != null && StringUtils.isNotEmpty(userRelationship.getInstallerId())) {
                    stationUser.setInstallerIdL1(userRelationship.getInstallerId());
                    // 分销商
                    SkyUserRelationship relation = getFromUserRelationshipByUserIdAndLevel(userRelationship.getInstallerId(), 1);
                    if (relation != null && StringUtils.isNotEmpty(relation.getUserId())) {
                        stationUser.setInstallerIdL2(userRelationship.getUserId());
                    }
                }
            }
            skyStationUserService.add(stationUser);
        }
    }

    // 多级安装商 0：安装商/业主；1：分销商/安装商
    private SkyUserRelationship getFromUserRelationshipByUserIdAndLevel(String id, Integer level) {
        SkyUserRelationship userRelationship = skyUserRelationshipService.getOne(new QueryWrapper<SkyUserRelationship>().lambda()
                .eq(SkyUserRelationship::getUserId, id)
                .eq(SkyUserRelationship::getInstallerLevel,level));
        return userRelationship;
    }

    // 多级安装商 0：安装商/业主；1：分销商/安装商
    private SkyUserRelationship getFromUserRelationshipByInstallerIdAndLevel(String id, Integer level) {
        SkyUserRelationship userRelationship = skyUserRelationshipService.getOne(new QueryWrapper<SkyUserRelationship>().lambda()
                .eq(SkyUserRelationship::getInstallerId, id)
                .eq(SkyUserRelationship::getInstallerLevel,level));
        return userRelationship;
    }

    private void insertStationEqu(StationInfo stationInfo, List<String> nbSns){
        List<SkySnAndEquTypeParam> snEquTypeList = skyInvInfoMapper.getEquType(nbSns);
        Map<String, SkySnAndEquTypeParam> snEquTypeMap = snEquTypeList.stream().collect(Collectors.toMap(SkySnAndEquTypeParam::getSn, Function.identity()));
        List<SkyStationEqu> stationEquList = new ArrayList<>();
        for (String sn : nbSns) {
            SkySnAndEquTypeParam typeParam = new SkySnAndEquTypeParam();
            SkySnAndEquTypeParam orDefault = snEquTypeMap.getOrDefault(sn, typeParam);
            SkyStationEqu stationEqu = new SkyStationEqu();
            stationEqu.setStationId(stationInfo.getId());
            stationEqu.setStationName(stationInfo.getStationName());
            stationEqu.setEquType(String.valueOf(orDefault.getEquType()));
            stationEqu.setEquSn(sn);
            stationEqu.setModelId(orDefault.getModelId());

            stationEquList.add(stationEqu);
        }
        skyStationEquService.saveBatch(stationEquList);
    }

    /**
     * 设备绑定电站
     * @param stationId
     * @param nbSns
     */
    private void bindEqu(String stationId, List<String> nbSns){
        QueryWrapper<SkyInvInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().in(SkyInvInfo::getInvSn,nbSns);
        List<SkyInvInfo> invInfos = skyInvInfoMapper.selectList(queryWrapper);
        List<SkyInvInfo> collect = invInfos.stream().filter(SkyInvInfo -> StringUtils.isNotEmpty(SkyInvInfo.getStationId())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(collect)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.INV_INSTALLED_PS_9109, "SN:" + JSON.toJSONString(collect.stream().map(SkyInvInfo::getInvSn).collect(Collectors.toList())));
        }
        UpdateWrapper<SkyInvInfo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda().set(SkyInvInfo::getStationId,stationId).in(SkyInvInfo::getInvSn,nbSns);
        skyInvInfoMapper.update(null,updateWrapper);
    }


    /**
     * @description: 系统功率比
     *      系统功率比是衡量一座光伏电站发电能力的重要指标之一，用于衡量电站当前发电能力。
     *      计算公式：系统功率比=当前发电功率/装机容量
     *      例如系统功率比为80%，表示此电站以额定功率的80%运行。
     * @author: wangjian
     * @date: 2023/10/11 13:47
     **/
    private void sysRate(StationInfo stationInfo) {
        if (stationInfo.getStationSize() == null || stationInfo.getStationSize() == 0D ||stationInfo.getElecEfficiency() == null) {
            stationInfo.setStationSysRate("0");
            return;
        }
        double rate = stationInfo.getElecEfficiency()  / stationInfo.getStationSize() * 100;
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        stationInfo.setStationSysRate(decimalFormat.format(rate));
    }

    /**
     * 国内用户手机号注册
     * @param stationInfoAddParam
     * @return
     */
    public String checkOrAddUser(StationInfoAddParam stationInfoAddParam) {
        QueryWrapper<BizUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(BizUser::getPhone, CommonCryptogramUtil.doSm4CbcEncrypt(stationInfoAddParam.getUserTel()));
        BizUser user = bizUserService.getOne(queryWrapper);
        // 没有该号码的业主则新增一个业主用户
        if (user == null) {
            // 默认账号和手机号相同、业主单位地址存入扩展字段里面
            BizUserAddParam userAddParam = new BizUserAddParam();
            userAddParam.setAccount(stationInfoAddParam.getUserTel());
            userAddParam.setName(stationInfoAddParam.getUserName());
            userAddParam.setPhone(stationInfoAddParam.getUserTel());
            userAddParam.setExtJson(stationInfoAddParam.getUserAddr());
            // 机构id直接设置为 安装商枚举值 岗位设为0
            userAddParam.setOrgId(stationInfoAddParam.getStationCompany());
            userAddParam.setPositionId("0");
            BizUser bizUser = BeanUtil.toBean(userAddParam, BizUser.class);
            bizUserMapper.insert(bizUser);

            // 用户(层级)关系表sky_user_relationship
            if (ObjectUtil.isEmpty(stationInfoAddParam.getStationCompany())) {
                throw new SkyCommonException(SkyCommonExceptionEnum.INSTALLER_NOT_NULL_9123);
            }
            SkyUserRelationshipAddParam addParam = new SkyUserRelationshipAddParam();
            if (ObjectUtil.isNull(bizUser.getId())) {
                throw new SkyCommonException("user failed to add sky_user_relationship for userId is null");
            }
            addParam.setUserId(bizUser.getId());
            addParam.setInstallerId(stationInfoAddParam.getStationCompany());
            addParam.setInstallerLevel(0);
            skyUserRelationshipService.add(addParam);

            return bizUser.getId();
        }
        // 和已存在的业主不同
        if (CommonCryptogramUtil.doSm4CbcEncrypt(stationInfoAddParam.getUserTel()).equals(user.getPhone())
                && (!stationInfoAddParam.getUserName().equals(user.getName()) || !stationInfoAddParam.getUserAddr().equals(user.getExtJson()))) {
        // 手机号已存在，且与已存在业主姓名或工作单位地址不一样
            throw new SkyCommonException(SkyCommonExceptionEnum.EXIST_OWNER_9104, "name:"  + user.getName() + ", address:" + user.getExtJson());
        }
        // 和已存在的业主相同
        return user.getId();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(StationInfoEditParam stationInfoEditParam) {
        // 0、登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            stationInfoEditParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            stationInfoEditParam.setStationContactid(loginUser.getId());
        }
        // 1、并网状态
        if (!Objects.isNull(stationInfoEditParam.getStationGridtime())) {
            boolean after = this.gridStatus(stationInfoEditParam.getStationGridtime() ,
                    StringUtils.isEmpty(stationInfoEditParam.getTimeZone()) ? stationInfoEditParam.getTimeZone() : skyInvInfoServiceImpl.timeZoneOperation());
            if (after) {
                stationInfoEditParam.setStationGridstatus(0);
            } else {
                stationInfoEditParam.setStationGridstatus(1);
            }
        }
        // 2、时区
//        try {
//            this.checkTimeZoneEdit(stationInfoEditParam);
//        } catch (ParseException e) {
//            throw new SkyCommonException(SkyCommonExceptionEnum.TIME_ZONE_ERROR_9105);
//        }

        // 3、更新电站表信息
        StationInfo stationInfo = this.queryEntity(stationInfoEditParam.getId());
        BeanUtil.copyProperties(stationInfoEditParam, stationInfo);
        //并网时间
        if(!CollectionUtils.isEmpty(stationInfoEditParam.getNbSns())){
            stationGridDate(stationInfoEditParam.getNbSns(),stationInfoEditParam.getTimeZone(),stationInfo);
        }
        // 4、系统功率比计算
        this.sysRate(stationInfo);
        //把安装商的名字写到电站表中
//        if(StringUtils.isNotEmpty(stationInfoEditParam.getStationCompany())){
//            JSONObject userByIdWithException = sysUserApi.getUserByIdWithException(stationInfoEditParam.getStationCompany());
//            stationInfo.setStationCompany(String.valueOf(userByIdWithException.get("account")));
//        }
        if(stationInfoEditParam.getNbSns().size()==0){
            stationInfo.setStationStatus(0);
        }
        this.updateById(stationInfo);
        // 5、更新区域表信息
        if (StringUtils.isNotEmpty(stationInfo.getAreaId())) {
            Area area = areaMapper.selectById(stationInfo.getAreaId());
            if (ObjectUtil.isEmpty(area)) {
                area = new Area();
                BeanUtil.copyProperties(stationInfoEditParam, area);
                area.setId(null);
                areaMapper.insert(area);
                stationInfo.setAreaId(area.getId());
            } else {
                BeanUtil.copyProperties(stationInfoEditParam, area);
                area.setId(stationInfo.getAreaId());
                areaMapper.updateById(area);
            }
        } else {
            Area area = new Area();
            BeanUtil.copyProperties(stationInfoEditParam, area);
            area.setId(stationInfo.getAreaId());
            areaMapper.insert(area);
        }

        // 6、修改已绑定sn，更新绑定sn
        SkyPsIdParam skyPsIdParam = new SkyPsIdParam();
        skyPsIdParam.setStationId(stationInfoEditParam.getId());
        List<SkyInvInfo> invInfos = skyInvInfoMapper.getEquipSnList(skyPsIdParam);
        if (!CollectionUtils.isEmpty(invInfos)) {
            List<String> sns  = invInfos.stream().map(SkyInvInfo::getInvSn).collect(Collectors.toList());
            UpdateWrapper<SkyInvInfo> updateWrapper = new UpdateWrapper<>();
            updateWrapper.lambda().set(SkyInvInfo::getStationId,null).in(SkyInvInfo::getInvSn,sns);
            skyInvInfoMapper.update(null,updateWrapper);
        }
        if (!CollectionUtils.isEmpty(stationInfoEditParam.getNbSns())){
            this.bindEqu(stationInfo.getId(), stationInfoEditParam.getNbSns());

            // 更新电站设备表
            QueryWrapper<SkyStationEqu> qw = new QueryWrapper<>();
            qw.lambda().eq(SkyStationEqu::getStationId,stationInfoEditParam.getId());
            List<SkyStationEqu> stationEqus = skyStationEquService.list(qw);
            List<String> equBindedSns = stationEqus.stream().map(SkyStationEqu::getEquSn).collect(Collectors.toList());
            List<String> newAddSn = new ArrayList<>(stationInfoEditParam.getNbSns());
            List<String> toDelSn = new ArrayList<>(equBindedSns);
            newAddSn.removeAll(equBindedSns);
            toDelSn.removeAll(stationInfoEditParam.getNbSns());
            if (!CollectionUtils.isEmpty(toDelSn)) {
                QueryWrapper<SkyStationEqu> del = new QueryWrapper<>();
                del.lambda().in(SkyStationEqu::getEquSn,toDelSn);
                skyStationEquService.remove(del);
            }
            if (!CollectionUtils.isEmpty(newAddSn)) {
                this.insertStationEqu(stationInfo, newAddSn);
            }
        }else{
            //把电站状态改为待接入
            UpdateWrapper<StationInfo> updateWrapper = new UpdateWrapper<>();
            updateWrapper.lambda().set(StationInfo::getStationStatus,0);
            this.update(updateWrapper);
            //没有传NbSns说明该电站的设备被清空了
            skyStationEquService.remove(Wrappers.lambdaQuery(SkyStationEqu.class).eq(SkyStationEqu::getStationId,stationInfoEditParam.getId()));
        }

        // 7、更新电站用户关系表sky_station_user
        SkyStationUser stationUser = skyStationUserService.getOne(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getStationId, stationInfoEditParam.getId()));
        UpdateWrapper<SkyStationUser> updateWrapper = new UpdateWrapper<>();
        boolean updateFlag = false;
        if (stationUser != null) {
            if(StringUtils.isEmpty(stationInfoEditParam.getUserName())){
                updateWrapper.lambda().set(SkyStationUser::getUserId,null);
            }
            if (StringUtils.isNotEmpty(stationInfoEditParam.getStationCompany()) && !stationInfoEditParam.getStationCompany().equals(stationUser.getInstallerIdL1())) {
                updateWrapper.lambda().set(SkyStationUser::getInstallerIdL1,stationInfoEditParam.getStationCompany());
                SkyUserRelationship relationship = getFromUserRelationshipByInstallerIdAndLevel(stationInfoEditParam.getStationCompany(), 1);
                if (relationship != null && ObjectUtil.isNotEmpty(relationship.getInstallerId())) {
                    updateWrapper.lambda().set(SkyStationUser::getInstallerIdL2,relationship.getInstallerId());
                }
                updateFlag = true;
            }
            if (ObjectUtil.isEmpty(stationUser.getInstallerIdL2())) {
                SkyUserRelationship relationship = getFromUserRelationshipByUserIdAndLevel(stationInfoEditParam.getStationCompany(), 1);
                if (relationship != null && ObjectUtil.isNotEmpty(relationship.getInstallerId())) {
                    updateWrapper.lambda().set(SkyStationUser::getInstallerIdL2,relationship.getInstallerId());
                }
                updateFlag = true;
            }
            //更新安装商id的时候把安装上绑定的分销商也更新上去
            SkyUserRelationship skyUserRelationship = skyUserRelationshipService.getOne(Wrappers.lambdaQuery(SkyUserRelationship.class).eq(SkyUserRelationship::getUserId, stationInfoEditParam.getStationCompany())
                    .eq(SkyUserRelationship::getInstallerLevel,1));
            if(ObjectUtil.isNotEmpty(skyUserRelationship)){
                updateWrapper.lambda().set(SkyStationUser::getInstallerIdL2,skyUserRelationship.getInstallerId());
                updateFlag = true;
            }
        } else {
            // 新增电站用户关系表sky_station_user
            this.insertStationUserRelation(stationInfo);
        }
        // 8、业主信息跟新库
        if (StringUtils.isNotEmpty(stationInfo.getStationContactid())) {
//            BizUser bizUser = bizUserService.getById(stationInfo.getStationContactid());
//            // 如果手机号都存在且不一样需要检查是否已存在用户
//            if (bizUser!= null && StringUtils.isNotEmpty(bizUser.getPhone())
//                    && StringUtils.isNotEmpty(stationInfoEditParam.getUserTel())
//                    && !CommonCryptogramUtil.doSm4CbcEncrypt(stationInfoEditParam.getUserTel()).equals(bizUser.getPhone())) {
//                QueryWrapper<BizUser> queryWrapper = new QueryWrapper<>();
//                queryWrapper.lambda().eq(BizUser::getPhone, CommonCryptogramUtil.doSm4CbcEncrypt(stationInfoEditParam.getUserTel()));
//                BizUser otherUser = bizUserService.getOne(queryWrapper);
//                // 和已存在的业主不同
//                if (otherUser != null && CommonCryptogramUtil.doSm4CbcEncrypt(stationInfoEditParam.getUserTel()).equals(otherUser.getPhone())
//                        && (!stationInfoEditParam.getUserName().equals(otherUser.getExtJson())
//                        || !stationInfoEditParam.getUserAddr().equals(otherUser.getExtJson()))) {
////                    throw new CommonException("手机号已存在，且与已存在业主姓名或工作单位地址不一样");
//                    throw new SkyCommonException(SkyCommonExceptionEnum.EXIST_OWNER_9104);
//                }
//            }
//
//            bizUser.setName(stationInfoEditParam.getUserName());
//            bizUser.setPhone(stationInfoEditParam.getUserTel());
//            // 设置工作单位地址(业主单位地址存入扩展字段里面)
//            bizUser.setExtJson(stationInfoEditParam.getUserAddr());
//            bizUserService.updateById(bizUser);

            // 准备电站用户关系表数据更新sky_station_user
            if (stationUser != null && (ObjectUtil.isEmpty(stationUser.getUserId()) || !stationInfo.getStationContactid().equals(stationUser.getUserId()))) {
                updateWrapper.lambda().set(SkyStationUser::getUserId,stationInfo.getStationContactid());
                updateFlag = true;
            }
        }
        // 电站用户关系表数据更新sky_station_user
        if (updateFlag) {
            updateWrapper.lambda().eq(SkyStationUser::getStationId,stationInfoEditParam.getId());
            skyStationUserService.update(updateWrapper);
        }

        // 升级表关联更新
        skyInvUpgradeMapper.update(null,new UpdateWrapper<SkyInvUpgrade>().lambda()
                .set(SkyInvUpgrade::getStationId, null).eq(SkyInvUpgrade::getEquSn,stationInfoEditParam.getId()));
        if (!CollectionUtils.isEmpty(stationInfoEditParam.getNbSns())) {
            extracted(stationInfo, stationInfoEditParam.getNbSns());
        }
    }

    private void checkTimeZoneEdit(StationInfoEditParam stationInfoEditParam) throws ParseException {
        if (StringUtils.isEmpty(stationInfoEditParam.getTimeZone()) || stationInfoConfig.getTimeZone().equals(stationInfoEditParam.getTimeZone())) {
            return;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            if (!Objects.isNull(stationInfoEditParam.getStationCreatetime())) {
                stationInfoEditParam.setStationCreatetime(
                        formTimeZone(sdf.format(stationInfoEditParam.getStationCreatetime()), stationInfoEditParam.getTimeZone())
                );
            }
            if (!Objects.isNull(stationInfoEditParam.getStationGridtime())) {
                stationInfoEditParam.setStationGridtime(
                        formTimeZone(sdf.format(stationInfoEditParam.getStationGridtime()), stationInfoEditParam.getTimeZone())
                );
            }
        }

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<StationInfoIdParam> stationInfoIdParamList) {
        for (StationInfoIdParam stationInfoIdParam:stationInfoIdParamList) {
            StationInfo info = this.getById(stationInfoIdParam.getId());

            QueryWrapper<SkyInvInfo> queryWrapper = new QueryWrapper();
            queryWrapper.lambda().eq(SkyInvInfo::getStationId,info.getId());
            boolean exists = skyInvInfoMapper.exists(queryWrapper);
            if (exists) {
//                throw new CommonException("存在设备绑定该电站，请先解绑设备！");
                throw new SkyCommonException(SkyCommonExceptionEnum.EXIST_EQU_BIND_STATION_9103);
            }

            // 区域表信息删除
            if (StringUtils.isNotEmpty(info.getAreaId())) {
                areaMapper.deleteById(info.getAreaId());
            }
            // 电站表信息删除
            this.removeById(stationInfoIdParam.getId());
            // 电站统计表重新计算
            LocalDateTime beginTime = LocalDateTime.now();
            skyInstallerStatisticService.recountStatisticById(StpLoginUserUtil.getLoginUser().getId());
            LocalDateTime endTime = LocalDateTime.now();
            log.info("电站删除-电站信息表更新-结束：{}。用时：{}毫秒", endTime , Duration.between(beginTime, endTime).toMillis());
            // 电站用户关系表删除
            skyStationUserService.remove(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getStationId, stationInfoIdParam.getId()));
            // 电站设备表删除
            skyStationEquService.remove(new QueryWrapper<SkyStationEqu>().lambda().eq(SkyStationEqu::getStationId, stationInfoIdParam.getId()));
            // 升级表删除
            skyInvUpgradeMapper.update(null,new UpdateWrapper<SkyInvUpgrade>().lambda()
                    .set(SkyInvUpgrade::getStationId, null).eq(SkyInvUpgrade::getEquSn,stationInfoIdParam.getId()));
            //电站发电量表删除
            skyStationPowerService.remove(new QueryWrapper<SkyStationPower>().lambda().eq(SkyStationPower::getStationId, stationInfoIdParam.getId()));
        }
    }

    @Override
    public StationInfoVO detail(StationInfoIdParam stationInfoIdParam) {
        // 先查询电站信息
        StationInfo stationInfo = this.queryEntity(stationInfoIdParam.getId());
        // 转化为Vo
        StationInfoVO stationInfoVO = new StationInfoVO();
        Optional.ofNullable(stationInfo).ifPresent(p -> BeanUtil.copyProperties(p,stationInfoVO));
        //并网状态
        stationInfoVO.setStationGridstatus(stationInfoVO.getStationGridstatus()==null?0:stationInfoVO.getStationGridstatus());

        // 查询区域信息
        if (StringUtils.isNotEmpty(Objects.requireNonNull(stationInfo).getAreaId())) {
            Area area = areaMapper.selectById(stationInfo.getAreaId());
            Optional.ofNullable(area).ifPresent(p -> BeanUtil.copyProperties(p,stationInfoVO));
            stationInfoVO.setId(stationInfo.getId());
        }
        // 查询用户信息
        if (StringUtils.isNotEmpty(stationInfo.getStationContactid())) {
            BizUser bizUser = bizUserService.queryEntity(stationInfo.getStationContactid());
            if (bizUser != null) {
                stationInfoVO.setUserName(bizUser.getAccount());
                stationInfoVO.setUserTel(bizUser.getPhone());
                stationInfoVO.setUserAddr(bizUser.getExtJson());
            }
        }
        //查询安装商名称
        if(StringUtils.isNotEmpty(stationInfo.getStationCompany())){
            BizUser bizUser = bizUserService.queryEntity(stationInfo.getStationCompany());
            if(ObjectUtil.isNotEmpty(bizUser)){
                stationInfoVO.setStationCompanyName(bizUser.getName());
            }
        }
        List<SkyCollectorInfoPageResVO> skyCollectorInfoPageResVOS = stationInfoMapper.selectByStationId(stationInfoIdParam.getId());
        if(!CollectionUtils.isEmpty(skyCollectorInfoPageResVOS)){
            stationInfoVO.setSkyCollectorInfoPageResVOList(skyCollectorInfoPageResVOS);
        }
        return stationInfoVO;
    }

    @Override
    public StationInfo queryEntity(String id) {
        StationInfo stationInfo = this.getById(id);
        if(ObjectUtil.isEmpty(stationInfo)) {
            // 电站信息不存在
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        return stationInfo;
    }

    @Override
    public List<JSONObject> dynamicFieldConfigList() {
        String currentDataSourceId = dbsApi.getCurrentDataSourceId();
        String tableName = AnnotationUtil.getAnnotationValue(StationInfo.class, TableName.class);
        return devDfcApi.getTableFieldList(currentDataSourceId, tableName);
    }

    @Override
    public StationStatusVO statusAndAlarmCount(StationInfoPageParam stationInfoPageParam) {
        StationStatusVO vo = new StationStatusVO();
        vo.setTotal(0);
        vo.setAlarm(0);
        vo.setNoAlarm(0);
        vo.setAccessing(0);
        vo.setNormal(0);
        vo.setPartialOffline(0);
        vo.setAllOffline(0);

        if (Objects.isNull(stationInfoPageParam.getStationSizeBegin()) && !Objects.isNull(stationInfoPageParam.getStationSizeEnd())) {
            stationInfoPageParam.setStationSizeBegin(Double.MIN_VALUE);
        }
        if (Objects.isNull(stationInfoPageParam.getStationSizeEnd()) && !Objects.isNull(stationInfoPageParam.getStationSizeBegin())) {
            stationInfoPageParam.setStationSizeEnd(Double.MAX_VALUE);
        }
        if (ObjectUtil.isEmpty(stationInfoPageParam.getStationCompanyId()) || ObjectUtil.isNotEmpty(stationInfoPageParam.getStationCompany())) {
            stationInfoPageParam.setStationCompanyId(stationInfoPageParam.getStationCompany());
        }
        List<StationInfo> psList = stationInfoMapper.getStatusPsList(stationInfoPageParam);
        if (CollectionUtils.isEmpty(psList)){
            return vo;
        }
        vo.setTotal(psList.size());

        // 电站id集合
        List<String> ids = psList.stream().map(StationInfo::getId).collect(Collectors.toList());
        // 根据电站id获取离线数量
        Map<String, Integer> offlineMap = getOfflineNumMapByPsIds(ids);
        // 根据电站id获取设备数量
        Map<String, Integer> equNumMap = getEquNumByPsIds(ids);
        for (StationInfo model : psList) {
            if (ObjectUtil.isNotEmpty(model.getStationAlarm())) {
                if (model.getStationAlarm()==1) {
                    vo.setAlarm(vo.getAlarm() + 1);
                } else {
                    vo.setNoAlarm(vo.getNoAlarm() + 1);
                }
            } else {
                vo.setNoAlarm(vo.getNoAlarm() + 1);
            }

            // 电站状态
            int stationStatus = stationStatus(offlineMap, equNumMap, model.getId());
            if (stationStatus == 0) {
                vo.setAccessing(vo.getAccessing() + 1);
            } else if (stationStatus == 1) {
                vo.setNormal(vo.getNormal() + 1);
            } else if (stationStatus == 2) {
                vo.setPartialOffline(vo.getPartialOffline() + 1);
            } else if (stationStatus == 3) {
                vo.setAllOffline(vo.getAllOffline() + 1);
            }
        }

        return vo;
    }

    @Override
    public List<StationInfoExportVO> exportStationInfoList(StationInfoExportParam exportParam) {

        if (exportParam!=null && !CollectionUtils.isEmpty(exportParam.getIds()) && exportParam.getIds().size() > 0) {
            List<StationInfoExportVO> list = stationInfoMapper.exportStationInfoListByIds(exportParam.getIds());
            return list;
        }
        List<StationInfoExportVO> list = stationInfoMapper.exportStationInfoList(exportParam);
        return list;
    }

    @Override
    public List<StationInfo> getAllStationNameAndId(PsInstallerAndUserReqParam installerAndUserModel) {
        QueryWrapper<StationInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().select(StationInfo::getId,StationInfo::getStationName);
        if(ObjectUtil.isNotEmpty(installerAndUserModel.getStationCompany())) {
            queryWrapper.lambda().eq(StationInfo::getStationCompany, installerAndUserModel.getStationCompany());
        }
        if(ObjectUtil.isNotEmpty(installerAndUserModel.getStationContactId())) {
            queryWrapper.lambda().eq(StationInfo::getStationContactid, installerAndUserModel.getStationContactId());
        }
        if (ObjectUtil.isNotEmpty(installerAndUserModel.getDistributor())) {
            List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getInstallerIdL2, installerAndUserModel.getDistributor()));
            if (CollectionUtils.isEmpty(list)) {
                return Collections.emptyList();
            }
            List<String> psIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            queryWrapper.lambda().in(StationInfo::getId, psIdList);
        }
        List<StationInfo> list = this.list(queryWrapper);
        return list;
    }

    @Override
    public StationInfoOverviewVO overviewInfo(StationInfoIdParam stationInfoIdParam) {
        StationInfo stationInfo = this.queryEntity(stationInfoIdParam.getId());
        if (stationInfo == null) {
            return null;
        }
        StationInfoOverviewVO vo = new StationInfoOverviewVO();
        Area area = areaMapper.selectById(stationInfo.getAreaId());
        BizUser bizUser = bizUserMapper.selectById(stationInfo.getStationContactid());
        BeanUtil.copyProperties(area,vo);
        BeanUtil.copyProperties(stationInfo,vo);
        if (bizUser != null) {
            vo.setUserTel(bizUser.getPhone());
        }
        //此电站下设备是否有故障
        List<SkyInvInfo> skyInvInfos = skyInvInfoMapper.selectList(Wrappers.lambdaQuery(SkyInvInfo.class).eq(SkyInvInfo::getStationId, stationInfoIdParam.getId()));
        if(CollectionUtils.isEmpty(skyInvInfos)){
            vo.setFaultNumber(0);
            //没有故障
            vo.setDeviceStatus(1);
        }else{
            //判断设备故障逻辑
            equipmentStatusAssignment(skyInvInfos,vo);
        }
        return vo;
    }

    /**
     * 判断设备故障逻辑
     */
    public void equipmentStatusAssignment(List<SkyInvInfo> skyInvInfos,StationInfoOverviewVO vo){
        List<String> invSnList = skyInvInfos.stream().map(SkyInvInfo::getInvSn).collect(Collectors.toList());
        List<SkyAlarmInfoCurrent> skyAlarmInfoCurrents = skyAlarmInfoCurrentMapper.selectList(Wrappers.lambdaQuery(SkyAlarmInfoCurrent.class).in(SkyAlarmInfoCurrent::getEquSn, invSnList)
                .eq(SkyAlarmInfoCurrent::getAlarmLevel,2));
        if(!CollectionUtils.isEmpty(skyAlarmInfoCurrents)){
            List<String> distinctEquSns = skyAlarmInfoCurrents.stream().map(SkyAlarmInfoCurrent::getEquSn).distinct().collect(Collectors.toList());
            vo.setFaultNumber(distinctEquSns.size());
            //2:部分故障  3:全部故障
            vo.setDeviceStatus(2);
        }else{
            vo.setFaultNumber(0);
            //没有故障
            vo.setDeviceStatus(1);
        }
    }

    @Override
    public StationInfoOverviewPowerVO power(StationInfoIdParam stationInfoIdParam) {
        StationInfoOverviewPowerVO vo = skyInvMonitorCurrentMapper.getPower(stationInfoIdParam.getId());
        if (ObjectUtil.isNull(vo)) {
            vo = new StationInfoOverviewPowerVO();
        }
        List<StationPower> list = stationPowerMapper.getPower(stationInfoIdParam.getId(), null);
        if (!list.stream().anyMatch(a -> a.getTimeType() == 1)) {
            vo.setMonToday("0");
        }
        if (!list.stream().anyMatch(a -> a.getTimeType() == 4)) {
            vo.setMonTotal("0");
        }
        // 获取当前日期
        LocalDate currentDate = LocalDate.now();
        // 使用 DateTimeFormatter 格式化为年-月格式
        DateTimeFormatter formatterDay = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter formatterMonth = DateTimeFormatter.ofPattern("yyyy-MM");
        DateTimeFormatter formatterYear = DateTimeFormatter.ofPattern("yyyy");
        String formattedDay = currentDate.format(formatterDay);
        String formattedDate = currentDate.format(formatterMonth);
        String formattedYear = currentDate.format(formatterYear);
        for (StationPower power : list) {
            switch (power.getTimeType()) {
                case 1:
                    if (formattedDay.equals(power.getDateTime())) {
                        vo.setMonToday(power.getStationPower());
                        vo.setDayIncome(DoubleUtils.round(Double.valueOf(vo.getMonToday()) * vo.getDayIncome(),2));
                    } else {
                        vo.setMonToday("0");
                        vo.setDayIncome(0D);
                    }
                    break;
                case 2:
                    if (formattedDate.equals(power.getDateTime())) {
                        vo.setMonMonth(power.getStationPower());
                    } else {
                        vo.setMonMonth("0");
                    }
                    break;
                case 3:
                    if (formattedYear.equals(power.getDateTime())) {
                        vo.setMonYear(power.getStationPower());
                    } else {
                        vo.setMonYear("0");
                    }
                    break;
                case 4:
                    vo.setMonTotal(power.getStationPower());
                    break;
            }
        }

        return vo;
    }

    @Override
    public StationInfoOverviewPowerUsedVO powerUsed(StationInfoIdParam stationInfoIdParam) {
        StationInfoOverviewPowerUsedVO vo = skyInvMonitorCurrentMapper.powerUsed(stationInfoIdParam.getId());
        List<StationPower> list = stationPowerMapper.getPower(stationInfoIdParam.getId(),null);
        for (StationPower power:list) {
            switch (power.getTimeType()) {
                case 13:
                    vo.setTodayEnergy(power.getStationPower());
                    break;
                case 14:
                    vo.setMonthEnergy(power.getStationPower());
                    break;
                case 15:
                    vo.setYearEnergy(power.getStationPower());
                    break;
                case 16:
                    vo.setTotalEnergy(power.getStationPower());
                    break;
            }
        }
        return vo;
    }

    @Override
    public StationInfoOverviewPowerGridVO powerGrid(StationInfoIdParam stationInfoIdParam) {
        StationInfoOverviewPowerGridVO vo = skyInvMonitorCurrentMapper.powerGrid(stationInfoIdParam.getId());
        List<StationPower> list = stationPowerMapper.getPower(stationInfoIdParam.getId(),null);
        for (StationPower power:list) {
            switch (power.getTimeType()) {
                case 5:
                    vo.setGridEnergyToday(power.getStationPower());
                    break;
                case 6:
                    vo.setGridEnergyMonth(power.getStationPower());
                    break;
                case 7:
                    vo.setGridEnergyYear(power.getStationPower());
                    break;
                case 8:
                    vo.setGridEnergyTotal(power.getStationPower());
                    break;
                case 9:
                    vo.setGridPurchaseToday(power.getStationPower());
                    break;
                case 10:
                    vo.setGridPurchaseMonth(power.getStationPower());
                    break;
                case 11:
                    vo.setGridPurchaseYear(power.getStationPower());
                    break;
                case 12:
                    vo.setGridPurchaseTotal(power.getStationPower());
                    break;
            }
        }
        return vo;
    }

    @Override
    public List<StationInfoOverviewPowerHisVO> powerHis(StationInfoOverviewPowerHisParam hisParam) {
        QueryWrapper<StationPower> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(StationPower::getStationId,hisParam.getId());

        if (!ObjectUtil.isEmpty(hisParam.getTimeType())) {
            queryWrapper.lambda().eq(StationPower::getTimeType, hisParam.getTimeType()-1);
        }
        queryWrapper.lambda().like(StationPower::getDateTime,hisParam.getDateTime());

        queryWrapper.lambda().orderByAsc(StationPower::getDateTime);
        List<StationPower> powerList = stationPowerMapper.selectList(queryWrapper);
        powerList = powerList.stream().filter(distinctByKey(StationPower::getDateTime)).collect(Collectors.toList());

        List<StationInfoOverviewPowerHisVO> powerHisVOS = new ArrayList<>();
        for (StationPower power:powerList) {
            StationInfoOverviewPowerHisVO hisVO = new StationInfoOverviewPowerHisVO();
            BeanUtil.copyProperties(power,hisVO);
            powerHisVOS.add(hisVO);
        }
        return powerHisVOS;
    }

    @Override
    public List<StationInfoOverviewPowerAnalysisVO> powerAnalysis(StationInfoOverviewPowerAnalysisParam param) {
        List<StationInfoOverviewPowerAnalysisVO> resList = new ArrayList<>();
        List<StationPower> list;
        if (StringUtils.isBlank(param.getDateTime())) {
            list = stationPowerMapper.getPower(param.getId(),null);
        } else {
            list = stationPowerMapper.getPower(param.getId(),param.getDateTime());
        }
        for (StationPower power:list) {
            StationInfoOverviewPowerAnalysisVO vo = new StationInfoOverviewPowerAnalysisVO();
            BeanUtil.copyProperties(power,vo);
            resList.add(vo);
        }
        return resList;
    }

    @Override
    public List<SkyInvInfoRankVO> invRank(StationInfoIdParam stationInfoIdParam) {
        List<SkyInvInfoRankVO> list = skyInvMonitorCurrentMapper.getRank(stationInfoIdParam.getId());
        return list;
    }

    @Override
    public StationInfoOverviewInvAlarmVO invAlarmBaseInfo(StationInfoIdParam stationInfoIdParam) {
        Integer count = stationPowerMapper.invNum(stationInfoIdParam.getId());
        Integer alarmNum = stationPowerMapper.invAlarmNum(stationInfoIdParam.getId());
        Integer offlineNum = stationPowerMapper.invOfflineNum(stationInfoIdParam.getId());
        StationInfoOverviewInvAlarmVO vo = new StationInfoOverviewInvAlarmVO();
        vo.setInvNum(count);
        vo.setAlarmNum(alarmNum);
        vo.setOfflineNum(offlineNum);
        return vo;
    }

    @Override
    public StationInfoOverviewFlowDiagramVO flowDiagram(StationInfoIdParam stationInfoIdParam) {
        //定义一个在线设备的集合，拿到sn
        List<String> strSns = new ArrayList<>();
        List<PsDeviceInfoVO> psDeviceInfoVOS = stationPowerMapper.deviceStatusList(stationInfoIdParam.getId());
        //筛选出在线的设备的sn
        for (PsDeviceInfoVO vo:psDeviceInfoVOS) {
            boolean flag = true;
            if(StringUtils.isEmpty(vo.getTimeZone())){
                vo.setTimeZone(skyInvInfoService.timeZoneOperation());
                flag = false;
            }
            if (StringUtils.isEmpty(vo.getMonitorTime())) {
                continue;
            }
            LocalDateTime localDateTime = LocalDateTime.parse(flag ? vo.getMonitorTime() : vo.getCurrentUpdateTime(), LocalDateUtils.DATETIME_FORMATTER);
            boolean after = skyInvInfoService.getNowTimeByZoneId(vo.getTimeZone()).isAfter(localDateTime.plusMinutes(15));
            if (after) {
                continue;
            }
            strSns.add(vo.getInvSn());
        }
        if(CollectionUtils.isEmpty(strSns)){
            return new StationInfoOverviewFlowDiagramVO();
        }
        StationInfoOverviewFlowDiagramVO resVO = stationPowerMapper.flowDiagram(stationInfoIdParam.getId(), strSns);
        // 流动图电网功率 = 如果有电表就取电表功率，没有就取grid口功率
        if (resVO != null) {
            if (ObjectUtil.isNotEmpty(resVO.getMeterPower()) && resVO.getMeterPower() != 0d) {
                resVO.setMonPower(resVO.getMeterPower());
            }
        }
        return resVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void userChange(StationInfoUserChangeReq reqDTO) {
        if (CollectionUtils.isEmpty(reqDTO.getPsIds()) || StringUtils.isBlank(reqDTO.getStationCompany())) {
            return;
        }
        //修改关系表
        List<SkyStationUser> skyStationUsers = skyStationUserService.list(Wrappers.lambdaQuery(SkyStationUser.class).in(SkyStationUser::getStationId, reqDTO.getPsIds()));
        if(!CollectionUtils.isEmpty(skyStationUsers)){
            skyStationUsers.stream().forEach(skyStationUser -> {
                skyStationUser.setInstallerIdL1(reqDTO.getStationCompany());
            });
            skyStationUserService.updateBatchById(skyStationUsers);
        }
        UpdateWrapper<StationInfo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda().in(StationInfo::getId, reqDTO.getPsIds())
                .set(StationInfo::getStationCompany, reqDTO.getStationCompany());
        stationInfoMapper.update(null,updateWrapper);
    }

    @Override
    public Map<String, Object> localDatetime(Map<String, Object> countapryMap) {
        Map<String, Object> resultMap = null;
        int countryCode = Integer.parseInt(countapryMap.get("countryCode").toString());
        if (countryCode >= 0) {
            String timezoneStr = timezoneService.queryByAddrCode(countryCode);
            resultMap = LocalDateUtils.getLocalTime(timezoneStr);

        }

        return resultMap;
    }

    @Override
    public PsInvStatisticsVO invStatistics(StationInfoIdParam stationInfoIdParam) {
        PsInvStatisticsVO vo = new PsInvStatisticsVO();
        List<PsInvStatisticsParam> list = skyInvMonitorCurrentMapper.invStatistics(stationInfoIdParam.getId());
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        List<String> sns = list.stream().map(PsInvStatisticsParam::getInvSn).collect(Collectors.toList());
        List<SkyInvInfoMonitorTimeParam> timeParamList = skyInvMonitorCurrentMapper.getInvStatus(sns);
        Map<String, Date> status = new HashMap<>();
        if (!CollectionUtils.isEmpty(timeParamList)) {
            //由于科士达传过来的数据invSn是有空格的
            status = timeParamList.stream().collect(Collectors.toMap(param -> param.getInvSn().trim(), SkyInvInfoMonitorTimeParam::getUploadDate));
        }
        for (PsInvStatisticsParam inv:list) {
            //由于科士达传过来的数据invSn是有空格的
            if (status.isEmpty() || !status.containsKey(inv.getInvSn().trim())) {
                inv.setStatus(10);
                continue;
            }
            if(StringUtils.isEmpty(inv.getTimeZone())){
                inv.setTimeZone(skyInvInfoServiceImpl.timeZoneOperation());
            }
            boolean ifOffline = skyInvInfoServiceImpl.ifOffline(status.get(inv.getInvSn().trim()), inv.getTimeZone());
            if(ifOffline){
                inv.setStatus(10);
            }
        }
        vo.setEquNum(list.size());
        vo.setEquNormalNum((int) list.stream().filter(x -> x.getStatus()!=null && x.getStatus() < 2).count());
        vo.setEquAbnormalNum(vo.getEquNum() - vo.getEquNormalNum());
        vo.setPsInvStatisticsParamList(list);
        return vo;
    }

    @Override
    public List<PsDeviceInfoVO> psDeviceList(StationInfoIdParam stationInfoIdParam) {
        List<PsDeviceInfoVO> list = stationPowerMapper.psDeviceList(stationInfoIdParam.getId());
        for (PsDeviceInfoVO vo:list) {
            vo.setMonStatus(1);
            if (StringUtils.isEmpty(vo.getMonitorTime())) {
                vo.setMonStatus(2);
                continue;
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            boolean after = false;
            try {
                after = skyInvInfoServiceImpl.ifOffline(dateFormat.parse(vo.getMonitorTime()), ObjectUtil.isNotEmpty(vo.getTimeZone()) ? vo.getTimeZone() : skyInvInfoServiceImpl.timeZoneOperation());
            } catch (ParseException e) {
                throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
            }
            if (after) {
                vo.setMonStatus(10);
            }
        }
        return list;
    }

//    public static boolean invIfOffline(String monitorTime) {
//        LocalDateTime localDateTime = LocalDateTime.parse(monitorTime, LocalDateUtils.DATETIME_FORMATTER);
//        return LocalDateTime.now().isAfter(localDateTime.plusMinutes(15));
//    }

    /**
     * 去重
     */
    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> set = ConcurrentHashMap.newKeySet();
        return t -> set.add(keyExtractor.apply(t));
    }

    /**
     * 获取当日当所有时间5分钟间隔
     *
     * @param date
     * @return
     */
    private List<String> getDateTimeList(String date) {
        LocalDate localDate = LocalDate.parse(date);
        List<String> timeList = new ArrayList<>(216);
        for (int h = 4; h < 22; h++) {
            //每隔5分钟1个点，每小时共12各点
            for (int m = 0; m < 60; m += 5) {
                timeList.add(LocalDateUtils.formatLocalDateTime(LocalDateTime.of(localDate, LocalTime.of(h, m, 0)), LocalDateUtils.DATETIME_FORMATTER));
            }
        }
        return timeList;
    }

    /**
     * 获取当月所有日
     *
     * @param date
     * @return
     */
    private List<LocalDate> getLocalDateList(LocalDate date) {
        int year = date.getYear();
        int month = date.getMonthValue();
        int daysInMonth = date.lengthOfMonth();
        List<LocalDate> dates = new ArrayList<>();
        for (int day = 1; day <= daysInMonth; day++) {
            LocalDate currentDate = LocalDate.of(year, month, day);
            dates.add(currentDate);
        }
        return dates;
    }


    /**
     * 获取当年所有月
     *
     * @param year
     * @return
     */
    private List<String> getMonthList(String year) {
        List<String> monthList = new ArrayList<>(12);
        for (int i = 1; i <= 12; i++) {
            monthList.add(String.format("%s-%02d", year, i));
        }
        return monthList;
    }
}
