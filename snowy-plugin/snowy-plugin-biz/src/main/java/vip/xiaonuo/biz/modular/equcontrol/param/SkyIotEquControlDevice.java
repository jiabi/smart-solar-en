package vip.xiaonuo.biz.modular.equcontrol.param;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/5/7 11:20
 */
@Getter
@Setter
public class SkyIotEquControlDevice<T> {

    /** 设备id **/
    private String id;

    /** 1：户用并网逆变；2：户储逆变器；3：电表；4：电池；5：采集器 **/
    private Integer type;

    private String sn;
    private String status;
    private List<T> parameter;
    private T SetSTKConfig;
}
