/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.equbin.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.equbin.param.*;
import vip.xiaonuo.biz.modular.invupgrade.param.SkyInvUpgradeParam;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.equbin.entity.SkyEquBin;
import vip.xiaonuo.biz.modular.equbin.service.SkyEquBinService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 设备升级包控制器
 *
 * @author 全佳璧
 * @date  2024/04/17 19:40
 */
@Api(tags = "设备升级包控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyEquBinController {

    @Resource
    private SkyEquBinService skyEquBinService;

    /**
     * 获取设备升级包分页
     *
     * @author 全佳璧
     * @date  2024/04/17 19:40
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取设备升级包分页")
    @SaCheckPermission("/biz/equbin/page")
    @PostMapping("/biz/equbin/page")
    public CommonResult<Page<SkyEquBinPageVO>> page(@RequestBody SkyEquBinPageParam skyEquBinPageParam) {
        return CommonResult.data(skyEquBinService.page(skyEquBinPageParam));
    }

    /**
     * 添加设备升级包
     *
     * @author 全佳璧
     * @date  2024/04/17 19:40
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加设备升级包")
    @CommonLog("添加设备升级包")
    @SaCheckPermission("/biz/equbin/add")
    @PostMapping("/biz/equbin/add")
    public CommonResult<String> add(@RequestBody @Valid SkyEquBinAddParam skyEquBinAddParam) {
        skyEquBinService.add(skyEquBinAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑设备升级包
     *
     * @author 全佳璧
     * @date  2024/04/17 19:40
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑设备升级包")
    @CommonLog("编辑设备升级包")
    @SaCheckPermission("/biz/equbin/edit")
    @PostMapping("/biz/equbin/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyEquBinEditParam skyEquBinEditParam) {
        skyEquBinService.edit(skyEquBinEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除设备升级包
     *
     * @author 全佳璧
     * @date  2024/04/17 19:40
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除设备升级包")
    @CommonLog("删除设备升级包")
    @SaCheckPermission("/biz/equbin/delete")
    @PostMapping("/biz/equbin/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyEquBinIdParam> skyEquBinIdParamList) {
        skyEquBinService.delete(skyEquBinIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取设备升级包详情
     *
     * @author 全佳璧
     * @date  2024/04/17 19:40
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取设备升级包详情")
    @SaCheckPermission("/biz/equbin/detail")
    @GetMapping("/biz/equbin/detail")
    public CommonResult<SkyEquBin> detail(@Valid SkyEquBinIdParam skyEquBinIdParam) {
        return CommonResult.data(skyEquBinService.detail(skyEquBinIdParam));
    }

    /**
     * 获取设备升级包列表
     *
     * @author 全佳璧
     * @date  2024/04/17 19:40
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("获取设备升级包列表")
    @CommonLog("获取设备升级包列表")
//    @SaCheckPermission("/biz/equbin/list")
    @PostMapping("/biz/equbin/list")
    public CommonResult<List<SkyEquBinLinkageVO>> selectSkyEquBin(@RequestBody SkyEquBinReqParam skyInvUpgradeParam) {
        List<SkyEquBinLinkageVO> list = skyEquBinService.selectSkyEquBin(skyInvUpgradeParam);
        return CommonResult.data(list);
    }
}
