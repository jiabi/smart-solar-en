/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.upgraderecord.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.collectorupgrade.param.SkyCollectorUpgradePageVO;
import vip.xiaonuo.biz.modular.invinfo.service.SkyInvInfoService;
import vip.xiaonuo.biz.modular.upgraderecord.param.*;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.upgraderecord.entity.SkyUpgradeRecord;
import vip.xiaonuo.biz.modular.upgraderecord.mapper.SkyUpgradeRecordMapper;
import vip.xiaonuo.biz.modular.upgraderecord.service.SkyUpgradeRecordService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 升级记录Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/04/17 19:55
 **/
@Service
public class SkyUpgradeRecordServiceImpl extends ServiceImpl<SkyUpgradeRecordMapper, SkyUpgradeRecord> implements SkyUpgradeRecordService {

    @Resource
    private SkyUpgradeRecordMapper skyUpgradeRecordMapper;

    @Resource
    private SkyInvInfoService skyInvInfoService;

    @Override
    public Page<SkyUpgradeRecordPageVO> page(SkyUpgradeRecordPageParam skyUpgradeRecordPageParam) {
        Page<SkyUpgradeRecordPageVO> pageDataModel = new Page<>(skyUpgradeRecordPageParam.getCurrent(),skyUpgradeRecordPageParam.getSize());
        List<SkyUpgradeRecordPageVO> list = skyUpgradeRecordMapper.page(skyUpgradeRecordPageParam);
        for (SkyUpgradeRecordPageVO vo:list) {
            if (ObjectUtil.isNotEmpty(vo.getUpgradeModule())) {
                if (vo.getUpgradeModule() == 1) {
                    vo.setUpgradeModuleName("wstk");
                }
                if (vo.getUpgradeModule() == 2) {
                    vo.setUpgradeModuleName("marm");
                }
                if (vo.getUpgradeModule() == 3) {
                    vo.setUpgradeModuleName("sarm");
                }
                if (vo.getUpgradeModule() == 4) {
                    vo.setUpgradeModuleName("mdsp");
                }
                if (vo.getUpgradeModule() == 5) {
                    vo.setUpgradeModuleName("sfty");
                }
                if (vo.getUpgradeModule() == 6) {
                    vo.setUpgradeModuleName("gstk");
                }
            }
            if(StringUtils.isEmpty(vo.getTimeZone())){
                vo.setTimeZone(skyInvInfoService.timeZoneOperation());
            }
        }
        pageDataModel.setRecords(list);
        pageDataModel.setTotal(skyUpgradeRecordMapper.countNum(skyUpgradeRecordPageParam));
        return pageDataModel;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyUpgradeRecordAddParam skyUpgradeRecordAddParam) {
        SkyUpgradeRecord skyUpgradeRecord = BeanUtil.toBean(skyUpgradeRecordAddParam, SkyUpgradeRecord.class);
        this.save(skyUpgradeRecord);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyUpgradeRecordEditParam skyUpgradeRecordEditParam) {
        SkyUpgradeRecord skyUpgradeRecord = this.queryEntity(skyUpgradeRecordEditParam.getId());
        BeanUtil.copyProperties(skyUpgradeRecordEditParam, skyUpgradeRecord);
        this.updateById(skyUpgradeRecord);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyUpgradeRecordIdParam> skyUpgradeRecordIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyUpgradeRecordIdParamList, SkyUpgradeRecordIdParam::getId));
    }

    @Override
    public SkyUpgradeRecord detail(SkyUpgradeRecordIdParam skyUpgradeRecordIdParam) {
        return this.queryEntity(skyUpgradeRecordIdParam.getId());
    }

    @Override
    public SkyUpgradeRecord queryEntity(String id) {
        SkyUpgradeRecord skyUpgradeRecord = this.getById(id);
        if(ObjectUtil.isEmpty(skyUpgradeRecord)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        return skyUpgradeRecord;
    }

}
