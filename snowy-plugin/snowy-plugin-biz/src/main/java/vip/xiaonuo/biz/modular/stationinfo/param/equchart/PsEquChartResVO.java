package vip.xiaonuo.biz.modular.stationinfo.param.equchart;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/6 16:04
 */
@Getter
@Setter
public class PsEquChartResVO {
    /**
     * 日曲线
     */
    private PsEquDayChartResParam dayChart;

    /**
     * 月、年、总图表
     */
    private PsEquChartResParam monYearTotalChart;

}
