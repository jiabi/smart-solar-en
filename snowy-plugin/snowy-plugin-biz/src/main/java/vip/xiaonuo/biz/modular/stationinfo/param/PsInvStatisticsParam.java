package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/4 14:25
 */

@Getter
@Setter
public class PsInvStatisticsParam {
    /** 逆变器SN */
    private String invSn;

    /** 逆变器名称 */
    private String invName;

    /** 设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪 */
    private Integer equType;

    /** 设备状态 */
    private Integer status;

    /** 厂家ID（设备品牌） */
    // 字典表值
    private String companyId;

    /**  时区 **/
    private String timeZone;
}
