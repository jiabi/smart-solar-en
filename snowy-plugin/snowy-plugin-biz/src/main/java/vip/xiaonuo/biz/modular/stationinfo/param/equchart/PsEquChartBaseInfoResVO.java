package vip.xiaonuo.biz.modular.stationinfo.param.equchart;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/8 17:32
 */
@Getter
@Setter
public class PsEquChartBaseInfoResVO {
    /**
     * 设备SN
     */
    private String sn;

    /**
     * 设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪
     */
    private Integer equType;

    /**
     * 直流功率
     */
    private Double inPower;
    /**
     * 交流功率
     */
    private Double pacPower;
    /**
     * 当日发电量（单位：kWh）
     */
    private Double dayPg;

    /**
     * 当日满发小时
     */
    private Double dayHour;
    /**
     * 当月发电量
     */
    private Double monthPg;
    /**
     * 当月满发小时
     */
    private Double monthHour;
    /**
     * 累计发电量
     */
    private Double total;
}
