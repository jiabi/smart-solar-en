package vip.xiaonuo.biz.modular.invmonitorcurrent.param;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2024/6/4 16:03
 */

@Getter
@Setter
public class SkyInvInfoMonitorTimeParam {
    private String invSn;

    private Date uploadDate;

    private Integer monState;
}
