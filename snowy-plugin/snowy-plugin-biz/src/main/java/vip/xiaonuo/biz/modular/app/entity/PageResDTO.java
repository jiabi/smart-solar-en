package vip.xiaonuo.biz.modular.app.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/19 14:26
 */

@Getter
@Setter
public class PageResDTO<T> implements Serializable {

    private static final long serialVersionUID = 4840257238690527227L;

    /**
     * 分页数据的总数
     */
    private long rows = 0;
    /**
     * 总页面数
     */
    private long pages = 0;

    /**
     * 返回的内容
     */
    private List<T> data;

    private PageResDTO() {
    }

    public PageResDTO(long rows, long pages, List<T> data) {
        this.rows = rows;
        this.pages = pages;
        this.data = data;
    }

    public static final <T> PageResDTO<T> emptyPage() {
        return (PageResDTO<T>) EmptyPage.EMPTY_PAGE;
    }

    private static class EmptyPage {
        public static final PageResDTO EMPTY_PAGE = new PageResDTO<>(0, 0, Collections.emptyList());
    }
}
