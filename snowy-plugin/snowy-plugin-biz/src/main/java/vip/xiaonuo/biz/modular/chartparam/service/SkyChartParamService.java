/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.chartparam.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.chartparam.param.*;
import vip.xiaonuo.biz.modular.chartparam.entity.SkyChartParam;

import java.util.List;

/**
 * 图表参数字典表Service接口
 *
 * @author 全佳璧
 * @date  2024/02/20 18:55
 **/
public interface SkyChartParamService extends IService<SkyChartParam> {

    /**
     * 获取图表参数字典表分页
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    Page<SkyChartParam> page(SkyChartParamPageParam skyChartParamPageParam);

    /**
     * 添加图表参数字典表
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    void add(SkyChartParamAddParam skyChartParamAddParam);

    /**
     * 编辑图表参数字典表
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    void edit(SkyChartParamEditParam skyChartParamEditParam);

    /**
     * 删除图表参数字典表
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    void delete(SkyChartParamIdParam skyChartParamIdParam);

    /**
     * 获取图表参数字典表详情
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    SkyChartParam detail(SkyChartParamIdParam skyChartParamIdParam);

    /**
     * 获取图表参数字典表详情
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     **/
    SkyChartParam queryEntity(String id);

    /**
     * 获取所有表名称
     */
    List<ChartParamTablesResVO> getAllTables();

    /**
     * 获取指定表的字段名称
     */
    List<ChartParamTableNameAndCommentResVO> getTablesNameAndComment(ChartParamTableNameReq req);

    List<SkyChartParam> getList(SkyChartParamListParam skyChartParamListParam);
}
