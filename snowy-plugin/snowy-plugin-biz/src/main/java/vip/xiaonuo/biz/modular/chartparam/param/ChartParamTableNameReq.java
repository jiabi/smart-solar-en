package vip.xiaonuo.biz.modular.chartparam.param;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @Author wangjian
 * @Date 2024/1/4 13:12
 */

@Getter
@Setter
public class ChartParamTableNameReq {
    @NotBlank(message = "tableName not blank")
    private String tableName;
}
