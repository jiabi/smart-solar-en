package vip.xiaonuo.biz.modular.chartparam.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/1/4 13:06
 */
@Getter
@Setter
public class ChartParamTableNameAndCommentResVO {
    private String columnName;

    private String comment;
}
