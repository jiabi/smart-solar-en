package vip.xiaonuo.biz.modular.equcontrol.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import vip.xiaonuo.auth.api.SaBaseLoginUserApi;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.equcontrol.entity.SkyMqttModbus;
import vip.xiaonuo.biz.modular.equcontrol.mapper.SkyEquControlMapper;
import vip.xiaonuo.biz.modular.equcontrol.param.*;
import vip.xiaonuo.biz.modular.equcontrol.service.SkyEquControlService;
import vip.xiaonuo.biz.modular.equrelationship.entity.SkyEquRelationship;
import vip.xiaonuo.biz.modular.equrelationship.mapper.SkyEquRelationshipMapper;
import vip.xiaonuo.common.util.CommonCryptogramUtil;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/5/6 14:27
 */
@Service
@Slf4j
public class SkyEquControlServiceImpl implements SkyEquControlService {

    @Resource
    private SkyEquControlMapper skyEquControlMapper;

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private IotProperties iotProperties;

    @Resource
    private SkyEquRelationshipMapper skyEquRelationshipMapper;

    @Resource(name = "loginUserApi")
    private SaBaseLoginUserApi loginUserApi;

    @Override
    public SkyIotParam<SkyIotReadResParam> getParam(SkyReqParam<SkyReadParamDeviceInfoParam> skyReqParam) {
        List<SkyReadParamDeviceInfoParam> deviceList = skyReqParam.getDeviceList();
        if (CollectionUtils.isEmpty(deviceList)) {
            return null;
        }

        SkyIotParam<SkyReadParamDeviceInfoIotParam> iotParam = new SkyIotParam();
        List<SkyReadParamDeviceInfoIotParam> deviceList2Iot = new ArrayList<>();
        String cjSN = null;
        for (SkyReadParamDeviceInfoParam param : deviceList) {
            if (ObjectUtil.isEmpty(param.getSn())) {
                throw new SkyCommonException(SkyCommonExceptionEnum.SN_CHECK_NOT_NULL_9107);
            }
//            List<SkyMqttModbus> modbusList = skyEquControlMapper.selectList(new QueryWrapper<SkyMqttModbus>().lambda().in(SkyMqttModbus::getMqttKey, param.getParamList()));
//            if (ObjectUtil.isEmpty(modbusList)) {
//                throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
//            }

            SkyReadParamDeviceInfoIotParam deviceIot = new SkyReadParamDeviceInfoIotParam();
            BeanUtil.copyProperties(param,deviceIot);
            List<String> modbus = new ArrayList<>();
//            for (SkyMqttModbus model:modbusList) {
//                // 保证对应顺序
//                paramList.add(model.getMqttKey());
//                modbus.add(String.valueOf(model.getModbusAddr()));
//            }
            List<String> paramList = new ArrayList<>(param.getParamList());
            deviceIot.setParamList(paramList);
            deviceIot.setModbus(modbus);
            deviceList2Iot.add(deviceIot);

            // 获取采集器sn
            SkyEquRelationship relationship = skyEquRelationshipMapper.selectOne(new QueryWrapper<SkyEquRelationship>().lambda().eq(SkyEquRelationship::getEquSn, param.getSn()));
            if (ObjectUtil.isEmpty(relationship)) {
                throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
            }
            cjSN = relationship.getCollectorSn();
        }
        iotParam.setSendTime(LocalDateTime.now().format(LocalDateUtils.DATETIME_FORMATTER));
        iotParam.setRequestId(String.valueOf(System.currentTimeMillis()));
        iotParam.setDeviceList(deviceList2Iot);
        // 调用iot接口
        log.info("调用iot读数据-请求参数：{}" , JSON.toJSONString(iotParam));
        MultiValueMap<String,Object> requestEntity = new LinkedMultiValueMap<>();
        requestEntity.add("topic",iotProperties.getTopicPrefix() + cjSN + iotProperties.getReadMethod());
        requestEntity.add("msg",iotParam);
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(15000); // 设置连接超时时间为15秒
        requestFactory.setReadTimeout(15000); // 设置读取超时时间为15秒
        restTemplate.setRequestFactory(requestFactory);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(iotProperties.getEquControlReadUrl(), HttpMethod.POST, new HttpEntity<>(requestEntity), String.class);
        } catch (RestClientException e) {
            log.info("调用iot读数据-连接超时");
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        log.info("调用iot读数据-返回数据：{}" , response.getBody());
        return JSON.parseObject(response.getBody(), new ParameterizedTypeReference<SkyIotParam<SkyIotReadResParam>>() {}.getType());

    }

    @Override
    public SkyIotParam<SkyIotEquControlDevice> equControl(SkyReqParam<SkyIotEquControlDevice<IotMqttKeyElecMeterParam>> skyReqParam){
        // SkyEquControlParameter
        skyReqParam.setSendTime(LocalDateTime.now().format(LocalDateUtils.DATETIME_FORMATTER));
        skyReqParam.setRequestId(String.valueOf(System.currentTimeMillis()));
        // 调用iot接口
        log.info("参数设置-请求参数：{}" , JSON.toJSONString(skyReqParam));
        MultiValueMap<String,Object> requestEntity = new LinkedMultiValueMap<>();
        String cjSN = null;

        List<SkyIotEquControlDevice<IotMqttKeyElecMeterParam>> deviceList = skyReqParam.getDeviceList();
//        SkyIotEquControlDeviceToStr parameterList = deviceList.get(0);
        if (CollectionUtils.isEmpty(deviceList)) {
            return null;
        }

        //modbus
//        List<String> paramerKeyList = parameterList.getParameter().stream().map(SkyEquControlParameter::getKey).collect(Collectors.toList());
//        List<SkyMqttModbus> modbusList = skyEquControlMapper.selectList(new QueryWrapper<SkyMqttModbus>().lambda().in(SkyMqttModbus::getMqttKey, paramerKeyList));
//        if (ObjectUtil.isEmpty(modbusList)) {
//            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
//        }
//        Map<String, Integer> keyAddrMap = modbusList.stream().collect(Collectors.toMap(SkyMqttModbus::getMqttKey, SkyMqttModbus::getModbusAddr));
//        List<SkyEquControlParameter> listParameter = parameterList.getParameter();
//        for (SkyEquControlParameter param : listParameter) {
//            if (ObjectUtil.isEmpty(param.getKey())) {
//                throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
//            }
//            if (keyAddrMap.containsKey(param.getKey())) {
//                param.setModbus(keyAddrMap.get(param.getKey()));
//            }
//        }
//        parameterList.setParameter(listParameter);
//        deviceList.set(0,parameterList);
//        skyReqParam.setDeviceList(deviceList);

        // 获取采集器sn
        SkyEquRelationship relationship = skyEquRelationshipMapper.selectOne(new QueryWrapper<SkyEquRelationship>().lambda().eq(SkyEquRelationship::getEquSn, skyReqParam.getDeviceList().get(0).getSn()));
        if (ObjectUtil.isEmpty(relationship)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        cjSN = relationship.getCollectorSn();
        requestEntity.add("topic",iotProperties.getTopicPrefix() + cjSN + iotProperties.getWriteMethod());
        requestEntity.add("msg",skyReqParam);
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(15000); // 设置连接超时时间为15秒
        requestFactory.setReadTimeout(15000); // 设置读取超时时间为15秒
        restTemplate.setRequestFactory(requestFactory);
        ResponseEntity<String> response = null;
        try {
            log.info("调用iot参数设置-请求参数：{}" , JSON.toJSONString(requestEntity));
            response = restTemplate.exchange(iotProperties.getEquControlWriteUrl(), HttpMethod.POST, new HttpEntity<>(requestEntity), String.class);
        } catch (RestClientException e) {
            log.info("调用iot参数设置-连接超时");
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        log.info("调用iot参数设置-返回数据：{}" , response.getBody());
        String body = response.getBody().replaceAll("null", "");
        return JSON.parseObject(body, new ParameterizedTypeReference<SkyIotParam<SkyIotEquControlDevice>>() {}.getType());
    }

    @Override
    public Boolean checkPassword(CheckPasswordParam model) {
        // SM2解密并获得前端传来的密码哈希值
        String passwordHash;
        try {
            // 解密，并做哈希值
            passwordHash = CommonCryptogramUtil.doHashValue(CommonCryptogramUtil.doSm2Decrypt(model.getPassword()));
        } catch (Exception e) {
            throw new SkyCommonException(SkyCommonExceptionEnum.PWD_DECRYPT_ERROR_9127);
        }
        SaBaseLoginUser saBaseLoginUser = loginUserApi.getUserByAccount(StpLoginUserUtil.getLoginUser().getAccount());
        if(ObjectUtil.isEmpty(saBaseLoginUser)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.ACCOUNT_ERROR_9128);
        }
        if (!saBaseLoginUser.getPassword().equals(passwordHash)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.PWD_ERROR_9129);
        }
        return Boolean.TRUE;
    }

    public static void main(String[] args) {
        SkyReqParam<SkyIotEquControlDeviceToStr> s = new SkyReqParam<SkyIotEquControlDeviceToStr>();
        s.setSendTime("55555");
        SkyReqParam<SkyIotEquControlDevice<SkyEquControlParameter>> n = new SkyReqParam<>();
        BeanUtil.copyProperties(s,n);
        System.out.println(n.getSendTime());
    }
}
