package vip.xiaonuo.biz.modular.stationinfo.param.pschart;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/8 13:22
 */
@Setter
@Getter
@ContentRowHeight(18)
@HeadRowHeight(20)
@ColumnWidth(24)
public class PsChartAnalysisYearExcelResParam {
    /**
     * 时间轴
     */
    @ExcelProperty(value = "时间")
    private String time;

    /**
     * 当日发电量
     */
    @ExcelProperty(value = "当月发电量（kWh）")
    private Double dayGenerate;

    /**
     * 当日用电量
     */
    @ExcelProperty(value = "当月用电量（kWh）")
    private Double dayUse;

    /**
     * 当日并网量
     */
    @ExcelProperty(value = "当月并网量（kWh）")
    private Double dayGrid;

    /**
     * 当日购电量
     */
    @ExcelProperty(value = "当月购电量（kWh）")
    private Double dayBuy;

    /**
     * 当日充电量
     */
    @ExcelProperty(value = "当月充电量（kWh）")
    private Double dayCharge;

    /**
     * 当日放电量
     */
    @ExcelProperty(value = "当月放电量（kWh）")
    private Double dayDischarge;
}
