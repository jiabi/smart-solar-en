package vip.xiaonuo.biz.modular.eququality.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.core.util.ExcelImport;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2023/10/5 14:50
 **/
@Data
public class SkyEquQualityImportModel {

    private int rowNum;

    /** 设备SN */
    @ExcelImport(value = "设备SN（示例数据请勿删除！必填！）",required = true , unique = true)
    private String equSn;

    /** 设备类型 ：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪*/
    @ExcelImport(value = "设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪(导入请填对应数字！必填！)")
    private Integer equType;

    /** 设备型号 */
    @ExcelImport(value = "设备型号（必填）")
    private String equModel;

    /** 发货日期 */
    @ExcelImport(value = "发货日期（日期格式，如：2023-10-01（必填！））")
//    @JsonFormat(pattern = "yyyy-MM-dd")
    private String startDate;

    /** 质保期限 */
//    @ExcelImport(value = "质保期限")
    private String endDate;

    /** 质保时长 */
    @ExcelImport(value = "质保时长（单位：年；最多一位小数（必填！））")
    private Double timeLimit;

    /** 临期状态 */
//    @ExcelImport(value = "临期状态 0：临期；1：正常（可不填）",required = false)
    private Integer criticalStatus;

    /** 错误提示 **/
    private String rowTips;
}
