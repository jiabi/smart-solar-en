package vip.xiaonuo.biz.modular.app.station.param.chart;

import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.PsEquDayChartResParam;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/26 9:12
 */

@Getter
@Setter
public class AppPsDayPowerResVO {
    /**
     * 日期集合
     */
    private List<String> hourList;

    /**
     * 当日发电总量
     */
    private Double totalVolume;

    /**
     * 明细
     */
    private List<Double> volumeHourList;

}
