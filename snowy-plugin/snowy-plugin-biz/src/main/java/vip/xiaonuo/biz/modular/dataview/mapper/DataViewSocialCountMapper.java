package vip.xiaonuo.biz.modular.dataview.mapper;

import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.dataview.param.DataViewMapVO;
import vip.xiaonuo.biz.modular.dataview.param.DataViewSocialCountParam;
import vip.xiaonuo.biz.modular.dataview.param.DataViewSocialCountVO;
import vip.xiaonuo.biz.modular.stationinfo.param.PsInstallerAndUserReqParam;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/7/8 13:30
 */
public interface DataViewSocialCountMapper {
    DataViewSocialCountVO social(@Param("reqVO") PsInstallerAndUserReqParam userReqParam);

    List<String> getPsByProvince(String province);

    DataViewSocialCountParam getPower(@Param("reqVO") PsInstallerAndUserReqParam userReqParam, @Param("date") String date);

    List<DataViewMapVO> map(@Param("reqVO") PsInstallerAndUserReqParam userReqParam);
}
