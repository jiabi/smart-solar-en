package vip.xiaonuo.biz.modular.stationinfo.controller;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.converters.Converter;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.core.enums.DateTypeEnum;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.ColumnTitleMap;
import vip.xiaonuo.biz.core.util.DoubleUtils;
import vip.xiaonuo.biz.core.util.ExportExcelUtil;
import vip.xiaonuo.biz.core.util.easyexcel.EasyExcelMaker;
import vip.xiaonuo.biz.core.util.easyexcel.ExportUtil;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoIdParam;
import vip.xiaonuo.biz.modular.stationinfo.param.*;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.*;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.*;
import vip.xiaonuo.biz.modular.stationinfo.service.StationChartService;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 电站图表控制器
 *
 * @Author wangjian
 * @Date 2024/2/28 18:06
 */
@Api(tags = "电站图表控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
@Slf4j
public class StationChartController {
    private static Logger logger = LoggerFactory.getLogger(StationInfoController.class);

    @Resource
    private StationChartService stationChartService;

    @Resource
    private StationInfoService stationInfoService;


    /**
     * 电站概览-历史发电量比重图
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("电站概览-历史发电量比重图")
//    @SaCheckPermission("/biz/chart/hisPowerProportion")
    @PostMapping("/biz/chart/hisPowerProportion")
    public CommonResult<PsOverviewHisPowerProportionVO> hisPowerProportion(@RequestBody @Valid PsIdAndDateReqParam req) {
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        if (StringUtils.isEmpty(req.getStationId())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9101);
        }
        PsOverviewHisPowerProportionVO vo = stationChartService.hisPowerProportion(type, date, req.getStationId());
        if (vo != null) {
            vo.setEnergy(DoubleUtils.round(vo.getEnergy(),2));
            vo.setEnergyToLoad(DoubleUtils.round(vo.getEnergyToLoad(),2));
            vo.setEnergyToCharge(DoubleUtils.round(vo.getEnergyToCharge(),2));
            vo.setEnergyToGrid(DoubleUtils.round(vo.getEnergyToGrid(),2));
            vo.setUsedEnergy(DoubleUtils.round(vo.getUsedEnergy(),2));
            vo.setUsedFromEnergy(DoubleUtils.round(vo.getUsedFromEnergy(),2));
            vo.setUsedFromDischarge(DoubleUtils.round(vo.getUsedFromDischarge(),2));
            vo.setUsedFromPurchase(DoubleUtils.round(vo.getUsedFromPurchase(),2));
        }
        return CommonResult.data(vo);
    }

    /**
     * 电站概览-历史发电量比重图导出
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("电站概览-历史发电量比重图导出")
//    @SaCheckPermission("/biz/chart/hisPowerProportionExport")
    @PostMapping("/biz/chart/hisPowerProportionExport")
    public void hisPowerProportionExport(HttpServletResponse response, @RequestBody @Valid PsIdAndDateReqParam req) {
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        if (StringUtils.isEmpty(req.getStationId())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9101);
        }
        PsOverviewHisPowerProportionVO resVO = stationChartService.hisPowerProportion(type, date, req.getStationId());
        if (resVO != null) {
            resVO.setEnergy(DoubleUtils.round(resVO.getEnergy(),2));
            resVO.setEnergy(DoubleUtils.round(resVO.getEnergyToLoad(),2));
            resVO.setEnergy(DoubleUtils.round(resVO.getEnergyToCharge(),2));
            resVO.setEnergy(DoubleUtils.round(resVO.getEnergyToGrid(),2));
            resVO.setEnergy(DoubleUtils.round(resVO.getUsedEnergy(),2));
            resVO.setEnergy(DoubleUtils.round(resVO.getUsedFromEnergy(),2));
            resVO.setEnergy(DoubleUtils.round(resVO.getUsedFromDischarge(),2));
            resVO.setEnergy(DoubleUtils.round(resVO.getUsedFromPurchase(),2));
        }
        if (ObjectUtils.isEmpty(resVO)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }

        List<PsOverviewHisPowerProportionExportVO> list = new ArrayList<>();
        PsOverviewHisPowerProportionExportVO exportVO = new PsOverviewHisPowerProportionExportVO();
        list.add(exportVO);

        EasyExcelMaker excelMaker = EasyExcelMaker.getInstance();
        excelMaker.make(response, list, PsOverviewHisPowerProportionExportVO.class, translateFileName("电站概览历史发电比重数据导出") + ".xls");
        log.debug("=====================监控平台电站历史电量比重图导出结束===================");

    }


    /**
     * 图表管理-电站图表-实时基础信息
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("图表管理-电站图表-实时基础信息")
//    @SaCheckPermission("/biz/chart/psChartBaseInfo")
    @PostMapping("/biz/chart/psChartBaseInfo")
    public CommonResult<PsChartBaseInfoResVO> psChartBaseInfo(@RequestBody @Valid StationInfoIdParam req) {
        if (StringUtils.isEmpty(req.getId())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9101);
        }
        return CommonResult.data(stationChartService.psChartBaseInfo(req));
    }

    /**
     * 图表管理-电站图表
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("图表管理-电站图表")
//    @SaCheckPermission("/biz/chart/psChart")
    @PostMapping("/biz/chart/psChart")
    public CommonResult<PsChartDataResVO> psChart(@RequestBody @Valid PsIdAndDateReqParam req) {
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        if(StringUtils.isEmpty(date) && type != 4){
            throw new SkyCommonException(SkyCommonExceptionEnum.DATE_NAME_9136);
        }
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        if (StringUtils.isEmpty(req.getStationId())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9101);
        }
        return CommonResult.data(stationChartService.psChart(type,date, Collections.singletonList(req.getStationId())));
    }

    /**
     * 图表管理-电站图表-导出
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("图表管理-电站图表-导出")
//    @SaCheckPermission("/biz/chart/psChart")
    @PostMapping("/biz/chart/psChartExport")
    public void psChart(HttpServletResponse response, @RequestBody @Valid PsIdAndDateReqParam req) {
        log.debug("=====================电站图表导出-开始===================");
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        if (StringUtils.isEmpty(req.getStationId())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9101);
        }
        PsChartDataResVO resDTO = stationChartService.psChart(type, date, Collections.singletonList(req.getStationId()));
        if (resDTO == null) {
            return;
        }

        EasyExcelMaker excelMaker = EasyExcelMaker.getInstance();

        if (DateTypeEnum.DAY.getType().equals(type)) {
            //日发电统计
            StationInfoIdParam stationInfoIdParam = new StationInfoIdParam();
            stationInfoIdParam.setId(req.getStationId());
            StationInfoVO detail = stationInfoService.detail(stationInfoIdParam);
            // 全额并网-直流发电功率
            if (detail.getStationSystem() == 1) {
                List<PsChartDayInPowerParam> excelData = this.dataInPowerChange(resDTO);
                excelMaker.make(response, excelData, PsChartDayInPowerParam.class,
                        ExportUtil.translateFileName("日电站直流发电功率图表"),"日电站直流发电功率图表");
            }
            // 储能/自发自用
            List<PsChartDayDataExportParam> excelStorageData = this.dataStoragePowerChange(resDTO);
            excelMaker.make(response, excelStorageData, PsChartDayDataExportParam.class,
                    ExportUtil.translateFileName("日电站图表"),"日电站图表");
        } else if (DateTypeEnum.MONTH.getType().equals(type)) {
            //月发电统计
            List<PsChartAnalysisExcelResParam> excelData = this.dataChange(resDTO);
            excelMaker.make(response, excelData, PsChartAnalysisExcelResParam.class,
                    ExportUtil.translateFileName("月电站图表"),"月电站图表");
        } else if (DateTypeEnum.YEAR.getType().equals(type)) {
            //年发电统计
            List<PsChartAnalysisExcelResParam> excelData = this.dataChange(resDTO);
            excelMaker.make(response, excelData, PsChartAnalysisExcelResParam.class,
                    ExportUtil.translateFileName("年电站图表"),"年电站图表");
        } else if (DateTypeEnum.ALL.getType().equals(type)) {
            //总发电统计
            List<PsChartAnalysisExcelResParam> excelData = this.dataChange(resDTO);
            excelMaker.make(response, excelData, PsChartAnalysisExcelResParam.class,
                    ExportUtil.translateFileName("累计电站图表"),"累计电站图表");
        }
        log.debug("=====================电站图表导出-结束===================");
    }

    private List<PsChartDayDataExportParam> dataStoragePowerChange(PsChartDataResVO resDTO) {
        List<PsChartDayDataExportParam> excelData = new ArrayList<>();
        List<String> time = resDTO.getDateList() ==null ? new ArrayList<>():resDTO.getDateList();
        List<PsChartDayDataParam> inPower = resDTO.getDayDataList();

        int size = resDTO.getDateList() == null  ? 1: resDTO.getDateList().size();
        for (int i = 0; i < size; i++) {
            PsChartDayDataExportParam monthData = new PsChartDayDataExportParam();
            monthData.setMonitorTime(i >= time.size() ? null : time.get(i));
            if (inPower == null) {
                excelData.add(monthData);
                continue;
            }
            if (i < inPower.size()) {
                BeanUtil.copyProperties(inPower.get(i), monthData);
            }
            excelData.add(monthData);
        }
        return excelData;
    }

    private List<PsChartDayInPowerParam> dataInPowerChange(PsChartDataResVO resDTO) {
        List<PsChartDayInPowerParam> excelData = new ArrayList<>();
        List<String> time = resDTO.getDateList() ==null ? new ArrayList<>():resDTO.getDateList();
        List<PsChartDayDataParam> inPower = resDTO.getDayDataList();

        int size = resDTO.getDateList() == null  ? 1: resDTO.getDateList().size();
        for (int i = 0; i < size; i++) {
            PsChartDayInPowerParam monthData = new PsChartDayInPowerParam();
            monthData.setMonitorTime(i >= time.size() ? null : time.get(i));
            if (inPower == null) {
                monthData.setInPower(null);
                excelData.add(monthData);
                continue;
            }
            monthData.setInPower(i >= inPower.size() ? null : inPower.get(i).getInPower());
            excelData.add(monthData);
        }
        return excelData;
    }

    private List<PsChartAnalysisExcelResParam> dataChange(PsChartDataResVO resDTO){
        List<PsChartAnalysisExcelResParam> excelData = new ArrayList<>();
        List<String> time = resDTO.getDateList() == null ? new ArrayList<>(Collections.singletonList("0")) : resDTO.getDateList();
        List<Double> power = CollectionUtils.isEmpty(resDTO.getPower()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getPower();
        List<Double> energy = CollectionUtils.isEmpty(resDTO.getEnergy()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getEnergy();
        List<Double> gridEnergy = CollectionUtils.isEmpty(resDTO.getGridEnergy()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getGridEnergy();
        List<Double> gridPurchase = CollectionUtils.isEmpty(resDTO.getGridPurchase()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getGridPurchase();
        List<Double> charging = CollectionUtils.isEmpty(resDTO.getCharging()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getCharging();
        List<Double> discharging = CollectionUtils.isEmpty(resDTO.getDischarging()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getDischarging();
        int size = resDTO.getDateList() == null  ? 1: resDTO.getDateList().size();
        for (int i = 0; i < size; i++) {
            PsChartAnalysisExcelResParam monthData = new PsChartAnalysisExcelResParam();
            monthData.setTime(i >= time.size() ? "0" : time.get(i));
            monthData.setDayGenerate(i >= power.size() ? 0D : power.get(i));
            monthData.setDayUse(i >= energy.size() ? 0D : energy.get(i));
            monthData.setDayGrid(i >= gridEnergy.size() ? 0D : gridEnergy.get(i));
            monthData.setDayBuy(i >= gridPurchase.size() ? 0D : gridPurchase.get(i));
            monthData.setDayCharge(i >= charging.size() ? 0D : charging.get(i));
            monthData.setDayDischarge(i >= discharging.size() ? 0D : discharging.get(i));
            excelData.add(monthData);
        }
        return excelData;
    }

    /**
     * 图表管理-设备图表-实时基础信息
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("图表管理-设备图表-实时基础信息")
//    @SaCheckPermission("/biz/chart/equChartBaseInfo")
    @PostMapping("/biz/chart/equChartBaseInfo")
    public CommonResult<PsEquChartBaseInfoResVO> equChartBaseInfo(@RequestBody @Valid SkyInvInfoIdParam req) {
        if (StringUtils.isEmpty(req.getInvSn())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.SN_CHECK_NOT_NULL_9107);
        }
        return CommonResult.data(stationChartService.equChartBaseInfo(req));
    }

    /**
     * 图表管理-设备图表
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("图表管理-设备图表")
//    @SaCheckPermission("/biz/chart/equChart")
    @PostMapping("/biz/chart/equChart")
    public CommonResult<PsEquChartResVO> equChart(@RequestBody @Valid PsEquChartReqParam req) {
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        if (StringUtils.isEmpty(req.getDeviceSn())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.SN_CHECK_NOT_NULL_9107);
        }
        String deviceSn = req.getDeviceSn();
        List<String> paramsList = req.getParamsList();
        return CommonResult.data(stationChartService.equChart(deviceSn, date, type,paramsList));
    }


    /**
     * 图表管理-设备图表-导出
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("图表管理-设备图表-导出")
//    @SaCheckPermission("/biz/chart/equChartExport")
    @PostMapping("/biz/chart/equChartExport")
    public void equChart(@RequestBody @Valid PsEquChartReqParam req, HttpServletResponse response) throws Exception {
        log.debug("=====================设备图表导出报表下载开始===================");
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        if (StringUtils.isEmpty(req.getDeviceSn())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.SN_CHECK_NOT_NULL_9107);
        }
        String deviceSn = req.getDeviceSn();
        List<String> paramsList = req.getParamsList();
        PsEquChartResVO data = stationChartService.equChart(deviceSn, date, type, paramsList);
        if (data == null || (ObjectUtils.isEmpty(data.getDayChart()) && ObjectUtils.isEmpty(data.getMonYearTotalChart()))) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }

        // 数据组装
        List<PsEquChartNullPropertiesResParam> dayChart = Lists.newArrayList();
        EasyExcelMaker excelMaker = EasyExcelMaker.getInstance();

        if (DateTypeEnum.DAY.getType().equals(type)) {
            //日发电统计
            //数据转换
            if (ObjectUtils.isEmpty(data) || ObjectUtils.isEmpty(data.getDayChart())) {
                throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
            }
            List<String> localTimeList = data.getDayChart().getTimeList();
            List<PsEquDayChartResParam.DayChartAnalysis> analysisList = data.getDayChart().getList();
            exportDayData(req, response, localTimeList, analysisList);

        } else if (DateTypeEnum.MONTH.getType().equals(type)) {
            //月发电统计
            List<PsEquChartExcelResParam> excelData = this.equDataChange(data.getMonYearTotalChart());
            excelMaker.make(response, excelData, PsEquChartExcelResParam.class,
                    ExportUtil.translateFileName("月设备图表"),"月设备图表");
        } else if (DateTypeEnum.YEAR.getType().equals(type)) {
            //年发电统计
            List<PsEquChartExcelResParam> excelData = this.equDataChange(data.getMonYearTotalChart());
            excelMaker.make(response, excelData, PsEquChartExcelResParam.class,
                    ExportUtil.translateFileName("年设备图表"),"年设备图表");
        } else if (DateTypeEnum.ALL.getType().equals(type)) {
            //总发电统计
            List<PsEquChartExcelResParam> excelData = this.equDataChange(data.getMonYearTotalChart());
            excelMaker.make(response, excelData, PsEquChartExcelResParam.class,
                    ExportUtil.translateFileName("累计设备图表"),"累计设备图表");
        }
        log.debug("=====================设备图表导出报表下载结束===================");
    }

    private void exportDayData(PsEquChartReqParam req, HttpServletResponse response, List<String> localTimeList, List<PsEquDayChartResParam.DayChartAnalysis> analysisList) {
        // 请求参数转为驼峰 增加返回列
        List<PsEquChartNameAndCommentReqParam> nameAndCommentList = req.getCheckValue();
        try {
            ArrayList<String> titleKeyList = new ColumnTitleMap("importEquChartResult").getTitleKeyList();
            Map<String, String> titleMap = new ColumnTitleMap("importEquChartResult").getColumnTitleMap();
            for(PsEquChartNameAndCommentReqParam param : nameAndCommentList) {
                String property = toCamelCase(Collections.singletonList(param.getStatus())).get(0);
                titleKeyList.add(property);
                String name = param.getName();
                // 添加导出字段单位
                if (name != null) {
                    name = getString(name);
                }
                titleMap.put(property, name);
            }
            // 一共多少条数据
            List<Map<String, Object>> reslist = new ArrayList<>();
            for(int i = 0; i < localTimeList.size(); i++){
                // 每条数据属性的封装
                Map<String, Object> objMap = new HashMap<>();
                objMap.put("time", localTimeList.get(i));
                for (PsEquDayChartResParam.DayChartAnalysis analysis: analysisList) {
                    String key = analysis.getName();
                    List dataList = analysis.getDataList();
                    Object value = i > dataList.size() ? "0" : dataList.get(i) == null ? "0" : dataList.get(i);
                    objMap.put(key, value);
                }
                reslist.add(objMap);
            }
            ExportExcelUtil.expoerDataExcel("日设备图表", "日设备图表", response, titleKeyList, titleMap, reslist);
        } catch (IOException e) {
            logger.error("日设备图表", e);
        }
    }

    private String getString(String key) {
        if (key.contains("功率")) {
            if (!key.contains("kW")) {
                key += "（kW）";
            }
        } else if (key.contains("电量") || key.contains("用电")) {
            key += "（kWh）";
        } else if (key.contains("电压") && !key.contains("（V）") && !key.contains("(V)")) {
            key += "（V）";
        } else if (key.contains("电流") && !key.contains("（A）") && !key.contains("(A)")) {
            key += "（A）";
        } else if (key.contains("温度") && !key.contains("℃")) {
            key += "（℃）";
        } else if (key.contains("频率") && !key.contains("Hz")) {
            key += "（Hz）";
        }
        return key;
    }

    private List<PsEquChartExcelResParam> equDataChange(PsEquChartResParam resDTO){
        List<PsEquChartExcelResParam> excelData = new ArrayList<>();
//        List<String> time = resDTO.getDateList() ==null ? new ArrayList<>():resDTO.getDateList();
//        List<Double> power = resDTO.getPower();
//        List<Double> energy = resDTO.getEnergy();
//        List<Double> gridEnergy = resDTO.getGridEnergy();
//        List<Double> gridPurchase = resDTO.getGridPurchase();
//        List<Double> charging = resDTO.getCharging();
//        List<Double> discharging = resDTO.getDischarging();

        List<String> time = resDTO.getDateList() == null ? new ArrayList<>(Collections.singletonList("0")) : resDTO.getDateList();
        List<Double> power = CollectionUtils.isEmpty(resDTO.getPower()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getPower();
        List<Double> energy = CollectionUtils.isEmpty(resDTO.getEnergy()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getEnergy();
        List<Double> gridEnergy = CollectionUtils.isEmpty(resDTO.getGridEnergy()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getGridEnergy();
        List<Double> gridPurchase = CollectionUtils.isEmpty(resDTO.getGridPurchase()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getGridPurchase();
        List<Double> charging = CollectionUtils.isEmpty(resDTO.getCharging()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getCharging();
        List<Double> discharging = CollectionUtils.isEmpty(resDTO.getDischarging()) ? new ArrayList<>(Collections.singletonList(0D)) : resDTO.getDischarging();

        int size = resDTO.getDateList() == null ? 1: resDTO.getDateList().size();
        for (int i = 0; i < size; i++) {
            PsEquChartExcelResParam monthData = new PsEquChartExcelResParam();
            monthData.setTime(i >= time.size() ? "0" : time.get(i));
            monthData.setDayGenerate(i >= power.size() ? 0D : power.get(i));
            monthData.setDayUse(i >= energy.size() ? 0D : energy.get(i));
             monthData.setDayGrid(i >= gridEnergy.size() ? 0D : gridEnergy.get(i));
            monthData.setDayBuy(i >= gridPurchase.size() ? 0D : gridPurchase.get(i));
            monthData.setDayCharge(i >= charging.size() ? 0D : charging.get(i));
            monthData.setDayDischarge(i >= discharging.size() ? 0D : discharging.get(i));
            excelData.add(monthData);
        }
        return excelData;
    }

    public String translateFileName(String fileName) {
        String name;
        try {
            name = URLEncoder.encode(fileName, "UTF-8");
        } catch (Exception e) {
            throw new SkyCommonException(SkyCommonExceptionEnum.FILE_NAME_ENCODING_FAILED_9102);
        }
        return name;
    }

    /**
     * 校验日期
     *
     * @param date 日期
     * @param type 类型（1-日2-月3-年4-总）
     * @return
     */
    private boolean validateDate(String date, int type) {
        String pattern;
        switch (type) {
            case 1:
                pattern = "\\d{4}-\\d{2}-\\d{2}";
                break;
            case 2:
                pattern = "\\d{4}-\\d{2}";
                break;
            case 3:
                pattern = "\\d{4}";
                break;
            case 4:
                return true;
            default:
                // 非法类型，直接返回false
                return false;
        }
        return Pattern.matches(pattern, date);
    }

    /**
     * 将例如STATION_ID转为驼峰命名
     * @param paramsList
     * @return
     */
    private List<String> toCamelCase(List<String> paramsList) {
        List<String> resultList = new ArrayList<>();
        for (String snakeCase:paramsList) {
            StringBuilder result = new StringBuilder();
            String[] words = snakeCase.split("_");
            for (int i = 0; i < words.length; i++) {
                String word = words[i];
                if (i == 0) {
                    result.append(word.toLowerCase());
                } else {
                    result.append(Character.toUpperCase(word.charAt(0)))
                            .append(word.substring(1).toLowerCase());
                }
            }
            resultList.add(result.toString());
        }
        return resultList;
    }
}
