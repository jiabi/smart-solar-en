/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.alarminfocurrent.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.*;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.invinfo.service.SkyInvInfoService;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoIdParam;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoPageModel;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoVO;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.alarminfocurrent.entity.SkyAlarmInfoCurrent;
import vip.xiaonuo.biz.modular.alarminfocurrent.mapper.SkyAlarmInfoCurrentMapper;
import vip.xiaonuo.biz.modular.alarminfocurrent.service.SkyAlarmInfoCurrentService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 故障信息实时信息Service接口实现类
 *
 * @author 全佳璧
 * @date  2023/10/23 10:48
 **/
@Service
public class SkyAlarmInfoCurrentServiceImpl extends ServiceImpl<SkyAlarmInfoCurrentMapper, SkyAlarmInfoCurrent> implements SkyAlarmInfoCurrentService {


    @Resource
    private SkyAlarmInfoCurrentMapper alarmInfoCurrentMapper;

    @Resource
    private SkyInvInfoMapper skyInvInfoMapper;

    @Resource
    private StationInfoService stationInfoService;

    @Resource
    private SkyInvInfoService skyInvInfoService;

    @Override
    public Page<SkyAlarmInfoPageVO> page(SkyAlarmInfoPageParam skyAlarmInfoPageParam) {
//        QueryWrapper<SkyAlarmInfoCurrent> queryWrapper = new QueryWrapper<>();
//        if (!ObjectUtil.isEmpty(skyAlarmInfoPageParam.getEquSn())) {
//            queryWrapper.lambda().ge(SkyAlarmInfoCurrent::getStationEquId, skyAlarmInfoPageParam.getEquSn());
//        }
//        if (!ObjectUtil.isEmpty(skyAlarmInfoPageParam.getBeginTime())) {
//            queryWrapper.lambda().ge(SkyAlarmInfoCurrent::getBeginTime, skyAlarmInfoPageParam.getBeginTime());
//        }
//        if (!ObjectUtil.isEmpty(skyAlarmInfoPageParam.getEndTime())) {
//            queryWrapper.lambda().le(SkyAlarmInfoCurrent::getEndTime, skyAlarmInfoPageParam.getEndTime());
//        }
//        if (!CollectionUtils.isEmpty(skyAlarmInfoPageParam.getAlarmLevelList())) {
//            queryWrapper.lambda().in(SkyAlarmInfoCurrent::getAlarmLevel, skyAlarmInfoPageParam.getAlarmLevelList());
//        }
//        if (ObjectUtil.isNotEmpty(skyAlarmInfoPageParam.getStatus()) && skyAlarmInfoPageParam.getStatus().equals("2")) {
//            queryWrapper.lambda().isNotNull(SkyAlarmInfoCurrent::getEndTime);
//        } else if (ObjectUtil.isNotEmpty(skyAlarmInfoPageParam.getStatus()) && skyAlarmInfoPageParam.getStatus().equals("1")) {
//            queryWrapper.lambda().isNull(SkyAlarmInfoCurrent::getEndTime);
//        }
//        queryWrapper.lambda().orderByDesc(SkyAlarmInfoCurrent::getCreateTime);
//        Page<SkyAlarmInfoCurrent> page = alarmInfoCurrentMapper.selectPage(CommonPageRequest.defaultPage(), queryWrapper);
//
//        List<SkyAlarmInfoPageVO> vos = new ArrayList<>();
//        for (SkyAlarmInfoCurrent current : page.getRecords()) {
//            SkyAlarmInfoPageVO vo = new SkyAlarmInfoPageVO();
//            BeanUtil.copyProperties(current, vo);
//            if (ObjectUtil.isNotEmpty(vo.getEndTime())) {
//                vo.setStatus("2");
//            } else {
//                vo.setStatus("1");
//            }
//
//            // 获取电站信息 所属电站名称
//            QueryWrapper<SkyInvInfo> queryWrapper1 = new QueryWrapper<>();
//            queryWrapper1.lambda().eq(SkyInvInfo::getInvSn,vo.getEquSn());
//            SkyInvInfo skyInvInfo = skyInvInfoMapper.selectOne(queryWrapper1);
//            if (skyInvInfo!=null && StringUtils.isNotEmpty(skyInvInfo.getStationId())) {
//                StationInfoIdParam param = new StationInfoIdParam();
//                param.setId(skyInvInfo.getStationId());
//                StationInfoVO detail = stationInfoService.detail(param);
//                if (detail != null && StringUtils.isNotEmpty(detail.getStationName())) {
//                    vo.setEquName(detail.getStationName());
//                }
//            }
//
//            vos.add(vo);
//        }
//
//        Page<SkyAlarmInfoPageVO> res = new Page<>();
//        BeanUtil.copyProperties(page, res);
//        res.setTotal(page.getTotal());
//        res.setRecords(vos);
//        return res;


        Page<SkyAlarmInfoPageVO> pageDataModel = new Page<>(skyAlarmInfoPageParam.getCurrent(),skyAlarmInfoPageParam.getSize());
        List<SkyAlarmInfoPageVO> list = alarmInfoCurrentMapper.pageCurrent(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList());
        for (SkyAlarmInfoPageVO vo:list) {
            if (ObjectUtil.isNotEmpty(vo.getEndTime())) {
                vo.setStatus("2");
            } else {
                vo.setStatus("1");
            }
            if(StringUtils.isEmpty(vo.getTimeZone())){
                vo.setTimeZone(skyInvInfoService.timeZoneOperation());
            }
        }
        pageDataModel.setRecords(list);
        pageDataModel.setTotal(alarmInfoCurrentMapper.countCurrentNum(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList()));
        return pageDataModel;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyAlarmInfoCurrentAddParam skyAlarmInfoCurrentAddParam) {
        SkyAlarmInfoCurrent skyAlarmInfoCurrent = BeanUtil.toBean(skyAlarmInfoCurrentAddParam, SkyAlarmInfoCurrent.class);
        this.save(skyAlarmInfoCurrent);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyAlarmInfoCurrentEditParam skyAlarmInfoCurrentEditParam) {
        SkyAlarmInfoCurrent skyAlarmInfoCurrent = this.queryEntity(skyAlarmInfoCurrentEditParam.getId());
        BeanUtil.copyProperties(skyAlarmInfoCurrentEditParam, skyAlarmInfoCurrent);
        this.updateById(skyAlarmInfoCurrent);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyAlarmInfoCurrentIdParam> skyAlarmInfoCurrentIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyAlarmInfoCurrentIdParamList, SkyAlarmInfoCurrentIdParam::getId));
    }

    @Override
    public SkyAlarmInfoCurrent detail(SkyAlarmInfoCurrentIdParam skyAlarmInfoCurrentIdParam) {
        return this.queryEntity(skyAlarmInfoCurrentIdParam.getId());
    }

    @Override
    public SkyAlarmInfoCurrent queryEntity(String id) {
        SkyAlarmInfoCurrent skyAlarmInfoCurrent = this.getById(id);
        if(ObjectUtil.isEmpty(skyAlarmInfoCurrent)) {
            throw new CommonException("故障信息实时信息不存在，id值为：{}", id);
        }
        return skyAlarmInfoCurrent;
    }

    @Override
    public Page<SkyAlarmInfoPageVO> pageInfo(SkyAlarmInfoPageInfoParam param) {
        Page<SkyAlarmInfoPageVO> pageDataModel = new Page<>(param.getCurrent(),param.getSize());
        List<SkyAlarmInfoPageVO> list = alarmInfoCurrentMapper.pageInfo(param);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }

        // 获取设备名称
        List<String> sns = list.stream().map(SkyAlarmInfoPageVO::getEquSn).collect(Collectors.toList());
        Map<String,String> map = alarmInfoCurrentMapper.getEquNameBySn(sns);
        for (SkyAlarmInfoPageVO vo:list) {
            if (map.containsKey(vo.getEquSn())) {
                vo.setEquName(map.get(vo.getEquSn()));
            }
        }

        pageDataModel.setRecords(list);
        pageDataModel.setTotal(alarmInfoCurrentMapper.countNum(param));
        return pageDataModel;
    }

    @Override
    public List<String> alarmMsg() {
        List<SkyAlarmInfoCurrent> list = alarmInfoCurrentMapper.selectList(new QueryWrapper<SkyAlarmInfoCurrent>());
        return list.stream().map(SkyAlarmInfoCurrent::getAlarmMsg).distinct().collect(Collectors.toList());
    }

}
