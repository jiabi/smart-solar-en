package vip.xiaonuo.biz.modular.app.station.param.chart;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/12 15:01
 */
@Getter
@Setter
public class AppPsPowerChartParam {
    /**
     * 日期集合
     */
    private String dateTime;

    /**
     * 发电量集合
     */
    private Double power;

}
