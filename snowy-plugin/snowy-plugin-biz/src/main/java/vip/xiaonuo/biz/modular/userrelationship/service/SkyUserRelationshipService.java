/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.userrelationship.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.userrelationship.entity.SkyUserRelationship;
import vip.xiaonuo.biz.modular.userrelationship.param.SkyUserRelationshipAddParam;
import vip.xiaonuo.biz.modular.userrelationship.param.SkyUserRelationshipEditParam;
import vip.xiaonuo.biz.modular.userrelationship.param.SkyUserRelationshipIdParam;
import vip.xiaonuo.biz.modular.userrelationship.param.SkyUserRelationshipPageParam;

import java.util.List;

/**
 * 用户关系Service接口
 *
 * @author 全佳璧
 * @date  2024/04/01 16:49
 **/
public interface SkyUserRelationshipService extends IService<SkyUserRelationship> {

    /**
     * 获取用户关系分页
     *
     * @author 全佳璧
     * @date  2024/04/01 16:49
     */
    Page<SkyUserRelationship> page(SkyUserRelationshipPageParam skyUserRelationshipPageParam);

    /**
     * 添加用户关系
     *
     * @author 全佳璧
     * @date  2024/04/01 16:49
     */
    void add(SkyUserRelationshipAddParam skyUserRelationshipAddParam);

    /**
     * 编辑用户关系
     *
     * @author 全佳璧
     * @date  2024/04/01 16:49
     */
    void edit(SkyUserRelationshipEditParam skyUserRelationshipEditParam);

    /**
     * 删除用户关系
     *
     * @author 全佳璧
     * @date  2024/04/01 16:49
     */
    void delete(List<SkyUserRelationshipIdParam> skyUserRelationshipIdParamList);

    /**
     * 获取用户关系详情
     *
     * @author 全佳璧
     * @date  2024/04/01 16:49
     */
    SkyUserRelationship detail(SkyUserRelationshipIdParam skyUserRelationshipIdParam);

    /**
     * 获取用户关系详情
     *
     * @author 全佳璧
     * @date  2024/04/01 16:49
     **/
    SkyUserRelationship queryEntity(String id);

}
