/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.chartparamconfig.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.chartparamconfig.entity.SkyChartParamConfig;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigAddParam;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigEditParam;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigIdParam;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigPageParam;

import java.util.List;

/**
 * 参数匹配Service接口
 *
 * @author 王坚
 * @date  2024/02/21 11:51
 **/
public interface SkyChartParamConfigService extends IService<SkyChartParamConfig> {

    /**
     * 获取参数匹配分页
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     */
    Page<SkyChartParamConfig> page(SkyChartParamConfigPageParam skyChartParamConfigPageParam);

    /**
     * 添加参数匹配
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     */
    void add(SkyChartParamConfigAddParam skyChartParamConfigAddParam);

    /**
     * 编辑参数匹配
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     */
    void edit(SkyChartParamConfigEditParam skyChartParamConfigEditParam);

    /**
     * 删除参数匹配
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     */
    void delete(SkyChartParamConfigIdParam skyChartParamConfigIdParam);

    /**
     * 获取参数匹配详情
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     */
    SkyChartParamConfig detail(SkyChartParamConfigIdParam skyChartParamConfigIdParam);

    /**
     * 获取参数匹配详情
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     **/
    SkyChartParamConfig queryEntity(String id);

}
