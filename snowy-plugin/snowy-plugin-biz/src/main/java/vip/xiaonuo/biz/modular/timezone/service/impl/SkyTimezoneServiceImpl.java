/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.timezone.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.timezone.entity.SkyTimezone;
import vip.xiaonuo.biz.modular.timezone.mapper.SkyTimezoneMapper;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezoneAddParam;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezoneEditParam;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezoneIdParam;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezonePageParam;
import vip.xiaonuo.biz.modular.timezone.service.SkyTimezoneService;

import java.util.List;

/**
 * 地区时区Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/03/08 16:23
 **/
@Service
public class SkyTimezoneServiceImpl extends ServiceImpl<SkyTimezoneMapper, SkyTimezone> implements SkyTimezoneService {


    @Override
    public Page<SkyTimezone> page(SkyTimezonePageParam skyTimezonePageParam) {
        QueryWrapper<SkyTimezone> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(skyTimezonePageParam.getAddrCode())) {
            queryWrapper.lambda().like(SkyTimezone::getAddrCode, skyTimezonePageParam.getAddrCode());
        }
        if(ObjectUtil.isNotEmpty(skyTimezonePageParam.getAddrNameCn())) {
            queryWrapper.lambda().like(SkyTimezone::getAddrNameCn, skyTimezonePageParam.getAddrNameCn());
        }
        if(ObjectUtil.isNotEmpty(skyTimezonePageParam.getTimezoneEn())) {
            queryWrapper.lambda().like(SkyTimezone::getTimezoneEn, skyTimezonePageParam.getTimezoneEn());
        }
        if(ObjectUtil.isAllNotEmpty(skyTimezonePageParam.getSortField(), skyTimezonePageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(skyTimezonePageParam.getSortOrder());
            queryWrapper.orderBy(true, skyTimezonePageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(skyTimezonePageParam.getSortField()));
        } else {
            queryWrapper.lambda().orderByAsc(SkyTimezone::getId);
        }
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyTimezoneAddParam skyTimezoneAddParam) {
        SkyTimezone skyTimezone = BeanUtil.toBean(skyTimezoneAddParam, SkyTimezone.class);
        this.save(skyTimezone);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyTimezoneEditParam skyTimezoneEditParam) {
        SkyTimezone skyTimezone = this.queryEntity(skyTimezoneEditParam.getId());
        BeanUtil.copyProperties(skyTimezoneEditParam, skyTimezone);
        this.updateById(skyTimezone);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyTimezoneIdParam> skyTimezoneIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyTimezoneIdParamList, SkyTimezoneIdParam::getId));
    }

    @Override
    public SkyTimezone detail(SkyTimezoneIdParam skyTimezoneIdParam) {
        return this.queryEntity(skyTimezoneIdParam.getId());
    }

    @Override
    public SkyTimezone queryEntity(String id) {
        SkyTimezone skyTimezone = this.getById(id);
        if(ObjectUtil.isEmpty(skyTimezone)) {
            throw new CommonException("地区时区不存在，id值为：{}", id);
        }
        return skyTimezone;
    }

    @Override
    public String queryByAddrCode(int addrCode) {
        QueryWrapper<SkyTimezone> queryWrapper = new QueryWrapper<>();
        if(addrCode >= 0) {
            queryWrapper.lambda().eq(SkyTimezone::getAddrCode, addrCode);
        }

        SkyTimezone skyTimezone = this.getOne(queryWrapper);
        if(ObjectUtil.isEmpty(skyTimezone)) {
            throw new CommonException("地区编码不存在，地区编码为：{}", addrCode);
        }
        return skyTimezone.getTimezoneEn();
    }

}
