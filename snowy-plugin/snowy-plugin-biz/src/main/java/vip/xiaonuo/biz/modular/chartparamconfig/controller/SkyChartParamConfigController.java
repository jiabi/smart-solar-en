/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.chartparamconfig.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.chartparamconfig.entity.SkyChartParamConfig;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigAddParam;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigEditParam;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigIdParam;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigPageParam;
import vip.xiaonuo.biz.modular.chartparamconfig.service.SkyChartParamConfigService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 参数匹配控制器
 *
 * @author 王坚
 * @date  2024/02/21 11:51
 */
@Api(tags = "参数匹配控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyChartParamConfigController {

    @Resource
    private SkyChartParamConfigService skyChartParamConfigService;

    /**
     * 获取参数匹配分页
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取参数匹配分页")
//    @SaCheckPermission("/biz/chartparamconfig/page")
    @GetMapping("/biz/chartparamconfig/page")
    public CommonResult<Page<SkyChartParamConfig>> page(SkyChartParamConfigPageParam skyChartParamConfigPageParam) {
        return CommonResult.data(skyChartParamConfigService.page(skyChartParamConfigPageParam));
    }

    /**
     * 添加参数匹配
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加参数匹配")
    @CommonLog("添加参数匹配")
//    @SaCheckPermission("/biz/chartparamconfig/add")
    @PostMapping("/biz/chartparamconfig/add")
    public CommonResult<String> add(@RequestBody @Valid SkyChartParamConfigAddParam skyChartParamConfigAddParam) {
        skyChartParamConfigService.add(skyChartParamConfigAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑参数匹配
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑参数匹配")
    @CommonLog("编辑参数匹配")
//    @SaCheckPermission("/biz/chartparamconfig/edit")
    @PostMapping("/biz/chartparamconfig/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyChartParamConfigEditParam skyChartParamConfigEditParam) {
        skyChartParamConfigService.edit(skyChartParamConfigEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除参数匹配
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除参数匹配")
    @CommonLog("删除参数匹配")
//    @SaCheckPermission("/biz/chartparamconfig/delete")
    @PostMapping("/biz/chartparamconfig/delete")
    public CommonResult<String> delete(@RequestBody @Valid SkyChartParamConfigIdParam skyChartParamConfigIdParam) {
        skyChartParamConfigService.delete(skyChartParamConfigIdParam);
        return CommonResult.ok();
    }

    /**
     * 获取参数匹配详情
     *
     * @author 王坚
     * @date  2024/02/21 11:51
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取参数匹配详情")
//    @SaCheckPermission("/biz/chartparamconfig/detail")
    @GetMapping("/biz/chartparamconfig/detail")
    public CommonResult<SkyChartParamConfig> detail(@Valid SkyChartParamConfigIdParam skyChartParamConfigIdParam) {
        return CommonResult.data(skyChartParamConfigService.detail(skyChartParamConfigIdParam));
    }
//
//    /**
//     * 获取参数匹配详情
//     *
//     * @author 王坚
//     * @date  2024/02/21 11:51
//     */
//    @ApiOperationSupport(order = 6)
//    @ApiOperation("根据")
////    @SaCheckPermission("/biz/chartparamconfig/detail")
//    @GetMapping("/biz/chartparamconfig/detail")
//    public CommonResult<SkyChartParamConfig> detail(@Valid SkyChartParamConfigIdParam skyChartParamConfigIdParam) {
//        return CommonResult.data(skyChartParamConfigService.detail(skyChartParamConfigIdParam));
//    }

}
