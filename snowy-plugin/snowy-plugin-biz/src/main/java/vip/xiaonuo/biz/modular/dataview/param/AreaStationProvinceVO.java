package vip.xiaonuo.biz.modular.dataview.param;

import lombok.Data;

import java.util.List;

@Data
public class AreaStationProvinceVO {

    /**
     * 电站id
     */
    private String stationId;

    /**
     * 设备id
     */
    private String equSn;

    /**
     * 城市
     */
    private String areaCity;

    /**
     * 电站数量
     */
    private Integer stationTotal;

    /**
     * 设备数量
     */
    private Integer eqnSnTotal;
}
