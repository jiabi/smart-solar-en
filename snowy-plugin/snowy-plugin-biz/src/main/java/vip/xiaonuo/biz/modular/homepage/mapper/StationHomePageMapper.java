package vip.xiaonuo.biz.modular.homepage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import vip.xiaonuo.biz.modular.homepage.param.StationDistributeVO;
import vip.xiaonuo.biz.modular.homepage.param.StationHomePagePowerVO;
import vip.xiaonuo.biz.modular.homepage.param.StationPowerCountVO;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/2/26 14:00
 */
@Mapper
@Repository
public interface StationHomePageMapper extends BaseMapper<StationInfo> {

    /**
     * 所有电站分布
     * @return
     */
    List<StationDistributeVO> distribute();

    /**
     * 所有电站发电信息
     * @param date
     * @param month
     * @param year
     * @return
     */
    List<StationPowerCountVO> allPower(@Param("date") String date, @Param("month") String month, @Param("year") String year);

    /**
     * 首页获取所有电站基础信息
     */
    StationHomePagePowerVO getAllPsHomePageBaseInfo();

    /**
     * 获取数据更新时间
     * @return
     */
    String getUpdateTime();
}
