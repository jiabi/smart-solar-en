/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invinfo.service.impl;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.core.enums.EquTypeForTableSkyStationEquEnum;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyDeleteFlagEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.alarminfocurrent.entity.SkyAlarmInfoCurrent;
import vip.xiaonuo.biz.modular.alarminfocurrent.mapper.SkyAlarmInfoCurrentMapper;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageParam;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageVO;
import vip.xiaonuo.biz.modular.chartparamconfig.entity.SkyChartParamConfig;
import vip.xiaonuo.biz.modular.chartparamconfig.mapper.SkyChartParamConfigMapper;
import vip.xiaonuo.biz.modular.equipmodel.entity.SkyEquipModel;
import vip.xiaonuo.biz.modular.equipmodel.param.SkyEquipModelIdParam;
import vip.xiaonuo.biz.modular.equipmodel.service.SkyEquipModelService;
import vip.xiaonuo.biz.modular.eququality.entity.SkyEquQuality;
import vip.xiaonuo.biz.modular.eququality.mapper.SkyEquQualityMapper;
import vip.xiaonuo.biz.modular.equrelationship.entity.SkyEquRelationship;
import vip.xiaonuo.biz.modular.equrelationship.mapper.SkyEquRelationshipMapper;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.invinfo.param.*;
import vip.xiaonuo.biz.modular.invinfo.service.SkyInvInfoService;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.SkyInvMonitorCurrentDetailVO;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.SkyInvMonitorCurrentSnParam;
import vip.xiaonuo.biz.modular.invmonitorcurrent.service.SkyInvMonitorCurrentService;
import vip.xiaonuo.biz.modular.stationequ.entity.SkyStationEqu;
import vip.xiaonuo.biz.modular.stationequ.service.SkyStationEquService;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoIdParam;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoVO;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.dbs.api.DbsApi;
import vip.xiaonuo.dev.api.DevDfcApi;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 逆变器信息Service接口实现类
 *
 * @author 全佳璧
 * @date 2023/10/05 16:24
 **/
@Service
public class SkyInvInfoServiceImpl extends ServiceImpl<SkyInvInfoMapper, SkyInvInfo> implements SkyInvInfoService {

    @Resource
    private DbsApi dbsApi;

    @Resource
    private SkyInvInfoMapper skyInvInfoMapper;

    @Resource
    private SkyAlarmInfoCurrentMapper alarmInfoCurrentMapper;

    @Resource
    private SkyEquQualityMapper skyEquQualityMapper;

    @Resource
    private SkyEquipModelService skyEquipModelService;

    @Resource
    private StationInfoService stationInfoService;

    @Resource
    private SkyInvMonitorCurrentService skyInvMonitorCurrentService;

    @Resource
    private DevDfcApi devDfcApi;

    @Resource
    private SkyStationEquService skyStationEquService;

    @Resource
    private SkyChartParamConfigMapper skyChartParamConfigMapper;

    @Resource
    private SkyEquRelationshipMapper skyEquRelationshipMapper;



    @Override
    public Page<SkyInvInfoPageVO> page(SkyInvInfoPageParam skyInvInfoPageParam) {
        Page<SkyInvInfoPageVO> pageDataModel = new Page<>(skyInvInfoPageParam.getCurrent(), skyInvInfoPageParam.getSize());
        List<SkyInvInfoPageVO> list = skyInvInfoMapper.pageList(skyInvInfoPageParam);
        for (SkyInvInfoPageVO vo:list) {
//            //该设备是否故障状态
//            List<SkyAlarmInfoCurrent> skyAlarmInfoCurrents = alarmInfoCurrentMapper.selectList(Wrappers.lambdaQuery(SkyAlarmInfoCurrent.class).eq(SkyAlarmInfoCurrent::getEquSn, vo.getInvSn()));
//            if(!CollectionUtils.isEmpty(skyAlarmInfoCurrents)){
//                vo.setMonState("4");
//                continue;
//            }
            boolean flag = true;
            if(StringUtils.isEmpty(vo.getTimeZone())){
                vo.setTimeZone(timeZoneOperation());
                flag = false;
            }
            if (StringUtils.isEmpty(vo.getMonitorTime())) {
                vo.setMonState("10");
                continue;
            }
            LocalDateTime localDateTime=null;
            if(flag && ObjectUtil.isNotEmpty(vo.getMonitorTime())){
                localDateTime = LocalDateTime.parse(vo.getMonitorTime(),LocalDateUtils.DATETIME_FORMATTER);
            }
            if(!flag && ObjectUtil.isNotEmpty(vo.getCurrentUpdateTime())){
                localDateTime = LocalDateTime.parse(vo.getCurrentUpdateTime(), LocalDateUtils.DATETIME_FORMATTER);
            }
            if(localDateTime != null){
                boolean after = getNowTimeByZoneId(vo.getTimeZone()).isAfter(localDateTime.plusMinutes(15));
                if (after) {
                    vo.setMonState("10");
                }
            }

        }
        pageDataModel.setRecords(list);
        pageDataModel.setTotal(skyInvInfoMapper.countNum(skyInvInfoPageParam));
        return pageDataModel;
    }

    /**
     * 服务器默认时区
     * @return
     */
    @Override
    public String timeZoneOperation(){
        // 获取服务器的默认时区
        ZoneId zoneId = ZoneId.systemDefault();
        // 获取当前时间的ZonedDateTime
        ZonedDateTime zonedDateTime = ZonedDateTime.now(zoneId);
        // 获取时区偏移量
        ZoneOffset offset = zonedDateTime.getOffset();
        // 格式化偏移量为UTC+XX或UTC-XX
        String utcOffset = "UTC" + (offset.getTotalSeconds() >= 0 ? "+" : "-") +
                String.format("%d", offset.getTotalSeconds() / 3600);
        return utcOffset;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyInvInfoAddParam skyInvInfoAddParam) {
        SkyInvInfo existInv = skyInvInfoMapper.selectOne(new QueryWrapper<SkyInvInfo>().lambda().eq(SkyInvInfo::getInvSn, skyInvInfoAddParam.getInvSn()));
        if (ObjectUtil.isNotEmpty(existInv)){
            throw new SkyCommonException(SkyCommonExceptionEnum.EQU_SN_EXIST_9137, skyInvInfoAddParam.getInvSn());
        }
        SkyInvInfo skyInvInfo = BeanUtil.toBean(skyInvInfoAddParam, SkyInvInfo.class);
        // 电站绑定设备
        if (ObjectUtil.isNotEmpty(skyInvInfoAddParam.getStationId())) {
            StationInfo stationInfo = stationInfoService.getById(skyInvInfoAddParam.getStationId());
            if (stationInfo == null) {
                throw new SkyCommonException(SkyCommonExceptionEnum.STATION_NOT_EXIST_9001,skyInvInfoAddParam.getStationId());
            }
            List<SkyEquRelationship> relationships = skyInvInfoMapper.getCjEquList(skyInvInfoAddParam.getInvSn());
            if (!CollectionUtils.isEmpty(relationships)) {
                // 逆变器型号
                List<String> snList = relationships.stream().map(SkyEquRelationship::getEquSn).collect(Collectors.toList());
                List<SkyInvInfo> invInfos = skyInvInfoMapper.selectList(new QueryWrapper<SkyInvInfo>().lambda().in(SkyInvInfo::getInvSn, snList));
                Map<String, String> snModelIdMap = invInfos.stream().collect(Collectors.toMap(SkyInvInfo::getInvSn, SkyInvInfo::getModelId));

                List<SkyStationEqu> stationEquList = new ArrayList<>();
                for (SkyEquRelationship relationship:relationships) {
                    SkyStationEqu stationEqu = new SkyStationEqu();
                    stationEqu.setStationId(stationInfo.getId());
                    stationEqu.setStationName(stationInfo.getStationName());
                    if (relationship.getTypeId() == 1) {
                        stationEqu.setEquType(String.valueOf(EquTypeForTableSkyStationEquEnum.COLLECTOR));
                    } else if (relationship.getTypeId() == 2) {
                        stationEqu.setEquType(String.valueOf(EquTypeForTableSkyStationEquEnum.GRID_INV));
                    } else if (relationship.getTypeId() == 3 || relationship.getTypeId() == 5) {
                        stationEqu.setEquType(String.valueOf(EquTypeForTableSkyStationEquEnum.STORAGE_INV));
                    }
                    stationEqu.setEquSn(relationship.getEquSn());
                    stationEqu.setModelId(snModelIdMap.getOrDefault(relationship.getEquSn(),""));
                    stationEquList.add(stationEqu);
                }
                skyStationEquService.saveBatch(stationEquList);
            }
        }
        SkyInvInfo inv = skyInvInfoMapper.selectDeleted(skyInvInfo.getInvSn());
        if (inv != null && ObjectUtil.isNotEmpty(inv.getInvSn())) {
            skyInvInfoMapper.updateDeletedInfo(skyInvInfo);
        } else {
            this.save(skyInvInfo);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyInvInfoEditParam skyInvInfoEditParam) {
        SkyInvInfo skyInvInfo = this.queryEntity(skyInvInfoEditParam.getInvSn());
        BeanUtil.copyProperties(skyInvInfoEditParam, skyInvInfo);
        if (ObjectUtil.isEmpty(skyInvInfoEditParam.getStationId())) {
            skyInvInfo.setStationId("");
        }
        this.updateById(skyInvInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyInvInfoIdParam> skyInvInfoIdParamList) {
        List<String> snList = CollStreamUtil.toList(skyInvInfoIdParamList, SkyInvInfoIdParam::getInvSn);

        QueryWrapper<SkyEquQuality> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().in(SkyEquQuality::getEquSn, snList);
        boolean exists = skyEquQualityMapper.exists(queryWrapper);
        if (exists) {
//            throw new CommonException("该设备存在质保信息，请先删除该设备质保信息！");
            throw new SkyCommonException(SkyCommonExceptionEnum.EQU_DELETE_EXIST_QUALITY_9120);
        }
        // 删除电站设备关系
        skyStationEquService.update(new UpdateWrapper<SkyStationEqu>().lambda().set(SkyStationEqu::getDeleteFlag, SkyDeleteFlagEnum.DELETED.getValue()).in(SkyStationEqu::getEquSn, snList));
        // 删除设备关系
        skyEquRelationshipMapper.delete(new UpdateWrapper<SkyEquRelationship>().lambda().set(SkyEquRelationship::getDeleteFlag, SkyDeleteFlagEnum.DELETED.getValue()).in(SkyEquRelationship::getEquSn, snList));

        // 执行删除
        this.removeByIds(snList);
    }

    @Override
    public SkyInvInfoDetailVO detail(SkyInvInfoIdParam skyInvInfoIdParam) {
        SkyInvInfoDetailVO skyInvInfoDetailVO = new SkyInvInfoDetailVO();
        SkyInvInfo skyInvInfo = this.queryEntity(skyInvInfoIdParam.getInvSn());
        //该设备是否故障状态
        List<SkyAlarmInfoCurrent> skyAlarmInfoCurrents = alarmInfoCurrentMapper.selectList(Wrappers.lambdaQuery(SkyAlarmInfoCurrent.class)
                .eq(SkyAlarmInfoCurrent::getEquSn, skyInvInfoIdParam.getInvSn())
                .eq(SkyAlarmInfoCurrent::getAlarmLevel,2));
        BeanUtil.copyProperties(skyInvInfo, skyInvInfoDetailVO);
        // pv路数
        int pvNum = 0;
        if (StringUtils.isNotEmpty(skyInvInfo.getModelId())) {
            SkyEquipModelIdParam param = new SkyEquipModelIdParam();
            param.setId(skyInvInfo.getModelId());
            SkyEquipModel detail = skyEquipModelService.detail(param);
            if (detail != null && ObjectUtil.isNotEmpty(detail.getMpptCount())) {
                pvNum = detail.getMpptCount();
            }
            //如果code==0则是app调用的该方法
            if(skyInvInfoIdParam.getCode()!=null){
                if (detail.getEquType() == 1) {
                    detail.setEquType(3);
                } else if (detail.getEquType() == 3) {
                    detail.setEquType(1);
                }
            }
            skyInvInfoDetailVO.setSkyEquipModel(detail);
        }
        if (StringUtils.isNotEmpty(skyInvInfo.getStationId())) {
            StationInfoIdParam param = new StationInfoIdParam();
            param.setId(skyInvInfo.getStationId());
            StationInfoVO detail = stationInfoService.detail(param);
            skyInvInfoDetailVO.setStationInfo(detail);
        }else{
            StationInfoVO stationInfoVO = new StationInfoVO();
            stationInfoVO.setTimeZone(timeZoneOperation());
            skyInvInfoDetailVO.setStationInfo(stationInfoVO);
        }
        SkyInvMonitorCurrentSnParam currentSnParam = new SkyInvMonitorCurrentSnParam();
        currentSnParam.setInvSn(skyInvInfoIdParam.getInvSn());
        SkyInvMonitorCurrentDetailVO currentDetailVO = skyInvMonitorCurrentService.detailByInvSn(currentSnParam);
        if (currentDetailVO != null) {
            // 负载电流
            if (ObjectUtil.isNotEmpty(currentDetailVO.getLoadVol())) {
                DecimalFormat decimalFormat = new DecimalFormat("#0.00");
                currentDetailVO.setLoadCur(decimalFormat.format(
                        Optional.ofNullable(currentDetailVO.getLoadPower()).orElse(0D) * 1000 / currentDetailVO.getLoadVol())
                );
            }
            // 电表状态
            if (ObjectUtil.isEmpty(currentDetailVO.getMeterStatus())) {
                currentDetailVO.setMeterStatus(0);
            }
            // pv路数
            if (ObjectUtil.isEmpty(currentDetailVO.getPvNum())) {
                currentDetailVO.setPvNum(pvNum);
            }
            // 设备状态
            if (ObjectUtil.isEmpty(currentDetailVO.getMonitorTime()) || ObjectUtil.isEmpty(currentDetailVO.getMonState())) {
                currentDetailVO.setMonState(10);
            }
            // 设备状态
            boolean after = ifOffline(ObjectUtil.isEmpty(skyInvInfoDetailVO.getStationInfo()) || StringUtils.isEmpty(skyInvInfoDetailVO.getStationInfo().getTimeZone())
                    ? currentDetailVO.getUpdateTime() : currentDetailVO.getMonitorTime()
                    ,
                    ObjectUtil.isNotEmpty(skyInvInfoDetailVO.getStationInfo()) && StringUtils.isNotEmpty(skyInvInfoDetailVO.getStationInfo().getTimeZone())
                            ? skyInvInfoDetailVO.getStationInfo().getTimeZone() : timeZoneOperation());
            currentDetailVO.setMonState(after ? 10 : currentDetailVO.getMonState());

            // 电池状态 将电站
            if (ObjectUtil.isNotEmpty(currentDetailVO.getBatStatus())) {
                char[] charArray = Integer.toBinaryString(currentDetailVO.getBatStatus()).toCharArray();
                char[] reversedArray = reverseCharArray(charArray);
                List<Integer> batStatusList = findIndexesOfValue(reversedArray);
                currentDetailVO.setBatStatusList(batStatusList);
            }else{
                currentDetailVO.setBatStatusList(new ArrayList<>());
            }
//            if( currentDetailVO.getMonState()==1||currentDetailVO.getMonState()==2){
                skyInvInfoDetailVO.setCurrentDetailVO(currentDetailVO);
//            }
            // 详情取绝对值
            // 买电为正，卖电为负
            if (ObjectUtil.isNotEmpty(currentDetailVO.getMonPac())) {
                currentDetailVO.setMonPac(Math.abs(currentDetailVO.getMonPac()));
            }
            if (ObjectUtil.isNotEmpty(currentDetailVO.getGridTotalPower())) {
                currentDetailVO.setGridTotalPower(Math.abs(currentDetailVO.getGridTotalPower()));
            }
            if (ObjectUtil.isNotEmpty(currentDetailVO.getLoadPower())) {
                currentDetailVO.setLoadPower(Math.abs(currentDetailVO.getLoadPower()));
            }
            if (ObjectUtil.isNotEmpty(currentDetailVO.getLoadPower())) {
                currentDetailVO.setLoadPower(Math.abs(currentDetailVO.getLoadPower()));
            }
            if (ObjectUtil.isNotEmpty(currentDetailVO.getMeterPower())) {
                currentDetailVO.setMeterPower(Math.abs(currentDetailVO.getMeterPower()));
            }
            if (ObjectUtil.isNotEmpty(currentDetailVO.getBatPower())) {
                currentDetailVO.setBatPower(Math.abs(currentDetailVO.getBatPower()));
            }
        }
        else{
            //app调用
            if(skyInvInfoIdParam.getCode()!=null){
                abnormalSkyInvInfo(skyInvInfoDetailVO,skyInvInfoIdParam.getCode(),skyAlarmInfoCurrents);
            }else{
                abnormalSkyInvInfo(skyInvInfoDetailVO,null,skyAlarmInfoCurrents);
            }
        }
        SkyChartParamConfig paramConfig = skyChartParamConfigMapper.selectOne(new QueryWrapper<SkyChartParamConfig>().lambda()
                .eq(SkyChartParamConfig::getSemId, skyInvInfoDetailVO.getModelId()));
        if (paramConfig != null) {
            skyInvInfoDetailVO.setParamConfig(paramConfig.getParams());
        }
        return skyInvInfoDetailVO;
    }

    public static char[] reverseCharArray(char[] array) {
        char[] reversed = new char[array.length];
        for (int i = 0; i < array.length; i++) {
            reversed[i] = array[array.length - 1 - i];
        }
        return reversed;
    }

    public void abnormalSkyInvInfo(SkyInvInfoDetailVO skyInvInfoDetailVO, Integer code, List<SkyAlarmInfoCurrent> skyAlarmInfoCurrents){
        SkyInvMonitorCurrentDetailVO skyInvMonitorCurrentDetailVO = new SkyInvMonitorCurrentDetailVO();
        skyInvMonitorCurrentDetailVO.setMonState(10);
        skyInvMonitorCurrentDetailVO.setMonPpv(0D);
        skyInvMonitorCurrentDetailVO.setMonPac(0D);
        skyInvMonitorCurrentDetailVO.setMonToday(0D);
        skyInvMonitorCurrentDetailVO.setMonTotal(0D);
        skyInvInfoDetailVO.setCurrentDetailVO(skyInvMonitorCurrentDetailVO);
    }

    /**
     * 超过15分钟为离线
     * @param monitorTime
     * @return
     */
    public boolean ifOffline(Date monitorTime ,String zoneIdStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format1 = format.format(monitorTime);
        LocalDateTime localDateTime = LocalDateTime.parse(format1, LocalDateUtils.DATETIME_FORMATTER);
        boolean after = getNowTimeByZoneId(zoneIdStr).isAfter(localDateTime.plusMinutes(15));
         return after;
    }

    /**
     * 根据时区如UTC+10，获取当前时间
     * @param zoneIdStr
     * @return
     */
    @Override
    public LocalDateTime getNowTimeByZoneId(String zoneIdStr){
        // 获取当前日期和时间
        LocalDateTime currentDateTime = LocalDateTime.now();
        // 获取 UTC+2 时区
        ZoneId zoneId = ZoneId.of(zoneIdStr);
        // 将当前时间转换为 UTC+2 时区的时间
        ZonedDateTime zonedDateTime = currentDateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(zoneId);
        // 将 UTC+2 时区的时间转换为 LocalDateTime
        LocalDateTime localDateTime = zonedDateTime.toLocalDateTime();
        return localDateTime;
    }

    /**
     * 获取数组中值为1的下角标数值集合
     * @param charArray
     * @return
     */
    public static List<Integer> findIndexesOfValue(char[] charArray) {
        List<Integer> indexes = new ArrayList<>();
        for (int i = charArray.length - 1; i >= 0; i--) {
            if (charArray[i] == '1') {
                indexes.add(i);
            }
        }
        return indexes;
    }

    @Override
    public SkyInvInfo queryEntity(String id) {
        SkyInvInfo skyInvInfo = this.getById(id);
        if (ObjectUtil.isEmpty(skyInvInfo)) {
            throw new CommonException("逆变器信息不存在，id值为：{}", id);
        }
        return skyInvInfo;
    }

    @Override
    public List<JSONObject> dynamicFieldConfigList() {
        String currentDataSourceId = dbsApi.getCurrentDataSourceId();
        String tableName = AnnotationUtil.getAnnotationValue(SkyInvInfo.class, TableName.class);
        return devDfcApi.getTableFieldList(currentDataSourceId, tableName);
    }

    @Override
    public Page<SkyAlarmInfoPageVO> alarmPage(SkyAlarmInfoPageParam skyAlarmInfoPageParam) {
        QueryWrapper<SkyAlarmInfoCurrent> queryWrapper = new QueryWrapper<>();
        if (!ObjectUtil.isEmpty(skyAlarmInfoPageParam.getEquSn())) {
            queryWrapper.lambda().ge(SkyAlarmInfoCurrent::getEquSn, skyAlarmInfoPageParam.getEquSn());
        }
        if (!ObjectUtil.isEmpty(skyAlarmInfoPageParam.getBeginTime())) {
            queryWrapper.lambda().ge(SkyAlarmInfoCurrent::getBeginTime, skyAlarmInfoPageParam.getBeginTime());
        }
        if (!ObjectUtil.isEmpty(skyAlarmInfoPageParam.getEndTime())) {
            queryWrapper.lambda().le(SkyAlarmInfoCurrent::getEndTime, skyAlarmInfoPageParam.getEndTime());
        }
        if (!CollectionUtils.isEmpty(skyAlarmInfoPageParam.getAlarmLevelList())) {
            queryWrapper.lambda().in(SkyAlarmInfoCurrent::getAlarmLevel, skyAlarmInfoPageParam.getAlarmLevelList());
        }
        if (ObjectUtil.isNotEmpty(skyAlarmInfoPageParam.getStatus()) && skyAlarmInfoPageParam.getStatus().equals("2")) {
            queryWrapper.lambda().isNotNull(SkyAlarmInfoCurrent::getEndTime);
        } else if (ObjectUtil.isNotEmpty(skyAlarmInfoPageParam.getStatus()) && skyAlarmInfoPageParam.getStatus().equals("1")) {
            queryWrapper.lambda().isNull(SkyAlarmInfoCurrent::getEndTime);
        }
        queryWrapper.lambda().orderByDesc(SkyAlarmInfoCurrent::getCreateTime);
        Page<SkyAlarmInfoCurrent> page = alarmInfoCurrentMapper.selectPage(CommonPageRequest.defaultPage(), queryWrapper);

        List<SkyAlarmInfoPageVO> vos = new ArrayList<>();
        for (SkyAlarmInfoCurrent current : page.getRecords()) {
            SkyAlarmInfoPageVO vo = new SkyAlarmInfoPageVO();
            BeanUtil.copyProperties(current, vo);
            if (ObjectUtil.isNotEmpty(vo.getEndTime())) {
               vo.setStatus("2");
            } else {
                vo.setStatus("1");
            }

            // 获取电站信息 所属电站名称
            QueryWrapper<SkyInvInfo> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.lambda().eq(SkyInvInfo::getInvSn,vo.getEquSn());
            SkyInvInfo skyInvInfo = skyInvInfoMapper.selectOne(queryWrapper1);
            if (skyInvInfo!=null && StringUtils.isNotEmpty(skyInvInfo.getStationId())) {
                StationInfoIdParam param = new StationInfoIdParam();
                param.setId(skyInvInfo.getStationId());
                StationInfoVO detail = stationInfoService.detail(param);
                if (detail != null && StringUtils.isNotEmpty(detail.getStationName())) {
                    vo.setEquName(detail.getStationName());
                }
            }

            vos.add(vo);
        }

        Page<SkyAlarmInfoPageVO> res = new Page<>();
        BeanUtil.copyProperties(page, res);
        res.setTotal(page.getTotal());
        res.setRecords(vos);
        return res;
    }

    @Override
    public List<SkyInvNotBindResVO> invNotBind(SkyInvNotBindReqParam reqParam) {
        List<SkyInvNotBindResVO> list = skyInvInfoMapper.invNotBind(reqParam);
        for (SkyInvNotBindResVO vo:list) {
            if (StringUtils.isEmpty(vo.getMonitorTime())) {
                vo.setMonStatus(10);
                continue;
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            boolean after = false;
            try {
                after = ifOffline(dateFormat.parse(vo.getMonitorTime()), timeZoneOperation());
            } catch (ParseException e) {
                throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
            }
            if (after) {
                vo.setMonStatus(10);
            }
        }
        return list;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void invBind(SkyInvBindPsParam reqParam) {
//        if (StringUtils.isEmpty(reqParam.getStationId())) {
//            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9101);
//        }
        if (CollectionUtils.isEmpty(reqParam.getInvSnList())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.SN_CHECK_NOT_NULL_9107);
        }
        skyInvInfoMapper.invBind(reqParam.getInvSnList(),ObjectUtil.isEmpty(reqParam.getStationId()) ? "" : reqParam.getStationId());

        // 电站设备关系
        skyStationEquService.update(new UpdateWrapper<SkyStationEqu>().lambda().set(SkyStationEqu::getDeleteFlag, SkyDeleteFlagEnum.DELETED.getValue()).in(SkyStationEqu::getEquSn, reqParam.getInvSnList()));
        if (ObjectUtil.isNotEmpty(reqParam.getStationId())) {
            List<SkyStationEqu> list = new ArrayList<>();
            for (String sn:reqParam.getInvSnList()) {
                SkyStationEqu entity = new SkyStationEqu();
                if (ObjectUtil.isEmpty(reqParam.getStationId())) {
                    entity.setStationId("");
                } else {
                    entity.setStationId(reqParam.getStationId());
                }
                entity.setEquSn(sn);
                list.add(entity);
            }
            skyStationEquService.saveBatch(list);
        }
    }

}
