package vip.xiaonuo.biz.modular.app.station.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/19 13:44
 */

@Getter
@Setter
public class AppStationStatusVO {
    /** 总数量 */
    @ApiModelProperty(value = "总数量")
    private Integer total;

    /** 通讯正常 1*/
    @ApiModelProperty(value = "通讯正常")
    private Integer normal;

    /** 异常-部分离线 2*/
    @ApiModelProperty(value = "异常-部分离线")
    private Integer alarm;

    /** 全部设备离线 3*/
    @ApiModelProperty(value = "离线")
    private Integer offline;

}
