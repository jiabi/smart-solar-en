package vip.xiaonuo.biz.modular.app.inv.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/14 17:31
 */
@Getter
@Setter
public class AppSkyInvInfoPageVO {
    /** 逆变器SN */
    @ApiModelProperty(value = "逆变器SN")
    private String sn;

    /** 逆变器状态 */
    @ApiModelProperty(value = "逆变器状态")
    private Integer monState;

    /** 逆变器名称 */
    @ApiModelProperty(value = "逆变器名称")
    private String name;

    /** 设备类型（1-储能逆变器 2-并网逆变器 3-采集器） */
    @ApiModelProperty(value = "设备类型（1-储能逆变器 2-并网逆变器 3-采集器）")
    private Integer type;

    /** 设备型号 */
    @ApiModelProperty(value = "设备型号")
    private String model;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称")
    private String psGuid;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称")
    private String psName;

    /** 当前功率 */
    @ApiModelProperty(value = "当前功率")
    private Double powerNow;

    /** 当日发电量 */
    @ApiModelProperty(value = "当日发电量")
    private Double quantity;

    /** 安规检测报告 */
    @ApiModelProperty(value = "安规检测报告")
    private String reportLink;

    @ApiModelProperty(value = "时区")
    private String timeZone;

    @ApiModelProperty(value = "数据更新时间")
    private String monitorTime;

    /** 实时表数据更新时间 */
    @ApiModelProperty(value = "数据更新时间")
    private String currentUpdateTime;




}
