package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/27 9:37
 */

@Getter
@Setter
public class PsInstallerAndUserReqParam {

    /** 分销商id */
    @ApiModelProperty(value = "分销商id")
    private String distributor;

    /** 安装公司id */
    @ApiModelProperty(value = "安装公司id")
    private String stationCompany;

    /** 业主id */
    @ApiModelProperty(value = "业主id")
    private String stationContactId;
}
