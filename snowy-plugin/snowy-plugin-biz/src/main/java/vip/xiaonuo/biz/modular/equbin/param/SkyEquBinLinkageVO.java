package vip.xiaonuo.biz.modular.equbin.param;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.modular.equbin.entity.SkyEquBin;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class SkyEquBinLinkageVO {

    @ApiModelProperty(value = "升级包ID", position = 1)
    private String id;

    /** 设备类型：1采集器；2并网逆变器；3储能逆变器 */
    @ApiModelProperty(value = "设备类型：1采集器；2并网逆变器；3储能逆变器", position = 2)
    private Integer equType;

    /** 适用型号 */
    @ApiModelProperty(value = "适用型号", position = 3)
    private String equModel;

    /** 适用模块：1采集器（wstk）、2主ARM（marm）、3辅ARM（sarm）、4DSP（mdsp）、5安规（sfty） */
    @ApiModelProperty(value = "适用模块：1采集器（wstk）、2主ARM（marm）、3辅ARM（sarm）、4DSP（mdsp）、5安规（sfty）", position = 4)
    private Integer equModule;

    /** 升级包名 */
    @ApiModelProperty(value = "升级包名", position = 6)
    private String binName;

    @ApiModelProperty(value = "升级模块字典值", position = 7)
    private String binNameSplit;

    @ApiModelProperty(value = "升级模块字典名称", position = 8)
    private String binNameValue;

    private  List<SkyEquBin> childrens;

}
