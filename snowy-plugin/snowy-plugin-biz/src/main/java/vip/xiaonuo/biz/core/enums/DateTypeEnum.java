package vip.xiaonuo.biz.core.enums;

import lombok.Getter;

@Getter
public enum DateTypeEnum {
    // 时间类型
    DAY(1,"日"),
    MONTH(2,"月"),
    YEAR(3,"年"),
    ALL(4,"全部");

    private Integer type;
    private String msg;

    DateTypeEnum(Integer type, String msg) {
        this.type = type;
        this.msg = msg;
    }
}
