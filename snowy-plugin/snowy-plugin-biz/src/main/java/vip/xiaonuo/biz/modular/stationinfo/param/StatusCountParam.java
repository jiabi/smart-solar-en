package vip.xiaonuo.biz.modular.stationinfo.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2023/9/22 14:18
 **/
@Getter
@Setter
public class StatusCountParam {
    private Integer status;
    private Integer count;
}
