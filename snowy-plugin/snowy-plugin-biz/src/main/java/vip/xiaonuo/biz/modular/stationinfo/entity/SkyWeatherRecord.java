package vip.xiaonuo.biz.modular.stationinfo.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.io.Serializable;

/**
 * 天气记录(SkyWeatherRecord)实体类
 *
 * @author wangjian
 * @since 2024-05-15 13:53:29
 */

@Getter
@Setter
public class SkyWeatherRecord implements Serializable {
    private static final long serialVersionUID = 640118132472165468L;
    /**
     * 主键ID
     */
    @TableId
    private String id;
    /**
     * 地区id
     */
    private String areaId;
    /**
     * 经度
     */
    private String areaLng;
    /**
     * 纬度
     */
    private String areaLat;
    /**
     * 日期
     */
    private String date;
    /**
     * 当日白天最高气温
     */
    private Double temperatureMax;
    /**
     * 当日白天最低气温
     */
    private Double temperatureMin;
    /**
     * 当日白天平均气温
     */
    private Double temperatureAvg;
    /**
     * 白天地表 2 米最高相对湿度(%)
     */
    private Double humidityMax;
    /**
     * 白天地表 2 米最低相对湿度(%)
     */
    private Double humidityMin;
    /**
     * 白天地表 2 米平均相对湿度(%)
     */
    private Double humidityAvg;
    /**
     * 白天地表 10 米最高风速
     */
    private Double windMax;
    /**
     * 白天地表 10 米最低风速
     */
    private Double windMin;
    /**
     * 白天地表 10 米平均风速
     */
    private Double windAvg;
    /**
     * 最大向下短波辐射通量(W/M2)
     */
    private Double dswrfMax;
    /**
     * 最小向下短波辐射通量(W/M2)
     */
    private Double dswrfMin;
    /**
     * 平均向下短波辐射通量(W/M2)
     */
    private Double dswrfAvg;
    /**
     * 白天主要天气现象
     */
    private String skycon;
    /**
     * 日出时间
     */
    private String sunrise;
    /**
     * 日落时间
     */
    private String sunset;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户")
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

}

