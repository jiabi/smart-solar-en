/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationpower.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.homepage.param.HisPowerAllPowerInfoModel;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.PsEquChartBaseInfoResVO;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.PsChartBaseInfoResVO;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.PsChartDayDataParam;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.param.SkyStationPowerTaskParam;

import java.util.List;

/**
 * 电站图表Mapper接口
 *
 * @author 全佳璧
 * @date  2024/02/22 16:57
 **/
public interface SkyStationPowerMapper extends BaseMapper<SkyStationPower> {

    List<HisPowerAllPowerInfoModel> queryMonthPower(@Param("ew") QueryWrapper<SkyStationPower> queryWrapper);

//    /**
//     * 电站日数据
//     * @param date
//     * @param stationId
//     * @return
//     */
//    List<PsChartDayDataParam> psDayChart(@Param("date")String date, @Param("stationId") String stationId);

    /**
     * 电站月数据
     * @param date
     * @param stationIds
     * @return
     */
    List<HisPowerAllPowerInfoModel> psMonthChart(@Param("date")String date, @Param("stationIds") List<String> stationIds);

    /**
     * 电站年数据
     * @param date
     * @param stationIds
     * @return
     */
    List<HisPowerAllPowerInfoModel> psYearChart(@Param("date")String date, @Param("stationIds") List<String> stationIds);

    /**
     * 电站总数据
     * @param stationIds
     * @return
     */
    List<HisPowerAllPowerInfoModel> psTotalChart(@Param("stationIds") List<String> stationIds);

    /**
     * 电站总数据
     */
    List<HisPowerAllPowerInfoModel> totalPower(@Param("ew") QueryWrapper<SkyStationPower> queryWrapper);

    /**
     * 电站图表基本信息
     * @param stationId
     * @return
     */
    PsChartBaseInfoResVO psChartBaseInfo(@Param("stationId")String stationId);

    /**
     * 电站图表基本实时信息
     * @param stationId
     * @return
     */
    PsChartBaseInfoResVO getPsCurrentInfo(@Param("stationId") String stationId);

    /**
     * 设备图表基本实时信息
     * @param invSn
     * @return
     */
    PsEquChartBaseInfoResVO equChartBaseInfo(@Param("invSn") String invSn);

    SkyStationPower queryStationPowerSum(@Param("stationId") String stationId,
                                         @Param("timeType") int timeType,
                                         @Param("dateTime") String dateTime);

    SkyStationPower queryStationPower(@Param("stationId") String stationId,
                                      @Param("timeType") int timeType,
                                      @Param("dateTime") String dateTime);

    List<SkyStationPowerTaskParam> queryStationPowerSumCurrent(@Param("stationId") String stationId,
                                                               @Param("timeType") Integer timeType,
                                                               @Param("dateTime") String dateTime);
}
