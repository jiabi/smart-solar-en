/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.equrelationship.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.stream.CollectorUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.modular.collectorinfo.entity.SkyCollectorInfo;
import vip.xiaonuo.biz.modular.collectorinfo.mapper.SkyCollectorInfoMapper;
import vip.xiaonuo.biz.modular.collectormonitorcurrent.entity.SkyCollectorMonitorCurrent;
import vip.xiaonuo.biz.modular.collectormonitorcurrent.mapper.SkyCollectorMonitorCurrentMapper;
import vip.xiaonuo.biz.modular.equrelationship.param.*;
import vip.xiaonuo.biz.modular.invmonitor.entity.SkyInvMonitor;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;
import vip.xiaonuo.biz.modular.invmonitorcurrent.mapper.SkyInvMonitorCurrentMapper;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.equrelationship.entity.SkyEquRelationship;
import vip.xiaonuo.biz.modular.equrelationship.mapper.SkyEquRelationshipMapper;
import vip.xiaonuo.biz.modular.equrelationship.service.SkyEquRelationshipService;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 通讯关系Service接口实现类
 *
 * @author 全佳璧
 * @date  2023/10/23 09:50
 **/
@Service
public class SkyEquRelationshipServiceImpl extends ServiceImpl<SkyEquRelationshipMapper, SkyEquRelationship> implements SkyEquRelationshipService {

    @Resource
    private SkyEquRelationshipMapper skyEquRelationshipMapper;

    @Resource
    private SkyCollectorMonitorCurrentMapper skyCollectorMonitorCurrentMapper;

    @Resource
    private SkyInvMonitorCurrentMapper skyInvMonitorCurrentMapper;

    @Override
    public Page<SkyEquRelationship> page(SkyEquRelationshipPageParam skyEquRelationshipPageParam) {
        Page<SkyEquRelationship> pageDataModel = new Page<>(skyEquRelationshipPageParam.getCurrent(),skyEquRelationshipPageParam.getSize());
        List<SkyEquRelationship> list = skyEquRelationshipMapper.pageList(skyEquRelationshipPageParam);
        pageDataModel.setRecords(list);
        pageDataModel.setTotal(skyEquRelationshipMapper.countNum(skyEquRelationshipPageParam));
        return pageDataModel;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyEquRelationshipAddParam skyEquRelationshipAddParam) {
        SkyEquRelationship skyEquRelationship = BeanUtil.toBean(skyEquRelationshipAddParam, SkyEquRelationship.class);
        this.save(skyEquRelationship);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyEquRelationshipEditParam skyEquRelationshipEditParam) {
        SkyEquRelationship skyEquRelationship = this.queryEntity(skyEquRelationshipEditParam.getId());
        BeanUtil.copyProperties(skyEquRelationshipEditParam, skyEquRelationship);
        this.updateById(skyEquRelationship);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyEquRelationshipIdParam> skyEquRelationshipIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyEquRelationshipIdParamList, SkyEquRelationshipIdParam::getId));
    }

    @Override
    public SkyEquRelationship detail(SkyEquRelationshipIdParam skyEquRelationshipIdParam) {
        return this.queryEntity(skyEquRelationshipIdParam.getId());
    }

    @Override
    public SkyEquRelationship queryEntity(String id) {
        SkyEquRelationship skyEquRelationship = this.getById(id);
        if(ObjectUtil.isEmpty(skyEquRelationship)) {
            throw new CommonException("通讯关系不存在，id值为：{}", id);
        }
        return skyEquRelationship;
    }

    @Override
    public SkyEquRelationshipTreeVO relationBySN(SkyEquRelationshipSNParam snParam) {
        SkyEquRelationshipTreeVO treeVO = new SkyEquRelationshipTreeVO();
        List<SkyEquRelationship> list = skyEquRelationshipMapper.relationBySN(snParam);
        List<SkyEquRelationshipVO> result = new ArrayList<>();
        treeVO.setRelationshipVO(result);
        if (!CollectionUtils.isEmpty(list)) {
            List<String> invSnList = list.stream().map(SkyEquRelationship::getEquSn).collect(Collectors.toList());
            List<SkyInvMonitorCurrent> currents = skyInvMonitorCurrentMapper.selectList(new QueryWrapper<SkyInvMonitorCurrent>().lambda().in(SkyInvMonitorCurrent::getInvSn, invSnList));
            Map<String, SkyInvMonitorCurrent> currentMap = currents.stream().collect(Collectors.toMap(SkyInvMonitorCurrent::getInvSn, Function.identity()));
            Integer timeInt=0;
            Date updateTime = null;
            for (SkyEquRelationship info:list) {
                SkyEquRelationshipVO vo = new SkyEquRelationshipVO();
                BeanUtil.copyProperties(info,vo);
                if (info.getTypeId()==2 || info.getTypeId()==3) {
                    if (currentMap.containsKey(info.getEquSn())) {
                        timeInt = compareTime(currentMap.get(info.getEquSn()).getMonitorTime());
                        vo.setStatus(timeInt);
                        updateTime = currentMap.get(info.getEquSn()).getMonitorTimeFormat();
                        vo.setUpdateTime(currentMap.get(info.getEquSn()).getMonitorTimeFormat());
                    }
                }
                result.add(vo);
            }
            // 设置采集器状态
            if (StringUtils.isNotEmpty(list.get(0).getCollectorSn())) {
                treeVO.setCollectorSn(list.get(0).getCollectorSn());
                treeVO.setUpdateTime(updateTime);
                treeVO.setStatus(timeInt);
                treeVO.setTypeId(1);
            }
            treeVO.setRelationshipVO(result);
        }
        return treeVO;
    }

    /**
     * 判断逆变器是否正常
     * @param updateDate
     * @return
     */
    public Integer compareTime(LocalDateTime updateDate){
        if(ObjectUtil.isNotEmpty(updateDate)){
            LocalDateTime now = LocalDateTime.now();
            // 获取系统默认时区
            ZoneId zoneId = ZoneId.systemDefault();
            ZonedDateTime nowZonedDateTime = now.atZone(zoneId);
            ZonedDateTime otherZonedDateTime = updateDate.atZone(zoneId);

            // 计算时间差
            Duration duration = Duration.between(otherZonedDateTime, nowZonedDateTime);
            long minutes = duration.toMinutes();
            // 判断时间差是否大于15分钟
            return minutes > 15 ? 2 : 1;
        }
        return 2;
    }

    @Override
    public String getCollectorSn(SkyEquRelationshipSNParam snParam) {
        SkyEquRelationship relationship = skyEquRelationshipMapper.selectOne(new QueryWrapper<SkyEquRelationship>().lambda().eq(SkyEquRelationship::getEquSn, snParam.getEquSn()));
        if (ObjectUtil.isEmpty(relationship)) {
            return null;
        }
        return relationship.getCollectorSn();
    }

}
