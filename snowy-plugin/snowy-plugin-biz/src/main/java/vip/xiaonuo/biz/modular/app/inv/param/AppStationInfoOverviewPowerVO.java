package vip.xiaonuo.biz.modular.app.inv.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description: 发电实时vo
 * @Author: wangjian
 * @CreateTime: 2023-11-15  13:36
 */
@Getter
@Setter
public class AppStationInfoOverviewPowerVO {
    /** 当日发电量 */
    @ApiModelProperty(value = "当日发电量")
    private Double volumeDay;

    /** 当月发电量 */
    @ApiModelProperty(value = "当月发电量")
    private Double volumeMonth;

    /** 当年发电量 */
    @ApiModelProperty(value = "当年发电量")
    private Double volumeYear;

    /** 累计发电量 */
    @ApiModelProperty(value = "累计发电量")
    private Double totalVolume;

    /** 当日收益 */
    @ApiModelProperty(value = "当日收益")
    private Double dailyEarning;

    /** 发电功率 */
    @ApiModelProperty(value = "发电功率")
    private Double elecEfficiency;

    /** 电池功率 */
    @ApiModelProperty(value = "电池功率")
    private Double batPower;

    /** 电池容量 */
    @ApiModelProperty(value = "电池容量")
    private Double batSoc;

    /** 电网功率 */
    @ApiModelProperty(value = "电网功率")
    private Double monPower;

    /** 用电功率 */
    @ApiModelProperty(value = "用电功率")
    private Double loadPower;

    /** 系统功率比 */
    @ApiModelProperty(value = "系统功率比")
    private String stationSysRate;

    /** 装机容量 */
    @ApiModelProperty(value = "装机容量")
    private Double stationSize;


}