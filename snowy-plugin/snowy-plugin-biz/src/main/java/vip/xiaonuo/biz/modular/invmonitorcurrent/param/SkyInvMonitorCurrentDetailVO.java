package vip.xiaonuo.biz.modular.invmonitorcurrent.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class SkyInvMonitorCurrentDetailVO {
    /** ID */
    @ApiModelProperty(value = "ID", position = 1)
    private String id;

    /** 逆变器SN */
    @ApiModelProperty(value = "逆变器SN", position = 2)
    private String invSn;

    /** 固件版本 */
    @ApiModelProperty(value = "固件版本", position = 3)
    private String firmwareVer;

    /** 系统时间 */
    @ApiModelProperty(value = "系统时间", position = 4)
    private Date monitorTime;

    /** 逆变器温度 */
    @ApiModelProperty(value = "逆变器温度", position = 5)
    private Double invTemp;

    /** 电池温度（DCDC） */
    @ApiModelProperty(value = "电池温度（DCDC）", position = 6)
    private Double dcdcTemp;

    /** 直流输入路数 */
    @ApiModelProperty(value = "直流输入路数", position = 7)
    private Integer pvNum;

    /** 电池路数 */
    @ApiModelProperty(value = "电池路数", position = 8)
    private Integer batNum;

    /** 输入功率 */
    @ApiModelProperty(value = "输入功率", position = 9)
    private Double monPpv;

    /** 输出功率 */
    @ApiModelProperty(value = "输出功率", position = 10)
    private Double monPac;

    /** 当日发电量 */
    @ApiModelProperty(value = "当日发电量", position = 11)
    private Double monToday;

    /** 当月发电量 */
    @ApiModelProperty(value = "当月发电量", position = 12)
    private Double monMonth;

    /** 当年发电量 */
    @ApiModelProperty(value = "当年发电量", position = 13)
    private Double monYear;

    /** 累计发电量 */
    @ApiModelProperty(value = "累计发电量", position = 14)
    private Double monTotal;

    /** 直流母线电压 */
    @ApiModelProperty(value = "直流母线电压", position = 15)
    private Double monDcbus;

    /** 运行状态（1-正常 2-异常） */
    @ApiModelProperty(value = "运行状态（1-正常 2-异常）", position = 16)
    private Integer monState;

    /** 故障编号 */
    @ApiModelProperty(value = "故障编号", position = 17)
    private String monFaultcode;

    /** 故障描述 */
    @ApiModelProperty(value = "故障描述", position = 18)
    private String monFaultmsg;

    /** 电网频率 */
    @ApiModelProperty(value = "电网频率", position = 19)
    private Double gridFac;

    /** 电网电压 */
    @ApiModelProperty(value = "电网电压", position = 20)
    private Double gridVol;

    /** 电网电流 */
    @ApiModelProperty(value = "电网电流", position = 21)
    private Double gridCur;

    /** 当日并网电量 */
    @ApiModelProperty(value = "当日并网电量", position = 22)
    private Double gridEnergyToday;

    /** 当月并网电量 */
    @ApiModelProperty(value = "当月并网电量", position = 23)
    private Double gridEnergyMonth;

    /** 当年并网电量 */
    @ApiModelProperty(value = "当年并网电量", position = 24)
    private Double gridEnergyYear;

    /** 累计并网电量 */
    @ApiModelProperty(value = "累计并网电量", position = 25)
    private Double gridEnergyTotal;

    /** 当日购电量 */
    @ApiModelProperty(value = "当日购电量", position = 26)
    private Double gridPurchaseToday;

    /** 当月购电量 */
    @ApiModelProperty(value = "当月购电量", position = 27)
    private Double gridPurchaseMonth;

    /** 当年购电量 */
    @ApiModelProperty(value = "当年购电量", position = 28)
    private Double gridPurchaseYear;

    /** 累计购电量 */
    @ApiModelProperty(value = "累计购电量", position = 29)
    private Double gridPurchaseTotal;

    /** 负载电压 */
    @ApiModelProperty(value = "负载电压", position = 30)
    private Double loadVol;

    /** 用电功率 */
    @ApiModelProperty(value = "用电功率", position = 31)
    private Double loadPower;

    /** 当日负载用电 */
    @ApiModelProperty(value = "当日负载用电", position = 32)
    private Double todayLoadEnergy;

    /** 当月负载用电 */
    @ApiModelProperty(value = "当月负载用电", position = 33)
    private Double monthLoadEnergy;

    /** 当年负载用电 */
    @ApiModelProperty(value = "当年负载用电", position = 34)
    private Double yearLoadEnergy;

    /** 累计负载用电 */
    @ApiModelProperty(value = "累计负载用电", position = 35)
    private Double totalLoadEnergy;

    /** 当日用电量 */
    @ApiModelProperty(value = "当日用电量", position = 36)
    private Double todayEnergy;

    /** 当月用电量 */
    @ApiModelProperty(value = "当月用电量", position = 37)
    private Double monthEnergy;

    /** 当年用电量 */
    @ApiModelProperty(value = "当年用电量", position = 38)
    private Double yearEnergy;

    /** 累计用电量 */
    @ApiModelProperty(value = "累计用电量", position = 39)
    private Double totalEnergy;

    /** 电池状态 */
    @ApiModelProperty(value = "电池状态", position = 40)
    private Integer batStatus;

    /** 电池使用次数 */
    @ApiModelProperty(value = "电池使用次数", position = 41)
    private Integer batRuncount;

    /** 电池温度 */
    @ApiModelProperty(value = "电池温度", position = 42)
    private Double batTemp;

    /** 电池电压 */
    @ApiModelProperty(value = "电池电压", position = 43)
    private Double batVol;

    /** 电池电流 */
    @ApiModelProperty(value = "电池电流", position = 44)
    private Double batCur;

    /** 电池功率 */
    @ApiModelProperty(value = "电池功率", position = 45)
    private Double batPower;

    /** 剩余电站容量 */
    @ApiModelProperty(value = "剩余电站容量", position = 46)
    private Double batSoc;

    /** 累计充电量 */
    @ApiModelProperty(value = "累计充电量", position = 47)
    private Double batChargingTotal;

    /** 累计放电量 */
    @ApiModelProperty(value = "累计放电量", position = 48)
    private Double batDischargingTotal;

    /** 当日充电量 */
    @ApiModelProperty(value = "当日充电量", position = 49)
    private Double batChargingToday;

    /** 当月充电量 */
    @ApiModelProperty(value = "当月充电量", position = 50)
    private Double batChargingMoth;

    /** 当年充电量 */
    @ApiModelProperty(value = "当年充电量", position = 51)
    private Double batChargingYear;

    /** 当日放电量 */
    @ApiModelProperty(value = "当日放电量", position = 52)
    private Double batDischargingToday;

    /** 当月放电量 */
    @ApiModelProperty(value = "当月放电量", position = 53)
    private Double batDischargingMonth;

    /** 当年放电量 */
    @ApiModelProperty(value = "当年放电量", position = 54)
    private Double batDischargingYear;

    /** 国家标准 */
    @ApiModelProperty(value = "国家标准", position = 55)
    private String monNationalstandards;

    /** 漏电流 */
    @ApiModelProperty(value = "漏电流", position = 56)
    private Double monGfci;

    /** 绝缘阻抗 */
    @ApiModelProperty(value = "绝缘阻抗", position = 57)
    private Double monIos;

    /** 无功功率 */
    @ApiModelProperty(value = "无功功率", position = 58)
    private Double monReactivepower;

    /** 有功功率 */
    @ApiModelProperty(value = "有功功率", position = 59)
    private Double monActivepower;

    /** 视在功率 */
    @ApiModelProperty(value = "视在功率", position = 60)
    private Double monInspectingpower;

    /** 功率因素 */
    @ApiModelProperty(value = "功率因素", position = 61)
    private Double monPowerfactor;

    /** 逆变器效率 */
    @ApiModelProperty(value = "逆变器效率", position = 62)
    private Double monEfficiency;

    /** MPPT直流电压1 */
    @ApiModelProperty(value = "MPPT直流电压1", position = 63)
    private Double monPv1vol;

    /** MPPT直流电压2 */
    @ApiModelProperty(value = "MPPT直流电压2", position = 64)
    private Double monPv2vol;

    /** MPPT直流电压3 */
    @ApiModelProperty(value = "MPPT直流电压3", position = 65)
    private Double monPv3vol;

    /** MPPT直流电压4 */
    @ApiModelProperty(value = "MPPT直流电压4", position = 66)
    private Double monPv4vol;

    /** MPPT直流电压5 */
    @ApiModelProperty(value = "MPPT直流电压5", position = 67)
    private Double monPv5vol;

    /** MPPT直流电压6 */
    @ApiModelProperty(value = "MPPT直流电压6", position = 68)
    private Double monPv6vol;

    /** MPPT直流电压7 */
    @ApiModelProperty(value = "MPPT直流电压7", position = 69)
    private Double monPv7vol;

    /** MPPT直流电压8 */
    @ApiModelProperty(value = "MPPT直流电压8", position = 70)
    private Double monPv8vol;

    /** MPPT直流电压9 */
    @ApiModelProperty(value = "MPPT直流电压9", position = 71)
    private Double monPv9vol;

    /** MPPT直流电压10 */
    @ApiModelProperty(value = "MPPT直流电压10", position = 72)
    private Double monPv10vol;

    /** MPPT直流电压11 */
    @ApiModelProperty(value = "MPPT直流电压11", position = 73)
    private Double monPv11vol;

    /** MPPT直流电压12 */
    @ApiModelProperty(value = "MPPT直流电压12", position = 74)
    private Double monPv12vol;

    /** MPPT直流电压13 */
    @ApiModelProperty(value = "MPPT直流电压13", position = 75)
    private Double monPv13vol;

    /** MPPT直流电压14 */
    @ApiModelProperty(value = "MPPT直流电压14", position = 76)
    private Double monPv14vol;

    /** MPPT直流电压15 */
    @ApiModelProperty(value = "MPPT直流电压15", position = 77)
    private Double monPv15vol;

    /** MPPT直流电压16 */
    @ApiModelProperty(value = "MPPT直流电压16", position = 78)
    private Double monPv16vol;

    /** MPPT直流电压17 */
    @ApiModelProperty(value = "MPPT直流电压17", position = 79)
    private Double monPv17vol;

    /** MPPT直流电压18 */
    @ApiModelProperty(value = "MPPT直流电压18", position = 80)
    private Double monPv18vol;

    /** MPPT直流电压19 */
    @ApiModelProperty(value = "MPPT直流电压19", position = 81)
    private Double monPv19vol;

    /** MPPT直流电压20 */
    @ApiModelProperty(value = "MPPT直流电压20", position = 82)
    private Double monPv20vol;

    /** MPPT直流电压21 */
    @ApiModelProperty(value = "MPPT直流电压21", position = 83)
    private Double monPv21vol;

    /** MPPT直流电压22 */
    @ApiModelProperty(value = "MPPT直流电压22", position = 84)
    private Double monPv22vol;

    /** MPPT直流电压23 */
    @ApiModelProperty(value = "MPPT直流电压23", position = 85)
    private Double monPv23vol;

    /** MPPT直流电压24 */
    @ApiModelProperty(value = "MPPT直流电压24", position = 86)
    private Double monPv24vol;

    /** MPPT直流电流1 */
    @ApiModelProperty(value = "MPPT直流电流1", position = 87)
    private Double monPv1cur;

    /** MPPT直流电流2 */
    @ApiModelProperty(value = "MPPT直流电流2", position = 88)
    private Double monPv2cur;

    /** MPPT直流电流3 */
    @ApiModelProperty(value = "MPPT直流电流3", position = 89)
    private Double monPv3cur;

    /** MPPT直流电流4 */
    @ApiModelProperty(value = "MPPT直流电流4", position = 90)
    private Double monPv4cur;

    /** MPPT直流电流5 */
    @ApiModelProperty(value = "MPPT直流电流5", position = 91)
    private Double monPv5cur;

    /** MPPT直流电流6 */
    @ApiModelProperty(value = "MPPT直流电流6", position = 92)
    private Double monPv6cur;

    /** MPPT直流电流7 */
    @ApiModelProperty(value = "MPPT直流电流7", position = 93)
    private Double monPv7cur;

    /** MPPT直流电流8 */
    @ApiModelProperty(value = "MPPT直流电流8", position = 94)
    private Double monPv8cur;

    /** MPPT直流电流9 */
    @ApiModelProperty(value = "MPPT直流电流9", position = 95)
    private Double monPv9cur;

    /** MPPT直流电流10 */
    @ApiModelProperty(value = "MPPT直流电流10", position = 96)
    private Double monPv10cur;

    /** MPPT直流电流11 */
    @ApiModelProperty(value = "MPPT直流电流11", position = 97)
    private Double monPv11cur;

    /** MPPT直流电流12 */
    @ApiModelProperty(value = "MPPT直流电流12", position = 98)
    private Double monPv12cur;

    /** MPPT直流电流13 */
    @ApiModelProperty(value = "MPPT直流电流13", position = 99)
    private Double monPv13cur;

    /** MPPT直流电流14 */
    @ApiModelProperty(value = "MPPT直流电流14", position = 100)
    private Double monPv14cur;

    /** MPPT直流电流15 */
    @ApiModelProperty(value = "MPPT直流电流15", position = 101)
    private Double monPv15cur;

    /** MPPT直流电流16 */
    @ApiModelProperty(value = "MPPT直流电流16", position = 102)
    private Double monPv16cur;

    /** MPPT直流电流17 */
    @ApiModelProperty(value = "MPPT直流电流17", position = 103)
    private Double monPv17cur;

    /** MPPT直流电流18 */
    @ApiModelProperty(value = "MPPT直流电流18", position = 104)
    private Double monPv18cur;

    /** MPPT直流电流19 */
    @ApiModelProperty(value = "MPPT直流电流19", position = 105)
    private Double monPv19cur;

    /** MPPT直流电流20 */
    @ApiModelProperty(value = "MPPT直流电流20", position = 106)
    private Double monPv20cur;

    /** MPPT直流电流21 */
    @ApiModelProperty(value = "MPPT直流电流21", position = 107)
    private Double monPv21cur;

    /** MPPT直流电流22 */
    @ApiModelProperty(value = "MPPT直流电流22", position = 108)
    private Double monPv22cur;

    /** MPPT直流电流23 */
    @ApiModelProperty(value = "MPPT直流电流23", position = 109)
    private Double monPv23cur;

    /** MPPT直流电流24 */
    @ApiModelProperty(value = "MPPT直流电流24", position = 110)
    private Double monPv24cur;

    /** R端电网电流 */
    @ApiModelProperty(value = "R端电网电流", position = 111)
    private Double monRcur;

    /** R端电网电压 */
    @ApiModelProperty(value = "R端电网电压", position = 112)
    private Double monRvol;

    /** R端电网功率 */
    @ApiModelProperty(value = "R端电网功率", position = 113)
    private Double monRpower;

    /** S端电网电流 */
    @ApiModelProperty(value = "S端电网电流", position = 114)
    private Double monScur;

    /** S端电网电压 */
    @ApiModelProperty(value = "S端电网电压", position = 115)
    private Double monSvol;

    /** S端电网功率 */
    @ApiModelProperty(value = "S端电网功率", position = 116)
    private Double monSpower;

    /** T端电网电流 */
    @ApiModelProperty(value = "T端电网电流", position = 117)
    private Double monTcur;

    /** T端电网电压 */
    @ApiModelProperty(value = "T端电网电压", position = 118)
    private Double monTvol;

    /** T端电网功率 */
    @ApiModelProperty(value = "T端电网功率", position = 119)
    private Double monTpower;

    /** RS线电压 */
    @ApiModelProperty(value = "RS线电压", position = 120)
    private Double monRs;

    /** ST线电压 */
    @ApiModelProperty(value = "ST线电压", position = 121)
    private Double monSt;

    /** TR线电压 */
    @ApiModelProperty(value = "TR线电压", position = 122)
    private Double monTr;

    /** 组串1电流 */
    @ApiModelProperty(value = "组串1电流", position = 123)
    private Double monIsf1;

    /** 组串2电流 */
    @ApiModelProperty(value = "组串2电流", position = 124)
    private Double monIsf2;

    /** 组串3电流 */
    @ApiModelProperty(value = "组串3电流", position = 125)
    private Double monIsf3;

    /** 组串4电流 */
    @ApiModelProperty(value = "组串4电流", position = 126)
    private Double monIsf4;

    /** 组串5电流 */
    @ApiModelProperty(value = "组串5电流", position = 127)
    private Double monIsf5;

    /** 组串6电流 */
    @ApiModelProperty(value = "组串6电流", position = 128)
    private Double monIsf6;

    /** 组串7电流 */
    @ApiModelProperty(value = "组串7电流", position = 129)
    private Double monIsf7;

    /** 组串8电流 */
    @ApiModelProperty(value = "组串8电流", position = 130)
    private Double monIsf8;

    /** 组串9电流 */
    @ApiModelProperty(value = "组串9电流", position = 131)
    private Double monIsf9;

    /** 组串10电流 */
    @ApiModelProperty(value = "组串10电流", position = 132)
    private Double monIsf10;

    /** 组串11电流 */
    @ApiModelProperty(value = "组串11电流", position = 133)
    private Double monIsf11;

    /** 组串12电流 */
    @ApiModelProperty(value = "组串12电流", position = 134)
    private Double monIsf12;

    /** 组串13电流 */
    @ApiModelProperty(value = "组串13电流", position = 135)
    private Double monIsf13;

    /** 组串14电流 */
    @ApiModelProperty(value = "组串14电流", position = 136)
    private Double monIsf14;

    /** 组串15电流 */
    @ApiModelProperty(value = "组串15电流", position = 137)
    private Double monIsf15;

    /** 组串16电流 */
    @ApiModelProperty(value = "组串16电流", position = 138)
    private Double monIsf16;

    /** 组串17电流 */
    @ApiModelProperty(value = "组串17电流", position = 139)
    private Double monIsf17;

    /** 组串18电流 */
    @ApiModelProperty(value = "组串18电流", position = 140)
    private Double monIsf18;

    /** 组串19电流 */
    @ApiModelProperty(value = "组串19电流", position = 141)
    private Double monIsf19;

    /** 组串20电流 */
    @ApiModelProperty(value = "组串20电流", position = 142)
    private Double monIsf20;

    /** 组串21电流 */
    @ApiModelProperty(value = "组串21电流", position = 143)
    private Double monIsf21;

    /** 组串22电流 */
    @ApiModelProperty(value = "组串22电流", position = 144)
    private Double monIsf22;

    /** 组串23电流 */
    @ApiModelProperty(value = "组串23电流", position = 145)
    private Double monIsf23;

    /** 组串24电流 */
    @ApiModelProperty(value = "组串24电流", position = 146)
    private Double monIsf24;

    /** 组串25电流 */
    @ApiModelProperty(value = "组串25电流", position = 147)
    private Double monIsf25;

    /** 组串26电流 */
    @ApiModelProperty(value = "组串26电流", position = 148)
    private Double monIsf26;

    /** 组串27电流 */
    @ApiModelProperty(value = "组串27电流", position = 149)
    private Double monIsf27;

    /** 组串28电流 */
    @ApiModelProperty(value = "组串28电流", position = 150)
    private Double monIsf28;

    /** 组串29电流 */
    @ApiModelProperty(value = "组串29电流", position = 151)
    private Double monIsf29;

    /** 组串30电流 */
    @ApiModelProperty(value = "组串30电流", position = 152)
    private Double monIsf30;

    /** 组串31电流 */
    @ApiModelProperty(value = "组串31电流", position = 153)
    private Double monIsf31;

    /** 组串32电流 */
    @ApiModelProperty(value = "组串32电流", position = 154)
    private Double monIsf32;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 155)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 156)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 157)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 158)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 159)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 160)
    private String tenantId;

    /** 逆变器类型：1.组串机；2.单相；3.储能机；4.三相；5.微逆机 */
    @ApiModelProperty(value = "逆变器类型", position = 160)
    private String invType;
    @ApiModelProperty(value = "额定功率")
    private Double ratedPower;
    @ApiModelProperty(value = "组串个数")
    private Integer sfNum;
    @ApiModelProperty(value = "最大无功（向电网馈入）")
    private Double maxOutRacPower;
    @ApiModelProperty(value = "最大无功（从电网吸收）")
    private Double maxInRacPower;
    @ApiModelProperty(value = "dsp软件版本")
    private String dspVer;
    @ApiModelProperty(value = "辅arm软件版本")
    private String armSideVer;
    @ApiModelProperty(value = "主arm软件版本")
    private String armMianVer;
    @ApiModelProperty(value = "安规国家")
    private String regulatoryCountry;
    @ApiModelProperty(value = "协议版本号")
    private String agreeVerison;
    @ApiModelProperty(value = "工作模式(0：自发自用 1：馈电优先 2：备用模式 3：离网模式 4：用户自定义)")
    private Integer workType;
    @ApiModelProperty(value = "母线电压")
    private Double monBcbus;
    @ApiModelProperty(value = "PV输入方式(0：独立，1：并联)")
    private Integer pvInputType;
    @ApiModelProperty(value = "DSP故障码1")
    private String dspFaultCode1;
    @ApiModelProperty(value = "DSP故障码2")
    private String dspFaultCode2;
    @ApiModelProperty(value = "DSP故障码3")
    private String dspFaultCode3;
    @ApiModelProperty(value = "DSP故障码4")
    private String dspFaultCode4;
    @ApiModelProperty(value = "DSP故障码5")
    private String dspFaultCode5;
    @ApiModelProperty(value = "DSP警告码1")
    private String dspAlarmCode1;
    @ApiModelProperty(value = "DSP警告码2")
    private String dspAlarmCode2;
    @ApiModelProperty(value = "DSP警告码3")
    private String dspAlarmCode3;
    @ApiModelProperty(value = "grid r相电流")
    private Double gridRcur;
    @ApiModelProperty(value = "grid r端电网电压")
    private Double gridRvol;
    @ApiModelProperty(value = "grid r端电网功率")
    private Double gridRpower;
    @ApiModelProperty(value = "grid s端电网电流")
    private Double gridScur;
    @ApiModelProperty(value = "grid s端电网电压")
    private Double gridSvol;
    @ApiModelProperty(value = "grid s端电网功率")
    private Double gridSpower;
    @ApiModelProperty(value = "grid t端电网电流")
    private Double gridTcur;
    @ApiModelProperty(value = "grid t端电网电压")
    private Double gridTvol;
    @ApiModelProperty(value = "grid t端电网功率")
    private Double gridTpower;
    @ApiModelProperty(value = "grid无功功率")
    private Double gridReactivepower;
    @ApiModelProperty(value = "grid有功功率")
    private Double gridActivepower;
    @ApiModelProperty(value = "grid视在功率")
    private Double gridInspectingpower;
    @ApiModelProperty(value = "EPS A相电压")
    private Double espAvol;
    @ApiModelProperty(value = "EPS B相电压")
    private Double espBvol;
    @ApiModelProperty(value = "EPS C相电压")
    private Double espCvol;
    @ApiModelProperty(value = "EPS A相电流")
    private Double espAcur;
    @ApiModelProperty(value = "EPS B相电流")
    private Double espBcur;
    @ApiModelProperty(value = "EPS C相电流")
    private Double espCcur;
    @ApiModelProperty(value = "EPS无功功率（单位：kW）")
    private Double espReactivePower;
    @ApiModelProperty(value = "EPS有功功率（单位：kW）")
    private Double espPower;
    @ApiModelProperty(value = "EPS视在功率")
    private Double espInspectingpower;
    @ApiModelProperty(value = "esp 电压频率")
    private Double espFac;
    @ApiModelProperty(value = "boost温度（°C）")
    private Double boostTemperature;
    @ApiModelProperty(value = "环境温度（°C）")
    private Double environmentTemperature;
    @ApiModelProperty(value = "辅ARM故障码1")
    private String armSideCode1;
    @ApiModelProperty(value = "主ARM故障码1")
    private String armMainFaultCode1;
    @ApiModelProperty(value = "主ARM告警码1")
    private String armMianAlarmCode1;
    @ApiModelProperty(value = "逆变器总并网运行时间（min）")
    private Integer runTime;
    @ApiModelProperty(value = "负载用电功率（所有负载耗电功率=家庭负载用电功率 + 关键负载）")
    private Double loadEnergyPower;
    @ApiModelProperty(value = "购电功率（单位：kW）")
    private Double purchasePower;
    @ApiModelProperty(value = "电网R相频率")
    private Double monRfac;
    @ApiModelProperty(value = "电网S相频率")
    private Double monSfac;
    @ApiModelProperty(value = "电网T相频率")
    private Double monTfac;
    @ApiModelProperty(value = "电网总功率")
    private Double monPower;
    @ApiModelProperty(value = "电池类型")
    private Integer batType;
    @ApiModelProperty(value = "bms版本")
    private String bmsVersion;
    @ApiModelProperty(value = "电池soh")
    private Double batSoh;
    @ApiModelProperty(value = "电池充电电流")
    private Double batChargeCur;
    @ApiModelProperty(value = "电池放电电流")
    private Double batDischargeCur;
    @ApiModelProperty(value = "电池充电功率")
    private Double batChargePower;
    @ApiModelProperty(value = "电池放电功率")
    private Double batDischargePower;
    @ApiModelProperty(value = "pv当日给电池充电电量")
    private Double pvChargeBatDay;
    @ApiModelProperty(value = "pv给电池总充电量")
    private Double pvChargeBatTotal;
    @ApiModelProperty(value = "软件升级进程")
    private Integer softwareUpProcess;
    @ApiModelProperty(value = "软件升级状态")
    private Integer softwareUpStatus;
    @ApiModelProperty(value = "按5分钟处理过的时间")
    private Date monitorTimeFormat;

    /*******************非直接数据库字段分割线****************/

    /** 电网总功率 */
    @ApiModelProperty(value = "电网总功率", position = 162)
    private Double gridTotalPower;

    /** 用电总视在功率 */
    @ApiModelProperty(value = "用电总视在功率", position = 163)
    private Double loadInspectingPower;

    /** 负载电流 */
    @ApiModelProperty(value = "负载电流", position = 31)
    private String loadCur;

    /**
     * pv1至pv16功率
     */
    private Double pv1Power;
    private Double pv2Power;
    private Double pv3Power;
    private Double pv4Power;
    private Double pv5Power;
    private Double pv6Power;
    private Double pv7Power;
    private Double pv8Power;
    private Double pv9Power;
    private Double pv10Power;
    private Double pv11Power;
    private Double pv12Power;
    private Double pv13Power;
    private Double pv14Power;
    private Double pv15Power;
    private Double pv16Power;

    private List<Integer> batStatusList;

    /** 组串（string)路数（测点key：P192） */
    private Integer seriesNum;
    /** 电池容量(单位： kwh；测点key：BATT20) */
    private Double batSize;
    /** CT1/电表1正向有功电量 */
    private Double forwardActive;
    /** CT1/电表1反向有功电量 */
    private Double backwardActive;
    /** 主从机类型（0：不使能并机，1：主机，2：从机） **/
    private Integer mainSideType;
    /** 信号强度 **/
    private Integer signalStrength;
    /** 电表状态（E2） **/
    private Integer meterStatus;

    /** 电表频率（E16） **/
    private Double meterFac;

    /** 电表总功率（E19） **/
    private Double meterPower;

    /** 正向有功电量（E43） **/
    private Double meterForwardActive;

    /** 反向有功电量（E44） **/
    private Double meterBackwardActive;
}
