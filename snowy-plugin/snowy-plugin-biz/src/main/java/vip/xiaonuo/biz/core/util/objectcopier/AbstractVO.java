package vip.xiaonuo.biz.core.util.objectcopier;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @Author wangjian
 * @Date 2024/3/20 18:15
 */
public abstract class AbstractVO implements Serializable {

    private static final long serialVersionUID = -3760675218487262838L;

    @Override
    public String toString() {
        return StringEscapeUtils.unescapeJava(ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE));
    }
}
