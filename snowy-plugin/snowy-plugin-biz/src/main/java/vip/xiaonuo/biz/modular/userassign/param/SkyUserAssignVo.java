package vip.xiaonuo.biz.modular.userassign.param;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class SkyUserAssignVo {

    /** 电站ID */
    @TableId
    @ApiModelProperty(value = "电站ID", position = 1)
    private String id;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称", position = 2)
    private String stationName;

    /** 电站备注 */
    @ApiModelProperty(value = "电站备注", position = 3)
    private String stationMemo;

    /** 建站日期 */
    @ApiModelProperty(value = "建站日期", position = 4)
    private Date stationCreatetime;

    /** 时区 */
    @ApiModelProperty(value = "时区", position = 6)
    private String timeZone;

    /** 装机容量 */
    @ApiModelProperty(value = "装机容量", position = 7)
    private Double stationSize;

    /** 电站类型 */
    @ApiModelProperty(value = "电站类型", position = 9)
    private Integer stationType;

    /** 电站系统 */
    @ApiModelProperty(value = "电站系统", position = 10)
    private Integer stationSystem;

    /** 并网状态 */
    @ApiModelProperty(value = "并网状态", position = 11)
    private Integer stationGridstatus;

    /** 业主ID */
    @ApiModelProperty(value = "业主ID", position = 15)
    private String stationContactid;

    /** 安装公司 */
    @ApiModelProperty(value = "安装公司Id", position = 34)
    private String stationCompany;

    @ApiModelProperty(value = "安装公司名称", position = 34)
    private String account;
}
