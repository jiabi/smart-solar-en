/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.equipmodel.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vip.xiaonuo.biz.core.util.ColumnTitleMap;
import vip.xiaonuo.biz.core.util.ExportExcelUtil;
import vip.xiaonuo.biz.modular.equcontrol.param.*;
import vip.xiaonuo.biz.modular.equipmodel.param.*;
import vip.xiaonuo.biz.modular.eququality.param.SkyEquQualityExportParam;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.equipmodel.entity.SkyEquipModel;
import vip.xiaonuo.biz.modular.equipmodel.service.SkyEquipModelService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 设备型号控制器
 *
 * @author 全佳璧
 * @date  2023/09/25 14:15
 */
@Api(tags = "设备型号控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyEquipModelController {

    private static Logger logger = LoggerFactory.getLogger(SkyEquipModelController.class);

    @Resource
    private SkyEquipModelService skyEquipModelService;

    /**
     * 获取设备型号分页
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取设备型号分页")
    @SaCheckPermission("/biz/equipmodel/page")
    @GetMapping("/biz/equipmodel/page")
    public CommonResult<Page<SkyEquipModel>> page(SkyEquipModelPageParam skyEquipModelPageParam) {
        return CommonResult.data(skyEquipModelService.page(skyEquipModelPageParam));
    }

    /**
     * 添加设备型号
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加设备型号")
    @CommonLog("添加设备型号")
    @SaCheckPermission("/biz/equipmodel/add")
    @PostMapping("/biz/equipmodel/add")
    public CommonResult<String> add(@RequestBody @Valid SkyEquipModelAddParam skyEquipModelAddParam) {
        skyEquipModelService.add(skyEquipModelAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑设备型号
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑设备型号")
    @CommonLog("编辑设备型号")
    @SaCheckPermission("/biz/equipmodel/edit")
    @PostMapping("/biz/equipmodel/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyEquipModelEditParam skyEquipModelEditParam) {
        skyEquipModelService.edit(skyEquipModelEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除设备型号
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除设备型号")
    @CommonLog("删除设备型号")
    @SaCheckPermission("/biz/equipmodel/delete")
    @PostMapping("/biz/equipmodel/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyEquipModelIdParam> skyEquipModelIdParamList) {
        skyEquipModelService.delete(skyEquipModelIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取设备型号详情
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取设备型号详情")
    @SaCheckPermission("/biz/equipmodel/detail")
    @GetMapping("/biz/equipmodel/detail")
    public CommonResult<SkyEquipModel> detail(@Valid SkyEquipModelIdParam skyEquipModelIdParam) {
        return CommonResult.data(skyEquipModelService.detail(skyEquipModelIdParam));
    }

    @ApiOperationSupport(order = 6)
    @ApiOperation("设备型号下载")
//    @SaCheckPermission("/biz/equipmodel/export")
    @PostMapping("/biz/equipmodel/export")
    public void export(SkyEquipModelExportParam exportParam, HttpServletResponse response) {
        try {
            ArrayList<String> titleKeyList = new ColumnTitleMap("equipmodelInfo").getTitleKeyList();
            Map<String, String> titleMap = new ColumnTitleMap("equipmodelInfo").getColumnTitleMap();
            List<Map<String, Object>> list = skyEquipModelService.exportInfoList(exportParam);
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            ExportExcelUtil.expoerDataExcel("设备型号", "设备型号", response, titleKeyList, titleMap, list);
        } catch (IOException e) {
            logger.error("导出设备型号失败", e);
        }
    }

    /**
     * @description: 获取设备型号集合
     * @author: wangjian
     * @date: 2023/9/27 17:48
     **/
    @ApiOperationSupport(order = 7)
    @ApiOperation("获取设备型号集合")
//    @SaCheckPermission("/biz/equipmodel/getEquipModelList")
    @GetMapping("/biz/equipmodel/getEquipModelList")
    public CommonResult<List<SkyEquipModel>> getEquipModelList(@RequestParam(name = "equType", required = false) Integer equType) {
        return CommonResult.data(skyEquipModelService.getEquipModelList(equType));
    }

    @ApiOperationSupport(order = 7)
    @ApiOperation("获取读取采集器的指令控制")
    @SaCheckPermission("/biz/collectorModel/collectorRed")
    @PostMapping("/biz/collectorModel/collectorRed")
    public CommonResult<SkyIotParam<SkyIotReadResParam>> collectorRed(@RequestBody SkyReqParam<SkyReadParamDeviceInfoParam> skyReqParam) {
        return CommonResult.data(skyEquipModelService.collectorRed(skyReqParam));
    }

    /**
     * 远程控制
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("采集器远程控制")
    @SaCheckPermission("/biz/collectorControl/collectorControl")
    @PostMapping("/biz/collectorControl/collectorControl")
    public CommonResult<SkyIotParam<SkyIotEquControlDevice>> collectorControl(@RequestBody @Valid SkyReqParam<SkyIotEquControlDevice<SetSTKConfigParam>> skyEquControlReqParam) {
        SkyIotParam<SkyIotEquControlDevice> res = skyEquipModelService.collectorControl(skyEquControlReqParam);
        return CommonResult.data(res);
    }
}
