package vip.xiaonuo.biz.modular.invupgrade.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.modular.collectorupgrade.param.SkyCollectorInvSnParam;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/5/5 21:38
 */

@Getter
@Setter
public class SkyInvUpgradeParam {

    /** 设备SN */
    private List<SkyCollectorInvSnParam> equSn;

    /** 升级模块 */
    private String upgradeModule;

    /** 型号名称 */
    private String modelName;

    /** 升级包id */
    private String equBinId;

    /** 发送时间(YYYY-MM-dd HH:mm:ss)例:2024-02-01 09:10:20 **/
    private String sendTime;

    /** 请求ID (云端下发指令唯一标识) **/
    private String requestId;

    /** 新固件的版本号 **/
    private String newVersion;

    /** 固件下载地址 **/
    private String downloadURL;

    /** 升级模块类型值 */
    private Integer upgradeModuleType;

}
