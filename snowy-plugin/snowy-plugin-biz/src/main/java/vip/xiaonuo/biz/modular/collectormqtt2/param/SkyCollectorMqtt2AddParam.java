/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.collectormqtt2.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 采集器转发添加参数
 *
 * @author hao
 * @date  2024/09/13 16:06
 **/
@Getter
@Setter
public class SkyCollectorMqtt2AddParam {

    /** 公司名称 */
    @ApiModelProperty(value = "公司名称", position = 2)
    private String mqttFactory;

    /** MQTT主域 */
    @ApiModelProperty(value = "MQTT主域", position = 3)
    private String mqttDomain;

    /** MQTT用户名 */
    @ApiModelProperty(value = "MQTT用户名", position = 4)
    private String mqttUsername;

    /** MQTT密码 */
    @ApiModelProperty(value = "MQTT密码", position = 5)
    private String mqttPassword;

    /** 服务器URL */
    @ApiModelProperty(value = "服务器URL", position = 6)
    private String mqttUrl;

    /** 预留字段1 */
    @ApiModelProperty(value = "预留字段1", position = 7)
    private String memo1;

    /** 预留字段2 */
    @ApiModelProperty(value = "预留字段2", position = 8)
    private String memo2;

    /** 预留字段3 */
    @ApiModelProperty(value = "预留字段3", position = 9)
    private String memo3;

}
