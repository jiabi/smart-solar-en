package vip.xiaonuo.biz.modular.stationinfo.param.weathertimetask;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 彩云天气api配置信息
 *
 * @Author wangjian
 * @Date 2024/5/7 17:56
 */


@Data
@Component
@ConfigurationProperties(
        prefix = "weather"
)
public class WeatherProperties {
    @Value("${weather.host}")
    private String weatherHost;
    @Value("${weather.path.ver}")
    private String pathVer;
    @Value("${weather.path.token}")
    private String pathToken;
    @Value("${weather.path.daily-method}")
    private String pathDailyMethod;

    @Value("${weather.path.seven-daily-method}")
    private String pathSevenDailyMethod;
    @Value("${weather.appcode}")
    private String appcode;
}
