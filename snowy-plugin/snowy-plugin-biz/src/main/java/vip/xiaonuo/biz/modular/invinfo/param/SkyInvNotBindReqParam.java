package vip.xiaonuo.biz.modular.invinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/11 17:21
 */
@Getter
@Setter
public class SkyInvNotBindReqParam {
    /** 逆变器SN */
    private String invSn;

    /** 设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪 */
    private Integer equType;

    /** 逆变器名称 */
    private String invName;

    /** 厂商id */
    private String companyId;
}
