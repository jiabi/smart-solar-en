/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.equipmodel.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 设备型号添加参数
 *
 * @author 全佳璧
 * @date  2023/09/25 14:15
 **/
@Getter
@Setter
public class SkyEquipModelAddParam {

    /** 设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪 */
    @ApiModelProperty(value = "设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪", position = 2)
    private Integer equType;

    /** 型号名称 */
    @ApiModelProperty(value = "型号名称", position = 3)
    private String modelName;

    /** 厂家ID */
    @ApiModelProperty(value = "厂家ID", position = 4)
    private String companyId;

    /** 额定功率 */
    @ApiModelProperty(value = "额定功率", position = 5)
    private Double ratedPower;

    /** 设备详细类型：0.单项；1.三项；2.单向表；4.双向表；5.棒式；6.导轨 */
    @ApiModelProperty(value = "设备详细类型：0.单项；1.三项；2.单向表；4.双向表；5.棒式；6.导轨", position = 6)
    private Integer detailType;

    /** MPPT路数 */
    @ApiModelProperty(value = "MPPT路数", position = 7)
    private Integer mpptCount;

    /** 电池电压类型：1.LV-48L */
    @ApiModelProperty(value = "电池电压类型：1.LV-48L", position = 8)
    private Integer batVolType;

    /** 电池类型：1.铅酸 */
    @ApiModelProperty(value = "电池类型：1.铅酸", position = 9)
    private Integer batType;

    /** 电池模式：1.电压模式 */
    @ApiModelProperty(value = "电池模式：1.电压模式", position = 10)
    private Integer batMode;

}
