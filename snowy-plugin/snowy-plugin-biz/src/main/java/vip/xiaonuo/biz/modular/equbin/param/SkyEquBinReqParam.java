package vip.xiaonuo.biz.modular.equbin.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/8/9 13:39
 */
@Getter
@Setter
public class SkyEquBinReqParam {
    /** 型号名称 */
    private String modelName;

    /** 设备类型：1采集器；2并网逆变器；3储能逆变器 */
    private Integer equType;
}
