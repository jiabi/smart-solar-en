/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.alarminfo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.alarminfo.entity.SkyAlarmInfo;
import vip.xiaonuo.biz.param.SkyAlarmInfoTripartiteVO;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageParam;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageVO;
import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteParam;

import java.util.List;

/**
 * 故障信息历史信息Mapper接口
 *
 * @author 全佳璧
 * @date  2023/10/23 10:50
 **/
public interface SkyAlarmInfoMapper extends BaseMapper<SkyAlarmInfo> {
    List<SkyAlarmInfoPageVO> pageCurrent(@Param("reqVO") SkyAlarmInfoPageParam skyAlarmInfoPageParam, @Param("list") List<Integer> alarmLevelList);


    List<SkyAlarmInfoPageVO> pageNoStatusCurrent(@Param("reqVO") SkyAlarmInfoPageParam skyAlarmInfoPageParam, @Param("list") List<Integer> alarmLevelList);


    int countCurrentNum(@Param("reqVO") SkyAlarmInfoPageParam skyAlarmInfoPageParam, @Param("list") List<Integer> alarmLevelList);

    int countNoStatusCurrentNum(@Param("reqVO") SkyAlarmInfoPageParam skyAlarmInfoPageParam, @Param("list") List<Integer> alarmLevelList);

    List<SkyAlarmInfoTripartiteVO>  alarmTripartites(@Param("invSns") List<String> invSns, SkyInvMonitorCurrentTripartiteParam skyInvMonitorCurrentTripartiteParam);
}
