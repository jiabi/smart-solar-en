package vip.xiaonuo.biz.modular.alarminfocurrent.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2023/10/12 13:38
 **/

@Getter
@Setter
public class SkyAlarmInfoPageReqDTO {
    /** 当前页 */
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    /** 每页条数 */
    @ApiModelProperty(value = "每页条数")
    private Integer size;

    /** 故障代码 */
    @ApiModelProperty(value = "故障代码")
    private String alarmCode;

    /** 故障等级 */
    @ApiModelProperty(value = "故障等级")
    private List<Integer> alarmLevelList;

    /** 电站id */
    @ApiModelProperty(value = "电站id")
    private String stationId;

    /** 设备类型 */
    @ApiModelProperty(value = "设备类型1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪")
    private Integer equType;

    /** 设备SN */
    @ApiModelProperty(value = "设备SN")
    private String equSn;

    /** 故障开始时间 */
    @ApiModelProperty(value = "故障开始时间")
    private String beginTime;

    /** 故障结束时间 */
    @ApiModelProperty(value = "故障结束时间")
    private String endTime;

    /** 故障状态 1：发生中，2：已恢复 */
    @ApiModelProperty(value = "故障状态 1：发生中，2：已恢复")
    private String status;

    @ApiModelProperty(value = "故障名称")
    private String alarmMsg;

    public int getLimitStart() {
        return (getCurrent() - 1) * getSize();
    }
}
