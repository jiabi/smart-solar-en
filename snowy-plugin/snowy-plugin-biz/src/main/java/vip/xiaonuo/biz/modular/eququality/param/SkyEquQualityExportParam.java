package vip.xiaonuo.biz.modular.eququality.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2023/9/27 16:42
 **/
@Getter
@Setter
public class SkyEquQualityExportParam {

    /** SN集合 */
    @ApiModelProperty(value = "SN集合")
    private List<String> sns;

    /** 设备SN */
    @ApiModelProperty(value = "设备SN")
    private String equSn;

    /** 设备类型 */
    @ApiModelProperty(value = "设备类型")
    private Integer equType;

    /** 设备型号 */
    @ApiModelProperty(value = "设备型号")
    private String equModel;

    /** 发货日期开始 */
    @ApiModelProperty(value = "发货日期开始")
    private String startStartDate;

    /** 发货日期结束 */
    @ApiModelProperty(value = "发货日期结束")
    private String endStartDate;

    /** 质保期限开始 */
    @ApiModelProperty(value = "质保期限开始")
    private String startEndDate;

    /** 质保期限结束 */
    @ApiModelProperty(value = "质保期限结束")
    private String endEndDate;

    /** 质保时长 */
    @ApiModelProperty(value = "质保时长")
    private Double timeLimit;

    /** 临期状态 */
    @ApiModelProperty(value = "临期状态")
    private Integer criticalStatus;

    /** 安装公司id */
    @ApiModelProperty(value = "安装公司id")
    private String stationCompany;

    /** 业主id */
    @ApiModelProperty(value = "业主id")
    private String stationContactId;

    /** 分销商 */
    @ApiModelProperty(value = "分销商")
    private String distributor;
}
