package vip.xiaonuo.biz.modular.equcontrol.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.xiaonuo.biz.modular.equbin.entity.SkyEquBin;
import vip.xiaonuo.biz.modular.equcontrol.entity.SkyMqttModbus;

/**
 * @Author wangjian
 * @Date 2024/5/6 14:55
 */
public interface SkyEquControlMapper extends BaseMapper<SkyMqttModbus> {
}
