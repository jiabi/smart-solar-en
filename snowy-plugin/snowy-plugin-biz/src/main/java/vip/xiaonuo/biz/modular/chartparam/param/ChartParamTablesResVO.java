package vip.xiaonuo.biz.modular.chartparam.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/1/4 11:41
 */

@Getter
@Setter
public class ChartParamTablesResVO {
    private String tableName;

    private String comment;
}
