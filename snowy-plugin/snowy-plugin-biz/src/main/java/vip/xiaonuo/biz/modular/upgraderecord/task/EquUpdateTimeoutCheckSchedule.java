package vip.xiaonuo.biz.modular.upgraderecord.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.modular.collectorupgrade.entity.SkyCollectorUpgrade;
import vip.xiaonuo.biz.modular.collectorupgrade.mapper.SkyCollectorUpgradeMapper;
import vip.xiaonuo.biz.modular.invupgrade.entity.SkyInvUpgrade;
import vip.xiaonuo.biz.modular.invupgrade.mapper.SkyInvUpgradeMapper;
import vip.xiaonuo.biz.modular.upgraderecord.entity.SkyUpgradeRecord;
import vip.xiaonuo.biz.modular.upgraderecord.mapper.SkyUpgradeRecordMapper;
import vip.xiaonuo.common.timer.CommonTimerTaskRunner;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/8/3 11:09
 */
@Slf4j
@Component
public class EquUpdateTimeoutCheckSchedule implements CommonTimerTaskRunner {

    @Resource
    private SkyUpgradeRecordMapper skyUpgradeRecordMapper;

    @Resource
    private SkyInvUpgradeMapper skyInvUpgradeMapper;

    @Resource
    private SkyCollectorUpgradeMapper skyCollectorUpgradeMapper;

    @Override
    public void action() {
        LocalDateTime beginTime = LocalDateTime.now();
        log.info("定时任务-设备升级超时检查-开始：{}", beginTime);
        // 1、查询所有没有升级好的升级记录
        List<SkyInvUpgrade> skyInvUpgrades = skyInvUpgradeMapper.selectList(new QueryWrapper<SkyInvUpgrade>().lambda().eq(SkyInvUpgrade::getUpgradeStatus, 1));
        List<SkyUpgradeRecord> list = skyUpgradeRecordMapper.selectList(new QueryWrapper<SkyUpgradeRecord>().lambda()
                .in(SkyUpgradeRecord::getEquSn, skyInvUpgrades.stream().map(SkyInvUpgrade::getEquSn).collect(Collectors.toList())));
        // 根据collectSn和equSn属性进行分组，并获取每组中upgradeStarttime值最大的对象
        Map<String, Map<String, SkyUpgradeRecord>> groupedRecords = list.stream()
                .collect(Collectors.groupingBy(SkyUpgradeRecord::getCollectSn,
                        Collectors.toMap(SkyUpgradeRecord::getEquSn, record -> record,
                                (record1, record2) -> record1.getUpgradeStarttime().after(record2.getUpgradeStarttime()) ? record1 : record2)));
        // 转为list
        List<SkyUpgradeRecord> upgradingList = groupedRecords.values().stream()
                .map(map -> map.values().stream().findFirst().orElse(null)) // 获取每个内部Map的第一个值
                .collect(Collectors.toList());

        // 2、判断开始升级时间到现在是否超过一个小时
        Date currentTime = new Date();
        List<SkyUpgradeRecord> timeoutUpdateList = upgradingList.stream().filter(record -> {
            Date upgradeStartTime = record.getUpgradeStarttime();
            Integer upgradeStatus = record.getUpgradeStatus();
            // 3600000毫秒 = 1小时 超过1小时且是在升级中的+
            return upgradeStartTime != null &&
                    (currentTime.getTime() - upgradeStartTime.getTime()) > 3600000 &&
                    (upgradeStatus != null && upgradeStatus >= 0);
        }).collect(Collectors.toList());

        // 3、超过一个小时的更新对应的设备更新表状态
        if (!CollectionUtils.isEmpty(timeoutUpdateList)) {
            List<String> invSnList = timeoutUpdateList.stream().map(SkyUpgradeRecord::getEquSn).collect(Collectors.toList());
            skyInvUpgradeMapper.update(null, new UpdateWrapper<SkyInvUpgrade>().lambda()
                    .set(SkyInvUpgrade::getUpgradeStatus, 0)
                    .in(SkyInvUpgrade::getEquSn, invSnList));
            // todo 采集器状态
        }
        LocalDateTime endTime = LocalDateTime.now();
        log.info("定时任务-设备升级超时检查-结束：{}。用时：{}毫秒", endTime , Duration.between(beginTime, endTime).toMillis());
    }
}
