package vip.xiaonuo.biz.modular.dataview.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/7/8 15:40
 */
@Getter
@Setter
public class DataViewSocialCountVO {
    /** 电站总数 */
    private Integer stationTotal;

    /** 设备总数 */
    private Integer equipTotal;

    /** 运行时间 */
    private Integer runDays;

    /** 总发电量 */
    private Double monTotal;

    /** 总收益 */
    private Double incomeTotal;

    /** 节约用煤（吨） */
    private Double saveCoal;

    /** 减少CO2排放（吨） */
    private Double reduceCo2;

    /** 减少SO2排放（吨） */
    private Double reduceSo2;

    /** 等效植树量（棵） */
    private Double equivalentPlanting;

    /** 总装机容量 */
    private Double installedCapacity;

    /** 售电单价 */
    private Double pricePer;

    /** 当日发电量 */
    private Double powerDaily;

    /** 当日收益 */
    private Double incomeDaily;

    /** 装机容量 */
    private Double stationSize;
}
