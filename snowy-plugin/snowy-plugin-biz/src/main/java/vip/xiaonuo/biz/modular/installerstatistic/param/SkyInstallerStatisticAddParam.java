/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.installerstatistic.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 安装商数据统计添加参数
 *
 * @author 全佳璧
 * @date  2024/03/13 09:19
 **/
@Getter
@Setter
public class SkyInstallerStatisticAddParam {

    /** 电站数量 */
    @ApiModelProperty(value = "电站数量", position = 2)
    private Integer stationTotal;

    /** 正常电站 */
    @ApiModelProperty(value = "正常电站", position = 3)
    private Integer stationNormal;

    /** 异常电站 */
    @ApiModelProperty(value = "异常电站", position = 4)
    private Integer stationAbnormal;

    /** 离线电站 */
    @ApiModelProperty(value = "离线电站", position = 5)
    private Integer stationOffline;

    /** 当日发电量 */
    @ApiModelProperty(value = "当日发电量", position = 6)
    private Double powerDaily;

    /** 当月发电量 */
    @ApiModelProperty(value = "当月发电量", position = 7)
    private Double powerMonth;

    /** 当年发电量 */
    @ApiModelProperty(value = "当年发电量", position = 8)
    private Double powerYear;

    /** 累计发电量 */
    @ApiModelProperty(value = "累计发电量", position = 9)
    private Double powerTotal;

    /** 当前总功率 */
    @ApiModelProperty(value = "当前总功率", position = 10)
    private Double currentPower;

    /** 总装机容量 */
    @ApiModelProperty(value = "总装机容量", position = 11)
    private Double installedCapacity;

    /** 系统功率比 */
    @ApiModelProperty(value = "系统功率比", position = 12)
    private Double powerRatio;

    /** 设备总数 */
    @ApiModelProperty(value = "设备总数", position = 13)
    private Integer equipTotal;

    /** 异常设备 */
    @ApiModelProperty(value = "异常设备", position = 14)
    private Integer equipAbnormal;

    /** 节约用煤（吨） */
    @ApiModelProperty(value = "节约用煤（吨）", position = 15)
    private Double saveCoal;

    /** 减少CO2排放（吨） */
    @ApiModelProperty(value = "减少CO2排放（吨）", position = 16)
    private Double reduceCo2;

    /** 减少SO2排放（吨） */
    @ApiModelProperty(value = "减少SO2排放（吨）", position = 17)
    private Double reduceSo2;

    /** 等效植树量（棵） */
    @ApiModelProperty(value = "等效植树量（棵）", position = 18)
    private Double equivalentPlanting;

}
