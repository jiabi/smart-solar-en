package vip.xiaonuo.biz.modular.eququality.param;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2023/10/5 15:57
 **/
@Setter
@Getter
@ContentRowHeight(18)
@HeadRowHeight(20)
@ColumnWidth(24)
public class ImportErrorReturnVO {
    /** 错误行号 */
    @ExcelProperty(value = "错误行号")
    private int rowNum;
    /** 错误内容 */
    @ExcelProperty(value = "错误内容")
    private String content;
}
