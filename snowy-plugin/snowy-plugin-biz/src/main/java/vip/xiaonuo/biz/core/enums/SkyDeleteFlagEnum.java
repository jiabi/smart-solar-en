package vip.xiaonuo.biz.core.enums;

import lombok.Getter;

/**
 * @Author wangjian
 * @Date 2024/3/22 11:10
 */
@Getter
public enum SkyDeleteFlagEnum {
    /**
     * NOT_DELETE
     */
    NOTDELETE("NOT_DELETE"),

    /**
     * DELETED
     */
    DELETED("DELETED");

    private final String value;

    SkyDeleteFlagEnum(String value) {
        this.value = value;
    }
}
