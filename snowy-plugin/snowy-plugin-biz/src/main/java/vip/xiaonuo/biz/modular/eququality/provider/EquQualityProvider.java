package vip.xiaonuo.biz.modular.eququality.provider;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.api.SkyEquQualityApi;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.modular.eququality.mapper.SkyEquQualityMapper;
import vip.xiaonuo.biz.param.EquQualityTripartiteVO;
import vip.xiaonuo.common.exception.CommonException;

import javax.annotation.Resource;
import java.util.List;

@Service
public class EquQualityProvider implements SkyEquQualityApi {

    @Resource
    private SkyEquQualityMapper skyEquQualityMapper;

    @Override
    public EquQualityTripartiteVO equQualityTripartite(String invSn){
        if(StringUtils.isEmpty(invSn)){
            throw new CommonException(SkyCommonExceptionEnum.MODEL_NAME_NULL.getMsg());
        }
        EquQualityTripartiteVO equQualityTripartiteVO = skyEquQualityMapper.selectEquQualityTripartite(invSn);
        return equQualityTripartiteVO;
    }
}
