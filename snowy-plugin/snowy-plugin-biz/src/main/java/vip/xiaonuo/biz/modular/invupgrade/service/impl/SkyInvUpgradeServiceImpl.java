/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invupgrade.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.collectorupgrade.param.SkyCollectorInvSnParam;
import vip.xiaonuo.biz.modular.collectorupgrade.param.SkyCollectorUpgradeIotParam;
import vip.xiaonuo.biz.modular.equbin.entity.SkyEquBin;
import vip.xiaonuo.biz.modular.equbin.mapper.SkyEquBinMapper;
import vip.xiaonuo.biz.modular.equcontrol.param.*;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.invupgrade.param.*;
import vip.xiaonuo.biz.modular.upgraderecord.entity.SkyUpgradeRecord;
import vip.xiaonuo.biz.modular.upgraderecord.mapper.SkyUpgradeRecordMapper;
import vip.xiaonuo.biz.modular.upgraderecord.param.SkyUpgradeRecordPageVO;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.invupgrade.entity.SkyInvUpgrade;
import vip.xiaonuo.biz.modular.invupgrade.mapper.SkyInvUpgradeMapper;
import vip.xiaonuo.biz.modular.invupgrade.service.SkyInvUpgradeService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 逆变器升级Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/04/17 19:49
 **/
@Service
@Slf4j
public class SkyInvUpgradeServiceImpl extends ServiceImpl<SkyInvUpgradeMapper, SkyInvUpgrade> implements SkyInvUpgradeService {

    @Resource
    private SkyInvUpgradeMapper skyInvUpgradeMapper;

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private IotProperties iotProperties;

    @Resource
    private SkyUpgradeRecordMapper skyUpgradeRecordMapper;

    @Resource
    private SkyInvInfoMapper skyInvInfoMapper;

    @Resource
    private SkyEquBinMapper skyEquBinMapper;

    @Override
    public Page<SkyInvUpgradePageVO> page(SkyInvUpgradePageParam skyInvUpgradePageParam) {
        Page<SkyInvUpgradePageVO> pageDataModel = new Page<>(skyInvUpgradePageParam.getCurrent(),skyInvUpgradePageParam.getSize());
        List<SkyInvUpgradePageVO> list = skyInvUpgradeMapper.page(skyInvUpgradePageParam);
        pageDataModel.setRecords(list);
        pageDataModel.setTotal(skyInvUpgradeMapper.countNum(skyInvUpgradePageParam));
        return pageDataModel;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyInvUpgradeAddParam skyInvUpgradeAddParam) {
        SkyInvUpgrade skyInvUpgrade = BeanUtil.toBean(skyInvUpgradeAddParam, SkyInvUpgrade.class);
        this.save(skyInvUpgrade);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyInvUpgradeEditParam skyInvUpgradeEditParam) {
        SkyInvUpgrade skyInvUpgrade = this.queryEntity(skyInvUpgradeEditParam.getId());
        BeanUtil.copyProperties(skyInvUpgradeEditParam, skyInvUpgrade);
        this.updateById(skyInvUpgrade);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyInvUpgradeIdParam> skyInvUpgradeIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyInvUpgradeIdParamList, SkyInvUpgradeIdParam::getId));
    }

    @Override
    public SkyInvUpgrade detail(SkyInvUpgradeIdParam skyInvUpgradeIdParam) {
        return this.queryEntity(skyInvUpgradeIdParam.getId());
    }

    @Override
    public SkyInvUpgrade queryEntity(String id) {
        SkyInvUpgrade skyInvUpgrade = this.getById(id);
        if(ObjectUtil.isEmpty(skyInvUpgrade)) {
            throw new CommonException("逆变器升级不存在，id值为：{}", id);
        }
        return skyInvUpgrade;
    }

    @Override
    public void upgrade(SkyInvUpgradeParam skyInvUpgradeParam) {
        List<SkyCollectorInvSnParam> equSnList = skyInvUpgradeParam.getEquSn();
        if (CollectionUtils.isEmpty(equSnList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.SN_CHECK_NOT_NULL_9107);
        }
        SkyInvUpgradeReqIotParam req = new SkyInvUpgradeReqIotParam();
        BeanUtil.copyProperties(skyInvUpgradeParam,req);
        req.setSendTime(LocalDateTime.now().format(LocalDateUtils.DATETIME_FORMATTER));
        req.setRequestId(String.valueOf(System.currentTimeMillis()));
        // 升级包大小
        SkyEquBin equBin = skyEquBinMapper.selectById(skyInvUpgradeParam.getEquBinId());
        if (equBin != null) {
            req.setSize(Optional.ofNullable(equBin.getBinSize()).orElse(0) / 8);
        } else {
            req.setSize(0);
        }

        List<SkyInvInfo> invInfos = skyInvInfoMapper.selectList(new QueryWrapper<SkyInvInfo>().lambda()
                .in(SkyInvInfo::getInvSn, equSnList.stream().map(SkyCollectorInvSnParam::getEquSn).collect(Collectors.toList())));
        Map<String, String> infoMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(invInfos)) {
            infoMap = invInfos.stream().collect(Collectors.toMap(SkyInvInfo::getInvSn, param -> param.getStationId() != null ? param.getStationId() : ""));
        } else {
            for (SkyCollectorInvSnParam param:equSnList) {
                infoMap.put(param.getEquSn(),"");
            }
        }

        // 调用iot接口
        for (SkyCollectorInvSnParam equSn:equSnList) {
            // 调用iot接口
            MultiValueMap<String,Object> requestEntity = new LinkedMultiValueMap<>();
            requestEntity.add("topic",iotProperties.getTopicPrefix() + equSn.getCollectorSn() + iotProperties.getUpgradeMethod());
            requestEntity.add("msg",req);
            log.info("调用iot读数据-请求参数：{}" , JSON.toJSONString(requestEntity));
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            requestFactory.setConnectTimeout(60000); // 设置连接超时时间为60秒
            requestFactory.setReadTimeout(60000); // 设置读取超时时间为60秒
            restTemplate.setRequestFactory(requestFactory);
            ResponseEntity<String> response = null;
            try {
                response = restTemplate.exchange(iotProperties.getEquControlUpgradeUrl(), HttpMethod.POST, new HttpEntity<>(requestEntity), String.class);
            } catch (RestClientException e) {
                log.info("调用iot参数设置-连接超时");
                throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
            }
            log.info("调用iot读数据-返回数据：{}" , response.getBody());
            SkyIotParam<SkyIotEquControlDevice<SkyIotEquControlResParameter>> res = JSON.parseObject(response.getBody(), new ParameterizedTypeReference<SkyIotParam<SkyIotEquControlDevice<SkyIotEquControlResParameter>>>() {}.getType());
            Integer status = 0;

            // 版本号更新
            if (ObjectUtil.isEmpty(skyInvUpgradeParam.getUpgradeModule())) {
                // 升级记录
                SkyUpgradeRecord record = new SkyUpgradeRecord();
                record.setCollectSn(equSn.getCollectorSn());
                record.setStationId(infoMap.get(equSn.getEquSn()));
                record.setEquSn(equSn.getEquSn());
                record.setUpgradeStarttime(new Date());
                record.setUpgradeModule(skyInvUpgradeParam.getUpgradeModuleType());
                String[] parts = skyInvUpgradeParam.getDownloadURL().split("/");
                record.setUpgradeBin(parts[parts.length - 1]);
                record.setUpgradeStatus(-5);
                skyUpgradeRecordMapper.insert(record);

                // 逆变器升级表记录
                invUpgradeRecord(skyInvUpgradeParam, equSn, -5);

                throw new SkyCommonException(SkyCommonExceptionEnum.UPGRADE_MODULE_NOT_NULL);
            }
            // 逆变器升级表记录
            invUpgradeRecord(skyInvUpgradeParam, equSn, 1);

            // 升级记录
            SkyUpgradeRecord record = new SkyUpgradeRecord();
            record.setCollectSn(equSn.getCollectorSn());
            record.setStationId(infoMap.get(equSn.getEquSn()));
            record.setEquSn(equSn.getEquSn());
            record.setUpgradeStarttime(new Date());
            record.setUpgradeModule(skyInvUpgradeParam.getUpgradeModuleType());
            String[] parts = skyInvUpgradeParam.getDownloadURL().split("/");
            record.setUpgradeBin(parts[parts.length - 1]);
            record.setUpgradeStatus(1);
            skyUpgradeRecordMapper.insert(record);

            try {
                Optional<Integer> optionalStatus = Optional.ofNullable(res)
                        .map(SkyIotParam::getDeviceList)
                        .filter(list -> !list.isEmpty())
                        .map(list -> list.get(0))
                        .map(SkyIotEquControlDevice::getParameter)
                        .filter(list -> !list.isEmpty())
                        .map(list -> list.get(0))
                        .map(SkyIotEquControlResParameter::getStatus);
                if (optionalStatus.isPresent()) {
                    status = optionalStatus.get();
                }
            } catch (SkyCommonException e) {
                log.info("逆变器升级返回对象映射错误:{}",e);
            }
            if (status < 0) {
                throw new SkyCommonException(SkyCommonExceptionEnum.UPGRADE_ERROR_9130);
            }
        }
    }

    private void invUpgradeRecord(SkyInvUpgradeParam skyInvUpgradeParam, SkyCollectorInvSnParam equSn, Integer upgradeStatus) {
        if ("wstk".equals(skyInvUpgradeParam.getUpgradeModule())) {
            skyInvUpgradeParam.setUpgradeModuleType(1);
            skyInvUpgradeMapper.update(null, new UpdateWrapper<SkyInvUpgrade>().lambda()
                    .set(SkyInvUpgrade::getUpgradeStatus,1)
                    .eq(SkyInvUpgrade::getEquSn, equSn.getEquSn()));
        }
        if ("marm".equals(skyInvUpgradeParam.getUpgradeModule())) {
            skyInvUpgradeParam.setUpgradeModuleType(2);
            skyInvUpgradeMapper.update(null, new UpdateWrapper<SkyInvUpgrade>().lambda()
                    .set(SkyInvUpgrade::getUpgradeStatus,1)
                    .eq(SkyInvUpgrade::getEquSn, equSn.getEquSn()));
        }
        if ("sarm".equals(skyInvUpgradeParam.getUpgradeModule())) {
            skyInvUpgradeParam.setUpgradeModuleType(3);
            skyInvUpgradeMapper.update(null, new UpdateWrapper<SkyInvUpgrade>().lambda()
                    .set(SkyInvUpgrade::getUpgradeStatus,1)
                    .eq(SkyInvUpgrade::getEquSn, equSn.getEquSn()));
        }
        if ("mdsp".equals(skyInvUpgradeParam.getUpgradeModule())) {
            skyInvUpgradeParam.setUpgradeModuleType(4);
            skyInvUpgradeMapper.update(null, new UpdateWrapper<SkyInvUpgrade>().lambda()
                    .set(SkyInvUpgrade::getUpgradeStatus,1)
                    .eq(SkyInvUpgrade::getEquSn, equSn.getEquSn()));
        }
        if ("sfty".equals(skyInvUpgradeParam.getUpgradeModule())) {
            skyInvUpgradeParam.setUpgradeModuleType(5);
            skyInvUpgradeMapper.update(null, new UpdateWrapper<SkyInvUpgrade>().lambda()
                    .set(SkyInvUpgrade::getUpgradeStatus,1)
                    .eq(SkyInvUpgrade::getEquSn, equSn.getEquSn()));
        }
        if ("gstk".equals(skyInvUpgradeParam.getUpgradeModule())) {
            skyInvUpgradeParam.setUpgradeModuleType(6);
            skyInvUpgradeMapper.update(null, new UpdateWrapper<SkyInvUpgrade>().lambda()
                    .set(SkyInvUpgrade::getUpgradeStatus,1)
                    .eq(SkyInvUpgrade::getEquSn, equSn.getEquSn()));
        }
    }

}
