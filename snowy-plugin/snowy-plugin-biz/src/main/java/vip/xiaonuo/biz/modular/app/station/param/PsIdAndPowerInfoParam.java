package vip.xiaonuo.biz.modular.app.station.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/27 11:16
 */

@Getter
@Setter
public class PsIdAndPowerInfoParam {
    private String stationId;

    private Double power;
}
