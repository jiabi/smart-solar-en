package vip.xiaonuo.biz.core.util.easyexcel;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.merge.LoopMergeStrategy;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import vip.xiaonuo.biz.core.util.easyexcel.annoation.LoopMerge;
import vip.xiaonuo.biz.core.util.easyexcel.converter.LocalDateConverter;
import vip.xiaonuo.biz.core.util.easyexcel.converter.LocalDateTimeConverter;
import vip.xiaonuo.biz.core.util.easyexcel.converter.LongConverter;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/2/29 18:41
 */
@Slf4j
public class EasyExcelMaker {
    private static final EasyExcelMaker easyExcelMaker = new EasyExcelMaker();


    public static EasyExcelMaker getInstance() {
        return easyExcelMaker;
    }

    /**
     * 生成Excel文件流并以Http形式导出
     *
     * @param <T>
     * @param response
     * @param data
     * @param clazz
     */
    public <T> void make(HttpServletResponse response, List<T> data, Class<T> clazz, String fileName) {
        this.make(response, data, clazz, fileName, fileName);
    }

    public <T> void make(HttpServletResponse response, List<T> data, Class<T> clazz, String fileName, String sheetName) {
        try (OutputStream out = response.getOutputStream()) {
            response.setHeader("Content-Disposition",
                    "attachment;filename=" + fileName + ".xls");
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            ExcelWriter excelWriter = EasyExcelFactory.write(out).excelType(ExcelTypeEnum.XLS).head(clazz).build();

            //sheet
            ExcelWriterSheetBuilder sheetBuilder = EasyExcelFactory.writerSheet(sheetName)
                    .sheetName(sheetName)
                    .registerConverter(new LocalDateTimeConverter())
                    .registerConverter(new LocalDateConverter())
                    .registerConverter(new LongConverter());
            registerWriteHandler(sheetBuilder);
            //合并单元格
            if (data.size() > 1) {
                mergeRow(sheetBuilder, clazz);
            }

            excelWriter.write(data, sheetBuilder.build());
            excelWriter.finish();
            out.flush();
        } catch (IOException e) {
            log.error("生成excel发生异常：{}", e);
        }
    }

    /**
     * 生成Excel文件流并以Http形式导出
     *
     * @param <T>
     * @param out
     * @param data
     * @param clazz
     */
    public <T> void make(OutputStream out, List<T> data, Class<T> clazz, String fileName) {
        try {
            ExcelWriter excelWriter = EasyExcelFactory.write(out).excelType(ExcelTypeEnum.XLS).head(clazz).build();
            //sheet
            ExcelWriterSheetBuilder sheetBuilder = EasyExcelFactory.writerSheet(fileName)
                    .sheetName(fileName)
                    .registerConverter(new LocalDateTimeConverter())
                    .registerConverter(new LocalDateConverter())
                    .registerConverter(new LongConverter());
            registerWriteHandler(sheetBuilder);
            //合并单元格
            if (data.size() > 1) {
                mergeRow(sheetBuilder, clazz);
            }
            excelWriter.write(data, sheetBuilder.build());
            excelWriter.finish();
            out.flush();
        } catch (IOException e) {
            log.error("生成excel发生异常：{}", e);
        }
    }

    /**
     * 生成Excel本地文件
     *
     * @param file
     * @param data
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> File makeFile(File file, List<T> data, Class<T> clazz) {
        try (OutputStream out = new FileOutputStream(file)) {
            ExcelWriter excelWriter = EasyExcelFactory.write(out).head(clazz).build();
            WriteSheet writeSheet = EasyExcelFactory.writerSheet().build();
            excelWriter.write(data, writeSheet);
            excelWriter.finish();
            out.flush();
            return file;
        } catch (IOException e) {
            log.error("生成excel发生异常：{}", e);
        }
        return null;
    }

    private void registerWriteHandler(ExcelWriterSheetBuilder sheetBuilder) {
        // 头部策略
        WriteCellStyle headWriteCellStyle = new WriteCellStyle();
        // 背景色
        headWriteCellStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
        WriteFont headWriteFont = new WriteFont();
        headWriteFont.setFontHeightInPoints((short) 12);
        headWriteCellStyle.setWriteFont(headWriteFont);
        //内容策略
        WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
        // 设置背景颜色白色
        contentWriteCellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        // 设置垂直居中为居中对齐
        contentWriteCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        // 设置左右对齐为靠左对齐
        contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.LEFT);
        // 设置单元格上下左右边框为细边框
        contentWriteCellStyle.setBorderBottom(BorderStyle.THIN);
        contentWriteCellStyle.setBorderLeft(BorderStyle.THIN);
        contentWriteCellStyle.setBorderRight(BorderStyle.THIN);
        contentWriteCellStyle.setBorderTop(BorderStyle.THIN);
        // 字体策略
        WriteFont contentWriteFont = new WriteFont();
        // 字体大小
        contentWriteFont.setFontHeightInPoints((short) 12);
        contentWriteCellStyle.setWriteFont(contentWriteFont);
        //设置 自动换行
        contentWriteCellStyle.setWrapped(true);
        HorizontalCellStyleStrategy horizontalCellStyleStrategy =
                new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);
        sheetBuilder.registerWriteHandler(horizontalCellStyleStrategy);
    }

    private <T> void mergeRow(ExcelWriterSheetBuilder sheetBuilder, Class<T> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        if (fields.length == 0) {
            return;
        }
        Arrays.stream(fields).forEach(field -> {
            LoopMerge loopMerge = field.getAnnotation(LoopMerge.class);
            ExcelProperty excelProperty = field.getAnnotation(ExcelProperty.class);
            if (loopMerge != null && excelProperty != null) {
                if (excelProperty.index() >= 0) {
                    LoopMergeStrategy loopMergeStrategy = new LoopMergeStrategy(loopMerge.eachRow(), excelProperty.index());
                    sheetBuilder.registerWriteHandler(loopMergeStrategy);
                }
            }
        });
    }

}
