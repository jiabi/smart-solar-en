/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invinfo.param;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 逆变器信息添加参数
 *
 * @author 全佳璧
 * @date  2023/10/05 16:24
 **/
@Getter
@Setter
public class SkyInvInfoAddParam {

    /** 逆变器SN */
    @ApiModelProperty(value = "逆变器SN", position = 1)
    @NotBlank(message = "invSn不能为空")
    private String invSn;

    /** 逆变器名称 */
    @ApiModelProperty(value = "逆变器名称", position = 2)
    private String invName;

    /** 图片路径 */
    @ApiModelProperty(value = "图片路径", position = 3)
    private String picUrl;

    /** 设备型号 */
    @ApiModelProperty(value = "设备型号", position = 4)
    private String modelId;

    /** 所属电站 */
    @ApiModelProperty(value = "所属电站", position = 5)
    private String stationId;

    /** 设备位置 */
    @ApiModelProperty(value = "设备位置", position = 6)
    private String invPosition;

    /** 安规标准 */
    @ApiModelProperty(value = "安规标准", position = 7)
    private String invRegulatory;

    /** 安规国家 */
    @ApiModelProperty(value = "安规国家", position = 8)
    private String regulatoryCountry;

}
