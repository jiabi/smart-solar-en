/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.equipmodel.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.equcontrol.param.*;
import vip.xiaonuo.biz.modular.equipmodel.entity.SkyEquipModel;
import vip.xiaonuo.biz.modular.equipmodel.param.*;
import vip.xiaonuo.biz.modular.eququality.param.SkyEquQualityExportParam;

import java.util.List;
import java.util.Map;

/**
 * 设备型号Service接口
 *
 * @author 全佳璧
 * @date  2023/09/25 14:15
 **/
public interface SkyEquipModelService extends IService<SkyEquipModel> {

    /**
     * 获取设备型号分页
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     */
    Page<SkyEquipModel> page(SkyEquipModelPageParam skyEquipModelPageParam);

    /**
     * 添加设备型号
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     */
    void add(SkyEquipModelAddParam skyEquipModelAddParam);

    /**
     * 编辑设备型号
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     */
    void edit(SkyEquipModelEditParam skyEquipModelEditParam);

    /**
     * 删除设备型号
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     */
    void delete(List<SkyEquipModelIdParam> skyEquipModelIdParamList);

    /**
     * 获取设备型号详情
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     */
    SkyEquipModel detail(SkyEquipModelIdParam skyEquipModelIdParam);

    /**
     * 获取设备型号详情
     *
     * @author 全佳璧
     * @date  2023/09/25 14:15
     **/
    SkyEquipModel queryEntity(String id);

    List<Map<String, Object>> exportInfoList(SkyEquipModelExportParam exportParam);

    List<SkyEquipModel> getEquipModelList(Integer equType);


    SkyIotParam<SkyIotReadResParam> collectorRed(SkyReqParam<SkyReadParamDeviceInfoParam> skyReqParam);

    SkyIotParam<SkyIotEquControlDevice> collectorControl(SkyReqParam<SkyIotEquControlDevice<SetSTKConfigParam>> skyReqParam);
}
