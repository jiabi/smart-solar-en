package vip.xiaonuo.biz.modular.stationinfo.param.equchart;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.core.util.easyexcel.converter.LocalDateTimeConverter;

import java.util.List;

/**
 * 电站图表信息-res
 *
 * @author lzy
 * @version 1.0
 */
@Setter
@Getter
@ContentRowHeight(18)
@HeadRowHeight(20)
@ColumnWidth(24)
public class PsEquChartAnalysisDayExcelResParam  {

    /**
     * 时间轴
     */
    @ExcelProperty(value = "时间", converter = LocalDateTimeConverter.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String time;

    /**
     * 明细
     */
    private List<PsEquDayChartResParam.DayChartAnalysis> list;


    @Setter
    @Getter
    public static  class DayChartAnalysis<T> {

        /**
         * 名称-（mppt电压1 （V））
         */
        private String name;

        /**
         * key="u_pv1"
         */
        private String key;

        /**
         * 数据点
         */
        private List<T>  dataList;

        public DayChartAnalysis(String name, String key, List<T> dataList) {
            this.name = name;
            this.key = key;
            this.dataList = dataList;
        }
    }
}
