package vip.xiaonuo.biz.modular.stationinfo.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/5/24 17:16
 */
@Getter
@Setter
public class PsIdDeviceCountParam {
    private String psId;
    private Integer num;
}
