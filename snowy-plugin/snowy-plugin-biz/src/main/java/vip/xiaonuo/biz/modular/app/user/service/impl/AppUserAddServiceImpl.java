package vip.xiaonuo.biz.modular.app.user.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.PhoneUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.auth.core.enums.SaClientTypeEnum;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.MailUtils;
import vip.xiaonuo.biz.modular.app.user.param.AppGetEmailValidCodeParam;
import vip.xiaonuo.biz.modular.app.user.param.AppGetPhoneValidCodeParam;
import vip.xiaonuo.biz.modular.app.user.param.AppUserAddParam;
import vip.xiaonuo.biz.modular.app.user.service.AppUserAddService;
import vip.xiaonuo.biz.modular.user.entity.BizSysRelation;
import vip.xiaonuo.biz.modular.user.entity.BizSysRole;
import vip.xiaonuo.biz.modular.user.entity.BizUser;
import vip.xiaonuo.biz.modular.user.enums.BizSysRelationCategoryEnum;
import vip.xiaonuo.biz.modular.user.enums.BizUserStatusEnum;
import vip.xiaonuo.biz.modular.user.mapper.BizSysRelationMapper;
import vip.xiaonuo.biz.modular.user.mapper.BizSysRoleMapper;
import vip.xiaonuo.biz.modular.user.mapper.BizUserMapper;
import vip.xiaonuo.biz.modular.user.param.BizUserAddParam;
import vip.xiaonuo.biz.modular.user.service.BizUserService;
import vip.xiaonuo.common.cache.CommonCacheOperator;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.util.CommonAvatarUtil;
import vip.xiaonuo.common.util.CommonCryptogramUtil;
import vip.xiaonuo.common.util.CommonEmailUtil;
import vip.xiaonuo.dev.api.DevEmailApi;
import vip.xiaonuo.dev.api.DevSmsApi;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/4/1 20:02
 */
@Service
public class AppUserAddServiceImpl extends ServiceImpl<BizUserMapper, BizUser> implements AppUserAddService {

    @Resource
    private CommonCacheOperator commonCacheOperator;

    @Resource
    private BizUserService bizUserService;

    @Resource
    private MailUtils mailUtils;

    @Resource
    private DevEmailApi devEmailApi;

    @Resource
    private DevSmsApi devSmsApi;

    @Resource
    private BizSysRoleMapper bizSysRoleMapper;

    @Resource
    private BizSysRelationMapper bizSysRelationMapper;

    private static final String USER_VALID_CODE_CACHE_KEY = "user-validCode:";

//    private static final String AUTH_VALID_CODE_CACHE_KEY = "auth-validCode:";

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void register(AppUserAddParam userAddParam) {
        // 执行校验验证码
        validValidCode(userAddParam.getPhoneOrEmail(),userAddParam.getValidCode(),userAddParam.getValidCodeReqNo());
        BizUser bizUser = new BizUser();
        bizUser.setAccount(userAddParam.getPhoneOrEmail());
        bizUser.setName(userAddParam.getPhoneOrEmail());
        bizUser.setPositionId("0");
        bizUser.setUserStatus(BizUserStatusEnum.ENABLE.getValue());
        bizUser.setPassword(CommonCryptogramUtil.doHashValue(CommonCryptogramUtil.doSm2Decrypt(userAddParam.getPassword())));
        // 设置默认头像
        bizUser.setAvatar(CommonAvatarUtil.generateImg(bizUser.getName()));
        if(PhoneUtil.isMobile(userAddParam.getPhoneOrEmail())) {
            bizUser.setPhone(userAddParam.getPhoneOrEmail());
        }
        if(CommonEmailUtil.isEmail(userAddParam.getPhoneOrEmail())) {
            bizUser.setEmail(userAddParam.getPhoneOrEmail());
        }
        if (!PhoneUtil.isMobile(userAddParam.getPhoneOrEmail()) && !CommonEmailUtil.isEmail(userAddParam.getPhoneOrEmail())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_PHONE_EMAIL_9110);
        }
        checkParam(bizUser);
        this.save(bizUser);
        //用户绑定角色
        addSysRelation(bizUser);
    }

    public void addSysRelation(BizUser bizUser){
        BizSysRole bizSysRole = bizSysRoleMapper.selectOne(Wrappers.lambdaQuery(BizSysRole.class).eq(BizSysRole::getCode, SkyRoleCodeEnum.BIZUSER));
        if (bizSysRole == null) {
            throw new SkyCommonException(SkyCommonExceptionEnum.USER_ROLE_NOT_EXIST_9131);
        }
        String userTypeId = bizSysRole.getId();
        BizSysRelation sysRelation = new BizSysRelation();
        sysRelation.setObjectId(bizUser.getId());
        sysRelation.setTargetId(userTypeId);
        sysRelation.setCategory(BizSysRelationCategoryEnum.SYS_USER_HAS_ROLE.getValue());
        bizSysRelationMapper.insert(sysRelation);
    }
    @Override
    public String appGetEmailValidCode(AppGetEmailValidCodeParam codeParam) {
        // 邮箱
        String email = codeParam.getEmail();
        // 验证码正确则校验邮箱格式
        if (!CommonEmailUtil.isEmail(email)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.EMAIL_ERROR_9115, codeParam.getEmail());
        }
        BizUser one = bizUserService.getOne(new QueryWrapper<BizUser>().lambda().eq(BizUser::getEmail, email));
        if (ObjectUtil.isNotEmpty(one)){
            throw new SkyCommonException(SkyCommonExceptionEnum.EMAIL_USER_EXIST_9118, email);
        }

        // 生成邮箱验证码的值，随机6为数字
        String emailValidCode = RandomUtil.randomNumbers(6);
        // 生成邮箱验证码的请求号
        String emailValidCodeReqNo = IdWorker.getIdStr();

        // 发送邮件
//        mailUtils.sendMail(codeParam.getEmail(),
//                "【SKYWORTH】注册验证 " +
//                        "尊敬的用户，您好！" +
//                        "完成注册，请使验证码：" +
//                        emailValidCode + "，验证邮箱并激活账户。 非本人操作可忽略。" +
//                        "SKYWORTH@skyworth.com" ,
//                "【SKYWORTH】注册验证");
        // TODO 使用本地发送邮件
        String content = "尊敬的用户，您好！完成注册，请使验证码：" + emailValidCode + "，验证邮箱并激活账户，5分钟内有效。 非本人操作可忽略。";
        devEmailApi.sendTextEmailLocal(email, "用户注册邮件", content, CollectionUtil.newArrayList());

        // 将请求号作为key，验证码的值作为value放到redis，用于校验，5分钟有效
        commonCacheOperator.put(USER_VALID_CODE_CACHE_KEY + email + StrUtil.UNDERLINE + emailValidCodeReqNo, emailValidCode, 5 * 60);
        // 返回请求号
        return emailValidCodeReqNo;
    }

    @Override
    public String appGetPhoneValidCode(AppGetPhoneValidCodeParam codeParam) {
        // 手机号
        String phone = codeParam.getPhone();
        // 验证码
        String validCode = codeParam.getValidCode();
        // 验证码请求号
        String validCodeReqNo = codeParam.getValidCodeReqNo();
        // 校验参数
//        validPhoneValidCodeParam(null, validCode, validCodeReqNo);
        // 根据手机号获取用户信息，判断用户是否存在
//        BizUser one = bizUserService.getOne(new QueryWrapper<BizUser>().lambda().eq(BizUser::getPhone, CommonCryptogramUtil.doSm4CbcEncrypt(phone)));
//        if (ObjectUtil.isNotEmpty(one)){
//            throw new SkyCommonException(SkyCommonExceptionEnum.PHONE_USER_EXIST_9122);
//        }

        // 生成手机验证码的值，随机6为数字
        String phoneValidCode = RandomUtil.randomNumbers(6);
        // 生成手机验证码的请求号
        String phoneValidCodeReqNo = IdWorker.getIdStr();


        // 使用阿里云执行发送验证码，将验证码作为短信内容的参数变量放入，
        // 签名不传则使用系统默认配置的签名，支持传入多个参数，示例：{"name":"张三","number":"15038****76"}
        devSmsApi.sendSmsAliyun(phone, null, "SMS_465530129", JSONUtil.toJsonStr(JSONUtil.createObj().set("code", phoneValidCode)));

        // TODO 使用腾讯云执行发送验证码，将验证码作为短信内容的参数变量放入，
        // TODO sdkAppId和签名不传则使用系统默认配置的sdkAppId和签名，支持传入多个参数，逗号拼接，示例："张三,15038****76,进行中"
//        devSmsApi.sendSmsTencent("sdkAppId", phone, "签名", "模板编码", phoneValidCode);

        // 将请求号作为key，验证码的值作为value放到redis，用于校验，5分钟有效
        commonCacheOperator.put(USER_VALID_CODE_CACHE_KEY + phone + StrUtil.UNDERLINE + phoneValidCodeReqNo, phoneValidCode, 5 * 60);
        // 返回请求号
        return phoneValidCodeReqNo;
    }

    /**
     * 校验验证码
     *
     * @author xuyuxiang
     * @date 2022/8/25 14:29
     **/
    private void validValidCode(String phoneOrEmail, String validCode, String validCodeReqNo) {
        // 依据请求号，取出缓存中的验证码进行校验
        Object existValidCode;
        if(ObjectUtil.isEmpty(phoneOrEmail)) {
            existValidCode = commonCacheOperator.get(USER_VALID_CODE_CACHE_KEY + validCodeReqNo);
        } else {
            existValidCode = commonCacheOperator.get(USER_VALID_CODE_CACHE_KEY + phoneOrEmail + StrUtil.UNDERLINE + validCodeReqNo);
        }
        // 为空则直接验证码错误
        if (ObjectUtil.isEmpty(existValidCode)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.VALID_CODE_ERROR_9111);
        }
        // 移除该验证码
        if(ObjectUtil.isEmpty(phoneOrEmail)) {
            commonCacheOperator.remove(USER_VALID_CODE_CACHE_KEY + validCodeReqNo);
        } else {
            commonCacheOperator.remove(USER_VALID_CODE_CACHE_KEY + phoneOrEmail + StrUtil.UNDERLINE + validCodeReqNo);
        }
        // 不一致则直接验证码错误
        if (!validCode.equals(Convert.toStr(existValidCode).toLowerCase())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.VALID_CODE_ERROR_9111);
        }
    }

    /**
     * 校验手机号与验证码等参数
     *
     * @author xuyuxiang
     * @date 2022/8/25 14:29
     **/
    private void validPhoneValidCodeParam(String phoneOrEmail, String validCode, String validCodeReqNo) {
        // 验证码正确则校验手机号格式
        if(ObjectUtil.isEmpty(phoneOrEmail)) {
            // 执行校验验证码
            validValidCode(null, validCode, validCodeReqNo);
        } else {
            if(!PhoneUtil.isMobile(phoneOrEmail) && !CommonEmailUtil.isEmail(phoneOrEmail)) {
                throw new SkyCommonException(SkyCommonExceptionEnum.PHONE_FORMAT_ERROR_9121);
            }
            // 执行校验验证码
            validValidCode(phoneOrEmail, validCode, validCodeReqNo);

            // 根据手机号获取用户信息，判断用户是否存在
            BizUser one = bizUserService.getOne(new QueryWrapper<BizUser>().lambda().eq(BizUser::getPhone, CommonCryptogramUtil.doSm4CbcEncrypt(phoneOrEmail)));
            if (ObjectUtil.isNotEmpty(one)){
                throw new SkyCommonException(SkyCommonExceptionEnum.PHONE_USER_EXIST_9122);
            }
        }
    }

    private void checkParam(BizUser bizUserAddParam) {
        // 校验数据范围
//        List<String> loginUserDataScope = StpLoginUserUtil.getLoginUserDataScope();
//        if(ObjectUtil.isNotEmpty(loginUserDataScope)) {
//            if(!loginUserDataScope.contains(bizUserAddParam.getOrgId())) {
//                throw new CommonException("您没有权限在该机构下增加人员，机构id：{}", bizUserAddParam.getOrgId());
//            }
//        } else {
//            throw new CommonException("您没有权限在该机构下增加人员，机构id：{}", bizUserAddParam.getOrgId());
//        }
        if (this.count(new LambdaQueryWrapper<BizUser>()
                .eq(BizUser::getAccount, bizUserAddParam.getAccount())) > 0) {
            throw new SkyCommonException(SkyCommonExceptionEnum.EXIST_ACCOUNT_9112, bizUserAddParam.getAccount());
        }
        if(ObjectUtil.isNotEmpty(bizUserAddParam.getPhone())) {
            if(!PhoneUtil.isMobile(bizUserAddParam.getPhone())) {
                throw new SkyCommonException(SkyCommonExceptionEnum.PHONE_ERROR_9113, bizUserAddParam.getPhone());
            }
            if (this.count(new LambdaQueryWrapper<BizUser>()
                    .eq(BizUser::getPhone, CommonCryptogramUtil.doSm4CbcEncrypt(bizUserAddParam.getPhone()))) > 0) {
                throw new SkyCommonException(SkyCommonExceptionEnum.PHONE_EXIST_9114, bizUserAddParam.getPhone());
            }
        }
        if(ObjectUtil.isNotEmpty(bizUserAddParam.getEmail())) {
            if(!CommonEmailUtil.isEmail(bizUserAddParam.getEmail())) {
                throw new SkyCommonException(SkyCommonExceptionEnum.EMAIL_ERROR_9115, bizUserAddParam.getEmail());
            }
            if (this.count(new LambdaQueryWrapper<BizUser>()
                    .eq(BizUser::getEmail, bizUserAddParam.getEmail())) > 0) {
                throw new SkyCommonException(SkyCommonExceptionEnum.EMAIL_EXIST_9116, bizUserAddParam.getEmail());
            }
        }
    }
}
