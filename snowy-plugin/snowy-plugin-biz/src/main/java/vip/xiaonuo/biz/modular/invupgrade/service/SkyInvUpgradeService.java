/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invupgrade.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.invupgrade.entity.SkyInvUpgrade;
import vip.xiaonuo.biz.modular.invupgrade.param.*;

import java.util.List;

/**
 * 逆变器升级Service接口
 *
 * @author 全佳璧
 * @date  2024/04/17 19:49
 **/
public interface SkyInvUpgradeService extends IService<SkyInvUpgrade> {

    /**
     * 获取逆变器升级分页
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     */
    Page<SkyInvUpgradePageVO> page(SkyInvUpgradePageParam skyInvUpgradePageParam);

    /**
     * 添加逆变器升级
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     */
    void add(SkyInvUpgradeAddParam skyInvUpgradeAddParam);

    /**
     * 编辑逆变器升级
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     */
    void edit(SkyInvUpgradeEditParam skyInvUpgradeEditParam);

    /**
     * 删除逆变器升级
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     */
    void delete(List<SkyInvUpgradeIdParam> skyInvUpgradeIdParamList);

    /**
     * 获取逆变器升级详情
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     */
    SkyInvUpgrade detail(SkyInvUpgradeIdParam skyInvUpgradeIdParam);

    /**
     * 获取逆变器升级详情
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     **/
    SkyInvUpgrade queryEntity(String id);

    void upgrade(SkyInvUpgradeParam skyInvUpgradeParam);
}
