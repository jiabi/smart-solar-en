package vip.xiaonuo.biz.modular.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import vip.xiaonuo.biz.modular.user.entity.BizSysRelation;
import vip.xiaonuo.biz.modular.user.entity.BizUser;
import vip.xiaonuo.biz.modular.user.mapper.BizSysRelationMapper;
import vip.xiaonuo.biz.modular.user.mapper.BizUserMapper;
import vip.xiaonuo.biz.modular.user.service.BizSysRelationService;
import vip.xiaonuo.biz.modular.user.service.BizUserService;

@Service
public class BizSysRelationServiceImpl extends ServiceImpl<BizSysRelationMapper, BizSysRelation> implements BizSysRelationService {
}
