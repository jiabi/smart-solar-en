package vip.xiaonuo.biz.modular.stationinfo.param.equchart;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/6 15:37
 */

@Getter
@Setter
public class PsEquChartReqParam {
    /**
     * 设备code（逆变器）
     */
    private String deviceSn;

    /**
     * 日 yyyy-MM-dd
     * 月 yyyy-MM
     * 年 yyyy
     * 总 不用传
     */
    private String date;

    /**
     *1日 2月 3年 4总
     */
    @NotNull(message = "类型不能为空")
    @Min(value = 1, message = "类型不能小于1")
    @Max(value = 4, message = "类型不能大于4")
    private Integer type;

    /**
     * 日图表参数集合
     */
    private List<String> paramsList;

    /**
     * 字段名和属性值
     */
    private List<PsEquChartNameAndCommentReqParam> checkValue;
}
