/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.collectorinfo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 采集器信息实体
 *
 * @author 全佳璧
 * @date  2023/10/23 09:46
 **/
@Getter
@Setter
@TableName("sky_collector_info")
public class SkyCollectorInfo {

    /** 采集器SN */
    @TableId
    @ApiModelProperty(value = "采集器SN", position = 1)
    private String collectorSn;

    /** 采集器名称 */
    @ApiModelProperty(value = "采集器名称", position = 2)
    private String collectorType;

    /** 设备型号 */
    @ApiModelProperty(value = "设备型号", position = 3)
    private String modelId;

    /** 所属电站 */
    @ApiModelProperty(value = "所属电站", position = 4)
    private String stationId;

    /** gprs卡号 */
    @ApiModelProperty(value = "gprs卡号", position = 5)
    private String gprsCard;

    /** 运营商 */
    @ApiModelProperty(value = "运营商", position = 6)
    private String cardOperator;

    /** 连接设备数量 */
    @ApiModelProperty(value = "连接设备数量", position = 7)
    private Integer equCount;

    /** 投运时间 */
    @ApiModelProperty(value = "投运时间", position = 7)
    private Integer runTime;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 8)
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 9)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 10)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 11)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 12)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 13)
    private String tenantId;
}
