package vip.xiaonuo.biz.modular.app.station.param.chart;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/12 13:29
 */
@Setter
@Getter
public class PsPowerDayChartResParam {
    private String dateTime;

    private Double power;
}
