package vip.xiaonuo.biz.modular.eququality.param;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2023/9/27 16:57
 **/
@Setter
@Getter
@ContentRowHeight(18)
@HeadRowHeight(20)
@ColumnWidth(24)
public class SkyEquQualityExportVO {

    /** 设备SN */
    @ApiModelProperty(value = "设备SN")
    @ExcelProperty(value = "设备SN（示例数据请勿删除！必填！）")
    private String equSn;

    /** 设备类型 */
    @ApiModelProperty(value = "设备类型")
    @ExcelProperty(value = "设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪(导入请填对应数字！必填！)")
    private Integer equType;

    /** 设备型号 */
    @ApiModelProperty(value = "设备型号")
    @ExcelProperty(value = "设备型号（必填）")
    private String equModel;

    /** 发货日期开始 */
    @ExcelProperty(value = "发货日期（日期格式，如：2023-10-01（必填！））")
    @ApiModelProperty(value = "发货日期开始")
    private String startDate;

    /** 质保期限结束 */
//    @ExcelProperty(value = "质保期限")
//    @ApiModelProperty(value = "质保期限结束")
//    private String endDate;

    /** 质保时长 */
    @ExcelProperty(value = "质保时长（单位：年；最多一位小数（必填！））")
    @ApiModelProperty(value = "质保时长")
    private Double timeLimit;

    /** 临期状态 */
//    @ExcelProperty(value = "临期状态 0：临期；1：正常（可不填）")
//    @ApiModelProperty(value = "临期状态")
//    private Integer criticalStatus;
}
