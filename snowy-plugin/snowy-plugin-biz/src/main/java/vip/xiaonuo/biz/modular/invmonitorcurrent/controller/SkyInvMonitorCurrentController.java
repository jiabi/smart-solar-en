/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invmonitorcurrent.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInverterPower;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.*;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;
import vip.xiaonuo.biz.modular.invmonitorcurrent.service.SkyInvMonitorCurrentService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 逆变器实时数据控制器
 *
 * @author 全佳璧
 * @date  2023/10/12 15:36
 */
@Api(tags = "逆变器实时数据控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyInvMonitorCurrentController {

    @Resource
    private SkyInvMonitorCurrentService skyInvMonitorCurrentService;

    /**
     * 获取逆变器实时数据分页
     *
     * @author 全佳璧
     * @date  2023/10/12 15:36
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取逆变器实时数据分页")
    @SaCheckPermission("/biz/invmonitorcurrent/page")
    @GetMapping("/biz/invmonitorcurrent/page")
    public CommonResult<Page<SkyInvMonitorCurrent>> page(SkyInvMonitorCurrentPageParam skyInvMonitorCurrentPageParam) {
        return CommonResult.data(skyInvMonitorCurrentService.page(skyInvMonitorCurrentPageParam));
    }

    /**
     * 添加逆变器实时数据
     *
     * @author 全佳璧
     * @date  2023/10/12 15:36
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加逆变器实时数据")
    @CommonLog("添加逆变器实时数据")
    @SaCheckPermission("/biz/invmonitorcurrent/add")
    @PostMapping("/biz/invmonitorcurrent/add")
    public CommonResult<String> add(@RequestBody @Valid SkyInvMonitorCurrentAddParam skyInvMonitorCurrentAddParam) {
        skyInvMonitorCurrentService.add(skyInvMonitorCurrentAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑逆变器实时数据
     *
     * @author 全佳璧
     * @date  2023/10/12 15:36
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑逆变器实时数据")
    @CommonLog("编辑逆变器实时数据")
    @SaCheckPermission("/biz/invmonitorcurrent/edit")
    @PostMapping("/biz/invmonitorcurrent/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyInvMonitorCurrentEditParam skyInvMonitorCurrentEditParam) {
        skyInvMonitorCurrentService.edit(skyInvMonitorCurrentEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除逆变器实时数据
     *
     * @author 全佳璧
     * @date  2023/10/12 15:36
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除逆变器实时数据")
    @CommonLog("删除逆变器实时数据")
    @SaCheckPermission("/biz/invmonitorcurrent/delete")
    @PostMapping("/biz/invmonitorcurrent/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyInvMonitorCurrentIdParam> skyInvMonitorCurrentIdParamList) {
        skyInvMonitorCurrentService.delete(skyInvMonitorCurrentIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取逆变器实时数据详情
     *
     * @author 全佳璧
     * @date  2023/10/12 15:36
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取逆变器实时数据详情")
    @SaCheckPermission("/biz/invmonitorcurrent/detail")
    @GetMapping("/biz/invmonitorcurrent/detail")
    public CommonResult<SkyInvMonitorCurrent> detail(@Valid SkyInvMonitorCurrentIdParam skyInvMonitorCurrentIdParam) {
        return CommonResult.data(skyInvMonitorCurrentService.detail(skyInvMonitorCurrentIdParam));
    }

    /**
     * 通过设备SN获取逆变器实时数据详情
     *
     * @author 全佳璧
     * @date  2023/10/12 15:36
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("通过设备SN获取逆变器实时数据详情")
//    @SaCheckPermission("/biz/invmonitorcurrent/detailByInvSn")
    @GetMapping("/biz/invmonitorcurrent/detailByInvSn")
    public CommonResult<SkyInvMonitorCurrentDetailVO> detailByInvSn(@Valid SkyInvMonitorCurrentSnParam skyInvMonitorCurrentSnParam) {
        return CommonResult.data(skyInvMonitorCurrentService.detailByInvSn(skyInvMonitorCurrentSnParam));
    }

    /**
     * 获取逆变器历史发电数据
     *
     * @author wangjian
     * @date  2023/10/19 15:36
     */
    @ApiOperationSupport(order = 7)
    @ApiOperation("获取逆变器历史发电数据")
//    @SaCheckPermission("/biz/invmonitorcurrent/historyDetailByInvSn")
    @GetMapping("/biz/invmonitorcurrent/historyDetailByInvSn")
    public CommonResult<List<SkyInvMonitorHistoryVO>> historyDetailByInvSn(@Valid SkyInvMonitorHistoryParam skyInvMonitorHistoryParam) throws ParseException {
        return CommonResult.data(skyInvMonitorCurrentService.historyDetailByInvSn(skyInvMonitorHistoryParam));
    }

    /**
     * 获取逆变器历史发电数据
     *
     * @author wangjian
     * @date  2023/10/19 15:36
     */
    @ApiOperationSupport(order = 7)
    @ApiOperation("获取逆变器历史发电数据")
//    @SaCheckPermission("/biz/invmonitorcurrent/power")
    @GetMapping("/biz/invmonitorcurrent/power")
    public CommonResult<List<SkyInverterPower>> power(@Valid SkyInvInfoPowerParam skyInvInfoPowerParam) {
        return CommonResult.data(skyInvMonitorCurrentService.power(skyInvInfoPowerParam));
    }

}
