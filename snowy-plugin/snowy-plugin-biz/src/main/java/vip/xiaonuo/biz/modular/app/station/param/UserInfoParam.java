package vip.xiaonuo.biz.modular.app.station.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/4/8 17:25
 */

@Getter
@Setter
public class UserInfoParam {
    private String name;

    private String id;
}
