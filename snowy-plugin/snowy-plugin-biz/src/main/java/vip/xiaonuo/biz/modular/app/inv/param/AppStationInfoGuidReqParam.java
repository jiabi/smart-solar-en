package vip.xiaonuo.biz.modular.app.inv.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/20 11:16
 */
@Getter
@Setter
public class AppStationInfoGuidReqParam {
    private String guid;
}
