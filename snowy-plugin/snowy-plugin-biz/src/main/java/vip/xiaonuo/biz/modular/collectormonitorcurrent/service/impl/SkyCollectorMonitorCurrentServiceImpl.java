package vip.xiaonuo.biz.modular.collectormonitorcurrent.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.xiaonuo.biz.modular.collectormonitorcurrent.entity.SkyCollectorMonitorCurrent;
import vip.xiaonuo.biz.modular.collectormonitorcurrent.service.SkyCollectorMonitorCurrentService;
import vip.xiaonuo.biz.modular.collectormonitorcurrent.mapper.SkyCollectorMonitorCurrentMapper;
import org.springframework.stereotype.Service;

/**
* @author wangjian
* @description 针对表【sky_collector_monitor_current(采集器实时数据)】的数据库操作Service实现
* @createDate 2023-10-25 14:30:23
*/
@Service
public class SkyCollectorMonitorCurrentServiceImpl extends ServiceImpl<SkyCollectorMonitorCurrentMapper, SkyCollectorMonitorCurrent>
    implements SkyCollectorMonitorCurrentService {

}




