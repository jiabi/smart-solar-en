package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2023/9/28 16:52
 **/
@Getter
@Setter
public class StationContactInfo {
    /** 业主id */
    @ApiModelProperty(value = "业主id", position = 1)
    private String userId;
    /** 业主姓名 */
    @ApiModelProperty(value = "业主姓名", position = 2)
    private String userName;

    /** 业主电话 */
    @ApiModelProperty(value = "业主电话", position = 3)
    private String userTel;

    /** 业主工作单位 */
    @ApiModelProperty(value = "业主工作单位", position = 4)
    private String userAddr;
}
