/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.eququality.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 设备质保信息查询参数
 *
 * @author 全佳璧
 * @date  2023/09/25 14:20
 **/
@Getter
@Setter
public class SkyEquQualityPageParam {

    /** 当前页 */
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    /** 每页条数 */
    @ApiModelProperty(value = "每页条数")
    private Integer size;

    /** 设备SN */
    @ApiModelProperty(value = "设备SN")
    private String equSn;

    /** 设备类型 */
    @ApiModelProperty(value = "设备类型")
    private Integer equType;

    /** 设备型号 */
    @ApiModelProperty(value = "设备型号")
    private String equModel;

    /** 发货日期开始 */
    @ApiModelProperty(value = "发货日期开始")
    private String startDate;

    /** 质保期限结束 */
    @ApiModelProperty(value = "质保期限结束")
    private String endDate;

    /** 质保时长 */
    @ApiModelProperty(value = "质保时长")
    private Double timeLimit;

    /** 临期状态 */
    @ApiModelProperty(value = "临期状态")
    private Integer criticalStatus;


    /** 安装公司id */
    @ApiModelProperty(value = "安装公司id")
    private String stationCompany;

    /** 业主id */
    @ApiModelProperty(value = "业主id")
    private String stationContactId;

    /** 分销商 */
    @ApiModelProperty(value = "分销商")
    private String distributor;

    public int getLimitStart() {
        return (getCurrent() - 1) * getSize();
    }

}
