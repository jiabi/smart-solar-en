package vip.xiaonuo.biz.modular.dict.param;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/6/3 15:43
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BizDictSafetyStandardParam {
    private String dictLabel;

    private String dictValue;
}
