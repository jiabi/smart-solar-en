package vip.xiaonuo.biz.modular.app.alarm.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import vip.xiaonuo.biz.modular.alarminfo.param.SkyAlarmInfoIdParam;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoCurrentIdParam;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageParam;
import vip.xiaonuo.biz.modular.app.alarm.param.AppSkyAlarmHisInfoRes;
import vip.xiaonuo.biz.modular.app.alarm.param.AppSkyAlarmInfoPageVO;
import vip.xiaonuo.biz.modular.app.alarm.param.AppSkyAlarmCurrentInfoRes;

/**
 * @Author wangjian
 * @Date 2024/3/15 15:23
 */
public interface AppSkyAlarmInfoService {
    Page<AppSkyAlarmInfoPageVO> page(SkyAlarmInfoPageParam skyAlarmInfoPageParam);

    Page<AppSkyAlarmInfoPageVO> currentPage(SkyAlarmInfoPageParam skyAlarmInfoCurrentPageParam);

    AppSkyAlarmCurrentInfoRes detail(SkyAlarmInfoCurrentIdParam skyAlarmInfoIdParam);

    AppSkyAlarmHisInfoRes getHistoryAlarmInfo(SkyAlarmInfoIdParam skyAlarmInfoIdParam);
}
