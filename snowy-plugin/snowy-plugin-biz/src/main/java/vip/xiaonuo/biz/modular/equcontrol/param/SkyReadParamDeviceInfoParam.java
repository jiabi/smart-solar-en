package vip.xiaonuo.biz.modular.equcontrol.param;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/5/6 14:35
 */
@Getter
@Setter
public class SkyReadParamDeviceInfoParam {
    private String id;

    /** 1：户用并网逆变；2：户储逆变器；3：电表；4：电池；5：采集器 **/
    private String type;

    private String sn;

    private List<String> paramList;
}
