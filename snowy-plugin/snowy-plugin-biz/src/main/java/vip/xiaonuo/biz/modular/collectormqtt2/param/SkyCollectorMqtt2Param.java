package vip.xiaonuo.biz.modular.collectormqtt2.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SkyCollectorMqtt2Param {
    @ApiModelProperty(value = "MQTT2 ID", required = true)
    private String id;

    /** 发送公司名称 */
    private String mqttFactory;

    /** MQTT第二路主域 */
    private String mqttDomain;

    /** MQTT第二路用户名 */
    private String mqttUsername;

    /** MQTT第二路密码 */
    private String mqttPassword;

    /** MQTT第二路服务器URL */
    private String mqttUrl;

}
