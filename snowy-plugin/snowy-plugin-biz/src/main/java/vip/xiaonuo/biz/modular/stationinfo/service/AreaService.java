package vip.xiaonuo.biz.modular.stationinfo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.mapstruct.Mapper;
import vip.xiaonuo.biz.modular.stationinfo.entity.Area;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;


public interface AreaService extends IService<Area> {
}
