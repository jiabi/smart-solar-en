package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @Description:
 * @Author: wangjian
 * @CreateTime: 2023-11-15  15:01
 */
@Getter
@Setter
public class StationInfoOverviewPowerHisParam {

    /** 电站ID */
    @ApiModelProperty(value = "电站ID", required = true)
    @NotBlank(message = "id不能为空")
    private String id;

    /** 时间类型 */
    @ApiModelProperty(value = "时间类型：1.日发电量；2.月发电量；3.年发电量；4.总发电量；5.今日并网电量；6.累计并网电量；7.今日购电量；8.累计购电量")
    private Integer timeType;

    /** 日期 */
    @ApiModelProperty(value = "年、年-月、年-月-日")
    private String dateTime;
}