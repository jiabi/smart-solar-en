package vip.xiaonuo.biz.modular.stationinfo.param.pschart;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/11 10:14
 */
@Setter
@Getter
@ContentRowHeight(18)
@HeadRowHeight(20)
@ColumnWidth(24)
public class PsChartDayInPowerParam {

    /**
     * 运行时间
     */
    @ExcelProperty(value = "时间")
    private String monitorTime;

    /**
     * 直流发电功率
     */
    @ExcelProperty(value = "直流发电功率(kw)")
    private Double inPower;
}
