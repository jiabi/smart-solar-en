package vip.xiaonuo.biz.modular.filemanager.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/7/11 15:17
 */
@Getter
@Setter
public class FileUploadResVO {
    private String downloadUrl;

    private Integer fileSize;
}
