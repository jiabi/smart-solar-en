package vip.xiaonuo.biz.modular.stationinfo.param;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.modular.stationinfo.entity.Area;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2023/9/28 15:28
 **/
@Getter
@Setter
public class StationInfoPageModel {
    /** 电站ID */
    @ApiModelProperty(value = "电站ID", position = 1)
    private String id;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称", position = 2)
    private String stationName;

    /** 电站备注 */
    @ApiModelProperty(value = "电站备注", position = 3)
    private String stationMemo;

    /** 建站日期 */
    @ApiModelProperty(value = "建站日期", position = 4)
    private Date stationCreatetime;

    /** 区域信息 */
    @ApiModelProperty(value = "区域信息", position = 5)
    private Area area;

    /** 装机容量 */
    @ApiModelProperty(value = "装机容量", position = 6)
    private Double stationSize;

    /** 电站类型 */
    @ApiModelProperty(value = "电站类型", position = 7)
    private Integer stationType;

    /** 电站系统 */
    @ApiModelProperty(value = "电站系统", position = 8)
    private Integer stationSystem;

    /** 并网状态 */
    @ApiModelProperty(value = "并网状态", position = 9)
    private Integer stationGridstatus;

    /** 并网时间 */
    @ApiModelProperty(value = "并网时间", position = 10)
    private Date stationGridtime;

    /** 电站状态 */
    @ApiModelProperty(value = "电站状态", position = 11)
    private Integer stationStatus;

    /** 报警状态 */
    @ApiModelProperty(value = "报警状态", position = 12)
    private Integer stationAlarm;

    /** 业主信息 */
    @ApiModelProperty(value = "业主信息", position = 13)
    private StationContactInfo stationContact;

    /** 售电单价 */
    @ApiModelProperty(value = "售电单价", position = 14)
    private Double pricePer;

    /** 补贴单价 */
    @ApiModelProperty(value = "补贴单价", position = 15)
    private Double subsidyPer;

    /** 货币种类 */
    @ApiModelProperty(value = "货币种类", position = 16)
    private String priceType;

    /** 发电收益 */
    @ApiModelProperty(value = "发电收益", position = 17)
    private Double elecIncome;

    /** 补贴收益 */
    @ApiModelProperty(value = "补贴收益", position = 18)
    private Double subsidyIncome;

    /** 累计收益 */
    @ApiModelProperty(value = "累计收益", position = 19)
    private Double totalIncome;

    /** 是否公开 */
    @ApiModelProperty(value = "是否公开", position = 20)
    private Integer stationPublic;

    /** 累计发电量 */
    @ApiModelProperty(value = "累计发电量", position = 21)
    private Double stationTotalpower;

    /** 累计运行天数 */
    @ApiModelProperty(value = "累计运行天数", position = 22)
    private Integer stationRuntime;

    /** 设备总数 */
    @ApiModelProperty(value = "设备总数", position = 23)
    private Integer equipTotal;

    /** 节约用煤（吨） */
    @ApiModelProperty(value = "节约用煤（吨）", position = 24)
    private Double saveCoal;

    /** 减少CO2排放（吨） */
    @ApiModelProperty(value = "减少CO2排放（吨）", position = 25)
    private Double reduceCo2;

    /** 减少SO2排放（吨） */
    @ApiModelProperty(value = "减少SO2排放（吨）", position = 26)
    private Double reduceSo2;

    /** 等效植树量（棵） */
    @ApiModelProperty(value = "等效植树量（棵）", position = 27)
    private Double equivalentPlanting;

    /** 安装公司 */
    @ApiModelProperty(value = "安装公司", position = 28)
    private String stationCompany;

    /** 时区 */
    @ApiModelProperty(value = "时区", position = 32)
    private String timeZone;

    /** 总成本 */
    @ApiModelProperty(value = "总成本", position = 35)
    private String totalCost;

    /** 日还款 */
    @ApiModelProperty(value = "日还款", position = 36)
    private Double dayRepayment;

    /** 图片url */
    @ApiModelProperty(value = "图片url", position = 44)
    private String picUrl;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 45)
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 46)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 47)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 48)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 49)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 50)
    private String tenantId;

    /** 发电功率 */
    @ApiModelProperty(value = "发电功率", position = 51)
    private String elecEfficiency;

    /** 当日发满小时 */
    @ApiModelProperty(value = "当日发满小时", position = 52)
    private String dayPowerhours;

    /** 系统功率比 */
    @ApiModelProperty(value = "系统功率比", position = 53)
    private String stationSysRate;

    /** 安装公司名称 */
    @ApiModelProperty(value = "安装公司名称", position = 54)
    private String stationCompanyName;
}
