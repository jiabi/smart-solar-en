package vip.xiaonuo.biz.modular.equipmodel.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author: wangjian
 * @date: 2023/10/11
 * @Modified By:
 */
@Getter
@Setter
public class SkyEquipModelExportParam {
    /** id集合 */
    @ApiModelProperty(value = "id集合")
    private List<String> ids;

    /** 设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪 */
    @ApiModelProperty(value = "设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪")
    private Integer equType;

    /** 型号名称 */
    @ApiModelProperty(value = "型号名称")
    private String modelName;

    /** 厂家ID */
    @ApiModelProperty(value = "厂家ID")
    private String companyId;

    /** 设备详细类型：0.单项；1.三项；2.单向表；4.双向表；5.棒式；6.导轨 */
    @ApiModelProperty(value = "设备详细类型：0.单项；1.三项；2.单向表；4.双向表；5.棒式；6.导轨")
    private Integer detailType;
}
