/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.core.exception;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.common.exception.CommonException;

/**
 * 通用异常
 *
 * @author xuyuxiang
 * @date 2020/4/8 15:54
 */
@Getter
@Setter
public class SkyCommonException extends CommonException {

    private Integer code;

    private String msg;

    public SkyCommonException() {
        super("服务器异常");
        this.code = 500;
        this.msg = "服务器异常";
    }

    public SkyCommonException(String msg, Object... arguments) {
        super(StrUtil.format(msg, arguments));
        this.code = 500;
        this.msg = StrUtil.format(msg, arguments);
    }

    public SkyCommonException(Integer code, String msg) {
        super(StrUtil.format(msg));
        this.code = code;
        this.msg = StrUtil.format(msg);
    }

    public SkyCommonException(SkyCommonExceptionEnum skyCommonExceptionEnum) {
        this(skyCommonExceptionEnum.getCode(),skyCommonExceptionEnum.getMsg());
    }
    public SkyCommonException(SkyCommonExceptionEnum skyCommonExceptionEnum,Object... arguments ) {
        this(skyCommonExceptionEnum.getCode(),StrUtil.format(skyCommonExceptionEnum.getMsg(), arguments));
    }

    public SkyCommonException(Integer code, String msg, Object... arguments) {
        super(StrUtil.format(msg, arguments));
        this.code = code;
        this.msg = StrUtil.format(msg, arguments);
    }
}
