package vip.xiaonuo.biz.modular.stationinfo.param;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author: wangjian
 * @date: 2023/10/11
 * @Modified By:
 */
@Getter
@Setter
public class StationInfoExportVO {
    /** 电站ID */
    @ApiModelProperty(value = "电站ID", position = 1)
    @ExcelProperty(value = "电站ID")
    private String id;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称", position = 2)
    @ExcelProperty(value = "电站名称")
    private String stationName;

    /** 电站状态 */
    @ApiModelProperty(value = "电站状态", position = 3)
    @ExcelProperty(value = "电站状态")
    private String stationStatusChine;

    /** 报警状态 */
    @ApiModelProperty(value = "报警状态", position = 4)
    @ExcelProperty(value = "报警状态")
    private String stationAlarmChine;

    /** 装机容量 默认小数点后两位*/
    @ApiModelProperty(value = "装机容量", position = 5)
    @ExcelProperty(value = "装机容量")
    private Double stationSize;

    /** 发电功率 */
    @ApiModelProperty(value = "发电功率", position = 6)
    @ExcelProperty(value = "发电功率")
    private Double elecEfficiency;

    /** 系统功率比 */
    @ApiModelProperty(value = "系统功率比", position = 6)
    @ExcelProperty(value = "系统功率比")
    private String stationSysRate;

    /** 当日发满小时 */
    @ApiModelProperty(value = "当日发满小时", position = 7)
    @ExcelProperty(value = "当日发满小时")
    private String dayPowerhours;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 8)
    @ExcelProperty(value = "修改时间")
    private String updateTime;

    /** 建站日期 */
    @ApiModelProperty(value = "建站日期", position = 9)
    @ExcelProperty(value = "建站日期")
    private String stationCreatetime;

    /** 并网状态 */
    @ApiModelProperty(value = "并网状态", position = 10)
    @ExcelProperty(value = "并网状态")
    private String stationGridstatusChine;


    @ApiModelProperty(value = "电站类型枚举翻译", position = 11)
    @ExcelProperty(value = "电站类型")
    private String stationTypeChine;

    /** 电站系统 */
    @ApiModelProperty(value = "电站系统", position = 12)
    @ExcelProperty(value = "电站系统")
    private String stationSystemChine;

    /** 安装公司 */
    @ApiModelProperty(value = "安装公司", position = 13)
    @ExcelProperty(value = "安装公司")
    private String stationCompany;

    /** 电站类型 */
    @ApiModelProperty(value = "电站类型", position = 14)
//    @ExcelProperty(value = "电站类型")
    @ExcelIgnore
    private Integer stationType;

    @ApiModelProperty(value = "电站系统", position = 15)
//    @ExcelProperty(value = "电站系统")
    @ExcelIgnore
    private Integer stationSystem;

    @ApiModelProperty(value = "并网状态", position = 16)
//    @ExcelProperty(value = "并网状态")
    @ExcelIgnore
    private Integer stationGridstatus;

    @ApiModelProperty(value = "电站状态", position = 17)
//    @ExcelProperty(value = "电站状态")
    @ExcelIgnore
    private Integer stationStatus;

    @ApiModelProperty(value = "报警状态", position = 18)
//    @ExcelProperty(value = "报警状态")
    @ExcelIgnore
    private Integer stationAlarm;

}
