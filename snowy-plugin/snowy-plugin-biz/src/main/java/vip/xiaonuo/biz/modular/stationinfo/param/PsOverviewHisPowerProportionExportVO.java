package vip.xiaonuo.biz.modular.stationinfo.param;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/2/29 18:38
 */

@Getter
@Setter
public class PsOverviewHisPowerProportionExportVO {
    /** 发电量 */
    @ExcelProperty(value = "发电量", index = 0)
    private Double energy;

    /** 发电用于负载的电量 */
    @ExcelProperty(value = "发电用于负载的电量", index = 1)
    private Double energyToLoad;

    /** 发电用于电池充电的量 */
    @ExcelProperty(value = "发电用于电池充电的量", index = 2)
    private Double energyToCharge;

    /** 发电用于并网的电量 */
    @ExcelProperty(value = "发电用于并网的电量", index = 3)
    private Double energyToGrid;

    /** 用电量 */
    @ExcelProperty(value = "用电量", index = 4)
    private Double usedEnergy;

    /** 用电来自发电的量 */
    @ExcelProperty(value = "用电来自发电的量", index = 5)
    private Double usedFromEnergy;

    /** 用电来自电池放电的量 */
    @ExcelProperty(value = "用电来自电池放电的量", index = 6)
    private Double usedFromDischarge;

    /** 用电来自购电的量 */
    @ExcelProperty(value = "用电来自购电的量", index = 7)
    private Double usedFromPurchase;
}
