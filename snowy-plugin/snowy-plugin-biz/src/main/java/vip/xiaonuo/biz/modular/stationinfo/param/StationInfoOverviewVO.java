package vip.xiaonuo.biz.modular.stationinfo.param;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: wangjian
 * @date: 2023/11/14 17:53
 **/
@Getter
@Setter
public class StationInfoOverviewVO {

    /** 电站ID */
    @TableId
    @ApiModelProperty(value = "电站ID")
    private String id;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称")
    private String stationName;

    /** 电站备注 */
    @ApiModelProperty(value = "电站备注")
    private String stationMemo;

    /** 图片url */
    @ApiModelProperty(value = "图片url")
    private String picUrl;

    /** 电站类型 */
    @ApiModelProperty(value = "电站类型")
    private Integer stationType;

    /** 电站系统类型 */
    @ApiModelProperty(value = "电站系统")
    private Integer stationSystem;

    /** 节约用煤（吨） */
    @ApiModelProperty(value = "节约用煤（吨）")
    private Double saveCoal;

    /** 减少CO2排放（吨） */
    @ApiModelProperty(value = "减少CO2排放（吨）")
    private Double reduceCo2;

    /** 减少SO2排放（吨） */
    @ApiModelProperty(value = "减少SO2排放（吨）")
    private Double reduceSo2;

    /** 等效植树量（棵） */
    @ApiModelProperty(value = "等效植树量（棵）")
    private Double equivalentPlanting;

    /** 业主电话 */
    @ApiModelProperty(value = "业主电话")
    private String userTel;

    /** 地址 */
    @ApiModelProperty(value = "地址")
    private String areaDetail;

    /** 洲 */
    @ApiModelProperty(value = "洲")
    private String areaContient;

    /** 国家 */
    @ApiModelProperty(value = "国家")
    private String areaCountry;

    /** 省 */
    @ApiModelProperty(value = "省")
    private String areaProvice;

    /** 市 */
    @ApiModelProperty(value = "市")
    private String areaCity;

    /** 故障设备数量 */
    @ApiModelProperty(value = "故障设备数量")
    private Integer faultNumber;

    /** 电站下设备状态 */
    @ApiModelProperty(value = "电站下设备状态")
    private Integer deviceStatus;
}
