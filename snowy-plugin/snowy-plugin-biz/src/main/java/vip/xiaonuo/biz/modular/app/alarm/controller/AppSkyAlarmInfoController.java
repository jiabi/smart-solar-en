/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.app.alarm.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.alarminfo.param.SkyAlarmInfoIdParam;
import vip.xiaonuo.biz.modular.alarminfo.service.SkyAlarmInfoService;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoCurrentIdParam;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageParam;
import vip.xiaonuo.biz.modular.app.alarm.param.AppPsAlarmInfoPageReq;
import vip.xiaonuo.biz.modular.app.alarm.param.AppSkyAlarmHisInfoRes;
import vip.xiaonuo.biz.modular.app.alarm.param.AppSkyAlarmInfoPageVO;
import vip.xiaonuo.biz.modular.app.alarm.param.AppSkyAlarmCurrentInfoRes;
import vip.xiaonuo.biz.modular.app.alarm.service.AppSkyAlarmInfoService;
import vip.xiaonuo.biz.modular.app.entity.PageResDTO;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 故障信息历史信息控制器
 *
 * @author 全佳璧
 * @date  2023/10/23 10:50
 */
@Api(tags = "故障信息历史信息控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class AppSkyAlarmInfoController {

    @Resource
    private AppSkyAlarmInfoService appSkyAlarmInfoService;

    @Resource
    private SkyAlarmInfoService skyAlarmInfoService;


    @ApiOperationSupport(order = 1)
    @ApiOperation("电站详情-告警列表-历史告警(单个电站下告警)")
//    @SaCheckPermission("/app/stationAlarm/getAlarmInfo")
    @PostMapping("/app/stationAlarm/pagePsAlarms")
    public CommonResult<PageResDTO<AppSkyAlarmInfoPageVO>> pagePsAlarms(@RequestBody @Valid AppPsAlarmInfoPageReq request) {
        SkyAlarmInfoPageParam skyAlarmInfoPageParam = new SkyAlarmInfoPageParam();
        BeanUtil.copyProperties(request,skyAlarmInfoPageParam);
        skyAlarmInfoPageParam.setSize(request.getPageSize());
        skyAlarmInfoPageParam.setCurrent(request.getPageNum());
        skyAlarmInfoPageParam.setStationId(request.getGuid());
        skyAlarmInfoPageParam.setAlarmLevelList(request.getAlarmLevelList());
        skyAlarmInfoPageParam.setBeginTime(request.getBeginTime());
        skyAlarmInfoPageParam.setEndTime(request.getEndTime());
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            skyAlarmInfoPageParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            skyAlarmInfoPageParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            skyAlarmInfoPageParam.setStationContactId(loginUser.getId());
        }
        Page<AppSkyAlarmInfoPageVO> page = appSkyAlarmInfoService.page(skyAlarmInfoPageParam);

        return CommonResult.data(new PageResDTO<>(page.getTotal(), page.getCurrent(),page.getRecords()));
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation("所有电站历史告警")
//    @SaCheckPermission("/app/stationAlarm/pageAllPsAlarms")
    @PostMapping("/app/stationAlarm/pageAllPsAlarms")
    public CommonResult<PageResDTO<AppSkyAlarmInfoPageVO>> pageAllPsAlarms(@RequestBody @Valid AppPsAlarmInfoPageReq request) {
        SkyAlarmInfoPageParam skyAlarmInfoPageParam = new SkyAlarmInfoPageParam();
        skyAlarmInfoPageParam.setSize(request.getPageSize());
        skyAlarmInfoPageParam.setCurrent(request.getPageNum());
        skyAlarmInfoPageParam.setStationId(request.getGuid());
        skyAlarmInfoPageParam.setAlarmLevelList(request.getAlarmLevelList());
        skyAlarmInfoPageParam.setBeginTime(request.getBeginTime());
        skyAlarmInfoPageParam.setEndTime(request.getEndTime());
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            skyAlarmInfoPageParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            skyAlarmInfoPageParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            skyAlarmInfoPageParam.setStationContactId(loginUser.getId());
        }
        Page<AppSkyAlarmInfoPageVO> page = appSkyAlarmInfoService.page(skyAlarmInfoPageParam);

        return CommonResult.data(new PageResDTO<>(page.getTotal(), page.getCurrent(),page.getRecords()));
    }

    @ApiOperationSupport(order = 3)
    @ApiOperation("电站详情-告警列表-实时告警(单个电站下告警)")
//    @SaCheckPermission("/app/stationAlarm/pageRunningAlarms")
    @PostMapping("/app/stationAlarm/pageRunningAlarms")
    public CommonResult<PageResDTO<AppSkyAlarmInfoPageVO>> pageRunningAlarms(@RequestBody @Valid AppPsAlarmInfoPageReq request) {
        SkyAlarmInfoPageParam skyAlarmInfoPageParam = new SkyAlarmInfoPageParam();
        skyAlarmInfoPageParam.setSize(request.getPageSize());
        skyAlarmInfoPageParam.setCurrent(request.getPageNum());
        skyAlarmInfoPageParam.setStationId(request.getGuid());
        skyAlarmInfoPageParam.setAlarmLevelList(request.getAlarmLevelList());
        skyAlarmInfoPageParam.setBeginTime(request.getBeginTime());
        skyAlarmInfoPageParam.setEndTime(request.getEndTime());
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            skyAlarmInfoPageParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            skyAlarmInfoPageParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            skyAlarmInfoPageParam.setStationContactId(loginUser.getId());
        }
        Page<AppSkyAlarmInfoPageVO> page = appSkyAlarmInfoService.currentPage(skyAlarmInfoPageParam);

        return CommonResult.data(new PageResDTO<>(page.getTotal(), request.getPageNum(),page.getRecords()));
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation("所有电站实时告警")
//    @SaCheckPermission("/app/stationAlarm/pageAllRunningAlarms")
    @PostMapping("/app/stationAlarm/pageAllRunningAlarms")
    public CommonResult<PageResDTO<AppSkyAlarmInfoPageVO>> pageAllRunningAlarms(@RequestBody @Valid AppPsAlarmInfoPageReq request) {
        SkyAlarmInfoPageParam skyAlarmInfoPageParam = new SkyAlarmInfoPageParam();
        skyAlarmInfoPageParam.setSize(request.getPageSize());
        skyAlarmInfoPageParam.setCurrent(request.getPageNum());
        skyAlarmInfoPageParam.setStationId(request.getGuid());
        skyAlarmInfoPageParam.setAlarmLevelList(request.getAlarmLevelList());
        skyAlarmInfoPageParam.setBeginTime(request.getBeginTime());
        skyAlarmInfoPageParam.setEndTime(request.getEndTime());
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            skyAlarmInfoPageParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            skyAlarmInfoPageParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            skyAlarmInfoPageParam.setStationContactId(loginUser.getId());
        }
        Page<AppSkyAlarmInfoPageVO> page = appSkyAlarmInfoService.currentPage(skyAlarmInfoPageParam);

        return CommonResult.data(new PageResDTO<>(page.getTotal(), request.getPageNum(),page.getRecords()));
    }


    /**
     * 实时告警(未修复)-告警详情
     *
     * @author 全佳璧
     * @date  2023/10/23 10:50
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("实时告警(未修复)-告警详情")
//    @SaCheckPermission("/app/stationAlarm/getAlarmInfo")
    @PostMapping("/app/stationAlarm/getAlarmInfo")
    public CommonResult<AppSkyAlarmCurrentInfoRes> pagePsAlarms(@RequestBody @Valid SkyAlarmInfoCurrentIdParam skyAlarmInfoIdParam) {
        AppSkyAlarmCurrentInfoRes info = appSkyAlarmInfoService.detail(skyAlarmInfoIdParam);
        return CommonResult.data(info);
    }

    /**
     * 实时告警(已修复)-告警详情
     *
     * @author 全佳璧
     * @date  2023/10/23 10:50
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("实时告警(已修复)-告警详情")
//    @SaCheckPermission("/app/stationAlarm/getHistoryAlarmInfo")
    @PostMapping("/app/stationAlarm/getHistoryAlarmInfo")
    public CommonResult<AppSkyAlarmHisInfoRes> getHistoryAlarmInfo(@RequestBody @Valid SkyAlarmInfoIdParam skyAlarmInfoIdParam) {
        AppSkyAlarmHisInfoRes info = appSkyAlarmInfoService.getHistoryAlarmInfo(skyAlarmInfoIdParam);
        return CommonResult.data(info);
    }


}
