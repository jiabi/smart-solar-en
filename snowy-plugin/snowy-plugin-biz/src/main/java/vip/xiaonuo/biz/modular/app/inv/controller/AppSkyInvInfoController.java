/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.app.inv.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.app.entity.PageResDTO;
import vip.xiaonuo.biz.modular.app.inv.param.AppSkyInvInfoPageReqAllParam;
import vip.xiaonuo.biz.modular.app.inv.param.AppSkyInvInfoPageReqParam;
import vip.xiaonuo.biz.modular.app.inv.param.AppSkyInvInfoPageVO;
import vip.xiaonuo.biz.modular.app.inv.param.AppSkyPSInvInfoPageReqParam;
import vip.xiaonuo.biz.modular.app.inv.service.AppSkyInvInfoService;
import vip.xiaonuo.biz.modular.invinfo.param.*;
import vip.xiaonuo.biz.modular.invinfo.service.SkyInvInfoService;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * 逆变器信息控制器
 *
 * @author 全佳璧
 * @date  2023/10/05 16:24
 */
@Api(tags = "app逆变器信息控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
@Slf4j
public class AppSkyInvInfoController {

    @Resource
    private SkyInvInfoService skyInvInfoService;

    @Resource
    private AppSkyInvInfoService appSkyInvInfoService;

    /**
     * 获取逆变器信息分页
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取逆变器信息分页")
//    @SaCheckPermission("/app/device/pageDevices")
    @PostMapping("/app/device/pageDevices")
    public CommonResult<PageResDTO<AppSkyInvInfoPageVO>> pageDevices(@RequestBody AppSkyInvInfoPageReqParam skyInvInfoPageParam) {
        AppSkyInvInfoPageReqAllParam allParam = new AppSkyInvInfoPageReqAllParam();
        allParam.setSize(skyInvInfoPageParam.getPageSize());
        allParam.setCurrent(skyInvInfoPageParam.getPageNum());
        if (!Objects.isNull(skyInvInfoPageParam.getType())) {
            allParam.setEquType(skyInvInfoPageParam.getType());
        }
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            allParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            allParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            allParam.setStationContactId(loginUser.getId());
        }
        allParam.setLoginId(StpLoginUserUtil.getLoginUser().getId());
        allParam.setStationId(skyInvInfoPageParam.getStationId());

        allParam.setInvSn(!StringUtils.isEmpty(skyInvInfoPageParam.getInvSn())?skyInvInfoPageParam.getInvSn():null);
        return CommonResult.data(appSkyInvInfoService.page(allParam));
    }

    @ApiOperationSupport(order = 1)
    @ApiOperation("获取逆变器信息分页-具体电站的")
//    @SaCheckPermission("/app/device/pagePsDevices")
    @PostMapping("/app/device/pagePsDevices")
    public CommonResult<PageResDTO<AppSkyInvInfoPageVO>> pagePsDevices(@RequestBody @Valid AppSkyPSInvInfoPageReqParam skyInvInfoPageParam) {
        AppSkyInvInfoPageReqAllParam allParam = new AppSkyInvInfoPageReqAllParam();
        allParam.setCurrent(skyInvInfoPageParam.getPageNum());
        allParam.setSize(skyInvInfoPageParam.getPageSize());
        if (!Objects.isNull(skyInvInfoPageParam.getType())) {
            allParam.setEquType(skyInvInfoPageParam.getType());
        }
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            allParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            allParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            allParam.setStationContactId(loginUser.getId());
        }
        allParam.setLoginId(StpLoginUserUtil.getLoginUser().getId());
        allParam.setStationId(skyInvInfoPageParam.getStationId());
        return CommonResult.data(appSkyInvInfoService.page(allParam));
    }

    /**
     * 获取逆变器信息详情
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取逆变器信息详情")
//    @SaCheckPermission("app/device/getDeviceInfo")
    @PostMapping("app/device/getDeviceInfo")
    public CommonResult<SkyInvInfoDetailVO> detail(@RequestBody @Valid SkyInvInfoIdParam skyInvInfoIdParam) {
        skyInvInfoIdParam.setCode(0);
        return CommonResult.data(skyInvInfoService.detail(skyInvInfoIdParam));
    }
}
