package vip.xiaonuo.biz.modular.app.station.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2024/3/15 14:25
 */
@Setter
@Getter
@NoArgsConstructor
public class AppStationBaseInfoVO {

    /** 电站guid */
    @ApiModelProperty(value = "电站guid")
    private String guid;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称")
    private String name;

    /** 装机容量 */
    @ApiModelProperty(value = "装机容量")
    private Double installed;

    /** 电站类型 */
    @ApiModelProperty(value = "电站类型：1.分布式户用；2.分布式商业；3.分布式工业；4.地面电站")
    private Integer psType;

    /** 电站系统：1.全额并网系统；2.自发自用系统；3.储能系统   光伏那边定义：（1-并网 2-储能）*/
    @ApiModelProperty(value = "电站系统：1.全额并网系统；2.自发自用系统；3.储能系统")
    private Integer gridType;

    /** 并网时间 */
    @ApiModelProperty(value = "并网时间")
    private Date gridTime;

    /** 地址 */
    @ApiModelProperty(value = "地址")
    private String address;

    /** 时区 */
    @ApiModelProperty(value = "时区")
    private String zone;

    /** 国家 */
    @ApiModelProperty(value = "国家")
    private String ccName;

    /** 图片url */
    @ApiModelProperty(value = "图片url")
    private String uri;


    /** 经度 */
    @ApiModelProperty(value = "经度")
    private String areaLng;

    /** 纬度 */
    @ApiModelProperty(value = "纬度")
    private String areaLat;

    /** 电站状态 */
    @ApiModelProperty(value = "电站状态: 电站状态 10-正常 20-故障 30-离线")
    private Integer stationStatus;

    /** 并网状态 */
    @ApiModelProperty(value = "并网状态：0.未并网；1.已并网")
    private Integer stationGridstatus;

    @ApiModelProperty(value = "货币种类")
    private String priceType;

    /** 建站日期 */
    @ApiModelProperty(value = "建站日期")
    private Date stationCreatetime;

}
