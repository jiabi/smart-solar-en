package vip.xiaonuo.biz.modular.invmonitorcurrent.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2023/10/13 10:57
 **/
@Setter
@Getter
public class SkyInvInfoPowerParam {

    /** 设备SN */
    @ApiModelProperty(value = "设备SN")
    private String invSn;

    /** 时间类型 */
    @ApiModelProperty(value = "时间类型：1.日发电量；2.月发电量；3.年发电量；4.总发电量；5.今日并网电量；6.累计并网电量；7.今日购电量；8.累计购电量")
    private Integer timeType;

    /** 日期 */
    @ApiModelProperty(value = "年、年-月、年-月-日")
    private String dateTime;
}
