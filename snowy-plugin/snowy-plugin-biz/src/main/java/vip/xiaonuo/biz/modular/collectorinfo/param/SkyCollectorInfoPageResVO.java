package vip.xiaonuo.biz.modular.collectorinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author wangjian
 * @Date 2024/8/7 17:31
 */

@Getter
@Setter
public class SkyCollectorInfoPageResVO {
    /** 采集器SN */
    @ApiModelProperty(value = "采集器SN")
    private String collectorSn;

    /** 采集器类型 */
    @ApiModelProperty(value = "采集器类型")

    private String collectorType;
    /** 设备型号 */
    @ApiModelProperty(value = "设备型号")
    private String modelId;

    /** 所属电站 */
    @ApiModelProperty(value = "所属电站", position = 4)
    private String stationId;

    /** gprs卡号 */
    @ApiModelProperty(value = "gprs卡号", position = 5)
    private String gprsCard;

    /** 运营商 */
    @ApiModelProperty(value = "运营商", position = 6)
    private String cardOperator;

    /** 连接设备数量 */
    @ApiModelProperty(value = "连接设备数量", position = 7)
    private Integer equCount;

    /** 投运时间 */
    @ApiModelProperty(value = "投运时间", position = 7)
    private Integer runTime;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 8)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 9)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 10)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 11)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 12)
    private String updateUser;

    @ApiModelProperty(value = "所属电站名称")
    private String stationName;

    @ApiModelProperty(value = "时区")
    private String timeZone;

    /** 信号强度 **/
    private Integer signalStrength;

    /** 系统时间 */
    private LocalDateTime monitorTime;

    /** 版本号 */
    private String ver;

}
