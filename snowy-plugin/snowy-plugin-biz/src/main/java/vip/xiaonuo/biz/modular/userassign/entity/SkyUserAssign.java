/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.userassign.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户转移实体
 *
 * @author 全佳璧
 * @date  2024/02/22 16:25
 **/
@Getter
@Setter
@TableName("sky_station_info")
public class SkyUserAssign {

    /** 电站ID */
    @TableId
    @ApiModelProperty(value = "电站ID", position = 1)
    private String id;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称", position = 2)
    private String stationName;

    /** 电站备注 */
    @ApiModelProperty(value = "电站备注", position = 3)
    private String stationMemo;

    /** 建站日期 */
    @ApiModelProperty(value = "建站日期", position = 4)
    private Date stationCreatetime;

    /** 区域ID */
    @ApiModelProperty(value = "区域ID", position = 5)
    private String areaId;

    /** 时区 */
    @ApiModelProperty(value = "时区", position = 6)
    private String timeZone;

    /** 装机容量 */
    @ApiModelProperty(value = "装机容量", position = 7)
    private Double stationSize;

    /** 系统功率比 */
    @ApiModelProperty(value = "系统功率比", position = 8)
    private Double stationSysRate;

    /** 电站类型 */
    @ApiModelProperty(value = "电站类型", position = 9)
    private Integer stationType;

    /** 电站系统 */
    @ApiModelProperty(value = "电站系统", position = 10)
    private Integer stationSystem;

    /** 并网状态 */
    @ApiModelProperty(value = "并网状态", position = 11)
    private Integer stationGridstatus;

    /** 并网时间 */
    @ApiModelProperty(value = "并网时间", position = 12)
    private Date stationGridtime;

    /** 电站状态 */
    @ApiModelProperty(value = "电站状态", position = 13)
    private Integer stationStatus;

    /** 报警状态 */
    @ApiModelProperty(value = "报警状态", position = 14)
    private Integer stationAlarm;

    /** 业主ID */
    @ApiModelProperty(value = "业主ID", position = 15)
    private String stationContactid;

    /** 售电单价 */
    @ApiModelProperty(value = "售电单价", position = 16)
    private Double pricePer;

    /** 补贴单价 */
    @ApiModelProperty(value = "补贴单价", position = 17)
    private Double subsidyPer;

    /** 货币种类 */
    @ApiModelProperty(value = "货币种类", position = 18)
    private String priceType;

    /** 发电功率 */
    @ApiModelProperty(value = "发电功率", position = 19)
    private Double elecEfficiency;

    /** 发电收益 */
    @ApiModelProperty(value = "发电收益", position = 20)
    private Double elecIncome;

    /** 补贴收益 */
    @ApiModelProperty(value = "补贴收益", position = 21)
    private Double subsidyIncome;

    /** 累计收益 */
    @ApiModelProperty(value = "累计收益", position = 22)
    private Double totalIncome;

    /** 总成本 */
    @ApiModelProperty(value = "总成本", position = 23)
    private Double totalCost;

    /** 日还款 */
    @ApiModelProperty(value = "日还款", position = 24)
    private Double dayRepayment;

    /** 是否公开 */
    @ApiModelProperty(value = "是否公开", position = 25)
    private Integer stationPublic;

    /** 当日发满小时 */
    @ApiModelProperty(value = "当日发满小时", position = 26)
    private Integer dayPowerhours;

    /** 累计发电量 */
    @ApiModelProperty(value = "累计发电量", position = 27)
    private Double stationTotalpower;

    /** 累计运行天数 */
    @ApiModelProperty(value = "累计运行天数", position = 28)
    private Integer stationRuntime;

    /** 设备总数 */
    @ApiModelProperty(value = "设备总数", position = 29)
    private Integer equipTotal;

    /** 节约用煤（吨） */
    @ApiModelProperty(value = "节约用煤（吨）", position = 30)
    private Double saveCoal;

    /** 减少CO2排放（吨） */
    @ApiModelProperty(value = "减少CO2排放（吨）", position = 31)
    private Double reduceCo2;

    /** 减少SO2排放（吨） */
    @ApiModelProperty(value = "减少SO2排放（吨）", position = 32)
    private Double reduceSo2;

    /** 等效植树量（棵） */
    @ApiModelProperty(value = "等效植树量（棵）", position = 33)
    private Double equivalentPlanting;

    /** 安装公司 */
    @ApiModelProperty(value = "安装公司", position = 34)
    private String stationCompany;

    /** 图片路径 */
    @ApiModelProperty(value = "图片路径", position = 35)
    private String picUrl;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 36)
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 37)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 38)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 39)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 40)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 41)
    private String tenantId;
}
