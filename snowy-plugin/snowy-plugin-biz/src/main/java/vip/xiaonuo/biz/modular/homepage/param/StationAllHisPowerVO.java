package vip.xiaonuo.biz.modular.homepage.param;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/2/27 11:18
 */

@Getter
@Setter
public class StationAllHisPowerVO {

    /**
     * 总发电量
     */
    private Double total;

    /**
     * 日期集合
     */
    private List<String> dateList;

    /**
     * 发电量集合
     */
    private List<Double> power;
}
