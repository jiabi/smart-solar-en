/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationequ.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 电站设备关系实体
 *
 * @author 全佳璧
 * @date  2024/03/22 14:00
 **/
@Getter
@Setter
@TableName("sky_station_equ")
public class SkyStationEqu {

    /** 电站设备关系ID */
    @TableId
    @ApiModelProperty(value = "电站设备关系ID", position = 1)
    private String id;

    /** 电站ID */
    @ApiModelProperty(value = "电站ID", position = 2)
    private String stationId;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称", position = 3)
    private String stationName;

    /** 设备SN */
    @ApiModelProperty(value = "设备SN", position = 4)
    private String equSn;

    /** 设备类型  1：采集器；2：并网逆变器；3：储能逆变器 */
    @ApiModelProperty(value = "设备类型", position = 5)
    private String equType;

    /** 设备型号ID */
    @ApiModelProperty(value = "设备型号ID", position = 6)
    private String modelId;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 7)
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 8)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 9)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 10)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 11)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 12)
    private String tenantId;
}
