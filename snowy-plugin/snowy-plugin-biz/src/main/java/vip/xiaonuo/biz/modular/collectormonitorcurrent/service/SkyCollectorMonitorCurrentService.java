package vip.xiaonuo.biz.modular.collectormonitorcurrent.service;

import vip.xiaonuo.biz.modular.collectormonitorcurrent.entity.SkyCollectorMonitorCurrent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author wangjian
* @description 针对表【sky_collector_monitor_current(采集器实时数据)】的数据库操作Service
* @createDate 2023-10-25 14:30:23
*/
public interface SkyCollectorMonitorCurrentService extends IService<SkyCollectorMonitorCurrent> {

}
