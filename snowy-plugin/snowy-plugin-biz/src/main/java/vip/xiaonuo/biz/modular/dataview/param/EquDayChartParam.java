package vip.xiaonuo.biz.modular.dataview.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class EquDayChartParam {

    @ApiModelProperty(value = "发电功率")
    private Double monPacTotal;

    @ApiModelProperty(value = "按5分钟处理过的时间")
    private Date monitorTimeFormat;

    @ApiModelProperty(value = "发电量")
    private Double monTodayTotal;

}
