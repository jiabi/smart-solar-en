package vip.xiaonuo.biz.modular.app.alarm.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/27 16:27
 */
@Setter
@Getter
public class AppPsAlarmInfoPageReq {
    /** 当前页 */
    @ApiModelProperty(value = "当前页码")
    @NotNull(message = "pageNum not null")
    private Integer pageNum;

    /** 每页条数 */
    @ApiModelProperty(value = "每页条数")
    @NotNull(message = "pageSize not null")
    private Integer pageSize;

    /** 电站id */
    @ApiModelProperty(value = "电站id")
    private String guid;

    /** 告警等级 (1-告警，2-故障) */
    @ApiModelProperty(value = "告警等级 (1-告警，2-故障)")
    private List<Integer> alarmLevelList;

    /** 故障开始时间 */
    @ApiModelProperty(value = "故障开始时间")
    private String beginTime;

    /** 故障结束时间 */
    @ApiModelProperty(value = "故障结束时间")
    private String endTime;




    public int getLimitStart() {
        return (getPageNum() - 1) * getPageSize();
    }
}
