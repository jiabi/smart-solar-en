package vip.xiaonuo.biz.core.util;

import org.apache.poi.ss.usermodel.*;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author wangjian
 * @Date 2023/9/26 09:10
 **/
public class ExportExcelUtil {
//    private static Logger log  = LoggerFactory.getLogger(ExportExcelUtil.class);
    public static void expoerDataExcel(String fileName, String sheetName, HttpServletResponse response, ArrayList<String> titleKeyList, Map<String, String> titleMap, List<Map<String, Object>> srcList) throws IOException {
        //输出xls文件名称  iso8859-1
        String xlsFileName =  new String(fileName.getBytes(), "iso8859-1") + "_" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + ".xls";
        //内存中只创建100个对象
        //关键语句
        Workbook wb = new HSSFWorkbook();
        //用户表对象
        Sheet sheet = null;
        //行对象
        Row nRow = null;
        //列对象
        Cell nCell = null;
        //总行号
        int rowNo = 0;
        //页行号
        int pageRowNo = 0;

        if (srcList.size() == 0) {
            //建立新的sheet对象
            sheet = wb.createSheet(sheetName);
            //动态指定当前的工作表
            sheet = wb.getSheetAt(rowNo / 300000);
            pageRowNo = 0;      //新建了工作表,重置工作表的行号为0
            // -----------定义表头-----------
            nRow = sheet.createRow(pageRowNo++);
            nRow.setHeightInPoints(20);

            // 设置表头样式
            CellStyle headerStyle = wb.createCellStyle();
            headerStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            Font headerFont = wb.createFont();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerFont.setColor(IndexedColors.BLACK.getIndex());
            headerStyle.setFont(headerFont);
            headerFont.setBold(true);

            // 列数 titleKeyList.size()
            for (int i = 0; i < titleKeyList.size(); i++) {
                Cell cellTem = nRow.createCell(i);
                cellTem.setCellValue(titleMap.get(titleKeyList.get(i)));

                cellTem.setCellStyle(headerStyle);
                sheet.setColumnWidth(i, 24 * 256);
            }
        }
        for (Map<String, Object> srcMap : srcList) {
            //写入300000条后切换到下个工作表
            if (rowNo % 300000 == 0) {
                // System.out.println("Current Sheet:" + rowNo / 300000);
                //建立新的sheet对象
                sheet = wb.createSheet(sheetName);
                //动态指定当前的工作表
                sheet = wb.getSheetAt(rowNo / 300000);
                //新建了工作表,重置工作表的行号为0
                pageRowNo = 0;
                // -----------定义表头-----------
                nRow = sheet.createRow(pageRowNo++);
                nRow.setHeightInPoints(20);

                // 设置表头样式
                CellStyle headerStyle = wb.createCellStyle();
                headerStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
                headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

                Font headerFont = wb.createFont();
                headerFont.setFontHeightInPoints((short) 12);
                headerFont.setColor(IndexedColors.BLACK.getIndex());
                headerStyle.setAlignment(HorizontalAlignment.CENTER);
                headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                headerStyle.setFont(headerFont);
                // 加粗
                headerFont.setBold(true);
                // 设置边框
                headerStyle.setBorderBottom(BorderStyle.THIN);
                headerStyle.setBorderTop(BorderStyle.THIN);
                headerStyle.setBorderLeft(BorderStyle.THIN);
                headerStyle.setBorderRight(BorderStyle.THIN);
                // 列数 titleKeyList.size()
                for (int i = 0; i < titleKeyList.size(); i++) {
                    Cell cellTem = nRow.createCell(i);
                    cellTem.setCellValue(titleMap.get(titleKeyList.get(i)));

                    cellTem.setCellStyle(headerStyle);
                    sheet.setColumnWidth(i, 24 * 256);
                }
                rowNo++;
            }
            rowNo++;
            //新建行对象
            nRow = sheet.createRow(pageRowNo++);
            // 内容行高
            nRow.setHeightInPoints(18);

            // 行，获取cell值
            for (int j = 0; j < titleKeyList.size(); j++) {
                nCell = nRow.createCell(j);
                if (srcMap.get(titleKeyList.get(j)) != null) {
                    // 内容样式
                    CellStyle contentStyle = wb.createCellStyle();
                    Font contentFont = wb.createFont();
                    // 字体大小颜色
                    contentFont.setFontHeightInPoints((short) 12);
                    contentFont.setColor(IndexedColors.BLACK.getIndex());
//                    // 水平居中
//                    contentStyle.setAlignment(HorizontalAlignment.CENTER);
                    contentStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                    contentStyle.setFont(contentFont);

                    // 设置边框
                    contentStyle.setBorderBottom(BorderStyle.THIN);
                    contentStyle.setBorderTop(BorderStyle.THIN);
                    contentStyle.setBorderLeft(BorderStyle.THIN);
                    contentStyle.setBorderRight(BorderStyle.THIN);

                    nCell.setCellStyle(contentStyle);

                    nCell.setCellValue(srcMap.get(titleKeyList.get(j)).toString());
                } else {
                    nCell.setCellValue("");
                }
            }
        }
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition",xlsFileName);
        response.flushBuffer();
        response.setCharacterEncoding("UTF-8");
        OutputStream outputStream = response.getOutputStream();
        wb.write(response.getOutputStream());
        wb.close();
        outputStream.flush();
        outputStream.close();
    }

    public static void expoerDataExcel(String fileName, String sheetName, HttpServletResponse response, ArrayList<String> titleKeyList, Map<String, String> titleMap, JSONArray src_list) throws IOException {
        //输出xls文件名称
        String xlsFile_name = fileName + "_" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + ".xlsx";
        //内存中只创建100个对象
        //关键语句
//        Workbook wb = new SXSSFWorkbook(100);
        Workbook wb = new HSSFWorkbook();
        //用户表对象
        Sheet sheet = null;
        //行对象
        Row nRow = null;
        //列对象
        Cell nCell = null;
        //总行号
        int rowNo = 0;
        //页行号
        int pageRowNo = 0;

        if (src_list.size() == 0) {
            //建立新的sheet对象
            sheet = wb.createSheet(sheetName);
            //动态指定当前的工作表
            sheet = wb.getSheetAt(rowNo / 300000);
            pageRowNo = 0;      //新建了工作表,重置工作表的行号为0
            // -----------定义表头-----------
            nRow = sheet.createRow(pageRowNo++);
            // 列数 titleKeyList.size()
            for (int i = 0; i < titleKeyList.size(); i++) {
                Cell cell_tem = nRow.createCell(i);
                cell_tem.setCellValue(titleMap.get(titleKeyList.get(i)));
            }
        }
        // 总价计算
        Map<String, BigDecimal> countMap;

        for (int k = 0; k < src_list.size(); k++) {
            JSONObject srcMap = src_list.getJSONObject(k);
            //写入300000条后切换到下个工作表
            if (rowNo % 300000 == 0) {
                // System.out.println("Current Sheet:" + rowNo / 300000);
                //建立新的sheet对象
                sheet = wb.createSheet(sheetName);
                //动态指定当前的工作表
                sheet = wb.getSheetAt(rowNo / 300000);
                //新建了工作表,重置工作表的行号为0
                pageRowNo = 0;
                // -----------定义表头-----------
                nRow = sheet.createRow(pageRowNo++);
                // 列数 titleKeyList.size()
                for (int i = 0; i < titleKeyList.size(); i++) {
                    Cell cell_tem = nRow.createCell(i);
                    cell_tem.setCellValue(titleMap.get(titleKeyList.get(i)));
                }
                rowNo++;
            }
            rowNo++;
            //新建行对象
            nRow = sheet.createRow(pageRowNo++);

            // 行，获取cell值
            for (int j = 0; j < titleKeyList.size(); j++) {
                nCell = nRow.createCell(j);
                if (srcMap.get(titleKeyList.get(j)) != null) {
                    nCell.setCellValue(srcMap.get(titleKeyList.get(j)).toString());
                } else {
                    nCell.setCellValue("");
                }
            }
        }
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(xlsFile_name, "utf-8"));
        response.flushBuffer();
        OutputStream outputStream = response.getOutputStream();
        wb.write(response.getOutputStream());
        wb.close();
        outputStream.flush();
        outputStream.close();
    }
}
