package vip.xiaonuo.biz.core.util.easyexcel;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/8 10:23
 */
@Getter
@Setter
public class EasyExcelRowBase {
    private int row;
}
