package vip.xiaonuo.biz.modular.equcontrol.param;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/5/6 14:33
 */
@Getter
@Setter
public class SkyIotParam<T> {
    /** 发送时间(YYYY-MM-dd HH:mm:ss)例:2024-02-01 09:10:20 **/
    private String sendTime;

    /** 请求ID (云端下发指令唯一标识) **/
    private String requestId;

    private List<T> deviceList;
}
