/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationequ.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.stationequ.entity.SkyStationEqu;
import vip.xiaonuo.biz.modular.stationequ.param.SkyStationEquAddParam;
import vip.xiaonuo.biz.modular.stationequ.param.SkyStationEquEditParam;
import vip.xiaonuo.biz.modular.stationequ.param.SkyStationEquIdParam;
import vip.xiaonuo.biz.modular.stationequ.param.SkyStationEquPageParam;

import java.util.List;

/**
 * 电站设备关系Service接口
 *
 * @author 全佳璧
 * @date  2024/03/22 14:00
 **/
public interface SkyStationEquService extends IService<SkyStationEqu> {

    /**
     * 获取电站设备关系分页
     *
     * @author 全佳璧
     * @date  2024/03/22 14:00
     */
    Page<SkyStationEqu> page(SkyStationEquPageParam skyStationEquPageParam);

    /**
     * 添加电站设备关系
     *
     * @author 全佳璧
     * @date  2024/03/22 14:00
     */
    void add(SkyStationEquAddParam skyStationEquAddParam);

    /**
     * 编辑电站设备关系
     *
     * @author 全佳璧
     * @date  2024/03/22 14:00
     */
    void edit(SkyStationEquEditParam skyStationEquEditParam);

    /**
     * 删除电站设备关系
     *
     * @author 全佳璧
     * @date  2024/03/22 14:00
     */
    void delete(List<SkyStationEquIdParam> skyStationEquIdParamList);

    /**
     * 获取电站设备关系详情
     *
     * @author 全佳璧
     * @date  2024/03/22 14:00
     */
    SkyStationEqu detail(SkyStationEquIdParam skyStationEquIdParam);

    /**
     * 获取电站设备关系详情
     *
     * @author 全佳璧
     * @date  2024/03/22 14:00
     **/
    SkyStationEqu queryEntity(String id);

    List<String> queryStationIdList();

}
