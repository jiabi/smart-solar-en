/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.timezone.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.timezone.entity.SkyTimezone;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezoneAddParam;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezoneEditParam;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezoneIdParam;
import vip.xiaonuo.biz.modular.timezone.param.SkyTimezonePageParam;
import vip.xiaonuo.biz.modular.timezone.service.SkyTimezoneService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 地区时区控制器
 *
 * @author 全佳璧
 * @date  2024/03/08 16:23
 */
@Api(tags = "地区时区控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyTimezoneController {

    @Resource
    private SkyTimezoneService skyTimezoneService;

    /**
     * 获取地区时区分页
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取地区时区分页")
    @SaCheckPermission("/biz/timezone/page")
    @GetMapping("/biz/timezone/page")
    public CommonResult<Page<SkyTimezone>> page(SkyTimezonePageParam skyTimezonePageParam) {
        return CommonResult.data(skyTimezoneService.page(skyTimezonePageParam));
    }

    /**
     * 添加地区时区
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加地区时区")
    @CommonLog("添加地区时区")
    @SaCheckPermission("/biz/timezone/add")
    @PostMapping("/biz/timezone/add")
    public CommonResult<String> add(@RequestBody @Valid SkyTimezoneAddParam skyTimezoneAddParam) {
        skyTimezoneService.add(skyTimezoneAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑地区时区
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑地区时区")
    @CommonLog("编辑地区时区")
    @SaCheckPermission("/biz/timezone/edit")
    @PostMapping("/biz/timezone/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyTimezoneEditParam skyTimezoneEditParam) {
        skyTimezoneService.edit(skyTimezoneEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除地区时区
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除地区时区")
    @CommonLog("删除地区时区")
    @SaCheckPermission("/biz/timezone/delete")
    @PostMapping("/biz/timezone/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyTimezoneIdParam> skyTimezoneIdParamList) {
        skyTimezoneService.delete(skyTimezoneIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取地区时区详情
     *
     * @author 全佳璧
     * @date  2024/03/08 16:23
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取地区时区详情")
    @SaCheckPermission("/biz/timezone/detail")
    @GetMapping("/biz/timezone/detail")
    public CommonResult<SkyTimezone> detail(@Valid SkyTimezoneIdParam skyTimezoneIdParam) {
        return CommonResult.data(skyTimezoneService.detail(skyTimezoneIdParam));
    }

}
