/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.app.station.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.app.entity.PageResDTO;
import vip.xiaonuo.biz.modular.app.inv.param.AppStationInfoGuidReqParam;
import vip.xiaonuo.biz.modular.app.inv.param.AppStationInfoOverviewPowerVO;
import vip.xiaonuo.biz.modular.app.station.param.*;
import vip.xiaonuo.biz.modular.app.station.service.AppStationInfoService;
import vip.xiaonuo.biz.modular.stationinfo.param.*;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import javax.annotation.Resource;

import java.util.List;

import javax.validation.Valid;
/**
 * 电站信息控制器
 *
 * @author 全佳璧
 * @date  2023/09/12 10:51
 */
@Api(tags = "app电站信息控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class AppStationInfoController {

    private static Logger logger = LoggerFactory.getLogger(AppStationInfoController.class);

    @Resource
    private StationInfoService stationInfoService;

    @Resource
    private AppStationInfoService appStationInfoService;

    @ApiOperationSupport(order = 1)
    @ApiOperation("app总览-获取电站概览信息-基础")
//    @SaCheckPermission("/app/homepage/getPowerPro")
    @PostMapping("/app/homepage/getPowerPro")
    public CommonResult<AppStationInfoOverviewVO>  overviewInfo() {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        String stationCompany=null;
        if (!roleCodeList.contains(SkyRoleCodeEnum.SUPERADMIN.getValue())) {
            stationCompany = loginUser.getId();
        }
        return CommonResult.data(appStationInfoService.overviewInfo(stationCompany));
    }

    /**
     * 获取电站离线和警报状态数量
     * @param
     * @return
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("app总览-状态和警报")
//    @SaCheckPermission("/app/homepage/getStationStats")
    @GetMapping("/app/homepage/getStationStats")
    public CommonResult<AppStationStatusVO>  statusAndAlarmCount() {
        AppStationStatusVO res = new AppStationStatusVO();
        StationInfoPageParam stationInfoPageParam = new StationInfoPageParam();
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            stationInfoPageParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            stationInfoPageParam.setStationCompanyId(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            stationInfoPageParam.setStationContactId(loginUser.getId());
        }
        StationStatusVO stationStatusVO = stationInfoService.statusAndAlarmCount(stationInfoPageParam);
        if (stationStatusVO != null) {
            BeanUtil.copyProperties(stationStatusVO,res);
            res.setOffline(stationStatusVO.getAllOffline());
            res.setAlarm(res.getTotal() - stationStatusVO.getAllOffline() - res.getNormal());
        }
        return CommonResult.data(res);
    }

    /**
     * 获取电站信息分页
     * @param request
     * @return
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("获取电站信息分页")
//    @SaCheckPermission("/app/powerStation/pagePowerStation")
    @PostMapping("/app/powerStation/pagePowerStation")
    public CommonResult<PageResDTO<AppStationInfoPageResVO>> page(@RequestBody AppStationInfoPageReqParam request) {
        // 分页
        StationInfoPageParam stationInfoPageParam = new StationInfoPageParam();
        stationInfoPageParam.setCurrent(request.getPageNum());
        stationInfoPageParam.setSize(request.getPageSize());
        if (StringUtils.isNotEmpty(request.getName())) {
            stationInfoPageParam.setStationName(request.getName());
        }
        if (request.getType() != null) {
            stationInfoPageParam.setStationType(request.getType());
        }
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            stationInfoPageParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            stationInfoPageParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            stationInfoPageParam.setStationContactId(loginUser.getId());
        }
        PageResDTO<AppStationInfoPageResVO> page = appStationInfoService.page(stationInfoPageParam);
        return CommonResult.data(page);
    }

    /**
     * 电站详情-电站概览-流动图信息
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("电站概览-电站概况流动图信息")
//    @SaCheckPermission("/app/stationinfo/flowDiagram")
    @PostMapping("/app/stationinfo/flowDiagram")
    public CommonResult<StationInfoOverviewFlowDiagramVO> flowDiagram(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.flowDiagram(stationInfoIdParam));
    }

    /**
     * 获取安装商数据统计详情
     *
     * @author 全佳璧
     * @date  2024/03/13 09:19
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("电站概览-电站概况基本信息")
//    @SaCheckPermission("/app/powerStation/getPro")
    @PostMapping("/app/powerStation/getPro")
    public CommonResult<AppStationInfoOverviewPowerVO> baseinfo(@RequestBody @Valid AppStationInfoGuidReqParam reqParam) {
        StationInfoIdParam stationInfoIdParam = new StationInfoIdParam();
        stationInfoIdParam.setId(reqParam.getGuid());
        return CommonResult.data(appStationInfoService.power(stationInfoIdParam));
    }

    /**
     * 获取电站信息详情
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("获取电站信息详情")
//    @SaCheckPermission("/app/powerStation/getBaseInfo")
    @GetMapping("/app/powerStation/getBaseInfo")
    public CommonResult<AppStationBaseInfoVO> detail(@Valid StationInfoIdParam stationInfoIdParam) {
        StationInfoVO detail = stationInfoService.detail(stationInfoIdParam);
        if (detail == null) {
            return CommonResult.data(new AppStationBaseInfoVO());
        }
        AppStationBaseInfoVO res = new AppStationBaseInfoVO();
        res.setGuid(detail.getId());
        res.setCcName(detail.getAreaCountry());
        res.setName(detail.getStationName());
        res.setPsType(detail.getStationType());
        res.setInstalled(detail.getStationSize());
        res.setZone(detail.getTimeZone());
        res.setAddress(detail.getAreaDetail());
        res.setPriceType(detail.getPriceType());
        res.setStationGridstatus(detail.getStationGridstatus());
        res.setStationCreatetime(detail.getStationCreatetime());
        // 同光伏状态一致转换
        if (ObjectUtil.isNotEmpty(detail.getStationStatus()) && (detail.getStationStatus() == 1 || detail.getStationStatus() == 0)){
            res.setStationStatus(10);
            if (ObjectUtil.isNotEmpty(detail.getStationAlarm()) && detail.getStationAlarm() == 1) {
                res.setStationStatus(20);
            }
        } else {
            res.setStationStatus(30);
            if (ObjectUtil.isNotEmpty(detail.getStationAlarm()) && detail.getStationAlarm() == 1) {
                res.setStationStatus(20);
            }
        }
        if (ObjectUtil.isNotEmpty(detail.getStationSystem())){
            if (detail.getStationSystem() == 3) {
                res.setGridType(3);
            } else if (detail.getStationSystem() == 2) {
                res.setGridType(2);
            } else {
                res.setGridType(1);
            }
        }
        res.setGridTime(detail.getStationGridtime());
        res.setUri(detail.getPicUrl());
        res.setAreaLat(detail.getAreaLat());
        res.setAreaLng(detail.getAreaLng());
        return CommonResult.data(res);
    }

    /**
     * app添加电站信息
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 7)
    @ApiOperation("app添加电站信息")
    @CommonLog("app添加电站信息")
//    @SaCheckPermission("/app/powerStation/add")
    @PostMapping("/app/powerStation/add")
    public CommonResult<String> add(@RequestBody @Valid StationInfoAddParam stationInfoAddParam) {
        appStationInfoService.add(stationInfoAddParam);
        return CommonResult.ok();
    }

    /**
     * app修改电站信息
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 7)
    @ApiOperation("app修改电站信息")
    @CommonLog("app修改电站信息")
//    @SaCheckPermission("/app/powerStation/add")
    @PostMapping("/app/powerStation/update")
    public CommonResult<String> update(@RequestBody @Valid AppStationInfoUpdateReq updateReq) {
        appStationInfoService.update(updateReq);
        return CommonResult.ok();
    }

    /**
     * app删除电站信息
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 7)
    @ApiOperation("app删除电站信息")
    @CommonLog("app删除电站信息")
//    @SaCheckPermission("/app/powerStation/delete")
    @PostMapping("/app/powerStation/delete")
    public CommonResult<String> delete(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        appStationInfoService.delete(stationInfoIdParam);
        return CommonResult.ok();
    }

    /**
     * app获取所有安装商
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 8)
    @ApiOperation("app获取所有安装商")
    @CommonLog("app获取所有安装商")
//    @SaCheckPermission("/app/powerStation/getAllInstaller")
    @PostMapping("/app/powerStation/getAllInstaller")
    public CommonResult<List<UserInfoParam>> getAllInstaller() {
        return CommonResult.data(appStationInfoService.getAllInstaller());
    }

    /**
     * app获取所有业主
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 8)
    @ApiOperation("app获取所有业主")
    @CommonLog("app获取所有业主")
//    @SaCheckPermission("/app/powerStation/getAllUser")
    @PostMapping("/app/powerStation/getAllUser")
    public CommonResult<List<UserInfoParam>> getAllUser() {
        return CommonResult.data(appStationInfoService.getAllUser());
    }

    /**
     * app获取单个电站的节能减排数据
     */
    @ApiOperationSupport(order = 8)
    @ApiOperation("app获取单个电站的节能减排数据")
    @CommonLog("app获取单个电站的节能减排数据")
//    @SaCheckPermission("/app/powerStation/getPsEnergySave")
    @PostMapping("/app/powerStation/getPsEnergySave")
    public CommonResult<AppStationInfoEnergySaveVO> getPsEnergySave(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(appStationInfoService.getPsEnergySave(stationInfoIdParam));
    }

    /**
     * app接口转发-地址查询
     */
    @ApiOperationSupport(order = 8)
    @ApiOperation("app接口转发-地址查询")
    @CommonLog("app接口转发-地址查询")
//    @SaCheckPermission("/app/powerStation/getPsEnergySave")
    @PostMapping("/app/powerStation/inputtips")
    public CommonResult<String> inputtips(@RequestBody @Valid AppApiTransmitParam param) {
        return CommonResult.data(appStationInfoService.inputtips(param));
    }

}
