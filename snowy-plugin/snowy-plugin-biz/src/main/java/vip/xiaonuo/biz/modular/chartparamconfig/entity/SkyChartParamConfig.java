/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.chartparamconfig.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 参数匹配实体
 *
 * @author 王坚
 * @date  2024/02/21 11:51
 **/
@Getter
@Setter
@TableName("sky_chart_param_config")
public class SkyChartParamConfig {

    /** ID */
    @TableId
    @ApiModelProperty(value = "设备型号ID(sky_equip_model)", position = 1)
    private String id;
    /** 设备型号ID(sky_equip_model) */
    @ApiModelProperty(value = "设备型号ID(sky_equip_model)", position = 2)
    private String semId;

    /** 设备品牌名称 */
    @ApiModelProperty(value = "设备品牌名称", position = 3)
    private String brand;

    /** 型号 */
    @ApiModelProperty(value = "型号", position = 4)
    private String model;
    /** 规格 */
    @ApiModelProperty(value = "规格", position = 5)
    private String specification;


    /** 对应参数列表 */
    @ApiModelProperty(value = "对应参数列表", position = 6)
    private String params;

    /** 类型 */
    @ApiModelProperty(value = "类型", position = 7)
    private Integer type;

    /** 是否删除（1-是 2-否） */
    @ApiModelProperty(value = "是否删除", position = 8)
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 9)
    private String tenantId;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 10)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}
