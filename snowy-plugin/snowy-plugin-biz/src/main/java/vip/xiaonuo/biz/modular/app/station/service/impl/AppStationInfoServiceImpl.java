package vip.xiaonuo.biz.modular.app.station.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.w3c.dom.Document;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.config.StationInfoConfig;
import vip.xiaonuo.biz.core.enums.EquTypeForTableSkyStationEquEnum;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyDeleteFlagEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.DoubleUtils;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.app.entity.PageResDTO;
import vip.xiaonuo.biz.modular.app.inv.param.AppStationInfoOverviewPowerVO;
import vip.xiaonuo.biz.modular.app.station.mapper.AppStationInfoMapper;
import vip.xiaonuo.biz.modular.app.station.param.*;
import vip.xiaonuo.biz.modular.app.station.service.AppStationInfoService;
import vip.xiaonuo.biz.modular.equrelationship.entity.SkyEquRelationship;
import vip.xiaonuo.biz.modular.equrelationship.service.SkyEquRelationshipService;
import vip.xiaonuo.biz.modular.installerstatistic.entity.SkyInstallerStatistic;
import vip.xiaonuo.biz.modular.installerstatistic.mapper.SkyInstallerStatisticMapper;
import vip.xiaonuo.biz.modular.installerstatistic.service.SkyInstallerStatisticService;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.invinfo.param.SkySnAndEquTypeParam;
import vip.xiaonuo.biz.modular.invinfo.service.SkyInvInfoService;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;
import vip.xiaonuo.biz.modular.invmonitorcurrent.mapper.SkyInvMonitorCurrentMapper;
import vip.xiaonuo.biz.modular.stationequ.entity.SkyStationEqu;
import vip.xiaonuo.biz.modular.stationequ.service.SkyStationEquService;
import vip.xiaonuo.biz.modular.stationinfo.entity.Area;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.mapper.AreaMapper;
import vip.xiaonuo.biz.modular.stationinfo.mapper.StationInfoMapper;
import vip.xiaonuo.biz.modular.stationinfo.mapper.StationPowerMapper;
import vip.xiaonuo.biz.modular.stationinfo.param.*;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.PsEquChartResVO;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.PsChartDataResVO;
import vip.xiaonuo.biz.modular.stationinfo.service.StationChartService;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.biz.modular.stationinfo.service.impl.StationInfoServiceImpl;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.mapper.SkyStationPowerMapper;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.service.SkyStationUserService;
import vip.xiaonuo.biz.modular.user.mapper.BizUserMapper;
import vip.xiaonuo.biz.modular.user.service.BizUserService;
import vip.xiaonuo.biz.modular.userrelationship.service.SkyUserRelationshipService;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/3/11 14:50
 */
@Service
@Slf4j
public class AppStationInfoServiceImpl implements AppStationInfoService {

    @Resource
    private AppStationInfoMapper appStationInfoMapper;

    @Resource
    private StationInfoService stationInfoService;

    @Resource
    private BizUserService bizUserService;

    @Resource
    private BizUserMapper bizUserMapper;

    @Resource
    private StationInfoMapper stationInfoMapper;

    @Resource
    private AreaMapper areaMapper;

    @Resource
    private SkyInvInfoMapper skyInvInfoMapper;

    @Resource
    private SkyInvInfoService skyInvInfoService;

    @Autowired
    private StationInfoConfig stationInfoConfig;

    @Resource
    private SkyStationEquService skyStationEquService;

    @Resource
    private SkyEquRelationshipService skyEquRelationshipService;

    @Resource
    private StationPowerMapper stationPowerMapper;

    @Resource
    private SkyInstallerStatisticService skyInstallerStatisticService;

    @Resource
    private StationInfoServiceImpl stationInfoServiceImpl;

    @Resource
    private SkyUserRelationshipService skyUserRelationshipService;

    @Resource
    private SkyInstallerStatisticMapper skyInstallerStatisticMapper;

    @Resource
    private SkyStationPowerMapper skyStationPowerMapper;

    @Resource
    private StationChartService stationChartService;

    @Resource
    private SkyStationUserService skyStationUserService;

    private final String SKYWORTH_SIMPLE = "SW";


    @Override
    public AppStationInfoOverviewVO overviewInfo(String stationCompany) {
        SkyInstallerStatistic statistic;
        if(StringUtils.isNotEmpty(stationCompany)){
            statistic = skyInstallerStatisticService.queryEntity(stationCompany);
        }else{
            statistic = skyInstallerStatisticMapper.selectBySuperAdmin();
        }
        //总发电量
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        List<StationInfo> stationInfoList = characterAssignment(loginUser, roleCodeList);

        AppStationInfoOverviewVO resvo = new AppStationInfoOverviewVO();
        if(ObjectUtil.isNotEmpty(stationInfoList)){
            List<String> stationIds = stationInfoList.stream().map(StationInfo::getId).collect(Collectors.toList());
            PsChartDataResVO psChartDataResVO = stationChartService.psChart(4, null, stationIds);
            resvo.setMonTotal(ObjectUtil.isEmpty(psChartDataResVO.getPower()) ? 0D : psChartDataResVO.getPower().stream().reduce(Double::sum).orElse(0D));
            resvo.setPac(stationInfoList.stream()
                    .filter(stationInfo -> Objects.nonNull(stationInfo.getElecEfficiency()))
                    .mapToDouble(StationInfo::getElecEfficiency)
                    .sum());
        }
        if (statistic != null) {
//            resvo.setPac(statistic.getCurrentPower());
            resvo.setMonToday(statistic.getPowerDaily());
//            resvo.setMonTotal(powerTotal);
            resvo.setSaveCoal(statistic.getSaveCoal());
            resvo.setReduceCo2(statistic.getReduceCo2());
            resvo.setReduceSo2(statistic.getReduceSo2());
            resvo.setEquivalentPlanting(statistic.getEquivalentPlanting());
            return resvo;
        }
        resvo = appStationInfoMapper.overviewInfo(stationCompany);
        if (resvo == null) {
            return new AppStationInfoOverviewVO();
        }
        return resvo;
    }

    public List<StationInfo> characterAssignment(SaBaseLoginUser loginUser,List<String> roleCodeList){
        // 获取数据
        QueryWrapper<StationInfo> queryWrapper = new QueryWrapper();
        if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            queryWrapper.lambda().eq(StationInfo::getStationCompany,loginUser.getId());
        }
        //分销商
        if(roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())){
            List<String> stationIds = skyStationUserService.list(Wrappers.lambdaQuery(SkyStationUser.class).eq(SkyStationUser::getInstallerIdL2, loginUser.getId()))
                    .stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            if(CollectionUtils.isEmpty(stationIds)){
                return null;
            }
            queryWrapper.lambda().in(StationInfo::getId,stationIds);
        }
        //业主
        if(roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())){
            List<String> stationIds = skyStationUserService.list(Wrappers.lambdaQuery(SkyStationUser.class).eq(SkyStationUser::getUserId, loginUser.getId()))
                    .stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            if(CollectionUtils.isEmpty(stationIds)){
                return null;
            }
            queryWrapper.lambda().in(StationInfo::getId,stationIds);
        }
        return stationInfoService.list(queryWrapper);
    }

    @Override
    public PageResDTO<AppStationInfoPageResVO> page(StationInfoPageParam stationInfoPageParam) {
        PageResDTO<AppStationInfoPageResVO> resDTO = new PageResDTO<>(0,stationInfoPageParam.getCurrent(),null);
        Page<StationInfoPageModel> page = stationInfoService.page(stationInfoPageParam);
        if (page.getRecords() == null){
            return resDTO;
        }
        // 今日发电量
        List<StationInfoPageModel> records = page.getRecords();
        List<String> idList = records.stream().map(StationInfoPageModel::getId).collect(Collectors.toList());
        String day = LocalDateUtils.formatLocalDateTime(LocalDateTime.now(), LocalDateUtils.DATE_FORMATTER);
        List<PsIdAndPowerInfoParam> dayPowerList = appStationInfoMapper.getTodayPower(idList,day);
        Map<String, Double> dayPowerMap = dayPowerList.stream().collect(Collectors.toMap(PsIdAndPowerInfoParam::getStationId, PsIdAndPowerInfoParam::getPower));

        // 组装返回数据
        List<AppStationInfoPageResVO> voList = new ArrayList<>();
        for (StationInfoPageModel model:records) {
            AppStationInfoPageResVO psInfo = new AppStationInfoPageResVO();
            psInfo.setId(model.getId());
            psInfo.setGuid(model.getId());
            psInfo.setName(model.getStationName());
            // 同光伏状态一致转换
            if (model.getStationStatus() == 1 || model.getStationStatus() == 0){
                psInfo.setStatus(10);
                if (model.getStationAlarm() == 1) {
                    psInfo.setStatus(20);
                }
            } else {
                psInfo.setStatus(30);
                if (model.getStationAlarm() == 1) {
                    psInfo.setStatus(20);
                }
            }
            psInfo.setInstalled(model.getStationSize());
            psInfo.setCurrentPower(ObjectUtil.isEmpty(model.getElecEfficiency()) ? 0D : Double.parseDouble(model.getElecEfficiency()));
            psInfo.setEnergyInDay(dayPowerMap.getOrDefault(model.getId(),0D));
            psInfo.setAddress(model.getArea().getAreaDetail());
            psInfo.setUri(model.getPicUrl());
            if (ObjectUtil.isNotEmpty(model.getArea())) {
                psInfo.setAreaLat(model.getArea().getAreaLat());
                psInfo.setAreaLng(model.getArea().getAreaLng());
            }
            voList.add(psInfo);
        }
        resDTO.setRows(page.getTotal());
        resDTO.setData(voList);
        return resDTO;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(StationInfoAddParam stationInfoAddParam) {
        log.info("==========request data app add station info :{}",JSON.toJSONString(stationInfoAddParam));
        // 设备sn
        Map<String, CollectorAndInvSnParam> snMap = null;
        List<String> invSnList = null;
        if (ObjectUtil.isNotEmpty(stationInfoAddParam.getEquSnList())) {
            List<CollectorAndInvSnParam> equSnList = stationInfoAddParam.getEquSnList();
            snMap = equSnList.stream().collect(Collectors.toMap(CollectorAndInvSnParam::getInvSn, Function.identity()));
            invSnList = equSnList.stream().map(CollectorAndInvSnParam::getInvSn).collect(Collectors.toList());
        }

        // 并网状态
        if (!Objects.isNull(stationInfoAddParam.getStationGridtime())) {
            boolean after = this.gridStatus(stationInfoAddParam.getStationGridtime());
            if (after) {
                stationInfoAddParam.setStationGridstatus(0);
            } else {
                stationInfoAddParam.setStationGridstatus(1);
            }
        }
        StationInfo stationInfo = BeanUtil.toBean(stationInfoAddParam, StationInfo.class);
        //建站日期为当前日期formatLocalDateTime
        String nowDate = LocalDateUtils.formatLocalDateTime(LocalDateTime.now(), LocalDateUtils.DATE_FORMATTER);
        try {
            Date date = formTimeZone(nowDate, stationInfoAddParam.getTimeZone());
            stationInfo.setStationCreatetime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //并网时间
        if(!CollectionUtils.isEmpty(invSnList)){
            stationInfoService.stationGridDate(invSnList, stationInfo.getTimeZone(), stationInfo);
        }
        // 系统功率比
        this.sysRate(stationInfo);
        // 业主信息
        if (StringUtils.isNotEmpty(stationInfoAddParam.getUserTel())) {
            String userId = stationInfoServiceImpl.checkOrAddUser(stationInfoAddParam);
            stationInfo.setStationContactid(userId);
        }

        // 时区
//        try {
//            checkTimeZone(stationInfoAddParam);
//        } catch (ParseException e) {
//            throw new SkyCommonException(SkyCommonExceptionEnum.TIME_ZONE_ERROR_9105);
//        }

        // 地区信息
        Area area = BeanUtil.copyProperties(stationInfoAddParam,Area.class);
        int i = areaMapper.insert(area);
        if (i!=1) {
            throw new SkyCommonException(SkyCommonExceptionEnum.AREA_SAVE_DEFAULT_9106);
        }

        stationInfo.setAreaId(area.getId());
        stationInfo.setUpdateTime(new Date());
        stationInfoMapper.insert(stationInfo);

        // 绑定设备
        if (invSnList != null && invSnList.size()>0) {
            // 自研采集器
            List<String> skyCjSns = new ArrayList<>();
            // 三方设备
            List<CollectorAndInvSnParam> thirdSnList = new ArrayList<>();
            for (String invSn : invSnList) {
                CollectorAndInvSnParam snParam = snMap.get(invSn);
                if (StringUtils.isEmpty(snParam.getColSn())) {
                    thirdSnList.add(snParam);
                } else if (!snParam.getColSn().startsWith("SW")) {
                    thirdSnList.add(snParam);
                } else {
                    skyCjSns.add(snParam.getColSn());
                }
            }


            // 如果是三方设备
            if (thirdSnList.size() > 0) {
                List<String> thirdCjSns = thirdSnList.stream().map(CollectorAndInvSnParam::getColSn).collect(Collectors.toList());
                List<String> thirdInvSns = thirdSnList.stream().map(CollectorAndInvSnParam::getInvSn).collect(Collectors.toList());
                // 电站设备关系表
                this.insertThirdStationEqu(stationInfo, thirdCjSns, thirdInvSns);

                // add三方逆变器信息
                this.addInvInfo(stationInfo.getId(), thirdInvSns);

                // 设备关系
                this.bindThirdEqu(thirdSnList);
            }

            // 自研
            if (skyCjSns.size() > 0) {
                // 通过采集器的sn集合获取逆变器的sn
                List<SkyEquRelationship> list = skyEquRelationshipService.list(new QueryWrapper<SkyEquRelationship>().lambda()
                        .in(SkyEquRelationship::getCollectorSn, skyCjSns).in(SkyEquRelationship::getTypeId,2,3,5));
                List<String> invSns = list.stream().map(SkyEquRelationship::getEquSn).collect(Collectors.toList());
                // 绑定
                this.bindEqu(stationInfo.getId(), skyCjSns, invSns);

                // 电站设备关系表 app上传的sn集合是采集器的！
                this.insertStationEqu(stationInfo, skyCjSns, invSns);

                // 安规
                if (ObjectUtil.isNotEmpty(stationInfoAddParam.getInvRegulatory()) && ObjectUtil.isNotEmpty(stationInfoAddParam.getRegulatoryCountry())) {
                    skyInvInfoMapper.update(null, new UpdateWrapper<SkyInvInfo>().lambda()
                            .set(SkyInvInfo::getInvRegulatory, stationInfoAddParam.getInvRegulatory())
                            .set(SkyInvInfo::getRegulatoryCountry, stationInfoAddParam.getRegulatoryCountry())
                            .in(SkyInvInfo::getInvSn, skyCjSns));
                }
            }

        }

        // 电站用户关系 sky_station_user
        stationInfoServiceImpl.insertStationUserRelation(stationInfo);
    }

    private void bindThirdEqu(List<CollectorAndInvSnParam> thirdSnList) {
        //
        List<SkyEquRelationship> list = new ArrayList<>();
        for (CollectorAndInvSnParam param : thirdSnList) {
            SkyEquRelationship rel = new SkyEquRelationship();
            rel.setCollectorSn(param.getColSn());
            rel.setEquSn(param.getInvSn());
            rel.setTypeId(2);
            rel.setCreateTime(new Date());
            rel.setDeleteFlag(SkyDeleteFlagEnum.NOTDELETE.getValue());
            list.add(rel);
        }
        skyEquRelationshipService.saveBatch(list);
    }


    private Map<String, String> addInvInfo(String stationId, List<String> thirdInvSns) {
        this.checkEquIfBind(thirdInvSns);
        Map<String, String> invSnModelId = new HashMap<>();
        List<SkyInvInfo> invList = new ArrayList<>();
        for (String invSn : thirdInvSns) {
            SkyInvInfo invInfo = new SkyInvInfo();
            invInfo.setInvSn(invSn);
            invInfo.setInvName(invSn);
            invInfo.setStationId(stationId);
            String modelId = this.getInvModel(invSn);
            invInfo.setModelId(modelId);
            invInfo.setCreateTime(new Date());
            invInfo.setDeleteFlag(SkyDeleteFlagEnum.NOTDELETE.getValue());

            invSnModelId.put(invSn, modelId);
            invList.add(invInfo);
        }
        if (invList.size() > 0) {
            skyInvInfoService.saveOrUpdateBatch(invList);
        }
        return invSnModelId;
    }

    private void checkEquIfBind(List<String> thirdInvSns) {
        List<SkyInvInfo> invInfos = skyInvInfoMapper.selectList(new QueryWrapper<SkyInvInfo>().lambda().in(SkyInvInfo::getInvSn, thirdInvSns));
        List<SkyInvInfo> collect = invInfos.stream().filter(SkyInvInfo -> StringUtils.isNotEmpty(SkyInvInfo.getStationId())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(collect)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.INV_INSTALLED_PS_9109, "SN:" + JSON.toJSONString(collect.stream().map(SkyInvInfo::getInvSn).collect(Collectors.toList())));
        }
    }

    /**
     * 根据逆变器sn获取设备型号
     * @param invSn
     * @return
     */
    private String getInvModel(String invSn) {
        String modelId = null;
        // 机器类别
        String type = invSn.substring(3, 5);
        // 功率
        String power = invSn.substring(5, 8);
        Integer powerW = Integer.parseInt(power);
        // 如有新增类型是储能的，需要改变后面bindThirdEqu()和insertThirdStationEqu()逻辑中设备关系的类型，暂时写死是并网的
        if (power.contains("25")) {
            modelId = "SW025KTL-T1";
        } else if (power.contains("30")) {
            modelId = "SW030KTL-T1";
        } else if (power.contains("33")) {
            modelId = "SW033KTL-T1";
        } else if (power.contains("40")) {
            modelId = "SW40KTL-T1";
        } else if (power.contains("50")) {
            modelId = "SW50KTL-T1";
        } else if (power.contains("110")) {
            modelId = "SWG110KTL-T2";
        }
        return modelId;
//        if (type == "0S") {
//            // 单相并网逆变器 SW
//            modelId += SKYWORTH_SIMPLE;
//            if (powerW > 10) {
//                modelId = modelId + power + "K" + "-S1";
//            }
//
//        } else if (type == "0T") {
//            // 三相并网逆变器
//            if () {
//                modelId = "SW025KTL-T1";
//            } else if () {
//                modelId = "SW50KTL-T1";
//            }
//
//        } else if (type == "0M") {
//            // 微型逆变器
//
//        } else if (type == "SH") {
//            // 单相高压混合逆变器
//
//        } else if (type == "SL") {
//            // 单相低压混合逆变器
//
//        } else if (type == "TH") {
//            // 三相高压混合逆变器
//
//        } else if (type == "TL") {
//            // 三相低压混合逆变器
//
//        } else if (type == "HS") {
//            // 单相高压储能一体机
//
//        } else if (type == "LS") {
//            // 单相低压储能一体机
//
//        } else if (type == "HT") {
//            // 三相高压储能一体机
//
//        } else if (type == "LT") {
//            // 三相低压储能一体机
//
//        } else if (type == "BL") {
//            // 低压户用储能电池
//
//        } else if (type == "BH") {
//            // 高压户用储能电池
//
//        } else if (type == "TA") {
//            // 工商业储能一体机
//
//        }
    }

    @Override
    public AppStationInfoOverviewPowerVO power(StationInfoIdParam stationInfoIdParam) {
        AppStationInfoOverviewPowerVO appStationInfoOverviewPowerVO = new AppStationInfoOverviewPowerVO();
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy-MM");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        AppStationInfoOverviewPowerVO appStationInfoOverviewPower = appStationInfoMapper.getpowerbyPsId(stationInfoIdParam.getId(),dateFormat.format(new Date()));
        BeanUtils.copyProperties(appStationInfoOverviewPower,appStationInfoOverviewPowerVO);
        if(ObjectUtil.isNotEmpty(appStationInfoOverviewPower)){
            //当日发电
            appStationInfoOverviewPowerVO.setVolumeDay(ObjectUtil.isNotEmpty(appStationInfoOverviewPower.getVolumeDay()) ? appStationInfoOverviewPower.getVolumeDay():0D);
            //收益
            appStationInfoOverviewPowerVO.setDailyEarning(ObjectUtil.isNotEmpty(appStationInfoOverviewPower.getDailyEarning()) ?appStationInfoOverviewPower.getDailyEarning():0D);
        }
        //当月发电
        SkyStationPower monthStationPower = skyStationPowerMapper.selectOne(Wrappers.lambdaQuery(SkyStationPower.class)
                .eq(SkyStationPower::getStationId, stationInfoIdParam.getId())
                .like(SkyStationPower::getDateTime,monthFormat.format(new Date()))
                .eq(SkyStationPower::getTimeType,2));
        appStationInfoOverviewPowerVO.setVolumeMonth(ObjectUtil.isNotEmpty(monthStationPower) ? DoubleUtils.round(Double.parseDouble(monthStationPower.getStationPower()),2):0D);
        //当年发电
        SkyStationPower yearStationPower = skyStationPowerMapper.selectOne(Wrappers.lambdaQuery(SkyStationPower.class)
                .eq(SkyStationPower::getStationId, stationInfoIdParam.getId())
                .like(SkyStationPower::getDateTime,yearFormat.format(new Date()))
                .eq(SkyStationPower::getTimeType,3));
        appStationInfoOverviewPowerVO.setVolumeYear(ObjectUtil.isNotEmpty(yearStationPower) ? DoubleUtils.round(Double.parseDouble(yearStationPower.getStationPower()),2):0D);
        //累计发电量
        SkyStationPower skyStationPower = skyStationPowerMapper.selectOne(Wrappers.lambdaQuery(SkyStationPower.class)
                .eq(SkyStationPower::getStationId, stationInfoIdParam.getId())
                .eq(SkyStationPower::getTimeType, 4));
        appStationInfoOverviewPowerVO.setTotalVolume(!ObjectUtil.isEmpty(skyStationPower)? DoubleUtils.round(Double.parseDouble(skyStationPower.getStationPower()),2):0D);

        return appStationInfoOverviewPowerVO;
    }

    @Override
    public List<UserInfoParam> getAllInstaller() {
        List<UserInfoParam> res = new ArrayList<>();
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        PsInstallerAndUserReqParam param = new PsInstallerAndUserReqParam();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.SUPERADMIN.getValue())) {
            res = appStationInfoMapper.getAllInstaller(param);
            return ObjectUtil.isEmpty(res) ? Collections.emptyList() : res;
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
            res = appStationInfoMapper.getAllInstaller(param);
            return ObjectUtil.isEmpty(res) ? Collections.emptyList() : res;
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            UserInfoParam userInfoParam = new UserInfoParam();
            userInfoParam.setName(loginUser.getName());
            userInfoParam.setId(loginUser.getId());
            res.add(userInfoParam);
            return res;
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
            res = appStationInfoMapper.getAllInstallerByUser(param);
            //如果业主没有绑定安装商
            if(CollectionUtils.isEmpty(res)){
                param.setStationContactId(null);
                res = appStationInfoMapper.getAllInstallerByUser(param);
            }
            return ObjectUtil.isEmpty(res) ? Collections.emptyList() : res;
        }
        return Collections.emptyList();
    }

    @Override
    public List<UserInfoParam> getAllUser() {
        List<UserInfoParam> res = new ArrayList<>();
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        PsInstallerAndUserReqParam param = new PsInstallerAndUserReqParam();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.SUPERADMIN.getValue())) {
            res = appStationInfoMapper.getAllUser(param);
            return ObjectUtil.isEmpty(res) ? Collections.emptyList() : res;
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
            res = appStationInfoMapper.getAllUser(param);
            return ObjectUtil.isEmpty(res) ? Collections.emptyList() : res;
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
            res = appStationInfoMapper.getInstallerUser(param);
            return ObjectUtil.isEmpty(res) ? Collections.emptyList() : res;
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            UserInfoParam userInfoParam = new UserInfoParam();
            userInfoParam.setName(loginUser.getName());
            userInfoParam.setId(loginUser.getId());
            res.add(userInfoParam);
            return res;
        }
        return Collections.emptyList();
    }

    @Override
    public AppStationInfoEnergySaveVO getPsEnergySave(StationInfoIdParam stationInfoIdParam) {
        AppStationInfoEnergySaveVO vo = new AppStationInfoEnergySaveVO(0.0,0.0,0.0,0.0);
        StationInfo stationInfo = stationInfoMapper.selectById(stationInfoIdParam.getId());
        if (stationInfo == null) {
            return vo;
        }
        vo.setSaveCoal(stationInfo.getSaveCoal());
        vo.setEquivalentPlanting(stationInfo.getEquivalentPlanting());
        vo.setReduceCo2(stationInfo.getReduceCo2());
        vo.setReduceSo2(stationInfo.getReduceSo2());
        return vo;
    }

    @Override
    public String inputtips(AppApiTransmitParam param) {
        log.info("app-地址接口转发-请求参数：{}" , JSON.toJSONString(param));
//        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("https://restapi.amap.com/v3/assistant/inputtips")
//                .queryParam("keywords", param.getKeywords())
//                .queryParam("output", param.getOutput())
//                .queryParam("datatype", param.getDatatype())
//                .queryParam("key", param.getKey());
//        // 创建 HttpEntity，可以包含请求头
//        HttpHeaders headers = new HttpHeaders();
//        // 可选：添加任何必要的请求头
//        HttpEntity<String> entity = new HttpEntity<>(headers);
//
//        // 发送 GET 请求并获取响应
//        ResponseEntity<String> response = restTemplate.exchange(
//                builder.toUriString(), // 请求 URL
//                HttpMethod.GET,        // HTTP 方法
//                entity,                // 请求实体
//                String.class           // 响应类型
//        );
//        String responseBody = response.getBody();
        // 请求的基础 URL
        String baseUrl = "https://restapi.amap.com/v3/assistant/inputtips";

        // 构造参数字符串
        String params = "";
        try {
            // 参数编码
            String keywords = URLEncoder.encode(param.getKeywords(), "UTF-8");
            params = String.format("keywords=%s&output=xml&datatype=poi&key=2ab59cabe3007300e4370592e8dc9a8e", keywords);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 构造完整的请求 URL
        String fullUrl = baseUrl + "?" + params;

        // 创建 HttpClient 实例
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            // 创建 HTTP GET 请求
            HttpGet httpGet = new HttpGet(fullUrl);
            // 发送 GET 请求并获取响应
            HttpResponse response = httpClient.execute(httpGet);

            // 获取响应状态码
            int statusCode = response.getStatusLine().getStatusCode();
            log.info("app-地址接口转发-Status Code：{}", statusCode);

            // 获取响应内容
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String responseBody = EntityUtils.toString(entity);
                log.info("app-地址接口转发-返回数据：{}", responseBody);
                return responseBody;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        log.info("app-地址接口转发-返回数据：{}", responseBody);
//        return response.getBody();
        return null;
    }

    @Override
    public void update(AppStationInfoUpdateReq updateReq) {
        StationInfoEditParam stationInfoEditParam = new StationInfoEditParam();
        BeanUtil.copyProperties(updateReq, stationInfoEditParam);
        stationInfoEditParam.setStationName(updateReq.getStationName());
        stationInfoEditParam.setStationSize(updateReq.getStationSize());
        stationInfoEditParam.setStationType(updateReq.getPsType());
        stationInfoEditParam.setStationSystem(updateReq.getGridType());
        stationInfoEditParam.setStationGridtime(updateReq.getGridTime());
        stationInfoEditParam.setAreaDetail(updateReq.getAreaDetail());
        stationInfoEditParam.setTimeZone(updateReq.getTimeZone());
        stationInfoEditParam.setAreaCountry(updateReq.getAreaCountry());
        stationInfoEditParam.setStationSystem(updateReq.getStationSystem());
        stationInfoEditParam.setStationSystem(updateReq.getStationSystem());
        stationInfoEditParam.setStationSystem(updateReq.getStationSystem());
        stationInfoEditParam.setPicUrl(updateReq.getPicUrl());

        // 设备sn
        Map<String, CollectorAndInvSnParam> snMap = null;
        List<String> invSnList = null;
        if (ObjectUtil.isNotEmpty(updateReq.getEquSnList())) {
            List<CollectorAndInvSnParam> equSnList = updateReq.getEquSnList();
            snMap = equSnList.stream().collect(Collectors.toMap(CollectorAndInvSnParam::getInvSn, Function.identity()));
            invSnList = equSnList.stream().map(CollectorAndInvSnParam::getInvSn).collect(Collectors.toList());
        }

        if (invSnList != null && invSnList.size()>0) {
            // 自研采集器
            List<String> skyCjSns = new ArrayList<>();
            // 三方设备
            List<CollectorAndInvSnParam> thirdSnList = new ArrayList<>();
            for (String invSn : invSnList) {
                CollectorAndInvSnParam snParam = snMap.get(invSn);
                if (StringUtils.isEmpty(snParam.getColSn())) {
                    thirdSnList.add(snParam);
                } else if (!snParam.getColSn().startsWith("SW")) {
                    thirdSnList.add(snParam);
                } else {
                    skyCjSns.add(snParam.getColSn());
                }
            }

            // 自研   这里自研逻辑要在三方逻辑之前
            if(!CollectionUtils.isEmpty(skyCjSns)){
                // 通过采集器的sn集合获取逆变器的sn
                List<SkyEquRelationship> list = skyEquRelationshipService.list(new QueryWrapper<SkyEquRelationship>().lambda()
                        .in(SkyEquRelationship::getCollectorSn, skyCjSns).in(SkyEquRelationship::getTypeId,2,3,5));
                List<String> invSns = list.stream().map(SkyEquRelationship::getEquSn).collect(Collectors.toList());
                stationInfoEditParam.setNbSns(invSns);
            }
            stationInfoServiceImpl.edit(stationInfoEditParam);

            // 三方
            if (thirdSnList.size() > 0) {
                List<String> thirdCjSns = thirdSnList.stream().map(CollectorAndInvSnParam::getColSn).collect(Collectors.toList());
                List<String> thirdInvSns = thirdSnList.stream().map(CollectorAndInvSnParam::getInvSn).collect(Collectors.toList());
                // 1.电站设备关系表
                // 1.1检查是否有已绑定其他电站的设备
                List<SkyInvInfo> invInfos = skyInvInfoMapper.selectList(new QueryWrapper<SkyInvInfo>().lambda()
                        .in(SkyInvInfo::getInvSn, thirdInvSns));
                List<SkyInvInfo> exceptThisStation = invInfos.stream()
                        .filter(info -> ObjectUtil.isNotEmpty(info.getStationId()) && !info.getStationId().equals(updateReq.getId()))
                        .collect(Collectors.toList());
                List<SkyInvInfo> collect = exceptThisStation.stream().filter(SkyInvInfo -> StringUtils.isNotEmpty(SkyInvInfo.getStationId())).collect(Collectors.toList());
                if (!CollectionUtils.isEmpty(collect)) {
                    throw new SkyCommonException(SkyCommonExceptionEnum.INV_INSTALLED_PS_9109, "SN:" + JSON.toJSONString(collect.stream().map(SkyInvInfo::getInvSn).collect(Collectors.toList())));
                }
                // 1.2过滤出新接入的设备
                List<String> oldinvList = exceptThisStation.stream().map(SkyInvInfo::getInvSn).collect(Collectors.toList());
                List<String> newInvList = thirdInvSns.stream()
                        .filter(sns -> !oldinvList.contains(sns))
                        .collect(Collectors.toList());
                // 1.3根据新接入的逆变器，获取对应的采集器
                List<CollectorAndInvSnParam> filteredList = thirdSnList.stream()
                        .filter(param -> newInvList.contains(param.getInvSn()))
                        .collect(Collectors.toList());
                List<String> newColList = filteredList.stream().map(CollectorAndInvSnParam::getColSn).collect(Collectors.toList());
                // 1.4更新电站设备关系表
                StationInfo stationInfo = new StationInfo();
                stationInfo.setId(updateReq.getId());
                stationInfo.setStationName(updateReq.getStationName());
                this.insertThirdStationEqu(stationInfo, newColList, newInvList);

                // 2.add三方逆变器信息
                this.addInvInfo(stationInfo.getId(), newInvList);

                // 3.设备关系
                this.bindThirdEqu(filteredList);
            }

        }
    }

    @Override
    public void delete(StationInfoIdParam stationInfoIdParam) {
        stationInfoServiceImpl.delete(CollectionUtil.toList(stationInfoIdParam));
    }

    /**
     * @description: 并网状态判断
     * @author: wangjian
     * @date: 2023/10/10 15:21
     **/
    private boolean gridStatus(Date stationGridTime){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String gridTime = format.format(stationGridTime);
        LocalDate ld = LocalDate.parse(gridTime);
        boolean after = ld.isAfter(LocalDate.now());
        return after;
    }

    /**
     * @description: 系统功率比
     *      系统功率比是衡量一座光伏电站发电能力的重要指标之一，用于衡量电站当前发电能力。
     *      计算公式：系统功率比=当前发电功率/装机容量
     *      例如系统功率比为80%，表示此电站以额定功率的80%运行。
     * @author: wangjian
     * @date: 2023/10/11 13:47
     **/
    private void sysRate(StationInfo stationInfo) {
        if (stationInfo.getStationSize() == null || stationInfo.getStationSize() == 0D ||stationInfo.getElecEfficiency() == null) {
            stationInfo.setStationSysRate("0");
            return;
        }
        double rate = stationInfo.getElecEfficiency()  / stationInfo.getStationSize() * 100;
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        stationInfo.setStationSysRate(decimalFormat.format(rate));
    }


    /**
     * @description: 新增时区修改
     * @author: wangjian
     * @date: 2023/10/10 15:46
     **/
    private void checkTimeZone(StationInfoAddParam stationInfoAddParam) throws ParseException {
        if (StringUtils.isEmpty(stationInfoAddParam.getTimeZone()) || stationInfoConfig.getTimeZone().equals(stationInfoAddParam.getTimeZone())) {
            return;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if (!Objects.isNull(stationInfoAddParam.getStationCreatetime())) {
                stationInfoAddParam.setStationCreatetime(
                        formTimeZone(sdf.format(stationInfoAddParam.getStationCreatetime()), stationInfoAddParam.getTimeZone())
                );
            }
            if (!Objects.isNull(stationInfoAddParam.getStationGridtime())) {
                stationInfoAddParam.setStationGridtime(
                        formTimeZone(sdf.format(stationInfoAddParam.getStationGridtime()), stationInfoAddParam.getTimeZone())
                );
            }
        }

    }

    /**
     * 设备绑定电站
     * @param stationId
     * @param nbSns
     */
    private void bindEqu(String stationId, List<String> cjSns, List<String> nbSns){
        if (nbSns == null || nbSns.isEmpty()) {
            return;
        }
        QueryWrapper<SkyInvInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().in(SkyInvInfo::getInvSn,nbSns);
        List<SkyInvInfo> invInfos = skyInvInfoMapper.selectList(queryWrapper);
        List<SkyInvInfo> collect = invInfos.stream().filter(SkyInvInfo -> StringUtils.isNotEmpty(SkyInvInfo.getStationId())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(collect)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.INV_INSTALLED_PS_9109, "SN:" + JSON.toJSONString(collect.stream().map(SkyInvInfo::getInvSn).collect(Collectors.toList())));
        }
        UpdateWrapper<SkyInvInfo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda().set(SkyInvInfo::getStationId,stationId).in(SkyInvInfo::getInvSn,nbSns);
        skyInvInfoMapper.update(null,updateWrapper);
    }


    /**
     * app设备绑定
     * @param stationInfo
     * @param cjSns
     */
    private void insertStationEqu(StationInfo stationInfo, List<String> cjSns, List<String> nbSns){
        List<SkySnAndEquTypeParam> snEquTypeList = skyInvInfoMapper.getEquType(nbSns);
        Map<String, SkySnAndEquTypeParam> snEquTypeMap = snEquTypeList.stream().collect(Collectors.toMap(SkySnAndEquTypeParam::getSn, Function.identity()));
        List<SkyStationEqu> stationEquList = new ArrayList<>();
        for (String sn : nbSns) {
            SkySnAndEquTypeParam typeParam = new SkySnAndEquTypeParam();
            SkySnAndEquTypeParam orDefault = snEquTypeMap.getOrDefault(sn, typeParam);
            SkyStationEqu stationEqu = new SkyStationEqu();
            stationEqu.setStationId(stationInfo.getId());
            stationEqu.setStationName(stationInfo.getStationName());
            stationEqu.setEquType(String.valueOf(orDefault.getEquType()));
            stationEqu.setEquSn(sn);
            stationEqu.setModelId(orDefault.getModelId());
            stationEqu.setCreateTime(new Date());
            stationEqu.setDeleteFlag(SkyDeleteFlagEnum.NOTDELETE.getValue());

            stationEquList.add(stationEqu);
        }
        // 采集器也绑定了
        for (String sn : cjSns) {
            if (ObjectUtil.isEmpty(sn)){
                continue;
            }
            SkyStationEqu stationEqu = new SkyStationEqu();
            stationEqu.setStationId(stationInfo.getId());
            stationEqu.setStationName(stationInfo.getStationName());
            stationEqu.setEquType(EquTypeForTableSkyStationEquEnum.COLLECTOR.getValue().toString());
            stationEqu.setEquSn(sn);
            stationEqu.setModelId(null);
            stationEqu.setCreateTime(new Date());
            stationEqu.setDeleteFlag(SkyDeleteFlagEnum.NOTDELETE.getValue());

            stationEquList.add(stationEqu);
        }
        skyStationEquService.saveBatch(stationEquList);
    }

    /**
     * app三方设备绑定
     * @param stationInfo
     * @param cjSns
     */
    private void insertThirdStationEqu(StationInfo stationInfo, List<String> cjSns, List<String> nbSns){
        List<SkyStationEqu> stationEquList = new ArrayList<>();
        for (String sn : nbSns) {
            if (ObjectUtil.isEmpty(sn)){
                continue;
            }
            SkyStationEqu stationEqu = new SkyStationEqu();
            stationEqu.setStationId(stationInfo.getId());
            stationEqu.setStationName(stationInfo.getStationName());
            stationEqu.setEquType("2");
            stationEqu.setEquSn(sn);
            stationEqu.setModelId(this.getInvModel(sn));
            stationEqu.setCreateTime(new Date());
            stationEqu.setDeleteFlag(SkyDeleteFlagEnum.NOTDELETE.getValue());

            stationEquList.add(stationEqu);
        }
        // 采集器也绑定了
        for (String sn : cjSns) {
            if (ObjectUtil.isEmpty(sn)){
                continue;
            }
            SkyStationEqu stationEqu = new SkyStationEqu();
            stationEqu.setStationId(stationInfo.getId());
            stationEqu.setStationName(stationInfo.getStationName());
            stationEqu.setEquType(EquTypeForTableSkyStationEquEnum.COLLECTOR.getValue().toString());
            stationEqu.setEquSn(sn);
            stationEqu.setModelId(this.getInvModel(sn));
            stationEqu.setCreateTime(new Date());
            stationEqu.setDeleteFlag(SkyDeleteFlagEnum.NOTDELETE.getValue());

            stationEquList.add(stationEqu);
        }
        if (stationEquList.size() > 0) {
            skyStationEquService.saveBatch(stationEquList);
        }
    }

    private Date formTimeZone(String time, String timeZone) throws ParseException {
        SimpleDateFormat formatter;
        if (time.length()>10) {
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        } else {
            formatter = new SimpleDateFormat("yyyy-MM-dd");
        }
        // 设置源时区
        formatter.setTimeZone(TimeZone.getTimeZone(stationInfoConfig.getTimeZone()));
        Date sourceDate = formatter.parse(time);
        // 设置目标时区
        formatter.setTimeZone(TimeZone.getTimeZone(timeZone));
        String targetDate = formatter.format(sourceDate);
        if (time.length()>10) {
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        } else {
            formatter = new SimpleDateFormat("yyyy-MM-dd");
        }
        return formatter.parse(targetDate);
    }
}
