package vip.xiaonuo.biz.modular.invinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.modular.equipmodel.param.SkyEquipModelIdAndNameInfo;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoIdAndNameModel;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2023/10/9 13:47
 **/
@Getter
@Setter
public class SkyInvInfoPageVO {
    /** 逆变器SN */
    @ApiModelProperty(value = "逆变器SN", position = 1)
    private String invSn;

    /** 逆变器名称 */
    @ApiModelProperty(value = "逆变器名称", position = 2)
    private String invName;

    /** 图片路径 */
    @ApiModelProperty(value = "图片路径", position = 3)
    private String picUrl;

    /** 设备型号信息 */
    @ApiModelProperty(value = "设备型号", position = 4)
    private SkyEquipModelIdAndNameInfo skyEquipModel;

    /** 所属电站信息 */
    @ApiModelProperty(value = "所属电站", position = 5)
    private StationInfoIdAndNameModel stationBaseInfo;

    /** 设备位置 */
    @ApiModelProperty(value = "设备位置", position = 6)
    private String invPosition;

    /** 安规标准 */
    @ApiModelProperty(value = "安规标准", position = 7)
    private String invRegulatory;

    /** 安规国家 */
    @ApiModelProperty(value = "安规国家", position = 8)
    private String regulatoryCountry;

    /** 累计运行天数 */
    @ApiModelProperty(value = "累计运行天数", position = 9)
    private Integer invRuntime;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 10)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 11)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 12)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 13)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 14)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 15)
    private String tenantId;

    /** 数据更新时间 */
    @ApiModelProperty(value = "数据更新时间", position = 16)
    private String monitorTime;

    /** 运行状态（1-正常 2-异常） */
    @ApiModelProperty(value = "运行状态（1-正常 2-异常）", position = 17)
    private String monState;

    @ApiModelProperty(value = "时区",position = 18)
    private String timeZone;

    /** 实时表数据更新时间 */
    @ApiModelProperty(value = "数据更新时间", position = 16)
    private String currentUpdateTime;

    /** app通讯状态：10正常 20故障 30离线 */
    @ApiModelProperty(value = "app通讯状态：10正常 20故障 30离线")
    private Integer connectStatus;

    /** app:设备类型（1-储能逆变器 2-并网逆变器 3-采集器） */
    @ApiModelProperty(value = "app:设备类型（1-储能逆变器 2-并网逆变器 3-采集器）")
    private Integer type;

}
