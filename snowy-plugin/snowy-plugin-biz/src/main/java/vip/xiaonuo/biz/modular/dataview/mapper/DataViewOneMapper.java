package vip.xiaonuo.biz.modular.dataview.mapper;

import vip.xiaonuo.biz.modular.dataview.param.AreaStationProvinceVO;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoPageParam;

import java.util.List;

public interface DataViewOneMapper {
    List<String> queryEquByAreaProvince(SkyInvInfoPageParam skyInvInfoPageParam);

    List<AreaStationProvinceVO> queryAreaByStationProvince(SkyInvInfoPageParam skyInvInfoPageParam);
}
