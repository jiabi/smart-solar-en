package vip.xiaonuo.biz.modular.stationinfo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationPower;
import vip.xiaonuo.biz.modular.stationinfo.param.PsDeviceInfoVO;
import vip.xiaonuo.biz.modular.stationinfo.param.PsOverviewHisPowerProportionVO;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoOverviewFlowDiagramVO;

import java.util.List;

/**
* @author wangjian
* @description 针对表【sky_station_power(电站发电量)】的数据库操作Mapper
* @createDate 2023-11-15 19:04:04
* @Entity src.main.java.vip.xiaonuo.biz.modular.stationinfo.entity.SkyStationPower
*/
public interface StationPowerMapper extends BaseMapper<StationPower> {

    int deleteByPrimaryKey(Long id);

    int insert(StationPower record);

    int insertSelective(StationPower record);

    StationPower selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StationPower record);

    int updateByPrimaryKey(StationPower record);

    /**
     * 根据电站id获取发电统计
     **/
    List<StationPower> getPower(@Param("stationId") String stationId , @Param("dateTime") String dateTime);

    /**
     * 获取逆变器总数
     **/
     Integer invNum(@Param("stationId") String stationId );

    /**
     * 获取逆变器告警数量
     **/
    Integer invAlarmNum(@Param("stationId") String stationId);

    /**
     * 获取逆变器离线数量
     **/
    Integer invOfflineNum(@Param("stationId") String stationId);

    /**
     * 电站概览-流动图信息
     */
    StationInfoOverviewFlowDiagramVO flowDiagram(@Param("stationId") String stationId,@Param("invSns") List<String> invSns);

    /**
     * 日发电比重统计
     * @return
     */
    PsOverviewHisPowerProportionVO psDayProportionChart(@Param("date") String date, @Param("stationId") String stationId);

    List<PsDeviceInfoVO> psDeviceList(@Param("stationId") String stationId);

    List<PsDeviceInfoVO> deviceStatusList(@Param("stationId") String stationId);
}
