package vip.xiaonuo.biz.core.enums;

import lombok.Getter;

/**
 * @Author wangjian
 * @Date 2024/3/22 11:10
 */
@Getter
public enum SkyRoleCodeEnum {
    /**
     * 超级管理员
     */
    SUPERADMIN("superAdmin"),

    /**
     * 分销商
     */
    DISTRIBUTOR("distributor"),

    /**
     * 安装商
     */
    BIZADMIN("bizAdmin"),

    /**
     * 电站业主
     */
    BIZUSER("bizUser");

    private final String value;

    SkyRoleCodeEnum(String value) {
        this.value = value;
    }
}
