package vip.xiaonuo.biz.modular.stationinfo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import vip.xiaonuo.biz.modular.stationinfo.entity.Area;
import vip.xiaonuo.biz.modular.stationinfo.mapper.AreaMapper;
import vip.xiaonuo.biz.modular.stationinfo.service.AreaService;
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements AreaService {
}
