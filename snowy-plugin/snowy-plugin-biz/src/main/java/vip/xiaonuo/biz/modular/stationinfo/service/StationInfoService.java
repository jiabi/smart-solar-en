/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationinfo.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.SkyInvInfoRankVO;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.param.*;
import cn.hutool.json.JSONObject;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * 电站信息Service接口
 *
 * @author 全佳璧
 * @date  2023/09/12 10:51
 **/
public interface StationInfoService extends IService<StationInfo> {

    /**
     * 获取电站信息分页
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
//    Page<StationInfo> pageInfo(StationInfoPageParam stationInfoPageParam);
    Page<StationInfoPageModel> page(StationInfoPageParam stationInfoPageParam);

    /**
     * 添加电站信息
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    void add(StationInfoAddParam stationInfoAddParam);


    /**
     * 更新并网日期
     * @param nbSns
     * @param timeZone
     * @param stationInfo
     */
    void stationGridDate(List<String> nbSns,String timeZone,StationInfo stationInfo);
    /**
     * 编辑电站信息
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    void edit(StationInfoEditParam stationInfoEditParam);

    /**
     * 删除电站信息
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    void delete(List<StationInfoIdParam> stationInfoIdParam);

    /**
     * 获取电站信息详情
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    StationInfoVO detail(StationInfoIdParam stationInfoIdParam);

    /**
     * 获取电站信息详情
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     **/
    StationInfo queryEntity(String id);

    /**
     * 获取动态字段配置
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
    **/
    List<JSONObject> dynamicFieldConfigList();

    StationStatusVO statusAndAlarmCount(StationInfoPageParam stationInfoPageParam);

    List<StationInfoExportVO> exportStationInfoList(StationInfoExportParam stationInfoExportParam);

    List<StationInfo> getAllStationNameAndId(PsInstallerAndUserReqParam installerAndUserModel);

    StationInfoOverviewVO overviewInfo(StationInfoIdParam stationInfoIdParam);

    StationInfoOverviewPowerVO power(StationInfoIdParam stationInfoIdParam);

    StationInfoOverviewPowerUsedVO powerUsed(StationInfoIdParam stationInfoIdParam);

    StationInfoOverviewPowerGridVO powerGrid(StationInfoIdParam stationInfoIdParam);

    List<StationInfoOverviewPowerHisVO> powerHis(StationInfoOverviewPowerHisParam hisParam);

    List<StationInfoOverviewPowerAnalysisVO> powerAnalysis(StationInfoOverviewPowerAnalysisParam hisParam);

    List<SkyInvInfoRankVO> invRank(StationInfoIdParam stationInfoIdParam);

    StationInfoOverviewInvAlarmVO invAlarmBaseInfo(StationInfoIdParam stationInfoIdParam);

    StationInfoOverviewFlowDiagramVO flowDiagram(StationInfoIdParam stationInfoIdParam);

    void userChange(StationInfoUserChangeReq stationInfoUserChangeReq);

    Map<String, Object> localDatetime(Map<String, Object> countapryMap);

    PsInvStatisticsVO invStatistics(StationInfoIdParam stationInfoIdParam);

    List<PsDeviceInfoVO> psDeviceList(StationInfoIdParam stationInfoIdParam);
}
