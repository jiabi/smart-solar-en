package vip.xiaonuo.biz.modular.app.user.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.app.station.param.AppStationInfoOverviewVO;
import vip.xiaonuo.biz.modular.app.user.param.AppGetEmailValidCodeParam;
import vip.xiaonuo.biz.modular.app.user.param.AppGetPhoneValidCodeParam;
import vip.xiaonuo.biz.modular.app.user.param.AppUserAddParam;
import vip.xiaonuo.biz.modular.app.user.service.AppUserAddService;
import vip.xiaonuo.biz.modular.user.param.BizUserAddParam;
import vip.xiaonuo.biz.modular.user.service.BizUserService;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @Author wangjian
 * @Date 2024/4/1 19:14
 */

@Api(tags = "app用户管理控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
@Slf4j
public class AppUserController {

    @Resource
    private AppUserAddService appUserAddService;

    // 获取图形验证码 /sys/userCenter/getPicCaptcha
    @ApiOperationSupport(order = 1)
    @ApiOperation("app-用户注册")
//    @SaCheckPermission("/app/user/register")
    @PostMapping("/app/user/register")
    public CommonResult<String> register(@RequestBody @Valid AppUserAddParam userAddParam) {
        appUserAddService.register(userAddParam);
        return CommonResult.ok();
    }

    @ApiOperationSupport(order = 1)
    @ApiOperation("app-邮箱获取验证码")
//    @SaCheckPermission("/app/user/appGetEmailValidCode")
    @PostMapping("/app/user/appGetEmailValidCode")
    public CommonResult<String>  appGetEmailValidCode(@RequestBody @Valid AppGetEmailValidCodeParam codeParam) {
        return CommonResult.data(appUserAddService.appGetEmailValidCode(codeParam));
    }

    @ApiOperationSupport(order = 1)
    @ApiOperation("app-手机获取验证码")
//    @SaCheckPermission("/app/user/appGetPhoneValidCode")
    @PostMapping("/app/user/appGetPhoneValidCode")
    public CommonResult<String>  appGetPhoneValidCode(@RequestBody @Valid AppGetPhoneValidCodeParam codeParam) {
        return CommonResult.data(appUserAddService.appGetPhoneValidCode(codeParam));
    }
}
