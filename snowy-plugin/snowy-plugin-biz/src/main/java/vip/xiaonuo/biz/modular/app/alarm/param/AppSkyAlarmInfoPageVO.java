package vip.xiaonuo.biz.modular.app.alarm.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class AppSkyAlarmInfoPageVO {

    private String id;

    /** 故障名称 */
    @ApiModelProperty(value = "故障名称")
    private String name;

    /** 设备SN */
    @ApiModelProperty(value = "设备SN")
    private String sn;

    private String psGuid;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称")
    private String psName;

    /** 设备类型 */
    @ApiModelProperty(value = "设备类型")
    private Integer deviceType;

    /** 设备名称 */
    @ApiModelProperty(value = "设备名称")
    private String deviceName;

    /** 故障类型type_id（1-故障 2-告警） */
    @ApiModelProperty(value = "类型（1-故障 2-告警）")
    private Integer type;

    /** 类型（1-故障 2-告警） */
    @ApiModelProperty(value = "类型（1-故障 2-告警）")
    private String faultType;

    /** 故障结束时间 */
    @ApiModelProperty(value = "故障结束时间")
    private String creatTime;

    /** 故障产生时间 */
    @ApiModelProperty(value = "故障产生时间")
    private String alarmTime;

    /** 设备时区 */
    @ApiModelProperty(value = "设备时区")
    private String zone;

    @ApiModelProperty(value = "状态")
    private String status;



}
