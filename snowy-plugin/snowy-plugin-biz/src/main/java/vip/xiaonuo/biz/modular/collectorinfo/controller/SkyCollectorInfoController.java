/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.collectorinfo.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.collectorinfo.param.*;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoPageParam;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.collectorinfo.entity.SkyCollectorInfo;
import vip.xiaonuo.biz.modular.collectorinfo.service.SkyCollectorInfoService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 采集器信息控制器
 *
 * @author 全佳璧
 * @date  2023/10/23 09:46
 */
@Api(tags = "采集器信息控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyCollectorInfoController {

    @Resource
    private SkyCollectorInfoService skyCollectorInfoService;

    /**
     * 获取采集器信息分页
     *
     * @author 全佳璧
     * @date  2023/10/23 09:46
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取采集器信息分页")
//    @SaCheckPermission("/biz/collectorinfo/page")
    @GetMapping("/biz/collectorinfo/page")
    public CommonResult<Page<SkyCollectorInfoPageResVO>> page(SkyCollectorInfoPageParam skyCollectorInfoPageParam) {
        SkyCollectorInfoReq param = new SkyCollectorInfoReq();
        BeanUtil.copyProperties(skyCollectorInfoPageParam,param);
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }
        return CommonResult.data(skyCollectorInfoService.page(param));
    }

    /**
     * 添加采集器信息
     *
     * @author 全佳璧
     * @date  2023/10/23 09:46
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加采集器信息")
    @CommonLog("添加采集器信息")
//    @SaCheckPermission("/biz/collectorinfo/add")
    @PostMapping("/biz/collectorinfo/add")
    public CommonResult<String> add(@RequestBody @Valid SkyCollectorInfoAddParam skyCollectorInfoAddParam) {
        skyCollectorInfoService.add(skyCollectorInfoAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑采集器信息
     *
     * @author 全佳璧
     * @date  2023/10/23 09:46
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑采集器信息")
    @CommonLog("编辑采集器信息")
//    @SaCheckPermission("/biz/collectorinfo/edit")
    @PostMapping("/biz/collectorinfo/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyCollectorInfoEditParam skyCollectorInfoEditParam) {
        skyCollectorInfoService.edit(skyCollectorInfoEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除采集器信息
     *
     * @author 全佳璧
     * @date  2023/10/23 09:46
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除采集器信息")
    @CommonLog("删除采集器信息")
//    @SaCheckPermission("/biz/collectorinfo/delete")
    @PostMapping("/biz/collectorinfo/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyCollectorInfoIdParam> skyCollectorInfoIdParamList) {
        skyCollectorInfoService.delete(skyCollectorInfoIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取采集器信息详情
     *
     * @author 全佳璧
     * @date  2023/10/23 09:46
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取采集器信息详情")
//    @SaCheckPermission("/biz/collectorinfo/detail")
    @GetMapping("/biz/collectorinfo/detail")
    public CommonResult<SkyCollectorInfo> detail(@Valid SkyCollectorInfoIdParam skyCollectorInfoIdParam) {
        return CommonResult.data(skyCollectorInfoService.detail(skyCollectorInfoIdParam));
    }

}
