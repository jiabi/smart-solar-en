package vip.xiaonuo.biz.modular.app.inv.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/15 11:14
 */
@Getter
@Setter
public class AppSkyInvInfoPageReqAllParam {
    /** 当前页 */
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    /** 每页条数 */
    @ApiModelProperty(value = "每页条数")
    private Integer size;

    /** 逆变器名称 */
    @ApiModelProperty(value = "逆变器名称")
    private String invName;

    /** 逆变器sn */
    @ApiModelProperty(value = "逆变器sn")
    private String invSn;

    /** 设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪 */
    private Integer equType;

    /** 设备型号 */
    @ApiModelProperty(value = "设备型号")
    private String modelId;

    /** 所属电站Id */
    @ApiModelProperty(value = "所属电站Id")
    private String stationId;

    /** 所属电站名称 */
    @ApiModelProperty(value = "所属电站名称")
    private String stationName;

    /** 电站类型：1.分布式户用；2.分布式商业；3.分布式工业；4.地面电站 */
    @ApiModelProperty(value = "电站类型：1.分布式户用；2.分布式商业；3.分布式工业；4.地面电站")
    private Integer stationType;

    /** 安装公司id */
    @ApiModelProperty(value = "安装公司id")
    private String stationCompany;

    /** 业主id */
    @ApiModelProperty(value = "业主id")
    private String stationContactId;

    /** 分销商 */
    @ApiModelProperty(value = "分销商")
    private String distributor;

    /** 登录id */
    @ApiModelProperty(value = "登录id")
    private String loginId;

    public int getLimitStart() {
        return (getCurrent() - 1) * getSize();
    }
}
