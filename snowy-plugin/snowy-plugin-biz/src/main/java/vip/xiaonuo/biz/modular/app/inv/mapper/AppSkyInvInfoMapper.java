package vip.xiaonuo.biz.modular.app.inv.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.app.inv.param.AppPsIdAndNameParam;
import vip.xiaonuo.biz.modular.app.inv.param.AppSkyInvInfoPageReqAllParam;
import vip.xiaonuo.biz.modular.app.inv.param.AppSkyInvInfoPageVO;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoPageParam;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;

import java.util.List;
import java.util.Map;

/**
 * @Author wangjian
 * @Date 2024/3/14 17:34
 */
public interface AppSkyInvInfoMapper extends BaseMapper<SkyInvInfo> {
    List<AppSkyInvInfoPageVO> pageList(AppSkyInvInfoPageReqAllParam skyInvInfoPageParam);

    Integer countNum(AppSkyInvInfoPageReqAllParam skyInvInfoPageParam);

    List<AppSkyInvInfoPageVO> pageListCurrentInfo(@Param("snList") List<String> snList);

    List<AppPsIdAndNameParam> getStationBySN(@Param("snList") List<String> snList);

}
