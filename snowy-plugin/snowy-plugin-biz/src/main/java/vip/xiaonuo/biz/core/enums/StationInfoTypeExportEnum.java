package vip.xiaonuo.biz.core.enums;

import lombok.Getter;

@Getter
public enum StationInfoTypeExportEnum {

    STATION_TYPE("stationType"),
    STATION_SYSTEM("stationSystem"),
    STATION_GRIDSTATUS("stationGridstatus"),
    STATION_STATUS("stationStatus"),
    STATION_ALARM("stationAlarm")
    ;

    private final String value;

    StationInfoTypeExportEnum(String value) {
        this.value = value;
    }

}
