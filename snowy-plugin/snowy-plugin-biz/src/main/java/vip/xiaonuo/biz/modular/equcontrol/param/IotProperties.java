package vip.xiaonuo.biz.modular.equcontrol.param;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author wangjian
 * @Date 2024/5/7 17:56
 */


@Data
@Component
@ConfigurationProperties(
        prefix = "iot"
)
public class IotProperties {
    @Value("${iot.equ-control-read-url}")
    private String equControlReadUrl;
    private String equControlUrl;
    @Value("${iot.equ-control-write-url}")
    private String equControlWriteUrl;
    @Value("${iot.equ-control-upgrade-url}")
    private String equControlUpgradeUrl;
    @Value("${iot.topic.prefix}")
    private String topicPrefix;
    @Value("${iot.topic.method.read}")
    private String readMethod;
    @Value("${iot.topic.method.write}")
    private String writeMethod;
    @Value("${iot.topic.method.upgrade}")
    private String upgradeMethod;
}
