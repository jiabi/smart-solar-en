/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationpower.task;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.modular.stationequ.service.SkyStationEquService;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.enums.DateFormatEnum;
import vip.xiaonuo.biz.modular.stationpower.mapper.SkyStationPowerMapper;
import vip.xiaonuo.biz.modular.stationpower.param.SkyStationPowerAddParam;
import vip.xiaonuo.biz.modular.stationpower.param.SkyStationPowerEditParam;
import vip.xiaonuo.biz.modular.stationpower.param.SkyStationPowerTaskParam;
import vip.xiaonuo.biz.modular.stationpower.service.SkyStationPowerService;
import vip.xiaonuo.common.timer.CommonTimerTaskRunner;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 电站发电量定时任务计算
 *
 * @author  全佳璧
 * @date    2024/3/25 9:54
 */
@Slf4j
@Component
public class StationPowerTimerTaskRunner implements CommonTimerTaskRunner {
    @Resource
    private SkyStationEquService stationEquService;

    @Resource
    private SkyStationPowerService stationPowerService;

    @Resource
    private SkyStationPowerMapper spMapper;

    @Override
    public void action() {
        LocalDateTime beginTime = LocalDateTime.now();
        List<String> stationIdList = stationEquService.queryStationIdList();
        int count = stationIdList == null ? 0 : stationIdList.size();
        String stationId = "";

        // 更新每个电站的发电量
        for (int i = 0; i < count; i++) {
            stationId = stationIdList.get(i);
            updateStationPower(stationId);

        }
        LocalDateTime endTime = LocalDateTime.now();
        log.info("定时任务-电量表统计-结束：{}。用时：{}毫秒", endTime , Duration.between(beginTime, endTime).toMillis());
    }

    private void updateStationPower(String stationId) {
        // 日发电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM_DD.getValue(), 1);
        // 月发电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM.getValue(), 2);
        // 年发电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY.getValue(), 3);
        // 累计发电量更新
        saveStationPower(stationId, DateFormatEnum.TOTAL.getValue(), 4);

        // 日并网电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM_DD.getValue(), 5);
        // 月并网电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM.getValue(), 6);
        // 年并网电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY.getValue(), 7);
        // 累计并网电量更新
        saveStationPower(stationId, DateFormatEnum.TOTAL.getValue(), 8);

        // 日购电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM_DD.getValue(), 9);
        // 月购电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM.getValue(), 10);
        // 年购电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY.getValue(), 11);
        // 累计购电量更新
        saveStationPower(stationId, DateFormatEnum.TOTAL.getValue(), 12);

        // 日用电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM_DD.getValue(), 13);
        // 月用电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM.getValue(), 14);
        // 年用电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY.getValue(), 15);
        // 累计用电量更新
        saveStationPower(stationId, DateFormatEnum.TOTAL.getValue(), 16);

        // 日负载电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM_DD.getValue(), 17);
        // 月负载电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM.getValue(), 18);
        // 年负载电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY.getValue(), 19);
        // 累计负载电量更新
        saveStationPower(stationId, DateFormatEnum.TOTAL.getValue(), 20);

        // 日充电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM_DD.getValue(), 21);
        // 月充电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM.getValue(), 22);
        // 年充电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY.getValue(), 23);
        // 累计充电量更新
        saveStationPower(stationId, DateFormatEnum.TOTAL.getValue(), 24);

        // 日放电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM_DD.getValue(), 25);
        // 月放电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY_MM.getValue(), 26);
        // 年放电量更新
        saveStationPower(stationId, DateFormatEnum.YYYY.getValue(), 27);
        // 累放充电量更新
        saveStationPower(stationId, DateFormatEnum.TOTAL.getValue(), 28);

        // 发电功率（实时）
        saveStationPower(stationId, DateFormatEnum.YYYY_MM_DD.getValue(), 29);
    }

    private void saveStationPower(String stationId, String formatStr, int timeType) {
        int updateInt = 0;
        String timeStr = "";
        if (DateFormatEnum.TOTAL.getValue().equals(formatStr)) {
            timeStr = formatStr;
        } else {
            timeStr = DateFormatUtils.format(new Date(), formatStr);
        }

        // 采集到的数据封装对象
        SkyStationPower stationPower = stationPowerService.getStationPower(stationId, timeType, timeStr);
        if (ObjectUtil.isEmpty(stationPower)) {
            return;
        }

        // 当前时间是否有记录
        SkyStationPower queryStationPower = stationPowerService.queryStationPower(stationId, timeType, timeStr);
        if (queryStationPower == null) {
            SkyStationPowerAddParam addParam =  BeanUtil.toBean(stationPower, SkyStationPowerAddParam.class);
            stationPowerService.add(addParam);
        } else {
            stationPower.setId(queryStationPower.getId());
            //stationPower.setCreateTime(null);
            stationPower.setUpdateTime(new Date());
            SkyStationPowerEditParam editParam =  BeanUtil.toBean(stationPower, SkyStationPowerEditParam.class);
            stationPowerService.edit(editParam);
        }
    }

    private void active2(){
        // 1、获取电站设备关系表中所有电站
        LocalDateTime beginTime = LocalDateTime.now();
        List<String> stationIdList = stationEquService.queryStationIdList();
        int count = stationIdList == null ? 0 : stationIdList.size();
        String stationId = "";

        // 2、获取四种时间类型的数据 YYYY_MM_DD YYYY_MM YYYY Total 并按照电站时间类型做处理
        // 日
        String day = DateFormatUtils.format(new Date(), DateFormatEnum.YYYY_MM_DD.getValue());
        List<SkyStationPowerTaskParam> dayPowerList = spMapper.queryStationPowerSumCurrent(null, null, day);
        if (!CollectionUtils.isEmpty(dayPowerList)) {
            Map<String, Map<Integer, List<SkyStationPowerTaskParam>>> dayMap = groupByPsIdAndTimeType(dayPowerList);
        }

        // 月
        String month = DateFormatUtils.format(new Date(), DateFormatEnum.YYYY_MM.getValue());
        List<SkyStationPowerTaskParam> monthPowerList = spMapper.queryStationPowerSumCurrent(null, null, month);
        Map<String, Map<Integer, List<SkyStationPowerTaskParam>>> monthMap = groupByPsIdAndTimeType(monthPowerList);

        // 年
        String year = DateFormatUtils.format(new Date(), DateFormatEnum.YYYY.getValue());
        List<SkyStationPowerTaskParam> yearPowerList = spMapper.queryStationPowerSumCurrent(null, null, year);
        Map<String, Map<Integer, List<SkyStationPowerTaskParam>>> yearMap = groupByPsIdAndTimeType(yearPowerList);

        // 总
        String total = DateFormatUtils.format(new Date(), DateFormatEnum.TOTAL.getValue());
        List<SkyStationPowerTaskParam> totalPowerList = spMapper.queryStationPowerSumCurrent(null, null, total);
        Map<String, Map<Integer, List<SkyStationPowerTaskParam>>> totalMap = groupByPsIdAndTimeType(totalPowerList);

        // 3、当前时间是否有记录


        // 更新每个电站的发电量
        for (int i = 0; i < count; i++) {
            stationId = stationIdList.get(i);
            updateStationPower(stationId);

        }
        LocalDateTime endTime = LocalDateTime.now();
        log.info("定时任务-电量表统计-结束：{}。用时：{}毫秒", endTime , Duration.between(beginTime, endTime).toMillis());
    }

    private Map<String, Map<Integer, List<SkyStationPowerTaskParam>>>  groupByPsIdAndTimeType(List<SkyStationPowerTaskParam> datPowerList) {
        // 按照 stationId 属性进行分组，然后在每个分组内再按照 timeType 进行分组
        Map<String, Map<Integer, List<SkyStationPowerTaskParam>>> groupedMap =
                datPowerList.stream()
                        .collect(Collectors.groupingBy(SkyStationPowerTaskParam::getStationId,
                                Collectors.groupingBy(SkyStationPowerTaskParam::getTimeType)));

//        groupedMap






        return groupedMap;
    }
}
