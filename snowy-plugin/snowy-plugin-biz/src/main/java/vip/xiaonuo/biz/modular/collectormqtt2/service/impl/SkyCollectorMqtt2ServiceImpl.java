/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.collectormqtt2.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.collectormqtt2.entity.SkyCollectorMqtt2;
import vip.xiaonuo.biz.modular.collectormqtt2.mapper.SkyCollectorMqtt2Mapper;
import vip.xiaonuo.biz.modular.collectormqtt2.param.SkyCollectorMqtt2AddParam;
import vip.xiaonuo.biz.modular.collectormqtt2.param.SkyCollectorMqtt2EditParam;
import vip.xiaonuo.biz.modular.collectormqtt2.param.SkyCollectorMqtt2IdParam;
import vip.xiaonuo.biz.modular.collectormqtt2.param.SkyCollectorMqtt2PageParam;
import vip.xiaonuo.biz.modular.collectormqtt2.service.SkyCollectorMqtt2Service;

import java.util.List;

/**
 * 采集器转发Service接口实现类
 *
 * @author hao
 * @date  2024/09/13 16:06
 **/
@Service
public class SkyCollectorMqtt2ServiceImpl extends ServiceImpl<SkyCollectorMqtt2Mapper, SkyCollectorMqtt2> implements SkyCollectorMqtt2Service {


    @Override
    public Page<SkyCollectorMqtt2> page(SkyCollectorMqtt2PageParam skyCollectorMqtt2PageParam) {
        QueryWrapper<SkyCollectorMqtt2> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(skyCollectorMqtt2PageParam.getMqttFactory())) {
            queryWrapper.lambda().like(SkyCollectorMqtt2::getMqttFactory, skyCollectorMqtt2PageParam.getMqttFactory());
        }
        if(ObjectUtil.isNotEmpty(skyCollectorMqtt2PageParam.getMqttDomain())) {
            queryWrapper.lambda().like(SkyCollectorMqtt2::getMqttDomain, skyCollectorMqtt2PageParam.getMqttDomain());
        }
        if(ObjectUtil.isAllNotEmpty(skyCollectorMqtt2PageParam.getSortField(), skyCollectorMqtt2PageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(skyCollectorMqtt2PageParam.getSortOrder());
            queryWrapper.orderBy(true, skyCollectorMqtt2PageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(skyCollectorMqtt2PageParam.getSortField()));
        } else {
            queryWrapper.lambda().orderByAsc(SkyCollectorMqtt2::getId);
        }
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyCollectorMqtt2AddParam skyCollectorMqtt2AddParam) {
        SkyCollectorMqtt2 skyCollectorMqtt2 = BeanUtil.toBean(skyCollectorMqtt2AddParam, SkyCollectorMqtt2.class);
        this.save(skyCollectorMqtt2);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyCollectorMqtt2EditParam skyCollectorMqtt2EditParam) {
        SkyCollectorMqtt2 skyCollectorMqtt2 = this.queryEntity(skyCollectorMqtt2EditParam.getId());
        BeanUtil.copyProperties(skyCollectorMqtt2EditParam, skyCollectorMqtt2);
        this.updateById(skyCollectorMqtt2);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyCollectorMqtt2IdParam> skyCollectorMqtt2IdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyCollectorMqtt2IdParamList, SkyCollectorMqtt2IdParam::getId));
    }

    @Override
    public SkyCollectorMqtt2 detail(SkyCollectorMqtt2IdParam skyCollectorMqtt2IdParam) {
        return this.queryEntity(skyCollectorMqtt2IdParam.getId());
    }

    @Override
    public SkyCollectorMqtt2 queryEntity(String id) {
        SkyCollectorMqtt2 skyCollectorMqtt2 = this.getById(id);
        if(ObjectUtil.isEmpty(skyCollectorMqtt2)) {
            throw new CommonException("采集器转发不存在，id值为：{}", id);
        }
        return skyCollectorMqtt2;
    }

}
