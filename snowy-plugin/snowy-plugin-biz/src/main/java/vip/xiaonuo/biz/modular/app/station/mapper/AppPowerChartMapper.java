package vip.xiaonuo.biz.modular.app.station.mapper;

import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.app.station.param.chart.AppPsPowerChartParam;
import vip.xiaonuo.biz.modular.app.station.param.chart.AppPsPowerChartVO;
import vip.xiaonuo.biz.modular.app.station.param.chart.PsPowerDayChartResParam;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationPower;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/12 9:57
 */
public interface AppPowerChartMapper {
    /**
     * app总览-发电量统计-日数据
     * @param date
     * @param companyCode
     * @return
     */
    List<PsPowerDayChartResParam> dayPowerChart(@Param("date") String date, @Param("companyCode") String companyCode, @Param("dateTime") String dateTime);

    List<PsPowerDayChartResParam> dayPowerCurrentChart(@Param("date") String date, @Param("companyCode") String companyCode);

    List<AppPsPowerChartParam> powerMonthChart(@Param("date") String date, @Param("companyCode") String companyCode);

    List<AppPsPowerChartParam> powerYearChart(@Param("date") String date, @Param("companyCode") String companyCode);

    List<AppPsPowerChartParam> powerTotalChart(@Param("companyCode") String companyCode);

    String measurePoint(@Param("invSn") String invSn);
}
