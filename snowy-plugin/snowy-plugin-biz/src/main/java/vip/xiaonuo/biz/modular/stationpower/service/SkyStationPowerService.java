/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationpower.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.param.SkyStationPowerAddParam;
import vip.xiaonuo.biz.modular.stationpower.param.SkyStationPowerEditParam;
import vip.xiaonuo.biz.modular.stationpower.param.SkyStationPowerIdParam;
import vip.xiaonuo.biz.modular.stationpower.param.SkyStationPowerPageParam;

import java.util.List;

/**
 * 电站图表Service接口
 *
 * @author 全佳璧
 * @date  2024/02/22 16:57
 **/
public interface SkyStationPowerService extends IService<SkyStationPower> {

    /**
     * 获取电站图表分页
     *
     * @author 全佳璧
     * @date  2024/02/22 16:57
     */
    Page<SkyStationPower> page(SkyStationPowerPageParam skyStationPowerPageParam);

    /**
     * 添加电站图表
     *
     * @author 全佳璧
     * @date  2024/02/22 16:57
     */
    void add(SkyStationPowerAddParam skyStationPowerAddParam);

    /**
     * 编辑电站图表
     *
     * @author 全佳璧
     * @date  2024/02/22 16:57
     */
    void edit(SkyStationPowerEditParam skyStationPowerEditParam);

    /**
     * 删除电站图表
     *
     * @author 全佳璧
     * @date  2024/02/22 16:57
     */
    void delete(List<SkyStationPowerIdParam> skyStationPowerIdParamList);

    /**
     * 获取电站图表详情
     *
     * @author 全佳璧
     * @date  2024/02/22 16:57
     */
    SkyStationPower detail(SkyStationPowerIdParam skyStationPowerIdParam);

    /**
     * 获取电站图表详情
     *
     * @author 全佳璧
     * @date  2024/02/22 16:57
     **/
    SkyStationPower queryEntity(String id);

    SkyStationPower getStationPower(String stationId, int timeType, String timeStr);

    SkyStationPower queryStationPower(String stationId, int timeType, String timeStr);

}
