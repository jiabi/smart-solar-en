/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.chartparamconfig.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyDeleteFlagEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.chartparamconfig.entity.SkyChartParamConfig;
import vip.xiaonuo.biz.modular.chartparamconfig.mapper.SkyChartParamConfigMapper;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigAddParam;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigEditParam;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigIdParam;
import vip.xiaonuo.biz.modular.chartparamconfig.param.SkyChartParamConfigPageParam;
import vip.xiaonuo.biz.modular.chartparamconfig.service.SkyChartParamConfigService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 参数匹配Service接口实现类
 *
 * @author 王坚
 * @date  2024/02/21 11:51
 **/
@Service
public class SkyChartParamConfigServiceImpl extends ServiceImpl<SkyChartParamConfigMapper, SkyChartParamConfig> implements SkyChartParamConfigService {

    @Resource
    private SkyChartParamConfigMapper skyChartParamConfigMapper;
    @Override
    public Page<SkyChartParamConfig> page(SkyChartParamConfigPageParam skyChartParamConfigPageParam) {
        QueryWrapper<SkyChartParamConfig> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(skyChartParamConfigPageParam.getBrand())) {
            queryWrapper.lambda().like(SkyChartParamConfig::getBrand, skyChartParamConfigPageParam.getBrand());
        }
        if(ObjectUtil.isNotEmpty(skyChartParamConfigPageParam.getModel())) {
            queryWrapper.lambda().like(SkyChartParamConfig::getModel, skyChartParamConfigPageParam.getModel());
        }
        if(ObjectUtil.isNotEmpty(skyChartParamConfigPageParam.getType())) {
            queryWrapper.lambda().eq(SkyChartParamConfig::getType, skyChartParamConfigPageParam.getType());
        }
        queryWrapper.lambda().orderByDesc(SkyChartParamConfig::getCreateTime);
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyChartParamConfigAddParam skyChartParamConfigAddParam) {
        List<SkyChartParamConfig> list = this.list(new QueryWrapper<SkyChartParamConfig>().lambda().eq(SkyChartParamConfig::getSemId, skyChartParamConfigAddParam.getSemId()));
        if (!CollectionUtils.isEmpty(list)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DEVICE_TYPE_EXIST_9117,skyChartParamConfigAddParam.getModel());
        }
        SkyChartParamConfig skyChartParamConfig = BeanUtil.toBean(skyChartParamConfigAddParam, SkyChartParamConfig.class);
        this.save(skyChartParamConfig);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyChartParamConfigEditParam skyChartParamConfigEditParam) {
        SkyChartParamConfig skyChartParamConfig = skyChartParamConfigMapper.selectOne(new QueryWrapper<SkyChartParamConfig>()
                .lambda().eq(SkyChartParamConfig::getSemId, skyChartParamConfigEditParam.getSemId()));
        BeanUtil.copyProperties(skyChartParamConfigEditParam, skyChartParamConfig);
        this.updateById(skyChartParamConfig);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(SkyChartParamConfigIdParam skyChartParamConfigIdParam) {
        // 逻辑删除
//        this.removeByIds(CollStreamUtil.toList(skyChartParamConfigIdParamList, SkyChartParamConfigIdParam::getDmcId));
        // 前端传的是id，存入了semid里面了
        SkyChartParamConfig entity = this.queryEntity(skyChartParamConfigIdParam.getSemId());
        if (entity != null) {
            skyChartParamConfigMapper.deleteById(entity);
        }
    }

    @Override
    public SkyChartParamConfig detail(SkyChartParamConfigIdParam skyChartParamConfigIdParam) {
        return skyChartParamConfigMapper.selectOne(new QueryWrapper<SkyChartParamConfig>().lambda().eq(SkyChartParamConfig::getSemId, skyChartParamConfigIdParam.getSemId()));
    }

    @Override
    public SkyChartParamConfig queryEntity(String id) {
        SkyChartParamConfig skyChartParamConfig = this.getById(id);
        if(ObjectUtil.isEmpty(skyChartParamConfig)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        return skyChartParamConfig;
    }

}
