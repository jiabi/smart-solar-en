package vip.xiaonuo.biz.modular.stationinfo.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/20 19:17
 */
@Getter
@Setter
public class PsDeviceInfoVO {
    /** 逆变器SN */
    private String invSn;

    /** 设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪 */
    private Integer equType;

    /** 设备名称 */
    private String invName;

    /** 状态（1-正常 2-故障) */
    private Integer monStatus;

    /** 品牌名称 */
    private String companyId;

    /** 数据更新时间 */
    private String monitorTime;

    /**  时区 **/
    private String timeZone;

    /**  最後更新時間 **/
    private String currentUpdateTime;
}
