/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationuser.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.param.SkyStationUserAddParam;
import vip.xiaonuo.biz.modular.stationuser.param.SkyStationUserEditParam;
import vip.xiaonuo.biz.modular.stationuser.param.SkyStationUserIdParam;
import vip.xiaonuo.biz.modular.stationuser.param.SkyStationUserPageParam;
import vip.xiaonuo.biz.modular.stationuser.service.SkyStationUserService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 电站用户关系控制器
 *
 * @author 全佳璧
 * @date  2024/04/01 16:32
 */
@Api(tags = "电站用户关系控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyStationUserController {

    @Resource
    private SkyStationUserService skyStationUserService;

    /**
     * 获取电站用户关系分页
     *
     * @author 全佳璧
     * @date  2024/04/01 16:32
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取电站用户关系分页")
    @SaCheckPermission("/biz/stationuser/page")
    @GetMapping("/biz/stationuser/page")
    public CommonResult<Page<SkyStationUser>> page(SkyStationUserPageParam skyStationUserPageParam) {
        return CommonResult.data(skyStationUserService.page(skyStationUserPageParam));
    }

    /**
     * 添加电站用户关系
     *
     * @author 全佳璧
     * @date  2024/04/01 16:32
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加电站用户关系")
    @CommonLog("添加电站用户关系")
    @SaCheckPermission("/biz/stationuser/add")
    @PostMapping("/biz/stationuser/add")
    public CommonResult<String> add(@RequestBody @Valid SkyStationUserAddParam skyStationUserAddParam) {
        skyStationUserService.add(skyStationUserAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑电站用户关系
     *
     * @author 全佳璧
     * @date  2024/04/01 16:32
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑电站用户关系")
    @CommonLog("编辑电站用户关系")
    @SaCheckPermission("/biz/stationuser/edit")
    @PostMapping("/biz/stationuser/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyStationUserEditParam skyStationUserEditParam) {
        skyStationUserService.edit(skyStationUserEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除电站用户关系
     *
     * @author 全佳璧
     * @date  2024/04/01 16:32
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除电站用户关系")
    @CommonLog("删除电站用户关系")
    @SaCheckPermission("/biz/stationuser/delete")
    @PostMapping("/biz/stationuser/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyStationUserIdParam> skyStationUserIdParamList) {
        skyStationUserService.delete(skyStationUserIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取电站用户关系详情
     *
     * @author 全佳璧
     * @date  2024/04/01 16:32
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取电站用户关系详情")
    @SaCheckPermission("/biz/stationuser/detail")
    @GetMapping("/biz/stationuser/detail")
    public CommonResult<SkyStationUser> detail(@Valid SkyStationUserIdParam skyStationUserIdParam) {
        return CommonResult.data(skyStationUserService.detail(skyStationUserIdParam));
    }

}
