package vip.xiaonuo.biz.modular.stationinfo.param.equchart;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/6 17:28
 */
@Setter
@Getter
public class PsEquDayChartResParam {

    /**
     * 时间轴
     */
    private List<String> timeList;

    /**
     * 明细
     */
    private List<DayChartAnalysis> list;


    @Setter
    @Getter
    public static  class DayChartAnalysis<T>{

        /**
         * 名称-（mppt电压1 （V））
         */
        private String name;

        /**
         * key="u_pv1"
         */
        private String key;

        /**
         * 数据点
         */
        private List<T>  dataList;

        public DayChartAnalysis(String name, String key, List<T> dataList) {
            this.name = name;
            this.key = key;
            this.dataList = dataList;
        }
    }
}
