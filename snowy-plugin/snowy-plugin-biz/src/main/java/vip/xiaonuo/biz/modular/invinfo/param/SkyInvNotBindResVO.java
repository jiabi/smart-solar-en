package vip.xiaonuo.biz.modular.invinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/11 17:26
 */
@Getter
@Setter
public class SkyInvNotBindResVO {
    /** 逆变器SN */
    private String invSn;

    /** 设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪 */
    private Integer equType;

    /** 逆变器名称 */
    private String invName;

    /** 运行状态（1-正常 2-异常） */
    private Integer monStatus;

    /** 厂商id */
    private String companyId;

    /** 数据更新时间 */
    private String monitorTime;

    /** 设备型号名称 */
    private String modelName;
}
