package vip.xiaonuo.biz.modular.filemanager.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vip.xiaonuo.biz.modular.filemanager.param.AutoTestParam;
import vip.xiaonuo.biz.modular.filemanager.param.FileUploadResVO;
import vip.xiaonuo.biz.modular.filemanager.service.FileManagerService;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.dev.api.DevFileApi;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/4/26 9:57
 */

@Api(tags = "文件管理控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Slf4j
public class FileManagerController {

    @Resource
    private DevFileApi devFileApi;

    @Resource
    private FileManagerService fileManagerService;

    /**
     * 本地文件上传，返回文件Url
     *
     * @author xuyuxiang
     * @date 2021/10/13 14:01
     **/
    @ApiOperationSupport(order = 1)
    @ApiOperation("上传本地文件返回url")
    @CommonLog("上传本地文件返回url")
    @PostMapping("/biz/file/storageFileWithReturnUrlLocal")
    public CommonResult<String> storageFileWithReturnUrlLocal(@RequestPart("file") MultipartFile file) {
        return CommonResult.data(devFileApi.storageFileWithReturnUrlLocal(file));
    }

    /**
     * 上传pic返回url
     *
     * @author xuyuxiang
     * @date 2021/10/13 14:01
     **/
    @ApiOperationSupport(order = 2)
    @ApiOperation("上传pic返回url")
    @CommonLog("上传pic返回url")
    @PostMapping("/biz/file/storagePicReturnUrlAliyun")
    public CommonResult<String> storagePicReturnUrlAliyun(@RequestPart("file") MultipartFile file) {
        return CommonResult.data(devFileApi.storageFileWithReturnUrlAliyunBySort(file, "pic",null));
    }

    /**
     * 上传升级包返回url
     *
     * @author xuyuxiang
     * @date 2021/10/13 14:01
     **/
    @ApiOperationSupport(order = 3)
    @ApiOperation("上传升级包返回url")
    @CommonLog("上传升级包返回url")
    @PostMapping("/biz/file/storageBinReturnUrlAliyun")
    public CommonResult<FileUploadResVO> storageBinReturnUrlAliyun(@RequestPart("file") MultipartFile file) {
        String downloadUrl = devFileApi.storageFileWithReturnUrlAliyunBySort(file, "bin", null);
        long fileSize = file.getSize() * 8;
        FileUploadResVO fileInfo = new FileUploadResVO();
        fileInfo.setFileSize((int) fileSize);
        fileInfo.setDownloadUrl(downloadUrl);
        return CommonResult.data(fileInfo);
    }

    /**
     * 导入导出模版上传，返回文件Url
     *
     * @author xuyuxiang
     * @date 2021/10/13 14:01
     **/
    @ApiOperationSupport(order = 4)
    @ApiOperation("导入导出模版上传，返回文件Url")
    @CommonLog("导入导出模版上传，返回文件Url")
    @PostMapping("/biz/file/storageTemplateReturnUrlAliyun")
    public CommonResult<String> storageTemplateReturnUrlAliyun(@RequestPart("file") MultipartFile file) {
        return CommonResult.data(devFileApi.storageFileWithReturnUrlAliyunBySort(file, "template",null));
    }

    /**
     * 未知分类文件上传，返回文件Url
     *
     * @author xuyuxiang
     * @date 2021/10/13 14:01
     **/
    @ApiOperationSupport(order = 5)
    @ApiOperation("未知分类文件上传，返回文件Url")
    @CommonLog("未知分类文件上传，返回文件Url")
    @PostMapping("/biz/file/storageFileReturnUrlAliyun")
    public CommonResult<String> storageFileReturnUrlAliyun(@RequestPart("file") MultipartFile file) {
        return CommonResult.data(devFileApi.storageFileWithReturnUrlAliyunBySort(file, null,null));
    }

    /**
     * 设备自检生成pdf
     *
     * @author xuyuxiang
     * @date 2021/10/13 14:01
     **/
    @ApiOperationSupport(order = 6)
    @ApiOperation("设备自检生成pdf")
    @CommonLog("设备自检生成pdf")
    @PostMapping("/biz/file/autoTestPdf")
    public CommonResult<String> autoTestPdf(@RequestBody List<AutoTestParam> list) throws Exception {
        return CommonResult.data(fileManagerService.autoTestPdf(list));
    }

}
