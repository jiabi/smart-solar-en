package vip.xiaonuo.biz.modular.collectorupgrade.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @Author wangjian
 * @Date 2024/5/9 10:14
 */
@Getter
@Setter
public class SkyCollectorInvSnParam {
    /** 设备SN */
    @ApiModelProperty(value = "设备SN", position = 1)
    private String equSn;

    /** 采集器sn **/
    @ApiModelProperty(value = "采集器sn", position = 2)
    private String collectorSn;

}
