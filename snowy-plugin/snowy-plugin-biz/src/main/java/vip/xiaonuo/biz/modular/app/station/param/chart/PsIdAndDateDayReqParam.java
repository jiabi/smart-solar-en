package vip.xiaonuo.biz.modular.app.station.param.chart;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @Author wangjian
 * @Date 2024/2/28 17:43
 */

@Getter
@Setter
public class PsIdAndDateDayReqParam {
    /** 电站ID */
    private String guid;

    /**
     * 日 yyyy-MM-dd
     * 月 yyyy-MM
     * 年 yyyy
     * 总 不用传
     */
    private String localDate;
}
