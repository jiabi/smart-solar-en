package vip.xiaonuo.biz.modular.app.station.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import vip.xiaonuo.biz.core.enums.DateTypeEnum;
import vip.xiaonuo.biz.core.util.ChartUtil;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.app.station.mapper.AppPowerChartMapper;
import vip.xiaonuo.biz.modular.app.station.param.chart.AppPsPowerChartParam;
import vip.xiaonuo.biz.modular.app.station.param.chart.AppPsPowerChartVO;
import vip.xiaonuo.biz.modular.app.station.param.chart.PsPowerDayChartResParam;
import vip.xiaonuo.biz.modular.app.station.service.AppStationChartService;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;
import vip.xiaonuo.biz.modular.invmonitorcurrent.mapper.SkyInvMonitorCurrentMapper;
import vip.xiaonuo.biz.modular.invpower.mapper.SkyInvPowerMapper;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationPower;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.PsEquChartResVO;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.PsEquDayChartResParam;
import vip.xiaonuo.biz.modular.stationpower.mapper.SkyStationPowerMapper;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/3/12 9:26
 */
@Service
public class AppStationChartServiceImpl implements AppStationChartService {
    @Resource
    private SkyStationPowerMapper skyStationPowerMapper;

    @Resource
    private SkyInvMonitorCurrentMapper skyInvMonitorCurrentMapper;

    @Resource
    private SkyInvPowerMapper skyInvPowerMapper;

    @Resource
    private AppPowerChartMapper appPowerChartMapper;


    /**
     * app总览-发电量统计
     * @param date
     * @param type
     * @param companyCode
     * @return
     */
    @Override
    public AppPsPowerChartVO power(String date, Integer type, String companyCode) {
        if (DateTypeEnum.DAY.getType().equals(type)) {
            //日统计
            return this.dayPowerChart(date,companyCode);
        } else if (DateTypeEnum.MONTH.getType().equals(type)) {
            //月统计
            return this.monthPowerChart(date,companyCode);
        } else if (DateTypeEnum.YEAR.getType().equals(type)) {
            //年统计
            return this.yearPowerChart(date,companyCode);
        } else if (DateTypeEnum.ALL.getType().equals(type)) {
            //总统计
            return this.totalPowerChart(companyCode);
        }
        return null;
    }

    @Override
    public String measurePoint(String invSn) {
        return appPowerChartMapper.measurePoint(invSn);
    }

    /**
     * app总览-发电量统计-日数据
     * @param date
     * @param companyCode
     * @return
     */
    private AppPsPowerChartVO dayPowerChart(String date, String companyCode) {
        AppPsPowerChartVO resVO = new AppPsPowerChartVO();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateLD = LocalDate.parse(date, formatter);
        List<PsPowerDayChartResParam> currents;
        if (dateLD.getMonth().compareTo(LocalDate.now().getMonth()) != 0 ) {
            // 非当月的
            DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyyMM");
            currents = appPowerChartMapper.dayPowerChart(dateLD.format(formatter2), companyCode, date);
        } else {
            // 当月的数据直接实时表
            currents = appPowerChartMapper.dayPowerCurrentChart(date,companyCode);
        }
        if (CollectionUtils.isEmpty(currents)) {
            return null;
        }
        // 去重
        List<PsPowerDayChartResParam> dayDataList = currents.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(PsPowerDayChartResParam::getDateTime))), ArrayList::new));
        Map<String, Double> dayDataMap = dayDataList.stream().collect(Collectors.toMap(PsPowerDayChartResParam::getDateTime, PsPowerDayChartResParam::getPower, (v1, v2) -> v1));
        //时间轴-当时是分秒
        List<String> timeList = this.getDateTimeList(date);
        List<Double> power = new ArrayList<>();
        for (String str:timeList) {
            power.add(dayDataMap.getOrDefault(str,0D));
        }
        resVO.setDateList(timeList);
        resVO.setPower(power);
        return resVO;
    }

    private AppPsPowerChartVO monthPowerChart(String date, String companyCode) {
        AppPsPowerChartVO resVO = new AppPsPowerChartVO();
        List<AppPsPowerChartParam> powerList = appPowerChartMapper.powerMonthChart(date,companyCode);
        if (CollectionUtils.isEmpty(powerList)) {
            return null;
        }
        List<String> dateList = new ArrayList<>();
        List<Double> resPowerList = new ArrayList<>();
        //当月多少日
        List<LocalDate> localDateList = this.getLocalDateList(LocalDateUtils.parseLocalDate(date + "-01", LocalDateUtils.DATE_FORMATTER));
        Map<String, Double> dayDataMap = powerList.stream().collect(Collectors.toMap(AppPsPowerChartParam::getDateTime, AppPsPowerChartParam::getPower, (v1, v2) -> v1));
        for (LocalDate localDate:localDateList) {
            String dateStr = LocalDateUtils.formatLocalDate(localDate, LocalDateUtils.DATE_FORMATTER);
            dateList.add(dateStr);
            resPowerList.add(dayDataMap.getOrDefault(dateStr,0D));
        }
        resVO.setPower(resPowerList);
        resVO.setDateList(dateList);
        return resVO;
    }

    private AppPsPowerChartVO yearPowerChart(String date, String companyCode) {
        AppPsPowerChartVO resVO = new AppPsPowerChartVO();
        List<AppPsPowerChartParam> powerList = appPowerChartMapper.powerYearChart(date,companyCode);
        if (CollectionUtils.isEmpty(powerList)) {
            return null;
        }
        List<String> dateList = new ArrayList<>();
        List<Double> resPowerList = new ArrayList<>();
        //当年多少月
        List<String> localDateList = this.getMonthList(date);
        Map<String, Double> dayDataMap = powerList.stream().collect(Collectors.toMap(AppPsPowerChartParam::getDateTime, AppPsPowerChartParam::getPower, (v1, v2) -> v1));
        for (String localDate:localDateList) {
            dateList.add(localDate);
            resPowerList.add(dayDataMap.getOrDefault(localDate,0D));
        }
        resVO.setPower(resPowerList);
        resVO.setDateList(dateList);
        return resVO;
    }

    private AppPsPowerChartVO totalPowerChart(String companyCode) {
        AppPsPowerChartVO resVO = new AppPsPowerChartVO();
        List<AppPsPowerChartParam> powerList = appPowerChartMapper.powerTotalChart(companyCode);
        if (CollectionUtils.isEmpty(powerList)) {
            return null;
        }
        List<String> dateList = new ArrayList<>();
        List<Double> resPowerList = new ArrayList<>();
        for (AppPsPowerChartParam model:powerList) {
            dateList.add(model.getDateTime());
            resPowerList.add(model.getPower());
        }
        resVO.setPower(resPowerList);
        resVO.setDateList(dateList);
        return resVO;
    }

    /**
     * 逆变器明细转换To DayChartAnalysis
     *
     * @param paramsList
     * @return
     */
    private List<PsEquDayChartResParam.DayChartAnalysis> convertToDayChartAnalysis(List<SkyInvMonitorCurrent> dayDataList, List<String> paramsList, String invType) {
        Map<String,List<Object>> doubleMap = ChartUtil.getPowerStationRecordValues(dayDataList,paramsList, invType);
        List<PsEquDayChartResParam.DayChartAnalysis> analysisList = new ArrayList<>(doubleMap.size());
        for (Map.Entry<String, List<Object>> stringListEntry : doubleMap.entrySet()) {
            String key = stringListEntry.getKey();
            List<Object> value = stringListEntry.getValue();
            analysisList.add(new PsEquDayChartResParam.DayChartAnalysis(key,null,value));
        }
        return analysisList;
    }

    /**
     * 获取当日当所有时间5分钟间隔
     *
     * @param date
     * @return
     */
    private List<String> getDateTimeList(String date) {
        LocalDate localDate = LocalDate.parse(date);
        List<String> timeList = new ArrayList<>(216);
        for (int h = 4; h < 22; h++) {
            //每隔5分钟1个点，每小时共12各点
            for (int m = 0; m < 60; m += 5) {
                timeList.add(LocalDateUtils.formatLocalDateTime(LocalDateTime.of(localDate, LocalTime.of(h, m, 0)), LocalDateUtils.DATETIME_FORMATTER));
            }
        }
        return timeList;
    }

    /**
     * 获取当月所有日
     *
     * @param date
     * @return
     */
    private List<LocalDate> getLocalDateList(LocalDate date) {
        int year = date.getYear();
        int month = date.getMonthValue();
        int daysInMonth = date.lengthOfMonth();
        List<LocalDate> dates = new ArrayList<>();
        for (int day = 1; day <= daysInMonth; day++) {
            LocalDate currentDate = LocalDate.of(year, month, day);
            dates.add(currentDate);
        }
        return dates;
    }


    /**
     * 获取当年所有月
     *
     * @param year
     * @return
     */
    private List<String> getMonthList(String year) {
        List<String> monthList = new ArrayList<>(12);
        for (int i = 1; i <= 12; i++) {
            monthList.add(String.format("%s-%02d", year, i));
        }
        return monthList;
    }
}
