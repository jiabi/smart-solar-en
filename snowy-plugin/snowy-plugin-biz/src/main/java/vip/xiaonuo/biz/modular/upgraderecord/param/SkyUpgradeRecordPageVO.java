package vip.xiaonuo.biz.modular.upgraderecord.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2024/4/22 14:46
 */

@Getter
@Setter
public class SkyUpgradeRecordPageVO {
    /** ID */
    @ApiModelProperty(value = "ID", position = 1)
    private String id;

    /** 采集器SN */
    @ApiModelProperty(value = "采集器SN", position = 2)
    private String collectSn;

    /** 设备SN */
    @ApiModelProperty(value = "设备SN", position = 3)
    private String equSn;

    /** 电站ID */
    @ApiModelProperty(value = "电站ID", position = 4)
    private String stationId;

    /** 升级模块 */
    @ApiModelProperty(value = "升级模块", position = 5)
    private Integer upgradeModule;

    /** 升级包名 */
    @ApiModelProperty(value = "升级包名", position = 6)
    private String upgradeBin;

    /** 升级状态 */
    @ApiModelProperty(value = "升级状态", position = 7)
    private Integer upgradeStatus;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 8)
    private Date createTime;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 9)
    private Date updateTime;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称", position = 10)
    private String stationName;

    /** 升级模块名称 */
    @ApiModelProperty(value = "升级模块名称", position = 11)
    private String upgradeModuleName;

    @ApiModelProperty(value = "时区")
    private String timeZone;

}
