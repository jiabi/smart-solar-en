package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2023/9/19 15:04
 **/
@Getter
@Setter
public class StationStatusVO {
    /** 总数量 */
    @ApiModelProperty(value = "总数量")
    private Integer total;

    /** 接入中 0*/
    @ApiModelProperty(value = "接入中")
    private Integer accessing;

    /** 通讯正常 1*/
    @ApiModelProperty(value = "通讯正常")
    private Integer normal;

    /** 部分离线 2*/
    @ApiModelProperty(value = "部分离线")
    private Integer partialOffline;

    /** 全部设备离线 3*/
    @ApiModelProperty(value = "全部设备离线")
    private Integer allOffline;

    /** 无警报 0*/
    @ApiModelProperty(value = "无警报")
    private Integer noAlarm;

    /** 有警报 1*/
    @ApiModelProperty(value = "有警报")
    private Integer alarm;

}
