package vip.xiaonuo.biz.modular.invmonitorcurrent.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description:
 * @Author: wangjian
 * @CreateTime: 2023-11-17  11:26
 */
@Setter
@Getter
public class SkyInvInfoRankVO {

    /** 逆变器名称 */
    @ApiModelProperty(value = "逆变器名称")
    private String invName;

    /** 当日发电量 */
    @ApiModelProperty(value = "当日发电量")
    private String monToday;

    /** 累计发电量 */
    @ApiModelProperty(value = "累计发电量")
    private String monTotal;
}