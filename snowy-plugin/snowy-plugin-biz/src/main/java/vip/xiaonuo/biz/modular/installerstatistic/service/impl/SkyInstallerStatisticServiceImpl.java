/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.installerstatistic.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.installerstatistic.task.InstallerStatisticTimerTaskRunner;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.service.SkyStationUserService;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.installerstatistic.entity.SkyInstallerStatistic;
import vip.xiaonuo.biz.modular.installerstatistic.mapper.SkyInstallerStatisticMapper;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticAddParam;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticEditParam;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticIdParam;
import vip.xiaonuo.biz.modular.installerstatistic.param.SkyInstallerStatisticPageParam;
import vip.xiaonuo.biz.modular.installerstatistic.service.SkyInstallerStatisticService;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 安装商数据统计Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/03/13 09:19
 **/
@Service
public class SkyInstallerStatisticServiceImpl extends ServiceImpl<SkyInstallerStatisticMapper, SkyInstallerStatistic> implements SkyInstallerStatisticService {

    @Resource
    private InstallerStatisticTimerTaskRunner installerStatisticTimerTaskRunner;

    @Resource
    private SkyStationUserService skyStationUserService;

    @Override
    public Page<SkyInstallerStatistic> page(SkyInstallerStatisticPageParam skyInstallerStatisticPageParam) {
        QueryWrapper<SkyInstallerStatistic> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isAllNotEmpty(skyInstallerStatisticPageParam.getSortField(), skyInstallerStatisticPageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(skyInstallerStatisticPageParam.getSortOrder());
            queryWrapper.orderBy(true, skyInstallerStatisticPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(skyInstallerStatisticPageParam.getSortField()));
        } else {
            queryWrapper.lambda().orderByAsc(SkyInstallerStatistic::getInstallerId);
        }
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyInstallerStatisticAddParam skyInstallerStatisticAddParam) {
        SkyInstallerStatistic skyInstallerStatistic = BeanUtil.toBean(skyInstallerStatisticAddParam, SkyInstallerStatistic.class);
        this.save(skyInstallerStatistic);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyInstallerStatisticEditParam skyInstallerStatisticEditParam) {
        SkyInstallerStatistic skyInstallerStatistic = this.queryEntity(skyInstallerStatisticEditParam.getInstallerId());
        BeanUtil.copyProperties(skyInstallerStatisticEditParam, skyInstallerStatistic);
        this.updateById(skyInstallerStatistic);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyInstallerStatisticIdParam> skyInstallerStatisticIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyInstallerStatisticIdParamList, SkyInstallerStatisticIdParam::getInstallerId));
    }

    @Override
    public SkyInstallerStatistic detail(SkyInstallerStatisticIdParam skyInstallerStatisticIdParam) {
        return this.queryEntity(skyInstallerStatisticIdParam.getInstallerId());
    }

    @Override
    public SkyInstallerStatistic queryEntity(String id) {
        SkyInstallerStatistic skyInstallerStatistic = this.getById(id);
        if(ObjectUtil.isEmpty(skyInstallerStatistic)) {
            return null;
        }
        return skyInstallerStatistic;
    }

    @Override
    public void recountStatisticById(String id) {
        SkyStationUser byId = skyStationUserService.getById(id);
        if (byId == null){
            return;
        }
        List<SkyStationUser> stationUsers = Collections.singletonList(byId);

        // 分销商数据统计
        Map<String, List<SkyStationUser>> fenxiaoshangMap = stationUsers.stream().filter(a -> a.getInstallerIdL2() != null ).collect(Collectors.groupingBy(SkyStationUser::getInstallerIdL2));
        if (!CollectionUtils.isEmpty(fenxiaoshangMap)) {
            installerStatisticTimerTaskRunner.stationStatistic(fenxiaoshangMap);
        }

        // 安装商数据统计
        Map<String, List<SkyStationUser>> installerMap = stationUsers.stream().filter(a -> a.getInstallerIdL1() != null ).collect(Collectors.groupingBy(SkyStationUser::getInstallerIdL1));
        if (!CollectionUtils.isEmpty(installerMap)) {
            installerStatisticTimerTaskRunner.stationStatistic(installerMap);
        }

        // 用户数据统计
        Map<String, List<SkyStationUser>> userMap = stationUsers.stream().filter(a -> a.getUserId() != null ).collect(Collectors.groupingBy(SkyStationUser::getUserId));
        if (!CollectionUtils.isEmpty(userMap)) {
            installerStatisticTimerTaskRunner.stationStatistic(userMap);
        }
    }

}
