package vip.xiaonuo.biz.core.util.objectcopier;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @Author wangjian
 * @Date 2024/3/20 18:14
 */
public abstract class AbstractDTO implements Serializable {

    private static final long serialVersionUID = -1625282648335813299L;

    @Override
    public String toString() {
        return StringEscapeUtils.unescapeJava(ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE));
    }
}
