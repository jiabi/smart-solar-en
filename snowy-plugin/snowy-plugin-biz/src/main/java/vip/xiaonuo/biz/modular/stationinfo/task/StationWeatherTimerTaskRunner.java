package vip.xiaonuo.biz.modular.stationinfo.task;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.HttpUtils;
import vip.xiaonuo.biz.modular.stationinfo.entity.Area;
import vip.xiaonuo.biz.modular.stationinfo.entity.SkyWeatherRecord;
import vip.xiaonuo.biz.modular.stationinfo.mapper.AreaMapper;
import vip.xiaonuo.biz.modular.stationinfo.mapper.WeatherRecordMapper;
import vip.xiaonuo.biz.modular.stationinfo.param.weathertimetask.Daily;
import vip.xiaonuo.biz.modular.stationinfo.param.weathertimetask.WeatherDailyDataResponse;
import vip.xiaonuo.biz.modular.stationinfo.param.weathertimetask.WeatherProperties;
import vip.xiaonuo.common.timer.CommonTimerTaskRunner;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author wangjian
 * @Date 2024/5/14 15:53
 */
@Slf4j
@Component
public class StationWeatherTimerTaskRunner implements CommonTimerTaskRunner {

    @Resource
    private AreaMapper areaMapper;

    @Resource
    private WeatherProperties weatherProperties;

    @Resource
    private WeatherRecordMapper weatherRecordMapper;

    @Override
    public void action() {
        List<Area> areaList = areaMapper.selectList(new QueryWrapper<Area>().lambda().isNotNull(Area::getAreaLat).isNotNull(Area::getAreaLng)
                .groupBy(Area::getAreaCity));
        for (Area area:areaList) {
            if (ObjectUtil.isEmpty(area) || ObjectUtil.isEmpty(area.getAreaLat()) || ObjectUtil.isEmpty(area.getAreaLng())){
                continue;
            }
            try {
                weather(area);
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void weather(Area area) throws UnsupportedEncodingException {
        SkyWeatherRecord weatherRecord = new SkyWeatherRecord();
        weatherRecord.setAreaId(area.getId());
        weatherRecord.setAreaLng(area.getAreaLng());
        weatherRecord.setAreaLat(area.getAreaLat());

        String host = weatherProperties.getWeatherHost();
        // path = "/v2.6/k6ZYYsFzqlHOkWKF/120.704639,31.303018/daily?dailysteps=1";   String path = "/v2.6/k6ZYYsFzqlHOkWKF/120.704639,31.303018/weather?alert=true&dailysteps=1&hourlysteps=24"; // true
//        String path = weatherProperties.getPathVer() + weatherProperties.getPathToken() + StrUtil.SLASH + area.getAreaLng() + StrUtil.COMMA + area.getAreaLat() + weatherProperties.getPathDailyMethod();
        String path =  weatherProperties.getPathSevenDailyMethod() + area.getAreaLng() + "," + area.getAreaLat() ;
        String method = "GET";
        String appcode = weatherProperties.getAppcode();
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        //需要给X-Ca-Nonce的值生成随机字符串，每次请求不能相同
        headers.put("X-Ca-Nonce", UUID.randomUUID().toString());
        Map<String, String> querys = new HashMap<String, String>();
        String responseStr = null;
        WeatherDailyDataResponse dailyDataResponse = null;
        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            System.out.println("彩云天气—请求参数path：" + path);
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            //获取response的body
            responseStr = EntityUtils.toString(response.getEntity());
            System.out.println("Response：" + responseStr);
            dailyDataResponse = JSON.parseObject(responseStr, WeatherDailyDataResponse.class);
            System.out.println("彩云天气—返回信息: " + JSON.toJSONString(dailyDataResponse));

            Daily daily = dailyDataResponse.getResult().getDaily();
            // 白天地表 2 米气温
            List<Daily.Temperature> temperature08h20h = daily.getTemperature_08h_20h();
            // 地表 2 米相对湿度(%)
            List<Daily.CommonData> humidity = daily.getHumidity();
            // 白天地表 10 米风速
            List<Daily.Wind> wind08h20h = daily.getWind_08h_20h();
            // 向下短波辐射通量(W/M2)

            List<Daily.CommonData> dswrf = daily.getDswrf();
            // 白天主要天气现象
            List<Daily.Skycon> skycon = daily.getSkycon();
            // 日出日落时间
            List<Daily.Astro> astro = daily.getAstro();
            weatherRecord.setDate(astro.get(0).getDate());
            weatherRecord.setTemperatureMax(temperature08h20h.get(0).getMax());
            weatherRecord.setTemperatureMin(temperature08h20h.get(0).getMin());
            weatherRecord.setTemperatureAvg(temperature08h20h.get(0).getAvg());
            weatherRecord.setHumidityMax(humidity.get(0).getMax());
            weatherRecord.setHumidityMin(humidity.get(0).getMin());
            weatherRecord.setHumidityAvg(humidity.get(0).getAvg());
            weatherRecord.setWindMax(wind08h20h.get(0).getMax().getSpeed());
            weatherRecord.setWindMin(wind08h20h.get(0).getMin().getSpeed());
            weatherRecord.setWindAvg(wind08h20h.get(0).getAvg().getSpeed());
            weatherRecord.setDswrfMax(dswrf.get(0).getMax());
            weatherRecord.setDswrfMin(dswrf.get(0).getMin());
            weatherRecord.setDswrfAvg(dswrf.get(0).getAvg());
            weatherRecord.setSkycon(skycon.get(0).getValue());
            weatherRecord.setSunrise(astro.get(0).getSunrise().getTime());
            weatherRecord.setSunset(astro.get(0).getSunset().getTime());
            weatherRecordMapper.insert(weatherRecord);
        } catch (Exception e) {
            if (dailyDataResponse != null && dailyDataResponse.getTzShift() == 0) {
                throw new SkyCommonException(SkyCommonExceptionEnum.WEATHER_API_CONNECT_NUM_USED_9139);
            }
            e.printStackTrace();
        }
    }
}
