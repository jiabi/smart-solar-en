package vip.xiaonuo.biz.modular.equrelationship.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class SkyEquRelationshipSNParam {
    /** 设备SN */
    @ApiModelProperty(value = "设备SN", required = true)
    @NotBlank(message = "equSn不能为空")
    private String equSn;
}
