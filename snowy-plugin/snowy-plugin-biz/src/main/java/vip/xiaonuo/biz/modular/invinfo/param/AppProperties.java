package vip.xiaonuo.biz.modular.invinfo.param;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author wangjian
 * @Date 2024/3/9 14:19
 */

@Data
@Component
@ConfigurationProperties(value = "app")
public class AppProperties {

    /**
     * 上传路径
     */
    private String uploadPath = "";

    /**
     * 下载路径
     */
    private String downloadPath = "";

    /**
     * 文件类型
     */
    private String[] fileTypeArray;

    /**
     * 文件大小
     */
    private int maxFileSize;

}
