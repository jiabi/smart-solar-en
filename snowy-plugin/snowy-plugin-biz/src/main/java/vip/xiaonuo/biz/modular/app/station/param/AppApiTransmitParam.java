package vip.xiaonuo.biz.modular.app.station.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/5/31 16:32
 */

@Getter
@Setter
public class AppApiTransmitParam {
    private String output;
    private String keywords;
    private String key;
    private String datatype;
}
