/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationinfo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.collectorinfo.param.SkyCollectorInfoPageResVO;
import vip.xiaonuo.biz.modular.homepage.param.SkyLoginUserIdParam;
import vip.xiaonuo.biz.modular.homepage.param.StationDistributeVO;
import vip.xiaonuo.biz.modular.homepage.param.StationHomePagePowerVO;
import vip.xiaonuo.biz.modular.homepage.param.StationPowerCountVO;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.param.*;

import java.util.List;

/**
 * 电站信息Mapper接口
 *
 * @author 全佳璧
 * @date  2023/09/12 10:51
 **/
public interface StationInfoMapper extends BaseMapper<StationInfo> {

    @MapKey("stationStatus")
    List<StatusCountParam> getStatusCount(@Param(Constants.WRAPPER) QueryWrapper<StationInfo> clone);

    List<StatusCountParam> getAlarmCount(@Param(Constants.WRAPPER) QueryWrapper<StationInfo> clone);

    List<StationInfoPageModel> pageStationInfo(StationInfoPageParam stationInfoPageParam);

    Integer countNum(StationInfoPageParam stationInfoPageParam);

    /**
     * 所有电站分布
     * @return
     */
    List<StationDistributeVO> distribute(SkyLoginUserIdParam param);

    /**
     * 所有电站发电信息
     * @param date
     * @param month
     * @param year
     * @return
     */
    List<StationPowerCountVO> allPower(@Param("date") String date, @Param("month") String month, @Param("year") String year, @Param("reqVO") SkyLoginUserIdParam param);

    /**
     * 首页获取所有电站基础信息
     */
    StationHomePagePowerVO getAllPsHomePageBaseInfo(SkyLoginUserIdParam param);

    /**
     * 获取数据更新时间
     * @return
     */
    String getUpdateTime();

    void userChange(@Param("psGuids") List<Long> psIds, @Param("stationCompanyId") String stationCompanyId);

    Integer getPsDeviceStatusCount(@Param("stationId") String stationId, @Param("offlineTime") String offlineTime);

    Integer alarmStatus(@Param("stationId") String stationId);

    List<PsIdDeviceCountParam> alarmStatusByPsIds(@Param("psId") List<String> ids);

    List<StationInfoExportVO> exportStationInfoList(StationInfoExportParam exportParam);

    List<StationInfoExportVO> exportStationInfoListByIds(@Param("psId") List<String> psIds);

    List<PsIdDeviceOfflineAndPowerParam> getPsDeviceStatusCountBatch(@Param("psId") List<String> ids, @Param("offlineTime") String offlineTime);

    List<StationInfo> getStatusPsList(StationInfoPageParam stationInfoPageParam);

    void plusOneHour(@Param("psId") List<String> ids );

    List<String> getGermanyStation();

    void miusOneHour(@Param("psId") List<String> ids );

    List<SkyCollectorInfoPageResVO> selectByStationId(@Param("stationId") String stationId);
}
