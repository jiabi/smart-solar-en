package vip.xiaonuo.biz.modular.app.station.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import vip.xiaonuo.biz.modular.app.entity.PageResDTO;
import vip.xiaonuo.biz.modular.app.inv.param.AppStationInfoOverviewPowerVO;
import vip.xiaonuo.biz.modular.app.station.param.*;
import vip.xiaonuo.biz.modular.stationinfo.param.*;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/11 14:50
 */
public interface AppStationInfoService {
    AppStationInfoOverviewVO overviewInfo(String stationCompany);

    PageResDTO<AppStationInfoPageResVO> page(StationInfoPageParam stationInfoPageParam);

    void add(StationInfoAddParam stationInfoAddParam);

    AppStationInfoOverviewPowerVO power(StationInfoIdParam stationInfoIdParam);

    List<UserInfoParam> getAllInstaller();

    List<UserInfoParam> getAllUser();

    AppStationInfoEnergySaveVO getPsEnergySave(StationInfoIdParam stationInfoIdParam);

    String inputtips(AppApiTransmitParam param);

    void update(AppStationInfoUpdateReq updateReq);

    void delete(StationInfoIdParam stationInfoIdParam);
}
