package vip.xiaonuo.biz.modular.homepage.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.homepage.param.*;
import vip.xiaonuo.biz.modular.homepage.service.HomePageService;
import vip.xiaonuo.biz.modular.stationinfo.param.StationStatusVO;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @Author wangjian
 * @Date 2024/2/21 17:48
 */
@Api(tags = "首页控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class HomePageController {

    @Resource
    private HomePageService homePageService;

    /**
     * @description: 获取电站离线和警报状态数量
     * @author: wangjian
     **/
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取电站离线和警报状态数量")
//    @SaCheckPermission("/biz/homepage/statusAndAlarmCount")
    @GetMapping("/biz/homepage/statusAndAlarmCount")
    public CommonResult<StationStatusVO> statusAndAlarmCount() {
        return CommonResult.data(homePageService.statusAndAlarmCount());
    }

    /**
     * @description: 首页地图电站分布
     * @author: wangjian
     **/
    @ApiOperationSupport(order = 2)
    @ApiOperation("首页地图电站分布")
//    @SaCheckPermission("/biz/homepage/distribute")
    @GetMapping("/biz/homepage/distribute")
    public CommonResult<List<StationDistributeVO>> distribute() {
        return CommonResult.data(homePageService.distribute());
    }

    /**
     * @description: 首页整体发电概况
     * @author: wangjian
     **/
    @ApiOperationSupport(order = 3)
    @ApiOperation("首页整体发电概况")
//    @SaCheckPermission("/biz/homepage/allPower")
    @GetMapping("/biz/homepage/allPower")
    public CommonResult<StationHomePagePowerVO> allPower() {
        return CommonResult.data(homePageService.allPower());
    }

    /**
     * @description: 首页整体发电历史
     * @author: wangjian
     **/
    @ApiOperationSupport(order = 4)
    @ApiOperation("首页整体发电历史")
//    @SaCheckPermission("/biz/homepage/allHisPower")
    @PostMapping("/biz/homepage/allHisPower")
    public CommonResult<StationAllHisPowerVO> allHisPower(@RequestBody @Valid HomePageHisPowerReqParam req) throws RuntimeException{
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        SkyLoginUserIdParam param = new SkyLoginUserIdParam();
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }
        return CommonResult.data(homePageService.allHisPower(type, date, param));
    }

    /**
     * 校验日期
     *
     * @param date 日期
     * @param type 类型（1-日2-月3-年4-总）
     * @return
     */
    private boolean validateDate(String date, int type) {
        String pattern;
        switch (type) {
            case 1:
                pattern = "\\d{4}-\\d{2}-\\d{2}";
                break;
            case 2:
                pattern = "\\d{4}-\\d{2}";
                break;
            case 3:
                pattern = "\\d{4}";
                break;
            case 4:
                return true;
            default:
                // 非法类型，直接返回false
                return false;
        }
        return Pattern.matches(pattern, date);
    }
}
