package vip.xiaonuo.biz.modular.dataview.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.DoubleUtils;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.dataview.mapper.DataViewSocialCountMapper;
import vip.xiaonuo.biz.modular.dataview.param.DataViewMapVO;
import vip.xiaonuo.biz.modular.dataview.param.DataViewSocialCountParam;
import vip.xiaonuo.biz.modular.dataview.param.DataViewSocialCountVO;
import vip.xiaonuo.biz.modular.dataview.service.DataViewSocialCountService;
import vip.xiaonuo.biz.modular.homepage.param.HisPowerAllPowerInfoModel;
import vip.xiaonuo.biz.modular.homepage.param.SkyLoginUserIdParam;
import vip.xiaonuo.biz.modular.homepage.param.StationAllHisPowerVO;
import vip.xiaonuo.biz.modular.installerstatistic.entity.SkyInstallerStatistic;
import vip.xiaonuo.biz.modular.installerstatistic.mapper.SkyInstallerStatisticMapper;
import vip.xiaonuo.biz.modular.stationinfo.entity.Area;
import vip.xiaonuo.biz.modular.stationinfo.mapper.AreaMapper;
import vip.xiaonuo.biz.modular.stationinfo.param.PsInstallerAndUserReqParam;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.mapper.SkyStationPowerMapper;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.service.SkyStationUserService;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/7/8 13:25
 */
@Service
public class DataViewSocialCountServiceImpl implements DataViewSocialCountService {

    @Resource
    private DataViewSocialCountMapper dataViewSocialCountMapper;

    @Resource
    private SkyStationUserService skyStationUserService;

    @Resource
    private SkyStationPowerMapper skyStationPowerMapper;

    @Resource
    private AreaMapper areaMapper;

    @Resource
    private SkyInstallerStatisticMapper skyInstallerStatisticMapper;

    @Override
    public DataViewSocialCountVO social() {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
//        SkyLoginUserIdParam param = new SkyLoginUserIdParam();
//        // 先去统计表查询，没查到再查其他表
//        SkyInstallerStatistic statistic = null;
//        if (org.springframework.util.CollectionUtils.isEmpty(roleCodeList)) {
//            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
//        }
//        if (roleCodeList.contains(SkyRoleCodeEnum.SUPERADMIN.getValue())) {
//            statistic = skyInstallerStatisticMapper.selectBySuperAdmin();
//        } else {
//            statistic = skyInstallerStatisticMapper.selectById(loginUser.getId());
//        }
//
//        // 获取当前系统时区
//        TimeZone defaultTimeZone = TimeZone.getDefault();
//        // 获取当前系统时区的偏移量（毫秒）
//        int rawOffset = defaultTimeZone.getRawOffset();
//        // 将偏移量转换为小时
//        int hours = rawOffset / (60 * 60 * 1000);
//        String timeZone = "UTC" + (hours >= 0 ? "+" : "") + hours;
//        if (statistic != null) {
//            res.setStationTotal(statistic.getStationTotal());
//            res.setEquipTotal(statistic.getEquipTotal());
//            // todo 运行时间 总收益 当日收益
//
//            res.setMonTotal(statistic.getPowerTotal());
//            res.setPowerDaily(statistic.getPowerDaily());
//            res.setSaveCoal(statistic.getSaveCoal());
//            res.setReduceCo2(statistic.getReduceCo2());
//            res.setReduceSo2(statistic.getReduceSo2());
//            res.setEquivalentPlanting(statistic.getEquivalentPlanting());
//        }

        PsInstallerAndUserReqParam userReqParam = new PsInstallerAndUserReqParam();
        // 登录信息
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            userReqParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            userReqParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            userReqParam.setStationContactId(loginUser.getId());
        }
        DataViewSocialCountVO social = dataViewSocialCountMapper.social(userReqParam);
        DataViewSocialCountParam param1 = dataViewSocialCountMapper.getPower(userReqParam, LocalDate.now().toString());
        social.setIncomeDaily(DoubleUtils.round(param1.getPowerDaily() * social.getPricePer(),2));
        social.setPowerDaily(param1.getPowerDaily());

        return social;
    }

    @Override
    public StationAllHisPowerVO monthPower(String date, SkyLoginUserIdParam param, String province) {
        StationAllHisPowerVO res = new StationAllHisPowerVO();
        QueryWrapper<SkyStationPower> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().likeRight(SkyStationPower::getDateTime,date)
                .eq(SkyStationPower::getTimeType,1)
                .groupBy(SkyStationPower::getDateTime);

        // 角色用户所属电站

        List<String> userPsIdList = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(param)) {
            if (ObjectUtil.isNotEmpty(param.getDistributor())) {
                List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getInstallerIdL2, param.getDistributor()));
                if (org.apache.commons.collections4.CollectionUtils.isEmpty(list)) {
                    return null;
                }
                userPsIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            } else if (ObjectUtil.isNotEmpty(param.getStationCompany())) {
                List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getInstallerIdL1, param.getStationCompany()));
                if (org.apache.commons.collections4.CollectionUtils.isEmpty(list)) {
                    return null;
                }
                userPsIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            } else if (ObjectUtil.isNotEmpty(param.getStationContactId())) {
                List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getUserId, param.getStationContactId()));
                if (org.apache.commons.collections4.CollectionUtils.isEmpty(list)) {
                    return null;
                }
                userPsIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            }
            if (CollectionUtils.isEmpty(userPsIdList)) {
                StationAllHisPowerVO vo = new StationAllHisPowerVO();
                vo.setTotal(0D);
                vo.setPower(Collections.emptyList());
                vo.setDateList(Collections.emptyList());
                return vo;
            }
        }
        // 省电站数据
        List<String> provincePs = new ArrayList<>();
        if (StringUtils.isNotEmpty(province)) {
            provincePs = dataViewSocialCountMapper.getPsByProvince(province);
            List<String> psIdList = null;
            // 如果非超管
            if (ObjectUtil.isNotEmpty(param)) {
                // 获取省电站和角色用户所属电站交集
                // 使用HashSet来存储userPsIdList中的元素，以便快速查找
                Set<String> set1 = new HashSet<>(userPsIdList);
                Set<String> intersection = new HashSet<>();
                // 遍历provincePs，将与userPsIdList中相同的元素添加到intersection中
                for (String s : provincePs) {
                    if (set1.contains(s)) {
                        intersection.add(s);
                    }
                }
                // 转换为List
                psIdList = new ArrayList<>(intersection);
            } else {
                psIdList = provincePs;
            }
            queryWrapper.lambda().in(SkyStationPower::getStationId, psIdList);
        } else if (!CollectionUtils.isEmpty(userPsIdList)){
            queryWrapper.lambda().in(SkyStationPower::getStationId, userPsIdList);
        }

        List<HisPowerAllPowerInfoModel> monthPowerList = skyStationPowerMapper.queryMonthPower(queryWrapper);
        if (org.apache.commons.collections4.CollectionUtils.isEmpty(monthPowerList)) {
            return null;
        }
        Map<String, String> dayMonthMaps = monthPowerList.stream().collect(Collectors.toMap(HisPowerAllPowerInfoModel::getDateTime, HisPowerAllPowerInfoModel::getStationPower));
        //当月多少日
        List<LocalDate> localDateList = this.getLocalDateList(LocalDateUtils.parseLocalDate(date + "-01", LocalDateUtils.DATE_FORMATTER));
        //当日日期
        List<String> dateList = new ArrayList<>();
        //当日发电量
        List<Double> powerList = new ArrayList<>();
        //当月总发电量
        double total = 0D;
        for (LocalDate localDate : localDateList) {
            String dateStr = LocalDateUtils.formatLocalDate(localDate, LocalDateUtils.DATE_FORMATTER);
            String orDefault = dayMonthMaps.getOrDefault(dateStr, "0");
            dateList.add(dateStr);
            powerList.add(DoubleUtils.round(Double.parseDouble(orDefault),2));
            total+=Double.parseDouble(orDefault);
        }
        res.setPower(powerList);
        res.setDateList(dateList);
        res.setTotal(DoubleUtils.round(total,2));
        return res;
    }

    @Override
    public List<String> city() {
        List<Area> areas = areaMapper.selectList(new QueryWrapper<>());
        return areas.stream()
                .filter(area -> area.getAreaCity() != null && !area.getAreaCity().isEmpty())
                .map(Area::getAreaCity)
                .distinct()
                .collect(Collectors.toList());
    }

    @Override
    public StationAllHisPowerVO yearPower(String date, SkyLoginUserIdParam param, String city) {
        StationAllHisPowerVO res = new StationAllHisPowerVO();
        QueryWrapper<SkyStationPower> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().likeRight(SkyStationPower::getDateTime,date)
                .eq(SkyStationPower::getTimeType,2)
                .groupBy(SkyStationPower::getDateTime);
        if (ObjectUtil.isNotEmpty(param)) {
            if (ObjectUtil.isNotEmpty(param.getDistributor())) {
                List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getInstallerIdL2, param.getDistributor()));
                if (org.apache.commons.collections4.CollectionUtils.isEmpty(list)) {
                    return null;
                }
                List<String> psIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
                queryWrapper.lambda().in(SkyStationPower::getStationId, psIdList);
            } else if (ObjectUtil.isNotEmpty(param.getStationCompany())) {
                List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getInstallerIdL1, param.getStationCompany()));
                if (org.apache.commons.collections4.CollectionUtils.isEmpty(list)) {
                    return null;
                }
                List<String> psIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
                queryWrapper.lambda().in(SkyStationPower::getStationId, psIdList);
            } else if (ObjectUtil.isNotEmpty(param.getStationContactId())) {
                List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getUserId, param.getStationContactId()));
                if (org.apache.commons.collections4.CollectionUtils.isEmpty(list)) {
                    return null;
                }
                List<String> psIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
                queryWrapper.lambda().in(SkyStationPower::getStationId, psIdList);
            }
        }
        List<HisPowerAllPowerInfoModel> yearPowerList = skyStationPowerMapper.queryMonthPower(queryWrapper);
        if (org.apache.commons.collections4.CollectionUtils.isEmpty(yearPowerList)) {
            return null;
        }
        Map<String, String> monthYearMaps = yearPowerList.stream().collect(Collectors.toMap(HisPowerAllPowerInfoModel::getDateTime, HisPowerAllPowerInfoModel::getStationPower));
        //当年多少月
        List<String> monthList  = this.getMonthList(date);
        //当月日期
        List<String> dateList = new ArrayList<>();
        //当月发电量
        List<Double> powerList = new ArrayList<>();
        //当年总发电量
        double total = 0D;
        for (String month  : monthList) {
            String orDefault = monthYearMaps.getOrDefault(month, "0");
            dateList.add(month);
            powerList.add(DoubleUtils.round(Double.valueOf(orDefault),2));
            total+=Double.parseDouble(orDefault);
        }
        res.setPower(powerList);
        res.setDateList(dateList);
        res.setTotal(DoubleUtils.round(total,2));
        return res;
    }

    @Override
    public List<DataViewMapVO> map() {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        PsInstallerAndUserReqParam userReqParam = new PsInstallerAndUserReqParam();
        // 登录信息
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            userReqParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            userReqParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            userReqParam.setStationContactId(loginUser.getId());
        }
        List<DataViewMapVO> res = dataViewSocialCountMapper.map(userReqParam);
        return res;
    }

    /**
     * 获取当月所有日
     *
     * @param date
     * @return
     */
    private List<LocalDate> getLocalDateList(LocalDate date) {
        int year = date.getYear();
        int month = date.getMonthValue();
        int daysInMonth = date.lengthOfMonth();
        List<LocalDate> dates = new ArrayList<>();
        for (int day = 1; day <= daysInMonth; day++) {
            LocalDate currentDate = LocalDate.of(year, month, day);
            dates.add(currentDate);
        }
        return dates;
    }

    /**
     * 获取当年所有月
     *
     * @param year
     * @return
     */
    private List<String> getMonthList(String year) {
        List<String> monthList = new ArrayList<>(12);
        for (int i = 1; i <= 12; i++) {
            monthList.add(String.format("%s-%02d", year, i));
        }
        return monthList;
    }
}
