package vip.xiaonuo.biz.modular.equcontrol.service;

import vip.xiaonuo.biz.modular.equcontrol.param.*;

/**
 * @Author wangjian
 * @Date 2024/5/6 14:27
 */
public interface SkyEquControlService {
    SkyIotParam<SkyIotReadResParam> getParam(SkyReqParam<SkyReadParamDeviceInfoParam> skyReqParam);
    SkyIotParam<SkyIotEquControlDevice> equControl(SkyReqParam<SkyIotEquControlDevice<IotMqttKeyElecMeterParam>> skyReqParam);

    Boolean checkPassword(CheckPasswordParam model);
}
