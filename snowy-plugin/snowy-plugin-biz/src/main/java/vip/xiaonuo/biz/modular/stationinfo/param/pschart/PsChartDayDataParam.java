package vip.xiaonuo.biz.modular.stationinfo.param.pschart;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/4 18:03
 */
@Setter
@Getter
@ContentRowHeight(18)
@HeadRowHeight(20)
@ColumnWidth(24)
public class PsChartDayDataParam {

    /**
     * 运行时间
     */
    @ExcelProperty(value = "时间")
    private String monitorTime;

    /**
     * 发电功率
     */
    @ExcelProperty(value = "发电功率(kW)")
    private Double monPac;

    /**
     * 用电功率
     */
    @ExcelProperty(value = "用电功率(kW)")
    private Double loadPower;

    /**
     * 电网功率
     */
    @ExcelProperty(value = "电网功率(kW)")
    private Double GridActivePower;

    /**
     * 电池功率
     */
    @ExcelProperty(value = "电池功率(kW)")
    private Double batPower;

    /**
     * 直流发电功率
     */
    @ExcelProperty(value = "直流发电功率(kW)")
    private Double inPower;
    private Double monToday;

    private String invSn;
    private String uploadTime;
}
