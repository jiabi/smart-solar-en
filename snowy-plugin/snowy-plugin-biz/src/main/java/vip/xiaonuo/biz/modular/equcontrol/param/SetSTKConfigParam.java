package vip.xiaonuo.biz.modular.equcontrol.param;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
//@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SetSTKConfigParam {
    /**
     * 采集器1.修改数据上报时间间隔
     */
    private Integer Tinterval;

    /**
     * 采集器2.修改MQTT1配置
     */
    private String key;

    private String name;

    private String ps;

    private String url;

    private String timeKey;

    /**
     * 采集器3.修改MQTT2配置
     */
    private String key2;

    private String name2;

    private String ps2;

    private String url2;

    /**
     * 采集器4.特殊指令
     */
//    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer special;

    /**
     * 不清楚的
     */
//    private String ssid;
//
//    private String password;
//
//    private String IP;
//
//    private String GW;
//
//    private String MK;
//
//    private String ap_s;
//
//    private String ap_p;
//
//    private String SN;
//
//    private String IP2;
//
//    private String GW2;
//
//    private String MK2;
//
//    private String HW;
//
//    private String mfr;
//
//    private Integer typ;
//
//    private String mod;
//
//    private Integer MDID;
//
//    private Integer PTMODE;
//
//    private String timekey;
//
//    private String W1;
//
//    private Integer W3;
}
