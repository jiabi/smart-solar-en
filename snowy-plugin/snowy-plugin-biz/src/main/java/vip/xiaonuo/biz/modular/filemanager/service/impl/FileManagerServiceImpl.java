package vip.xiaonuo.biz.modular.filemanager.service.impl;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.CustomMultipartFile;
import vip.xiaonuo.biz.modular.filemanager.param.AutoTestParam;
import vip.xiaonuo.biz.modular.filemanager.service.FileManagerService;
import vip.xiaonuo.dev.api.DevFileApi;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/4/26 17:21
 */

@Service
public class FileManagerServiceImpl implements FileManagerService {

    @Resource
    private DevFileApi devFileApi;
    @Override
    public String autoTestPdf(List<AutoTestParam> reqList) throws Exception {
        try {
            InputStream inputStream = this.getClass().getResourceAsStream("/jasper/autotest.jasper");

            List<AutoTestParam> list = new ArrayList<>();
            list.add(new AutoTestParam("Risultati della prova","Successo"));
            list.add(new AutoTestParam("",""));
            list.add(new AutoTestParam("Livello 1 di sovratensione (59.S1)","Successo"));
            list.add(new AutoTestParam("Soglia di tensione impostata", formData(reqList.get(12).getValue())));
            list.add(new AutoTestParam("Valore di tensione rilevato", formData(reqList.get(14).getValue())));
            list.add(new AutoTestParam("Soglia di tempo impostata",formData(reqList.get(13).getValue())));
            list.add(new AutoTestParam("Valore di tempo rilevato",formData(reqList.get(15).getValue())));
            list.add(new AutoTestParam("",""));
            list.add(new AutoTestParam("Livello 2 di sovratensione (59.S2)","Successo"));
            list.add(new AutoTestParam("Soglia di tensione impostata", formData(reqList.get(0).getValue())));
            list.add(new AutoTestParam("Valore di tensione rilevato", formData(reqList.get(2).getValue())));
            list.add(new AutoTestParam("Soglia di tempo impostata", formData(reqList.get(1).getValue())));
            list.add(new AutoTestParam("Valore di tempo rilevato", formData(reqList.get(3).getValue())));
            list.add(new AutoTestParam("",""));
            list.add(new AutoTestParam("Livello 1 di sovratensione (27.S1)","Successo"));
            list.add(new AutoTestParam("Soglia di tensione impostata", formData(reqList.get(4).getValue())));
            list.add(new AutoTestParam("Valore di tensione rilevato", formData(reqList.get(6).getValue())));
            list.add(new AutoTestParam("Soglia di tempo impostata", formData(reqList.get(5).getValue())));
            list.add(new AutoTestParam("Valore di tempo rilevato", formData(reqList.get(7).getValue())));
            list.add(new AutoTestParam("",""));
            list.add(new AutoTestParam("Livello 2 di sovratensione (27.S2)","Successo"));
            list.add(new AutoTestParam("Soglia di tensione impostata",formData(reqList.get(8).getValue())));
            list.add(new AutoTestParam("Valore di tensione rilevato", formData(reqList.get(10).getValue())));
            list.add(new AutoTestParam("Soglia di tempo impostata",formData(reqList.get(9).getValue())));
            list.add(new AutoTestParam("Valore di tempo rilevato", formData(reqList.get(11).getValue())));
            list.add(new AutoTestParam("",""));
            list.add(new AutoTestParam("Livello 1 di sovratensione (81>.S1)","Successo"));
            list.add(new AutoTestParam("Soglia di tensione impostata", formData(reqList.get(16).getValue())));
            list.add(new AutoTestParam("Valore di tensione rilevato", formData(reqList.get(18).getValue())));
            list.add(new AutoTestParam("Soglia di tempo impostata", formData(reqList.get(17).getValue())));
            list.add(new AutoTestParam("Valore di tempo rilevato", formData(reqList.get(19).getValue())));
            list.add(new AutoTestParam("",""));
            list.add(new AutoTestParam("Livello 1 di sovratensione (81<.S1)","Successo"));
            list.add(new AutoTestParam("Soglia di tensione impostata", formData(reqList.get(20).getValue())));
            list.add(new AutoTestParam("Valore di tensione rilevato", formData(reqList.get(22).getValue())));
            list.add(new AutoTestParam("Soglia di tempo impostata", formData(reqList.get(21).getValue())));
            list.add(new AutoTestParam("Valore di tempo rilevato", formData(reqList.get(23).getValue())));
            list.add(new AutoTestParam("",""));
            list.add(new AutoTestParam("Livello 1 di sovratensione (81>.S2)","Successo"));
            list.add(new AutoTestParam("Soglia di tensione impostata", formData(reqList.get(24).getValue())));
            list.add(new AutoTestParam("Valore di tensione rilevato", formData(reqList.get(26).getValue())));
            list.add(new AutoTestParam("Soglia di tempo impostata", formData(reqList.get(25).getValue())));
            list.add(new AutoTestParam("Valore di tempo rilevato", formData(reqList.get(27).getValue())));
            list.add(new AutoTestParam("",""));
            list.add(new AutoTestParam("Livello 2 di sovratensione (81<.S2)","Successo"));
            list.add(new AutoTestParam("Soglia di tensione impostata", formData(reqList.get(28).getValue())));
            list.add(new AutoTestParam("Valore di tensione rilevato", formData(reqList.get(30).getValue())));
            list.add(new AutoTestParam("Soglia di tempo impostata", formData(reqList.get(29).getValue())));
            list.add(new AutoTestParam("Valore di tempo rilevato", formData(reqList.get(31).getValue())));

            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream,null,dataSource);
//            // 将pdf通过HttpServletResponse直接返回
//            response.setContentType("application/pdf");
//            String fileName = "autoTest.pdf";
//            response.setHeader("content-disposition","attachment;filename=" + URLEncoder.encode(fileName,"utf-8"));
//            JasperExportManager.exportReportToPdfStream(jasperPrint,response.getOutputStream());

            // 将pdf保存保存返回url
            // 创建一个字节数组输出流
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            // 将 Jasper 生成的 PDF 写入到输出流中
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

            // 将输出流内容转换为字节数组
            byte[] content = outputStream.toByteArray();

            // 创建 CustomMultipartFile 对象
            MultipartFile multipartFile = new CustomMultipartFile("file", "report.pdf", "application/pdf", content);

            // 关闭输出流
            outputStream.close();

            // 保存文件
            String urlLocal = devFileApi.storageFileWithReturnUrlAliyun(multipartFile);

            return urlLocal;

        } catch (Exception e) {
            throw new SkyCommonException(SkyCommonExceptionEnum.EQU_AUTO_TEST_PDF_PRODUCE_ERROR);
        }
    }

    private String formData(String data){
        if (data.endsWith("Hz")) {
            // 从字符串中提取数字部分，去除单位
            String numberPart = data.replaceAll("[^\\d.]", "");
            // 将提取的字符串转换为浮点数
            double frequency = Double.parseDouble(numberPart);
            // 使用DecimalFormat格式化为两位小数的字符串
            DecimalFormat df = new DecimalFormat("#.##");
            String formatted = df.format(frequency);
            return formatted + "Hz";
        } else if (data.endsWith("V")) {
            char lastChar = data.charAt(data.length() - 1);
            // 从字符串中提取数字部分，去除单位
            String numberPart = data.replaceAll("[^\\d.]", "");
            // 将提取的字符串转换为浮点数
            double frequency = Double.parseDouble(numberPart);
            // 使用DecimalFormat格式化为两位小数的字符串
            DecimalFormat df = new DecimalFormat("#.##");
            String formatted = df.format(frequency);
            return formatted + lastChar;
        } else {
            char lastChar = data.charAt(data.length() - 1);
            // 从字符串中提取数字部分，去除单位
            String numberPart = data.replaceAll("[^\\d.]", "");
            // 将提取的字符串转换为浮点数
            double frequency = Double.parseDouble(numberPart);
            // 使用DecimalFormat格式化为两位小数的字符串
            DecimalFormat df = new DecimalFormat("#.##");
            String formatted = df.format(frequency);
            return formatted + lastChar;
        }
    }
}
