package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @Description:
 * @Author: wangjian
 * @CreateTime: 2023-11-16  17:36
 */
@Getter
@Setter
public class StationInfoOverviewPowerAnalysisParam {

    /** 电站ID */
    @ApiModelProperty(value = "电站ID", required = true)
    @NotBlank(message = "id不能为空")
    private String id;

    /** 日期 */
    @ApiModelProperty(value = "年、年-月、年-月-日")
    private String dateTime;
}