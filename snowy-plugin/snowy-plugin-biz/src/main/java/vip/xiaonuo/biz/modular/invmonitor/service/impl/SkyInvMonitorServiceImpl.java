/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invmonitor.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.invmonitor.entity.SkyInvMonitor;
import vip.xiaonuo.biz.modular.invmonitor.mapper.SkyInvMonitorMapper;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorAddParam;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorEditParam;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorIdParam;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorPageParam;
import vip.xiaonuo.biz.modular.invmonitor.service.SkyInvMonitorService;

import java.util.List;

/**
 * 逆变器历史数据Service接口实现类
 *
 * @author 全佳璧
 * @date  2023/10/23 09:44
 **/
@Service
public class SkyInvMonitorServiceImpl extends ServiceImpl<SkyInvMonitorMapper, SkyInvMonitor> implements SkyInvMonitorService {


    @Override
    public Page<SkyInvMonitor> page(SkyInvMonitorPageParam skyInvMonitorPageParam) {
        QueryWrapper<SkyInvMonitor> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isAllNotEmpty(skyInvMonitorPageParam.getSortField(), skyInvMonitorPageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(skyInvMonitorPageParam.getSortOrder());
            queryWrapper.orderBy(true, skyInvMonitorPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(skyInvMonitorPageParam.getSortField()));
        } else {
            queryWrapper.lambda().orderByAsc(SkyInvMonitor::getId);
        }
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyInvMonitorAddParam skyInvMonitorAddParam) {
        SkyInvMonitor skyInvMonitor = BeanUtil.toBean(skyInvMonitorAddParam, SkyInvMonitor.class);
        this.save(skyInvMonitor);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyInvMonitorEditParam skyInvMonitorEditParam) {
        SkyInvMonitor skyInvMonitor = this.queryEntity(skyInvMonitorEditParam.getId());
        BeanUtil.copyProperties(skyInvMonitorEditParam, skyInvMonitor);
        this.updateById(skyInvMonitor);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyInvMonitorIdParam> skyInvMonitorIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyInvMonitorIdParamList, SkyInvMonitorIdParam::getId));
    }

    @Override
    public SkyInvMonitor detail(SkyInvMonitorIdParam skyInvMonitorIdParam) {
        return this.queryEntity(skyInvMonitorIdParam.getId());
    }

    @Override
    public SkyInvMonitor queryEntity(String id) {
        SkyInvMonitor skyInvMonitor = this.getById(id);
        if(ObjectUtil.isEmpty(skyInvMonitor)) {
            throw new CommonException("逆变器历史数据不存在，id值为：{}", id);
        }
        return skyInvMonitor;
    }

}
