/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.eququality.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.ColumnTitleMap;
import vip.xiaonuo.biz.core.util.ExcelUtils;
import vip.xiaonuo.biz.core.util.ExportExcelUtil;
import vip.xiaonuo.biz.core.util.easyexcel.EasyExcelMaker;
import vip.xiaonuo.biz.core.util.easyexcel.ExportUtil;
import vip.xiaonuo.biz.modular.equipmodel.entity.SkyEquipModel;
import vip.xiaonuo.biz.modular.eququality.param.*;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoPageParam;
import vip.xiaonuo.biz.modular.stationinfo.controller.StationInfoController;
import vip.xiaonuo.biz.modular.stationinfo.param.PsOverviewHisPowerProportionExportVO;
import vip.xiaonuo.biz.modular.stationinfo.param.PsOverviewHisPowerProportionVO;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoExportParam;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.eququality.entity.SkyEquQuality;
import vip.xiaonuo.biz.modular.eququality.service.SkyEquQualityService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 设备质保信息控制器
 *
 * @author 全佳璧
 * @date  2023/09/25 14:20
 */
@Api(tags = "设备质保信息控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyEquQualityController {

    private static Logger logger = LoggerFactory.getLogger(SkyEquQualityController.class);

    @Resource
    private SkyEquQualityService skyEquQualityService;

    /**
     * 获取设备质保信息分页
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取设备质保信息分页")
    @SaCheckPermission("/biz/eququality/page")
    @GetMapping("/biz/eququality/page")
    public CommonResult<Page<SkyEquQualityPageVO>> page(SkyEquQualityPageReqDTO skyEquQualityPageReqDTO) {
        SkyEquQualityPageParam param = new SkyEquQualityPageParam();
        BeanUtil.copyProperties(skyEquQualityPageReqDTO,param);
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }
        return CommonResult.data(skyEquQualityService.page(param));
    }

    /**
     * 添加设备质保信息
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加设备质保信息")
    @CommonLog("添加设备质保信息")
    @SaCheckPermission("/biz/eququality/add")
    @PostMapping("/biz/eququality/add")
    public CommonResult<String> add(@RequestBody @Valid SkyEquQualityAddParam skyEquQualityAddParam) {
        skyEquQualityService.add(skyEquQualityAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑设备质保信息
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑设备质保信息")
    @CommonLog("编辑设备质保信息")
    @SaCheckPermission("/biz/eququality/edit")
    @PostMapping("/biz/eququality/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyEquQualityEditParam skyEquQualityEditParam) {
        skyEquQualityService.edit(skyEquQualityEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除设备质保信息
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除设备质保信息")
    @CommonLog("删除设备质保信息")
    @SaCheckPermission("/biz/eququality/delete")
    @PostMapping("/biz/eququality/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyEquQualityIdParam> skyEquQualityIdParamList) {
        skyEquQualityService.delete(skyEquQualityIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取设备质保信息详情
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取设备质保信息详情")
    @SaCheckPermission("/biz/eququality/detail")
    @GetMapping("/biz/eququality/detail")
    public CommonResult<SkyEquQuality> detail(@Valid SkyEquQualityIdParam skyEquQualityIdParam) {
        return CommonResult.data(skyEquQualityService.detail(skyEquQualityIdParam));
    }

//    /**
//     * @description: 设备质保信息下载
//     * @author: wangjian
//     * @date: 2023/9/27 16:13
//     **/
//    @ApiOperationSupport(order = 6)
//    @ApiOperation("设备质保信息下载")
////    @SaCheckPermission("/biz/eququality/export")
//    @PostMapping("/biz/eququality/export")
//    public void export(SkyEquQualityExportParam exportParam, HttpServletResponse response) {
//        try {
//            ArrayList<String> titleKeyList = new ColumnTitleMap("eququalityInfo").getTitleKeyList();
//            Map<String, String> titleMap = new ColumnTitleMap("eququalityInfo").getColumnTitleMap();
//            List<Map<String, Object>> list = skyEquQualityService.exportInfoList(exportParam);
//            ExportExcelUtil.expoerDataExcel("设备质保信息", "设备质保信息", response, titleKeyList, titleMap, list);
//        } catch (IOException e) {
//            logger.error("导出设备质保信息失败", e);
//        }
//    }

    /**
     * @description: 设备质保信息下载
     * @author: wangjian
     * @date: 2023/9/27 16:13
     **/
    @ApiOperationSupport(order = 6)
    @ApiOperation("设备质保信息下载")
//    @SaCheckPermission("/biz/eququality/export")
    @GetMapping("/biz/eququality/export")
    public void export(SkyEquQualityExportParam exportParam, HttpServletResponse response) {
        /**********************************设备质保信息下载 begin***********************************/
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            exportParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            exportParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            exportParam.setStationContactId(loginUser.getId());
        }
        List<SkyEquQualityExcelVO>  list = skyEquQualityService.exportInfoList2(exportParam);
        EasyExcelMaker excelMaker = EasyExcelMaker.getInstance();
        excelMaker.make(response, list, SkyEquQualityExcelVO.class, ExportUtil.translateFileName("设备质保信息"),"设备质保信息");
        /**********************************设备质保信息下载 end***********************************/
    }

    /**
     * @description: 获取设备SN集合
     * @author: wangjian
     * @date: 2023/9/27 17:53
     **/
    @ApiOperationSupport(order = 7)
    @ApiOperation("获取设备SN集合")
//    @SaCheckPermission("/biz/equipmodel/getEquipSnList")
    @GetMapping("/biz/eququality/getEquipSnList")
    public CommonResult<List<SkySnAndParamerParam>> getEquipSnList(SkyPsIdParam skyPsIdParam) {

        return CommonResult.data(skyEquQualityService.getEquipSnList(skyPsIdParam));
    }

    /**
     * @description: 检查设备SN是否存在
     * @author: wangjian
     * @date: 2023/9/28 11:45
     **/
    @ApiOperationSupport(order = 8)
    @ApiOperation("检查设备SN是否存在")
//    @SaCheckPermission("/biz/equipmodel/getEquipSnList")
    @GetMapping("/biz/eququality/checkEquipSn")
    public CommonResult<Boolean> checkEquipSn(@RequestBody @Valid EquipSnParam equipSnParam) {
        return CommonResult.data(skyEquQualityService.checkEquipSn(equipSnParam));
    }

//    /**
//     * @description: 下载质保信息导入模板
//     * @author: wangjian
//     * @date: 2023/10/5 11:48
//     **/
//    @ApiOperationSupport(order = 9)
//    @ApiOperation("下载质保信息导入模板")
////    @SaCheckPermission("/biz/equipmodel/getExcelModel")
//    @GetMapping("/biz/eququality/getExcelModel")
//    public void getExcelModel(HttpServletResponse response) {
//        try {
//            ArrayList<String> titleKeyList = new ColumnTitleMap("eququalityImportInfo").getTitleKeyList();
//            Map<String, String> titleMap = new ColumnTitleMap("eququalityImportInfo").getColumnTitleMap();
//            List<Map<String, Object>> info = skyEquQualityService.getExcelModel();
//            ExportExcelUtil.expoerDataExcel("质保信息管理", "质保信息管理", response, titleKeyList, titleMap, info);
//        } catch (IOException e) {
//            logger.error("导出质保信息模板失败", e);
//        } catch (ParseException e) {
//            throw new RuntimeException("日期转换错误",e);
//        }
//    }

    /**
     * @description: 下载质保信息导入模板
     * @author: wangjian
     * @date: 2023/10/5 11:48
     **/
    @ApiOperationSupport(order = 9)
    @ApiOperation("下载质保信息导入模板")
//    @SaCheckPermission("/biz/equipmodel/getExcelModel")
    @GetMapping("/biz/eququality/getExcelModel")
    public void getExcelModel(HttpServletResponse response) throws ParseException {
        List<SkyEquQualityExportVO> list = new ArrayList<>();
        SkyEquQualityExportVO info = new SkyEquQualityExportVO();
        LocalDate localDate = LocalDate.now();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        info.setEquSn("SR1CS6N5P4S002");
        info.setEquModel("INV7000W");
        info.setEquType(1);
        info.setStartDate(localDate.format(DateTimeFormatter.ISO_LOCAL_DATE));
        info.setTimeLimit(1.5D);
        list.add(info);

        EasyExcelMaker excelMaker = EasyExcelMaker.getInstance();
        excelMaker.make(response, list, SkyEquQualityExportVO.class, ExportUtil.translateFileName("质保信息导入模板"),"质保信息导入模板");
    }

    /**
     * @description: 质保信息批量导入
     * @author: wangjian
     * @date: 2023/10/5 14:29
     **/
    @ApiOperationSupport(order = 9)
    @ApiOperation("质保信息批量导入")
//    @SaCheckPermission("/biz/equipmodel/getExcelModel")
    @PostMapping("/biz/eququality/importByExcel")
    public CommonResult<String> importByExcel(@RequestPart("file") MultipartFile file, HttpServletResponse response) {
        try {
            List<SkyEquQualityImportModel> models = ExcelUtils.readMultipartFile(file,SkyEquQualityImportModel.class);
            List<ImportErrorReturnVO> errors = null;
            // 第一行为示例
            if (models.size() > 1) {
                errors = skyEquQualityService.importByExcel(models);
            }
            if (!CollectionUtils.isEmpty(errors)){
                EasyExcelMaker excelMaker = EasyExcelMaker.getInstance();
                excelMaker.make(response, errors, ImportErrorReturnVO.class, ExportUtil.translateFileName("质保信息导入错误信息"));
            }

        } catch (Exception e) {
            logger.error("导入质保信息失败", e);
        }
        return CommonResult.ok();
    }

}
