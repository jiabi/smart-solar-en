/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 电站信息查询参数
 *
 * @author 全佳璧
 * @date  2023/09/12 10:51
 **/
@Getter
@Setter
public class StationInfoPageReqDTO {


    /** 当前页 */
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    /** 每页条数 */
    @ApiModelProperty(value = "每页条数")
    private Integer size;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称")
    private String stationName;

    /** 装机容量范围起始值（默认小数点后两位） */
    @ApiModelProperty(value = "装机容量范围起始值（默认小数点后两位）")
    private Double stationSizeBegin;

    /** 装机容量范围结束值（默认小数点后两位） */
    @ApiModelProperty(value = "装机容量范围结束值（默认小数点后两位）")
    private Double stationSizeEnd;

    /** 电站类型 */
    @ApiModelProperty(value = "电站类型")
    private Integer stationType;

    /** 电站系统 */
    @ApiModelProperty(value = "电站系统")
    private Integer stationSystem;

    /** 并网状态 */
    @ApiModelProperty(value = "并网状态")
    private Integer stationGridstatus;

    /** 电站区域 */
    @ApiModelProperty(value = "电站区域")
    private String stationArea;

    /** 电站状态 */
    @ApiModelProperty(value = "电站状态")
    private Integer stationStatus;

    /** 报警状态 */
    @ApiModelProperty(value = "报警状态")
    private Integer stationAlarm;

    /** 安装公司 */
    @ApiModelProperty(value = "安装公司")
    private String stationCompany;

    public int getLimitStart() {
        return (getCurrent() - 1) * getSize();
    }
}
