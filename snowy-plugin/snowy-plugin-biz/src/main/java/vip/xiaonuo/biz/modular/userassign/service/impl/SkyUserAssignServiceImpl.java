/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.userassign.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.invupgrade.param.SkyInvUpgradePageVO;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoPageModel;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.mapper.SkyStationUserMapper;
import vip.xiaonuo.biz.modular.userassign.param.*;
import vip.xiaonuo.biz.modular.userrelationship.entity.SkyUserRelationship;
import vip.xiaonuo.biz.modular.userrelationship.mapper.SkyUserRelationshipMapper;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.userassign.entity.SkyUserAssign;
import vip.xiaonuo.biz.modular.userassign.mapper.SkyUserAssignMapper;
import vip.xiaonuo.biz.modular.userassign.service.SkyUserAssignService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户转移Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/02/22 16:25
 **/
@Service
public class SkyUserAssignServiceImpl extends ServiceImpl<SkyUserAssignMapper, SkyUserAssign> implements SkyUserAssignService {

    @Resource
    private SkyUserRelationshipMapper skyUserRelationshipMapper;

    @Resource
    private SkyStationUserMapper skyStationUserMapper;

    @Resource
    private SkyUserAssignMapper skyUserAssignMapper;

    @Override
    public Page<SkyUserAssignVo> page(SkyUserAssignPageParam skyUserAssignPageParam) {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList) || roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue()) || roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        List<String> installerList = new ArrayList<>();
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            List<SkyUserRelationship> relationships = skyUserRelationshipMapper.selectList(new QueryWrapper<SkyUserRelationship>().lambda()
                    .eq(SkyUserRelationship::getInstallerId, loginUser.getId())
                    .eq(SkyUserRelationship::getInstallerLevel, 1));
            if (relationships!= null && relationships.size()>0) {
                installerList = relationships.stream().map(SkyUserRelationship::getUserId).collect(Collectors.toList());
            }
            if(CollectionUtils.isEmpty(installerList)){
                return new Page<SkyUserAssignVo>().setRecords(null).setTotal(0);
            }
        }
//        QueryWrapper<SkyUserAssign> queryWrapper = new QueryWrapper<>();
//        if(ObjectUtil.isNotEmpty(skyUserAssignPageParam.getStationName())) {
//            queryWrapper.lambda().like(SkyUserAssign::getStationName, skyUserAssignPageParam.getStationName());
//        }
//        if(ObjectUtil.isNotEmpty(skyUserAssignPageParam.getStationContactid())) {
//            queryWrapper.lambda().like(SkyUserAssign::getStationContactid, skyUserAssignPageParam.getStationContactid());
//        }
//        if(ObjectUtil.isNotEmpty(skyUserAssignPageParam.getStationCompany())) {
//            queryWrapper.lambda().like(SkyUserAssign::getStationCompany, skyUserAssignPageParam.getStationCompany());
//        }

        if (!CollectionUtils.isEmpty(installerList)) {
            List<String> stationIds = skyStationUserMapper.selectList(Wrappers.lambdaQuery(SkyStationUser.class)
                            .in(SkyStationUser::getInstallerIdL1, installerList))
                    .stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            if(!CollectionUtils.isEmpty(stationIds)){
//                queryWrapper.lambda().in(SkyUserAssign::getId,stationIds);
                skyUserAssignPageParam.setStationIds(stationIds);
            }
        }
//        if (!CollectionUtils.isEmpty(installerList)) {
//            queryWrapper.lambda().in(SkyUserAssign::getStationCompany, installerList);
//        }
//        if(ObjectUtil.isAllNotEmpty(skyUserAssignPageParam.getSortField(), skyUserAssignPageParam.getSortOrder())) {
//            CommonSortOrderEnum.validate(skyUserAssignPageParam.getSortOrder());
//            queryWrapper.orderBy(true, skyUserAssignPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
//                    StrUtil.toUnderlineCase(skyUserAssignPageParam.getSortField()));
//        } else {
//            queryWrapper.lambda().orderByAsc(SkyUserAssign::getId);
//        }
//        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
        //分页sql查询
        Page<SkyUserAssignVo> pageDataModel = new Page<>(skyUserAssignPageParam.getCurrent(),skyUserAssignPageParam.getSize());
        List<SkyUserAssignVo> skyUserAssigns = skyUserAssignMapper.stationInfoUserPage(skyUserAssignPageParam);
        pageDataModel.setRecords(skyUserAssigns);
        pageDataModel.setTotal(skyUserAssignMapper.stationInfoUserCount(skyUserAssignPageParam));
        return pageDataModel;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyUserAssignAddParam skyUserAssignAddParam) {
        SkyUserAssign skyUserAssign = BeanUtil.toBean(skyUserAssignAddParam, SkyUserAssign.class);
        this.save(skyUserAssign);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyUserAssignEditParam skyUserAssignEditParam) {
        SkyUserAssign skyUserAssign = this.queryEntity(skyUserAssignEditParam.getId());
        BeanUtil.copyProperties(skyUserAssignEditParam, skyUserAssign);
        this.updateById(skyUserAssign);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyUserAssignIdParam> skyUserAssignIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyUserAssignIdParamList, SkyUserAssignIdParam::getId));
    }

    @Override
    public SkyUserAssign detail(SkyUserAssignIdParam skyUserAssignIdParam) {
        return this.queryEntity(skyUserAssignIdParam.getId());
    }

    @Override
    public SkyUserAssign queryEntity(String id) {
        SkyUserAssign skyUserAssign = this.getById(id);
        if(ObjectUtil.isEmpty(skyUserAssign)) {
            throw new CommonException("用户转移不存在，id值为：{}", id);
        }
        return skyUserAssign;
    }

}
