package vip.xiaonuo.biz.modular.app.station.param;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author: wangjian
 * @date: 2023/11/14 17:53
 **/
@Getter
@Setter
public class AppStationInfoOverviewVO {

    /** 累计发电量（单位：kWh） */
    private Double monTotal;

    /** 当日发电量（单位：kWh） */
    private Double monToday;

    /** 当前发电总功率 */
    private Double pac;

    /** 节约用煤（吨） */
    private Double saveCoal;

    /** 减少CO2排放（吨） */
    private Double reduceCo2;

    /** 减少SO2排放（吨） */
    private Double reduceSo2;

    /** 等效植树量（棵） */
    private Double equivalentPlanting;
}
