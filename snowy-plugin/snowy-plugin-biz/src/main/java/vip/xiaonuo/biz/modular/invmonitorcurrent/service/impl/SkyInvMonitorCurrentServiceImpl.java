/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invmonitorcurrent.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONString;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.biz.core.util.DoubleUtils;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInverterPower;
import vip.xiaonuo.biz.modular.invmonitorcurrent.mapper.SkyInverterPowerMapper;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.*;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;
import vip.xiaonuo.biz.modular.invmonitorcurrent.mapper.SkyInvMonitorCurrentMapper;
import vip.xiaonuo.biz.modular.invmonitorcurrent.service.SkyInvMonitorCurrentService;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * 逆变器实时数据Service接口实现类
 *
 * @author 全佳璧
 * @date  2023/10/12 15:36
 **/
@Service
public class SkyInvMonitorCurrentServiceImpl extends ServiceImpl<SkyInvMonitorCurrentMapper, SkyInvMonitorCurrent> implements SkyInvMonitorCurrentService {

    @Resource
    private SkyInvMonitorCurrentMapper skyInvMonitorCurrentMapper;

    @Resource
    private SkyInverterPowerMapper skyInverterPowerMapper;

    @Override
    public Page<SkyInvMonitorCurrent> page(SkyInvMonitorCurrentPageParam skyInvMonitorCurrentPageParam) {
        QueryWrapper<SkyInvMonitorCurrent> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isAllNotEmpty(skyInvMonitorCurrentPageParam.getSortField(), skyInvMonitorCurrentPageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(skyInvMonitorCurrentPageParam.getSortOrder());
            queryWrapper.orderBy(true, skyInvMonitorCurrentPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(skyInvMonitorCurrentPageParam.getSortField()));
        } else {
            queryWrapper.lambda().orderByAsc(SkyInvMonitorCurrent::getId);
        }
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyInvMonitorCurrentAddParam skyInvMonitorCurrentAddParam) {
        SkyInvMonitorCurrent skyInvMonitorCurrent = BeanUtil.toBean(skyInvMonitorCurrentAddParam, SkyInvMonitorCurrent.class);
        this.save(skyInvMonitorCurrent);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyInvMonitorCurrentEditParam skyInvMonitorCurrentEditParam) {
        SkyInvMonitorCurrent skyInvMonitorCurrent = this.queryEntity(skyInvMonitorCurrentEditParam.getId());
        BeanUtil.copyProperties(skyInvMonitorCurrentEditParam, skyInvMonitorCurrent);
        this.updateById(skyInvMonitorCurrent);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyInvMonitorCurrentIdParam> skyInvMonitorCurrentIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyInvMonitorCurrentIdParamList, SkyInvMonitorCurrentIdParam::getId));
    }

    @Override
    public SkyInvMonitorCurrent detail(SkyInvMonitorCurrentIdParam skyInvMonitorCurrentIdParam) {
        return this.queryEntity(skyInvMonitorCurrentIdParam.getId());
    }

    @Override
    public SkyInvMonitorCurrent queryEntity(String id) {
        SkyInvMonitorCurrent skyInvMonitorCurrent = this.getById(id);
        if(ObjectUtil.isEmpty(skyInvMonitorCurrent)) {
            throw new CommonException("逆变器实时数据不存在，id值为：{}", id);
        }
        return skyInvMonitorCurrent;
    }

    @Override
    public SkyInvMonitorCurrentDetailVO detailByInvSn(SkyInvMonitorCurrentSnParam param) {
        SkyInvMonitorCurrentDetailVO vo = new SkyInvMonitorCurrentDetailVO();
        QueryWrapper<SkyInvMonitorCurrent> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SkyInvMonitorCurrent::getInvSn,param.getInvSn())
                .orderByDesc(SkyInvMonitorCurrent::getCreateTime)
                .last("limit 1");
        SkyInvMonitorCurrent currentInfo = this.getOne(queryWrapper);
        if(ObjectUtil.isEmpty(currentInfo)) {
//            throw new CommonException("逆变器实时数据不存在，设备SN值为：{}", param.getInvSn());
            return null;
        }
        BeanUtil.copyProperties(currentInfo,vo);


        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        if (ObjectUtil.isNotEmpty(currentInfo.getMonPv1vol()) && ObjectUtil.isNotEmpty(currentInfo.getMonPv1cur())) {
            vo.setPv1Power(Double.valueOf(decimalFormat.format(currentInfo.getMonPv1vol() * currentInfo.getMonPv1cur())));
        }
        if (ObjectUtil.isNotEmpty(currentInfo.getGridVol()) && ObjectUtil.isNotEmpty(currentInfo.getGridCur())) {
            vo.setGridTotalPower(currentInfo.getGridActivepower());
        }
        // TODO: 确认用电总视在功率
        if (ObjectUtil.isNotEmpty(currentInfo.getGridVol()) && ObjectUtil.isNotEmpty(currentInfo.getGridCur())) {
            vo.setLoadInspectingPower(Double.valueOf(decimalFormat.format(currentInfo.getLoadPower())));
        }
        // 功率计算
        this.handleData(currentInfo,vo);
        return vo;
    }

    @Override
    public List<SkyInvMonitorHistoryVO> historyDetailByInvSn(SkyInvMonitorHistoryParam skyInvMonitorHistoryParam) throws ParseException {
        // 日数据
        if (StringUtils.isNotEmpty(skyInvMonitorHistoryParam.getDateTime()) && skyInvMonitorHistoryParam.getDateTime().length() > 7) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate ldt = LocalDate.parse(skyInvMonitorHistoryParam.getDateTime(), formatter);
            LocalDateTime startDay = LocalDateTime.of(ldt, LocalTime.MIN);
            LocalDateTime endDay = LocalDateTime.of(ldt, LocalTime.MAX);

            // 非当前月份的去对应月份表查询历史数据
            if (ldt.getMonth().compareTo(LocalDate.now().getMonth()) != 0 ) {
                DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyyMM");
                List<SkyInvMonitorCurrent> list = skyInvMonitorCurrentMapper.selectHistory(ldt.format(formatter2),skyInvMonitorHistoryParam.getInvSn());
                return getMonitorHistoryVOList(list);
            }

            QueryWrapper<SkyInvMonitorCurrent> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().between(SkyInvMonitorCurrent::getMonitorTime,startDay,endDay)
                    .orderByAsc(SkyInvMonitorCurrent::getMonitorTime);
            List<SkyInvMonitorCurrent> current = this.list(queryWrapper);
            return getMonitorHistoryVOList(current);
        }
        return null;
    }

    @Override
    public List<SkyInverterPower> power(SkyInvInfoPowerParam skyInvInfoPowerParam) {
        // 月、年数据
        QueryWrapper<SkyInverterPower> queryWrapper = new QueryWrapper<>();
        if (!ObjectUtil.isEmpty(skyInvInfoPowerParam.getInvSn())) {
            queryWrapper.lambda().eq(SkyInverterPower::getInvSn, skyInvInfoPowerParam.getInvSn());
        } else {
            throw new CommonException("逆变器SN不能为空！");
        }
        if (!ObjectUtil.isEmpty(skyInvInfoPowerParam.getTimeType())) {
            queryWrapper.lambda().eq(SkyInverterPower::getTimeType, skyInvInfoPowerParam.getTimeType()-1);
        }
        queryWrapper.lambda().like(SkyInverterPower::getDateTime,skyInvInfoPowerParam.getDateTime());

        queryWrapper.lambda().orderByAsc(SkyInverterPower::getDateTime);
        List<SkyInverterPower> powerList = skyInverterPowerMapper.selectList(queryWrapper);
        powerList = powerList.stream().filter(distinctByKey(SkyInverterPower::getDateTime)).collect(Collectors.toList());
//        Map<String, SkyInvPower> collect = powerList.stream().distinct()
//                .collect(Collectors.toMap(SkyInvPower::getDateTime, SkyInvPower -> SkyInvPower));
//        return sortByKey(collect, false);
        return powerList;
    }

    private List<SkyInvMonitorHistoryVO> getMonitorHistoryVOList(List<SkyInvMonitorCurrent> current) throws ParseException {
        List<SkyInvMonitorHistoryVO> voList = new ArrayList<>();
        for (SkyInvMonitorCurrent currentInfo: current) {
            SkyInvMonitorHistoryVO vo = new SkyInvMonitorHistoryVO();
            BeanUtil.copyProperties(currentInfo,vo);
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
            vo.setMonitorTime(sdf.parse(sdf.format(currentInfo.getMonitorTime())));

            voList.add(vo);

            // 功率计算
            DecimalFormat decimalFormat = new DecimalFormat("#0.00000");
            if (ObjectUtil.isNotEmpty(currentInfo.getMonPv1vol()) && ObjectUtil.isNotEmpty(currentInfo.getMonPv1cur())) {
                vo.setPV1NPower(Double.valueOf(decimalFormat.format(currentInfo.getMonPv1vol() * currentInfo.getMonPv1cur() / 1000)));
            }
            if (ObjectUtil.isNotEmpty(currentInfo.getGridVol()) && ObjectUtil.isNotEmpty(currentInfo.getGridCur())) {
                vo.setGridTotalPower(Double.valueOf(decimalFormat.format(currentInfo.getGridVol() * currentInfo.getGridCur() / 1000)));
            }
        }
        voList = voList.stream().filter(distinctByKey(SkyInvMonitorHistoryVO::getMonitorTime))
                .collect(Collectors.toList());
        return voList;
    }

    @NotNull
    private Map<Date, SkyInvMonitorHistoryVO> getMonitorHistoryVOMap(List<SkyInvMonitorCurrent> current) {
        List<SkyInvMonitorHistoryVO> voList = new ArrayList<>();
        for (SkyInvMonitorCurrent currentInfo: current) {
            SkyInvMonitorHistoryVO vo = new SkyInvMonitorHistoryVO();
            BeanUtil.copyProperties(currentInfo,vo);
            voList.add(vo);

            // 功率计算
            DecimalFormat decimalFormat = new DecimalFormat("#0.00");
            if (ObjectUtil.isNotEmpty(currentInfo.getMonPv1vol()) && ObjectUtil.isNotEmpty(currentInfo.getMonPv1cur())) {
                vo.setPV1NPower(Double.valueOf(decimalFormat.format(currentInfo.getMonPv1vol() * currentInfo.getMonPv1cur())));
            }
            if (ObjectUtil.isNotEmpty(currentInfo.getGridVol()) && ObjectUtil.isNotEmpty(currentInfo.getGridCur())) {
                vo.setGridTotalPower(Double.valueOf(decimalFormat.format(currentInfo.getGridVol() * currentInfo.getGridCur())));
            }
        }
        voList = voList.stream().filter(distinctByKey(SkyInvMonitorHistoryVO::getMonitorTime))
                .collect(Collectors.toList());
        Map<Date, SkyInvMonitorHistoryVO> collect = voList.stream()
                .collect(Collectors.toMap(SkyInvMonitorHistoryVO::getMonitorTime, SkyInvMonitorHistoryVO -> SkyInvMonitorHistoryVO));
        return collect;
    }

    /**
     * 去重
     */
    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> set = ConcurrentHashMap.newKeySet();
        return t -> set.add(keyExtractor.apply(t));
    }

    /**
     * 根据map的key排序
     *
     * @param map 待排序的map
     * @param isDesc 是否降序，true：降序，false：升序
     * @return 排序好的map
     * @author zero 2019/04/08
     */
    public static <K extends Comparable<? super K>, V> Map<K, V> sortByKey(Map<K, V> map, boolean isDesc) {
        Map<K, V> result = Maps.newLinkedHashMap();
        if (isDesc) {
            map.entrySet().stream().sorted(Map.Entry.<K, V>comparingByKey().reversed())
                    .forEachOrdered(e -> result.put(e.getKey(), e.getValue()));
        } else {
            map.entrySet().stream().sorted(Map.Entry.<K, V>comparingByKey())
                    .forEachOrdered(e -> result.put(e.getKey(), e.getValue()));
        }
        return result;
    }

    private void handleData(SkyInvMonitorCurrent deviceNb, SkyInvMonitorCurrentDetailVO resDTO){
        if (deviceNb.getMonPv1vol() == null || deviceNb.getMonPv1cur() == null) {
//            resDTO.setPv1StartStatus(2);
            resDTO.setPv1Power(0D);
        } else {
            resDTO.setPv1Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv1cur(), deviceNb.getMonPv1vol()), 0));
//            resDTO.setPv1StartStatus(1);
        }

        if (deviceNb.getMonPv2cur()==null || deviceNb.getMonPv2vol() == null) {
//            resDTO.setPv2StartStatus(2);
            resDTO.setPv2Power(0D);
        } else {
            resDTO.setPv2Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv2cur(), deviceNb.getMonPv2vol()), 0));
//            resDTO.setPv2StartStatus(1);
        }

        if (deviceNb.getMonPv3cur()==null || deviceNb.getMonPv3vol() == null) {
//            resDTO.setPv3StartStatus(2);
            resDTO.setPv3Power(0D);
        } else {
            resDTO.setPv3Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv3cur(), deviceNb.getMonPv3vol()), 0));
//            resDTO.setPv3StartStatus(1);
        }

        if (deviceNb.getMonPv4cur()==null || deviceNb.getMonPv4vol() == null) {
//            resDTO.setPv4StartStatus(2);
            resDTO.setPv4Power(0D);
        } else {
            resDTO.setPv4Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv4cur(), deviceNb.getMonPv4vol()), 0));
//            resDTO.setPv4StartStatus(1);
        }

        if (deviceNb.getMonPv5cur()==null || deviceNb.getMonPv5vol() == null) {
//            resDTO.setPv5StartStatus(2);
            resDTO.setPv5Power(0D);
        } else {
            resDTO.setPv5Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv5cur(), deviceNb.getMonPv5vol()), 0));
//            resDTO.setPv5StartStatus(1);
        }

        if (deviceNb.getMonPv6cur()==null || deviceNb.getMonPv6vol() == null) {
//            resDTO.setPv6StartStatus(2);
            resDTO.setPv6Power(0D);
        } else {
            resDTO.setPv6Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv6cur(), deviceNb.getMonPv6vol()), 0));
//            resDTO.setPv6StartStatus(1);
        }

        if (deviceNb.getMonPv7cur()==null || deviceNb.getMonPv7vol() == null) {
//            resDTO.setPv7StartStatus(2);
            resDTO.setPv7Power(0D);
        } else {
            resDTO.setPv7Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv7cur(), deviceNb.getMonPv7vol()), 0));
//            resDTO.setPv7StartStatus(1);
        }

        if (deviceNb.getMonPv8cur()==null || deviceNb.getMonPv8vol() == null) {
//            resDTO.setPv8StartStatus(2);
            resDTO.setPv8Power(0D);
        } else {
            resDTO.setPv8Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv8cur(), deviceNb.getMonPv8vol()), 0));
//            resDTO.setPv8StartStatus(1);
        }

        if (deviceNb.getMonPv9cur()==null || deviceNb.getMonPv9vol() == null) {
//            resDTO.setPv9StartStatus(2);
            resDTO.setPv9Power(0D);
        } else {
            resDTO.setPv9Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv9cur(), deviceNb.getMonPv9vol()), 0));
//            resDTO.setPv9StartStatus(1);
        }

        if (deviceNb.getMonPv10cur()==null || deviceNb.getMonPv10vol() == null) {
//            resDTO.setPv10StartStatus(2);
            resDTO.setPv10Power(0D);
        } else {
            resDTO.setPv10Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv10cur(), deviceNb.getMonPv10vol()),0));
//            resDTO.setPv10StartStatus(1);
        }

        if (deviceNb.getMonPv11cur()==null || deviceNb.getMonPv11vol() == null) {
//            resDTO.setPv11StartStatus(2);
            resDTO.setPv11Power(0D);
        } else {
            resDTO.setPv11Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv11cur(), deviceNb.getMonPv11vol()),0));
//            resDTO.setPv11StartStatus(1);
        }

        if (deviceNb.getMonPv12cur()==null || deviceNb.getMonPv12vol() == null) {
//            resDTO.setPv12StartStatus(2);
            resDTO.setPv12Power(0D);
        } else {
            resDTO.setPv12Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv12cur(), deviceNb.getMonPv12vol()),0));
//            resDTO.setPv12StartStatus(1);
        }

        if (deviceNb.getMonPv13cur()==null || deviceNb.getMonPv13vol() == null) {
//            resDTO.setPv13StartStatus(2);
            resDTO.setPv13Power(0D);
        } else {
            resDTO.setPv13Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv13cur(), deviceNb.getMonPv13vol()),0));
//            resDTO.setPv13StartStatus(1);
        }

        if (deviceNb.getMonPv14cur()==null || deviceNb.getMonPv14vol() == null) {
//            resDTO.setPv14StartStatus(2);
            resDTO.setPv14Power(0D);
        } else {
            resDTO.setPv14Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv14cur(), deviceNb.getMonPv14vol()),0));
//            resDTO.setPv14StartStatus(1);
        }

        if (deviceNb.getMonPv15cur()==null || deviceNb.getMonPv15vol() == null) {
//            resDTO.setPv15StartStatus(2);
            resDTO.setPv15Power(0D);
        } else {
            resDTO.setPv15Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv15cur(), deviceNb.getMonPv15vol()),0));
//            resDTO.setPv15StartStatus(1);
        }

        if (deviceNb.getMonPv16cur()==null || deviceNb.getMonPv16vol() == null) {
//            resDTO.setPv16StartStatus(2);
            resDTO.setPv16Power(0D);
        } else {
            resDTO.setPv16Power(DoubleUtils.round(DoubleUtils.mul(deviceNb.getMonPv16cur(), deviceNb.getMonPv16vol()),0));
//            resDTO.setPv16StartStatus(1);
        }
    }


}
