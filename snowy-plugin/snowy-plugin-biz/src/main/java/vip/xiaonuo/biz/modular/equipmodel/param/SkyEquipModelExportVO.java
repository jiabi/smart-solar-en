package vip.xiaonuo.biz.modular.equipmodel.param;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author: wangjian
 * @date: 2023/10/11
 * @Modified By:
 */
@Getter
@Setter
public class SkyEquipModelExportVO {
    /** 设备型号ID */
    @ApiModelProperty(value = "设备型号ID", position = 1)
    private String id;

    /** 设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪 */
    @ApiModelProperty(value = "设备类型：1.采集器；2.逆变器；3.混合逆变器；4.电池；5.储能逆变器；6.电表；7.汇流箱；8.环境检测仪", position = 2)
    private Integer equType;

    /** 型号名称 */
    @ApiModelProperty(value = "型号名称", position = 3)
    private String modelName;

    /** 厂家ID */
    @ApiModelProperty(value = "厂家ID", position = 4)
    private String companyId;

    /** 额定功率 */
    @ApiModelProperty(value = "额定功率", position = 5)
    private Double ratedPower;

    /** 设备详细类型：0.单项；1.三项；2.单向表；4.双向表；5.棒式；6.导轨 */
    @ApiModelProperty(value = "设备详细类型：0.单项；1.三项；2.单向表；4.双向表；5.棒式；6.导轨", position = 6)
    private Integer detailType;

    /** MPPT路数 */
    @ApiModelProperty(value = "MPPT路数", position = 7)
    private Integer mpptCount;

    /** 电池电压类型：1.LV-48L */
    @ApiModelProperty(value = "电池电压类型：1.LV-48L", position = 8)
    private Integer batVolType;

    /** 电池类型：1.铅酸 */
    @ApiModelProperty(value = "电池类型：1.铅酸", position = 9)
    private Integer batType;

    /** 电池模式：1.电压模式 */
    @ApiModelProperty(value = "电池模式：1.电压模式", position = 10)
    private Integer batMode;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 11)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 12)
    private String createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 13)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 14)
    private String updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 15)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 16)
    private String tenantId;
}
