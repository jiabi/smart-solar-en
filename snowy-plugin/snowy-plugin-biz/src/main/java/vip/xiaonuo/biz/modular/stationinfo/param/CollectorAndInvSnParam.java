package vip.xiaonuo.biz.modular.stationinfo.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/9/10 10:06
 */
@Getter
@Setter
public class CollectorAndInvSnParam {
    // 采集器sn
    private String colSn;
    // 逆变器sn
    private String invSn;
}
