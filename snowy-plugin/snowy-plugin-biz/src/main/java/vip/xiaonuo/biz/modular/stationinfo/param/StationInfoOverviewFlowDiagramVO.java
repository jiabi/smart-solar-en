package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 电站概览流动图信息
 */
@Getter
@Setter
public class StationInfoOverviewFlowDiagramVO {
    /** 发电功率 */
    @ApiModelProperty(value = "发电功率")
    private Double elecEfficiency;

    /** 装机容量 */
    @ApiModelProperty(value = "装机容量")
    private Double stationSize;

    /** 系统功率比 */
    @ApiModelProperty(value = "系统功率比")
    private String stationSysRate;

    /** 电池功率 */
    @ApiModelProperty(value = "电池功率")
    private Double batPower;

    /** 电池容量 */
    @ApiModelProperty(value = "电池容量")
    private Double batSoc;

    /** 电网功率 */
    @ApiModelProperty(value = "电网功率")
    private Double monPower;

    /** 用电功率 */
    @ApiModelProperty(value = "用电功率")
    private Double loadPower;

    /** 电表功率 */
    @ApiModelProperty(value = "电表功率")
    private Double meterPower;
}
