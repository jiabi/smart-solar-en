/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.equrelationship.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.equrelationship.param.*;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.equrelationship.entity.SkyEquRelationship;
import vip.xiaonuo.biz.modular.equrelationship.service.SkyEquRelationshipService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 通讯关系控制器
 *
 * @author 全佳璧
 * @date  2023/10/23 09:50
 */
@Api(tags = "通讯关系控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyEquRelationshipController {

    @Resource
    private SkyEquRelationshipService skyEquRelationshipService;

    /**
     * 获取通讯关系分页
     *
     * @author 全佳璧
     * @date  2023/10/23 09:50
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取通讯关系分页")
//    @SaCheckPermission("/biz/equrelationship/page")
    @GetMapping("/biz/equrelationship/page")
    public CommonResult<Page<SkyEquRelationship>> page(SkyEquRelationshipPageParam skyEquRelationshipPageParam) {
        return CommonResult.data(skyEquRelationshipService.page(skyEquRelationshipPageParam));
    }

    /**
     * 添加通讯关系
     *
     * @author 全佳璧
     * @date  2023/10/23 09:50
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加通讯关系")
    @CommonLog("添加通讯关系")
    @SaCheckPermission("/biz/equrelationship/add")
    @PostMapping("/biz/equrelationship/add")
    public CommonResult<String> add(@RequestBody @Valid SkyEquRelationshipAddParam skyEquRelationshipAddParam) {
        skyEquRelationshipService.add(skyEquRelationshipAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑通讯关系
     *
     * @author 全佳璧
     * @date  2023/10/23 09:50
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑通讯关系")
    @CommonLog("编辑通讯关系")
    @SaCheckPermission("/biz/equrelationship/edit")
    @PostMapping("/biz/equrelationship/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyEquRelationshipEditParam skyEquRelationshipEditParam) {
        skyEquRelationshipService.edit(skyEquRelationshipEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除通讯关系
     *
     * @author 全佳璧
     * @date  2023/10/23 09:50
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除通讯关系")
    @CommonLog("删除通讯关系")
    @SaCheckPermission("/biz/equrelationship/delete")
    @PostMapping("/biz/equrelationship/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyEquRelationshipIdParam> skyEquRelationshipIdParamList) {
        skyEquRelationshipService.delete(skyEquRelationshipIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取通讯关系详情
     *
     * @author 全佳璧
     * @date  2023/10/23 09:50
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取通讯关系详情")
    @SaCheckPermission("/biz/equrelationship/detail")
    @GetMapping("/biz/equrelationship/detail")
    public CommonResult<SkyEquRelationship> detail(@Valid SkyEquRelationshipIdParam skyEquRelationshipIdParam) {
        return CommonResult.data(skyEquRelationshipService.detail(skyEquRelationshipIdParam));
    }

    /**
     * 通过SN获取通讯关系
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("通过SN获取通讯关系")
//    @SaCheckPermission("/biz/equrelationship/relationBySN")
    @PostMapping("/biz/equrelationship/relationBySN")
    public CommonResult<SkyEquRelationshipTreeVO> relationBySN(@RequestBody @Valid SkyEquRelationshipSNParam snParam) {
        return CommonResult.data(skyEquRelationshipService.relationBySN(snParam));
    }

    /**
     * 根据设备sn获取采集器sn
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("根据设备sn获取采集器sn")
//    @SaCheckPermission("/biz/equrelationship/getCollectorSn")
    @PostMapping("/biz/equrelationship/getCollectorSn")
    public CommonResult<String> getCollectorSn(@RequestBody @Valid SkyEquRelationshipSNParam snParam) {
        return CommonResult.data(skyEquRelationshipService.getCollectorSn(snParam));
    }

}
