package vip.xiaonuo.biz.modular.invinfo.param;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/11 17:41
 */
@Getter
@Setter
public class SkyInvBindPsParam {
    /** 逆变器SN */
    private List<String> invSnList;

    /** 电站id */
    private String stationId;
}
