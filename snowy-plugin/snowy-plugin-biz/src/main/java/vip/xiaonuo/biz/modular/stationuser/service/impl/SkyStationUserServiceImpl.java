/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationuser.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.mapper.SkyStationUserMapper;
import vip.xiaonuo.biz.modular.stationuser.param.SkyStationUserAddParam;
import vip.xiaonuo.biz.modular.stationuser.param.SkyStationUserEditParam;
import vip.xiaonuo.biz.modular.stationuser.param.SkyStationUserIdParam;
import vip.xiaonuo.biz.modular.stationuser.param.SkyStationUserPageParam;
import vip.xiaonuo.biz.modular.stationuser.service.SkyStationUserService;

import java.util.List;

/**
 * 电站用户关系Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/04/01 16:32
 **/
@Service
public class SkyStationUserServiceImpl extends ServiceImpl<SkyStationUserMapper, SkyStationUser> implements SkyStationUserService {


    @Override
    public Page<SkyStationUser> page(SkyStationUserPageParam skyStationUserPageParam) {
        QueryWrapper<SkyStationUser> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(skyStationUserPageParam.getStationId())) {
            queryWrapper.lambda().like(SkyStationUser::getStationId, skyStationUserPageParam.getStationId());
        }
        if(ObjectUtil.isNotEmpty(skyStationUserPageParam.getUserId())) {
            queryWrapper.lambda().like(SkyStationUser::getUserId, skyStationUserPageParam.getUserId());
        }
        if(ObjectUtil.isNotEmpty(skyStationUserPageParam.getInstallerIdL1())) {
            queryWrapper.lambda().like(SkyStationUser::getInstallerIdL1, skyStationUserPageParam.getInstallerIdL1());
        }
        if(ObjectUtil.isNotEmpty(skyStationUserPageParam.getInstallerIdL2())) {
            queryWrapper.lambda().like(SkyStationUser::getInstallerIdL2, skyStationUserPageParam.getInstallerIdL2());
        }
        if(ObjectUtil.isAllNotEmpty(skyStationUserPageParam.getSortField(), skyStationUserPageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(skyStationUserPageParam.getSortOrder());
            queryWrapper.orderBy(true, skyStationUserPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(skyStationUserPageParam.getSortField()));
        } else {
            queryWrapper.lambda().orderByAsc(SkyStationUser::getId);
        }
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyStationUserAddParam skyStationUserAddParam) {
        SkyStationUser skyStationUser = BeanUtil.toBean(skyStationUserAddParam, SkyStationUser.class);
        this.save(skyStationUser);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyStationUserEditParam skyStationUserEditParam) {
        SkyStationUser skyStationUser = this.queryEntity(skyStationUserEditParam.getId());
        BeanUtil.copyProperties(skyStationUserEditParam, skyStationUser);
        this.updateById(skyStationUser);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyStationUserIdParam> skyStationUserIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyStationUserIdParamList, SkyStationUserIdParam::getId));
    }

    @Override
    public SkyStationUser detail(SkyStationUserIdParam skyStationUserIdParam) {
        return this.queryEntity(skyStationUserIdParam.getId());
    }

    @Override
    public SkyStationUser queryEntity(String id) {
        SkyStationUser skyStationUser = this.getById(id);
        if(ObjectUtil.isEmpty(skyStationUser)) {
            throw new CommonException("电站用户关系不存在，id值为：{}", id);
        }
        return skyStationUser;
    }

}
