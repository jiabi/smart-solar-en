/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.eququality.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyDeleteFlagEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.equipmodel.entity.SkyEquipModel;
import vip.xiaonuo.biz.modular.equipmodel.service.SkyEquipModelService;
import vip.xiaonuo.biz.modular.eququality.param.*;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.service.SkyStationUserService;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.biz.modular.eququality.entity.SkyEquQuality;
import vip.xiaonuo.biz.modular.eququality.mapper.SkyEquQualityMapper;
import vip.xiaonuo.biz.modular.eququality.service.SkyEquQualityService;
import vip.xiaonuo.dev.api.DevDictApi;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 设备质保信息Service接口实现类
 *
 * @author 全佳璧
 * @date  2023/09/25 14:20
 **/
@Service
public class SkyEquQualityServiceImpl extends ServiceImpl<SkyEquQualityMapper, SkyEquQuality> implements SkyEquQualityService {

    private static final Logger logger = LoggerFactory.getLogger(SkyEquQualityServiceImpl.class);

    @Resource
    private SkyEquQualityMapper skyEquQualityMapper;

    @Resource
    private SkyEquipModelService skyEquipModelService;

    @Resource
    private SkyInvInfoMapper skyInvInfoMapper;

    @Resource
    private SkyStationUserService skyStationUserService;

    @Override
    public Page<SkyEquQualityPageVO> page(SkyEquQualityPageParam skyEquQualityPageParam) {

        Page<SkyEquQualityPageVO> pageDataModel = new Page<>(skyEquQualityPageParam.getCurrent(), skyEquQualityPageParam.getSize());
        List<SkyEquQualityPageVO> list = skyEquQualityMapper.pageSkyEquQuality(skyEquQualityPageParam);

        for (SkyEquQualityPageVO vo:list) {
            if (!Objects.isNull(vo.getEndDate())) {
                boolean status = this.criticalStatus(vo.getEndDate());
                if (status) {
                    vo.setCriticalStatus(1);
                } else {
                    vo.setCriticalStatus(0);
                }
            }
        }
        pageDataModel.setRecords(list);
        pageDataModel.setTotal(skyEquQualityMapper.countNum(skyEquQualityPageParam));
        return pageDataModel;

//        QueryWrapper<SkyEquQuality> queryWrapper = new QueryWrapper<>();
//        if (ObjectUtil.isNotEmpty(skyEquQualityPageParam.getEquSn())) {
//            queryWrapper.lambda().eq(SkyEquQuality::getEquSn, skyEquQualityPageParam.getEquSn());
//        }
//        if(ObjectUtil.isNotEmpty(skyEquQualityPageParam.getEquType())) {
//            queryWrapper.lambda().eq(SkyEquQuality::getEquType, skyEquQualityPageParam.getEquType());
//        }
//        if(ObjectUtil.isNotEmpty(skyEquQualityPageParam.getEquModel())) {
//            queryWrapper.lambda().eq(SkyEquQuality::getEquModel, skyEquQualityPageParam.getEquModel());
//        }
//        if(ObjectUtil.isNotEmpty(skyEquQualityPageParam.getStartStartDate()) && ObjectUtil.isNotEmpty(skyEquQualityPageParam.getEndStartDate())) {
//            queryWrapper.lambda().between(SkyEquQuality::getStartDate, skyEquQualityPageParam.getStartStartDate(), skyEquQualityPageParam.getEndStartDate());
//        }
//        if(ObjectUtil.isNotEmpty(skyEquQualityPageParam.getStartEndDate()) && ObjectUtil.isNotEmpty(skyEquQualityPageParam.getEndEndDate())) {
//            queryWrapper.lambda().between(SkyEquQuality::getEndDate, skyEquQualityPageParam.getStartEndDate(), skyEquQualityPageParam.getEndEndDate());
//        }
//        if(ObjectUtil.isNotEmpty(skyEquQualityPageParam.getTimeLimit())) {
//            queryWrapper.lambda().eq(SkyEquQuality::getTimeLimit, skyEquQualityPageParam.getTimeLimit());
//        }
//        if(ObjectUtil.isNotEmpty(skyEquQualityPageParam.getCriticalStatus())) {
//            queryWrapper.lambda().eq(SkyEquQuality::getCriticalStatus, skyEquQualityPageParam.getCriticalStatus());
//        }
//        queryWrapper.lambda().orderByAsc(SkyEquQuality::getEquSn);
//        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    /**
     * @description: 判断质保状态是否临期
     * @author: wangjian
     * @date: 2023/10/6 17:47
     **/
    private boolean criticalStatus(Date endDate){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String end = simpleDateFormat.format(endDate);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate endTime = LocalDate.parse(end,dateTimeFormatter);
        Integer monsNum = skyEquQualityMapper.getCriticalTime();
        if (Objects.isNull(monsNum)) {
            monsNum =0;
        }
        LocalDate limitTime = endTime.minusMonths(monsNum);

        return limitTime.isAfter(LocalDate.now());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyEquQualityAddParam skyEquQualityAddParam) {
        // 判断是否临期
        if (!Objects.isNull(skyEquQualityAddParam.getEndDate())) {
            boolean status = this.criticalStatus(skyEquQualityAddParam.getEndDate());
            if (status) {
                skyEquQualityAddParam.setCriticalStatus(1);
            } else {
                skyEquQualityAddParam.setCriticalStatus(0);
            }
        }
        SkyEquQuality skyEquQuality = BeanUtil.toBean(skyEquQualityAddParam, SkyEquQuality.class);
        this.save(skyEquQuality);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyEquQualityEditParam skyEquQualityEditParam) {
        // 判断是否临期
        if (!Objects.isNull(skyEquQualityEditParam.getEndDate())) {
            boolean status = this.criticalStatus(skyEquQualityEditParam.getEndDate());
            if (status) {
                skyEquQualityEditParam.setCriticalStatus(1);
            } else {
                skyEquQualityEditParam.setCriticalStatus(0);
            }
        }
        SkyEquQuality skyEquQuality = this.queryEntity(skyEquQualityEditParam.getEquSn());
        BeanUtil.copyProperties(skyEquQualityEditParam, skyEquQuality);
        this.updateById(skyEquQuality);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyEquQualityIdParam> skyEquQualityIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyEquQualityIdParamList, SkyEquQualityIdParam::getEquSn));
    }

    @Override
    public SkyEquQuality detail(SkyEquQualityIdParam skyEquQualityIdParam) {
        SkyEquQuality info = this.queryEntity(skyEquQualityIdParam.getEquSn());
        // 判断是否临期
        if (!Objects.isNull(info.getEndDate())) {
            boolean status = this.criticalStatus(info.getEndDate());
            if (status) {
                info.setCriticalStatus(1);
            } else {
                info.setCriticalStatus(0);
            }
        }
        return info;
    }

    @Override
    public SkyEquQuality queryEntity(String id) {
        SkyEquQuality skyEquQuality = this.getById(id);
        if(ObjectUtil.isEmpty(skyEquQuality)) {
            throw new CommonException("设备质保信息不存在，id值为：{}", id);
        }
        return skyEquQuality;
    }

    @Override
    public List<Map<String, Object>> exportInfoList(SkyEquQualityExportParam exportParam) {
        List<Map<String, Object>> list = new ArrayList<>();
        if (exportParam!=null && !CollectionUtils.isEmpty(exportParam.getSns()) && exportParam.getSns().size() > 0) {
            List<SkyEquQualityPageVO> infoList = skyEquQualityMapper.getBySns(exportParam.getSns());
            for (SkyEquQualityPageVO info:infoList) {
                // 判断是否临期
                if (!Objects.isNull(info.getEndDate())) {
                    boolean status = this.criticalStatus(info.getEndDate());
                    if (status) {
                        info.setCriticalStatus(1);
                    } else {
                        info.setCriticalStatus(0);
                    }
                }
                Map<String, Object> map = JSON.parseObject(JSON.toJSONString(info));
                list.add(map);
            }
            return list;
        }

        List<SkyEquQualityPageVO> exportInfoList = skyEquQualityMapper.exportInfoList(exportParam);
        for (SkyEquQualityPageVO info:exportInfoList) {
            Map<String, Object> map = JSON.parseObject(JSON.toJSONString(info));
            list.add(map);
        }
        return list;
    }

    @Override
    public List<SkyEquQualityExcelVO> exportInfoList2(SkyEquQualityExportParam exportParam) {
        List<SkyEquQualityExcelVO> res = new ArrayList<>();
        List<SkyEquQualityPageVO> infoList = null;
        if (exportParam!=null && !CollectionUtils.isEmpty(exportParam.getSns()) && exportParam.getSns().size() > 0) {
            infoList = skyEquQualityMapper.getBySns(exportParam.getSns());
            for (SkyEquQualityPageVO info:infoList) {
                // 判断是否临期
                if (!Objects.isNull(info.getEndDate())) {
                    boolean status = this.criticalStatus(info.getEndDate());
                    if (status) {
                        info.setCriticalStatus(1);
                    } else {
                        info.setCriticalStatus(0);
                    }
                }
            }
        }
        infoList = skyEquQualityMapper.exportInfoList(exportParam);
        for (SkyEquQualityPageVO vo:infoList) {
            SkyEquQualityExcelVO excelVO = new SkyEquQualityExcelVO();
            BeanUtil.copyProperties(vo,excelVO);
            res.add(excelVO);
        }
        return res;
    }

    @Override
    public List<SkySnAndParamerParam> getEquipSnList(SkyPsIdParam skyPsIdParam) {
//        QueryWrapper<SkyEquQuality> queryWrapper = new QueryWrapper<>();
//        queryWrapper.lambda().select(SkyEquQuality::getEquSn);
//        List<String> collect = this.list(queryWrapper).stream().map(SkyEquQuality::getEquSn).collect(Collectors.toList());

        QueryWrapper<SkyInvInfo> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotEmpty(skyPsIdParam.getStationId())) {
            queryWrapper.lambda().eq(SkyInvInfo::getStationId,skyPsIdParam.getStationId());
        }
        //加权限判定
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if(!roleCodeList.contains(SkyRoleCodeEnum.SUPERADMIN.getValue())){
            List<String> stationIds = jurisdiction(skyPsIdParam,roleCodeList,loginUser);
            if(CollectionUtils.isEmpty(stationIds)){
                return null;
            }
            queryWrapper.lambda().in(SkyInvInfo::getStationId,stationIds);
        }
        queryWrapper.lambda().orderByDesc(SkyInvInfo::getCreateTime);
        List<SkyInvInfo> invInfoList = skyInvInfoMapper.selectList(queryWrapper);
        List<String> snList = invInfoList.stream().map(SkyInvInfo::getInvSn).collect(Collectors.toList());
        List<SkySnAndParamerParam> list = skyInvInfoMapper.getEquipSnAndParamList();
        Map<String, String> map = null;
        if (list != null && list.size() > 0) {
            map = list.stream().collect(Collectors.toMap(SkySnAndParamerParam::getSn, SkySnAndParamerParam::getParam));
        }
        List<SkySnAndParamerParam> res = new ArrayList<>();
        for (String sn:snList) {
            SkySnAndParamerParam param = new SkySnAndParamerParam();
            if (map != null) {
                param.setParam(map.getOrDefault(sn,null));
            }
            param.setSn(sn);
            res.add(param);
        }
        return res;
    }

    /**
     * 获取不同权限下所属电站
     * @return
     */
    public List<String> jurisdiction(SkyPsIdParam skyPsIdParam,List<String> roleCodeList,SaBaseLoginUser loginUser){

        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
         if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            skyPsIdParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            skyPsIdParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            skyPsIdParam.setStationContactId(loginUser.getId());
        }else{
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        LambdaQueryWrapper<SkyStationUser> wrapper = Wrappers.lambdaQuery(SkyStationUser.class);
        if(StringUtils.isNotEmpty(skyPsIdParam.getDistributor())){
            wrapper.eq(SkyStationUser::getInstallerIdL2,skyPsIdParam.getDistributor());
        }
        if(StringUtils.isNotEmpty(skyPsIdParam.getStationCompany())){
            wrapper.eq(SkyStationUser::getInstallerIdL1,skyPsIdParam.getStationCompany());
        }
        if(StringUtils.isNotEmpty(skyPsIdParam.getStationContactId())){
            wrapper.eq(SkyStationUser::getUserId,skyPsIdParam.getStationContactId());
        }
        List<SkyStationUser> skyStationUserList = skyStationUserService.list(wrapper);
        if(!CollectionUtils.isEmpty(skyStationUserList)){
            return skyStationUserList.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
        }
        return null;
    }


    @Override
    public Boolean checkEquipSn(EquipSnParam equipSnParam) {
        QueryWrapper<SkyEquQuality> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotEmpty(equipSnParam.getEquSn())) {
            queryWrapper.lambda().eq(SkyEquQuality::getEquSn, equipSnParam.getEquSn());
        }
        long count = this.count(queryWrapper);
        return count>0 ? true:false;
    }

    @Override
    public List<Map<String, Object>> getExcelModel() throws ParseException {
        List<Map<String, Object>> list = new ArrayList<>();
        SkyEquQualityAddParam info = new SkyEquQualityAddParam();
        LocalDate localDate = LocalDate.now();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        info.setEquSn("SR1CS6N5P4S002");
        info.setEquModel("INV7000W");
        info.setEquType(1);
        info.setStartDate(sdf.parse(localDate.format(DateTimeFormatter.ISO_LOCAL_DATE)));
        info.setEndDate(sdf.parse(localDate.plusYears(1L).format(DateTimeFormatter.ISO_LOCAL_DATE)));
        info.setCriticalStatus(1);
        info.setTimeLimit(1.5D);
        Map<String, Object> map = JSONObject.parseObject(JSON.toJSONString(info));
        list.add(map);
        return list;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<ImportErrorReturnVO> importByExcel(List<SkyEquQualityImportModel> models) {
        logger.info("导入质保信息-入参：{}",models);
        List<ImportErrorReturnVO> errors = new ArrayList<>();
        Boolean insertFlag = true;
        List<SkyEquQuality> insertList = new ArrayList<>();

        // 所有设备SN、设备类型名称
        List<SkyEquQuality> allList = this.list();
        Set<String> allEquSn = allList.stream().map(SkyEquQuality::getEquSn).collect(Collectors.toSet());
        List<SkyEquipModel> equipModelList = skyEquipModelService.list();
        Set<String> allEquipModelName = equipModelList.stream().map(SkyEquipModel::getModelName).collect(Collectors.toSet());
        Map<String, String> nameIdMap = equipModelList.stream().collect(Collectors.toMap(SkyEquipModel::getModelName, SkyEquipModel::getId));

        // 校验
        for (SkyEquQualityImportModel model:models) {
            // 第二行为示例数据
            if (model.getRowNum()==2) {
                continue;
            }
            if (StringUtils.isNotEmpty(model.getRowTips())) {
                insertFlag = false;
                ImportErrorReturnVO error = new ImportErrorReturnVO();
                error.setRowNum(model.getRowNum());
                error.setContent(model.getRowTips());
                errors.add(error);
            }

            if (allEquSn.contains(model.getEquSn())) {
                insertFlag = false;
                ImportErrorReturnVO error = new ImportErrorReturnVO();
                error.setRowNum(model.getRowNum());
                error.setContent(model.getRowTips()+ "设备SN号：" + model.getEquSn() + "，已存在！");
                errors.add(error);
            }
            if (!allEquipModelName.contains(model.getEquModel())) {
                insertFlag = false;
                ImportErrorReturnVO error = new ImportErrorReturnVO();
                error.setRowNum(model.getRowNum());
                error.setContent(model.getRowTips()+ "设备类型：" + model.getEquModel() + "，不存在！");
                errors.add(error);
            }
            // 质保期限
            if (!Objects.isNull(model.getStartDate()) && !Objects.isNull(model.getTimeLimit())) {
//                LocalDateTime startTime = LocalDateTime.ofInstant(model.getStartDate().toInstant(), ZoneId.systemDefault());
//                LocalDateTime endTime = startTime.plusYears(model.getTimeLimit().longValue());
//                model.setEndDate(Date.from(endTime.atZone(ZoneId.systemDefault()).toInstant()));
                model.setEndDate(String.valueOf(LocalDate.parse(model.getStartDate(), DateTimeFormatter.ISO_LOCAL_DATE).plusYears(model.getTimeLimit().longValue())));
            } else {
                insertFlag = false;
                if (Objects.isNull(model.getStartDate())) {
                    ImportErrorReturnVO error = new ImportErrorReturnVO();
                    error.setRowNum(model.getRowNum());
                    error.setContent(model.getRowTips()+ "发货日期：" + model.getEquModel() + "，不存在！");
                    errors.add(error);
                }
                if (Objects.isNull(model.getTimeLimit())) {
                    ImportErrorReturnVO error = new ImportErrorReturnVO();
                    error.setRowNum(model.getRowNum());
                    error.setContent(model.getRowTips()+ "质保时长：" + model.getEquModel() + "，不存在！");
                    errors.add(error);
                }
            }
            // 判断是否临期
            if (!Objects.isNull(model.getEndDate())) {
                boolean status = false;
                try {
                    status = this.criticalStatus(new SimpleDateFormat("yyyy-MM-dd").parse(model.getEndDate()));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
                if (status) {
                    model.setCriticalStatus(1);
                } else {
                    model.setCriticalStatus(0);
                }
            }
            SkyEquQuality equQuality = new SkyEquQuality();
            BeanUtil.copyProperties(model,equQuality);
            equQuality.setEquModel(nameIdMap.get(model.getEquModel()));
            equQuality.setDeleteFlag(SkyDeleteFlagEnum.NOTDELETE.getValue());
            insertList.add(equQuality);
        }

        // insert
        if (Boolean.TRUE.equals(insertFlag) && !insertList.isEmpty()) {
            try {
                skyEquQualityMapper.setNotDelete(insertList.stream().map(SkyEquQuality::getEquSn).collect(Collectors.toList()));
                // 批量入库
                this.saveOrUpdateBatch(insertList);
                return null;
            } catch (Exception e) {
                logger.info("导入失败",e);
                ImportErrorReturnVO error = new ImportErrorReturnVO();
                error.setRowNum(0);
                error.setContent("全部数据保存失败");
                errors.add(error);
            }
        }

        // 返回错误信息
//        List<Map<String, Object>> list = new ArrayList<>();
//        for (ImportErrorReturnVO error : errors) {
//            Map<String, Object> map = JSONObject.parseObject(JSON.toJSONString(error));
//            list.add(map);
//        }
//        return list;
        return errors;
    }
}
