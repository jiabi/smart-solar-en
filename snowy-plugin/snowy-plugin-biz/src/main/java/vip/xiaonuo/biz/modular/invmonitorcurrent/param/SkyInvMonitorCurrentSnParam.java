package vip.xiaonuo.biz.modular.invmonitorcurrent.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @Author wangjian
 * @Date 2023/10/12 16:33
 **/
@Getter
@Setter
public class SkyInvMonitorCurrentSnParam {
    /** 设备SN */
    @ApiModelProperty(value = "设备SN", required = true)
    @NotBlank(message = "设备SN不能为空")
    private String invSn;
}
