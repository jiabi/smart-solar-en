package vip.xiaonuo.biz.modular.stationinfo.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/2/28 17:41
 */
@Getter
@Setter
public class PsOverviewHisPowerProportionVO {
    /** 发电量 */
    private Double energy;

    /** 发电用于负载的电量 */
    private Double energyToLoad;

    /** 发电用于电池充电的量 */
    private Double energyToCharge;

    /** 发电用于并网的电量 */
    private Double energyToGrid;

    /** 用电量 */
    private Double usedEnergy;

    /** 用电来自发电的量 */
    private Double usedFromEnergy;

    /** 用电来自电池放电的量 */
    private Double usedFromDischarge;

    /** 用电来自购电的量 */
    private Double usedFromPurchase;
}
