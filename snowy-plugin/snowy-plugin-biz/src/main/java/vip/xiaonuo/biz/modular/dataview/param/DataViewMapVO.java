package vip.xiaonuo.biz.modular.dataview.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/7/10 14:12
 */
@Getter
@Setter
public class DataViewMapVO {
    /** 电站总数 */
    private Integer stationTotal;

    /** 设备总数 */
    private Integer equipTotal;

    /** 总装机容量 */
    private Double installedCapacity;

    /** 经度 */
    private String areaLng;

    /** 纬度 */
    private String areaLat;

    /** 城市 */
    private String city;
}
