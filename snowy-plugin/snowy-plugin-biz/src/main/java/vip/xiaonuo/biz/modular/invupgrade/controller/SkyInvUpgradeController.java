/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invupgrade.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.invupgrade.param.*;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.invupgrade.entity.SkyInvUpgrade;
import vip.xiaonuo.biz.modular.invupgrade.service.SkyInvUpgradeService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 逆变器升级控制器
 *
 * @author 全佳璧
 * @date  2024/04/17 19:49
 */
@Api(tags = "逆变器升级控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyInvUpgradeController {

    @Resource
    private SkyInvUpgradeService skyInvUpgradeService;

    /**
     * 获取逆变器升级分页
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取逆变器升级分页")
    @SaCheckPermission("/biz/invupgrade/page")
    @PostMapping("/biz/invupgrade/page")
    public CommonResult<Page<SkyInvUpgradePageVO>> page(@RequestBody SkyInvUpgradePageParam skyInvUpgradePageParam) {
        return CommonResult.data(skyInvUpgradeService.page(skyInvUpgradePageParam));
    }

    /**
     * 添加逆变器升级
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加逆变器升级")
    @CommonLog("添加逆变器升级")
    @SaCheckPermission("/biz/invupgrade/add")
    @PostMapping("/biz/invupgrade/add")
    public CommonResult<String> add(@RequestBody @Valid SkyInvUpgradeAddParam skyInvUpgradeAddParam) {
        skyInvUpgradeService.add(skyInvUpgradeAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑逆变器升级
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑逆变器升级")
    @CommonLog("编辑逆变器升级")
    @SaCheckPermission("/biz/invupgrade/edit")
    @PostMapping("/biz/invupgrade/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyInvUpgradeEditParam skyInvUpgradeEditParam) {
        skyInvUpgradeService.edit(skyInvUpgradeEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除逆变器升级
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除逆变器升级")
    @CommonLog("删除逆变器升级")
    @SaCheckPermission("/biz/invupgrade/delete")
    @PostMapping("/biz/invupgrade/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyInvUpgradeIdParam> skyInvUpgradeIdParamList) {
        skyInvUpgradeService.delete(skyInvUpgradeIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取逆变器升级详情
     *
     * @author 全佳璧
     * @date  2024/04/17 19:49
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取逆变器升级详情")
    @SaCheckPermission("/biz/invupgrade/detail")
    @GetMapping("/biz/invupgrade/detail")
    public CommonResult<SkyInvUpgrade> detail(@Valid SkyInvUpgradeIdParam skyInvUpgradeIdParam) {
        return CommonResult.data(skyInvUpgradeService.detail(skyInvUpgradeIdParam));
    }

    /**
     * 逆变器升级
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("逆变器升级")
//    @SaCheckPermission("/biz/invupgrade/upgrade")
    @PostMapping("/biz/invupgrade/upgrade")
    public CommonResult<String> upgrade(@RequestBody SkyInvUpgradeParam skyInvUpgradeParam) {
        skyInvUpgradeService.upgrade(skyInvUpgradeParam);
        return CommonResult.ok();
    }

}
