package vip.xiaonuo.biz.modular.collectorupgrade.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/5/5 21:38
 */

@Getter
@Setter
public class SkyCollectorUpgradeParam {

    /** 设备SN */
    @ApiModelProperty(value = "设备SN", position = 1)
    private List<SkyCollectorInvSnParam> equSn;

    /** 升级包id */
    @ApiModelProperty(value = "升级包id", required = true, position = 2)
    @NotBlank(message = "equBinId not null")
    private String equBinId;

    /** 发送时间(YYYY-MM-dd HH:mm:ss)例:2024-02-01 09:10:20 **/
    private String sendTime;

    /** 请求ID (云端下发指令唯一标识) **/
    private String requestId;

    /** 新固件的版本号 **/
    private String newVersion;

    /** 固件下载地址 **/
    private String downloadURL;

    /** 升级模块类型值 */
    private Integer upgradeModuleType;

    /** 升级模块 */
    private String upgradeModule;

    /** 型号名称 */
    private String modelName;

}
