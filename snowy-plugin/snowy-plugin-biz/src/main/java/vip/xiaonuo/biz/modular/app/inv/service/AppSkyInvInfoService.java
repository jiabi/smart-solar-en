package vip.xiaonuo.biz.modular.app.inv.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import vip.xiaonuo.biz.modular.app.entity.PageResDTO;
import vip.xiaonuo.biz.modular.app.inv.param.AppSkyInvInfoPageReqAllParam;
import vip.xiaonuo.biz.modular.app.inv.param.AppSkyInvInfoPageVO;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoPageParam;

/**
 * @Author wangjian
 * @Date 2024/3/14 17:11
 */
public interface AppSkyInvInfoService {
    PageResDTO<AppSkyInvInfoPageVO> page(AppSkyInvInfoPageReqAllParam skyInvInfoPageParam);
}
