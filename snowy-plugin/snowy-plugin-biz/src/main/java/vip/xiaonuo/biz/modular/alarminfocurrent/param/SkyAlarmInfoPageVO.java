package vip.xiaonuo.biz.modular.alarminfocurrent.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class SkyAlarmInfoPageVO {
    /** 设备ID（电站/采集器/逆变器/汇流箱/电表等） */
    @ApiModelProperty(value = "设备ID")
    private String id;

    /** 设备SN */
    @ApiModelProperty(value = "设备SN")
    private String equSn;

    /** 设备名称 */
    @ApiModelProperty(value = "设备名称")
    private String equName;

    /** 故障状态 1：发生中，2：已恢复 */
    @ApiModelProperty(value = "故障状态 1：发生中，2：已恢复")
    private String status;

    /** 故障开始时间 */
    @ApiModelProperty(value = "故障开始时间")
    private String beginTime;

    /** 故障结束时间 */
    @ApiModelProperty(value = "故障结束时间")
    private String endTime;

    /** 故障等级 */
    @ApiModelProperty(value = "故障等级")
    private Integer alarmLevel;

    /** 故障代码 */
    @ApiModelProperty(value = "故障代码")
    private String alarmCode;

    /** 故障描述 */
    @ApiModelProperty(value = "故障描述")
    private String alarmMsg;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称")
    private String stationName;

    @ApiModelProperty(value = "时区")
    private String timeZone;
}
