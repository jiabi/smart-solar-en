package vip.xiaonuo.biz.modular.stationinfo.param;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @Author wangjian
 * @Date 2024/2/28 17:43
 */

@Getter
@Setter
public class PsIdAndDateReqParam {
    /** 电站ID */
    private String stationId;

    /**
     * 日 yyyy-MM-dd
     * 月 yyyy-MM
     * 年 yyyy
     * 总 不用传
     */
    private String date;

    /**
     *1日 2月 3年 4总
     */
    @NotNull(message = "not null")
    @Min(value = 1, message = "Cannot be less than 1")
    @Max(value = 4, message = "Cannot be greater than 4")
    private Integer type;
}
