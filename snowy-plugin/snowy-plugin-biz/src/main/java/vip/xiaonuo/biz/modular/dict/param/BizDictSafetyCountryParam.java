package vip.xiaonuo.biz.modular.dict.param;

import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.modular.dict.entity.BizDict;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/6/3 15:43
 */

@Setter
@Getter
public class BizDictSafetyCountryParam {
    private String country;

    private String code;

    private List<BizDictSafetyStandardParam> standardList;
}
