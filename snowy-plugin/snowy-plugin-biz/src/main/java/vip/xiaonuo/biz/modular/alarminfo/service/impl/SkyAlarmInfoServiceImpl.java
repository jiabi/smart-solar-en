/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.alarminfo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.alarminfocurrent.entity.SkyAlarmInfoCurrent;
import vip.xiaonuo.biz.modular.alarminfocurrent.mapper.SkyAlarmInfoCurrentMapper;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageParam;
import vip.xiaonuo.biz.modular.alarminfocurrent.param.SkyAlarmInfoPageVO;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.invinfo.service.SkyInvInfoService;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoIdParam;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoVO;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.alarminfo.entity.SkyAlarmInfo;
import vip.xiaonuo.biz.modular.alarminfo.mapper.SkyAlarmInfoMapper;
import vip.xiaonuo.biz.modular.alarminfo.param.SkyAlarmInfoAddParam;
import vip.xiaonuo.biz.modular.alarminfo.param.SkyAlarmInfoEditParam;
import vip.xiaonuo.biz.modular.alarminfo.param.SkyAlarmInfoIdParam;
import vip.xiaonuo.biz.modular.alarminfo.service.SkyAlarmInfoService;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * 故障信息历史信息Service接口实现类
 *
 * @author 全佳璧
 * @date  2023/10/23 10:50
 **/
@Service
public class SkyAlarmInfoServiceImpl extends ServiceImpl<SkyAlarmInfoMapper, SkyAlarmInfo> implements SkyAlarmInfoService {


    @Resource
    private SkyAlarmInfoMapper alarmInfoMapper;

    @Resource
    private SkyAlarmInfoCurrentMapper alarmInfoCurrentMapper;

    @Resource
    private SkyInvInfoService skyInvInfoService;

    @Resource
    private SkyInvInfoMapper skyInvInfoMapper;

    @Resource
    private StationInfoService stationInfoService;

    @Override
    public Page<SkyAlarmInfoPageVO> page(SkyAlarmInfoPageParam skyAlarmInfoPageParam) {
//        QueryWrapper<SkyAlarmInfo> queryWrapper = new QueryWrapper<>();
//        if (!ObjectUtil.isEmpty(skyAlarmInfoPageParam.getEquSn())) {
//            queryWrapper.lambda().ge(SkyAlarmInfo::getEquSn, skyAlarmInfoPageParam.getEquSn());
//        }
//        if (!ObjectUtil.isEmpty(skyAlarmInfoPageParam.getBeginTime())) {
//            queryWrapper.lambda().ge(SkyAlarmInfo::getBeginTime, skyAlarmInfoPageParam.getBeginTime());
//        }
//        if (!ObjectUtil.isEmpty(skyAlarmInfoPageParam.getEndTime())) {
//            queryWrapper.lambda().le(SkyAlarmInfo::getEndTime, skyAlarmInfoPageParam.getEndTime());
//        }
//        if (!CollectionUtils.isEmpty(skyAlarmInfoPageParam.getAlarmLevelList())) {
//            queryWrapper.lambda().in(SkyAlarmInfo::getAlarmLevel, skyAlarmInfoPageParam.getAlarmLevelList());
//        }
//        if (ObjectUtil.isNotEmpty(skyAlarmInfoPageParam.getStatus()) && skyAlarmInfoPageParam.getStatus().equals("2")) {
//            queryWrapper.lambda().isNotNull(SkyAlarmInfo::getEndTime);
//        } else if (ObjectUtil.isNotEmpty(skyAlarmInfoPageParam.getStatus()) && skyAlarmInfoPageParam.getStatus().equals("1")) {
//            queryWrapper.lambda().isNull(SkyAlarmInfo::getEndTime);
//        }
//        queryWrapper.lambda().orderByDesc(SkyAlarmInfo::getCreateTime);
//        Page<SkyAlarmInfo> page = alarmInfoMapper.selectPage(CommonPageRequest.defaultPage(), queryWrapper);
//
//        List<SkyAlarmInfoPageVO> vos = new ArrayList<>();
//        for (SkyAlarmInfo current : page.getRecords()) {
//            SkyAlarmInfoPageVO vo = new SkyAlarmInfoPageVO();
//            BeanUtil.copyProperties(current, vo);
//            if (ObjectUtil.isNotEmpty(vo.getEndTime())) {
//                vo.setStatus("2");
//            } else {
//                vo.setStatus("1");
//            }
//
//            // 获取电站信息 所属电站名称
//            QueryWrapper<SkyInvInfo> queryWrapper1 = new QueryWrapper<>();
//            queryWrapper1.lambda().eq(SkyInvInfo::getInvSn,vo.getEquSn());
//            SkyInvInfo skyInvInfo = skyInvInfoMapper.selectOne(queryWrapper1);
//            if (skyInvInfo!=null && StringUtils.isNotEmpty(skyInvInfo.getStationId())) {
//                StationInfoIdParam param = new StationInfoIdParam();
//                param.setId(skyInvInfo.getStationId());
//                StationInfoVO detail = stationInfoService.detail(param);
//                if (detail != null && StringUtils.isNotEmpty(detail.getStationName())) {
//                    vo.setEquName(detail.getStationName());
//                }
//            }
//
//            vos.add(vo);
//        }
//
//        Page<SkyAlarmInfoPageVO> res = new Page<>();
//        BeanUtil.copyProperties(page, res);
//        res.setTotal(page.getTotal());
//        res.setRecords(vos);
//        return res;
        Page<SkyAlarmInfoPageVO> pageDataModel = new Page<>(skyAlarmInfoPageParam.getCurrent(),skyAlarmInfoPageParam.getSize());
        List<SkyAlarmInfoPageVO> list;
        if (ObjectUtil.isNotEmpty(skyAlarmInfoPageParam.getBeginTime()) && ObjectUtil.isNotEmpty(skyAlarmInfoPageParam.getEndTime())){
            LocalDateTime start = LocalDateTime.of(LocalDate.parse(skyAlarmInfoPageParam.getBeginTime()), LocalTime.MIN);
            skyAlarmInfoPageParam.setBeginTime(start.format(LocalDateUtils.DATETIME_FORMATTER));
            LocalDateTime end = LocalDateTime.of(LocalDate.parse(skyAlarmInfoPageParam.getEndTime()), LocalTime.MAX);
            skyAlarmInfoPageParam.setEndTime(end.format(LocalDateUtils.DATETIME_FORMATTER));
        }
        if(StringUtils.isEmpty(skyAlarmInfoPageParam.getStatus())){
            list=alarmInfoMapper.pageNoStatusCurrent(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList());
        }else if("1".equals(skyAlarmInfoPageParam.getStatus())){
            list =alarmInfoCurrentMapper.pageCurrent(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList());
        }else {
            list = alarmInfoMapper.pageCurrent(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList());
        }
        if(!CollectionUtils.isEmpty(list)){
            for (SkyAlarmInfoPageVO vo:list) {
                if (ObjectUtil.isNotEmpty(vo.getEndTime())) {
                    vo.setStatus("2");
                } else {
                    vo.setStatus("1");
                }
                if(StringUtils.isEmpty(vo.getTimeZone())){
                    vo.setTimeZone(skyInvInfoService.timeZoneOperation());
                }
            }
        }
        pageDataModel.setRecords(list);
        if(StringUtils.isEmpty(skyAlarmInfoPageParam.getStatus())){
            pageDataModel.setTotal(alarmInfoMapper.countNoStatusCurrentNum(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList()));
        }else if("1".equals(skyAlarmInfoPageParam.getStatus())){
            pageDataModel.setTotal(alarmInfoCurrentMapper.countCurrentNum(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList()));
        }else{
            pageDataModel.setTotal(alarmInfoMapper.countCurrentNum(skyAlarmInfoPageParam,skyAlarmInfoPageParam.getAlarmLevelList()));
        }
        return pageDataModel;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyAlarmInfoAddParam skyAlarmInfoAddParam) {
        SkyAlarmInfo skyAlarmInfo = BeanUtil.toBean(skyAlarmInfoAddParam, SkyAlarmInfo.class);
        this.save(skyAlarmInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyAlarmInfoEditParam skyAlarmInfoEditParam) {
        SkyAlarmInfo skyAlarmInfo = this.queryEntity(skyAlarmInfoEditParam.getId());
        BeanUtil.copyProperties(skyAlarmInfoEditParam, skyAlarmInfo);
        this.updateById(skyAlarmInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyAlarmInfoIdParam> skyAlarmInfoIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyAlarmInfoIdParamList, SkyAlarmInfoIdParam::getId));
    }

    @Override
    public SkyAlarmInfo detail(SkyAlarmInfoIdParam skyAlarmInfoIdParam) {
        return this.queryEntity(skyAlarmInfoIdParam.getId());
    }

    @Override
    public SkyAlarmInfo queryEntity(String id) {
        SkyAlarmInfo skyAlarmInfo = this.getById(id);
        if(ObjectUtil.isEmpty(skyAlarmInfo)) {
            throw new CommonException("故障信息历史信息不存在，id值为：{}", id);
        }
        return skyAlarmInfo;
    }

}
