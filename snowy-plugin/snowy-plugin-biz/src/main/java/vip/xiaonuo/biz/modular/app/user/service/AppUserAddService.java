package vip.xiaonuo.biz.modular.app.user.service;

import vip.xiaonuo.biz.modular.app.user.param.AppGetEmailValidCodeParam;
import vip.xiaonuo.biz.modular.app.user.param.AppGetPhoneValidCodeParam;
import vip.xiaonuo.biz.modular.app.user.param.AppUserAddParam;
import vip.xiaonuo.common.pojo.CommonResult;

/**
 * @Author wangjian
 * @Date 2024/4/1 20:01
 */
public interface AppUserAddService {
    void register(AppUserAddParam userAddParam);

    String appGetEmailValidCode(AppGetEmailValidCodeParam codeParam);

    String appGetPhoneValidCode(AppGetPhoneValidCodeParam codeParam);
}
