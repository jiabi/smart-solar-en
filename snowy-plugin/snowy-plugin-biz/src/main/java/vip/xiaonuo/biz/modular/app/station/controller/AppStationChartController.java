package vip.xiaonuo.biz.modular.app.station.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.app.station.param.chart.*;
import vip.xiaonuo.biz.modular.app.station.service.AppStationChartService;
import vip.xiaonuo.biz.modular.chartparam.entity.SkyChartParam;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoIdParam;
import vip.xiaonuo.biz.modular.stationinfo.controller.StationInfoController;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.param.*;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.*;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.*;
import vip.xiaonuo.biz.modular.stationinfo.service.StationChartService;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.service.SkyStationPowerService;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.service.SkyStationUserService;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 电站图表控制器
 *
 * @Author wangjian
 * @Date 2024/2/28 18:06
 */
@Api(tags = "app电站图表控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
@Slf4j
public class AppStationChartController {
    private static Logger logger = LoggerFactory.getLogger(StationInfoController.class);

    @Resource
    private StationChartService stationChartService;

    @Resource
    private StationInfoService stationInfoService;

    @Resource
    private SkyStationUserService skyStationUserService;


    @Resource
    private AppStationChartService appStationChartService;

    @Resource
    private SkyStationPowerService stationPowerService;


    /**
     * 角色区分
     * @param loginUser
     * @param roleCodeList
     * @return
     */
    public List<StationInfo> characterAssignment(SaBaseLoginUser loginUser,List<String> roleCodeList){
        // 获取数据
        QueryWrapper<StationInfo> queryWrapper = new QueryWrapper();
        if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            queryWrapper.lambda().eq(StationInfo::getStationCompany,loginUser.getId());
        }
        //分销商
        if(roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())){
            List<String> stationIds = skyStationUserService.list(Wrappers.lambdaQuery(SkyStationUser.class).eq(SkyStationUser::getInstallerIdL2, loginUser.getId()))
                    .stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            if(CollectionUtils.isEmpty(stationIds)){
                return null;
            }
            queryWrapper.lambda().in(StationInfo::getId,stationIds);
        }
        //业主
        if(roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())){
            List<String> stationIds = skyStationUserService.list(Wrappers.lambdaQuery(SkyStationUser.class).eq(SkyStationUser::getUserId, loginUser.getId()))
                    .stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            if(CollectionUtils.isEmpty(stationIds)){
                return null;
            }
            queryWrapper.lambda().in(StationInfo::getId,stationIds);
        }
        return stationInfoService.list(queryWrapper);
    }

    /**
     * app-首页总览-日
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("图表管理-首页总览-日")
//    @SaCheckPermission("/app/homepage/getDayStat")
    @PostMapping("/app/homepage/getDayStat")
    public CommonResult<AppPsDayPowerResVO> getDayStat(@RequestBody @Valid PsIdAndDateDayReqParam request) {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }

        PsIdAndDateReqParam req = new PsIdAndDateReqParam();
        req.setStationId(request.getGuid());
        req.setType(1);
        req.setDate(request.getLocalDate());
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        //角色区分
        List<StationInfo> ps = characterAssignment(loginUser,roleCodeList);
        PsChartDataResVO dayData = null;
        List<String> psIds = null;
        AppPsDayPowerResVO resVO = new AppPsDayPowerResVO();
        if (ObjectUtil.isNotEmpty(ps)) {
            psIds = ps.stream().map(StationInfo::getId).collect(Collectors.toList());
            dayData = stationChartService.psChart(type, date, psIds);
            // 返回数据
            resVO.setHourList(ObjectUtil.isEmpty(Objects.requireNonNull(dayData).getDateList()) ? Collections.emptyList() : dayData.getDateList());
            List<PsChartDayDataParam> dayDataList = dayData.getDayDataList();
            List<Double> dayPower = new ArrayList<>();
            if (ObjectUtil.isNotEmpty(dayDataList)) {
                dayPower = dayDataList.stream().map(PsChartDayDataParam::getMonToday).collect(Collectors.toList());
            }
            resVO.setVolumeHourList(ObjectUtil.isEmpty(dayPower) ? Collections.emptyList() : dayPower);
            //当日发电量
            if(!CollectionUtils.isEmpty(psIds)){
                List<SkyStationPower> skyStationPowers = stationPowerService.list(Wrappers.lambdaQuery(SkyStationPower.class)
                        .eq(SkyStationPower::getDateTime,date)
                        .eq(SkyStationPower::getTimeType,1)
                        .in(SkyStationPower::getStationId,psIds));
                resVO.setTotalVolume(CollectionUtils.isEmpty(skyStationPowers) ? 0D : skyStationPowers.stream().mapToDouble(station -> Double.parseDouble(station.getStationPower())).sum());
            }
        }else {
            resVO.setHourList(Collections.emptyList());
            resVO.setVolumeHourList(Collections.emptyList());
            resVO.setTotalVolume(0D);
        }
        return CommonResult.data(resVO);
    }

    /**
     * app-首页总览-月
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("图表管理-首页总览-月")
//    @SaCheckPermission("/app/homepage/getMonthStat")
    @PostMapping("/app/homepage/getMonthStat")
    public CommonResult<AppPsMonthPowerResVO> getMonthStat(@RequestBody @Valid PsIdAndDateDayReqParam request) {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }

        PsIdAndDateReqParam req = new PsIdAndDateReqParam();
        req.setStationId(request.getGuid());
        req.setType(2);
        req.setDate(request.getLocalDate());
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        //角色区分
        List<StationInfo> ps = characterAssignment(loginUser,roleCodeList);
        PsChartDataResVO dayData = null;
        List<String> psIds = null;
        // 返回数据
        AppPsMonthPowerResVO resVO = new AppPsMonthPowerResVO();
        if (ObjectUtil.isNotEmpty(ps)) {
            psIds = ps.stream().map(StationInfo::getId).collect(Collectors.toList());
            dayData = stationChartService.psChart(type, date, psIds);
            resVO.setDayList(ObjectUtil.isEmpty(dayData.getDateList()) ? Collections.emptyList() : dayData.getDateList());
            resVO.setVolumeDayList(ObjectUtil.isEmpty(dayData.getPower()) ? Collections.emptyList() : dayData.getPower());
            resVO.setTotalVolume(ObjectUtil.isEmpty(dayData.getPower()) ? 0D : dayData.getPower().stream().reduce(Double::sum).orElse(0D));
        }else {
            resVO.setDayList(Collections.emptyList());
            resVO.setVolumeDayList(Collections.emptyList());
            resVO.setTotalVolume(0D);
        }
        return CommonResult.data(resVO);
    }

    /**
     * app-首页总览-年
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("图表管理-首页总览-年")
//    @SaCheckPermission("/app//homepage/getYearStat")
    @PostMapping("/app//homepage/getYearStat")
    public CommonResult<AppPsYearPowerResVO> getYearStat(@RequestBody @Valid PsIdAndDateDayReqParam request) {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }

        PsIdAndDateReqParam req = new PsIdAndDateReqParam();
        req.setStationId(request.getGuid());
        req.setType(3);
        req.setDate(request.getLocalDate());
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        //角色区分
        List<StationInfo> ps = characterAssignment(loginUser,roleCodeList);
        PsChartDataResVO dayData = null;
        List<String> psIds = null;
        // 返回数据
        AppPsYearPowerResVO resVO = new AppPsYearPowerResVO();
        if (ps != null) {
            psIds = ps.stream().map(StationInfo::getId).collect(Collectors.toList());
            dayData = stationChartService.psChart(type, date, psIds);
            resVO.setMonthList(ObjectUtil.isEmpty(dayData.getDateList()) ? Collections.emptyList() : dayData.getDateList());
            resVO.setVolumeMonthList(ObjectUtil.isEmpty(dayData.getPower()) ? Collections.emptyList() : dayData.getPower());
            resVO.setTotalVolume(ObjectUtil.isEmpty(dayData.getPower()) ? 0D : dayData.getPower().stream().reduce(Double::sum).orElse(0D));
        }else {
            resVO.setMonthList(Collections.emptyList());
            resVO.setVolumeMonthList(Collections.emptyList());
            resVO.setTotalVolume(0D);
        }
        return CommonResult.data(resVO);
    }

    /**
     * app-首页总览-总
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("图表管理-首页总览-总")
//    @SaCheckPermission("/app//homepage/getTotalStat")
    @PostMapping("/app//homepage/getTotalStat")
    public CommonResult<AppPsTotalPowerResVO> getTotalStat(@RequestBody @Valid PsIdAndDateDayReqParam request) {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }

        PsIdAndDateReqParam req = new PsIdAndDateReqParam();
        req.setStationId(request.getGuid());
        req.setType(4);
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        //角色区分
        List<StationInfo> ps = characterAssignment(loginUser,roleCodeList);
        PsChartDataResVO dayData = null;
        List<String> psIds = null;
        // 返回数据
        AppPsTotalPowerResVO resVO = new AppPsTotalPowerResVO();
        if (ps != null) {
            psIds = ps.stream().map(StationInfo::getId).collect(Collectors.toList());
            dayData = stationChartService.psChart(type, date, psIds);
            resVO.setYearList(ObjectUtil.isEmpty(dayData.getDateList()) ? Collections.emptyList() : dayData.getDateList());
            resVO.setVolumeYearList(ObjectUtil.isEmpty(dayData.getPower()) ? Collections.emptyList() : dayData.getPower());
            resVO.setTotalVolume(ObjectUtil.isEmpty(dayData.getPower()) ? 0D : dayData.getPower().stream().reduce(Double::sum).orElse(0D));
        }else {
            resVO.setYearList(Collections.emptyList());
            resVO.setVolumeYearList(Collections.emptyList());
            resVO.setTotalVolume(0D);
        }
        return CommonResult.data(resVO);
    }


    /**
     * app-电站图表-日
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("图表管理-电站图表-日")
//    @SaCheckPermission("/app/powerStation/getDayStats")
    @PostMapping("/app/powerStation/getDayStats")
    public CommonResult<AppPsDayPowerResVO> getDayStats(@RequestBody @Valid PsIdAndDateDayReqParam request) {
        PsIdAndDateReqParam req = new PsIdAndDateReqParam();
        req.setStationId(request.getGuid());
        req.setType(1);
        req.setDate(request.getLocalDate());
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        if (StringUtils.isEmpty(req.getStationId())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9101);
        }
        PsChartDataResVO dayData = stationChartService.psChart(type, date, Collections.singletonList(req.getStationId()));
        // 返回数据
        AppPsDayPowerResVO resVO = new AppPsDayPowerResVO();
        resVO.setHourList(ObjectUtil.isEmpty(dayData.getDateList()) ? Collections.emptyList() : dayData.getDateList());
        List<PsChartDayDataParam> dayDataList = dayData.getDayDataList();
        List<Double> dayPower = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(dayDataList)) {
            dayPower = dayDataList.stream().map(PsChartDayDataParam::getMonPac).collect(Collectors.toList());
        }
        resVO.setVolumeHourList(ObjectUtil.isEmpty(dayPower) ? Collections.emptyList() : dayPower);
        resVO.setTotalVolume(ObjectUtil.isEmpty(dayPower) ? 0D : Objects.requireNonNull(dayPower).stream().filter(Objects::nonNull).reduce(Double::sum).orElse(0D));
        return CommonResult.data(resVO);
    }

    /**
     * app-电站图表-月
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("图表管理-电站图表-月")
//    @SaCheckPermission("/app/powerStation/getMonthStats")
    @PostMapping("/app/powerStation/getMonthStats")
    public CommonResult<AppPsMonthPowerResVO> getMonthStats(@RequestBody @Valid PsIdAndDateDayReqParam request) {
        PsIdAndDateReqParam req = new PsIdAndDateReqParam();
        req.setStationId(request.getGuid());
        req.setType(2);
        req.setDate(request.getLocalDate());
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        if (StringUtils.isEmpty(req.getStationId())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9101);
        }
        PsChartDataResVO dayData = stationChartService.psChart(type, date, Collections.singletonList(req.getStationId()));
        // 返回数据
        AppPsMonthPowerResVO resVO = new AppPsMonthPowerResVO();
        resVO.setDayList(ObjectUtil.isEmpty(dayData.getDateList()) ? Collections.emptyList() : dayData.getDateList());
        resVO.setVolumeDayList(ObjectUtil.isEmpty(dayData.getPower()) ? Collections.emptyList() : dayData.getPower());
        resVO.setTotalVolume(ObjectUtil.isEmpty(dayData.getPower()) ? 0D : dayData.getPower().stream().reduce(Double::sum).orElse(0D));
        return CommonResult.data(resVO);
    }

    /**
     * app-电站图表-年
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("图表管理-电站图表-年")
//    @SaCheckPermission("/app/powerStation/getYearStats")
    @PostMapping("/app/powerStation/getYearStats")
    public CommonResult<AppPsYearPowerResVO> getYearStats(@RequestBody @Valid PsIdAndDateDayReqParam request) {
        PsIdAndDateReqParam req = new PsIdAndDateReqParam();
        req.setStationId(request.getGuid());
        req.setType(3);
        req.setDate(request.getLocalDate());
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        if (StringUtils.isEmpty(req.getStationId())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9101);
        }
        PsChartDataResVO dayData = stationChartService.psChart(type, date, Collections.singletonList(req.getStationId()));
        // 返回数据
        AppPsYearPowerResVO resVO = new AppPsYearPowerResVO();
        resVO.setMonthList(ObjectUtil.isEmpty(dayData.getDateList()) ? Collections.emptyList() : dayData.getDateList());
        resVO.setVolumeMonthList(ObjectUtil.isEmpty(dayData.getPower()) ? Collections.emptyList() : dayData.getPower());
        resVO.setTotalVolume(ObjectUtil.isEmpty(dayData.getPower()) ? 0D : dayData.getPower().stream().reduce(Double::sum).orElse(0D));
        return CommonResult.data(resVO);
    }

    /**
     * app-电站图表-总
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("图表管理-电站图表-总")
//    @SaCheckPermission("/app/powerStation/getTotalStats")
    @PostMapping("/app/powerStation/getTotalStats")
    public CommonResult<AppPsTotalPowerResVO> getTotalStats(@RequestBody @Valid PsIdAndDateDayReqParam request) {
        PsIdAndDateReqParam req = new PsIdAndDateReqParam();
        req.setStationId(request.getGuid());
        req.setType(4);
        //日期
        String date = req.getDate();
        //类型
        Integer type = req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        if (StringUtils.isEmpty(req.getStationId())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_OUT_LENGTH_9101);
        }
        PsChartDataResVO dayData = stationChartService.psChart(type, date, Collections.singletonList(req.getStationId()));
        // 返回数据
        AppPsTotalPowerResVO resVO = new AppPsTotalPowerResVO();
        resVO.setYearList(ObjectUtil.isEmpty(dayData.getDateList()) ? Collections.emptyList() : dayData.getDateList());
        resVO.setVolumeYearList(ObjectUtil.isEmpty(dayData.getPower()) ? Collections.emptyList() : dayData.getPower());
        resVO.setTotalVolume(ObjectUtil.isEmpty(dayData.getPower()) ? 0D : dayData.getPower().stream().reduce(Double::sum).orElse(0D));
        return CommonResult.data(resVO);
    }

    /**
     * app-设备图表-日
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("图表管理-设备图表-日")
//    @SaCheckPermission("/app/device/getDeviceDayStats")
    @PostMapping("/app/device/getDeviceDayStats")
    public CommonResult<AppPsDeviceDayPowerResVO> getDeviceDayStats(@RequestBody AppPsDeviceChartReqParam request) {
        if (StringUtils.isEmpty(request.getSn())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.SN_CHECK_NOT_NULL_9107);
        }
        PsEquChartReqParam req = new PsEquChartReqParam();
        req.setDeviceSn(request.getSn());
        req.setDate(request.getLocalDate());
        req.setType(1);
        req.setParamsList(request.getParamsList());
        //日期
        String date = req.getDate();
        //类型
        Integer type =  req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }

        String deviceSn = req.getDeviceSn();
        List<String> paramsList = req.getParamsList();
        PsEquChartResVO equChart = stationChartService.equChart(deviceSn, date, type, paramsList);
//        if (equChart == null || Objects.isNull(equChart.getDayChart())) {
//            return CommonResult.ok();
//        }
        PsEquDayChartResParam dayData = equChart.getDayChart();

        AppPsDeviceDayPowerResVO resVO = new AppPsDeviceDayPowerResVO();
        if(!ObjectUtil.isEmpty(dayData)){
            resVO.setHourList(ObjectUtil.isEmpty(dayData.getTimeList()) ? Collections.emptyList() : dayData.getTimeList());
            resVO.setVolumeHourList(ObjectUtil.isEmpty(dayData.getList()) ? Collections.emptyList() : dayData.getList());
        }
        return CommonResult.data(resVO);
    }

    /**
     * app-设备图表-月
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("图表管理-设备图表-月")
//    @SaCheckPermission("/app/device/getDeviceMonthStats")
    @PostMapping("/app/device/getDeviceMonthStats")
    public CommonResult<AppPsMonthPowerResVO> getDeviceMonthStats(@RequestBody AppPsDeviceChartReqParam request) {
        if (StringUtils.isEmpty(request.getSn())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.SN_CHECK_NOT_NULL_9107);
        }
        PsEquChartReqParam req = new PsEquChartReqParam();
        req.setDeviceSn(request.getSn());
        req.setDate(request.getLocalDate());
        req.setType(2);
        req.setParamsList(request.getParamsList());
        //日期
        String date = req.getDate();
        //类型
        Integer type =  req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        String deviceSn = req.getDeviceSn();
        List<String> paramsList = req.getParamsList();
        PsEquChartResVO equChart = stationChartService.equChart(deviceSn, date, type, paramsList);
//        if (equChart == null || Objects.isNull(equChart.getMonYearTotalChart())) {
//            return CommonResult.ok();
//        }
        PsEquChartResParam dayData = equChart.getMonYearTotalChart();

        // 返回数据
        AppPsMonthPowerResVO resVO = new AppPsMonthPowerResVO();
        resVO.setDayList(ObjectUtil.isEmpty(dayData.getDateList()) ? Collections.emptyList() : dayData.getDateList());
        resVO.setVolumeDayList(ObjectUtil.isEmpty(dayData.getPower()) ? Collections.emptyList() : dayData.getPower());
        resVO.setTotalVolume(ObjectUtil.isEmpty(dayData.getPower()) ? 0D : dayData.getPower().stream().reduce(Double::sum).orElse(0D));
        return CommonResult.data(resVO);
    }

    /**
     * app-设备图表-月
     */
    @ApiOperationSupport(order = 7)
    @ApiOperation("图表管理-设备图表-年")
//    @SaCheckPermission("/app/device/getDeviceYearStats")
    @PostMapping("/app/device/getDeviceYearStats")
    public CommonResult<AppPsYearPowerResVO> getDeviceYearStats(@RequestBody AppPsDeviceChartReqParam request) {
        if (StringUtils.isEmpty(request.getSn())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.SN_CHECK_NOT_NULL_9107);
        }
        PsEquChartReqParam req = new PsEquChartReqParam();
        req.setDeviceSn(request.getSn());
        req.setDate(request.getLocalDate());
        req.setType(3);
        req.setParamsList(request.getParamsList());
        //日期
        String date = req.getDate();
        //类型
        Integer type =  req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        String deviceSn = req.getDeviceSn();
        List<String> paramsList = req.getParamsList();
        PsEquChartResVO equChart = stationChartService.equChart(deviceSn, date, type, paramsList);
//        if (equChart == null || Objects.isNull(equChart.getMonYearTotalChart())) {
//            return CommonResult.ok();
//        }
        PsEquChartResParam dayData = equChart.getMonYearTotalChart();

        // 返回数据
        AppPsYearPowerResVO resVO = new AppPsYearPowerResVO();
        resVO.setMonthList(ObjectUtil.isEmpty(dayData.getDateList()) ? Collections.emptyList() : dayData.getDateList());
        resVO.setVolumeMonthList(ObjectUtil.isEmpty(dayData.getPower()) ? Collections.emptyList() : dayData.getPower());
        resVO.setTotalVolume(ObjectUtil.isEmpty(dayData.getPower()) ? 0D : dayData.getPower().stream().reduce(Double::sum).orElse(0D));
        return CommonResult.data(resVO);
    }

    /**
     * app-设备图表-总
     */
    @ApiOperationSupport(order = 8)
    @ApiOperation("图表管理-设备图表-总")
//    @SaCheckPermission("/app/device/getDeviceTotalStats")
    @PostMapping("/app/device/getDeviceTotalStats")
    public CommonResult<AppPsTotalPowerResVO> getDeviceTotalStats(@RequestBody AppPsDeviceChartReqParam request) {
        if (StringUtils.isEmpty(request.getSn())) {
            throw new SkyCommonException(SkyCommonExceptionEnum.SN_CHECK_NOT_NULL_9107);
        }
        PsEquChartReqParam req = new PsEquChartReqParam();
        req.setDeviceSn(request.getSn());
        req.setDate(request.getLocalDate());
        req.setType(4);
        req.setParamsList(request.getParamsList());
        //日期
        String date = req.getDate();
        //类型
        Integer type =  req.getType();
        //校验日期
        boolean validate = this.validateDate(date, type);
        if (!validate) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_CHECK_DATE_ERROR_9002);
        }
        String deviceSn = req.getDeviceSn();
        List<String> paramsList = req.getParamsList();
        PsEquChartResVO equChart = stationChartService.equChart(deviceSn, date, type, paramsList);
//        if (equChart == null || Objects.isNull(equChart.getMonYearTotalChart())) {
//            return CommonResult.ok();
//        }
        PsEquChartResParam dayData = equChart.getMonYearTotalChart();
        // 返回数据
        AppPsTotalPowerResVO resVO = new AppPsTotalPowerResVO();
        resVO.setYearList(ObjectUtil.isEmpty(dayData.getDateList()) ? Collections.emptyList() : dayData.getDateList());
        resVO.setVolumeYearList(ObjectUtil.isEmpty(dayData.getPower()) ? Collections.emptyList() : dayData.getPower());
        resVO.setTotalVolume(ObjectUtil.isEmpty(dayData.getPower()) ? 0D : dayData.getPower().stream().reduce(Double::sum).orElse(0D));
        return CommonResult.data(resVO);
    }

    /**
     * 获取测点数据
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取测点数据")
//    @SaCheckPermission("app/device/getDeviceInfo")
    @PostMapping("app/device/measurePoint")
    public CommonResult<String> measurePoint(@RequestBody SkyInvInfoIdParam sn) {
        return CommonResult.data(appStationChartService.measurePoint(sn.getInvSn()));
    }


    /**
     * 校验日期
     *
     * @param date 日期
     * @param type 类型（1-日2-月3-年4-总）
     * @return
     */
    private boolean validateDate(String date, int type) {
        String pattern;
        switch (type) {
            case 1:
                pattern = "\\d{4}-\\d{2}-\\d{2}";
                break;
            case 2:
                pattern = "\\d{4}-\\d{2}";
                break;
            case 3:
                pattern = "\\d{4}";
                break;
            case 4:
                return true;
            default:
                // 非法类型，直接返回false
                return false;
        }
        return Pattern.matches(pattern, date);
    }
}
