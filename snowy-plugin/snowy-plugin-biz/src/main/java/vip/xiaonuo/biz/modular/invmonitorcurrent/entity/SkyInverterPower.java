package vip.xiaonuo.biz.modular.invmonitorcurrent.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2023/10/13 10:14
 **/
@Getter
@Setter
@TableName("sky_inv_power")
public class SkyInverterPower {
    /** 设备ID（逆变器） */
    @ApiModelProperty(value = "设备ID", position = 1)
    @TableId
    private String id;

    /** 设备SN */
    @ApiModelProperty(value = "设备SN", position = 2)
    private String invSn;

    /** 时间类型 */
    @ApiModelProperty(value = "时间类型：1.日发电量；2.月发电量；3.年发电量；4.总发电量；5.今日并网电量；6.累计并网电量；7.今日购电量；8.累计购电量", position = 3)
    private Integer timeType;

    /** 日期 */
    @ApiModelProperty(value = "日期（年、年/月、年/月/日）", position = 4)
    private String dateTime;

    /** 运行时间 */
    @ApiModelProperty(value = "运行时间", position = 5)
    private Double runHours;

    /** 发电量 */
    @ApiModelProperty(value = "发电量", position = 6)
    private String invPower;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 7)
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 8)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 9)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 10)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 11)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 12)
    private String tenantId;

}
