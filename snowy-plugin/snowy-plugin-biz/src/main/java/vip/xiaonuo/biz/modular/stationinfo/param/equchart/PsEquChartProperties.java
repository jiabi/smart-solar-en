package vip.xiaonuo.biz.modular.stationinfo.param.equchart;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author wangjian
 * @Date 2024/5/27 14:47
 */
@Data
@Component
public class PsEquChartProperties {
    /**
     * 储能逆变器sn命名中特有字段
     */
    @Value("${equ.name.type}")
    private String[] equNameType;
}
