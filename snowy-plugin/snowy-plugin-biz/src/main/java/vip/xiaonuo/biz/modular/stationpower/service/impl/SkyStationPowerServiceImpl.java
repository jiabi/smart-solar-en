/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationpower.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.biz.modular.stationpower.param.*;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.mapper.SkyStationPowerMapper;
import vip.xiaonuo.biz.modular.stationpower.service.SkyStationPowerService;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 电站图表Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/02/22 16:57
 **/
@Service
public class SkyStationPowerServiceImpl extends ServiceImpl<SkyStationPowerMapper, SkyStationPower> implements SkyStationPowerService {

    @Resource
    private SkyStationPowerMapper spMapper;

    @Override
    public Page<SkyStationPower> page(SkyStationPowerPageParam skyStationPowerPageParam) {
        QueryWrapper<SkyStationPower> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(skyStationPowerPageParam.getStationId())) {
            queryWrapper.lambda().like(SkyStationPower::getStationId, skyStationPowerPageParam.getStationId());
        }
        if(ObjectUtil.isAllNotEmpty(skyStationPowerPageParam.getSortField(), skyStationPowerPageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(skyStationPowerPageParam.getSortOrder());
            queryWrapper.orderBy(true, skyStationPowerPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(skyStationPowerPageParam.getSortField()));
        } else {
            queryWrapper.lambda().orderByAsc(SkyStationPower::getId);
        }
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyStationPowerAddParam skyStationPowerAddParam) {
        SkyStationPower skyStationPower = BeanUtil.toBean(skyStationPowerAddParam, SkyStationPower.class);
        this.save(skyStationPower);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyStationPowerEditParam skyStationPowerEditParam) {
        SkyStationPower skyStationPower = this.queryEntity(skyStationPowerEditParam.getId());
        BeanUtil.copyProperties(skyStationPowerEditParam, skyStationPower);
        this.updateById(skyStationPower);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyStationPowerIdParam> skyStationPowerIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyStationPowerIdParamList, SkyStationPowerIdParam::getId));
    }

    @Override
    public SkyStationPower detail(SkyStationPowerIdParam skyStationPowerIdParam) {
        return this.queryEntity(skyStationPowerIdParam.getId());
    }

    @Override
    public SkyStationPower queryEntity(String id) {
        SkyStationPower skyStationPower = this.getById(id);
        if(ObjectUtil.isEmpty(skyStationPower)) {
            throw new CommonException("电站图表不存在，id值为：{}", id);
        }
        return skyStationPower;
    }
    @Override
    public SkyStationPower getStationPower(String stationId, int timeType, String timeStr) {
        SkyStationPower stationPower = new SkyStationPower();
        if (29 == timeType) {
            List<SkyStationPowerTaskParam> stationPowerList = spMapper.queryStationPowerSumCurrent(stationId, timeType, timeStr);
            // 使用流按照invSn属性进行分组，并获取每组中dateTime值最大的元素
            if (CollectionUtils.isEmpty(stationPowerList)) {
                return null;
            }
            List<SkyStationPowerTaskParam> result = new ArrayList<>(stationPowerList.stream()
                    .collect(Collectors.toMap(SkyStationPowerTaskParam::getInvSn, Function.identity(), BinaryOperator.maxBy(Comparator.comparing(SkyStationPowerTaskParam::getDateTime))))
                    .values());
            stationPower.setStationId(stationId);
            stationPower.setTimeType(29);
            stationPower.setDateTime(timeStr);
            stationPower.setRunHours(result.get(0).getRunHours());
            stationPower.setStationPower(String.valueOf(result.stream()
                    .mapToDouble(param -> Double.parseDouble(param.getStationPower()))
                    .sum()));
        } else {
            stationPower = spMapper.queryStationPowerSum(stationId, timeType, timeStr);
        }
        if (ObjectUtil.isEmpty(stationPower)) {
            return null;
        }
        String idStr = IdWorker.getIdStr();
        if (stationPower != null) {
            stationPower.setId(idStr);
            if (ObjectUtil.isEmpty(stationPower.getStationId())) {
                stationPower.setStationId(stationId);
            }
            if (ObjectUtil.isEmpty(stationPower.getTimeType())) {
                stationPower.setTimeType(timeType);
            }
            if (ObjectUtil.isEmpty(stationPower.getDateTime())) {
                stationPower.setDateTime(timeStr);
            }
        } else {
            stationPower = new SkyStationPower();
            stationPower.setStationId(idStr);
            stationPower.setStationId(stationId);
            stationPower.setTimeType(timeType);
            stationPower.setStationPower("0.0");
        }

        return stationPower;

    }
    @Override
    public SkyStationPower queryStationPower(String stationId, int timeType, String timeStr) {
        return spMapper.queryStationPower(stationId, timeType, timeStr);
    }


}
