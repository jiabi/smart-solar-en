package vip.xiaonuo.biz.core.util.objectcopier;

import net.sf.cglib.core.Converter;
import vip.xiaonuo.biz.core.util.LocalDateUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author wangjian
 * @Date 2024/3/20 15:57
 */
public class ObjectCopierConverter implements Converter {

    /**
     * 自定义属性转换
     *
     * @param value   源对象属性类
     * @param target  目标对象里属性对应set方法名,eg.setId
     * @param context 目标对象属性类
     * @return
     */
    @Override
    public Object convert(Object value, Class target, Object context) {
        if (value instanceof LocalDateTime && target.equals(Date.class)) {
            return LocalDateUtils.localDateTime2Date((LocalDateTime) value);
        } else if (value instanceof LocalDate && target.equals(Date.class)) {
            return LocalDateUtils.localDate2Date((LocalDate) value);
        } else if (value instanceof AbstractDTO) {
            return ObjectCopierUtils.copy(value, target);
        } else if (value instanceof AbstractPO) {
            return ObjectCopierUtils.copy(value, target);
        } else if (value instanceof AbstractEntity) {
            return ObjectCopierUtils.copy(value, target);
        } else if (value instanceof AbstractVO) {
            return ObjectCopierUtils.copy(value, target);
        }
        return value;
    }
}
