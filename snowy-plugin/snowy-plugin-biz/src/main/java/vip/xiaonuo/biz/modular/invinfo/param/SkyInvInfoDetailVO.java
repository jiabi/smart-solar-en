package vip.xiaonuo.biz.modular.invinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.modular.equipmodel.entity.SkyEquipModel;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.SkyInvMonitorCurrentDetailVO;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoVO;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2023/10/12 10:59
 **/
@Getter
@Setter
public class SkyInvInfoDetailVO {
    /** 逆变器SN */
    @ApiModelProperty(value = "逆变器SN", position = 1)
    private String invSn;

    /** 逆变器名称 */
    @ApiModelProperty(value = "逆变器名称", position = 2)
    private String invName;

    /** 图片路径 */
    @ApiModelProperty(value = "图片路径", position = 3)
    private String picUrl;

    /** 设备型号 */
    @ApiModelProperty(value = "设备型号", position = 4)
    private String modelId;

    /** 所属电站 */
    @ApiModelProperty(value = "所属电站", position = 5)
    private String stationId;

    /** 设备位置 */
    @ApiModelProperty(value = "设备位置", position = 6)
    private String invPosition;

    /** 安规标准 */
    @ApiModelProperty(value = "安规标准", position = 7)
    private String invRegulatory;

    /** 安规国家 */
    @ApiModelProperty(value = "安规国家", position = 8)
    private String regulatoryCountry;

    /** 累计运行天数 */
    @ApiModelProperty(value = "累计运行天数", position = 9)
    private Integer invRuntime;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 10)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 11)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 12)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 13)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 14)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 15)
    private String tenantId;

    /** 设备型号详细信息 */
    @ApiModelProperty(value = "设备型号详细信息", position = 16)
    private SkyEquipModel skyEquipModel;

    /** 所属电站详细信息 */
    @ApiModelProperty(value = "所属电站详细信息", position = 17)
    private StationInfoVO stationInfo;

    /** 逆变器实时数据 */
    private SkyInvMonitorCurrentDetailVO currentDetailVO;

    /** 设备型号参数匹配字段 **/
    private String paramConfig;
}
