package vip.xiaonuo.biz.core.enums;


import lombok.Getter;

@Getter
public enum StationInfoExportEnum {

    DISTRIBUTED_USER(1,"分布式用户","stationType"),
    DISTRIBUTED_COMMERCE(2,"分布式商业","stationType"),
    DISTRIBUTED_ENGINEERING(3,"分布式工业","stationType"),
    SURFACE_POWER_STATION(4,"地面电站","stationType"),

    FULL_GRID_CONNECTION(1,"全额并网系统","stationSystem"),
    SPONTANEOUS_SELF_USE(2,"自发自用系统","stationSystem"),
    STORED_ENERGY(3,"储能系统","stationSystem"),

    UNCONNECTED(0,"未并网","stationGridstatus"),
    GRID_CONNECTED(1,"已并网","stationGridstatus"),

    IDLE_POWER_STATION(0,"空电站","stationStatus"),
    POWER_PLANT_NORMAL(1,"电站正常","stationStatus"),
    PARTIALLY_OFFLINE(2,"部分离线","stationStatus"),
    ALL_OFFLINE(3,"全部离线","stationStatus"),

    HAVE_AN_ALARM(0,"无报警","stationAlarm"),
    ALARM_FREE(1,"有报警","stationAlarm"),

    ;
    private Integer code;
    private String value;
    private String type;

    StationInfoExportEnum(Integer code, String value,String type) {
        this.code = code;
        this.value = value;
        this.type=type;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static String findByCode(Integer code,String type) {
        for (StationInfoExportEnum va : StationInfoExportEnum.values()) {
            if (va.getCode().equals(code) && va.getType().equals(type)) {
                return va.getValue();
            }
        }
        return "";
    }

}
