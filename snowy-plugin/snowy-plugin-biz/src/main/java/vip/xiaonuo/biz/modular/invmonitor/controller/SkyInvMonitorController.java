/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invmonitor.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.invmonitor.entity.SkyInvMonitor;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorAddParam;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorEditParam;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorIdParam;
import vip.xiaonuo.biz.modular.invmonitor.param.SkyInvMonitorPageParam;
import vip.xiaonuo.biz.modular.invmonitor.service.SkyInvMonitorService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 逆变器历史数据控制器
 *
 * @author 全佳璧
 * @date  2023/10/23 09:44
 */
@Api(tags = "逆变器历史数据控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyInvMonitorController {

    @Resource
    private SkyInvMonitorService skyInvMonitorService;

    /**
     * 获取逆变器历史数据分页
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取逆变器历史数据分页")
    @SaCheckPermission("/biz/invmonitor/page")
    @GetMapping("/biz/invmonitor/page")
    public CommonResult<Page<SkyInvMonitor>> page(SkyInvMonitorPageParam skyInvMonitorPageParam) {
        return CommonResult.data(skyInvMonitorService.page(skyInvMonitorPageParam));
    }

    /**
     * 添加逆变器历史数据
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加逆变器历史数据")
    @CommonLog("添加逆变器历史数据")
    @SaCheckPermission("/biz/invmonitor/add")
    @PostMapping("/biz/invmonitor/add")
    public CommonResult<String> add(@RequestBody @Valid SkyInvMonitorAddParam skyInvMonitorAddParam) {
        skyInvMonitorService.add(skyInvMonitorAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑逆变器历史数据
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑逆变器历史数据")
    @CommonLog("编辑逆变器历史数据")
    @SaCheckPermission("/biz/invmonitor/edit")
    @PostMapping("/biz/invmonitor/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyInvMonitorEditParam skyInvMonitorEditParam) {
        skyInvMonitorService.edit(skyInvMonitorEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除逆变器历史数据
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除逆变器历史数据")
    @CommonLog("删除逆变器历史数据")
    @SaCheckPermission("/biz/invmonitor/delete")
    @PostMapping("/biz/invmonitor/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyInvMonitorIdParam> skyInvMonitorIdParamList) {
        skyInvMonitorService.delete(skyInvMonitorIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取逆变器历史数据详情
     *
     * @author 全佳璧
     * @date  2023/10/23 09:44
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取逆变器历史数据详情")
    @SaCheckPermission("/biz/invmonitor/detail")
    @GetMapping("/biz/invmonitor/detail")
    public CommonResult<SkyInvMonitor> detail(@Valid SkyInvMonitorIdParam skyInvMonitorIdParam) {
        return CommonResult.data(skyInvMonitorService.detail(skyInvMonitorIdParam));
    }

}
