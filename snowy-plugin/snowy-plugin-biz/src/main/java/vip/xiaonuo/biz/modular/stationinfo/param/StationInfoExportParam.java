package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2023/9/26 09:51
 **/
@Getter
@Setter
public class StationInfoExportParam {

    /** 电站id集合 */
    @ApiModelProperty(value = "电站id集合")
    private List<String> ids;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称")
    private String stationName;

    /** 装机容量范围起始值（默认小数点后两位） */
    @ApiModelProperty(value = "装机容量范围起始值（默认小数点后两位）")
    private Double stationSizeBegin;

    /** 装机容量范围结束值（默认小数点后两位） */
    @ApiModelProperty(value = "装机容量范围结束值（默认小数点后两位）")
    private Double stationSizeEnd;

    /** 电站类型 */
    @ApiModelProperty(value = "电站类型")
    private Integer stationType;

    /** 电站系统 */
    @ApiModelProperty(value = "电站系统")
    private Integer stationSystem;

    /** 并网状态 */
    @ApiModelProperty(value = "并网状态")
    private Integer stationGridstatus;

    /** 电站区域 */
    @ApiModelProperty(value = "电站区域")
    private Integer stationArea;

    /** 电站状态 */
    @ApiModelProperty(value = "电站状态")
    private Integer stationStatus;

    /** 报警状态 */
    @ApiModelProperty(value = "报警状态")
    private Integer stationAlarm;

    /** 安装公司id */
    @ApiModelProperty(value = "安装公司id")
    private String stationCompany;

    /** 业主id */
    @ApiModelProperty(value = "业主id")
    private String stationContactId;

    /** 分销商 */
    @ApiModelProperty(value = "分销商")
    private String distributor;

}
