package vip.xiaonuo.biz.modular.app.station.service;

import vip.xiaonuo.biz.modular.app.station.param.chart.AppPsPowerChartVO;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.PsEquChartResVO;

/**
 * @Author wangjian
 * @Date 2024/3/12 9:26
 */
public interface AppStationChartService {

    AppPsPowerChartVO power(String date, Integer type, String companyCode);

    String measurePoint(String invSn);
}
