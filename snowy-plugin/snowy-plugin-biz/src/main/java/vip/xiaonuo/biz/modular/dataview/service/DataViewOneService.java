package vip.xiaonuo.biz.modular.dataview.service;

import vip.xiaonuo.biz.modular.dataview.param.AreaStationProvinceCountVO;
import vip.xiaonuo.biz.modular.dataview.param.AreaStationProvinceVO;
import vip.xiaonuo.biz.modular.dataview.param.EquDayChartVO;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/7/8 13:25
 */
public interface DataViewOneService {

    EquDayChartVO equDayChart();

    AreaStationProvinceCountVO statisticsPowerStationEquipment();

    AreaStationProvinceCountVO statisticsDeviceRanking();
}
