package vip.xiaonuo.biz.modular.user.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @Author wangjian
 * @Date 2024/4/30 11:25
 */

@Getter
@Setter
public class BizUserAddSimpleParam {

    /** 用户名 */
    @ApiModelProperty(value = "用户名", required = true, position = 1)
    @NotBlank(message = "account not null")
    private String account;

    /** 手机 */
    @ApiModelProperty(value = "手机", position = 2)
    private String phone;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱", position = 3)
    private String email;

    /** 密码 */
    @ApiModelProperty(value = "密码", position = 4)
    @NotBlank(message = "password not null")
    private String password;

    /** 用户类型 */
    @ApiModelProperty(value = "用户类型 1:终端用户，2：安装商", position = 5)
    private Integer userType;

    /** 安装商id */
    @ApiModelProperty(value = "安装商id", position = 6)
    private String installerId;

    /** 业主工作单位 */
    @ApiModelProperty(value = "业主工作单位")
    private String userAddr;

}
