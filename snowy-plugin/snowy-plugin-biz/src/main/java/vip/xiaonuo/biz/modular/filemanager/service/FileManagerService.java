package vip.xiaonuo.biz.modular.filemanager.service;

import cn.hutool.http.server.HttpServerResponse;
import vip.xiaonuo.biz.modular.filemanager.param.AutoTestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/4/26 17:21
 */
public interface FileManagerService {
    String autoTestPdf(List<AutoTestParam> list) throws Exception;
}
