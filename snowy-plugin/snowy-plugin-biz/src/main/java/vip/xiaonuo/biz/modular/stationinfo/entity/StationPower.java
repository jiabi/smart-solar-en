package vip.xiaonuo.biz.modular.stationinfo.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 电站发电量
 * @TableName sky_station_power
 */
@Data
@TableName("sky_station_power")
public class StationPower implements Serializable {
    /**
     * 
     */
    @TableId
    private String id;

    /**
     * 电站ID
     */
    private String stationId;

    /**
     * 时间类型：1.日发电量；2.月发电量；3.年发电量；4.总发电量；5.今日并网电量；6.累计并网电量；7.今日购电量；8.累计购电量
     */
    private Integer timeType;

    /**
     * 日期（年、年/月、年/月/日）
     */
    private String dateTime;

    /**
     * 运行时间
     */
    private Double runHours;

    /**
     * 发电量
     */
    private String stationPower;

    /**
     * 删除标志
     */
    private String deleteFlag;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 创建用户
     */
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /**
     * 修改用户
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    /**
     * 租户id
     */
    private String tenantId;

    private static final long serialVersionUID = 1L;
}