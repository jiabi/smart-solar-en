package vip.xiaonuo.biz.modular.installerstatistic.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/4/7 14:00
 */

@Getter
@Setter
public class InstallerStatisticEquInfoParam {
    private Double monPpv;

    private Integer monSate;
}
