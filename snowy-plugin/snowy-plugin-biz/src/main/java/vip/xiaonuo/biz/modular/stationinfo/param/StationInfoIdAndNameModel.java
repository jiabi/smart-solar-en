package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2023/10/9 16:34
 **/
@Getter
@Setter
public class StationInfoIdAndNameModel {
    /** 电站ID */
    @ApiModelProperty(value = "电站ID", position = 1)
    private String id;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称", position = 2)
    private String stationName;
}
