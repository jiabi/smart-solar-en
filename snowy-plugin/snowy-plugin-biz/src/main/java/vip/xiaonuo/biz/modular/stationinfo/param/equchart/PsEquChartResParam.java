package vip.xiaonuo.biz.modular.stationinfo.param.equchart;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/7 10:35
 */
@Getter
@Setter
public class PsEquChartResParam {
    /**
     * 日期集合
     */
    private List<String> dateList;

    /**
     * 发电量集合
     */
    private List<Double> power;

    /**
     * 用电量集合
     */
    private List<Double> energy;

    /**
     * 并网电量集合
     */
    private List<Double> GridEnergy;

    /**
     * 购电量集合
     */
    private List<Double> GridPurchase;

    /**
     * 充电量集合
     */
    private List<Double> charging;

    /**
     * 放电量集合
     */
    private List<Double> discharging;
}
