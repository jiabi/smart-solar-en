package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description: 发电实时vo
 * @Author: wangjian
 * @CreateTime: 2023-11-15  13:36
 */
@Getter
@Setter
public class StationInfoOverviewPowerVO {
    /** 当日发电量 */
    @ApiModelProperty(value = "当日发电量")
    private String monToday;

    /** 当月发电量 */
    @ApiModelProperty(value = "当月发电量")
    private String monMonth;

    /** 当年发电量 */
    @ApiModelProperty(value = "当年发电量")
    private String monYear;

    /** 累计发电量 */
    @ApiModelProperty(value = "累计发电量")
    private String monTotal;

    /** 系统功率比 */
    @ApiModelProperty(value = "系统功率比")
    private String stationSysRate;

    /** 发电功率 */
    @ApiModelProperty(value = "发电功率")
    private Double elecEfficiency;

    /** 当日收益 */
    @ApiModelProperty(value = "当日收益")
    private Double dayIncome;

    /** 累计收益 */
    @ApiModelProperty(value = "累计收益")
    private Double totalIncome;

    /** 装机容量 */
    @ApiModelProperty(value = "装机容量")
    private Double stationSize;

    /** 电池功率 */
    @ApiModelProperty(value = "电池功率")
    private Double batPower;

    /** 电池容量 */
    @ApiModelProperty(value = "电池容量")
    private Double batSoc;

    /** 电网功率 */
    @ApiModelProperty(value = "电网功率")
    private Double monPower;

    /** 用电功率 */
    @ApiModelProperty(value = "用电功率")
    private Double loadPower;

    public StationInfoOverviewPowerVO() {
        // 将属性设置为默认值
        this.monToday = "0";
        this.monMonth = "0";
        this.monYear = "0";
        this.monTotal = "0";
        this.stationSysRate = "0";
        this.elecEfficiency = 0.0;
        this.dayIncome = 0.0;
        this.totalIncome = 0.0;
        this.stationSize = 0.0;
        this.batPower = 0.0;
        this.batSoc = 0.0;
        this.monPower = 0.0;
        this.loadPower = 0.0;
    }
}