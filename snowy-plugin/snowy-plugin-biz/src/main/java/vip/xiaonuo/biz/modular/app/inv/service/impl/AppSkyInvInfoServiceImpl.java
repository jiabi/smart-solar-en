package vip.xiaonuo.biz.modular.app.inv.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.alarminfocurrent.entity.SkyAlarmInfoCurrent;
import vip.xiaonuo.biz.modular.alarminfocurrent.mapper.SkyAlarmInfoCurrentMapper;
import vip.xiaonuo.biz.modular.app.entity.PageResDTO;
import vip.xiaonuo.biz.modular.app.inv.mapper.AppSkyInvInfoMapper;
import vip.xiaonuo.biz.modular.app.inv.param.AppSkyInvInfoPageReqAllParam;
import vip.xiaonuo.biz.modular.app.inv.param.AppSkyInvInfoPageVO;
import vip.xiaonuo.biz.modular.app.inv.service.AppSkyInvInfoService;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoPageParam;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoPageVO;
import vip.xiaonuo.biz.modular.invinfo.service.SkyInvInfoService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/3/14 17:11
 */
@Service
public class AppSkyInvInfoServiceImpl implements AppSkyInvInfoService {

    @Resource
    private AppSkyInvInfoMapper appSkyInvInfoMapper;

    @Resource
    private SkyAlarmInfoCurrentMapper skyAlarmInfoCurrentMapper;

    @Resource
    private SkyInvInfoService skyInvInfoService;


    @Override
    public PageResDTO<AppSkyInvInfoPageVO> page(AppSkyInvInfoPageReqAllParam skyInvInfoPageParam) {

        PageResDTO<AppSkyInvInfoPageVO> pageDataModel = new PageResDTO<>(0, skyInvInfoPageParam.getCurrent(), Collections.emptyList());
        List<AppSkyInvInfoPageVO> list = appSkyInvInfoMapper.pageList(skyInvInfoPageParam);
        if (CollectionUtils.isEmpty(list)){
            return pageDataModel;
        }
        for (AppSkyInvInfoPageVO model:list) {
            // type
            if (!Objects.isNull(model.getType())) {
                // 同光伏一致 1-储能逆变器 2-并网逆变器 3-采集器
                if (model.getType() == 1) {
                    model.setType(3);
                } else if (model.getType() == 3) {
                    model.setType(1);
                }
            }
            //默认
            if (ObjectUtil.isEmpty(model.getMonState())) {
                model.setMonState(0);
            }

            // 状态
            boolean flag = true;
            if(StringUtils.isEmpty(model.getTimeZone())){
                model.setTimeZone(skyInvInfoService.timeZoneOperation());
                flag = false;
            }
            if (StringUtils.isEmpty(model.getMonitorTime())) {
                model.setMonState(10);
                continue;
            }
            LocalDateTime localDateTime=null;
            if(flag && ObjectUtil.isNotEmpty(model.getMonitorTime())){
                localDateTime = LocalDateTime.parse(model.getMonitorTime(),LocalDateUtils.DATETIME_FORMATTER);
            }
            if(!flag && ObjectUtil.isNotEmpty(model.getCurrentUpdateTime())){
                localDateTime = LocalDateTime.parse(model.getCurrentUpdateTime(), LocalDateUtils.DATETIME_FORMATTER);
            }
            if(localDateTime != null){
                boolean after = skyInvInfoService.getNowTimeByZoneId(model.getTimeZone()).isAfter(localDateTime.plusMinutes(15));
                if (after) {
                    model.setMonState(10);
                }
            }
        }
        pageDataModel.setRows(appSkyInvInfoMapper.countNum(skyInvInfoPageParam));
        pageDataModel.setData(list);
        return pageDataModel;
    }
}
