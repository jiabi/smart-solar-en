package vip.xiaonuo.biz.modular.equcontrol.param;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @Author wangjian
 * @Date 2024/5/8 17:52
 */
@Getter
@Setter
public class CheckPasswordParam {
    @NotBlank(message = "密码不能为空")
    private String password;
}
