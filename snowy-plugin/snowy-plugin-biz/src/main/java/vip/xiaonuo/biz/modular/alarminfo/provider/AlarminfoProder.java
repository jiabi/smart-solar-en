package vip.xiaonuo.biz.modular.alarminfo.provider;


import org.springframework.stereotype.Service;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.api.SkyAlarmInfoTripartiteApi;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.modular.alarminfo.mapper.SkyAlarmInfoMapper;
import vip.xiaonuo.biz.param.SkyAlarmInfoTripartiteVO;
import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteParam;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AlarminfoProder implements SkyAlarmInfoTripartiteApi {

    @Resource
    private SkyAlarmInfoMapper skyAlarmInfoMapper;

    @Override
    public List<SkyAlarmInfoTripartiteVO> alarmTripartite(List<String> invSns){
        SkyInvMonitorCurrentTripartiteParam skyInvMonitorCurrentTripartiteParam = new SkyInvMonitorCurrentTripartiteParam();
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if(roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            skyInvMonitorCurrentTripartiteParam.setDistributor(loginUser.getId());
        }else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            skyInvMonitorCurrentTripartiteParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            skyInvMonitorCurrentTripartiteParam.setStationContactId(loginUser.getId());
        }
       return skyAlarmInfoMapper.alarmTripartites(invSns,skyInvMonitorCurrentTripartiteParam);
    }

}
