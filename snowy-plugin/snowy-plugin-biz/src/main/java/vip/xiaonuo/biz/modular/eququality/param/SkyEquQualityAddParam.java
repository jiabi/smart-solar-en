/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.eququality.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 设备质保信息添加参数
 *
 * @author 全佳璧
 * @date  2023/09/25 14:20
 **/
@Getter
@Setter
public class SkyEquQualityAddParam {

    /** 设备SN */
    @ApiModelProperty(value = "设备SN", position = 1)
    private String equSn;

    /** 设备类型 */
    @ApiModelProperty(value = "设备类型", position = 2)
    private Integer equType;

    /** 设备型号 */
    @ApiModelProperty(value = "设备型号", position = 3)
    private String equModel;

    /** 发货日期 */
    @ApiModelProperty(value = "发货日期", position = 4)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /** 质保期限 */
    @ApiModelProperty(value = "质保期限", position = 5)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /** 质保时长 */
    @ApiModelProperty(value = "质保时长", position = 6)
    private Double timeLimit;

    /** 临期状态 */
    @ApiModelProperty(value = "临期状态", position = 7)
    private Integer criticalStatus;

}
