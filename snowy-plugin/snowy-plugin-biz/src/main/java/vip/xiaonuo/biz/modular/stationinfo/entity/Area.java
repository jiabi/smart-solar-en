package vip.xiaonuo.biz.modular.stationinfo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2023/9/20 11:56
 **/
@Getter
@Setter
@TableName("sky_area")
public class Area {
    /** 区域ID */
    @TableId
    @ApiModelProperty(value = "区域ID", position = 1)
    private String id;

    /** 洲 */
    @ApiModelProperty(value = "洲", position = 2)
    private String areaContient;

    /** 国家 */
    @ApiModelProperty(value = "国家", position = 3)
    private String areaCountry;

    /** 省 */
    @ApiModelProperty(value = "省", position = 4)
    private String areaProvice;

    /** 市 */
    @ApiModelProperty(value = "市", position = 5)
    private String areaCity;

    /** 详细地址 */
    @ApiModelProperty(value = "详细地址", position = 6)
    private String areaDetail;

    /** 经度 */
    @ApiModelProperty(value = "经度", position = 7)
    private String areaLng;

    /** 纬度 */
    @ApiModelProperty(value = "纬度", position = 8)
    private String areaLat;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 9)
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 10)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 11)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 12)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 13)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 14)
    private String tenantId;
}
