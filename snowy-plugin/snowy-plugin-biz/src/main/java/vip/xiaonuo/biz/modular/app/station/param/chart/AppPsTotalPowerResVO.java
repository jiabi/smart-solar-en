package vip.xiaonuo.biz.modular.app.station.param.chart;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/26 9:12
 */

@Getter
@Setter
public class AppPsTotalPowerResVO {
    /**
     * 日期集合
     */
    private List<String> yearList;

    /**
     * 发电量集合
     */
    private List<Double> volumeYearList;

    /**
     * 当日发电总量
     */
    private Double totalVolume;
}
