package vip.xiaonuo.biz.modular.homepage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.homepage.param.*;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoPageParam;
import vip.xiaonuo.biz.modular.stationinfo.param.StationStatusVO;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/2/21 17:51
 */
public interface HomePageService extends IService<StationInfo> {
    StationStatusVO statusAndAlarmCount();

    List<StationDistributeVO> distribute();

    StationHomePagePowerVO allPower();

    StationAllHisPowerVO allHisPower(Integer type, String date, SkyLoginUserIdParam param);
}
