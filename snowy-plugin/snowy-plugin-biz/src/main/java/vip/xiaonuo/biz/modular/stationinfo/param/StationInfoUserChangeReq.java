package vip.xiaonuo.biz.modular.stationinfo.param;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/1/3 14:34
 */

@Getter
@Setter
public class StationInfoUserChangeReq {

    /**
     * 电站id集合
     */
    private List<Long> psIds;

    /**
     * 安装商
     */
    private String stationCompany;

}
