/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.collectormqtt2.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.collectormqtt2.entity.SkyCollectorMqtt2;
import vip.xiaonuo.biz.modular.collectormqtt2.param.SkyCollectorMqtt2AddParam;
import vip.xiaonuo.biz.modular.collectormqtt2.param.SkyCollectorMqtt2EditParam;
import vip.xiaonuo.biz.modular.collectormqtt2.param.SkyCollectorMqtt2IdParam;
import vip.xiaonuo.biz.modular.collectormqtt2.param.SkyCollectorMqtt2PageParam;
import vip.xiaonuo.biz.modular.collectormqtt2.service.SkyCollectorMqtt2Service;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 采集器转发控制器
 *
 * @author hao
 * @date  2024/09/13 16:06
 */
@Api(tags = "采集器转发控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyCollectorMqtt2Controller {

    @Resource
    private SkyCollectorMqtt2Service skyCollectorMqtt2Service;

    /**
     * 获取采集器转发分页
     *
     * @author hao
     * @date  2024/09/13 16:06
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取采集器转发分页")
    @SaCheckPermission("/biz/collectormqtt2/page")
    @GetMapping("/biz/collectormqtt2/page")
    public CommonResult<Page<SkyCollectorMqtt2>> page(SkyCollectorMqtt2PageParam skyCollectorMqtt2PageParam) {
        return CommonResult.data(skyCollectorMqtt2Service.page(skyCollectorMqtt2PageParam));
    }

    /**
     * 添加采集器转发
     *
     * @author hao
     * @date  2024/09/13 16:06
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加采集器转发")
    @CommonLog("添加采集器转发")
    @SaCheckPermission("/biz/collectormqtt2/add")
    @PostMapping("/biz/collectormqtt2/add")
    public CommonResult<String> add(@RequestBody @Valid SkyCollectorMqtt2AddParam skyCollectorMqtt2AddParam) {
        skyCollectorMqtt2Service.add(skyCollectorMqtt2AddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑采集器转发
     *
     * @author hao
     * @date  2024/09/13 16:06
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑采集器转发")
    @CommonLog("编辑采集器转发")
    @SaCheckPermission("/biz/collectormqtt2/edit")
    @PostMapping("/biz/collectormqtt2/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyCollectorMqtt2EditParam skyCollectorMqtt2EditParam) {
        skyCollectorMqtt2Service.edit(skyCollectorMqtt2EditParam);
        return CommonResult.ok();
    }

    /**
     * 删除采集器转发
     *
     * @author hao
     * @date  2024/09/13 16:06
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除采集器转发")
    @CommonLog("删除采集器转发")
    @SaCheckPermission("/biz/collectormqtt2/delete")
    @PostMapping("/biz/collectormqtt2/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyCollectorMqtt2IdParam> skyCollectorMqtt2IdParamList) {
        skyCollectorMqtt2Service.delete(skyCollectorMqtt2IdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取采集器转发详情
     *
     * @author hao
     * @date  2024/09/13 16:06
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取采集器转发详情")
    @SaCheckPermission("/biz/collectormqtt2/detail")
    @GetMapping("/biz/collectormqtt2/detail")
    public CommonResult<SkyCollectorMqtt2> detail(@Valid SkyCollectorMqtt2IdParam skyCollectorMqtt2IdParam) {
        return CommonResult.data(skyCollectorMqtt2Service.detail(skyCollectorMqtt2IdParam));
    }

}
