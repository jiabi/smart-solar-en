package vip.xiaonuo.biz.core.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.util.Assert;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/3/6 17:39
 */
public class ChartUtil {

    protected static final int[] ARRY = {216, 108, 72, 36, 18};

//    protected static final int[] ARRY = {288, 144, 96, 48, 24};

    protected static final int[] MINUTES_ARRY = {5, 10, 15, 30, 60};

    private static final int MIN_HOUR_5 = 5;
    private static final int MAX_HOUR_21 = 21;

    private static final int MIN_HOUR_0 = 0;
    private static final int MAX_HOUR_24 = 24;

    /**
     * 初始化time时间X轴list
     *
     * @param type 1-4（[5分钟,10分钟,15分钟,30分钟,1小时]）
     * @return
     */
    public static List<LocalDateTime> initTimeAxisList(LocalDate localDate, int type) {
        Assert.isTrue(ARRY.length >= type, "type类型错误");
        int size = ARRY[type - 1];
        //默认5分钟
        int minutesPeriod = MINUTES_ARRY[type - 1];
        List<LocalDateTime> timeList = new ArrayList<>(size);
        for (int h = 4; h < 22; h++) {
            //每隔5分钟1个点，每小时共12各点
            for (int m = 0; m < 60; m += minutesPeriod) {
                timeList.add(LocalDateTime.of(localDate, LocalTime.of(h, m, 0)));
            }
        }
        return timeList;
    }


    /**
     * 初始化time时间X轴list
     *
     * @param type 1-4（[5分钟,10分钟,15分钟,30分钟,1小时]）
     * @return
     */
    public static List<LocalDateTime> initTimeAxisList(LocalDate localDate, LocalTime endTime, int type) {
        Assert.isTrue(ARRY.length >= type, "type类型错误");
        int size = ARRY[type - 1];
        //默认5分钟
        int minutesPeriod = MINUTES_ARRY[type - 1];
        List<LocalDateTime> timeList = new ArrayList<>(size);
        int endHour = endTime.getHour();
        int endMinute = endTime.getMinute();
        for (int h = 4; h < 22; h++) {
            //每隔5分钟1个点，每小时共12各点
            for (int m = 0; m < 60; m += minutesPeriod) {
                timeList.add(LocalDateTime.of(localDate, LocalTime.of(h, m, 0)));
                if (h > endHour || (endHour == h && m > endMinute)) {
                    return timeList;
                }

            }
        }
        return timeList;
    }

    public static List<Double> addLists(List<Double> list1, List<Double> list2) {
        List<Double> sumList = new ArrayList<>();
        if (list1.size() != list2.size()) {
            throw new IllegalArgumentException("Lists must have the same size");
        }
        for (int i = 0; i < list1.size(); i++) {
            sumList.add(list1.get(i) + list2.get(i));
        }
        return sumList;
    }


    /**
     * 初始化数据点List
     *
     * @param type
     * @return
     */
    public static List<Double> initDataList(Integer type) {
        Assert.isTrue(ARRY.length >= type, "type类型错误");
        int size = ARRY[type - 1];
        Double[] result = new Double[size];
        for (int i = 0; i < size; i++) {
            result[i] = 0D;
        }
        return Arrays.asList(result);
    }


    /**
     * 数据转换
     *
     * @param objects
     * @param pA
     * @param pB
     * @param <T>
     * @param <R>
     * @param isRepeat 是否重复最后一个数据
     * @return
     * @parm type    （5-分钟 15分钟 30分钟 1小时）
     */
    public static <T, R> List<Double> getPropertyValues(List<T> objects, Function<T, LocalDateTime> pA, Function<T, Double> pB, Integer type, boolean isRepeat) {
        Assert.isTrue(ARRY.length >= type, "type类型错误");
        int size = ARRY[type - 1];
        Double[] result = new Double[size];
        for (int i = 0; i < size; i++) {
            result[i] = 0D;
        }
        final int minHour = 4;
        final int maxHour = 22;
        if (CollectionUtils.isNotEmpty(objects)) {
            Map<LocalDateTime, Double> keyMaps = objects.stream().filter(obj -> Objects.nonNull(pB.apply(obj))).collect(Collectors.toMap(pA, pB, (v1, v2) -> v1));
            if (MapUtils.isEmpty(keyMaps)) {
                return Arrays.asList(result);
            }
            for (Map.Entry<LocalDateTime, Double> keyMap : keyMaps.entrySet()) {
                LocalDateTime key = keyMap.getKey();
                //时分秒
                LocalTime localTime = key.toLocalTime();
                int hour = localTime.getHour();
                if (minHour > hour || hour > maxHour) {
                    continue;
                }
                //计算key所在位置
                int index = getIndexByLocalTime2(localTime, minHour, type);
                if (index >= size) {
                    continue;
                }
                result[index] = keyMap.getValue();
            }
            if (isRepeat) {
                Double preVal = 0D;
                //后续数据点设置为重复数据
                for (int i = 0; i < size; i++) {
                    Double aDouble = result[i];
                    if (Double.compare(aDouble, 0D) > 0) {
                        preVal = result[i];
                    } else {
                        result[i] = preVal;
                    }
                }
            }
        }
        return Arrays.asList(result);
    }

    /**
     * 获取key 所在位置
     *
     * @param localTime
     * @param type
     * @return
     */
    public static int getIndexByLocalTime(LocalTime localTime, Integer type) {
        int minutesPeriod = MINUTES_ARRY[type - 1];
        //小时 0-23
        int hours = localTime.getHour();
        hours = hours - 6;
        //分 0-59
        int minutes = localTime.getMinute();
        int totalMinutes = (hours * 60) + minutes;
        int arrayIndex = totalMinutes / minutesPeriod;
        return arrayIndex;
    }
    public static int randomInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }
    /**
     * 获取key 所在位置
     *
     * @param localTime
     * @param minHour
     * @param type
     * @return
     */
    public static int getIndexByLocalTime2(LocalTime localTime, int minHour, Integer type) {
        int minutesPeriod = MINUTES_ARRY[type - 1];
        //小时 0-23
        int hours = localTime.getHour() - minHour;
        //分 0-59
        int minutes = localTime.getMinute();
        int totalMinutes = (hours * 60) + minutes;
        int roundedMinutes = (totalMinutes / minutesPeriod) * minutesPeriod;
        int arrayIndex = roundedMinutes / minutesPeriod;
        return arrayIndex;
    }
//
//    public static void main(String[] args) {
//        List<PowerStationRecordListResDTO> resDTOList = new ArrayList<>();
//        LocalDateTime localDateTime = LocalDateTime.of(2024, 1, 20, 07, 20, 33);
//        Double etotal=Integer.valueOf(randomInt(1,30)).doubleValue();
//        for (int i = 0; i < 125; i++) {
//            PowerStationRecordListResDTO aaa = new PowerStationRecordListResDTO();
//            localDateTime = localDateTime.plusMinutes(randomInt(1,10));
//            aaa.setNbTime(localDateTime);
//            aaa.setTodayEnergy(Integer.valueOf(randomInt(1,20)).doubleValue());
//            etotal=DoubleUtils.add(etotal,randomInt(1,20));
//            aaa.setETotal(etotal);
//            aaa.setUPv1(Integer.valueOf(randomInt(1,100)).doubleValue());
//            resDTOList.add(aaa);
//        }
//        System.out.println(JacksonUtils.obj2jsonIgnoreNull(resDTOList));
//        Map<String, List<Object>> powerStationRecordValues = ChartUtil.getPowerStationRecordValues(resDTOList, Arrays.asList("etotal","aa","vv","upv1"));
//        String s = JacksonUtils.obj2json(powerStationRecordValues);
//        System.out.println(s);
////        for (Map.Entry<String, List<Object>> stringListEntry : powerStationRecordValues.entrySet()) {
////            System.out.println("key:"+stringListEntry.getKey());
////            System.out.println("val:"+stringListEntry.getValue());
////        }
//    }

    /**
     * PowerStationRecord 数据转换 5分钟
     *
     * @param resDTOList
     * @param paramsList
     * @return
     */
    public static Map<String, List<Object>> getPowerStationRecordValues(List<SkyInvMonitorCurrent> resDTOList, List<String> paramsList, String invType) {
        if (CollectionUtils.isEmpty(resDTOList) || CollectionUtils.isEmpty(paramsList)) {
            return null;
        }
        int size = ARRY[0];
        Object[] result = new Object[size];
        for (int i = 0; i < size; i++) {
            result[i] = null;
        }
        //初始化数组
        Map<String, Object[]> initMap = new HashMap<>(paramsList.size());
        for (String paramKey : paramsList) {
            initMap.putIfAbsent(paramKey, Arrays.copyOf(result, size));
        }
        int minHour = MIN_HOUR_5;
        int maxHour = MAX_HOUR_21;
        // 逆变器类型：1.并网机；2.储能机；3.电表；4.电池；5.采集器；6.微逆机；7.微逆机（储能）
        if (invType.equals("2") || invType.equals("7")) {
            minHour = MIN_HOUR_0;
            maxHour = MAX_HOUR_24;
        }

        //根据是分秒转化为map
        Map<LocalDateTime, SkyInvMonitorCurrent> keyMaps = resDTOList.stream().collect(Collectors.toMap(dto -> LocalDateTime.ofInstant(dto.getMonitorTimeFormat().toInstant(), ZoneId.systemDefault()),
                Function.identity(), (v1, v2) -> v1));
        if (MapUtils.isEmpty(keyMaps)) {
            return null;
        }
        for (Map.Entry<LocalDateTime, SkyInvMonitorCurrent> keyMap : keyMaps.entrySet()) { // 将查到的数据，循环，如果其中包含要查询的参数，则将对应的值所在位置取出
            LocalDateTime key = keyMap.getKey();
            //时分秒
            LocalTime localTime = key.toLocalTime();
            int hour = localTime.getHour();
            if (minHour > hour || hour > maxHour) {
                continue;
            }
            //计算key所在位置
            int index = getIndexByLocalTime2(localTime, minHour, 1);
            if (index >= size) {
                continue;
            }
            // valmap为一条数据的map形式
            Map<String, Object> valMap = JacksonUtils.json2map(JacksonUtils.obj2json(keyMap.getValue()));
            for (Map.Entry<String, Object[]> stringListEntry : initMap.entrySet()) {
                String key1 = stringListEntry.getKey();
                boolean exist = valMap.containsKey(key1);
                if (exist) {
                    Object[] value = stringListEntry.getValue();
                    value[index] = valMap.get(key1);
                }
            }
        }
//        if (isRepeat) {
//            Double preVal = 0D;
//            //后续数据点设置为重复数据
//            for (int i = 0; i < size; i++) {
//                Double aDouble = result[i];
//                if (Double.compare(aDouble, 0D) > 0) {
//                    preVal = result[i];
//                } else {
//                    result[i] = preVal;
//                }
//            }
//        }
        Map<String, List<Object>> resultMap = new HashMap<>(initMap.size());
        for (Map.Entry<String, Object[]> entry : initMap.entrySet()) {
            List<Object> list = Arrays.asList(entry.getValue());
            resultMap.put(entry.getKey(), list);
        }
        return resultMap;
    }

}
