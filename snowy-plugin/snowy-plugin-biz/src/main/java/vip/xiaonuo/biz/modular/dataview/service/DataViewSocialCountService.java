package vip.xiaonuo.biz.modular.dataview.service;

import vip.xiaonuo.biz.modular.dataview.param.DataViewMapVO;
import vip.xiaonuo.biz.modular.dataview.param.DataViewMonthPowerReq;
import vip.xiaonuo.biz.modular.dataview.param.DataViewSocialCountVO;
import vip.xiaonuo.biz.modular.homepage.param.SkyLoginUserIdParam;
import vip.xiaonuo.biz.modular.homepage.param.StationAllHisPowerVO;

import java.util.List;
import java.util.Set;

/**
 * @Author wangjian
 * @Date 2024/7/8 13:25
 */
public interface DataViewSocialCountService {


    DataViewSocialCountVO social();

    StationAllHisPowerVO monthPower(String date, SkyLoginUserIdParam param, String province);

    List<String> city();

    StationAllHisPowerVO yearPower(String date, SkyLoginUserIdParam param, String city);

    List<DataViewMapVO> map();
}
