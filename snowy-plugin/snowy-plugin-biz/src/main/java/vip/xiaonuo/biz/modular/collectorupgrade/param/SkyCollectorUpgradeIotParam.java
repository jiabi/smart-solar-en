package vip.xiaonuo.biz.modular.collectorupgrade.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/5/8 17:20
 */
@Getter
@Setter
public class SkyCollectorUpgradeIotParam {
    /** 发送时间(YYYY-MM-dd HH:mm:ss)例:2024-02-01 09:10:20 **/
    private String sendTime;

    /** 请求ID (云端下发指令唯一标识) **/
    private String requestId;

    /** 新固件的版本号 **/
    private String newVersion;

    /** 固件下载地址 **/
    private Integer status;
}
