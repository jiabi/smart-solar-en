/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invpower.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.invpower.entity.SkyInvPower;
import vip.xiaonuo.biz.modular.invpower.mapper.SkyInvPowerMapper;
import vip.xiaonuo.biz.modular.invpower.param.SkyInvPowerAddParam;
import vip.xiaonuo.biz.modular.invpower.param.SkyInvPowerEditParam;
import vip.xiaonuo.biz.modular.invpower.param.SkyInvPowerIdParam;
import vip.xiaonuo.biz.modular.invpower.param.SkyInvPowerPageParam;
import vip.xiaonuo.biz.modular.invpower.service.SkyInvPowerService;

import java.util.List;

/**
 * 设备图表Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/02/22 17:00
 **/
@Service
public class SkyInvPowerServiceImpl extends ServiceImpl<SkyInvPowerMapper, SkyInvPower> implements SkyInvPowerService {


    @Override
    public Page<SkyInvPower> page(SkyInvPowerPageParam skyInvPowerPageParam) {
        QueryWrapper<SkyInvPower> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(skyInvPowerPageParam.getInvSn())) {
            queryWrapper.lambda().like(SkyInvPower::getInvSn, skyInvPowerPageParam.getInvSn());
        }
        if(ObjectUtil.isAllNotEmpty(skyInvPowerPageParam.getSortField(), skyInvPowerPageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(skyInvPowerPageParam.getSortOrder());
            queryWrapper.orderBy(true, skyInvPowerPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(skyInvPowerPageParam.getSortField()));
        } else {
            queryWrapper.lambda().orderByAsc(SkyInvPower::getId);
        }
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyInvPowerAddParam skyInvPowerAddParam) {
        SkyInvPower skyInvPower = BeanUtil.toBean(skyInvPowerAddParam, SkyInvPower.class);
        this.save(skyInvPower);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyInvPowerEditParam skyInvPowerEditParam) {
        SkyInvPower skyInvPower = this.queryEntity(skyInvPowerEditParam.getId());
        BeanUtil.copyProperties(skyInvPowerEditParam, skyInvPower);
        this.updateById(skyInvPower);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyInvPowerIdParam> skyInvPowerIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyInvPowerIdParamList, SkyInvPowerIdParam::getId));
    }

    @Override
    public SkyInvPower detail(SkyInvPowerIdParam skyInvPowerIdParam) {
        return this.queryEntity(skyInvPowerIdParam.getId());
    }

    @Override
    public SkyInvPower queryEntity(String id) {
        SkyInvPower skyInvPower = this.getById(id);
        if(ObjectUtil.isEmpty(skyInvPower)) {
            throw new CommonException("设备图表不存在，id值为：{}", id);
        }
        return skyInvPower;
    }

}
