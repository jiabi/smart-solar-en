package vip.xiaonuo.biz.modular.homepage.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.config.StationInfoConfig;
import vip.xiaonuo.biz.core.enums.DateTypeEnum;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyDeleteFlagEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.DoubleUtils;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.homepage.param.*;
import vip.xiaonuo.biz.modular.homepage.service.HomePageService;
import vip.xiaonuo.biz.modular.installerstatistic.entity.SkyInstallerStatistic;
import vip.xiaonuo.biz.modular.installerstatistic.mapper.SkyInstallerStatisticMapper;
import vip.xiaonuo.biz.modular.installerstatistic.service.SkyInstallerStatisticService;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.mapper.StationInfoMapper;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoPageParam;
import vip.xiaonuo.biz.modular.stationinfo.param.StationStatusVO;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.PsChartDataResVO;
import vip.xiaonuo.biz.modular.stationinfo.service.StationChartService;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.mapper.SkyStationPowerMapper;
import vip.xiaonuo.biz.modular.stationuser.entity.SkyStationUser;
import vip.xiaonuo.biz.modular.stationuser.service.SkyStationUserService;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/2/21 17:52
 */
@Service
public class HomePageServiceImpl extends ServiceImpl<StationInfoMapper, StationInfo> implements HomePageService  {

    @Resource
    private StationInfoService stationInfoService;

    @Resource
    private StationInfoMapper stationInfoMapper;

    @Resource
    private SkyStationPowerMapper skyStationPowerMapper;

    @Autowired
    private StationInfoConfig stationInfoConfig;

    @Resource
    private SkyInstallerStatisticService skyInstallerStatisticService;

    @Resource
    private SkyInstallerStatisticMapper skyInstallerStatisticMapper;

    @Resource
    private SkyStationUserService skyStationUserService;

    @Resource
    private StationChartService stationChartService;

    @Override
    public StationStatusVO statusAndAlarmCount() {
        StationInfoPageParam param = new StationInfoPageParam();
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (org.springframework.util.CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompanyId(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }
        StationStatusVO stationStatusVO = stationInfoService.statusAndAlarmCount(param);
        stationStatusVO.setAllOffline(stationStatusVO.getAllOffline() + stationStatusVO.getAccessing()+stationStatusVO.getPartialOffline());
        stationStatusVO.setAccessing(0);
        return stationStatusVO;
    }

    @Override
    public List<StationDistributeVO> distribute() {
        // 登录信息
        SkyLoginUserIdParam param = new SkyLoginUserIdParam();
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (org.springframework.util.CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }
        List<StationDistributeVO> res = stationInfoMapper.distribute(param);
        return res;
    }

    @Override
    public StationHomePagePowerVO allPower() {
        StationHomePagePowerVO res = new StationHomePagePowerVO();
        res.setStationSize(0.0);
        res.setDayPower(0D);
        res.setMonTotal(0D);
        res.setYearPower(0D);
        res.setStationSysRate(0.0);
        res.setPac(0.0);

        // 登录信息
        SkyLoginUserIdParam param = new SkyLoginUserIdParam();
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();


        // 先去统计表查询，没查到再查其他表
        SkyInstallerStatistic statistic = null;
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (org.springframework.util.CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.SUPERADMIN.getValue())) {
            statistic = skyInstallerStatisticMapper.selectBySuperAdmin();
        } else {
            statistic = skyInstallerStatisticMapper.selectById(loginUser.getId());
        }
        double sumCurrentPower=0D;
        List<StationInfo> stationInfoList = characterAssignment(loginUser, roleCodeList);
        if(ObjectUtil.isNotEmpty(stationInfoList)){
            sumCurrentPower = stationInfoList.stream()
                    .filter(stationInfo -> Objects.nonNull(stationInfo.getElecEfficiency()))
                    .mapToDouble(StationInfo::getElecEfficiency)
                    .sum();
            List<String> stationIds = stationInfoList.stream().map(StationInfo::getId).collect(Collectors.toList());
            PsChartDataResVO psChartDataResVO = stationChartService.psChart(4, null, stationIds);
            res.setMonTotal(ObjectUtil.isEmpty(psChartDataResVO.getPower()) ? 0D : psChartDataResVO.getPower().stream().reduce(Double::sum).orElse(0D));
        }
        // 获取当前系统时区
        TimeZone defaultTimeZone = TimeZone.getDefault();
        // 获取当前系统时区的偏移量（毫秒）
        int rawOffset = defaultTimeZone.getRawOffset();
        // 将偏移量转换为小时
        int hours = rawOffset / (60 * 60 * 1000);
        String timeZone = "UTC" + (hours >= 0 ? "+" : "") + hours;
        if (statistic != null) {
            res.setStationSize(statistic.getInstalledCapacity());
//            res.setMonTotal(statistic.getPowerTotal());
            res.setDayPower(statistic.getPowerDaily());
            res.setMonthPower(statistic.getPowerMonth());
            res.setYearPower(statistic.getPowerYear());
            if (ObjectUtil.isNotEmpty(sumCurrentPower) && ObjectUtil.isNotEmpty(statistic.getInstalledCapacity())) {
                res.setStationSysRate(statistic.getInstalledCapacity()  == 0 ? 0 : DoubleUtils.round(sumCurrentPower / statistic.getInstalledCapacity() * 100, 2));
            } else {
                res.setStationSysRate(0D);
            }
            res.setPac(ObjectUtil.isNotEmpty(sumCurrentPower) ?DoubleUtils.round(sumCurrentPower, 2):0D);
            res.setTimeZone(timeZone);
            res.setTime(stationInfoMapper.getUpdateTime());

            return res;
        }

        // 直接查询数据表
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }
        List<StationPowerCountVO> power = stationInfoMapper.allPower(LocalDate.now().toString(),
                LocalDate.now().getMonth().toString(),
                String.valueOf(LocalDate.now().getYear()),
                param);
        for (StationPowerCountVO vo:power) {
            switch (vo.getTimeType()) {
                case 1:
                    res.setDayPower(DoubleUtils.round(Double.valueOf(vo.getPower()),2));
                    break;
                case 2:
                    res.setMonthPower(DoubleUtils.round(Double.valueOf(vo.getPower()),2));
                    break;
                case 3:
                    res.setYearPower(DoubleUtils.round(Double.valueOf(vo.getPower()),2));
                    break;
                case 4:
                    res.setMonTotal(DoubleUtils.round(Double.valueOf(vo.getPower()),2));
                    break;
            }
        }
        StationHomePagePowerVO vo = stationInfoMapper.getAllPsHomePageBaseInfo(param);
        if(ObjectUtil.isNotEmpty(vo)){
            res.setPac(vo.getPac()!=null? vo.getPac() : 0D);
            res.setStationSize(vo.getStationSize()!=null? vo.getStationSize() : 0D);
            res.setStationSysRate(vo.getStationSysRate()!=null? vo.getStationSysRate() : 0D);
        }
        res.setTimeZone(timeZone);
        res.setTime(stationInfoMapper.getUpdateTime());
        return res;
    }

    public List<StationInfo> characterAssignment(SaBaseLoginUser loginUser,List<String> roleCodeList){
        // 获取数据
        QueryWrapper<StationInfo> queryWrapper = new QueryWrapper();
        if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            queryWrapper.lambda().eq(StationInfo::getStationCompany,loginUser.getId());
        }
        //分销商
        if(roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())){
            List<String> stationIds = skyStationUserService.list(Wrappers.lambdaQuery(SkyStationUser.class).eq(SkyStationUser::getInstallerIdL2, loginUser.getId()))
                    .stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            if(org.springframework.util.CollectionUtils.isEmpty(stationIds)){
                return null;
            }
            queryWrapper.lambda().in(StationInfo::getId,stationIds);
        }
        //业主
        if(roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())){
            List<String> stationIds = skyStationUserService.list(Wrappers.lambdaQuery(SkyStationUser.class).eq(SkyStationUser::getUserId, loginUser.getId()))
                    .stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            if(org.springframework.util.CollectionUtils.isEmpty(stationIds)){
                return null;
            }
            queryWrapper.lambda().in(StationInfo::getId,stationIds);
        }
        return stationInfoService.list(queryWrapper);
    }

    @Override
    public StationAllHisPowerVO allHisPower(Integer type, String date, SkyLoginUserIdParam param) {
        if (DateTypeEnum.MONTH.getType().equals(type)) {
            //月发电统计
            return this.psMonthChart(date,param);
        } else if (DateTypeEnum.YEAR.getType().equals(type)) {
            //年发电统计
            return this.psYearChart(date, param);
        }
        return null;
    }


    private StationAllHisPowerVO psMonthChart(String date, SkyLoginUserIdParam param) {
        StationAllHisPowerVO res = new StationAllHisPowerVO();
        QueryWrapper<SkyStationPower> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().likeRight(SkyStationPower::getDateTime,date)
                .eq(SkyStationPower::getTimeType,1)
                .groupBy(SkyStationPower::getDateTime);
        if (ObjectUtil.isNotEmpty(param.getDistributor())) {
            List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getInstallerIdL2, param.getDistributor()));
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            List<String> psIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            queryWrapper.lambda().in(SkyStationPower::getStationId, psIdList);
        } else if (ObjectUtil.isNotEmpty(param.getStationCompany())) {
            List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getInstallerIdL1, param.getStationCompany()));
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            List<String> psIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            queryWrapper.lambda().in(SkyStationPower::getStationId, psIdList);
        } else if (ObjectUtil.isNotEmpty(param.getStationContactId())) {
            List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getUserId, param.getStationContactId()));
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            List<String> psIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            queryWrapper.lambda().in(SkyStationPower::getStationId, psIdList);
        }
        List<HisPowerAllPowerInfoModel> monthPowerList = skyStationPowerMapper.queryMonthPower(queryWrapper);
        if (CollectionUtils.isEmpty(monthPowerList)) {
            return null;
        }
        Map<String, String> dayMonthMaps = monthPowerList.stream().collect(Collectors.toMap(HisPowerAllPowerInfoModel::getDateTime, HisPowerAllPowerInfoModel::getStationPower));
        //当月多少日
        List<LocalDate> localDateList = this.getLocalDateList(LocalDateUtils.parseLocalDate(date + "-01", LocalDateUtils.DATE_FORMATTER));
        //当日日期
        List<String> dateList = new ArrayList<>();
        //当日发电量
        List<Double> powerList = new ArrayList<>();
        //当月总发电量
        double total = 0D;
        for (LocalDate localDate : localDateList) {
            String dateStr = LocalDateUtils.formatLocalDate(localDate, LocalDateUtils.DATE_FORMATTER);
            String orDefault = dayMonthMaps.getOrDefault(dateStr, "0");
            dateList.add(dateStr);
            powerList.add(DoubleUtils.round(Double.parseDouble(orDefault),2));
            total+=Double.parseDouble(orDefault);
        }
        res.setPower(powerList);
        res.setDateList(dateList);
        res.setTotal(DoubleUtils.round(total,2));
        return res;
    }

    private StationAllHisPowerVO psYearChart(String date, SkyLoginUserIdParam param) {
        StationAllHisPowerVO res = new StationAllHisPowerVO();
        QueryWrapper<SkyStationPower> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().likeRight(SkyStationPower::getDateTime,date)
                .eq(SkyStationPower::getTimeType,2)
                .groupBy(SkyStationPower::getDateTime);
        if (ObjectUtil.isNotEmpty(param.getDistributor())) {
            List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getInstallerIdL2, param.getDistributor()));
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            List<String> psIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            queryWrapper.lambda().in(SkyStationPower::getStationId, psIdList);
        } else if (ObjectUtil.isNotEmpty(param.getStationCompany())) {
            List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getInstallerIdL1, param.getStationCompany()));
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
                List<String> psIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
                queryWrapper.lambda().in(SkyStationPower::getStationId, psIdList);
        } else if (ObjectUtil.isNotEmpty(param.getStationContactId())) {
            List<SkyStationUser> list = skyStationUserService.list(new QueryWrapper<SkyStationUser>().lambda().eq(SkyStationUser::getUserId, param.getStationContactId()));
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            List<String> psIdList = list.stream().map(SkyStationUser::getStationId).collect(Collectors.toList());
            queryWrapper.lambda().in(SkyStationPower::getStationId, psIdList);
        }
        List<HisPowerAllPowerInfoModel> yearPowerList = skyStationPowerMapper.queryMonthPower(queryWrapper);
        if (CollectionUtils.isEmpty(yearPowerList)) {
            return null;
        }
        Map<String, String> monthYearMaps = yearPowerList.stream().collect(Collectors.toMap(HisPowerAllPowerInfoModel::getDateTime, HisPowerAllPowerInfoModel::getStationPower));
        //当年多少月
        List<String> monthList  = this.getMonthList(date);
        //当月日期
        List<String> dateList = new ArrayList<>();
        //当月发电量
        List<Double> powerList = new ArrayList<>();
        //当年总发电量
        double total = 0D;
        for (String month  : monthList) {
            String orDefault = monthYearMaps.getOrDefault(month, "0");
            dateList.add(month);
            powerList.add(DoubleUtils.round(Double.valueOf(orDefault),2));
            total+=Double.parseDouble(orDefault);
        }
        res.setPower(powerList);
        res.setDateList(dateList);
        res.setTotal(DoubleUtils.round(total,2));
        return res;
    }

    /**
     * 获取当日当所有时间5分钟间隔
     *
     * @param date
     * @return
     */
    private List<String> getDateTimeList(String date) {
        LocalDate localDate = LocalDate.parse(date);
        List<String> timeList = new ArrayList<>(216);
        for (int h = 4; h < 22; h++) {
            //每隔5分钟1个点，每小时共12各点
            for (int m = 0; m < 60; m += 5) {
                timeList.add(LocalDateUtils.formatLocalDateTime(LocalDateTime.of(localDate, LocalTime.of(h, m, 0)), LocalDateUtils.DATETIME_FORMATTER));
            }
        }
        return timeList;
    }

    /**
     * 获取当月所有日
     *
     * @param date
     * @return
     */
    private List<LocalDate> getLocalDateList(LocalDate date) {
        int year = date.getYear();
        int month = date.getMonthValue();
        int daysInMonth = date.lengthOfMonth();
        List<LocalDate> dates = new ArrayList<>();
        for (int day = 1; day <= daysInMonth; day++) {
            LocalDate currentDate = LocalDate.of(year, month, day);
            dates.add(currentDate);
        }
        return dates;
    }


    /**
     * 获取当年所有月
     *
     * @param year
     * @return
     */
    private List<String> getMonthList(String year) {
        List<String> monthList = new ArrayList<>(12);
        for (int i = 1; i <= 12; i++) {
            monthList.add(String.format("%s-%02d", year, i));
        }
        return monthList;
    }
}
