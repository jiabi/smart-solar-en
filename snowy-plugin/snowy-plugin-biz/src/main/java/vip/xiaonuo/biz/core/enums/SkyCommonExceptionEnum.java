/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.core.enums;

import lombok.Getter;

/**
 * 返回信息code值含义
 */
@Getter
public enum SkyCommonExceptionEnum {

    DATA_NOT_EXIST_9001(9001, "数据不存在！"),
    DATA_CHECK_DATE_ERROR_9002(9002, "日期格式错误！"),
    DATA_CHECK_OUT_LENGTH_9003(9003, "备注内容不超过50字符！"),
    NOT_EXIST_ROLE_9004(9004, "无角色权限！"),



    DATA_CHECK_OUT_LENGTH_9101(9101, "电站id不能为空！"),
    FILE_NAME_ENCODING_FAILED_9102(9102, "文件名称编码转换失败！"),
    EXIST_EQU_BIND_STATION_9103(9103, "存在设备绑定该电站，请先解绑设备！"),
    EXIST_OWNER_9104(9104, "手机号已存在，且与已存在业主姓名或工作单位地址不一样！"),
    TIME_ZONE_ERROR_9105(9105, "时区换算错误，请检查时区是否正确!"),
    AREA_SAVE_DEFAULT_9106(9106, "电站信息新增失败，地址保存失败!"),
    SN_CHECK_NOT_NULL_9107(9107, "设备SN不能为空！"),
    CHART_PARAM_CHECK_NULL_9108(9108, "日图表参数不能为空！"),
    DATA_LENGTH_OUT_3000_9109(9107, "最多上传3000条数据！"),
    INV_INSTALLED_PS_9109(9109, "设备：{}已绑定！"),
    NOT_PHONE_EMAIL_9110(9110, "手机或邮箱格式错误！"),
    VALID_CODE_ERROR_9111(9111, "验证码错误！"),
    EXIST_ACCOUNT_9112(9112, "存在重复的账号，账号为：{}"),
    PHONE_ERROR_9113(9113, "手机号码：{}格式错误"),
    PHONE_EXIST_9114(9114, "存在重复的手机号，手机号为：{}"),
    EMAIL_ERROR_9115(9115, "邮箱：{}格式错误"),
    EMAIL_EXIST_9116(9116, "存在重复的邮箱，邮箱为：{}"),
    DEVICE_TYPE_EXIST_9117(9117, "该设备类型参数列表已存在，型号为：{}"),
    EMAIL_USER_EXIST_9118(9118, "邮箱：{}，已注册"),
    STATION_NOT_EXIST_9001(9119, "电站：{}不存在！"),
    EQU_DELETE_EXIST_QUALITY_9120(9120, "该设备存在质保信息，请先删除该设备质保信息！"),
    PHONE_FORMAT_ERROR_9121(9120,"手机号格式错误"),
    PHONE_USER_EXIST_9122(9122, "手机：{}，已注册"),
    INSTALLER_NOT_NULL_9123(9123,"安装商信息不能为空"),
    NAME_EXIST_9124(9124,"该名称已存在，请重新命名！"),
    EQU_AUTO_TEST_PDF_PRODUCE_ERROR(9125,"设备自检pdf文档生成错误！"),
    NO_PERMISSION(9126,"没有权限！"),
    PWD_DECRYPT_ERROR_9127(9127,"密码解密失败，请检查前端公钥！"),
    ACCOUNT_ERROR_9128(9128,"账号错误！"),
    PWD_ERROR_9129(9129,"密码错误！"),
    UPGRADE_ERROR_9130(9130,"设备SN：{}，升级执行失败！"),
    USER_ROLE_NOT_EXIST_9131(9131,"没有该角色！"),
    EXIST_BELONG_USER_9132(9132,"存在所属账号{}，请先删除所属账号！"),
    ID_NULL(9133,"id不能为null"),
    UPGRADE_MODULE_NOT_NULL(9134,"升级模块不能为空"),
    MODEL_NAME_NULL(9135,"设备型号不能为空"),
    DATE_NAME_9136(9136,"请输入对应时间"),
    EQU_SN_EXIST_9137(9137,"设备SN：{}，已存在！"),
    INSTALLER_AND_USER_ALL_NULL_9138(9138,"业主和安装商信息不能同时为空！"),
    WEATHER_API_CONNECT_NUM_USED_9139(9139,"彩云天气接口调用次数已用完，请管理员购买套餐！"),
    SN_COLLECTOR_NOT_NULL_9107(9107, "采集器SN不能为空！"),
    FILE_UPLOAD_SUCCESS_9137(9137,"文件上传成功！");

    private final Integer code;

    private final String msg;

    SkyCommonExceptionEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
