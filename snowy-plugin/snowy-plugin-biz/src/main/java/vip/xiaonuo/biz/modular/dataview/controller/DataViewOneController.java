package vip.xiaonuo.biz.modular.dataview.controller;


import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.dataview.param.AreaStationProvinceCountVO;
import vip.xiaonuo.biz.modular.dataview.param.AreaStationProvinceVO;
import vip.xiaonuo.biz.modular.dataview.param.EquDayChartVO;
import vip.xiaonuo.biz.modular.dataview.service.DataViewOneService;
import vip.xiaonuo.biz.modular.dataview.service.DataViewSocialCountService;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author wanghao
 * @Date 2024/7/8 13:21
 */
@Api(tags = "大屏第一行")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class DataViewOneController {

    @Resource
    private DataViewOneService dataViewOneService;

    @ApiOperationSupport(order = 1)
    @ApiOperation("当日发电量")
    @PostMapping("/biz/dataView/equDayChart")
    public CommonResult<EquDayChartVO> equDayChart(){
        return CommonResult.data(dataViewOneService.equDayChart());
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation("电站设备统计(按市)")
    @PostMapping("/biz/dataView/statisticsPowerStationEquipment")
    public CommonResult<AreaStationProvinceCountVO> statisticsPowerStationEquipment(){
        return CommonResult.data(dataViewOneService.statisticsPowerStationEquipment());
    }

    @ApiOperationSupport(order = 3)
    @ApiOperation("设备排名(按市)")
    @PostMapping("/biz/dataView/statisticsDeviceRanking")
    public CommonResult<AreaStationProvinceCountVO> statisticsDeviceRanking(){
        return CommonResult.data(dataViewOneService.statisticsDeviceRanking());
    }
}
