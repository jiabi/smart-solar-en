package vip.xiaonuo.biz.modular.user.param;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/5/10 14:38
 */

@Getter
@Setter
public class BizUserPageInfoReqParam {
    /** 当前页 */
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    /** 每页条数 */
    @ApiModelProperty(value = "每页条数")
    private Integer size;

    /** 角色 */
    @ApiModelProperty(value = "角色")
    private String roleName;

    /** 用户名 */
    @ApiModelProperty(value = "用户名")
    private String account;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱")
    @TableField(insertStrategy = FieldStrategy.IGNORED, updateStrategy = FieldStrategy.IGNORED)
    private String email;

    /** 角色code */
    @ApiModelProperty(value = "角色code")
    private List<String> roleCode;

    public int getLimitStart() {
        return (getCurrent() - 1) * getSize();
    }
}
