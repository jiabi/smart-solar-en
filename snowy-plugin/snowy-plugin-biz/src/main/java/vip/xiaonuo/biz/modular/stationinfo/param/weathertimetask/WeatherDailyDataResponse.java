package vip.xiaonuo.biz.modular.stationinfo.param.weathertimetask;

/**
 * @Author wangjian
 * @Date 2024/5/14 17:53
 */
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class WeatherDailyDataResponse {
    @ApiModelProperty(position = 1)
    private String status;
    @ApiModelProperty(position = 2)
    private String api_version;
    @ApiModelProperty(position = 3)
    private String api_status;
    @ApiModelProperty(position = 4)
    private String lang;
    @ApiModelProperty(position = 5)
    private String unit;
    @ApiModelProperty(position = 6)
    private int tzShift;
    @ApiModelProperty(position = 7)
    private String timezone;
    @ApiModelProperty(position = 8)
    private long server_time;
    @ApiModelProperty(position = 9)
    private double[] location;
    @ApiModelProperty(position = 10)
    private Result result;

    @Getter
    @Setter
    public static class Result {
        private Daily daily;
        private Integer primary;

    }
}