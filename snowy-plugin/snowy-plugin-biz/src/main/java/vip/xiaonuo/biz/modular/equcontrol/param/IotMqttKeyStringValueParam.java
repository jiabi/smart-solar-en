package vip.xiaonuo.biz.modular.equcontrol.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/5/7 11:26
 */
@Getter
@Setter
public class IotMqttKeyStringValueParam {
    private String key;
    private String value;
    private Integer modbus;
}
