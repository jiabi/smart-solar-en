package vip.xiaonuo.biz.modular.app.station.param.chart;

import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.PsEquDayChartResParam;

import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/26 9:12
 */

@Getter
@Setter
public class AppPsDeviceDayPowerResVO {
    /**
     * 日期集合
     */
    private List<String> hourList;

    /**
     * 当日发电总量
     */
    private Double totalVolume;

    /**
     * 明细
     */
    private List<PsEquDayChartResParam.DayChartAnalysis> volumeHourList;


    @Setter
    @Getter
    public static  class DayChartAnalysis<T>{

        /**
         * 名称-（mppt电压1 （V））
         */
        private String name;

        /**
         * key="u_pv1"
         */
        private String key;

        /**
         * 数据点
         */
        private List<T>  dataList;

        public DayChartAnalysis(String name, String key, List<T> dataList) {
            this.name = name;
            this.key = key;
            this.dataList = dataList;
        }
    }
}
