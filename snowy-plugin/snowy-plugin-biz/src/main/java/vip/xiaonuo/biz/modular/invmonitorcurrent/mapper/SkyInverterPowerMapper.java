package vip.xiaonuo.biz.modular.invmonitorcurrent.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInverterPower;

/**
 * @Author wangjian
 * @Date 2023/10/13 13:21
 **/
public interface SkyInverterPowerMapper extends BaseMapper<SkyInverterPower> {
}
