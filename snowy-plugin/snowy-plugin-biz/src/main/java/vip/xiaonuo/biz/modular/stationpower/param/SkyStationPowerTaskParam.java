/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationpower.param;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 电站图表实体
 *
 * @author 全佳璧
 * @date  2024/02/22 16:57
 **/
@Getter
@Setter
public class SkyStationPowerTaskParam {

    /** ID */
    @TableId
    @ApiModelProperty(value = "ID", position = 1)
    private String id;

    /** 电站ID */
    @ApiModelProperty(value = "电站ID", position = 2)
    private String stationId;

    /** 时间类型 */
    @ApiModelProperty(value = "时间类型", position = 3)
    private Integer timeType;

    /** 日期 */
    @ApiModelProperty(value = "日期", position = 4)
    private String dateTime;

    /** 运行时间 */
    @ApiModelProperty(value = "运行时间", position = 5)
    private Double runHours;

    /** 发电量 */
    @ApiModelProperty(value = "发电量", position = 6)
    private String stationPower;

    /** 逆变器sn */
    @ApiModelProperty(value = "逆变器sn", position = 6)
    private String invSn;


}
