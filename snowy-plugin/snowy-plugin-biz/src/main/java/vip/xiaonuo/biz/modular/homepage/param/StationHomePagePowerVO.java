package vip.xiaonuo.biz.modular.homepage.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/2/26 15:21
 */

@Setter
@Getter
public class StationHomePagePowerVO {
    /** 总装机容量 */
    private Double stationSize;

    /** 累计发电量（单位：kWh） */
    private Double monTotal;

    /** 当日发电量（单位：kWh） */
    private Double dayPower;

    /** 当月发电量（单位：kWh） */
    private Double monthPower;

    /** 当年发电量 （单位：kWh）*/
    private Double yearPower;

    /** 系统功率比 */
    private Double stationSysRate;

    /** 当前发电总功率 */
    private Double pac;

    private String time;

    private String timeZone;
}
