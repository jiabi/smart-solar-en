package vip.xiaonuo.biz.modular.stationinfo.param;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @Author wangjian
 * @Date 2023/9/20 14:30
 **/
@Getter
@Setter
public class StationInfoPageVO {
    /** 电站ID */
    @ApiModelProperty(value = "电站ID", position = 1)
    private String id;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称", position = 2)
    private String stationName;

    /** 电站状态 */
    @ApiModelProperty(value = "电站状态", position = 3)
    private Integer stationStatus;

    /** 报警状态 */
    @ApiModelProperty(value = "报警状态", position = 4)
    private Integer stationAlarm;

    /** 装机容量 默认小数点后两位*/
    @ApiModelProperty(value = "装机容量 默认小数点后两位", position = 5)
    private Double stationSize;

    /** 发电功率 */
    @ApiModelProperty(value = "发电功率", position = 6)
    private Double elecEfficiency;

    /** 系统功率比 */
    @ApiModelProperty(value = "系统功率比", position = 6)
    private String stationSysRate;

    /** 当日发满小时 */
    @ApiModelProperty(value = "当日发满小时", position = 7)
    private String dayPowerhours;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 8)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 建站日期 */
    @ApiModelProperty(value = "建站日期", position = 9)
    private Date stationCreatetime;

    /** 并网状态 */
    @ApiModelProperty(value = "并网状态", position = 10)
    private Integer stationGridstatus;

    /** 电站类型 */
    @ApiModelProperty(value = "电站类型", position = 11)
    private Integer stationType;

    /** 电站系统 */
    @ApiModelProperty(value = "电站系统", position = 12)
    private Integer stationSystem;

    /** 安装公司 */
    @ApiModelProperty(value = "安装公司", position = 13)
    private String stationCompany;
}
