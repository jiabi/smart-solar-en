package vip.xiaonuo.biz.modular.user.param;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vip.xiaonuo.common.handler.CommonSm4CbcTypeHandler;

import java.util.Date;

/**
 * @Author wangjian
 * @Date 2024/5/10 14:38
 */

@Getter
@Setter
public class BizUserPageInfoResParam {
    /** id */
    @ApiModelProperty(value = "id")
    private String id;
    /** 角色 */
    @ApiModelProperty(value = "角色")
    private String roleName;

    /** 用户名 */
    @ApiModelProperty(value = "用户名")
    private String account;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱")
    @TableField(insertStrategy = FieldStrategy.IGNORED, updateStrategy = FieldStrategy.IGNORED)
    private String email;

    /** 手机 */
    @ApiModelProperty(value = "手机")
    @TableField(insertStrategy = FieldStrategy.IGNORED, updateStrategy = FieldStrategy.IGNORED, typeHandler = CommonSm4CbcTypeHandler.class)
    private String phone;

    /** 注册时间 */
    @ApiModelProperty(value = "注册时间")
    private Date createTime;

    /** 用户状态 */
    @ApiModelProperty(value = "用户状态")
    private String userStatus;

    /** 业主工作单位 */
    @ApiModelProperty(value = "业主工作单位")
    private String userAddr;
}
