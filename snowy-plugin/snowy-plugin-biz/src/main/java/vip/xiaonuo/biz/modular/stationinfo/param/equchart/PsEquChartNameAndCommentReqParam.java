package vip.xiaonuo.biz.modular.stationinfo.param.equchart;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/6/5 16:09
 */
@Getter
@Setter
public class PsEquChartNameAndCommentReqParam {
    /**
     * 中文字段名
     */
    private String name;

    /**
     * 属性名
     */
    private String status;
}
