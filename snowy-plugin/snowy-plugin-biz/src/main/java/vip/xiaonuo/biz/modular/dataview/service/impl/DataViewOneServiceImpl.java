package vip.xiaonuo.biz.modular.dataview.service.impl;

import cn.hutool.core.util.ObjectUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.modular.dataview.mapper.DataViewOneMapper;
import vip.xiaonuo.biz.modular.dataview.mapper.DataViewSocialCountMapper;
import vip.xiaonuo.biz.modular.dataview.param.AreaStationProvinceCountVO;
import vip.xiaonuo.biz.modular.dataview.param.AreaStationProvinceVO;
import vip.xiaonuo.biz.modular.dataview.param.EquDayChartParam;
import vip.xiaonuo.biz.modular.dataview.param.EquDayChartVO;
import vip.xiaonuo.biz.modular.dataview.service.DataViewOneService;
import vip.xiaonuo.biz.modular.dataview.service.DataViewSocialCountService;
import vip.xiaonuo.biz.modular.homepage.param.StationHomePagePowerVO;
import vip.xiaonuo.biz.modular.homepage.service.HomePageService;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoPageParam;
import vip.xiaonuo.biz.modular.invmonitorcurrent.mapper.SkyInvMonitorCurrentMapper;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/7/8 13:25
 */
@Service
public class DataViewOneServiceImpl implements DataViewOneService {

    @Resource
    private DataViewOneMapper dataViewOneMapper;
    @Resource
    private SkyInvMonitorCurrentMapper skyInvMonitorCurrentMapper;
    @Resource
    private HomePageService homePageService;

    @Override
    public EquDayChartVO equDayChart() {
        //权限区分
        SkyInvInfoPageParam skyInvInfoPageParam = rolePermission();
        EquDayChartVO resVO = new EquDayChartVO();
        //获取设备sn
        List<String> invSns = dataViewOneMapper.queryEquByAreaProvince(skyInvInfoPageParam);
        if (!CollectionUtils.isEmpty(invSns)){
            SimpleDateFormat tableDateFormat = new SimpleDateFormat("yyyyMM");
            SimpleDateFormat fieldDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat chartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            List<EquDayChartParam> dayDataList = skyInvMonitorCurrentMapper.selectDayHistory(tableDateFormat.format(new Date()), invSns, fieldDateFormat.format(new Date()));
            Map<String, EquDayChartParam> dayTimeMaps = dayDataList.stream().collect(Collectors.toMap(p->
                    chartDateFormat.format(p.getMonitorTimeFormat()), Function.identity()));
            if(CollectionUtils.isEmpty(dayTimeMaps)){
                return resVO;
            }
            //获取凌晨12点到现在的每隔一小时的时间
            List<String> dateTimes = dateTimes();
            //当日发电量总数
            List<Double> dataPowers = new ArrayList<>();
            //时间轴
            List<String> dateList = new ArrayList<>();
            DateTimeFormatter originalFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            DateTimeFormatter targetFormatter = DateTimeFormatter.ofPattern("HH:mm");
            for (String localDate : dateTimes) {
                //数据赋值
                dataPowers.add(obtainDataHour(localDate,dayTimeMaps));
                LocalDateTime dateTime = LocalDateTime.parse(localDate, originalFormatter);
                dateList.add(dateTime.format(targetFormatter));
            }
            resVO.setPowers(dataPowers);
            resVO.setDateList(dateList);
            //总发电量
//            StationHomePagePowerVO stationHomePagePowerVO = homePageService.allPower();
//            resVO.setPowerTotal(ObjectUtil.isNotEmpty(stationHomePagePowerVO) && stationHomePagePowerVO.getDayPower()!=null?stationHomePagePowerVO.getDayPower():0D);
        }
        return resVO;
    }

    /**
     * 区分权限方法
     * @return
     */
    public SkyInvInfoPageParam rolePermission(){
        SkyInvInfoPageParam param = new SkyInvInfoPageParam();
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }
        return param;
    }

    /**
     * 获取当天24小时中整点数据
     * @return
     */
    public List<String> dateTimes(){
        List<String> dateTimes = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        // 获取当前日期的午夜时间
        LocalDateTime dateTime = LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.of(0, 0));
        // 当日凌晨到当前时间的整点数据
        while (dateTime.isBefore(LocalDateTime.now()) || dateTime.equals(LocalDateTime.now())) {
            String formattedDateTime = dateTime.format(formatter);
            dateTimes.add(formattedDateTime);
            // 每次增加一个小时
            dateTime = dateTime.plusHours(1);
        }
        return dateTimes;
    }

    /**
     * 每间隔一小时的发电功率
     * @param localDate
     * @param dayTimeMaps
     * @return
     */
    public Double obtainDataHour(String localDate,Map<String, EquDayChartParam> dayTimeMaps){
        List<Double> entryDoubleList = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startTime = LocalDateTime.parse(localDate, formatter);
        // 增加一个小时
        LocalDateTime endTime = startTime.plusHours(1);
        //查找在localDate和localDate+1小时的数据
        for (Map.Entry<String, EquDayChartParam> entry : dayTimeMaps.entrySet()) {
            LocalDateTime entryTime = LocalDateTime.parse(entry.getKey(), formatter);
            entryDoubleList.add(entryTime.compareTo(startTime)>=0 && entryTime.compareTo(endTime)<0?entry.getValue().getMonPacTotal():0D);
        }
        return entryDoubleList.stream().mapToDouble(Double::doubleValue).sum();
    }


    @Override
    public AreaStationProvinceCountVO statisticsPowerStationEquipment(){
        AreaStationProvinceCountVO areaStationProvinceCountVO = new AreaStationProvinceCountVO();
        List<AreaStationProvinceVO> areaStationProvinceList = new ArrayList<>();
        //区分权限
        SkyInvInfoPageParam skyInvInfoPageParam = rolePermission();
        List<AreaStationProvinceVO> areaStationProvinces = dataViewOneMapper.queryAreaByStationProvince(skyInvInfoPageParam);
        if(!CollectionUtils.isEmpty(areaStationProvinces)){
            //根据区域进行分组
            Map<String, List<AreaStationProvinceVO>> groupedByAreaCities = areaStationProvinces.stream()
                    .filter(vo -> StringUtils.isNotEmpty(vo.getAreaCity()))
                    .collect(Collectors.groupingBy(AreaStationProvinceVO::getAreaCity));
            for (Map.Entry<String, List<AreaStationProvinceVO>> entryMap : groupedByAreaCities.entrySet()) {
                AreaStationProvinceVO areaStationProvinceVO = new AreaStationProvinceVO();
                //装入城市名称
                areaStationProvinceVO.setAreaCity(entryMap.getKey());
                //电站总数
                areaStationProvinceVO.setStationTotal(!CollectionUtils.isEmpty(entryMap.getValue())
                        ?Math.toIntExact(entryMap.getValue().stream().map(AreaStationProvinceVO::getStationId).distinct().count()):0);
                //设备总数
                areaStationProvinceVO.setEqnSnTotal(!CollectionUtils.isEmpty(entryMap.getValue())
                        ?Math.toIntExact(entryMap.getValue().stream().map(AreaStationProvinceVO::getEquSn).distinct().count()):0);
                areaStationProvinceList.add(areaStationProvinceVO);
            }
        }
        if(!CollectionUtils.isEmpty(areaStationProvinceList)){
            List<AreaStationProvinceVO> areaStationProvinceVOList = areaStationProvinceList.stream()
                    .sorted(Comparator.comparingInt(AreaStationProvinceVO::getStationTotal).reversed())
                    .collect(Collectors.toList());
            areaStationProvinceCountVO.setAreaStationProvinces(areaStationProvinceVOList);
        }
        areaStationProvinceCountVO.setEquTotalNumber(!CollectionUtils.isEmpty(areaStationProvinceList)
                ?areaStationProvinceList.stream().mapToInt(AreaStationProvinceVO::getEqnSnTotal).sum():0);
        return areaStationProvinceCountVO;
    }

    @Override
    public AreaStationProvinceCountVO statisticsDeviceRanking(){
       return statisticsPowerStationEquipment();
    }
}
