package vip.xiaonuo.biz.modular.equcontrol.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/5/7 11:26
 */
@Getter
@Setter
public class SkyEquControlParameter {
    private String key;
    private Integer value;
    private Integer modbus;
}
