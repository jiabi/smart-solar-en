package vip.xiaonuo.biz.modular.equcontrol.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * mqtt与modbus对照表(SkyMqttModbus)实体类
 *
 * @author wangjian
 * @since 2024-05-06 15:09:53
 */
public class SkyMqttModbus implements Serializable {
    private static final long serialVersionUID = -18835524617358975L;
    /**
     * ID
     */
    private String id;
    /**
     * mqtt key
     */
    private String mqttKey;
    /**
     * modbus addr
     */
    private Integer modbusAddr;
    /**
     * equType 1：INV；2：PCS；3：METER；4：BATT
     */
    private Integer equType;
    /**
     * remark
     */
    private String remark;
    /**
     * 删除标志
     */
    private String deleteFlag;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建用户
     */
    private String createUser;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 修改用户
     */
    private String updateUser;
    /**
     * 租户id
     */
    private String tenantId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMqttKey() {
        return mqttKey;
    }

    public void setMqttKey(String mqttKey) {
        this.mqttKey = mqttKey;
    }

    public Integer getModbusAddr() {
        return modbusAddr;
    }

    public void setModbusAddr(Integer modbusAddr) {
        this.modbusAddr = modbusAddr;
    }

    public Integer getEquType() {
        return equType;
    }

    public void setEquType(Integer equType) {
        this.equType = equType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

}

