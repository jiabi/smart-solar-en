package vip.xiaonuo.biz.modular.equrelationship.param;


import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class SkyEquRelationshipTreeVO {

    /** 采集器SN */
    @ApiModelProperty(value = "采集器SN")
    private String collectorSn;

    /** 设备状态 */
    @ApiModelProperty(value = "设备状态")
    private Integer status;

    /** 修改时间 */
    @ApiModelProperty(value = "设备状态")
    private Date updateTime;

    @ApiModelProperty(value = "采集器类型")
    private Integer typeId;



    /** 通讯关系数据集合 */
    @ApiModelProperty(value = "通讯关系数据集合")
    private List<SkyEquRelationshipVO> relationshipVO;
}
