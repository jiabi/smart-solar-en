/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invpower.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.homepage.param.HisPowerAllPowerInfoModel;
import vip.xiaonuo.biz.modular.invpower.entity.SkyInvPower;

import java.util.List;
import java.util.Set;

/**
 * 设备图表Mapper接口
 *
 * @author 全佳璧
 * @date  2024/02/22 17:00
 **/
public interface SkyInvPowerMapper extends BaseMapper<SkyInvPower> {
    List<HisPowerAllPowerInfoModel> equMonthChart(@Param("date") String date, @Param("deviceSn") String deviceSn);

    List<HisPowerAllPowerInfoModel> equYearChart(@Param("date") String date, @Param("deviceSn") String deviceSn);

    List<HisPowerAllPowerInfoModel> totalPower(@Param("ew") QueryWrapper<SkyInvPower> queryWrapper);

    List<HisPowerAllPowerInfoModel> equTotalChart(@Param("deviceSn") String deviceSn);

    List<SkyInvPower> selectInvPowers(@Param("invSns") Set<String> invSns, @Param("dateTime") String dateTime, @Param("timeType") int timeType);
    SkyInvPower selectInvPowerSum(@Param("invSn") String invSn, @Param("dateTime") String dateTime, @Param("timeType") int timeType);
    int insertInvPowers(@Param("list") List<SkyInvPower> skyInvPowers);
    int updateInvPowers(List<SkyInvPower> skyInvPowers);
    SkyInvPower selectInvPower(@Param("invSn") String invSn, @Param("dateTime") String dateTime, @Param("timeType") int timeType);
    int insertInvPower(SkyInvPower skyInvPower);
    int updateInvPower(SkyInvPower skyInvPower);
}
