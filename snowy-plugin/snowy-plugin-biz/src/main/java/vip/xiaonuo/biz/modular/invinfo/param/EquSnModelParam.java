package vip.xiaonuo.biz.modular.invinfo.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/4/10 9:43
 */

@Getter
@Setter
public class EquSnModelParam {

    private String sn;

    private String modelId;
}
