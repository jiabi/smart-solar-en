/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invinfo.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.FileUploadUtil;
import vip.xiaonuo.biz.modular.invinfo.param.*;
import vip.xiaonuo.biz.modular.stationinfo.param.StationInfoPageParam;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.invinfo.service.SkyInvInfoService;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 逆变器信息控制器
 *
 * @author 全佳璧
 * @date  2023/10/05 16:24
 */
@Api(tags = "逆变器信息控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
@Slf4j
public class SkyInvInfoController {

    @Resource
    private SkyInvInfoService skyInvInfoService;

    /**
     * 获取逆变器信息分页
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取逆变器信息分页")
    @SaCheckPermission("/biz/invinfo/page")
    @GetMapping("/biz/invinfo/page")
    public CommonResult<Page<SkyInvInfoPageVO>> page(SkyInvInfoPageReqDTO skyInvInfoPageReqDTO) {
        SkyInvInfoPageParam param = new SkyInvInfoPageParam();
        BeanUtil.copyProperties(skyInvInfoPageReqDTO,param);
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }
        return CommonResult.data(skyInvInfoService.page(param));
    }

    /**
     * 添加逆变器信息
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加逆变器信息")
    @CommonLog("添加逆变器信息")
    @SaCheckPermission("/biz/invinfo/add")
    @PostMapping("/biz/invinfo/add")
    public CommonResult<String> add(@RequestBody @Valid SkyInvInfoAddParam skyInvInfoAddParam) {
        skyInvInfoService.add(skyInvInfoAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑逆变器信息
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑逆变器信息")
    @CommonLog("编辑逆变器信息")
    @SaCheckPermission("/biz/invinfo/edit")
    @PostMapping("/biz/invinfo/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyInvInfoEditParam skyInvInfoEditParam) {
        skyInvInfoService.edit(skyInvInfoEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除逆变器信息
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除逆变器信息")
    @CommonLog("删除逆变器信息")
    @SaCheckPermission("/biz/invinfo/delete")
    @PostMapping("/biz/invinfo/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<SkyInvInfoIdParam> skyInvInfoIdParamList) {
        skyInvInfoService.delete(skyInvInfoIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取逆变器信息详情
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取逆变器信息详情")
    @SaCheckPermission("/biz/invinfo/detail")
    @GetMapping("/biz/invinfo/detail")
    public CommonResult<SkyInvInfoDetailVO> detail(@Valid SkyInvInfoIdParam skyInvInfoIdParam) {
        return CommonResult.data(skyInvInfoService.detail(skyInvInfoIdParam));
    }

    /**
     * 获取动态字段的配置
     *
     * @author 全佳璧
     * @date  2023/10/05 16:24
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("获取逆变器信息动态字段的配置")
    @SaCheckPermission("/biz/invinfo/dynamicFieldConfigList")
    @GetMapping("/biz/invinfo/dynamicFieldConfigList")
    public CommonResult<List<JSONObject>> dynamicFieldConfigList() {
        return CommonResult.data(skyInvInfoService.dynamicFieldConfigList());
    }

    @Resource
    private AppProperties appProperties;
    /**
     * 获取动态字段的配置
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("获取逆变器信息动态字段的配置")
//    @SaCheckPermission("/biz/invinfo/dynamicFieldConfigList")
    @GetMapping("/biz/invinfo/upload")
    public CommonResult<String> upload(HttpServletRequest request, @RequestParam(value = "uploadFile", required = false) MultipartFile multiFile) {
        String path = System.getProperty("user.dir");
        JSONObject json = new JSONObject();
        try {
            Pair<Boolean, String> pair = checkFile(multiFile);
            if (!pair.getLeft()) {
                json.put("msg", pair.getRight());
                return CommonResult.data(json.toString());
            }
            boolean b = FileUploadUtil.uploadToServer(multiFile, appProperties.getUploadPath(), multiFile.getOriginalFilename());
            json.put("msg", b ? appProperties.getUploadPath() + multiFile.getOriginalFilename() : "上传失败");
            return CommonResult.data(b ? appProperties.getUploadPath() + multiFile.getOriginalFilename() : "上传失败");
        } catch (Exception e) {
            log.error("系统异常e:", e);
            json.put("msg", "上传失败");
            return CommonResult.data("上传失败");
        }
    }


    /**
     * 获取未绑定设备集合
     */
    @ApiOperationSupport(order = 22)
    @ApiOperation("获取未绑定设备集合")
    @PostMapping("/biz/invinfo/invNotBind")
    public CommonResult<List<SkyInvNotBindResVO>>  invNotBind(@RequestBody SkyInvNotBindReqParam reqParam) {
        return CommonResult.data(skyInvInfoService.invNotBind(reqParam));
    }

    /**
     * 绑定设备
     */
    @ApiOperationSupport(order = 22)
    @ApiOperation("绑定设备")
    @PostMapping("/biz/invinfo/invBind")
    public CommonResult<String>  invBind(@RequestBody SkyInvBindPsParam reqParam) {
        skyInvInfoService.invBind(reqParam);
        return CommonResult.ok();
    }

    @ApiOperationSupport(order = 23)
    @ApiOperation("解绑移机")
    @PostMapping("/biz/invinfo/unbindAndChange")
    public CommonResult<String>  unbindAndChange(@RequestBody SkyInvBindPsParam reqParam) {
        skyInvInfoService.invBind(reqParam);
        return CommonResult.ok();
    }

    public Pair<Boolean, String> checkFile(MultipartFile multiFile) {
        if (multiFile.isEmpty()) {
            return Pair.of(false, "文件为空");
        }
        //获取
        String filename = multiFile.getOriginalFilename();
        String contentType = multiFile.getContentType();
        if (StringUtils.isBlank(filename)) {
            return Pair.of(false, "文件名为空");
        }
        long size = multiFile.getSize();//字节
        log.info("收到的请求文件信息：原生文件名：{},文件类型：{},文件大小：{}", filename, contentType, size);
        //获取文件后缀
        String suffix = filename.substring(filename.lastIndexOf("."));
        //判断配置的文件列表里是否支持该文件类型
        if (!ArrayUtils.contains(appProperties.getFileTypeArray(), suffix)) {
            return Pair.of(false, "不支持该类型文件上传");
        }
        double fileSize = size / 1024.0;//单位kb
        if (fileSize > appProperties.getMaxFileSize()) {
            return Pair.of(false, "文件大小超过限制");
        }
        return Pair.of(true, "验证通过");
    }


//    /**
//     * @description: 故障信息分页
//     * @author: wangjian
//     * @date: 2023/10/12 13:48
//     **/
//    @ApiOperationSupport(order = 7)
//    @ApiOperation("故障信息分页")
////    @SaCheckPermission("/biz/invinfo/alarmPage")
//    @PostMapping("/biz/invinfo/alarmPage")
//    public CommonResult<Page<SkyAlarmInfoPageVO>> alarmPage(@RequestBody @Valid SkyAlarmInfoPageParam skyAlarmInfoPageParam) {
//        return CommonResult.data(skyInvInfoService.alarmPage(skyAlarmInfoPageParam));
//    }

    /*
    private BufferedOutputStream bufferedOutputStream = null;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public String readFile(HttpServletRequest request, @RequestParam("name") String name, @RequestPart("file1") MultipartFile file3, @RequestPart("photo") MultipartFile photo) throws IOException, ServletException {

        String path= "I:\\spring\\spring-mybatis-plus\\src\\main\\resources\\public\\static\\";

        System.out.println(name);
        *//*
        第一种 ： 使用 MultipartFile 封装好的 transferTo() 方法保存文件
        photo.transferTo(new File(path+photo.getOriginalFilename()));

        第二种 ：  使用 MultipartFile 字节流保存文件
         fileUtil(file3, String.valueOf(path));

		第三种 ：  使用 Part 接收文件字节流
        Part file2 = request.getPart("file2");
        file2.write(path + file2.getSubmittedFileName());
        *//*
        // request.getParts() 获取的是全部参数（name,age,file1,file2），包括文件参数和非文件参数
        for (Part part : request.getParts()) {
            // 获取文件类型
            part.getContentType();
            // 获取文件大小
            part.getSize();
            // 获取文件名
            part.getSubmittedFileName();
            // 获取参数名 （name,age,file1,file2）
            part.getName();
            if(part.getContentType()!=null){
                part.write(path + part.getSubmittedFileName());
            }else{
                // 可以获取文本参数值,文本参数 part.getContentType() 为 null
                request.getParameter(part.getName())
            }
        }
        return "success";
    }

    public String fileUtil(MultipartFile file, String path) {

        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(
                        new File(path + file.getOriginalFilename())));
                bufferedOutputStream.write(bytes);
                bufferedOutputStream.close();
                return file.getOriginalFilename() + "success upload";
            } catch (Exception e) {
                return file.getOriginalFilename() + "failed to upload ---> " + e;
            }
        } else {
            return file.getOriginalFilename() + "You failed to upload file was empty.";
        }
    }*/
}
