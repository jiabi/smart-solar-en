package vip.xiaonuo.biz.modular.dataview.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/7/9 14:32
 */
@Getter
@Setter
public class DataViewMonthPowerReq {
    /**
     * 月 yyyy-MM
     * 年 yyyy
     */
    private String date;

    /**
     * 省
     */
    private String city;
}
