package vip.xiaonuo.biz.modular.homepage.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/22 17:45
 */

@Getter
@Setter
public class SkyLoginUserIdParam {
    /** 安装公司id */
    @ApiModelProperty(value = "安装公司id")
    private String stationCompany;

    /** 业主id */
    @ApiModelProperty(value = "业主id")
    private String stationContactId;

    /** 分销商 */
    @ApiModelProperty(value = "分销商")
    private String distributor;
}
