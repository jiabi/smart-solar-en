package vip.xiaonuo.biz.modular.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.user.entity.BizSysRelation;
import vip.xiaonuo.biz.modular.user.entity.BizUser;

public interface BizSysRelationService extends IService<BizSysRelation> {
}
