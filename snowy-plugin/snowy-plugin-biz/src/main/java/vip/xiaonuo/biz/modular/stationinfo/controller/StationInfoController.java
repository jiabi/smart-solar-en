/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationinfo.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vip.xiaonuo.auth.core.pojo.SaBaseLoginUser;
import vip.xiaonuo.auth.core.util.StpLoginUserUtil;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.enums.SkyRoleCodeEnum;
import vip.xiaonuo.biz.core.enums.StationInfoExportEnum;
import vip.xiaonuo.biz.core.enums.StationInfoTypeExportEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.ColumnTitleMap;
import vip.xiaonuo.biz.core.util.ExportExcelUtil;
import vip.xiaonuo.biz.core.util.easyexcel.EasyExcelMaker;
import vip.xiaonuo.biz.core.util.easyexcel.ExportUtil;
import vip.xiaonuo.biz.modular.eququality.param.SkyEquQualityExcelVO;
import vip.xiaonuo.biz.modular.invmonitorcurrent.param.SkyInvInfoRankVO;
import vip.xiaonuo.biz.modular.stationinfo.param.*;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import cn.hutool.json.JSONObject;
import org.slf4j.Logger;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.dev.api.DevDictApi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 电站信息控制器
 *
 * @author 全佳璧
 * @date  2023/09/12 10:51
 */
@Api(tags = "电站信息控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class StationInfoController {

    private static Logger logger = LoggerFactory.getLogger(StationInfoController.class);

    @Resource
    private StationInfoService stationInfoService;

    @Resource
    private DevDictApi devDictApi;

//    /**
//     * 获取电站信息分页
//     *
//     * @author 全佳璧
//     * @date  2023/09/12 10:51
//     */
//    @ApiOperationSupport(order = 1)
//    @ApiOperation("获取电站信息分页")
//    @SaCheckPermission("/biz/stationinfo/page")
//    @GetMapping("/biz/stationinfo/pageInfo")
//    public CommonResult<Page<StationInfo>> pageInfo(StationInfoPageParam stationInfoPageParam) {
//        return CommonResult.data(stationInfoService.pageInfo(stationInfoPageParam));
//    }

    /**
     * 获取电站信息分页
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取电站信息分页")
    @SaCheckPermission("/biz/stationinfo/page")
    @GetMapping("/biz/stationinfo/page")
    public CommonResult<Page<StationInfoPageModel>> page(StationInfoPageReqDTO stationInfoPageReqDTO) {
        StationInfoPageParam param = new StationInfoPageParam();
        BeanUtil.copyProperties(stationInfoPageReqDTO,param);
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            param.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            param.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            param.setStationContactId(loginUser.getId());
        }
        return CommonResult.data(stationInfoService.page(param));
    }

    /**
     * 添加电站信息
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加电站信息")
    @CommonLog("添加电站信息")
    @SaCheckPermission("/biz/stationinfo/add")
    @PostMapping("/biz/stationinfo/add")
    public CommonResult<String> add(@RequestBody @Valid StationInfoAddParam stationInfoAddParam) {
        stationInfoService.add(stationInfoAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑电站信息
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑电站信息")
    @CommonLog("编辑电站信息")
    @SaCheckPermission("/biz/stationinfo/edit")
    @PostMapping("/biz/stationinfo/edit")
    public CommonResult<String> edit(@RequestBody @Valid StationInfoEditParam stationInfoEditParam) {
        stationInfoService.edit(stationInfoEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除电站信息
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除电站信息-非批量删除")
    @CommonLog("删除电站信息-非批量删除")
    @SaCheckPermission("/biz/stationinfo/delete")
    @PostMapping("/biz/stationinfo/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                           CommonValidList<StationInfoIdParam> stationInfoIdParam) {
        stationInfoService.delete(stationInfoIdParam);
        return CommonResult.ok();
    }

    /**
     * 获取电站信息详情
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取电站信息详情")
    @SaCheckPermission("/biz/stationinfo/detail")
    @GetMapping("/biz/stationinfo/detail")
    public CommonResult<StationInfoVO> detail(@Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.detail(stationInfoIdParam));
    }

    /**
     * 获取动态字段的配置
     *
     * @author 全佳璧
     * @date  2023/09/12 10:51
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("获取电站信息动态字段的配置")
    @SaCheckPermission("/biz/stationinfo/dynamicFieldConfigList")
    @GetMapping("/biz/stationinfo/dynamicFieldConfigList")
    public CommonResult<List<JSONObject>> dynamicFieldConfigList() {
        return CommonResult.data(stationInfoService.dynamicFieldConfigList());
    }

    /**
     * @description: 电站信息下载
     * @author: wangjian
     * @date: 2023/9/26 9:27
     **/
    @ApiOperationSupport(order = 7)
    @ApiOperation("电站信息下载")
//    @SaCheckPermission("/biz/stationinfo/export")
    @GetMapping("/biz/stationinfo/export")
    public CommonResult<String> export(StationInfoExportParam stationInfoExportParam, HttpServletResponse response) {
//        try {
//            ArrayList<String> titleKeyList = new ColumnTitleMap("stationInfo").getTitleKeyList();
//            Map<String, String> titleMap = new ColumnTitleMap("stationInfo").getColumnTitleMap();
//            List<Map<String, Object>> list = stationInfoService.exportStationInfoList(stationInfoExportParam);
//            ExportExcelUtil.expoerDataExcel("电站管理", "电站管理", response, titleKeyList, titleMap, list);
//        } catch (IOException e) {
//            logger.error("导出电站信息失败", e);
//        }
        /**********************************电站信息下载 begin***********************************/
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            stationInfoExportParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            stationInfoExportParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            stationInfoExportParam.setStationContactId(loginUser.getId());
        }
        List<StationInfoExportVO> list = stationInfoService.exportStationInfoList(stationInfoExportParam);
        List<StationInfoExportVO> stationInfoExportVOS = assignment(list);
        EasyExcelMaker excelMaker = EasyExcelMaker.getInstance();
        excelMaker.make(response, stationInfoExportVOS, StationInfoExportVO.class, ExportUtil.translateFileName("电站信息"));
        return CommonResult.ok();
        /**********************************电站信息下载 end***********************************/
    }

    public List<StationInfoExportVO> assignment(List<StationInfoExportVO> stationInfoExportVOS){
        stationInfoExportVOS.stream().forEach(stationInfoExportVO -> {
            if(stationInfoExportVO.getStationType()!=null){
                stationInfoExportVO.setStationTypeChine(StationInfoExportEnum.findByCode(stationInfoExportVO.getStationType(),StationInfoTypeExportEnum.STATION_TYPE.getValue()));
            }
            if(stationInfoExportVO.getStationSystem()!=null){
                stationInfoExportVO.setStationSystemChine(StationInfoExportEnum.findByCode(stationInfoExportVO.getStationSystem(),StationInfoTypeExportEnum.STATION_SYSTEM.getValue()));
            }
            if(stationInfoExportVO.getStationGridstatus()!=null){
                stationInfoExportVO.setStationGridstatusChine(StationInfoExportEnum.findByCode(stationInfoExportVO.getStationGridstatus(),StationInfoTypeExportEnum.STATION_GRIDSTATUS.getValue()));
            }
            if(stationInfoExportVO.getStationStatus()!=null){
                stationInfoExportVO.setStationStatusChine(StationInfoExportEnum.findByCode(stationInfoExportVO.getStationStatus(),StationInfoTypeExportEnum.STATION_STATUS.getValue()));
            }
            if(stationInfoExportVO.getStationAlarm()!=null){
                stationInfoExportVO.setStationAlarmChine(StationInfoExportEnum.findByCode(stationInfoExportVO.getStationAlarm(),StationInfoTypeExportEnum.STATION_ALARM.getValue()));
            }
        });
        return stationInfoExportVOS;
    }

    /**
     * @description: 获取电站离线和警报状态数量
     * @author: wangjian
     * @date: 2023/9/19 15:12
     **/
    @ApiOperationSupport(order = 8)
    @ApiOperation("获取电站离线和警报状态数量")
//    @SaCheckPermission("/biz/stationinfo/statusAndAlarmCount")
    @GetMapping("/biz/stationinfo/statusAndAlarmCount")
    public CommonResult<StationStatusVO>  statusAndAlarmCount(StationInfoPageParam stationInfoPageParam) {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            stationInfoPageParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            stationInfoPageParam.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            stationInfoPageParam.setStationContactId(loginUser.getId());
        }
        return CommonResult.data(stationInfoService.statusAndAlarmCount(stationInfoPageParam));
    }


    @ApiOperationSupport(order = 9)
    @ApiOperation("获取电站名称及id")
//    @SaCheckPermission("/biz/stationinfo/getAllStationNameAndId")
    @GetMapping("/biz/stationinfo/getAllStationNameAndId")
    public CommonResult<List<StationInfo>>  getAllStationNameAndId() {
        PsInstallerAndUserReqParam installerAndUserModel = new PsInstallerAndUserReqParam();
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            installerAndUserModel.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            installerAndUserModel.setStationCompany(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            installerAndUserModel.setStationContactId(loginUser.getId());
        }

        return CommonResult.data(stationInfoService.getAllStationNameAndId(installerAndUserModel));
    }

    @ApiOperationSupport(order = 10)
    @ApiOperation("获取电站概览信息-基础")
//    @SaCheckPermission("/biz/stationinfo/overviewInfo")
    @PostMapping("/biz/stationinfo/overviewInfo")
    public CommonResult<StationInfoOverviewVO>  overviewInfo(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.overviewInfo(stationInfoIdParam));
    }

    @ApiOperationSupport(order = 11)
    @ApiOperation("电站概览-发电实时")
//    @SaCheckPermission("/biz/stationinfo/power")
    @PostMapping("/biz/stationinfo/power")
    public CommonResult<StationInfoOverviewPowerVO>  power(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.power(stationInfoIdParam));
    }

    @ApiOperationSupport(order = 12)
    @ApiOperation("电站概览-用电实时")
//    @SaCheckPermission("/biz/stationinfo/powerUsed")
    @PostMapping("/biz/stationinfo/powerUsed")
    public CommonResult<StationInfoOverviewPowerUsedVO>  powerUsed(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.powerUsed(stationInfoIdParam));
    }

    @ApiOperationSupport(order = 13)
    @ApiOperation("电站概览-电网实时")
//    @SaCheckPermission("/biz/stationinfo/powerGrid")
    @PostMapping("/biz/stationinfo/powerGrid")
    public CommonResult<StationInfoOverviewPowerGridVO>  powerGrid(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.powerGrid(stationInfoIdParam));
    }

    @ApiOperationSupport(order = 14)
    @ApiOperation("电站概览-历史发电量图")
//    @SaCheckPermission("/biz/stationinfo/powerHis")
    @PostMapping("/biz/stationinfo/powerHis")
    public CommonResult<List<StationInfoOverviewPowerHisVO>>  powerHis(@RequestBody @Valid StationInfoOverviewPowerHisParam hisParam) {
        return CommonResult.data(stationInfoService.powerHis(hisParam));
    }

    @ApiOperationSupport(order = 15)
    @ApiOperation("电站概览-电能分析")
//    @SaCheckPermission("/biz/stationinfo/powerAnalysis")
    @PostMapping("/biz/stationinfo/powerAnalysis")
    public CommonResult<List<StationInfoOverviewPowerAnalysisVO>>  powerAnalysis(@RequestBody @Valid StationInfoOverviewPowerAnalysisParam hisParam) {
        return CommonResult.data(stationInfoService.powerAnalysis(hisParam));
    }

    /**
     * 电站概览-根据电站id获取逆变器前十排名
     */
    @ApiOperationSupport(order = 16)
    @ApiOperation("电站概览-获取逆变器前十排名")
//    @SaCheckPermission("/biz/stationinfo/invRank")
    @PostMapping("/biz/stationinfo/invRank")
    public CommonResult<List<SkyInvInfoRankVO>> invRank(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.invRank(stationInfoIdParam));
    }

    /**
     * 电站概览-获取逆变器告警概要
     */
    @ApiOperationSupport(order = 17)
    @ApiOperation("电站概览-获取逆变器告警概要")
//    @SaCheckPermission("/biz/stationinfo/invAlarmBaseInfo")
    @PostMapping("/biz/stationinfo/invAlarmBaseInfo")
    public CommonResult<StationInfoOverviewInvAlarmVO> invAlarmBaseInfo(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.invAlarmBaseInfo(stationInfoIdParam));
    }

    /**
     * 电站概览-流动图信息
     */
    @ApiOperationSupport(order = 18)
    @ApiOperation("电站概览-流动图信息")
//    @SaCheckPermission("/biz/stationinfo/flowDiagram")
    @PostMapping("/biz/stationinfo/flowDiagram")
    public CommonResult<StationInfoOverviewFlowDiagramVO> flowDiagram(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.flowDiagram(stationInfoIdParam));
    }

    /**
     * 电站概览-设备统计（逆变器）
     */
    @ApiOperationSupport(order = 19)
    @ApiOperation("电站概览-设备统计（逆变器）")
//    @SaCheckPermission("/biz/stationinfo/invRank")
    @PostMapping("/biz/stationinfo/invStatistics")
    public CommonResult<PsInvStatisticsVO> invStatistics(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.invStatistics(stationInfoIdParam));
    }

    /**
     * 用户转移
     */
    @ApiOperationSupport(order = 20)
    @ApiOperation("用户转移")
//    @SaCheckPermission("/biz/stationinfo/flowDiagram")
    @PostMapping("/biz/stationinfo/userChange")
    public CommonResult<String>  userChange(@RequestBody StationInfoUserChangeReq stationInfoUserChangeReq) {
        stationInfoService.userChange(stationInfoUserChangeReq);
        return CommonResult.ok();
    }

    /**
     * 返回本地时间
     */
    @ApiOperationSupport(order = 21)
    @ApiOperation("返回本地时间")
    @GetMapping("/biz/stationinfo/localdatetime")
    public CommonResult<Map>  localDatetime(@RequestParam Map countryMap) {
        return CommonResult.data(stationInfoService.localDatetime(countryMap));
    }

    /**
     * 已绑定设备列表
     */
    @ApiOperationSupport(order = 19)
    @ApiOperation("已绑定设备列表")
//    @SaCheckPermission("/biz/stationinfo/psDeviceList")
    @PostMapping("/biz/stationinfo/psDeviceList")
    public CommonResult<List<PsDeviceInfoVO>> psDeviceList(@RequestBody @Valid StationInfoIdParam stationInfoIdParam) {
        return CommonResult.data(stationInfoService.psDeviceList(stationInfoIdParam));
    }

    /**
     * @description: 获取电站离线和警报状态数量
     * @author: wangjian
     * @date: 2023/9/19 15:12
     **/
    @ApiOperationSupport(order = 20)
    @ApiOperation("获取电站离线和警报状态数量")
//    @SaCheckPermission("/biz/stationinfo/statusAndAlarmCountByPost")
    @PostMapping("/biz/stationinfo/statusAndAlarmCountByPost")
    public CommonResult<StationStatusVO>  statusAndAlarmCountByPost(@RequestBody StationInfoPageParam stationInfoPageParam) {
        // 登录信息
        SaBaseLoginUser loginUser = StpLoginUserUtil.getLoginUser();
        List<String> roleCodeList = loginUser.getRoleCodeList();
        if (CollectionUtils.isEmpty(roleCodeList)) {
            throw new SkyCommonException(SkyCommonExceptionEnum.NOT_EXIST_ROLE_9004);
        }
        if (roleCodeList.contains(SkyRoleCodeEnum.DISTRIBUTOR.getValue())) {
            stationInfoPageParam.setDistributor(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZADMIN.getValue())) {
            stationInfoPageParam.setStationCompanyId(loginUser.getId());
        } else if (roleCodeList.contains(SkyRoleCodeEnum.BIZUSER.getValue())) {
            stationInfoPageParam.setStationContactId(loginUser.getId());
        }
        return CommonResult.data(stationInfoService.statusAndAlarmCount(stationInfoPageParam));
    }
}
