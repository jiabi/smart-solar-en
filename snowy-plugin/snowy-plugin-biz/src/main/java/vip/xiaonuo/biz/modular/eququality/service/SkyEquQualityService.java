/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.eququality.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.eququality.entity.SkyEquQuality;
import vip.xiaonuo.biz.modular.eququality.param.*;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * 设备质保信息Service接口
 *
 * @author 全佳璧
 * @date  2023/09/25 14:20
 **/
public interface SkyEquQualityService extends IService<SkyEquQuality> {

    /**
     * 获取设备质保信息分页
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     */
    Page<SkyEquQualityPageVO> page(SkyEquQualityPageParam skyEquQualityPageParam);

    /**
     * 添加设备质保信息
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     */
    void add(SkyEquQualityAddParam skyEquQualityAddParam);

    /**
     * 编辑设备质保信息
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     */
    void edit(SkyEquQualityEditParam skyEquQualityEditParam);

    /**
     * 删除设备质保信息
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     */
    void delete(List<SkyEquQualityIdParam> skyEquQualityIdParamList);

    /**
     * 获取设备质保信息详情
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     */
    SkyEquQuality detail(SkyEquQualityIdParam skyEquQualityIdParam);

    /**
     * 获取设备质保信息详情
     *
     * @author 全佳璧
     * @date  2023/09/25 14:20
     **/
    SkyEquQuality queryEntity(String id);

    /**
     * @description: 设备质保信息下载
     * @author: wangjian
     * @date: 2023/10/5 11:54
     **/
    List<Map<String, Object>> exportInfoList(SkyEquQualityExportParam exportParam);

    List<SkyEquQualityExcelVO> exportInfoList2(SkyEquQualityExportParam exportParam);

    /**
     * @description: 获取设备SN集合
     * @author: wangjian
     * @date: 2023/10/5 11:54
     **/
    List<SkySnAndParamerParam> getEquipSnList(SkyPsIdParam skyPsIdParam);

    /**
     * @description: 检查设备SN是否存在
     * @author: wangjian
     * @date: 2023/10/5 11:55
     **/
    Boolean checkEquipSn(EquipSnParam equipSnParam);

    /**
     * @description: 下载质保信息导入模板
     * @author: wangjian
     * @date: 2023/10/5 11:55
     **/
    List<Map<String, Object>> getExcelModel() throws ParseException;

    List<ImportErrorReturnVO> importByExcel(List<SkyEquQualityImportModel> models);
}
