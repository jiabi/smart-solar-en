package vip.xiaonuo.biz.modular.collectormonitorcurrent.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 采集器实时数据
 * @TableName sky_collector_monitor_current
 */
@TableName(value ="sky_collector_monitor_current")
@Data
public class SkyCollectorMonitorCurrent implements Serializable {
    /**
     * 
     */
    @TableId(value = "ID")
    private String id;

    /**
     * 采集器SN
     */
    @TableField(value = "COLLECTOR_SN")
    private String collectorSn;

    /**
     * 固件版本
     */
    @TableField(value = "FIRMWARE_VER")
    private String firmwareVer;

    /**
     * 设备状态
     */
    @TableField(value = "COLLECTOR_STATUS")
    private Integer collectorStatus;

    /**
     * 信号强度
     */
    @TableField(value = "COLLECTOR_SI")
    private Integer collectorSi;

    /**
     * 累计运行时间
     */
    @TableField(value = "RUN_TOTALTIME")
    private Integer runTotaltime;

    /**
     * gprs卡号
     */
    @TableField(value = "GPRS_CARD")
    private String gprsCard;

    /**
     * 运营商
     */
    @TableField(value = "CARD_OPERATOR")
    private String cardOperator;

    /**
     * 连接设备数量
     */
    @TableField(value = "EQU_COUNT")
    private Integer equCount;

    /**
     * 删除标志
     */
    @TableField(value = "DELETE_FLAG" ,fill = FieldFill.INSERT)
    private String deleteFlag;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME" ,fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 创建用户
     */
    @TableField(value = "CREATE_USER" ,fill = FieldFill.INSERT)
    private String createUser;

    /**
     * 修改时间
     */
    @TableField(value = "UPDATE_TIME" ,fill = FieldFill.UPDATE)
    private Date updateTime;

    /**
     * 修改用户
     */
    @TableField(value = "UPDATE_USER" ,fill = FieldFill.UPDATE)
    private String updateUser;

    /**
     * 租户id
     */
    @TableField(value = "TENANT_ID" )
    private String tenantId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}