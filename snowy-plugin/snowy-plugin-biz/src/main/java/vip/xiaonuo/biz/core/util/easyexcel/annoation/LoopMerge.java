package vip.xiaonuo.biz.core.util.easyexcel.annoation;

import java.lang.annotation.*;

/**
 * @Author wangjian
 * @Date 2024/2/29 19:25
 */

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface LoopMerge {

    /**
     * Each row
     *
     * @return
     */
    int eachRow() default 1;
}
