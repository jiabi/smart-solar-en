package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description:
 * @Author: wangjian
 * @CreateTime: 2023-11-15  14:06
 */
@Getter
@Setter
public class StationInfoOverviewPowerGridVO {
    /** 电网功率 */
    @ApiModelProperty(value = "电网功率")
    private Double gridPower;

    /** 当日并网量 */
    @ApiModelProperty(value = "当日并网量")
    private String gridEnergyToday;

    /** 当月并网量 */
    @ApiModelProperty(value = "当月并网量")
    private String gridEnergyMonth;

    /** 当年并网量 */
    @ApiModelProperty(value = "当年并网量")
    private String gridEnergyYear;

    /** 累计并网量 */
    @ApiModelProperty(value = "累计并网量")
    private String gridEnergyTotal;

    /** 当日并网量 */
    @ApiModelProperty(value = "当日购电量")
    private String gridPurchaseToday;

    /** 当月并网量 */
    @ApiModelProperty(value = "当月购电量")
    private String gridPurchaseMonth;

    /** 当年并网量 */
    @ApiModelProperty(value = "当年购电量")
    private String gridPurchaseYear;

    /** 累计并网量 */
    @ApiModelProperty(value = "累计购电量")
    private String gridPurchaseTotal;
}