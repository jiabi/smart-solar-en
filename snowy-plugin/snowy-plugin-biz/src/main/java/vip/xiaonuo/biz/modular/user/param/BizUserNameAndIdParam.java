package vip.xiaonuo.biz.modular.user.param;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/5/29 9:53
 */
@Setter
@Getter
@AllArgsConstructor
public class BizUserNameAndIdParam {
    private String id;

    private String account;
}
