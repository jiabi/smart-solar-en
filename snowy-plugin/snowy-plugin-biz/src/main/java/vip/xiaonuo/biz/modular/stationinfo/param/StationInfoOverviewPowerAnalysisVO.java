package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description:
 * @Author: wangjian
 * @CreateTime: 2023-11-16  17:49
 */
@Getter
@Setter
public class StationInfoOverviewPowerAnalysisVO {
    /** 发电量 */
    @ApiModelProperty(value = "发电量")
    private String stationPower;

    /** 时间类型 */
    @ApiModelProperty(value = "时间类型：1.日发电量；2.月发电量；3.年发电量；4.总发电量；5.今日并网电量；6.累计并网电量；7.今日购电量；8.累计购电量")
    private Integer timeType;

    /** 日期 */
    @ApiModelProperty(value = "日期")
    private Double datetime;
}