package vip.xiaonuo.biz.modular.stationinfo.param.pschart;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/3/8 17:32
 */
@Getter
@Setter
public class PsChartBaseInfoResVO {
    /**
     * 电站id
     */
    private String id;
    /**
     * 电站名称
     */
    private String stationName;

    /**
     * 系统类型(1.全额并网系统；2.自发自用系统；3.储能系统)
     */
    private Integer stationSystem;

    /**
     * 电站类型：1.分布式户用；2.分布式商业；3.分布式工业；4.地面电站
     */
    private String stationType;

    /**
     * 报警状态：0.无报警；1.有报警
     */
    private Integer stationAlarm;
    /**
     * 电站状态：0.空电站；1.电站正常；2.部分离线；3.全部离线
     */
    private Integer stationStatus;

    /**
     * 并网状态：0.未并网；1.已并网
     */
    private Integer stationGridstatus;

    /**
     * 直流功率
     */
    private Double inPower;
    /**
     * 交流功率
     */
    private Double pacPower;
    /**
     * 当日发电量（单位：kWh）
     */
    private Double dayPg;

    /**
     * 当日满发小时
     */
    private Double dayHour;
    /**
     * 当月发电量
     */
    private Double monthPg;
    /**
     * 当月满发小时
     */
    private Double monthHour;
    /**
     * 累计发电量
     */
    private Double total;
}
