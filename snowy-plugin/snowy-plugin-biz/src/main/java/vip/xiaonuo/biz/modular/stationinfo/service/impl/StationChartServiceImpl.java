package vip.xiaonuo.biz.modular.stationinfo.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.stereotype.Service;
import vip.xiaonuo.biz.core.enums.DateTypeEnum;
import vip.xiaonuo.biz.core.enums.SkyCommonExceptionEnum;
import vip.xiaonuo.biz.core.exception.SkyCommonException;
import vip.xiaonuo.biz.core.util.ChartUtil;
import vip.xiaonuo.biz.core.util.DoubleUtils;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.app.station.param.chart.AppPsDayPowerResVO;
import vip.xiaonuo.biz.modular.homepage.param.HisPowerAllPowerInfoModel;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.invinfo.param.SkyInvInfoIdParam;
import vip.xiaonuo.biz.modular.invmonitor.mapper.SkyInvMonitorMapper;
import vip.xiaonuo.biz.modular.invmonitorcurrent.entity.SkyInvMonitorCurrent;
import vip.xiaonuo.biz.modular.invmonitorcurrent.mapper.SkyInvMonitorCurrentMapper;
import vip.xiaonuo.biz.modular.invpower.entity.SkyInvPower;
import vip.xiaonuo.biz.modular.invpower.mapper.SkyInvPowerMapper;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.mapper.StationInfoMapper;
import vip.xiaonuo.biz.modular.stationinfo.param.*;
import vip.xiaonuo.biz.modular.stationinfo.param.equchart.*;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.PsChartBaseInfoResVO;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.PsChartDataResVO;
import vip.xiaonuo.biz.modular.stationinfo.param.pschart.PsChartDayDataParam;
import vip.xiaonuo.biz.modular.stationinfo.service.StationChartService;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.mapper.SkyStationPowerMapper;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/2/28 18:38
 */
@Service
public class StationChartServiceImpl extends ServiceImpl<StationInfoMapper, StationInfo> implements StationChartService {

    @Resource
    private SkyStationPowerMapper skyStationPowerMapper;

    @Resource
    private SkyInvMonitorCurrentMapper skyInvMonitorCurrentMapper;

    @Resource
    private SkyInvPowerMapper skyInvPowerMapper;

    @Resource
    private SkyInvInfoMapper skyInvInfoMapper;

    @Resource
    private SkyInvMonitorMapper skyInvMonitorMapper;

    @Resource
    private PsEquChartProperties psEquChartProperties;

    private static final int MIN_HOUR_5 = 5;
    private static final int MAX_HOUR_21 = 21;

    private static final int MIN_HOUR_0 = 0;
    private static final int MAX_HOUR_24 = 24;

    /**
     * 电站概览-历史发电量比重图
     * @param type
     * @param date
     * @param stationId
     * @return
     */
    @Override
    public PsOverviewHisPowerProportionVO hisPowerProportion(Integer type, String date, String stationId) {
        if (DateTypeEnum.DAY.getType().equals(type)) {
            //日发电比重统计
            return this.psDayProportionChart(date,stationId);
        } else if (DateTypeEnum.MONTH.getType().equals(type)) {
            //月发电比重统计
            return this.psMonthProportionChart(date,stationId);
        } else if (DateTypeEnum.YEAR.getType().equals(type)) {
            //年发电比重统计
            return this.psYearProportionChart(date,stationId);
        } else if (DateTypeEnum.ALL.getType().equals(type)) {
            //总发电比重统计
            return this.psTotalProportionChart(stationId);
        }
        return null;
    }

    /**
     * 电站日图表(日、月、年)
     *
     * @param type
     * @param date 格式（yyyy-mm-dd,yyyy-mm,yyyy）
     * @param stationIds
     * @return
     */
    @Override
    public PsChartDataResVO psChart(Integer type, String date, List<String> stationIds) {
        if (DateTypeEnum.DAY.getType().equals(type)) {
            //日统计
            return this.psDayChart(date,stationIds);
        } else if (DateTypeEnum.MONTH.getType().equals(type)) {
            //月统计
            return this.psMonthChart(date,stationIds);
        } else if (DateTypeEnum.YEAR.getType().equals(type)) {
            //年统计
            return this.psYearChart(date,stationIds);
        } else if (DateTypeEnum.ALL.getType().equals(type)) {
            //总统计
            return this.psTotalChart(stationIds);
        }
        return null;
    }

    /**
     * 设备图表(日、月、年)
     *
     * @param deviceSn
     * @param date   格式（yyyy-mm-dd,yyyy-mm,yyyy）
     * @param type
     * @param paramsList
     * @return
     */
    @Override
    public PsEquChartResVO equChart(String deviceSn, String date, Integer type, List<String> paramsList) {
        if (DateTypeEnum.DAY.getType().equals(type)) {
            //日统计
            if (paramsList.isEmpty()) {
                throw new SkyCommonException(SkyCommonExceptionEnum.CHART_PARAM_CHECK_NULL_9108);
            }
            return this.equDayChart(date,deviceSn,paramsList);
        } else if (DateTypeEnum.MONTH.getType().equals(type)) {
            //月统计
            return this.equMonthChart(date,deviceSn);
        } else if (DateTypeEnum.YEAR.getType().equals(type)) {
            //年统计
            return this.equYearChart(date,deviceSn);
        } else if (DateTypeEnum.ALL.getType().equals(type)) {
            //总统计
            return this.equTotalChart(deviceSn);
        }
        return null;
    }

    @Override
    public PsChartBaseInfoResVO psChartBaseInfo(StationInfoIdParam req) {
        PsChartBaseInfoResVO psCurrentInfo = skyStationPowerMapper.getPsCurrentInfo(req.getId());
        if(ObjectUtil.isEmpty(psCurrentInfo)){
            PsChartBaseInfoResVO psChartBaseInfoResVO = new PsChartBaseInfoResVO();
            psChartBaseInfoResVO.setDayHour(0D);
            psChartBaseInfoResVO.setInPower(0D);
            psChartBaseInfoResVO.setPacPower(0D);
            psChartBaseInfoResVO.setTotal(0D);
            return psChartBaseInfoResVO;
        }
        return psCurrentInfo;
    }

    @Override
    public PsEquChartBaseInfoResVO equChartBaseInfo(SkyInvInfoIdParam req) {
        PsEquChartBaseInfoResVO psEquChartBaseInfoResVO = skyStationPowerMapper.equChartBaseInfo(req.getInvSn());
        if(ObjectUtil.isEmpty(psEquChartBaseInfoResVO)){
            PsEquChartBaseInfoResVO psEquChartBaseInfo = new PsEquChartBaseInfoResVO();
            psEquChartBaseInfo.setInPower(0D);
            psEquChartBaseInfo.setPacPower(0D);
            psEquChartBaseInfo.setDayPg(0D);
            psEquChartBaseInfo.setDayHour(0D);
            psEquChartBaseInfo.setMonthPg(0D);
            psEquChartBaseInfo.setMonthHour(0D);
            psEquChartBaseInfo.setTotal(0D);
            return psEquChartBaseInfo;
        }
        return psEquChartBaseInfoResVO;
    }

    private PsOverviewHisPowerProportionVO psDayProportionChart(String date, String stationId) {
        PsOverviewHisPowerProportionVO res = new PsOverviewHisPowerProportionVO();
        QueryWrapper<SkyStationPower>  queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SkyStationPower::getStationId,stationId)
                .eq(SkyStationPower::getDateTime,date);
        List<SkyStationPower> powerList = skyStationPowerMapper.selectList(queryWrapper);
        Map<Integer, String> map = powerList.stream().collect(Collectors.toMap(SkyStationPower::getTimeType, SkyStationPower::getStationPower));
        // sky_station_power 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；     5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
        // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；     13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
        // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；    21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
        // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
//        res.setEnergy(Double.valueOf(map.getOrDefault(1, "0")));
//        res.setEnergyToLoad(Double.valueOf(map.getOrDefault(17, "0")));
//        res.setEnergyToCharge(Double.valueOf(map.getOrDefault(21, "0")));
//        res.setEnergyToGrid(res.getEnergy()  - res.getEnergyToCharge() - res.getEnergyToLoad());
//        res.setEnergyToGrid(Double.valueOf(map.getOrDefault(5, "0")));
        res.setUsedEnergy(DoubleUtils.round(Double.valueOf(map.getOrDefault(13, "0")), 2));
        res.setUsedFromDischarge(DoubleUtils.round(Double.valueOf(map.getOrDefault(25, "0")), 2));
        res.setUsedFromPurchase(DoubleUtils.round(Double.valueOf(map.getOrDefault(9, "0")), 2));
        res.setUsedFromEnergy(DoubleUtils.round(res.getUsedEnergy() - res.getUsedFromDischarge() - res.getUsedFromPurchase(), 2));

        res.setEnergy(DoubleUtils.round(Double.valueOf(map.getOrDefault(1, "0")), 2));
        res.setEnergyToLoad(res.getUsedFromEnergy());
        res.setEnergyToGrid(DoubleUtils.round(Double.valueOf(map.getOrDefault(5, "0")), 2));
        double toCharge = res.getEnergy() - res.getEnergyToLoad() - res.getEnergyToGrid();
        res.setEnergyToCharge(toCharge < 0 ? 0 : toCharge);
        return res;
    }

    private PsOverviewHisPowerProportionVO psMonthProportionChart(String date, String stationId) {
        PsOverviewHisPowerProportionVO res = new PsOverviewHisPowerProportionVO();
        QueryWrapper<SkyStationPower>  queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SkyStationPower::getStationId,stationId)
                .eq(SkyStationPower::getDateTime,date);
        List<SkyStationPower> powerList = skyStationPowerMapper.selectList(queryWrapper);
        Map<Integer, String> map = powerList.stream().collect(Collectors.toMap(SkyStationPower::getTimeType, SkyStationPower::getStationPower));
        // sky_station_power 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；     5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
        // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；     13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
        // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；    21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
        // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
        res.setUsedEnergy(DoubleUtils.round(Double.valueOf(map.getOrDefault(14, "0")), 2));
        res.setUsedFromDischarge(DoubleUtils.round(Double.valueOf(map.getOrDefault(26, "0")), 2));
        res.setUsedFromPurchase(DoubleUtils.round(Double.valueOf(map.getOrDefault(10, "0")), 2));
        res.setUsedFromEnergy(res.getUsedEnergy() - res.getUsedFromDischarge() - res.getUsedFromPurchase());

        res.setEnergy(DoubleUtils.round(Double.valueOf(map.getOrDefault(2, "0")), 2));
        res.setEnergyToLoad(res.getUsedFromEnergy());
        res.setEnergyToGrid(DoubleUtils.round(Double.valueOf(map.getOrDefault(6, "0")), 2));
        double toCharge = res.getEnergy() - res.getEnergyToLoad() - res.getEnergyToGrid();
        res.setEnergyToCharge(toCharge < 0 ? 0 : toCharge);
        return res;
    }

    private PsOverviewHisPowerProportionVO psYearProportionChart(String date, String stationId) {
        PsOverviewHisPowerProportionVO res = new PsOverviewHisPowerProportionVO();
        QueryWrapper<SkyStationPower>  queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SkyStationPower::getStationId,stationId)
                .eq(SkyStationPower::getDateTime,date);
        List<SkyStationPower> powerList = skyStationPowerMapper.selectList(queryWrapper);
        Map<Integer, String> map = powerList.stream().collect(Collectors.toMap(SkyStationPower::getTimeType, SkyStationPower::getStationPower));
        // sky_station_power 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；     5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
        // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；     13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
        // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；    21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
        // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
        res.setUsedEnergy(DoubleUtils.round(Double.valueOf(map.getOrDefault(15, "0")), 2));
        res.setUsedFromDischarge(DoubleUtils.round(Double.valueOf(map.getOrDefault(27, "0")), 2));
        res.setUsedFromPurchase(DoubleUtils.round(Double.valueOf(map.getOrDefault(11, "0")), 2));
        res.setUsedFromEnergy(res.getUsedEnergy() - res.getUsedFromDischarge() - res.getUsedFromPurchase());

        res.setEnergy(DoubleUtils.round(Double.valueOf(map.getOrDefault(3, "0")), 2));
        res.setEnergyToLoad(res.getUsedFromEnergy());
        res.setEnergyToGrid(DoubleUtils.round(Double.valueOf(map.getOrDefault(7, "0")), 2));
        double toCharge = res.getEnergy() - res.getEnergyToLoad() - res.getEnergyToGrid();
        res.setEnergyToCharge(toCharge < 0 ? 0 : toCharge);
        return res;
    }

    private PsOverviewHisPowerProportionVO psTotalProportionChart(String stationId) {
        PsOverviewHisPowerProportionVO res = new PsOverviewHisPowerProportionVO();
        QueryWrapper<SkyStationPower>  queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SkyStationPower::getStationId,stationId)
                .eq(SkyStationPower::getDateTime,"Total");
        List<SkyStationPower> powerList = skyStationPowerMapper.selectList(queryWrapper);
        Map<Integer, String> map = powerList.stream().collect(Collectors.toMap(SkyStationPower::getTimeType, SkyStationPower::getStationPower));
        // sky_station_power 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；     5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
        // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；     13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
        // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；    21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
        // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
        res.setUsedEnergy(DoubleUtils.round(Double.valueOf(map.getOrDefault(16, "0")), 2));
        res.setUsedFromDischarge(DoubleUtils.round(Double.valueOf(map.getOrDefault(28, "0")), 2));
        res.setUsedFromPurchase(DoubleUtils.round(Double.valueOf(map.getOrDefault(12, "0")), 2));
        res.setUsedFromEnergy(res.getUsedEnergy() - res.getUsedFromDischarge() - res.getUsedFromPurchase());

        res.setEnergy(DoubleUtils.round(Double.valueOf(map.getOrDefault(4, "0")), 2));
        res.setEnergyToLoad(res.getUsedFromEnergy());
        res.setEnergyToGrid(DoubleUtils.round(Double.valueOf(map.getOrDefault(8, "0")), 2));
        double toCharge = res.getEnergy() - res.getEnergyToLoad() - res.getEnergyToGrid();
        res.setEnergyToCharge(toCharge < 0 ? 0 : toCharge);
        return res;
    }

    /**
     * 电站日图表-每日明细
     *
     * @param stationIds
     * @param date   格式（yyyy-mm-dd）
     * @return
     */
    private PsChartDataResVO psDayChart(String date, List<String> stationIds) {
        PsChartDataResVO resVO = new PsChartDataResVO();
//        List<PsChartDayDataParam> dayDataList = skyStationPowerMapper.psDayChart(date, stationId);
        // 获取sn集合
        QueryWrapper<SkyInvInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().in(SkyInvInfo::getStationId,stationIds);
        List<SkyInvInfo> invInfos = null;
        try {
            invInfos = skyInvInfoMapper.selectList(queryWrapper);
        } catch (Exception e) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
        List<String> snList = invInfos.stream().map(SkyInvInfo::getInvSn).collect(Collectors.toList());
        //如果是snList为null说明此电站下无设备
        if (CollectionUtils.isEmpty(snList)) {
            return resVO;
        }
        // 日期对应表查询
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateLD = LocalDate.parse(date, formatter);
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyyMM");
        List<PsChartDayDataParam> dayDataList = null;
        Map<String, PsChartDayDataParam> dayTimeMaps = new HashMap<>();
        try {
//            dayDataList = skyInvMonitorMapper.dayChartBySns(dateLD.format(formatter2), snList, date);
            dayDataList = skyInvMonitorMapper.dayChartBySnsDistinct(dateLD.format(formatter2), snList, date);
            // 按照invSn进行分组，然后按照monitorTime再次分组后按照uploadTime排序后，输出每组最大值的数据集合
            List<PsChartDayDataParam> distinctList = dayDataList.stream()
                    .collect(Collectors.groupingBy(
                            PsChartDayDataParam::getInvSn,
                            Collectors.groupingBy(PsChartDayDataParam::getMonitorTime,
                                    Collectors.collectingAndThen(
                                            Collectors.maxBy(Comparator.comparing(PsChartDayDataParam::getUploadTime)),
                                            Optional::get
                                    )
                            )
                    ))
                    .values().stream()
                    .flatMap(monitorTimeMap -> monitorTimeMap.values().stream())
                    .collect(Collectors.toList());
            // 对distinctList按照monitorTime进行分组
            Map<String, List<PsChartDayDataParam>> groupedByMonitorTime = distinctList.stream()
                    .collect(Collectors.groupingBy(PsChartDayDataParam::getMonitorTime));
            // 将每组的各类型值求和
            for (Map.Entry<String, List<PsChartDayDataParam>> entry : groupedByMonitorTime.entrySet()) {
                String key = entry.getKey();
                List<PsChartDayDataParam> dataList = entry.getValue();

                PsChartDayDataParam sumData = new PsChartDayDataParam();
                for (PsChartDayDataParam data : dataList) {
                    sumData.setMonitorTime(data.getMonitorTime());
                    sumData.setMonPac(DoubleUtils.round((sumData.getMonPac() != null ? sumData.getMonPac() : 0) + (data.getMonPac() != null ? data.getMonPac() : 0),2));
                    sumData.setLoadPower(DoubleUtils.round((sumData.getLoadPower() != null ? sumData.getLoadPower() : 0) + (data.getLoadPower() != null ? data.getLoadPower() : 0),2));
                    sumData.setGridActivePower(DoubleUtils.round((sumData.getGridActivePower() != null ? sumData.getGridActivePower() : 0) + (data.getGridActivePower() != null ? data.getGridActivePower() : 0),2));
                    sumData.setBatPower(DoubleUtils.round((sumData.getBatPower() != null ? sumData.getBatPower() : 0) + (data.getBatPower() != null ? data.getBatPower() : 0),2));
                    sumData.setInPower(DoubleUtils.round((sumData.getInPower() != null ? sumData.getInPower() : 0) + (data.getInPower() != null ? data.getInPower() : 0),2));
                    sumData.setMonToday(DoubleUtils.round((sumData.getMonToday() != null ? sumData.getMonToday() : 0) + (data.getMonToday() != null ? data.getMonToday() : 0),2));
                    sumData.setInvSn(data.getInvSn());
                }
                dayTimeMaps.put(key, sumData);
            }
        } catch (Exception e) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }

        if (CollectionUtils.isEmpty(dayDataList)) {
            return resVO;
        }
//        Map<String, PsChartDayDataParam> dayTimeMaps = dayDataList.stream().collect(Collectors.toMap(PsChartDayDataParam::getMonitorTime, Function.identity()));
        //获取当日当所有时间5分钟间隔
        List<String> localDateList = this.getDateTimeList(date, MIN_HOUR_0, MAX_HOUR_24);
        //数据
        List<PsChartDayDataParam> resDataList = new ArrayList<>();
        //时间轴
        List<String> dateList = new ArrayList<>();

        //组装数据
        for (String localDate : localDateList) {
            //默认数据
            PsChartDayDataParam defaultData = new PsChartDayDataParam();
//            defaultData.setBatPower(0D);
//            defaultData.setLoadPower(0D);
//            defaultData.setMonPac(0D);
//            defaultData.setGridActivePower(0D);
//            defaultData.setInPower(0D);
//            defaultData.setMonToday(0D);
            defaultData.setMonitorTime(localDate);
            PsChartDayDataParam orDefault = dayTimeMaps.getOrDefault(localDate, defaultData);
            dateList.add(localDate);
            resDataList.add(orDefault);
        }
        resVO.setDateList(dateList);
        resVO.setDayDataList(resDataList);
        return resVO;
    }

    /**
     * 电站图表-每月明细
     *
     * @param stationIds
     * @param date   格式（yyyy-mm）
     * @return
     */
    private PsChartDataResVO psMonthChart(String date, List<String> stationIds) {
        PsChartDataResVO res = new PsChartDataResVO();
//        QueryWrapper<SkyStationPower> queryWrapper = new QueryWrapper<>();
//        queryWrapper.lambda().eq(SkyStationPower::getStationId, stationId);
//        List<HisPowerAllPowerInfoModel> monthPowerList = skyStationPowerMapper.queryMonthPower(queryWrapper);
        List<HisPowerAllPowerInfoModel> monthPowerList = skyStationPowerMapper.psMonthChart(date,stationIds);
        if (CollectionUtils.isEmpty(monthPowerList)) {
            return new PsChartDataResVO();
        }

        Map<String, List<HisPowerAllPowerInfoModel>> dateMaps = monthPowerList.stream().collect(Collectors.groupingBy(HisPowerAllPowerInfoModel::getDateTime));
        //当月多少日
        List<LocalDate> localDateList = this.getLocalDateList(LocalDateUtils.parseLocalDate(date + "-01", LocalDateUtils.DATE_FORMATTER));
        //当日日期
        List<String> dateList = new ArrayList<>();
        //当日发电量
        List<Double> powerList = new ArrayList<>();
        //当日用电量
        List<Double> energyList = new ArrayList<>();
        //当日并网电量
        List<Double> gridEnergyList = new ArrayList<>();
        //当日购电量
        List<Double> gridPurchaseList = new ArrayList<>();
        //当日充电量
        List<Double> chargingList = new ArrayList<>();
        //当日放电量
        List<Double> dischargingList = new ArrayList<>();
        for (LocalDate localDate : localDateList) {
            String dateStr = LocalDateUtils.formatLocalDate(localDate, LocalDateUtils.DATE_FORMATTER);
            List<HisPowerAllPowerInfoModel> infoModelList = dateMaps.get(dateStr);

            if (dateMaps.containsKey(dateStr) && dateMaps.get(dateStr) != null) {
                Map<Integer, String> infoModelMap = infoModelList.stream().collect(Collectors.toMap(HisPowerAllPowerInfoModel::getTimeType, HisPowerAllPowerInfoModel::getStationPower));
                // sky_station_power 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；     5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
                // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；     13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
                // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；    21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
                // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
                if (infoModelMap.containsKey(1) && infoModelMap.get(1) != null) {
                    powerList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(1)), 2));
                } else {
                    powerList.add(0D);
                }
                if (infoModelMap.containsKey(13) && infoModelMap.get(13) != null) {
                    energyList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(13)), 2));
                } else {
                    energyList.add(0D);
                }
                if (infoModelMap.containsKey(5) && infoModelMap.get(5) != null) {
                    gridEnergyList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(5)), 2));
                } else {
                    gridEnergyList.add(0D);
                }
                if (infoModelMap.containsKey(9) && infoModelMap.get(9) != null) {
                    gridPurchaseList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(9)), 2));
                } else {
                    gridPurchaseList.add(0D);
                }
                if (infoModelMap.containsKey(21) && infoModelMap.get(21) != null) {
                    chargingList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(21)), 2));
                } else {
                    chargingList.add(0D);
                }
                if (infoModelMap.containsKey(25) && infoModelMap.get(25) != null) {
                    dischargingList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(25)), 2));
                } else {
                    dischargingList.add(0D);
                }
            } else {
                powerList.add(0D);
                energyList.add(0D);
                gridEnergyList.add(0D);
                gridPurchaseList.add(0D);
                chargingList.add(0D);
                dischargingList.add(0D);
            }
            dateList.add(dateStr);
        }
        res.setPower(powerList);
        res.setEnergy(energyList);
        res.setGridEnergy(gridEnergyList);
        res.setGridPurchase(gridPurchaseList);
        res.setCharging(chargingList);
        res.setDischarging(dischargingList);
        res.setDateList(dateList);
        return res;
    }

    /**
     * 电站图表-每年明细
     *
     * @param stationIds
     * @param date   格式（yyyy）
     * @return
     */
    private PsChartDataResVO psYearChart(String date, List<String> stationIds) {
        PsChartDataResVO res = new PsChartDataResVO();
//        QueryWrapper<SkyStationPower> queryWrapper = new QueryWrapper<>();
//        queryWrapper.lambda().eq(SkyStationPower::getStationId, stationId);
//        List<HisPowerAllPowerInfoModel> monthPowerList = skyStationPowerMapper.pschart(queryWrapper);
        List<HisPowerAllPowerInfoModel> monthPowerList = skyStationPowerMapper.psYearChart(date,stationIds);
        if (CollectionUtils.isEmpty(monthPowerList)) {
            return res;
        }
        Map<String, List<HisPowerAllPowerInfoModel>> dateMaps = monthPowerList.stream().collect(Collectors.groupingBy(HisPowerAllPowerInfoModel::getDateTime));

        //当年多少月
        List<String> localDateList = this.getMonthList(date);
        //当日日期
        List<String> dateList = new ArrayList<>();
        //当日发电量
        List<Double> powerList = new ArrayList<>();
        //当日用电量
        List<Double> energyList = new ArrayList<>();
        //当日并网电量
        List<Double> gridEnergyList = new ArrayList<>();
        //当日购电量
        List<Double> gridPurchaseList = new ArrayList<>();
        //当日充电量
        List<Double> chargingList = new ArrayList<>();
        //当日放电量
        List<Double> dischargingList = new ArrayList<>();
        for (String localDate : localDateList) {
//            String dateStr = LocalDateUtils.formatLocalDate(localDate, LocalDateUtils.DATE_FORMATTER);
            List<HisPowerAllPowerInfoModel> infoModelList = dateMaps.get(localDate);

            if (dateMaps.containsKey(localDate) && dateMaps.get(localDate) != null) {
                Map<Integer, String> infoModelMap = infoModelList.stream().collect(Collectors.toMap(HisPowerAllPowerInfoModel::getTimeType, HisPowerAllPowerInfoModel::getStationPower));
                // sky_station_power 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；     5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
                // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；     13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
                // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；    21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
                // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
                if (infoModelMap.containsKey(2) && infoModelMap.get(2) != null) {
                    powerList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(2)),2));
                } else {
                    powerList.add(0D);
                }
                if (infoModelMap.containsKey(14) && infoModelMap.get(14) != null) {
                    energyList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(14)),2));
                } else {
                    energyList.add(0D);
                }
                if (infoModelMap.containsKey(6) && infoModelMap.get(6) != null) {
                    gridEnergyList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(6)),2));
                } else {
                    gridEnergyList.add(0D);
                }
                if (infoModelMap.containsKey(10) && infoModelMap.get(10) != null) {
                    gridPurchaseList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(10)),2));
                } else {
                    gridPurchaseList.add(0D);
                }
                if (infoModelMap.containsKey(22) && infoModelMap.get(22) != null) {
                    chargingList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(22)),2));
                } else {
                    chargingList.add(0D);
                }
                if (infoModelMap.containsKey(26) && infoModelMap.get(26) != null) {
                    dischargingList.add(DoubleUtils.round(Double.parseDouble(infoModelMap.get(26)),2));
                } else {
                    dischargingList.add(0D);
                }
            } else {
                powerList.add(0D);
                energyList.add(0D);
                gridEnergyList.add(0D);
                gridPurchaseList.add(0D);
                chargingList.add(0D);
                dischargingList.add(0D);
            }
            dateList.add(localDate);
        }
        res.setPower(powerList);
        res.setEnergy(energyList);
        res.setGridEnergy(gridEnergyList);
        res.setGridPurchase(gridPurchaseList);
        res.setCharging(chargingList);
        res.setDischarging(dischargingList);
        res.setDateList(dateList);
        return res;
    }

    /**
     * 电站图表-总电量明细
     *
     * @param stationIds
     * @return
     */
    private PsChartDataResVO psTotalChart(List<String> stationIds) {
        PsChartDataResVO res = new PsChartDataResVO();
        List<HisPowerAllPowerInfoModel> totalList = skyStationPowerMapper.psTotalChart(stationIds);
        if (CollectionUtils.isEmpty(totalList)) {
            return res;
        }
//        Map<String, List<HisPowerAllPowerInfoModel>> dateMaps = monthPowerList.stream().collect(Collectors.groupingBy(HisPowerAllPowerInfoModel::getDateTime));
//        List<HisPowerAllPowerInfoModel> totalList = dateMaps.get("total");

        // 年份
        List<String> dayList = new ArrayList<>();
        //发电量
        List<Double> powerList = new ArrayList<>();
        //用电量
        List<Double> energyList = new ArrayList<>();
        //并网电量
        List<Double> gridEnergyList = new ArrayList<>();
        //购电量
        List<Double> gridPurchaseList = new ArrayList<>();
        //充电量
        List<Double> chargingList = new ArrayList<>();
        //放电量
        List<Double> dischargingList = new ArrayList<>();
        Map<String, List<HisPowerAllPowerInfoModel>> groupedByTimeType = totalList.stream()
                .collect(Collectors.groupingBy(HisPowerAllPowerInfoModel::getDateTime));
        // 对分组后的 Map 按键进行排序
        Map<String, List<HisPowerAllPowerInfoModel>> sortedMap = groupedByTimeType.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        //放假数据
        fakeData(sortedMap);
        for (Map.Entry<String, List<HisPowerAllPowerInfoModel>> entry : sortedMap.entrySet()) {
            String dateTime = entry.getKey();
            List<HisPowerAllPowerInfoModel> models = entry.getValue();
            Map<Integer, String> typeAndPower = models.stream().collect(Collectors.toMap(HisPowerAllPowerInfoModel::getTimeType, HisPowerAllPowerInfoModel::getStationPower));
            // sky_inv_power 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；     5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
            // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；     13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
            // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；    21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
            // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
            if (typeAndPower.containsKey(3) && typeAndPower.get(3) != null) {
                powerList.add(DoubleUtils.round(Double.parseDouble(typeAndPower.get(3)),2));
            } else {
                powerList.add(0D);
            }
            if (typeAndPower.containsKey(7) && typeAndPower.get(7) != null) {
                gridEnergyList.add(DoubleUtils.round(Double.parseDouble(typeAndPower.get(7)),2));
            } else {
                gridEnergyList.add(0D);
            }
            if (typeAndPower.containsKey(11) && typeAndPower.get(11) != null) {
                gridPurchaseList.add(DoubleUtils.round(Double.parseDouble(typeAndPower.get(11)),2));
            } else {
                gridPurchaseList.add(0D);
            }
            if (typeAndPower.containsKey(15) && typeAndPower.get(15) != null) {
                energyList.add(DoubleUtils.round(Double.parseDouble(typeAndPower.get(15)),2));
            } else {
                energyList.add(0D);
            }
            if (typeAndPower.containsKey(23) && typeAndPower.get(23) != null) {
                chargingList.add(DoubleUtils.round(Double.parseDouble(typeAndPower.get(23)),2));
            } else {
                chargingList.add(0D);
            }
            if (typeAndPower.containsKey(27) && typeAndPower.get(27) != null) {
                dischargingList.add(DoubleUtils.round(Double.parseDouble(typeAndPower.get(27)),2));
            } else {
                dischargingList.add(0D);
            }
            dayList.add(dateTime);
        }
        res.setPower(powerList);
        res.setEnergy(energyList);
        res.setGridEnergy(gridEnergyList);
        res.setGridPurchase(gridPurchaseList);
        res.setCharging(chargingList);
        res.setDischarging(dischargingList);
        res.setDateList(dayList);
        return res;
    }

    /**
     * 年份少图形不美观，加点假数据，未来数据多了也不会受影响
     * @param sortedMap
     * @return
     */
    public void fakeData(Map<String, List<HisPowerAllPowerInfoModel>> sortedMap){
        List<HisPowerAllPowerInfoModel> models = new ArrayList<>();
        if(!CollectionUtils.isEmpty(sortedMap.keySet()) && sortedMap.keySet().size()<5){
            String min = Collections.min(sortedMap.keySet());
            String max = Collections.max(sortedMap.keySet());
            sortedMap.put(String.valueOf(Integer.parseInt(min)-1),models);
            sortedMap.put(String.valueOf(Integer.parseInt(min)-2),models);
            sortedMap.put(String.valueOf(Integer.parseInt(max)+1),models);
            sortedMap.put(String.valueOf(Integer.parseInt(max)+2),models);
        }
        // 对sortedMap按key进行排序
        Map<String, List<HisPowerAllPowerInfoModel>> sortedByKeyMap = sortedMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        sortedMap.clear();
        sortedMap.putAll(sortedByKeyMap);
    }


    /**
     * 设备日图表-每日明细
     *
     * @param deviceSn
     * @param date   格式（yyyy-mm-dd）
     * @return
     */
    private PsEquChartResVO equDayChart(String date, String deviceSn, List<String> paramsList) {
        PsEquChartResVO resVO = new PsEquChartResVO();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateLD = LocalDate.parse(date, formatter);
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyyMM");
        List<SkyInvMonitorCurrent> currents = null;
        try {
            currents = skyInvMonitorCurrentMapper.selectCurrentHistory(dateLD.format(formatter2), deviceSn , date);
        } catch (Exception e) {
            throw new SkyCommonException(SkyCommonExceptionEnum.DATA_NOT_EXIST_9001);
        }
//        if (dateLD.getMonth().compareTo(LocalDate.now().getMonth()) != 0 ) {
//            // 非当月的
//            DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyyMM");
//            currents = skyInvMonitorCurrentMapper.selectCurrentHistory(dateLD.format(formatter2), deviceSn);
//        } else {
//            // 当月的数据直接实时表
//            QueryWrapper<SkyInvMonitorCurrent> queryWrapper = new QueryWrapper<>();
//            queryWrapper.lambda().eq(SkyInvMonitorCurrent::getInvSn, deviceSn)
//                    .like(SkyInvMonitorCurrent::getMonitorTime, date);
//            currents = skyInvMonitorCurrentMapper.selectList(queryWrapper);
//        }
        if (CollectionUtils.isEmpty(currents)) {
            return resVO;
        }
        // 去重
        List<SkyInvMonitorCurrent> dayDataList = currents.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(SkyInvMonitorCurrent::getMonitorTimeFormat))), ArrayList::new));
        PsEquDayChartResParam dayChart = new PsEquDayChartResParam();
        //时间轴-当时是分秒  如果时间段取值改变，对应的convertToDayChartAnalysis()方法中ChartUtil.getPowerStationRecordValues()也需要改
        List<String> timeList = this.getDateTimeList(date, MIN_HOUR_5, MAX_HOUR_21);
        List<PsEquDayChartResParam.DayChartAnalysis> analysisList = this.convertToDayChartAnalysis(dayDataList, paramsList,
                ObjectUtil.isEmpty(currents.get(0).getInvType()) ? "0" : currents.get(0).getInvType());
        dayChart.setTimeList(timeList);
        dayChart.setList(analysisList);
        resVO.setDayChart(dayChart);
        return resVO;
    }

    /**
     * 逆变器明细转换To DayChartAnalysis
     *
     * @param paramsList
     * @return
     */
    private List<PsEquDayChartResParam.DayChartAnalysis> convertToDayChartAnalysis(List<SkyInvMonitorCurrent> dayDataList, List<String> paramsList, String invType) {
        List<String> camelCaseList = this.toCamelCase(paramsList);
        Map<String,List<Object>>  doubleMap = ChartUtil.getPowerStationRecordValues(dayDataList,camelCaseList, invType);
        List<PsEquDayChartResParam.DayChartAnalysis> analysisList = new ArrayList<>(doubleMap.size());
        for (Map.Entry<String, List<Object>> stringListEntry : doubleMap.entrySet()) {
            String key = stringListEntry.getKey();
            List<Object> value = stringListEntry.getValue();
            analysisList.add(new PsEquDayChartResParam.DayChartAnalysis(key,null,value));
        }
        return analysisList;
    }


    /**
     * 将例如STATION_ID转为驼峰命名
     * @param paramsList
     * @return
     */
    private List<String> toCamelCase(List<String> paramsList) {
        List<String> resultList = new ArrayList<>();
        for (String snakeCase:paramsList) {
            StringBuilder result = new StringBuilder();
            String[] words = snakeCase.split("_");
            for (int i = 0; i < words.length; i++) {
                String word = words[i];
                if (i == 0) {
                    result.append(word.toLowerCase());
                } else {
                    result.append(Character.toUpperCase(word.charAt(0)))
                            .append(word.substring(1).toLowerCase());
                }
            }
            resultList.add(result.toString());
        }
        return resultList;
    }

    /**
     * 逆变器设备月图表-每日明细
     *
     * @param deviceSn
     * @param date     格式（yyyy-mm）
     * @return
     */
    private PsEquChartResVO equMonthChart(String date, String deviceSn) {
        PsEquChartResVO resVO = new PsEquChartResVO();
        PsEquChartResParam resParam = new PsEquChartResParam();
        List<HisPowerAllPowerInfoModel> dataList = skyInvPowerMapper.equMonthChart(date,deviceSn);
        if (CollectionUtils.isEmpty(dataList)) {
            return resVO;
        }
        // 是否储能逆变器
        boolean ifCuNengInv = ArrayUtils.contains(psEquChartProperties.getEquNameType(), deviceSn.substring(2, 5));

        Map<String, List<HisPowerAllPowerInfoModel>> dateMaps = dataList.stream().collect(Collectors.groupingBy(HisPowerAllPowerInfoModel::getDateTime));
        //当月多少日
        List<LocalDate> localDateList = this.getLocalDateList(LocalDateUtils.parseLocalDate(date + "-01", LocalDateUtils.DATE_FORMATTER));
        //当日日期
        List<String> dateList = new ArrayList<>();
        //当日发电量
        List<Double> powerList = new ArrayList<>();
        //当日用电量
        List<Double> energyList = new ArrayList<>();
        //当日并网电量
        List<Double> gridEnergyList = new ArrayList<>();
        //当日购电量
        List<Double> gridPurchaseList = new ArrayList<>();
        //当日充电量
        List<Double> chargingList = new ArrayList<>();
        //当日放电量
        List<Double> dischargingList = new ArrayList<>();
        for (LocalDate localDate : localDateList) {
            String dateStr = LocalDateUtils.formatLocalDate(localDate, LocalDateUtils.DATE_FORMATTER);
            List<HisPowerAllPowerInfoModel> infoModelList = dateMaps.get(dateStr);

            if (dateMaps.containsKey(dateStr) && dateMaps.get(dateStr) != null) {
                Map<Integer, String> infoModelMap = infoModelList.stream().collect(Collectors.toMap(HisPowerAllPowerInfoModel::getTimeType, HisPowerAllPowerInfoModel::getStationPower));
                // sky_inv_power 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；     5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
                // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；     13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
                // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；    21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
                // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
                if (infoModelMap.containsKey(1) && infoModelMap.get(1) != null) {
                    powerList.add(Double.valueOf(infoModelMap.get(1)));
                } else {
                    powerList.add(0D);
                }
                if (ifCuNengInv) {
                    if (infoModelMap.containsKey(13) && infoModelMap.get(13) != null) {
                        energyList.add(Double.valueOf(infoModelMap.get(13)));
                    } else {
                        energyList.add(0D);
                    }
                    if (infoModelMap.containsKey(5) && infoModelMap.get(5) != null) {
                        gridEnergyList.add(Double.valueOf(infoModelMap.get(5)));
                    } else {
                        gridEnergyList.add(0D);
                    }
                    if (infoModelMap.containsKey(9) && infoModelMap.get(9) != null) {
                        gridPurchaseList.add(Double.valueOf(infoModelMap.get(9)));
                    } else {
                        gridPurchaseList.add(0D);
                    }
                    if (infoModelMap.containsKey(21) && infoModelMap.get(21) != null) {
                        chargingList.add(Double.valueOf(infoModelMap.get(21)));
                    } else {
                        chargingList.add(0D);
                    }
                    if (infoModelMap.containsKey(25) && infoModelMap.get(25) != null) {
                        dischargingList.add(Double.valueOf(infoModelMap.get(25)));
                    } else {
                        dischargingList.add(0D);
                    }
                }
            } else {
                powerList.add(0D);
                energyList.add(0D);
                gridEnergyList.add(0D);
                gridPurchaseList.add(0D);
                chargingList.add(0D);
                dischargingList.add(0D);
            }
            dateList.add(dateStr);
        }
        resParam.setPower(powerList);
        if (ifCuNengInv) {
            resParam.setEnergy(energyList);
            resParam.setGridEnergy(gridEnergyList);
            resParam.setGridPurchase(gridPurchaseList);
            resParam.setCharging(chargingList);
            resParam.setDischarging(dischargingList);
        }
        resParam.setDateList(dateList);
        resVO.setMonYearTotalChart(resParam);
        return resVO;
    }

    private PsEquChartResVO equYearChart(String date, String deviceSn) {
        PsEquChartResVO resVO = new PsEquChartResVO();
        PsEquChartResParam resParam = new PsEquChartResParam();
        List<HisPowerAllPowerInfoModel> dataList = skyInvPowerMapper.equYearChart(date,deviceSn);
        if (CollectionUtils.isEmpty(dataList)) {
            return resVO;
        }
        // 是否储能逆变器
        boolean ifCuNengInv = ArrayUtils.contains(psEquChartProperties.getEquNameType(), deviceSn.substring(2, 5));

        Map<String, List<HisPowerAllPowerInfoModel>> dateMaps = dataList.stream().collect(Collectors.groupingBy(HisPowerAllPowerInfoModel::getDateTime));

        //当年多少月
        List<String> localDateList = this.getMonthList(date);
        //当日日期
        List<String> dateList = new ArrayList<>();
        //当日发电量
        List<Double> powerList = new ArrayList<>();
        //当日用电量
        List<Double> energyList = new ArrayList<>();
        //当日并网电量
        List<Double> gridEnergyList = new ArrayList<>();
        //当日购电量
        List<Double> gridPurchaseList = new ArrayList<>();
        //当日充电量
        List<Double> chargingList = new ArrayList<>();
        //当日放电量
        List<Double> dischargingList = new ArrayList<>();
        for (String localDate : localDateList) {
//            String dateStr = LocalDateUtils.formatLocalDate(localDate, LocalDateUtils.DATE_FORMATTER);
            List<HisPowerAllPowerInfoModel> infoModelList = dateMaps.get(localDate);

            if (dateMaps.containsKey(localDate) && dateMaps.get(localDate) != null) {
                Map<Integer, String> infoModelMap = infoModelList.stream().collect(Collectors.toMap(HisPowerAllPowerInfoModel::getTimeType, HisPowerAllPowerInfoModel::getStationPower));
                // sky_inv_power 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；     5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
                // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；     13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
                // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；    21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
                // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
                if (infoModelMap.containsKey(2) && infoModelMap.get(2) != null) {
                    powerList.add(Double.valueOf(infoModelMap.get(2)));
                } else {
                    powerList.add(0D);
                }
                if (ifCuNengInv) {
                    if (infoModelMap.containsKey(14) && infoModelMap.get(14) != null) {
                        energyList.add(Double.valueOf(infoModelMap.get(14)));
                    } else {
                        energyList.add(0D);
                    }
                    if (infoModelMap.containsKey(6) && infoModelMap.get(6) != null) {
                        gridEnergyList.add(Double.valueOf(infoModelMap.get(6)));
                    } else {
                        gridEnergyList.add(0D);
                    }
                    if (infoModelMap.containsKey(10) && infoModelMap.get(10) != null) {
                        gridPurchaseList.add(Double.valueOf(infoModelMap.get(10)));
                    } else {
                        gridPurchaseList.add(0D);
                    }
                    if (infoModelMap.containsKey(22) && infoModelMap.get(22) != null) {
                        chargingList.add(Double.valueOf(infoModelMap.get(22)));
                    } else {
                        chargingList.add(0D);
                    }
                    if (infoModelMap.containsKey(26) && infoModelMap.get(26) != null) {
                        dischargingList.add(Double.valueOf(infoModelMap.get(26)));
                    } else {
                        dischargingList.add(0D);
                    }
                }
            } else {
                powerList.add(0D);
                energyList.add(0D);
                gridEnergyList.add(0D);
                gridPurchaseList.add(0D);
                chargingList.add(0D);
                dischargingList.add(0D);
            }
            dateList.add(localDate);
        }
        resParam.setPower(powerList);
        if (ifCuNengInv) {
            resParam.setEnergy(energyList);
            resParam.setGridEnergy(gridEnergyList);
            resParam.setGridPurchase(gridPurchaseList);
            resParam.setCharging(chargingList);
            resParam.setDischarging(dischargingList);
        }
        resParam.setDateList(dateList);
        resVO.setMonYearTotalChart(resParam);
        return resVO;
    }

    private PsEquChartResVO equTotalChart(String deviceSn) {
        PsEquChartResVO resVO = new PsEquChartResVO();
        PsEquChartResParam resParam = new PsEquChartResParam();
//        QueryWrapper<SkyInvPower> queryWrapper = new QueryWrapper<>();
//        queryWrapper.lambda().eq(SkyInvPower::getInvSn, deviceSn)
//                .in(SkyInvPower::getTimeType,Arrays.asList(3,7,11,15,23,27))
//                .groupBy(SkyInvPower::getTimeType);
//        List<HisPowerAllPowerInfoModel> totalList = skyInvPowerMapper.totalPower(queryWrapper);
        List<HisPowerAllPowerInfoModel> totalList = skyInvPowerMapper.equTotalChart(deviceSn);
        if (CollectionUtils.isEmpty(totalList)) {
            return resVO;
        }
        // 是否储能逆变器
        boolean ifCuNengInv = ArrayUtils.contains(psEquChartProperties.getEquNameType(), deviceSn.substring(2, 5));
        //年份
        List<String> yearList = new ArrayList<>();
        //发电量
        List<Double> powerList = new ArrayList<>();
        //用电量
        List<Double> energyList = new ArrayList<>();
        //并网电量
        List<Double> gridEnergyList = new ArrayList<>();
        //购电量
        List<Double> gridPurchaseList = new ArrayList<>();
        //充电量
        List<Double> chargingList = new ArrayList<>();
        //放电量
        List<Double> dischargingList = new ArrayList<>();

        Map<String, List<HisPowerAllPowerInfoModel>> groupedByTimeType = totalList.stream()
                .collect(Collectors.groupingBy(HisPowerAllPowerInfoModel::getDateTime));
        // 对分组后的 Map 按键进行排序
        Map<String, List<HisPowerAllPowerInfoModel>> sortedMap = groupedByTimeType.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        for (Map.Entry<String, List<HisPowerAllPowerInfoModel>> entry : sortedMap.entrySet()) {
            String dateTime = entry.getKey();
            List<HisPowerAllPowerInfoModel> models = entry.getValue();
            Map<Integer, String> typeAndPower = models.stream().collect(Collectors.toMap(HisPowerAllPowerInfoModel::getTimeType, HisPowerAllPowerInfoModel::getStationPower));
            // sky_inv_power 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；     5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；
            // 9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；     13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；
            // 17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；    21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；
            // 25.当日放电量；26.当月放电量；27.当年放电量；28.累计放电量；
            if (typeAndPower.containsKey(3) && typeAndPower.get(3) != null) {
                powerList.add(Double.valueOf(typeAndPower.get(3)));
            } else {
                powerList.add(0D);
            }
            // 非储能逆变器只需要展示发电量
            if (ifCuNengInv) {
                if (typeAndPower.containsKey(7) && typeAndPower.get(7) != null) {
                    gridEnergyList.add(Double.valueOf(typeAndPower.get(7)));
                } else {
                    gridEnergyList.add(0D);
                }
                if (typeAndPower.containsKey(11) && typeAndPower.get(11) != null) {
                    gridPurchaseList.add(Double.valueOf(typeAndPower.get(11)));
                } else {
                    gridPurchaseList.add(0D);
                }
                if (typeAndPower.containsKey(15) && typeAndPower.get(15) != null) {
                    energyList.add(Double.valueOf(typeAndPower.get(15)));
                } else {
                    energyList.add(0D);
                }
                if (typeAndPower.containsKey(23) && typeAndPower.get(23) != null) {
                    chargingList.add(Double.valueOf(typeAndPower.get(23)));
                } else {
                    chargingList.add(0D);
                }
                if (typeAndPower.containsKey(27) && typeAndPower.get(27) != null) {
                    dischargingList.add(Double.valueOf(typeAndPower.get(27)));
                } else {
                    dischargingList.add(0D);
                }
            }
            yearList.add(dateTime);
        }
        resParam.setPower(powerList);
        if (ifCuNengInv) {
            resParam.setEnergy(energyList);
            resParam.setGridEnergy(gridEnergyList);
            resParam.setGridPurchase(gridPurchaseList);
            resParam.setCharging(chargingList);
            resParam.setDischarging(dischargingList);
        }
        resParam.setDateList(yearList);
        resVO.setMonYearTotalChart(resParam);
        return resVO;
    }

    /**
     * 获取当日当所有时间5分钟间隔
     *
     * @param date
     * @return
     */
    private List<String> getDateTimeList(String date, int minHour, int maxHour) {
        List<String> timeList = new ArrayList<>(216);

        LocalDate currentDate = LocalDate.now();
        LocalDate localDate = LocalDate.parse(date);
        if(currentDate.equals(localDate)){
            LocalTime currentTime = LocalTime.now();
            for (int h = minHour; h <= currentTime.getHour(); h++) {
                for (int m = 0; m < 60; m += 5) {
                    //每隔5分钟1个点
                    if (h == currentTime.getHour() && m > currentTime.getMinute()) {
                        break;
                    }
                    timeList.add(LocalDateUtils.formatLocalDateTime(LocalDateTime.of(currentDate, LocalTime.of(h, m, 0)), LocalDateUtils.DATETIME_FORMATTER));
                }
            }
            return timeList;
        }
        for (int h = minHour; h < maxHour; h++) {
             //每隔5分钟1个点，每小时共12各点
            for (int m = 0; m < 60; m += 5) {
                timeList.add(LocalDateUtils.formatLocalDateTime(LocalDateTime.of(localDate, LocalTime.of(h, m, 0)), LocalDateUtils.DATETIME_FORMATTER));
            }
        }
        return timeList;
    }

    /**
     * 获取当月所有日
     *
     * @param date
     * @return
     */
    private List<LocalDate> getLocalDateList(LocalDate date) {
        int year = date.getYear();
        int month = date.getMonthValue();
        int daysInMonth = date.lengthOfMonth();
        List<LocalDate> dates = new ArrayList<>();
        for (int day = 1; day <= daysInMonth; day++) {
            LocalDate currentDate = LocalDate.of(year, month, day);
            dates.add(currentDate);
        }
        return dates;
    }


    /**
     * 获取当年所有月
     *
     * @param year
     * @return
     */
    private List<String> getMonthList(String year) {
        List<String> monthList = new ArrayList<>(12);
        for (int i = 1; i <= 12; i++) {
            monthList.add(String.format("%s-%02d", year, i));
        }
        return monthList;
    }
}
