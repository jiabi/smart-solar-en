package vip.xiaonuo.biz.modular.stationinfo.param.weathertimetask;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Daily {
    private String status;
    private List<Astro> astro;
    private List<Precipitation> precipitation_08h_20h;
    private List<Precipitation> precipitation_20h_32h;
    private List<Precipitation> precipitation;
    private List<Temperature> temperature;
    private List<Temperature> temperature_08h_20h;
    private List<Temperature> temperature_20h_32h;
    private List<Wind> wind;
    private List<Wind> wind_08h_20h;
    private List<Wind> wind_20h_32h;
    private List<CommonData> humidity;
    private List<CommonData> cloudrate;
    private List<CommonData> pressure;
    private List<CommonData> visibility;
    private List<CommonData> dswrf;
    private AirQuality air_quality;
    private List<Skycon> skycon;
    private List<Skycon> skycon_08h_20h;
    private List<Skycon> skycon_20h_32h;
    private List<LifeIndex> ultraviolet;
    private List<LifeIndex> carWashing;
    private List<LifeIndex> dressing;
    private List<LifeIndex> comfort;
    private List<LifeIndex> coldRisk;

    @Getter
    @Setter
    public static class Astro {
        private String date;
        private Sunrise sunrise;
        private Sunset sunset;
    }

    @Getter
    @Setter
    public static class Precipitation {
        private String date;
        private double max;
        private double min;
        private double avg;
        private int probability;
    }

    @Getter
    @Setter
    public static class Temperature {
        private String date;
        private double max;
        private double min;
        private double avg;
    }

    @Getter
    @Setter
    public static class Wind {
        private String date;
        private WindDetails max;
        private WindDetails min;
        private WindDetails avg;

        @Getter
        @Setter
        public static class WindDetails {
            private double speed;
            private double direction;
        }
    }

    @Getter
    @Setter
    public class CommonData {
        private String date;
        private double max;
        private double min;
        private double avg;
    }
    @Getter
    @Setter
    public class AirQuality {
        private List<AQIData> aqi;
        private List<PM25Data> pm25;
        @Getter
        @Setter
        public class AQIData {
            private String date;
            private aqiCommon max;
            private aqiCommon avg;
            private aqiCommon min;
            @Getter
            @Setter
            class aqiCommon {
                private int chn;
                private int usa;
            }
        }
        @Getter
        @Setter
        // Inner class for PM2.5 details
        public class PM25Data {
            private String date;
            private int max;
            private int avg;
            private int min;
        }
    }
    @Getter
    @Setter
    public class Skycon {
        private String date;
        private String value;
    }
    @Getter
    @Setter
    public class LifeIndex {
        private String date;
        private String index;
        private String desc;
    }

    @Getter
    @Setter
    public static class Sunrise {
        private String time;
    }

    @Getter
    @Setter
    public static class Sunset {
        private String time;
    }
}
