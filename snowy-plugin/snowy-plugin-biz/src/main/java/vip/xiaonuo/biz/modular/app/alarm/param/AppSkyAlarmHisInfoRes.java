/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.app.alarm.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 故障信息历史信息实体
 *
 * @author 全佳璧
 * @date  2023/10/23 10:50
 **/
@Getter
@Setter
public class AppSkyAlarmHisInfoRes {

    /** 故障名称 */
    @ApiModelProperty(value = "故障名称")
    private String name;

    /** 设备SN */
    @ApiModelProperty(value = "设备SN")
    private String sn;

    private String psGuid;

    /** 电站名称 */
    @ApiModelProperty(value = "电站名称")
    private String psName;

    /** 类型（1-故障 2-告警） */
    @ApiModelProperty(value = "类型（1-故障 2-告警）")
    private Integer type;

    /** 故障修复时间 */
    @ApiModelProperty(value = "故障修复时间")
    private String creatTime;

    /** 故障产生时间 */
    @ApiModelProperty(value = "故障产生时间")
    private String alarmTime;

    /** 故障原因 */
    @ApiModelProperty(value = "故障原因")
    private String cause;

    /** 修复建议 */
    @ApiModelProperty(value = "修复建议")
    private String advice;

}
