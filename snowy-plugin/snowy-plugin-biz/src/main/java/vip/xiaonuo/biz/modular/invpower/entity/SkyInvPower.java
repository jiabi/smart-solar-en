/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.invpower.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 设备图表实体
 *
 * @author 全佳璧
 * @date  2024/02/22 17:00
 **/
@Getter
@Setter
@TableName("sky_inv_power")
public class SkyInvPower {

    /** ID */
    @TableId
    @ApiModelProperty(value = "ID", position = 1)
    private String id;

    /** 逆变器SN */
    @ApiModelProperty(value = "逆变器SN", position = 2)
    private String invSn;

    /** 时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；21.当日放电量；22.当月放电量；23.当年放电量；24.累计放电量； */
    @ApiModelProperty(value = "时间类型：1.当日发电量；2.当月发电量；3.当年发电量；4.累计发电量；5.当日并网电量；6.当月并网电量；7.当年并网电量；8.累计并网电量；9.当日购电量；10.当月购电量；11.当年购电量；12.累计购电量；13.当日用电量；14.当月用电量；15.当年用电量；16.累计用电量；17.当日负载电量；18.当月负载电量；19.当年负载电量；20.累计负载电量；21.当日充电量；22.当月充电量；23.当年充电量；24.累计充电量；21.当日放电量；22.当月放电量；23.当年放电量；24.累计放电量；", position = 3)
    private Integer timeType;

    /** 日期（年、年/月、年/月/日） */
    @ApiModelProperty(value = "日期（年、年/月、年/月/日）", position = 4)
    private String dateTime;

    /** 运行时间 */
    @ApiModelProperty(value = "运行时间", position = 5)
    private Double runHours;

    /** 发电量 */
    @ApiModelProperty(value = "发电量", position = 6)
    private String invPower;

    /** 删除标志 */
    @ApiModelProperty(value = "删除标志", position = 7)
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String deleteFlag;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间", position = 8)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户", position = 9)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** 修改时间 */
    @ApiModelProperty(value = "修改时间", position = 10)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** 修改用户 */
    @ApiModelProperty(value = "修改用户", position = 11)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    /** 租户id */
    @ApiModelProperty(value = "租户id", position = 12)
    private String tenantId;
}
