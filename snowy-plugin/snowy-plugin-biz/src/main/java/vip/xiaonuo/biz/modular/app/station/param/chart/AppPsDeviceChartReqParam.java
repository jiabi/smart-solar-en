package vip.xiaonuo.biz.modular.app.station.param.chart;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author wangjian
 * @Date 2024/3/6 15:37
 */

@Getter
@Setter
public class AppPsDeviceChartReqParam {
    /**
     * 设备code（逆变器）
     */
    private String sn;

    /**
     * 日 yyyy-MM-dd
     * 月 yyyy-MM
     * 年 yyyy
     * 总 不用传
     */
    private String localDate;

    /**
     * 日图表参数集合
     */
    private List<String> paramsList;
}
