package vip.xiaonuo.biz.modular.filemanager.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/4/26 17:22
 */

@Getter
@Setter
public class AutoTestParam {
    private String name;

    private String value;

    public AutoTestParam() {
    }

    public AutoTestParam(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
