package vip.xiaonuo.biz.modular.eququality.param;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2023/9/28 11:46
 **/
@Getter
@Setter
public class EquipSnParam {
    /** 设备SN */
    @TableId
    @ApiModelProperty(value = "设备SN", position = 1)
    private String equSn;
}
