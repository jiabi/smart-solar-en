/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.stationequ.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.stationequ.entity.SkyStationEqu;
import vip.xiaonuo.biz.modular.stationequ.mapper.SkyStationEquMapper;
import vip.xiaonuo.biz.modular.stationequ.param.SkyStationEquAddParam;
import vip.xiaonuo.biz.modular.stationequ.param.SkyStationEquEditParam;
import vip.xiaonuo.biz.modular.stationequ.param.SkyStationEquIdParam;
import vip.xiaonuo.biz.modular.stationequ.param.SkyStationEquPageParam;
import vip.xiaonuo.biz.modular.stationequ.service.SkyStationEquService;

import javax.annotation.Resource;
import java.time.LocalTime;
import java.util.List;

/**
 * 电站设备关系Service接口实现类
 *
 * @author 全佳璧
 * @date  2024/03/22 14:00
 **/
@Service
public class SkyStationEquServiceImpl extends ServiceImpl<SkyStationEquMapper, SkyStationEqu> implements SkyStationEquService {

    @Resource
    private SkyStationEquMapper stationEquMapper;

    @Override
    public Page<SkyStationEqu> page(SkyStationEquPageParam skyStationEquPageParam) {
        QueryWrapper<SkyStationEqu> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(skyStationEquPageParam.getStationName())) {
            queryWrapper.lambda().like(SkyStationEqu::getStationName, skyStationEquPageParam.getStationName());
        }
        if(ObjectUtil.isNotEmpty(skyStationEquPageParam.getEquSn())) {
            queryWrapper.lambda().like(SkyStationEqu::getEquSn, skyStationEquPageParam.getEquSn());
        }
        if(ObjectUtil.isNotEmpty(skyStationEquPageParam.getModelId())) {
            queryWrapper.lambda().like(SkyStationEqu::getModelId, skyStationEquPageParam.getModelId());
        }
        if(ObjectUtil.isAllNotEmpty(skyStationEquPageParam.getSortField(), skyStationEquPageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(skyStationEquPageParam.getSortOrder());
            queryWrapper.orderBy(true, skyStationEquPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(skyStationEquPageParam.getSortField()));
        } else {
            queryWrapper.lambda().orderByAsc(SkyStationEqu::getId);
        }
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SkyStationEquAddParam skyStationEquAddParam) {
        SkyStationEqu skyStationEqu = BeanUtil.toBean(skyStationEquAddParam, SkyStationEqu.class);
        this.save(skyStationEqu);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SkyStationEquEditParam skyStationEquEditParam) {
        SkyStationEqu skyStationEqu = this.queryEntity(skyStationEquEditParam.getId());
        BeanUtil.copyProperties(skyStationEquEditParam, skyStationEqu);
        this.updateById(skyStationEqu);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SkyStationEquIdParam> skyStationEquIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(skyStationEquIdParamList, SkyStationEquIdParam::getId));
    }

    @Override
    public SkyStationEqu detail(SkyStationEquIdParam skyStationEquIdParam) {
        return this.queryEntity(skyStationEquIdParam.getId());
    }

    @Override
    public SkyStationEqu queryEntity(String id) {
        SkyStationEqu skyStationEqu = this.getById(id);
        if(ObjectUtil.isEmpty(skyStationEqu)) {
            throw new CommonException("电站设备关系不存在，id值为：{}", id);
        }
        return skyStationEqu;
    }

    @Override
    public List<String> queryStationIdList() {
        return stationEquMapper.selectStationIdList();
    }

    public static void main(String[] args) {
        System.out.println(IdWorker.getIdStr());
    }

}
