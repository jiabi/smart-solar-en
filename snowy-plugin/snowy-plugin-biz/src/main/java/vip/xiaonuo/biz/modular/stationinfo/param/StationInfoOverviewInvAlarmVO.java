package vip.xiaonuo.biz.modular.stationinfo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description:
 * @Author: wangjian
 * @CreateTime: 2023-11-17  13:55
 */
@Getter
@Setter
public class StationInfoOverviewInvAlarmVO {

    /** 逆变器数量 */
    @ApiModelProperty(value = "逆变器数量")
    private Integer invNum;

    /** 逆变器离线数量 */
    @ApiModelProperty(value = "逆变器离线数量")
    private Integer offlineNum;

    /** 逆变器报警数量 */
    @ApiModelProperty(value = "逆变器报警数量")
    private Integer alarmNum;
}