package vip.xiaonuo.biz.modular.eququality.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/5/13 14:52
 */
@Getter
@Setter
public class SkySnAndParamerParam {
    private String sn;
    private String param;
}
