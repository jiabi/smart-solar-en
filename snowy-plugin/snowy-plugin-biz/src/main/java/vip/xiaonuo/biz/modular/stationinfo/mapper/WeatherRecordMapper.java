package vip.xiaonuo.biz.modular.stationinfo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.xiaonuo.biz.modular.stationinfo.entity.SkyWeatherRecord;

/**
 * @Author wangjian
 * @Date 2023/9/20 17:33
 **/
public interface WeatherRecordMapper extends BaseMapper<SkyWeatherRecord> {
}
