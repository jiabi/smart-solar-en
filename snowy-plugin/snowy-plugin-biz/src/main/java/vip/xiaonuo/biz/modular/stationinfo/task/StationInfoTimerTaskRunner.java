package vip.xiaonuo.biz.modular.stationinfo.task;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import vip.xiaonuo.biz.core.enums.SkyDeleteFlagEnum;
import vip.xiaonuo.biz.core.util.DoubleUtils;
import vip.xiaonuo.biz.core.util.LocalDateUtils;
import vip.xiaonuo.biz.modular.invinfo.entity.SkyInvInfo;
import vip.xiaonuo.biz.modular.invinfo.mapper.SkyInvInfoMapper;
import vip.xiaonuo.biz.modular.stationinfo.entity.StationInfo;
import vip.xiaonuo.biz.modular.stationinfo.mapper.StationInfoMapper;
import vip.xiaonuo.biz.modular.stationinfo.param.PsIdDeviceCountParam;
import vip.xiaonuo.biz.modular.stationinfo.param.PsIdDeviceOfflineAndPowerParam;
import vip.xiaonuo.biz.modular.stationinfo.service.StationInfoService;
import vip.xiaonuo.biz.modular.stationpower.entity.SkyStationPower;
import vip.xiaonuo.biz.modular.stationpower.mapper.SkyStationPowerMapper;
import vip.xiaonuo.common.timer.CommonTimerTaskRunner;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author wangjian
 * @Date 2024/4/9 15:34
 */

@Slf4j
@Component
//@RestController
public class StationInfoTimerTaskRunner implements CommonTimerTaskRunner {

    @Resource
    private SkyStationPowerMapper skyStationPowerMapper;

    @Resource
    private StationInfoMapper stationInfoMapper;

    @Resource
    private SkyInvInfoMapper skyInvInfoMapper;

    @Resource
    private StationInfoService stationInfoService;

    @Override
//    @GetMapping("/biz/action")
    public void action() {
        LocalDateTime beginTime = LocalDateTime.now();
        log.info("定时任务-电站信息表更新-开始：{}", beginTime);
        List<StationInfo> stationInfos = stationInfoMapper.selectList(new QueryWrapper<StationInfo>().lambda().eq(StationInfo::getDeleteFlag, SkyDeleteFlagEnum.NOTDELETE.getValue()));
        if (CollectionUtils.isEmpty(stationInfos)){
            return;
        }
        List<String> stationIdList = stationInfos.stream().map(StationInfo::getId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(stationIdList)) {
            return;
        }
        // 电站信息
        Map<String, StationInfo> stationInfoMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(stationInfos)) {
            stationInfoMap = stationInfos.stream().collect(Collectors.toMap(StationInfo::getId, Function.identity()));
        }
        // 各电站离线设备数量
        String offlineTime = LocalDateTime.now().minusMinutes(15).format(LocalDateUtils.DATETIME_FORMATTER);
        List<PsIdDeviceOfflineAndPowerParam> offlineList = stationInfoMapper.getPsDeviceStatusCountBatch(stationIdList,offlineTime);
        log.info("各电站离线设备数量:{}", JSON.toJSONString(offlineList));
        Map<String, Integer> offlineMap = new HashMap<>();
        Map<String, Double> currentPowerMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(offlineList)) {
            offlineMap = offlineList.stream().collect(Collectors.toMap(PsIdDeviceOfflineAndPowerParam::getPsId, PsIdDeviceOfflineAndPowerParam::getNum));
            currentPowerMap = offlineList.stream().collect(Collectors.toMap(PsIdDeviceOfflineAndPowerParam::getPsId, PsIdDeviceOfflineAndPowerParam::getMonPpv));
        }
        // 各电站设备数量
        List<PsIdDeviceCountParam>  equCount = skyInvInfoMapper.getEquNum(stationIdList);
        log.info("各电站设备数量:{}", JSON.toJSONString(equCount));
        Map<String, Integer> equNumMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(equCount)) {
            equNumMap = equCount.stream().collect(Collectors.toMap(PsIdDeviceCountParam::getPsId, PsIdDeviceCountParam::getNum));
        }
        // 告警数量
        List<PsIdDeviceCountParam> alarmNumCount = stationInfoMapper.alarmStatusByPsIds(stationIdList);
        log.info("告警数量:{}", JSON.toJSONString(alarmNumCount));
        Map<String, Integer> alarmNumMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(equCount)) {
            alarmNumMap = alarmNumCount.stream().collect(Collectors.toMap(PsIdDeviceCountParam::getPsId, PsIdDeviceCountParam::getNum));
        }

        for (String stationId : stationIdList) {
            updateStationPower(stationId,
                    stationInfoMap,
                    offlineMap,
                    equNumMap,
                    alarmNumMap,
                    currentPowerMap);
        }
        LocalDateTime endTime = LocalDateTime.now();
        log.info("定时任务-电站信息表更新-结束：{}。用时：{}毫秒", endTime , Duration.between(beginTime, endTime).toMillis());
    }

    private void updateStationPower(String stationId,
                                    Map<String, StationInfo> stationInfoMap,
                                    Map<String, Integer> offlineMap,
                                    Map<String, Integer> equNumMap,
                                    Map<String, Integer> alarmNumMap,
                                    Map<String, Double> currentPowerMap) {
        if (!stationInfoMap.containsKey(stationId) || ObjectUtil.isEmpty(stationInfoMap.get(stationId))) {
            return;
        }
        StationInfo stationInfo = stationInfoMap.get(stationId);
        // 电站状态
        Integer offline = offlineMap.getOrDefault(stationId, 0);
        Integer count = equNumMap.getOrDefault(stationId,0);
        if(offline > 0 && count > 0 && count.compareTo(offline) == 0){
            //没有离线
            stationInfo.setStationStatus(1);
        } else if(offline >0 && count.compareTo(offline) > 0){
            //部分离线
            stationInfo.setStationStatus(2);
        } else if(offline ==0 && count != 0){
            // 全部离线
            stationInfo.setStationStatus(3);
        }else{
            //接入中
            stationInfo.setStationStatus(0);
        }

        // 报警状态
//        Integer alarmStatus = stationInfoMapper.alarmStatus(stationId);
        Integer alarmNum = alarmNumMap.getOrDefault(stationId, 0);
        if (alarmNum > 0) {
            // 有告警
            stationInfo.setStationAlarm(1);
        } else {
            stationInfo.setStationAlarm(0);
        }

        // 发电功率
        List<SkyStationPower> powerList = skyStationPowerMapper.selectList(new QueryWrapper<SkyStationPower>().lambda()
                .eq(SkyStationPower::getStationId, stationId)
                .isNotNull(SkyStationPower::getDateTime)
                .isNotNull(SkyStationPower::getTimeType));


        // 过滤掉 dateTime 或 timeType 为 null 的数据，并按照 timeType 分组并找到每组中最大的 dateTime 值的 SkyStationPower 对象
        String nowForm = LocalDate.now().format(LocalDateUtils.DATE_FORMATTER);
        // Step 1: 过滤掉 dateTime 或 timeType 为 null 的数据
        powerList = powerList.stream()
                .filter(p -> p.getDateTime() != null && p.getTimeType() != null)
                .collect(Collectors.toList());

        // Step 2: 使用 groupingBy() 方法按 timeType 分组，并且使用 maxBy() 找到每组中最大的 dateTime 值的 SkyStationPower 对象
        Map<Integer, SkyStationPower> maxDateTimeByTimeType = powerList.stream()
                .collect(Collectors.groupingBy(SkyStationPower::getTimeType,
                        Collectors.collectingAndThen(
                                Collectors.maxBy(Comparator.comparing(SkyStationPower::getDateTime)),
                                Optional::get)));

        // Step 3: 遍历每组最大值对象的 Map 更新属性
        maxDateTimeByTimeType.forEach((timeType, power) -> {
            if (power.getTimeType() == 1 && !power.getDateTime().equals(nowForm)) {
                    power.setDateTime(nowForm);
                    power.setStationPower("0");
                };
        });
        Map<Integer, String> powerMap = maxDateTimeByTimeType.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getStationPower()));
//        Map<Integer, String> powerMap = powerList.stream()
//                .filter(power -> power.getDateTime() != null && power.getTimeType() != null)
//                .collect(Collectors.groupingBy(SkyStationPower::getTimeType,
//                        Collectors.collectingAndThen(
//                                Collectors.maxBy(Comparator.comparing(SkyStationPower::getDateTime)),
//                                maxPower -> maxPower.map(SkyStationPower::getStationPower)
//                                        .orElse(null))));

        Double currentPower = 0D;
        if (currentPowerMap.containsKey(stationId)){
            currentPower = currentPowerMap.getOrDefault(stationId,0D);
        }
        stationInfo.setElecEfficiency(currentPower);

        // 发电收益
        Double dayPower = powerMap.containsKey(1) ? Double.parseDouble(powerMap.get(1)) : 0D;
        Double totalPower = powerMap.containsKey(4) ? Double.parseDouble(powerMap.get(4)) : 0D;
        stationInfo.setElecIncome(DoubleUtils.round(
                ObjectUtil.isNotNull(stationInfo.getPricePer()) ? stationInfo.getPricePer() * dayPower : 0D,
                2));

        // 补贴收益
        stationInfo.setSubsidyIncome(DoubleUtils.round(
                ObjectUtil.isNotNull(stationInfo.getSubsidyPer()) ? stationInfo.getSubsidyPer() * dayPower : 0D,
                2));

        // 累计收益
        stationInfo.setTotalIncome(DoubleUtils.round(
                ObjectUtil.isNotNull(stationInfo.getPricePer()) ? stationInfo.getPricePer() * totalPower : 0D,
                2));

        // 当日发满小时数
        stationInfo.setDayPowerhours(String.valueOf(DoubleUtils.round(
                ObjectUtil.isNotNull(stationInfo.getStationSize()) && 0.0 != stationInfo.getStationSize() ?  dayPower/stationInfo.getStationSize()  : 0D,
                2)));

        // 累计发电量
        stationInfo.setStationTotalpower(totalPower);

        // 累计运行天数
        if (ObjectUtil.isNotNull(stationInfo.getStationGridtime())) {
            LocalDateTime gridLocalTime = LocalDateTime.ofInstant(stationInfo.getStationGridtime().toInstant(), ZoneId.systemDefault());
            stationInfo.setStationRuntime(Math.toIntExact(Duration.between(gridLocalTime, LocalDateTime.now()).toDays()));
        }

        // 设备总数
        stationInfo.setEquipTotal(Math.toIntExact(count));

        // 节约标准煤（吨）=0.000305*累计发电量（kWh），数据来自国家发改委
        //CO₂减排量（吨）=0.000793*累计发电量（kWh），数据来自国家生态环境部
        //等效植树量（棵）=累计发电量（kWh）*0.997/18.3，即1年内需要种多少棵树才能吸收掉CO₂减排量中的所有CO₂，数据来自IPCC
        //累计收入=度电收益*累计发电量（kWh）
        //SO₂减排量（吨）=0.000003*累计发电量（kWh）
        stationInfo.setSaveCoal(0.000305 * totalPower);
        stationInfo.setReduceCo2(0.000793 * totalPower);
        stationInfo.setReduceSo2(0.000003 * totalPower);
        stationInfo.setEquivalentPlanting(totalPower * 0.997 / 18.3);

        // 系统功率比
        stationInfo.setStationSysRate(String.valueOf(DoubleUtils.round(
                ObjectUtil.isNotNull(stationInfo.getStationSize()) && 0.0 != stationInfo.getStationSize() ? currentPower / stationInfo.getStationSize() * 100 : 0D,
                2)));
        //查看是否并网
        if(stationInfo.getStationGridstatus()!=null && stationInfo.getStationGridstatus()!=1){
            //更新并网时间
            List<SkyInvInfo> skyInvInfos = skyInvInfoMapper.selectList(Wrappers.lambdaQuery(SkyInvInfo.class).eq(SkyInvInfo::getStationId, stationId));
            if(!CollectionUtils.isEmpty(skyInvInfos)){
                List<String> invSns = skyInvInfos.stream().map(SkyInvInfo::getInvSn).collect(Collectors.toList());
                stationInfoService.stationGridDate(invSns,stationInfo.getTimeZone(),stationInfo);
            }
        }
        stationInfoMapper.updateById(stationInfo);
    }
}
