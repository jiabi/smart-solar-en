/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.chartparam.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.chartparam.param.*;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.biz.modular.chartparam.entity.SkyChartParam;
import vip.xiaonuo.biz.modular.chartparam.service.SkyChartParamService;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 图表参数字典表控制器
 *
 * @author 全佳璧
 * @date  2024/02/20 18:55
 */
@Api(tags = "图表参数字典表控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class SkyChartParamController {

    @Resource
    private SkyChartParamService skyChartParamService;

    /**
     * 获取图表参数字典表分页
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取图表参数字典表分页")
    @SaCheckPermission("/biz/chartparam/page")
    @GetMapping("/biz/chartparam/page")
    public CommonResult<Page<SkyChartParam>> page(SkyChartParamPageParam skyChartParamPageParam) {
        return CommonResult.data(skyChartParamService.page(skyChartParamPageParam));
    }

    /**
     * 添加图表参数字典表
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加图表参数字典表")
    @CommonLog("添加图表参数字典表")
    @SaCheckPermission("/biz/chartparam/add")
    @PostMapping("/biz/chartparam/add")
    public CommonResult<String> add(@RequestBody @Valid SkyChartParamAddParam skyChartParamAddParam) {
        skyChartParamService.add(skyChartParamAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑图表参数字典表
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑图表参数字典表")
    @CommonLog("编辑图表参数字典表")
    @SaCheckPermission("/biz/chartparam/edit")
    @PostMapping("/biz/chartparam/edit")
    public CommonResult<String> edit(@RequestBody @Valid SkyChartParamEditParam skyChartParamEditParam) {
        skyChartParamService.edit(skyChartParamEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除图表参数字典表
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除图表参数字典表")
    @CommonLog("删除图表参数字典表")
    @SaCheckPermission("/biz/chartparam/delete")
    @PostMapping("/biz/chartparam/delete")
    public CommonResult<String> delete(@RequestBody @Valid SkyChartParamIdParam skyChartParamIdParam) {
        skyChartParamService.delete(skyChartParamIdParam);
        return CommonResult.ok();
    }
//    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
//                                                   CommonValidList<SkyChartParamIdParam> skyChartParamIdParamList) {
//        skyChartParamService.delete(skyChartParamIdParamList);
//        return CommonResult.ok();
//    }

    /**
     * 获取图表参数字典表详情
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取图表参数字典表详情")
    @SaCheckPermission("/biz/chartparam/detail")
    @GetMapping("/biz/chartparam/detail")
    public CommonResult<SkyChartParam> detail(@Valid SkyChartParamIdParam skyChartParamIdParam) {
        return CommonResult.data(skyChartParamService.detail(skyChartParamIdParam));
    }

    /**
     * 获取所有表名称
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation("获取所有表名称")
//    @SaCheckPermission("/biz/chartparam/getAllTables")
    @GetMapping("/biz/chartparam/getAllTables")
    public CommonResult<List<ChartParamTablesResVO>> getAllTables() {
        return CommonResult.data(skyChartParamService.getAllTables());
    }

    /**
     * 获取指定表的字段名称
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    @ApiOperationSupport(order = 7)
    @ApiOperation("获取指定表的字段名称")
//    @SaCheckPermission("/biz/chartparam/getTablesNameAndComment")
    @PostMapping("/biz/chartparam/getTablesNameAndComment")
    public CommonResult<List<ChartParamTableNameAndCommentResVO>> getTablesNameAndComment(@RequestBody @Valid ChartParamTableNameReq req) {
        return CommonResult.data(skyChartParamService.getTablesNameAndComment(req));
    }

    /**
     * 获取指定表的字段名称
     *
     * @author 全佳璧
     * @date  2024/02/20 18:55
     */
    @ApiOperationSupport(order = 7)
    @ApiOperation("获取指定表的字段名称")
//    @SaCheckPermission("/biz/chartparam/list")
    @PostMapping("/biz/chartparam/list")
    public CommonResult<List<SkyChartParam>> getList(@RequestBody SkyChartParamListParam skyChartParamListParam) {
        return CommonResult.data(skyChartParamService.getList(skyChartParamListParam));
    }
}
