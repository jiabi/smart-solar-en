package vip.xiaonuo.biz.modular.homepage.param;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/2/26 13:24
 */

@Getter
@Setter
public class StationDistributeVO {

    /** 电站ID */
    private String stationId;

    /**
     * 电站名称
     */
    private String stationName;

    /**
     * 经度
     */
    private String areaLng;

    /**
     * 纬度
     */
    private String areaLat;

    /**
     * 报警状态：0.无报警；1.有报警
     */
    private Integer stationAlarm;

    /**
     * 电站状态：0.空电站；1.电站正常；2.部分离线；3.全部离线
     */
    private Integer stationStatus;

}
