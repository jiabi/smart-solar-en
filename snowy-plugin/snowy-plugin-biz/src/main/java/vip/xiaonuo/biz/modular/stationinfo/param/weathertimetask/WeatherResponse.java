package vip.xiaonuo.biz.modular.stationinfo.param.weathertimetask;

/**
 * @Author wangjian
 * @Date 2024/5/14 17:53
 */
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class WeatherResponse {
    private String status;
    private String api_version;
    private String api_status;
    private String lang;
    private String unit;
    private int tzShift;
    private String timezone;
    private long server_time;
    private double[] location;
    private Result result;


    // Inner classes for nested JSON objects
    @Getter
    @Setter
    public static class Result {
//        private Alert alert;
//        private Realtime realtime;
//        private Minutely minutely;
//        private Hourly hourly;
        private Daily daily;

        private Integer primary;

    }
    @Getter
    @Setter
    public static class Alert {
        private String status;
        private List<WeatherContent> content;
        private List<Adcode> adcodes;
    }

    @Getter
    @Setter
    class WeatherContent {
        private String province;
        private String status;
        private String code;
        private String description;
        private String regionId;
        private String county;
        private long pubtimestamp;
        private List<Double> latlon;
        private String city;
        private String alertId;
        private String title;
        private String adcode;
        private String source;
        private String location;
        private String request_status;

        // Getters and setters
    }

    @Getter
    @Setter
    class Adcode {
        private int adcode;
        private String name;

        // Getters and setters
    }


    @Getter
    @Setter
    class Realtime {
        private String status;
        private double temperature;
        private double humidity;
        private double cloudRate;
        private String skycon;
        private double visibility;
        private double dswrf;
        private WindData wind;
        private double pressure;
        private double apparent_temperature;
        private PrecipitationData precipitation;
        private AirQualityData air_quality;
        private LifeIndexData life_index;
    }
    class WindData {
        private double speed;
        private double direction;

        // Getters and setters
    }

    class PrecipitationData {
        private LocalPrecipitationData local;
        private NearestPrecipitationData nearest;

        // Getters and setters
    }

    class LocalPrecipitationData {
        private String status;
        private String dataSource;
        private double intensity;

        // Getters and setters
    }

    class NearestPrecipitationData {
        private String status;
        private double distance;
        private double intensity;

        // Getters and setters
    }

    class AirQualityData {
        private int pm25;
        private int pm10;
        private int o3;
        private int so2;
        private int no2;
        private double co;
        private AQIData aqi;
        private DescriptionData description;

        // Getters and setters
    }

    class AQIData {
        private int chn;
        private int usa;

        // Getters and setters
    }

    class DescriptionData {
        private String chn;
        private String usa;

        // Getters and setters
    }

    class LifeIndexData {
        private UltravioletData ultraviolet;
        private ComfortData comfort;

        // Getters and setters
    }

    class UltravioletData {
        private double index;
        private String desc;

        // Getters and setters
    }

    public static class ComfortData {
        private int index;
        private String desc;

        // Getters and setters
    }

    public static class Minutely {
        private String status;
        private String dataSource;
        private List<Double> precipitation_2h;
        private List<Double> precipitation;
        private List<Double> probability;
        private String description;
    }

    public static class Hourly {
        private String status;
        private String description;
        private List<Precipitation> precipitation;
        private List<ApparentTemperature> apparent_temperature;
        private List<Wind> wind;
        private List<Humidity> humidity;
        private List<CloudRate> cloudrate;
        private List<SkyCon> skyCon;
        private List<Pressure> pressure;
        private List<Visibility> visibility;
        private List<Dswrf> dswrf;
        private AirQuality air_quality;

        // Constructors

        // Inner classes for nested objects

        public static class Precipitation {
            private String datetime;
            private double value;
            private int probability;
        }

        public static class ApparentTemperature {
            private String datetime;
            private double value;
        }

        public static class Wind {
            private String datetime;
            private double speed;
            private double direction;
        }

        public static class Humidity {
            private String datetime;
            private double value;
        }

        public static class CloudRate {
            private String datetime;
            private double value;
        }

        public static class SkyCon {
            private String datetime;
            private String value;
        }

        public static class Pressure {
            private String datetime;
            private double value;
        }

        public static class Visibility {
            private String datetime;
            private double value;
        }

        public static class Dswrf {
            private String datetime;
            private double value;
        }

        public static class AirQuality {
            private List<AQI> aqi;
            private List<PM25> pm25;

            public static class AQI {
                private String datetime;
                private AQIValue value;

                public static class AQIValue {
                    private int chn;
                    private int usa;
                }
            }

            public static class PM25 {
                private String datetime;
                private int value;
            }
        }
    }
    @Getter
    @Setter
    public static class Daily {
        private String status;
        private List<Astro> astro;
        private List<Precipitation> precipitation_08h_20h;
        private List<Precipitation> precipitation_20h_32h;
        private List<Precipitation> precipitation;
        private List<Temperature> temperature;
        private List<Temperature> temperature_08h_20h;
        private List<Temperature> temperature_20h_32h;
        private List<Wind> wind;
        private List<Wind> wind_08h_20h;
        private List<Wind> wind_20h_32h;
        private List<CommonData> humidity;
        private List<CommonData> cloudrate;
        private List<CommonData> pressure;
        private List<CommonData> visibility;
        private List<CommonData> dswrf;
        private AirQuality air_quality;
        private List<Skycon> skycon;
        private List<Skycon> skycon_08h_20h;
        private List<Skycon> skycon_20h_32h;
        private List<LifeIndex> ultraviolet;
        private List<LifeIndex> carWashing;
        private List<LifeIndex> dressing;
        private List<LifeIndex> comfort;
        private List<LifeIndex> coldRisk;

        @Getter
        @Setter
        public static class Astro {
            private String date;
            private Sunrise sunrise;
            private Sunset sunset;
        }

        @Getter
        @Setter
        public static class Precipitation {
            private String date;
            private double max;
            private double min;
            private double avg;
            private int probability;
        }

        @Getter
        @Setter
        public static class  Temperature {
            private String date;
            private double max;
            private double min;
            private double avg;
        }

        @Getter
        @Setter
        public static class Wind {
            private String date;
            private WindDetails max;
            private WindDetails min;
            private WindDetails avg;

            @Getter
            @Setter
            public static class WindDetails {
                private double speed;
                private double direction;
            }
        }

        @Getter
        @Setter
        public class CommonData {
            private String date;
            private double max;
            private double min;
            private double avg;
        }

        public class AirQuality {
            private List<AQIData> aqi;
            private List<PM25Data> pm25;

            public  class AQIData {
                private String date;
                private aqiCommon max;
                private aqiCommon avg;
                private aqiCommon min;

                class aqiCommon{
                    private int chn;
                    private int usa;
                }
            }

            // Inner class for PM2.5 details
            public  class PM25Data {
                private String date;
                private int max;
                private int avg;
                private int min;
            }
        }

        public class Skycon {
            private String date;
            private String value;
        }

        public class LifeIndex{
            private String date;
            private String index;
            private String desc;
        }
    }



    @Getter
    @Setter
    public static class Sunrise {
        private String time;
    }

    @Getter
    @Setter
    public static class Sunset {
        private String time;
    }


}