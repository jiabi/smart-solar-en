package vip.xiaonuo.biz.modular.homepage.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author wangjian
 * @Date 2024/2/26 16:53
 */
@Getter
@Setter
public class StationPowerCountVO {
    private String power;

    private Integer timeType;
}
