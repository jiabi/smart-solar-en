import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/invinfo/` + url, ...arg)

/**
 * 逆变器信息Api接口管理器
 *
 * @author 全佳璧
 * @date  2023/10/05 16:24
 **/
export default {
	// 获取逆变器信息分页
	skyInvInfoPage(data) {
		return request('page', data, 'get')
	},
	// 提交逆变器信息表单 edit为true时为编辑，默认为新增
	skyInvInfoSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除逆变器信息
	skyInvInfoDelete(data) {
		return request('delete', data)
	},
	// 获取逆变器信息详情
	skyInvInfoDetail(data) {
		return request('detail', data, 'get')
	},
    // 获取逆变器信息动态字段的配置
	skyInvInfoDynamicFieldConfigList(data) {
		return request('dynamicFieldConfigList', data, 'get')
	},
	// 警报分页
	alarmPageList(data) {
		return request('alarmPage', data)
	},
	// 设备未绑定分页
	invNotBindList(data) {
		return request('invNotBind', data)
	},
	// 设备绑定、解绑、移机
	skyInvInfoInvBind(data) {
		return request('invBind', data)
	}
}
