import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/stationinfo/` + url, ...arg)

/**
 * 电站信息Api接口管理器
 *
 * @author 全佳璧
 * @date  2023/09/12 12:03
 **/
export default {
	// 获取电站信息分页
	stationInfoPage(data) {
		return request('page', data, 'get')
	},
	// 提交电站信息表单 edit为true时为编辑，默认为新增
	stationInfoSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除电站信息
	stationInfoDelete(data) {
		return request('delete', data)
	},
	// 获取电站信息详情
	stationInfoDetail(data) {
		return request('detail', data, 'get')
	},
    // 获取电站信息动态字段的配置
	stationInfoDynamicFieldConfigList(data) {
		return request('dynamicFieldConfigList', data, 'get')
	},
	// 状态和警报
	fetchStatusAndAlarmCount(data) {
		return request('statusAndAlarmCount', data, 'get')
	},
	// 下载
	fetchExport(data) {
		return request('export', data,'get', {
			responseType: 'blob'
		})
	},
	// 获取所有电站名称和id列表
	fetchStationId(data) {
		return request('getAllStationNameAndId', data, 'get')
	},
	// 获取电站概览基础信息
	fetchOverview(data) {
		return request('overviewInfo', data)
	},
	// 电站概览-发电实时
	fetchPower(data) {
		return request('power', data)
	},
	// 电站概览-用电实时
	fetchPowerUsed(data) {
		return request('powerUsed', data)
	},
	// 电站概览-电网实时
	fetchPowerGrid(data) {
		return request('powerGrid', data)
	},
	// 电站概览-逆变器排名
	fetchInvRank(data) {
		return request('invRank', data)
	},
	// 电站概览-逆变器告警概要
	fetchInvAlarmBase(data) {
		return request('invAlarmBaseInfo', data)
	},
	// 电站概览-逆变器告警概要
	// fetchPowerAnalysis(data) {
	// 	return request('powerAnalysis', data)
	// },
	// 电站概览-设备统计（逆变器）
	fetchInvStatistics(data) {
		return request('invStatistics', data)
	},
	// 用户转移
	fetchUserChange(data) {
		return request('userChange', data)
	},
	// 已绑定设备列表
	fetchPsDeviceList(data) {
		return request('psDeviceList', data)
	},
	// 电站概览-流动图信息
	fetchSkyFlowDiagram(data) {
		return request('flowDiagram', data)
	},
}
