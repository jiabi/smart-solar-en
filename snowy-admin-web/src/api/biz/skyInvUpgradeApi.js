import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/invupgrade/` + url, ...arg)

/**
 * 逆变器升级Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/04/17 19:49
 **/
export default {
	// 获取逆变器升级分页
	skyInvUpgradePage(data) {
		return request('page', data)
	},
	// 提交逆变器升级表单 edit为true时为编辑，默认为新增
	skyInvUpgradeSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除逆变器升级
	skyInvUpgradeDelete(data) {
		return request('delete', data)
	},
	// 获取逆变器升级详情
	skyInvUpgradeDetail(data) {
		return request('detail', data, 'get')
	},
	// 逆变器升级
	skyInvUpgrade(data) {
		return request('upgrade', data)
	},
}
