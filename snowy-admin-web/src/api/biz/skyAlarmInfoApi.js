import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/alarminfo/` + url, ...arg)

/**
 * 故障信息历史信息Api接口管理器
 *
 * @author 全佳璧
 * @date  2023/10/23 10:50
 **/
export default {
	// 获取故障信息历史信息分页
	skyAlarmInfoPage(data) {
		return request('page', data,'get')
	},
	// 提交故障信息历史信息表单 edit为true时为编辑，默认为新增
	skyAlarmInfoSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除故障信息历史信息
	skyAlarmInfoDelete(data) {
		return request('delete', data)
	},
	// 获取故障信息历史信息详情
	skyAlarmInfoDetail(data) {
		return request('detail', data, 'get')
	}}
