import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/equipmodel/` + url, ...arg)

/**
 * 设备型号Api接口管理器
 *
 * @author 全佳璧
 * @date  2023/09/25 14:15
 **/
export default {
	// 获取设备型号分页
	skyEquipModelPage(data) {
		return request('page', data, 'get')
	},
	// 提交设备型号表单 edit为true时为编辑，默认为新增
	skyEquipModelSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除设备型号
	skyEquipModelDelete(data) {
		return request('delete', data)
	},
	// 获取设备型号详情
	skyEquipModelDetail(data) {
		return request('detail', data, 'get')
	},
	// 获取设备型号集合
	fetchModelList(data) {
		return request('getEquipModelList', data, 'get')
	}
}
	