import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/alarminfocurrent/` + url, ...arg)

/**
 * 故障信息实时信息Api接口管理器
 *
 * @author 全佳璧
 * @date  2023/10/23 10:48
 **/
export default {
	// 获取故障信息实时信息分页
	skyAlarmInfoCurrentPage(data) {
		return request('page', data, 'get')
	},
	// 提交故障信息实时信息表单 edit为true时为编辑，默认为新增
	skyAlarmInfoCurrentSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除故障信息实时信息
	skyAlarmInfoCurrentDelete(data) {
		return request('delete', data)
	},
	// 获取故障信息实时信息详情
	skyAlarmInfoCurrentDetail(data) {
		return request('detail', data, 'get')
	}}
