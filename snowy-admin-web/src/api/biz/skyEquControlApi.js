import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/equControl/` + url, ...arg)

/**
 * 通讯关系Api接口管理器
 *
 * @author 全佳璧
 * @date  2023/10/23 09:50
 **/
export default {
	// 获取通讯关系分页
	skyEquGetParam(data) {
		return request('getParam', data)
	},
	// 远程控制-登录密码校验
	skyEquCheckPassword(data) {
		return request('checkPassword', data)
	},
	// 远程控制
	skyEquControl(data) {
		return request('control', data)
	},
}

