import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/chart/` + url, ...arg)

/**
 * 电站图表Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/02/22 16:57
 **/
export default {
	// 电站图表
	skyStationPsChart(data) {
		return request('psChart', data)
	},
	// 设备图表
	skyEquChart(data) {
		return request('equChart', data)
	},
	// 电站概览-电能分析
	fetchHisPowerProportion(data) {
		return request('hisPowerProportion', data)
	},
	// 电站图表基本实时数据
	fetchPsChartBaseInfo(data) {
		return request('psChartBaseInfo', data)
	},
	// 设备图表基本实时数据
	fetchEquChartBaseInfo(data) {
		return request('equChartBaseInfo', data)
	},
	// 电站图表-日、月、年、总-导出
	fetchPsExport(data) {
		return request('psChartExport', data,  'post', {
			responseType: 'blob'
		  })
	},
	// 电站图表-日、月、年、总-导出
	fetchEquExport(data) {
		return request('equChartExport', data, 'post', {
			responseType: 'blob'
		  })
	},
}
