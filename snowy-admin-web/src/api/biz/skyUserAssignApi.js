import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/userassign/` + url, ...arg)

/**
 * 用户转移Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/02/22 16:25
 **/
export default {
	// 获取用户转移分页
	skyUserAssignPage(data) {
		return request('page', data, 'get')
	},
	// 提交用户转移表单 edit为true时为编辑，默认为新增
	skyUserAssignSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除用户转移
	skyUserAssignDelete(data) {
		return request('delete', data)
	},
	// 获取用户转移详情
	skyUserAssignDetail(data) {
		return request('detail', data, 'get')
	}}
