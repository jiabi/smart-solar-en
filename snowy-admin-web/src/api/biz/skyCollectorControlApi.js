import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/collectorControl/` + url, ...arg)


export default {

	// 采集器远程控制
	skyCollectorControl(data) {
		return request('collectorControl', data)
	},
}
