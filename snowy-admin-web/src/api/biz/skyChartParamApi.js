import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/chartparam/` + url, ...arg)

/**
 * 图表参数字典表Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/02/20 18:55
 **/
export default {
	// 获取图表参数字典表分页
	skyChartParamPage(data) {
		return request('page', data, 'get')
	},
	// 提交图表参数字典表表单 edit为true时为编辑，默认为新增
	skyChartParamSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除图表参数字典表
	skyChartParamDelete(data) {
		return request('delete', data)
	},
	// 获取图表参数字典表详情
	skyChartParamDetail(data) {
		return request('detail', data, 'get')
	},
	// 获取所有表名称
	skyChartParamAllTables(data) {
		return request('getAllTables', data, 'get')
	},
	// 获取指定表的字段名称
	skyChartParamTablesNameAndComment(data) {
		return request('getTablesNameAndComment', data)
	},
	// 获取指定表的字段名称
	skyChartParamList(data) {
		return request('list', data)
	}
}
