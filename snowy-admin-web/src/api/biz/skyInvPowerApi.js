import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/invpower/` + url, ...arg)

/**
 * 设备图表Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/02/22 17:00
 **/
export default {
	// 获取设备图表分页
	skyInvPowerPage(data) {
		return request('page', data, 'get')
	},
	// 提交设备图表表单 edit为true时为编辑，默认为新增
	skyInvPowerSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除设备图表
	skyInvPowerDelete(data) {
		return request('delete', data)
	},
	// 获取设备图表详情
	skyInvPowerDetail(data) {
		return request('detail', data, 'get')
	}}
