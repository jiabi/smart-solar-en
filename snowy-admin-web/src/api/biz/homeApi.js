import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/homepage/` + url, ...arg)

/**
 * 首页Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/03/05 10:39
 **/
export default {
	// 整体发电历史
	fetchAllHisPower(data) {
		return request('allHisPower', data)
	},
	// 获取首页详情
	fetchAllPower(data) {
		return request('allPower', data, 'get')
	},
	// 首页电站分布
	fetchDistribute(data) {
		return request('distribute', data, 'get')
	},
	// 首页电站分布
	fetchDistribute(data) {
		return request('distribute', data, 'get')
	},
	// 获取电站离线和警报状态数量
	fetchStatusAndAlarmCount(data) {
		return request('statusAndAlarmCount', data, 'get')
	},
}
