import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/equrelationship/` + url, ...arg)

/**
 * 通讯关系Api接口管理器
 *
 * @author 全佳璧
 * @date  2023/10/23 09:50
 **/
export default {
	// 获取通讯关系分页
	skyEquRelationshipPage(data) {
		return request('page', data, 'get')
	},
	// 提交通讯关系表单 edit为true时为编辑，默认为新增
	skyEquRelationshipSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除通讯关系
	skyEquRelationshipDelete(data) {
		return request('delete', data)
	},
	// 获取通讯关系详情
	skyEquRelationshipDetail(data) {
		return request('detail', data, 'get')
	},
	// 通过SN获取通讯关系
	skyEquRelationshipRelationBySN(data) {
		return request('relationBySN', data)
	},
	skyGetCollectorSn(data) {
		return request('getCollectorSn', data)
	}
}

