import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/stationequ/` + url, ...arg)

/**
 * 电站设备关系Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/03/22 14:00
 **/
export default {
	// 获取电站设备关系分页
	skyStationEquPage(data) {
		return request('page', data, 'get')
	},
	// 提交电站设备关系表单 edit为true时为编辑，默认为新增
	skyStationEquSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除电站设备关系
	skyStationEquDelete(data) {
		return request('delete', data)
	},
	// 获取电站设备关系详情
	skyStationEquDetail(data) {
		return request('detail', data, 'get')
	}}
