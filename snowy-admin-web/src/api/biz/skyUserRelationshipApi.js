import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/userrelationship/` + url, ...arg)

/**
 * 用户关系Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/04/01 16:49
 **/
export default {
	// 获取用户关系分页
	skyUserRelationshipPage(data) {
		return request('page', data, 'get')
	},
	// 提交用户关系表单 edit为true时为编辑，默认为新增
	skyUserRelationshipSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除用户关系
	skyUserRelationshipDelete(data) {
		return request('delete', data)
	},
	// 获取用户关系详情
	skyUserRelationshipDetail(data) {
		return request('detail', data, 'get')
	}}
