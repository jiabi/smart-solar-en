import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/invmonitor/` + url, ...arg)

/**
 * 逆变器历史数据Api接口管理器
 *
 * @author 全佳璧
 * @date  2023/10/23 09:44
 **/
export default {
	// 获取逆变器历史数据分页
	skyInvMonitorPage(data) {
		return request('page', data, 'get')
	},
	// 提交逆变器历史数据表单 edit为true时为编辑，默认为新增
	skyInvMonitorSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除逆变器历史数据
	skyInvMonitorDelete(data) {
		return request('delete', data)
	},
	// 获取逆变器历史数据详情
	skyInvMonitorDetail(data) {
		return request('detail', data, 'get')
	}}
