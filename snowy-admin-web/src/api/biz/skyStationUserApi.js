import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/stationuser/` + url, ...arg)

/**
 * 电站用户关系Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/04/01 16:32
 **/
export default {
	// 获取电站用户关系分页
	skyStationUserPage(data) {
		return request('page', data, 'get')
	},
	// 提交电站用户关系表单 edit为true时为编辑，默认为新增
	skyStationUserSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除电站用户关系
	skyStationUserDelete(data) {
		return request('delete', data)
	},
	// 获取电站用户关系详情
	skyStationUserDetail(data) {
		return request('detail', data, 'get')
	}}
