import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/eququality/` + url, ...arg)

/**
 * 设备质保信息Api接口管理器
 *
 * @author 全佳璧
 * @date  2023/09/25 14:20
 **/
export default {
	// 获取设备质保信息分页
	skyEquQualityPage(data) {
		return request('page', data, 'get')
	},
	// 提交设备质保信息表单 edit为true时为编辑，默认为新增
	skyEquQualitySubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除设备质保信息
	skyEquQualityDelete(data) {
		return request('delete', data)
	},
	// 获取设备质保信息详情
	skyEquQualityDetail(data) {
		return request('detail', data, 'get')
	},
	// 获取设备Sn集合
	skyEquipSnList(data) {
		return request('getEquipSns', data, 'get')
	},
	// 质保信息下载
	skyEquExport(data) {
		return request('export', data, 'get',{
			responseType: 'blob'
		})
	},
	// 质保模板信息下载
	skyGetExcelModel(data) {
		return request('getExcelModel', data, 'get',{
			responseType: 'blob'
		})
	},
	// 质保模板信息导入
	skyImportByExcel(data) {
		return request('importByExcel', data, 'post',{
			responseType: 'blob'
		})
	},
}