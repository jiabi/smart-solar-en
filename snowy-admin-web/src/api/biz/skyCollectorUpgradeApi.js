import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/collectorupgrade/` + url, ...arg)

/**
 * 采集器升级Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/04/17 19:51
 **/
export default {
	// 获取采集器升级分页
	skyCollectorUpgradePage(data) {
		return request('page', data)
	},
	// 提交采集器升级表单 edit为true时为编辑，默认为新增
	skyCollectorUpgradeSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除采集器升级
	skyCollectorUpgradeDelete(data) {
		return request('delete', data)
	},
	// 获取采集器升级详情
	skyCollectorUpgradeDetail(data) {
		return request('detail', data, 'get')
	},
	// 采集器升级
	skyInvUpgrade(data) {
		return request('upgrade', data)
	},
}
