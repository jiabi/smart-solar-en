import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/invmonitorcurrent/` + url, ...arg)

/**
 * 逆变器信息Api接口管理器
 *
 * @author wmf
 * @date  2023/10/05 16:24
 **/
export default {
	// 通过SN获取历史数据(日)
	invSnDay(data) {
		return request('historyDetailByInvSn', data, 'get')
	},
	invSnPower(data) {
		return request('power', data, 'get')
	},
}	