import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/upgraderecord/` + url, ...arg)

/**
 * 升级记录Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/04/17 19:55
 **/
export default {
	// 获取升级记录分页
	skyUpgradeRecordPage(data) {
		return request('page', data)
	},
	// 提交升级记录表单 edit为true时为编辑，默认为新增
	skyUpgradeRecordSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除升级记录
	skyUpgradeRecordDelete(data) {
		return request('delete', data)
	},
	// 获取升级记录详情
	skyUpgradeRecordDetail(data) {
		return request('detail', data, 'get')
	}}
