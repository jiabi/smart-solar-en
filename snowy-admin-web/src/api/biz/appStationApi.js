import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/app/powerStation/` + url, ...arg)

/**
 * 通讯关系Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/04/09
 **/
export default {
	// 获取所有安装商
	skyGetAllInstaller(data) {
		return request('getAllInstaller', data)
	}
}

