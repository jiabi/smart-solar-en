import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/app/user/` + url, ...arg)

/**
 * 通讯关系Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/04/09
 **/
export default {
	// 获取手机验证码
	skyAppGetPhoneValidCode(data) {
		return request('appGetPhoneValidCode', data)
	},
	// 获取邮箱验证码
	skyAppGetEmailValidCode(data) {
		return request('appGetEmailValidCode', data)
	},
	// 用户注册
	skyAppRegister(data) {
		return request('register', data)
	}
}

