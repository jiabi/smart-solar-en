import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/equbin/` + url, ...arg)

/**
 * 设备升级包Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/04/17 19:40
 **/
export default {
	// 获取设备升级包分页
	skyEquBinPage(data) {
		return request('page', data)
	},
	// 提交设备升级包表单 edit为true时为编辑，默认为新增
	skyEquBinSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除设备升级包
	skyEquBinDelete(data) {
		return request('delete', data)
	},
	// 获取所有升级包集合
	skyEquBinList(data) {
		return request('list', data)
	},
	// 腾讯云文件上传，返回文件id
	fileStorageFileWithReturnUrlLocal(data) {
		return request('storageFileWithReturnUrlLocal', data)
	},
	// 获取设备升级包详情
	skyEquBinDetail(data) {
		return request('detail', data, 'get')
	}}
