import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/timezone/` + url, ...arg)

/**
 * 地区时区Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/03/08 16:23
 **/
export default {
	// 获取地区时区分页
	skyTimezonePage(data) {
		return request('page', data, 'get')
	},
	// 提交地区时区表单 edit为true时为编辑，默认为新增
	skyTimezoneSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除地区时区
	skyTimezoneDelete(data) {
		return request('delete', data)
	},
	// 获取地区时区详情
	skyTimezoneDetail(data) {
		return request('detail', data, 'get')
	}}
