import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/collectorModel/` + url, ...arg)


export default {

	// 读取采集器的指令数据
	skyCollectorRed(data) {
		return request('collectorRed', data)
	},
}
