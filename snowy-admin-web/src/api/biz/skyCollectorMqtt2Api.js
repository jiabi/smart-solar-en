import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/collectormqtt2/` + url, ...arg)

/**
 * 采集器转发Api接口管理器
 *
 * @author hao
 * @date  2024/09/11 19:16
 **/
export default {
	// 获取采集器转发分页
	skyCollectorMqtt2Page(data) {
		return request('page', data, 'get')
	},
	// 提交采集器转发表单 edit为true时为编辑，默认为新增
	skyCollectorMqtt2SubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除采集器转发
	skyCollectorMqtt2Delete(data) {
		return request('delete', data)
	},
	// 获取采集器转发详情
	skyCollectorMqtt2Detail(data) {
		return request('detail', data, 'get')
	},
	// 获取采集器转发详情
	skyCollectorQueryMqtt2(data) {
		return request('queryMqtt2', data, 'get')
	}
}
