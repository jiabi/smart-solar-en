import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/file/` + url, ...arg)

export default {
	// 腾讯云文件上传，返回文件id
	fileStorageBinReturnUrlAliyun(data) {
		return request('storageBinReturnUrlAliyun', data)
	},
	// 腾讯云文件上传，返回文件id
	fileStoragePicReturnUrlAliyun(data) {
		return request('storagePicReturnUrlAliyun', data)
	},
}
