import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/chartparamconfig/` + url, ...arg)

/**
 * 参数匹配Api接口管理器
 *
 * @author 王坚
 * @date  2024/02/21 11:51
 **/
export default {
	// 获取参数匹配分页
	skyChartParamConfigPage(data) {
		return request('page', data, 'get')
	},
	// 提交参数匹配表单 edit为true时为编辑，默认为新增
	skyChartParamConfigSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除参数匹配
	skyChartParamConfigDelete(data) {
		return request('delete', data)
	},
	// 获取参数匹配详情
	skyChartParamConfigDetail(data) {
		return request('detail', data, 'get')
	}}
