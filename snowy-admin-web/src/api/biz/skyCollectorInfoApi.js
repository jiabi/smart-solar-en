import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/biz/collectorinfo/` + url, ...arg)

/**
 * 采集器信息Api接口管理器
 *
 * @author 全佳璧
 * @date  2023/10/23 09:46
 **/
export default {
	// 获取采集器信息分页
	skyCollectorInfoPage(data) {
		return request('page', data, 'get')
	},
	// 提交采集器信息表单 edit为true时为编辑，默认为新增
	skyCollectorInfoSubmitForm(data, edit = false) {
		return request(edit ? 'edit' : 'add', data)
	},
	// 删除采集器信息
	skyCollectorInfoDelete(data) {
		return request('delete', data)
	},
	// 获取采集器信息详情
	skyCollectorInfoDetail(data) {
		return request('detail', data, 'get')
	}}
