import { baseRequest } from '@/utils/request'

const request = (url, ...arg) => baseRequest(`/sys/userCenter/` + url, ...arg)

/**
 * 通讯关系Api接口管理器
 *
 * @author 全佳璧
 * @date  2024/04/09
 **/
export default {
	// 获取手机验证码
	skyFindPasswordByPhone(data) {
		return request('findPasswordByPhone', data)
	},
	// 直接获取手机验证码并返回请求号
	skyGetPhoneValidCode(data) {
		return request('getPhoneValidCode', data)
	},
	// 忘记密码-通过手机号校验验证码
	skyFindPwdByPhoneCheckValidCode(data) {
		return request('findPwdByPhoneCheckValidCode', data)
	},
	// 忘记密码-Email
	skyFindPwdCheckEmailValidCode(data) {
		return request('findPwdCheckEmailValidCode', data)
	},
	// 用户注册
	skyAppRegister(data) {
		return request('register', data)
	},
	// 忘记密码-通过手机号校验验证码
	skyNewPassword(data) {
		return request('newPassword', data)
	},
	// 用户注册--获取邮箱验证码
	skyLoginGetEmailValidCode(data) {
		return request('getEmailValidCode', data);
	}
	
}

