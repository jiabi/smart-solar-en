// formatPower.js

export const formatPower = (e) => {
  if (e !== null) {
    const value = Number(e);
    if (value >= 0) {
      if (value > 1000000) {
        return `${(value / 1000000).toFixed(2)}GW`;
      } else if (value >= 1000) {
        return `${(value / 1000).toFixed(2)}MW`;
      } else {
        return `${value}kW`;
      }
    } else {
      return `${value}kW`;
    }
  } else {
    return '--';
  }

};

export const analysisNum = (e) => {
  if (e !== null) {
    const value = Number(e);
    if (value >= 0) {
      if (value > 1000000) {
        return `${(value / 1000000).toFixed(2)}GWh`;
      } else if (value >= 1000) {
        return `${(value / 1000).toFixed(2)}MWh`;
      } else {
        return `${value}kWh`;
      }
    } else {
      return `${value}kWh`;
    }
  } else {
    return '--';
  }

};

export const analysisSize = (e) => {
  if (e !== null) {
        const value = Number(e);
        if (value >= 0) {
          if (value > 1000000) {
            return `${(value / 1000000).toFixed(2)}GWp`;
          } else if (value >= 1000) {
            return `${(value / 1000).toFixed(2)}MWp`;
          } else {
            return `${value}KWp`;
          }
        } else {
          return `${value}KWp`;
        }
      } else {
        return '--';
      }


};


export const powerNum = (e) => {
  if (e !== null) {
        const value = Number(e);
        if (value >= 0) {
          if (value > 1000000) {
            return `${(value / 1000000).toFixed(2)}MW`;
          } else if (value >= 1000) {
            return `${(value / 1000).toFixed(2)}KW`;
          } else {
            return `${value}W`;
          }
        } else {
          return `${value}W`;
        }
      } else {
        return '--';
      }

};
export const monNum = (e) => {
  if (e !== null) {
    const value = Number(e)
    if (value >= 0) {
      if (value < 10000) {
        return `${value}元`;
      } else if (value > 10000) {
        return `${(value / 10000).toFixed(2)}万`;
      }
    } else {
      return `${value}元`;
    }
  } else {
    return '--';
  }
}