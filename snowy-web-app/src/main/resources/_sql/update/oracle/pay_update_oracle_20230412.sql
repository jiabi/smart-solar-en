-- ----------------------------
-- 创建订单表
-- ----------------------------
CREATE TABLE "SNOWY"."PAY_ORDER" (
  "ID" NVARCHAR2(20) NOT NULL,
  "TENANT_ID" NVARCHAR2(20),
  "OUT_TRADE_NO" NVARCHAR2(255),
  "TRADE_NO" NVARCHAR2(255),
  "SUBJECT" NVARCHAR2(255),
  "BODY" NVARCHAR2(255),
  "ORDER_AMOUNT" NVARCHAR2(255),
  "PAY_USER_ID" NVARCHAR2(255),
  "PAY_ACCOUNT" NVARCHAR2(255),
  "PAY_AMOUNT" NVARCHAR2(255),
  "PAY_PLATFORM" NVARCHAR2(255),
  "PAY_STATUS" NVARCHAR2(255),
  "PAY_TIME" DATE,
  "HAS_REFUND" NVARCHAR2(255),
  "DELETE_FLAG" NVARCHAR2(255),
  "CREATE_TIME" DATE,
  "CREATE_USER" NVARCHAR2(20),
  "UPDATE_TIME" DATE,
  "UPDATE_USER" NVARCHAR2(20)
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."TENANT_ID" IS '租户id';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."OUT_TRADE_NO" IS '商户订单号';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."TRADE_NO" IS '支付平台订单号';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."SUBJECT" IS '订单标题';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."BODY" IS '订单描述';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."ORDER_AMOUNT" IS '订单金额';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."PAY_USER_ID" IS '买家id';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."PAY_ACCOUNT" IS '买家账号';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."PAY_AMOUNT" IS '支付金额';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."PAY_PLATFORM" IS '支付平台';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."PAY_STATUS" IS '支付状态';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."PAY_TIME" IS '支付时间';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."HAS_REFUND" IS '退款标志';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."DELETE_FLAG" IS '删除标志';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."CREATE_USER" IS '创建用户';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."UPDATE_TIME" IS '修改时间';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER"."UPDATE_USER" IS '修改用户';
COMMENT ON TABLE "SNOWY"."PAY_ORDER" IS '订单';

-- ----------------------------
-- 创建订单明细表
-- ----------------------------
CREATE TABLE "SNOWY"."PAY_ORDER_DETAILS" (
  "ID" NVARCHAR2(20) NOT NULL,
  "TENANT_ID" NVARCHAR2(20),
  "ORDER_ID" NVARCHAR2(20),
  "PRODUCT_ID" NVARCHAR2(20),
  "PRODUCT_NAME" NVARCHAR2(255),
  "PRODUCT_AMOUNT" NVARCHAR2(255),
  "PRODUCT_COUNT" NUMBER(11,0),
  "DELETE_FLAG" NVARCHAR2(255),
  "CREATE_TIME" DATE,
  "CREATE_USER" NVARCHAR2(20),
  "UPDATE_TIME" DATE,
  "UPDATE_USER" NVARCHAR2(20)
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."ID" IS 'id';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."TENANT_ID" IS '租户id';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."ORDER_ID" IS '订单id';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."PRODUCT_ID" IS '产品id';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."PRODUCT_NAME" IS '产品名称';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."PRODUCT_AMOUNT" IS '产品价格';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."PRODUCT_COUNT" IS '产品数量';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."DELETE_FLAG" IS '删除标志';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."CREATE_USER" IS '创建用户';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."UPDATE_TIME" IS '修改时间';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_DETAILS"."UPDATE_USER" IS '修改用户';
COMMENT ON TABLE "SNOWY"."PAY_ORDER_DETAILS" IS '订单明细';

-- ----------------------------
-- 创建订单退款表
-- ----------------------------
CREATE TABLE "SNOWY"."PAY_ORDER_REFUND" (
  "ID" NVARCHAR2(20) NOT NULL,
  "TENANT_ID" NVARCHAR2(20),
  "ORDER_ID" NVARCHAR2(20),
  "OUT_TRADE_NO" NVARCHAR2(255),
  "TRADE_NO" NVARCHAR2(255),
  "REFUND_NO" NVARCHAR2(255),
  "REFUND_USER_ID" NVARCHAR2(255),
  "REFUND_ACCOUNT" NVARCHAR2(255),
  "REFUND_AMOUNT" NVARCHAR2(255),
  "REFUND_STATUS" NVARCHAR2(255),
  "REFUND_TIME" DATE,
  "DELETE_FLAG" NVARCHAR2(255),
  "CREATE_TIME" DATE,
  "CREATE_USER" NVARCHAR2(20),
  "UPDATE_TIME" DATE,
  "UPDATE_USER" NVARCHAR2(20)
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."TENANT_ID" IS '租户id';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."ORDER_ID" IS '订单id';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."OUT_TRADE_NO" IS '商户订单号';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."TRADE_NO" IS '支付平台退款单号';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."REFUND_NO" IS '商户退款单号';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."REFUND_USER_ID" IS '退款到买家id';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."REFUND_ACCOUNT" IS '退款到买家账号';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."REFUND_AMOUNT" IS '退款金额';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."REFUND_STATUS" IS '退款状态';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."REFUND_TIME" IS '退款时间';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."DELETE_FLAG" IS '删除标志';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."CREATE_USER" IS '创建用户';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."UPDATE_TIME" IS '修改时间';
COMMENT ON COLUMN "SNOWY"."PAY_ORDER_REFUND"."UPDATE_USER" IS '修改用户';
COMMENT ON TABLE "SNOWY"."PAY_ORDER_REFUND" IS '订单退款';

-- ----------------------------
-- Checks structure for table PAY_ORDER
-- ----------------------------
ALTER TABLE "SNOWY"."PAY_ORDER" ADD CONSTRAINT "SYS_C0011382" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table PAY_ORDER_DETAILS
-- ----------------------------
ALTER TABLE "SNOWY"."PAY_ORDER_DETAILS" ADD CONSTRAINT "SYS_C0011383" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table PAY_ORDER_REFUND
-- ----------------------------
ALTER TABLE "SNOWY"."PAY_ORDER_REFUND" ADD CONSTRAINT "SYS_C0011384" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- 新增支付相关字典
-- ----------------------------
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103344661086210', '-1', '0', '支付订单平台', 'PAY_PLATFORM', 'FRM', '100', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:36:17', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103422368956417', '-1', '1645103344661086210', '支付宝', 'ALIPAY', 'FRM', '101', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:36:35', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103492682268673', '-1', '1645103344661086210', '微信支付', 'WXPAY', 'FRM', '102', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:36:52', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729423874', '-1', '0', '支付订单状态', 'PAY_STATUS', 'FRM', '103', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:38:26', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424861', '-1', '1645103888729423874', '已下单', 'NO_TRADE', 'FRM', '104', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:38:47', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424862', '-1', '1645103888729423874', '暂未支付', 'NOTPAY', 'FRM', '104', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:38:47', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424863', '-1', '1645103888729423874', '等待支付', 'USERPAYING', 'FRM', '104', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:38:47', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424864', '-1', '1645103888729423874', '等待支付', 'WAIT_BUYER_PAY', 'FRM', '104', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:38:47', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424865', '-1', '1645103888729423874', '支付成功', 'SUCCESS', 'FRM', '104', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:38:47', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424866', '-1', '1645103888729423874', '支付成功', 'TRADE_SUCCESS', 'FRM', '104', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:38:47', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424867', '-1', '1645103888729423874', '支付失败', 'PAYERROR', 'FRM', '104', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:38:47', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424868', '-1', '1645103888729423874', '转入退款', 'REFUND', 'FRM', '104', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:38:47', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424869', '-1', '1645103888729423874', '交易撤销', 'REVOKED', 'FRM', '104', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:38:47', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424870', '-1', '1645103888729423874', '交易关闭', 'CLOSED', 'FRM', '105', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:39:08', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424871', '-1', '1645103888729423874', '交易关闭', 'TRADE_CLOSED', 'FRM', '106', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 01:02:18', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729424872', '-1', '1645103888729423874', '交易结束', 'TRADE_FINISHED', 'FRM', '107', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 01:03:14', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729425876', '-1', '0', '订单退款状态', 'PAY_ORDER_REFUND_STATUS', 'FRM', '100', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:36:17', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729425920', '-1', '1645103888729425876', '退款处理中', 'REFUND_PENDING', 'FRM', '101', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:36:35', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729425921', '-1', '1645103888729425876', '退款成功', 'REFUND_SUCCESS', 'FRM', '102', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:36:52', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);
INSERT INTO "SNOWY"."DEV_DICT"("ID", "TENANT_ID", "PARENT_ID", "DICT_LABEL", "DICT_VALUE", "CATEGORY", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1645103888729425922', '-1', '1645103888729425876', '退款失败', 'REFUND_FAIL', 'FRM', '102', NULL, 'NOT_DELETE', TO_DATE('2023-04-10 00:36:52', 'SYYYY-MM-DD HH24:MI:SS'), '1543837863788879871', NULL, NULL);

-- ----------------------------
-- 更新原有的两个菜单
-- ----------------------------
UPDATE "SNOWY"."SYS_RESOURCE" SET "ID" = '1548901111999773237', "TENANT_ID" = '-1', "PARENT_ID" = '1548901111999773236', "TITLE" = '支付示例', "NAME" = 'zhiFuShiLi', "CODE" = 'mep5ficfau', "CATEGORY" = 'MENU', "MODULE" = '1548901111999770525', "MENU_TYPE" = 'MENU', "PATH" = '/pay/sample/index', "COMPONENT" = 'pay/sample/index', "ICON" = 'alert-outlined', "COLOR" = NULL, "SORT_CODE" = '39', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1548901111999773237' AND "TENANT_ID" = '-1' AND "PARENT_ID" = '1548901111999773236' AND "TITLE" = '支付示例' AND "NAME" = 'zhiFuShiLi' AND "CODE" = 'mep5ficfau' AND "CATEGORY" = 'MENU' AND "MODULE" = '1548901111999770525' AND "MENU_TYPE" = 'MENU' AND "PATH" = '/pay/sample/index' AND "COMPONENT" = 'pay/sample/index' AND "ICON" = 'alert-outlined' AND "COLOR" IS NULL AND "SORT_CODE" = '39' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;
UPDATE "SNOWY"."SYS_RESOURCE" SET "ID" = '1548901111999773238', "TENANT_ID" = '-1', "PARENT_ID" = '1548901111999773236', "TITLE" = '订单管理', "NAME" = 'dingDanGuanLi', "CODE" = '7x4pc5az81', "CATEGORY" = 'MENU', "MODULE" = '1548901111999770525', "MENU_TYPE" = 'MENU', "PATH" = '/pay/order', "COMPONENT" = 'pay/order/index', "ICON" = 'clear-outlined', "COLOR" = NULL, "SORT_CODE" = '40', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1548901111999773238' AND "TENANT_ID" = '-1' AND "PARENT_ID" = '1548901111999773236' AND "TITLE" = '订单管理' AND "NAME" = 'dingDanGuanLi' AND "CODE" = '7x4pc5az81' AND "CATEGORY" = 'MENU' AND "MODULE" = '1548901111999770525' AND "MENU_TYPE" = 'MENU' AND "PATH" = '/pay/order' AND "COMPONENT" = 'pay/order/index' AND "ICON" = 'clear-outlined' AND "COLOR" IS NULL AND "SORT_CODE" = '40' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;

-- ----------------------------
-- 更新和增加配置
-- ----------------------------
UPDATE "SNOWY"."DEV_CONFIG" SET "ID" = '1554740179362967595', "TENANT_ID" = '-1', "CONFIG_KEY" = 'SNOWY_PAY_ALI_APP_ID', "CONFIG_VALUE" = '支付宝支付应用id', "CATEGORY" = 'PAY_ALI', "REMARK" = '支付宝支付应用id', "SORT_CODE" = '50', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1554740179362967595' AND "TENANT_ID" = '-1' AND "CONFIG_KEY" = 'SNOWY_PAY_ALI_APP_ID' AND "CONFIG_VALUE" = '支付宝支付应用id' AND "CATEGORY" = 'PAY_ALI' AND "REMARK" = '支付宝支付应用id' AND "SORT_CODE" = '50' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;
UPDATE "SNOWY"."DEV_CONFIG" SET "ID" = '1554740179362967596', "TENANT_ID" = '-1', "CONFIG_KEY" = 'SNOWY_PAY_ALI_PRIVATE_KEY', "CONFIG_VALUE" = '支付宝支付私钥', "CATEGORY" = 'PAY_ALI', "REMARK" = '支付宝支付私钥', "SORT_CODE" = '51', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1554740179362967596' AND "TENANT_ID" = '-1' AND "CONFIG_KEY" = 'SNOWY_PAY_ALI_PRIVATE_KEY' AND "CONFIG_VALUE" = '支付宝支付私钥' AND "CATEGORY" = 'PAY_ALI' AND "REMARK" = '支付宝支付私钥' AND "SORT_CODE" = '51' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;
UPDATE "SNOWY"."DEV_CONFIG" SET "ID" = '1554740179362967597', "TENANT_ID" = '-1', "CONFIG_KEY" = 'SNOWY_PAY_ALI_CERT_PATH', "CONFIG_VALUE" = '支付宝支付证书位置', "CATEGORY" = 'PAY_ALI', "REMARK" = '支付宝支付证书位置', "SORT_CODE" = '52', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1554740179362967597' AND "TENANT_ID" = '-1' AND "CONFIG_KEY" = 'SNOWY_PAY_ALI_CERT_PATH' AND "CONFIG_VALUE" = '支付宝支付证书位置' AND "CATEGORY" = 'PAY_ALI' AND "REMARK" = '支付宝支付证书位置' AND "SORT_CODE" = '52' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;
UPDATE "SNOWY"."DEV_CONFIG" SET "ID" = '1554740179362967598', "TENANT_ID" = '-1', "CONFIG_KEY" = 'SNOWY_PAY_ALI_ROOT_CERT_PATH', "CONFIG_VALUE" = '支付宝支付根证书位置', "CATEGORY" = 'PAY_ALI', "REMARK" = '支付宝支付根证书位置', "SORT_CODE" = '53', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1554740179362967598' AND "TENANT_ID" = '-1' AND "CONFIG_KEY" = 'SNOWY_PAY_ALI_ROOT_CERT_PATH' AND "CONFIG_VALUE" = '支付宝支付根证书位置' AND "CATEGORY" = 'PAY_ALI' AND "REMARK" = '支付宝支付根证书位置' AND "SORT_CODE" = '53' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;
UPDATE "SNOWY"."DEV_CONFIG" SET "ID" = '1554740179362967599', "TENANT_ID" = '-1', "CONFIG_KEY" = 'SNOWY_PAY_ALI_APP_CERT_PATH', "CONFIG_VALUE" = '支付宝支付应用证书位置', "CATEGORY" = 'PAY_ALI', "REMARK" = '支付宝支付应用证书位置', "SORT_CODE" = '54', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1554740179362967599' AND "TENANT_ID" = '-1' AND "CONFIG_KEY" = 'SNOWY_PAY_ALI_APP_CERT_PATH' AND "CONFIG_VALUE" = '支付宝支付应用证书位置' AND "CATEGORY" = 'PAY_ALI' AND "REMARK" = '支付宝支付应用证书位置' AND "SORT_CODE" = '54' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;
UPDATE "SNOWY"."DEV_CONFIG" SET "ID" = '1554740179362967600', "TENANT_ID" = '-1', "CONFIG_KEY" = 'SNOWY_PAY_ALI_SERVER_URL', "CONFIG_VALUE" = '支付宝支付网关', "CATEGORY" = 'PAY_ALI', "REMARK" = '支付宝支付网关', "SORT_CODE" = '55', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1554740179362967600' AND "TENANT_ID" = '-1' AND "CONFIG_KEY" = 'SNOWY_PAY_ALI_SERVER_URL' AND "CONFIG_VALUE" = '支付宝支付网关' AND "CATEGORY" = 'PAY_ALI' AND "REMARK" = '支付宝支付网关' AND "SORT_CODE" = '55' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;
UPDATE "SNOWY"."DEV_CONFIG" SET "ID" = '1554740179362967701', "TENANT_ID" = '-1', "CONFIG_KEY" = 'SNOWY_PAY_WX_APP_ID', "CONFIG_VALUE" = '微信支付应用id', "CATEGORY" = 'PAY_WX', "REMARK" = '微信支付应用id', "SORT_CODE" = '58', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1554740179362967701' AND "TENANT_ID" = '-1' AND "CONFIG_KEY" = 'SNOWY_PAY_WX_APP_ID' AND "CONFIG_VALUE" = '微信支付应用id' AND "CATEGORY" = 'PAY_WX' AND "REMARK" = '微信支付应用id' AND "SORT_CODE" = '58' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;
UPDATE "SNOWY"."DEV_CONFIG" SET "ID" = '1554740179362967702', "TENANT_ID" = '-1', "CONFIG_KEY" = 'SNOWY_PAY_WX_APP_SECRET', "CONFIG_VALUE" = '微信支付公众号AppSecret', "CATEGORY" = 'PAY_WX', "REMARK" = '微信支付公众号AppSecret', "SORT_CODE" = '59', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1554740179362967702' AND "TENANT_ID" = '-1' AND "CONFIG_KEY" = 'SNOWY_PAY_WX_APP_SECRET' AND "CONFIG_VALUE" = '微信支付公众号AppSecret' AND "CATEGORY" = 'PAY_WX' AND "REMARK" = '微信支付公众号AppSecret' AND "SORT_CODE" = '59' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;
UPDATE "SNOWY"."DEV_CONFIG" SET "ID" = '1554740179362967703', "TENANT_ID" = '-1', "CONFIG_KEY" = 'SNOWY_PAY_WX_MCH_ID', "CONFIG_VALUE" = '微信支付商户号', "CATEGORY" = 'PAY_WX', "REMARK" = '微信支付商户号', "SORT_CODE" = '60', "EXT_JSON" = NULL, "DELETE_FLAG" = 'NOT_DELETE', "CREATE_TIME" = NULL, "CREATE_USER" = NULL, "UPDATE_TIME" = NULL, "UPDATE_USER" = NULL WHERE "ID" = '1554740179362967703' AND "TENANT_ID" = '-1' AND "CONFIG_KEY" = 'SNOWY_PAY_WX_MCH_ID' AND "CONFIG_VALUE" = '微信支付商户号' AND "CATEGORY" = 'PAY_WX' AND "REMARK" = '微信支付商户号' AND "SORT_CODE" = '60' AND "EXT_JSON" IS NULL AND "DELETE_FLAG" = 'NOT_DELETE' AND "CREATE_TIME" IS NULL AND "CREATE_USER" IS NULL AND "UPDATE_TIME" IS NULL AND "UPDATE_USER" IS NULL;
INSERT INTO "SNOWY"."DEV_CONFIG"("ID", "TENANT_ID", "CONFIG_KEY", "CONFIG_VALUE", "CATEGORY", "REMARK", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1554740179362967601', '-1', 'SNOWY_PAY_ALI_MCH_USER_ID', '支付宝支付商家用户id', 'PAY_ALI', '支付宝支付商家用户id', '56', NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO "SNOWY"."DEV_CONFIG"("ID", "TENANT_ID", "CONFIG_KEY", "CONFIG_VALUE", "CATEGORY", "REMARK", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1554740179362967602', '-1', 'SNOWY_PAY_ALI_NOTIFY_URL', '支付宝支付回调地址', 'PAY_ALI', '支付宝支付回调地址', '57', NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO "SNOWY"."DEV_CONFIG"("ID", "TENANT_ID", "CONFIG_KEY", "CONFIG_VALUE", "CATEGORY", "REMARK", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1554740179362967704', '-1', 'SNOWY_PAY_WX_MCH_KEY', '微信支付商户V2密钥', 'PAY_WX', '微信支付商户V2密钥', '61', NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO "SNOWY"."DEV_CONFIG"("ID", "TENANT_ID", "CONFIG_KEY", "CONFIG_VALUE", "CATEGORY", "REMARK", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1554740179362967705', '-1', 'SNOWY_PAY_WX_KEY_PATH', '微信支付p12证书路径', 'PAY_WX', '微信支付p12证书路径', '62', NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO "SNOWY"."DEV_CONFIG"("ID", "TENANT_ID", "CONFIG_KEY", "CONFIG_VALUE", "CATEGORY", "REMARK", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1554740179362967706', '-1', 'SNOWY_PAY_WX_PRIVATE_KEY_PATH', '微信支付apiClientKey路径', 'PAY_WX', '微信支付apiClientKey路径', '63', NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO "SNOWY"."DEV_CONFIG"("ID", "TENANT_ID", "CONFIG_KEY", "CONFIG_VALUE", "CATEGORY", "REMARK", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1554740179362967707', '-1', 'SNOWY_PAY_WX_PRIVATE_CERT_PATH', '微信支付apiClientCert路径', 'PAY_WX', '微信支付apiClientCert路径', '64', NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO "SNOWY"."DEV_CONFIG"("ID", "TENANT_ID", "CONFIG_KEY", "CONFIG_VALUE", "CATEGORY", "REMARK", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1554740179362967708', '-1', 'SNOWY_PAY_WX_CERT_SERIAL_NO', '微信支付ApiV3证书序列号值', 'PAY_WX', '微信支付ApiV3证书序列号值', '65', NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO "SNOWY"."DEV_CONFIG"("ID", "TENANT_ID", "CONFIG_KEY", "CONFIG_VALUE", "CATEGORY", "REMARK", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1554740179362967709', '-1', 'SNOWY_PAY_WX_API_V3_KEY', '微信支付ApiV3密钥值', 'PAY_WX', '微信支付ApiV3密钥值', '66', NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO "SNOWY"."DEV_CONFIG"("ID", "TENANT_ID", "CONFIG_KEY", "CONFIG_VALUE", "CATEGORY", "REMARK", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1554740179362967710', '-1', 'SNOWY_PAY_WX_NOTIFY_URL', '微信支付回调地址', 'PAY_WX', '微信支付回调地址', '67', NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO "SNOWY"."DEV_CONFIG"("ID", "TENANT_ID", "CONFIG_KEY", "CONFIG_VALUE", "CATEGORY", "REMARK", "SORT_CODE", "EXT_JSON", "DELETE_FLAG", "CREATE_TIME", "CREATE_USER", "UPDATE_TIME", "UPDATE_USER") VALUES ('1554740179362967711', '-1', 'SNOWY_PAY_WX_REFUND_NOTIFY_URL', '微信支付退款回调地址', 'PAY_WX', '微信支付退款回调地址', '68', NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
