-- ----------------------------
-- 2023.02.10增加移动端管理的相关字典、菜单
-- ----------------------------
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Records of DEV_DICT
-- ----------------------------
INSERT INTO `DEV_DICT` VALUES ('1619343323218432002', '-1', '0', '移动菜单状态', 'MOBILE_STATUS', 'FRM', 93, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `DEV_DICT` VALUES ('1619343680636047362', '-1', '1619343323218432002', '可用', 'ENABLE', 'FRM', 94, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `DEV_DICT` VALUES ('1619343846382358529', '-1', '1619343323218432002', '不可用', 'DISABLED', 'FRM', 96, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `DEV_DICT` VALUES ('1619344256295882753', '-1', '0', '移动菜单规则', 'MOBILE_REG_TYPE', 'FRM', 97, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `DEV_DICT` VALUES ('1619344428111351809', '-1', '1619344256295882753', '正规则', 'YES', 'FRM', 98, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `DEV_DICT` VALUES ('1619344504456073218', '-1', '1619344256295882753', '反规则', 'NO', 'FRM', 99, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for MOBILE_RESOURCE
-- ----------------------------
DROP TABLE IF EXISTS `MOBILE_RESOURCE`;
CREATE TABLE `MOBILE_RESOURCE`  (
  `ID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `TENANT_ID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户id',
  `PARENT_ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父ID',
  `TITLE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `CODE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '编码',
  `CATEGORY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类',
  `MODULE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块',
  `MENU_TYPE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单类型',
  `PATH` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路径',
  `ICON` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `COLOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '颜色',
  `REG_TYPE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则类型',
  `STATUS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '可用状态',
  `SORT_CODE` int(11) NULL DEFAULT NULL COMMENT '排序码',
  `EXT_JSON` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展信息',
  `DELETE_FLAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标志',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `CREATE_USER` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建用户',
  `UPDATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `UPDATE_USER` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改用户',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '移动资源' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of MOBILE_RESOURCE
-- ----------------------------
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623380023993298945', '-1', NULL, '业务', '217gcp9ifi', 'MODULE', NULL, NULL, NULL, 'container-outlined', '#1890ff', NULL, NULL, 1, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623380258656219138', '-1', '0', '机构管理', NULL, 'MENU', '1623380023993298945', 'MENU', '/pages/biz/org/index', 'apartment-outlined', '#1890ff', 'YES', 'ENABLE', 0, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623380614295449601', '-1', '0', '岗位管理', NULL, 'MENU', '1623380023993298945', 'MENU', '/pages/biz/position/index', 'robot-outlined', '#9c28b1', 'YES', 'ENABLE', 2, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623380765202313218', '-1', '0', '人员管理', NULL, 'MENU', '1623380023993298945', 'MENU', '/pages/biz/user/index', 'team-outlined', '#fed835', 'YES', 'ENABLE', 4, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623381127095250946', '-1', '0', '更多', NULL, 'MENU', '1623380023993298945', 'CATALOG', '7029146815941316608', 'small-dash-outlined', '#f1627e', 'YES', 'ENABLE', 7, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623381298801668098', '-1', '1623380258656219138', '新增机构', 'mobileBizOrgAdd', 'BUTTON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623381632131395586', '-1', '1623380258656219138', '编辑机构', 'mobileBizOrgEdit', 'BUTTON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623381698837606401', '-1', '1623380258656219138', '删除机构', 'mobileBizOrgDelete', 'BUTTON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623696035909316610', '-1', '1623380614295449601', '新增岗位', 'mobileBizPositionAdd', 'BUTTON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623696094872842242', '-1', '1623380614295449601', '编辑岗位', 'mobileBizPositionEdit', 'BUTTON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623696153760870401', '-1', '1623380614295449601', '删除岗位', 'mobileBizPositionDelete', 'BUTTON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623696236267024386', '-1', '1623380765202313218', '新增人员', 'mobileBizUserAdd', 'BUTTON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623696292042878977', '-1', '1623380765202313218', '编辑人员', 'mobileBizUserEdit', 'BUTTON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `MOBILE_RESOURCE` VALUES ('1623696346547859458', '-1', '1623380765202313218', '删除人员', 'mobileBizUserDelete', 'BUTTON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Records of SYS_RESOURCE
-- ----------------------------
INSERT INTO `SYS_RESOURCE` VALUES ('1623378345382506498', '-1', '0', '移动端管理', NULL, NULL, 'MENU', '1548901111999770525', 'CATALOG', '/7029144034425704448', NULL, 'mobile-outlined', NULL, 50, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `SYS_RESOURCE` VALUES ('1623378675591671810', '-1', '1623378345382506498', '模块管理', 'mobileModuleIndex', NULL, 'MENU', '1548901111999770525', 'MENU', '/mobile/module/index', 'mobile/resource/module/index', 'build-outlined', NULL, 51, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);
INSERT INTO `SYS_RESOURCE` VALUES ('1623378996099411969', '-1', '1623378345382506498', '菜单管理', 'mobileMenuIndex', NULL, 'MENU', '1548901111999770525', 'MENU', '/mobile/menu/index', 'mobile/resource/menu/index', 'appstore-add-outlined', NULL, 52, NULL, 'NOT_DELETE', NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
