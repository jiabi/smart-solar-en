package vip.xiaonuo.auth.api;

import vip.xiaonuo.auth.param.AuthAccountPasswordLoginParam;

public interface AuthApi {

    String doLoginTripartite(AuthAccountPasswordLoginParam authAccountPasswordLoginParam, String type);
}
