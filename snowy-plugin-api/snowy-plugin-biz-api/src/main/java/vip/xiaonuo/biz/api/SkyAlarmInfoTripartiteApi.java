package vip.xiaonuo.biz.api;

import vip.xiaonuo.biz.param.SkyAlarmInfoTripartiteVO;

import java.util.List;

public interface SkyAlarmInfoTripartiteApi {

    List<SkyAlarmInfoTripartiteVO> alarmTripartite(List<String> invSns);
}
