package vip.xiaonuo.biz.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SkyInvMonitorCurrentTripartiteParam {

//    private List<String> invSns;
    /** 分销商id */
    private String distributor;
    /** 安装公司id */
    @ApiModelProperty(value = "安装公司id")
    private String stationCompany;

    /** 业主id */
    @ApiModelProperty(value = "业主id")
    private String stationContactId;

    @ApiModelProperty(value = "时间区间开始时间")
    private String startTime;

    @ApiModelProperty(value = "时间区间结束时间")
    private String endTime;


}
