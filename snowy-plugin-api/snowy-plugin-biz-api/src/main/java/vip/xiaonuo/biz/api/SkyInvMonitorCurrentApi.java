package vip.xiaonuo.biz.api;


import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteParam;
import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteVO;

import java.util.List;

public interface SkyInvMonitorCurrentApi {

    List<SkyInvMonitorCurrentTripartiteVO> selectInvMonitorCurrents(List<String> invSns);

}
