package vip.xiaonuo.biz.param;

import lombok.Data;

@Data
public class SkyInvMonitorCurrentTripartiteVO {

    /** ID */
    private Long id;

    /** 设备SN */
    private String invSn;

    /** 逆变器温度 */
    private Double invTemp;

    /** 电池温度（DCDC） */
    private Double dcdcTemp;

    /** 直流输入路数 */
    private Integer pvNum;

    /** 电池路数 */
    private Integer batNum;

    /** 输入功率 */
    private Double monPpv;

    /** 输出功率 */
    private Double monPac;

    /** 当日发电量 */
    private Double monToday;

    /** 累计发电量 */
    private Double monTotal;

    /** 运行状态  */
    private Integer monState;

    /** 电网频率 */
    private Double gridFac;

    /** 电网电压 */
    private Double gridVol;

    /** 电网电流 */
    private Double gridCur;

    /** 累计并网电量 */
    private Double gridEnergyTotal;

    /** 当日购电量 */
    private Double gridPurchaseToday;

    /** 累计购电量 */
    private Double gridPurchaseTotal;

    /** 电池使用次数 */
    private Integer batRuncount;

    /** 电池温度 */
    private Double batTemp;

    /** 电池电压 */
    private Double batVol;

    /** 电池电流 */
    private Double batCur;

    /** 电池功率 */
    private Double batPower;

    /** 累计充电量 */
    private Double batChargingTotal;

    /** 累计放电量 */
    private Double batDischargingTotal;

    /** 当日充电量 */
    private Double batChargingToday;

    /** 直流电压1-10 */
    private Double monPv1vol;
    private Double monPv2vol;
    private Double monPv3vol;
    private Double monPv4vol;
    private Double monPv5vol;
    private Double monPv6vol;
    private Double monPv7vol;
    private Double monPv8vol;
    private Double monPv9vol;
    private Double monPv10vol;

    /** 直流电流1-10 */
    private Double monPv1cur;
    private Double monPv2cur;
    private Double monPv3cur;
    private Double monPv4cur;
    private Double monPv5cur;
    private Double monPv6cur;
    private Double monPv7cur;
    private Double monPv8cur;
    private Double monPv9cur;
    private Double monPv10cur;

    /** A相电网电流、电压 */
    private Double gridRcur;
    private Double gridRvol;

    /** B相电网电流、电压 */
    private Double gridScur;
    private Double gridSvol;

    /** C相电网电流、电压 */
    private Double gridTcur;
    private Double gridTvol;

    /** 并网后总运行时间（min） */
    private Integer runTime;

    /** 电网总功率 */
    private Double monPower;

    /** 当日并网电量 */
    private Double gridEnergyToday;

    /** 母线电压 */
    private Double monBcbus;

    /** 漏电流 */
    private Double monGfci;

    /** 绝缘阻抗 */
    private Double monIos;

    /** 电池容量（单位：kwh） */
    private Double batSize;

    /** 电池健康指数（%） */
    private Double batSoh;

    /** 当日放电量 */
    private Double batDischargingToday;

    /** 电池状态 */
    private Integer batStatus;
}
