package vip.xiaonuo.biz.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import vip.xiaonuo.biz.api.SkyEquQualityApi;
import vip.xiaonuo.biz.api.SkyInvMonitorCurrentApi;
import vip.xiaonuo.biz.param.EquQualityTripartiteVO;
import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteParam;
import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteVO;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class SkyInvMonitorCurrentApiController {

    @Resource
    private SkyInvMonitorCurrentApi skyInvMonitorCurrentApi;

    @ApiOperation("三方设备实时数据")
    @PostMapping("/biz/invCurrent/currentTripartite")
    public CommonResult<List<SkyInvMonitorCurrentTripartiteVO>> selectInvMonitorCurrents(@RequestBody(required=false) List<String> invSns) {
        List<SkyInvMonitorCurrentTripartiteVO> skyInvMonitorCurrentTripartites = skyInvMonitorCurrentApi.selectInvMonitorCurrents(invSns);
        return CommonResult.data(skyInvMonitorCurrentTripartites);
    }
}

