package vip.xiaonuo.biz.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import vip.xiaonuo.biz.api.SkyEquQualityApi;

import vip.xiaonuo.biz.param.EquQualityTripartiteVO;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class SkyEquQualityApiController {

    @Resource
    private SkyEquQualityApi skyEquQualityApi;

    @ApiOperation("三方质保接口")
    @GetMapping("/biz/eququality/equQualityTripartite")
    public CommonResult<EquQualityTripartiteVO> equQualityTripartite(@RequestParam(value = "invSn",required = false) String invSn) {
        EquQualityTripartiteVO equQualityTripartiteVO = skyEquQualityApi.equQualityTripartite(invSn);
        return CommonResult.data(equQualityTripartiteVO);
    }
}
