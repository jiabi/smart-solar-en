package vip.xiaonuo.biz.api;


import vip.xiaonuo.biz.param.EquQualityTripartiteVO;

public interface SkyEquQualityApi {

    EquQualityTripartiteVO equQualityTripartite(String invSn);
}
