package vip.xiaonuo.biz.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.api.SkyAlarmInfoTripartiteApi;
import vip.xiaonuo.biz.param.SkyAlarmInfoTripartiteVO;
import vip.xiaonuo.biz.param.SkyInvMonitorCurrentTripartiteVO;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class SkyAlarmInfoApiController {

    @Resource
    private SkyAlarmInfoTripartiteApi skyAlarmInfoTripartiteApi;
    @ApiOperation("三方设备告警")
    @PostMapping("/biz/alarm/alarmTripartite")
    public CommonResult<List<SkyAlarmInfoTripartiteVO>> alarmTripartite(@RequestBody(required=false) List<String> invSns) {
        List<SkyAlarmInfoTripartiteVO> skyAlarmInfoTripartiteVOS = skyAlarmInfoTripartiteApi.alarmTripartite(invSns);
        return CommonResult.data(skyAlarmInfoTripartiteVOS);
    }
}
