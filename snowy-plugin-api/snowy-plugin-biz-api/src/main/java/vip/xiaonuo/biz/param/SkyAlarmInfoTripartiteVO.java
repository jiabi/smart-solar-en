package vip.xiaonuo.biz.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class SkyAlarmInfoTripartiteVO {

    @ApiModelProperty(value = "id", position = 1)
    private String id;

    /** 电站设备 */
    @ApiModelProperty(value = "设备SN", position = 2)
    private String equSn;

    /** 故障开始时间 */
    @ApiModelProperty(value = "故障开始时间", position = 3)
    private Date beginTime;

    /** 故障结束时间 */
    @ApiModelProperty(value = "故障结束时间", position = 4)
    private Date endTime;

    /** 故障来源 */
    @ApiModelProperty(value = "故障来源", position = 5)
    private String alarmSource;

    /** 故障等级 */
    @ApiModelProperty(value = "故障等级", position = 6)
    private Integer alarmLevel;

    /** 故障代码 */
    @ApiModelProperty(value = "故障代码", position = 7)
    private String alarmCode;

    /** 故障描述 */
    @ApiModelProperty(value = "故障描述", position = 8)
    private String alarmMsg;
}
